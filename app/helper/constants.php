<?php

define("LEGEND_USERS", [2, 4]);
define("DEV_USERS", [2, 4]);
define("ADMIN_USERS", [2, 3, 4]);
define("YES", 'Yes');
define("NO", 'No');
define("LEDGER_LEDGEND", 'Legend Ledger');
define("LEDGER_BANKING", 'Banking Ledger');

define("GENDER_MALE", "Male");
define("GENDER_FEMALE", "Female");
define("GENDER_OTHER", "Other");
define("PROJECT_START_YEAR", 2020);

// User Types
define("USER_TYPE_ADMIN", "Admin");
define("USER_TYPE_DEPO", "Depo");
define("USER_TYPE_DSM", "Dsm");
define("USER_TYPE_ZM", "Zm");
define("USER_TYPE_SR", "Sr");
define("USER_TYPE_DEALER", "Dealer");

// User Status
define("USER_STATUS_ACTIVE", 1);
define("USER_STATUS_INACTIVE", 0);
define("USER_STATUS_BLOCKED", 2);
define("STATUS_PENDING", "Pending");
define("STATUS_PROCESED", "Processed");
define("STATUS_DELIVERED", "Delivered");

// User Role
define("ROLE_SUPERADMIN", 1);
define("ROLE_ADMIN", 2);

// Product Type
define("PRO_RCS", "RCS");
define("PRO_ACCESSORIES", "Accessories");
define("PRO_PACKAGING", "Packaging");
define("PRO_FINISH", "Finish");
// Product Type ID
define("PRODUCT_TYPE_FINISH", 5);

// Voucher Type
define("PAYMENT_VOUCHER", "Payment Voucher");
define("RECEIVE_VOUCHER", "Receive Voucher");
define("JOURNAL_VOUCHER", "Journal Voucher");
define("PURCHASE_VOUCHER", "Purchase Voucher");
define("SALES_VOUCHER", "Sales Voucher");
define("DUE_VOUCHER", "Due Voucher");

// Transaction Type
define("TRANSACTION_ADVANCE", "advance");
define("TRANSACTION_PRE_DUE", "previous due");
define("TRANSACTION_DUE_PAID", "due paid");
define("TRANSACTION_INVOICE", "invoice");
define("TRANSACTION_REGULAR", "regular");
//transaction method
define("TRANSACTION_CASH", "Cash");
define("TRANSACTION_BANK", "Bank");
define("TRANSACTION_NO", "No");

// payment methode
define("PAYMENT_NO", "No Payment");
define("PAYMENT_BANK", "Bank Payment");
define("PAYMENT_CASH", "Cash Payment");
define("PAYMENT_PAID", "Paid");
define("PAYMENT_PAID_PARTIAL", "Partial Paid");
define("PAYMENT_NOT_PAID", "Not Paid");

// Account type
define("ACCOUNT_LC", "LC");
define("ACCOUNT_CURRENT", "CURRENT");
define("ACCOUNT_SAVINGS", "SAVINGS");

// Head ID
define("HEAD_CASH_IN_HAND", 1);
define("HEAD_CASH_AT_BANK", 2);
define("HEAD_DEBTORS", 3);
define("HEAD_CUSTOMER", 3);
define("HEAD_SUPPLIER", 4);
define("HEAD_SR_ACCOUNT", 5);
define("HEAD_SALES_PERSON", 5);
define("HEAD_PURCHASE", 6);
define("HEAD_SALES", 7);
define("HEAD_SALES_PERSON_SALE", 8);
define("HEAD_SR_SALE_ACCOUNT", 8);
define("HEAD_EXPENSE", 9);
define("HEAD_INCOME", 10);
define("HEAD_COST_OF_GOODS", 11);
define("HEAD_ACC_PAYABLE", 12);
define("HEAD_ACC_RECEIVABLE", 13);
define("HEAD_CAPITAL", 14);
define("HEAD_INV_STOCK", 15);
define("HEAD_DSM_ACCOUNT", 16);
define("HEAD_ZM_ACCOUNT", 17);
define("HEAD_DEPO_ACCOUNT", 18);
define("HEAD_DEALER_ACCOUNT", 19);

// Subhead ID
define("SUBHEAD_CASH", 1);
define("SUBHEAD_BANK", 2);
define("SUBHEAD_PURCHASE", 3);
define("SUBHEAD_RETURN", 4);
define("SUBHEAD_SALES", 5);
define("SUBHEAD_AP", 9);
define("SUBHEAD_AR", 10);
define("SUBHEAD_TRANSPORT", 11);
define("SUBHEAD_LABOR", 12);
define("SUBHEAD_DEBTORS", 15);
define("SUBHEAD_CREDITORS", 16);
define("SUBHEAD_COMPANY", 2);
define("SUBHEAD_SALARY", 24);
define("SUBHEAD_ADVANCE_SALARY", 28);
define("SUBHEAD_OPENING", 42);
define("SUBHEAD_STOCK", 43);

// Stock types
define("STOCK_IN", "in");
define("STOCK_OUT", "out");
define("STOCK_DAMAGE", "damage");
define("STOCK_TYPE_OPENING", "Opening");
define("STOCK_TYPE_PURCHASE", "Purchase");
define("STOCK_TYPE_PURCHASE_RETURN", "Purchase Return");
define("STOCK_TYPE_SALE", "Sale");
define("STOCK_TYPE_SALE_RETURN", "Sale Return");
define("STOCK_TYPE_DAMAGE", "Damage");
define("STOCK_TYPE_PRODUCTION", "Production");
define("STOCK_TYPE_DELIVERY", "Delivery");

// Categories
define("CONST_NO", "No");
define("CONST_YES", "Yes");
define("CLOSING_YEAR_NO", 1);

// Categories
define("TARGET_AMOUNT", "amount");
define("TARGET_QUANTITY", "quantity");

// head,subhead,particular (is-common)
define("COMMON_YES", 'Yes');
define("COMMON_NO", 'No');

//stock
define("PROCESS_COMPLETE", "Complete");

