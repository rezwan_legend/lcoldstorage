<?php

function user_types_array() {
    return [
        USER_TYPE_ADMIN => USER_TYPE_ADMIN,
        USER_TYPE_DEPO => USER_TYPE_DEPO,
        USER_TYPE_DSM => USER_TYPE_DSM,
        USER_TYPE_ZM => USER_TYPE_ZM,
        USER_TYPE_SR => USER_TYPE_SR,
        USER_TYPE_DEALER => USER_TYPE_DEALER,
    ];
}

function user_avatar($obj) {
    if (!empty($obj->avatar)) {
        if (File::exists(public_path('/uploads/' . $obj->avatar))) {
            $_avatar = asset('/public/uploads/' . $obj->avatar);
        } else {
            $_avatar = asset('/public/img/no-image.jpg');
        }
    } else {
        $_avatar = asset('/public/img/no-image.jpg');
    }
    return $_avatar;
}

function test_helper() {
    return 'This is test function';
}

function persentange($per, $value) {
    $result = ($per / 100) * $value;
    return $result;
}

function pr($object, $exit = true) {
    echo '<pre>';
    print_r($object);
    echo '</pre>';
    if ($exit == true) {
        exit;
    }
}

function change_lang($str) {
    $_lang = App::getLocale();
    $en = array('0', '1', '2', '3', '4', '5', '6', '7', '8', '9');
    $bn = array('o', '১', '২', '৩', '৪', '৫', '৬', '৭', '৮', '৯');
    if ($_lang == 'bn') {
        $tstr = str_replace($en, $bn, $str);
    } else {
        $tstr = str_replace($bn, $en, $str);
    }
    return $tstr;
}

function to_eng($str) {
    $bn = array('o', '১', '২', '৩', '৪', '৫', '৬', '৭', '৮', '৯');
    $en = array('0', '1', '2', '3', '4', '5', '6', '7', '8', '9');
    $tstr = str_replace($bn, $en, $str);
    return $tstr;
}

function cur_date_time() {
    date_default_timezone_set("Asia/Dhaka");
    return date('Y-m-d H:i:s');
}

function date_ymd($value, $sy = false) {
    $_retVal = "";
    $_format = "Y-m-d";
    if ($sy == true) {
        $_format = "y-m-d";
    }
    if ($value > "0000-00-00") {
        $_retVal = date($_format, strtotime($value));
    }
    return !empty($_retVal) ? $_retVal : '';
}

function date_dmy($value, $sy = false) {
    $_retVal = "";
    $_format = "d-m-Y";
    if ($sy == true) {
        $_format = "d-m-y";
    }
    if ($value > "0000-00-00") {
        $_retVal = date($_format, strtotime($value));
    }
    return !empty($_retVal) ? $_retVal : '';
}

function date_dd($value) {
    return date('d', strtotime($value));
}

function date_mm($value) {
    return date('m', strtotime($value));
}

function date_yy($value) {
    return date('Y', strtotime($value));
}

function bn2enNumber($number) {
    $bn = array("১", "২", "৩", "৪", "৫", "৬", "৭", "৮", "৯", "০");
    $en = array("1", "2", "3", "4", "5", "6", "7", "8", "9", "0");
    $en_number = str_replace($bn, $en, $number);
    return $en_number;
}

function en2bnNumber($number) {
    $bn = array("১", "২", "৩", "৪", "৫", "৬", "৭", "৮", "৯", "০");
    $en = array("1", "2", "3", "4", "5", "6", "7", "8", "9", "0");
    $bn_number = str_replace($en, $bn, $number);
    return $bn_number;
}

function en2bnDate($date) {
    $bn = array(
        '১', '২', '৩', '৪', '৫', '৬', '৭', '৮', '৯', '০', 'জানুয়ারী', 'ফেব্রুয়ারী', 'মার্চ', 'এপ্রিল', 'মে', 'জুন', 'জুলাই', 'আগস্ট', 'সেপ্টেম্বর', 'অক্টোবর', 'নভেম্বর', 'ডিসেম্বর', 'শনিবার', 'রবিবার', 'সোমবার', 'মঙ্গলবার', '
        বুধবার', 'বৃহস্পতিবার', 'শুক্রবার'
    );
    $en = array('1', '2', '3', '4', '5', '6', '7', '8', '9', '0', 'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December', 'Saturday', 'Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday');
    $bn_date = str_replace($en, $bn, $date);
    return $bn_date;
}

function product_type_list() {
    $product_type = array(
        'RCS' => 'RCS',
        'Accessories' => 'Accessories',
        'Packaging' => 'Packaging',
        'Finish' => 'Finish',
    );
    return $product_type;
}

function unit_list() {
    $unit = array(
        'bag' => 'Bag',
        'cartoon' => 'Cartoon',
        'packet' => 'Packet',
        'piece' => 'Piece',
        'roll' => 'Roll',
        'rim' => 'Rim',
        'trolly' => 'Trolly',
        'van' => 'Van',
        'sft' => 'SFT',
        'hour' => 'Hour',
        'kg' => 'Kilogram',
        'inch' => 'Inch',
        'foot' => 'Foot',
        'meter' => 'Meter',
        'leter' => 'Liter',
    );
    return $unit;
}

function month_list() {
    $month = array(
        'January' => 'January',
        'February' => 'February',
        'March' => 'March',
        'April' => 'April',
        'May' => 'May',
        'June' => 'June',
        'July' => 'July',
        'August' => 'August',
        'September' => 'September',
        'October' => 'October',
        'November' => 'November',
        'December' => 'December',
    );
    return $month;
}

function get_months($_id = null) {
    $_months = [
        '1' => 'January',
        '2' => 'February',
        '3' => 'March',
        '4' => 'April',
        '5' => 'May',
        '6' => 'June',
        '7' => 'July',
        '8' => 'August',
        '9' => 'September',
        '10' => 'October',
        '11' => 'November',
        '12' => 'December'
    ];
    if (!is_null($_id)) {
        return $_months[$_id];
    } else {
        return $_months;
    }
}

function month_days($monthVal, $year) {
    return cal_days_in_month(CAL_GREGORIAN, $monthVal, $year);
}

function years_dropdown($_id = null, $_name = null, $_end = null, $_sval = null, $_req = null) {
    $_curYear = date('Y');
    $_yearToRun = !is_null($_end) ? $_end : 2015;

    $_str = "<select class='form-control' ";
    $_str .= !is_null($_id) ? " id='{$_id}' " : " id='year' ";
    $_str .= !is_null($_name) ? " name='{$_name}' " : " name='year' ";
    $_str .= !is_null($_req) ? " $_req " : " ";
    $_str .= ">";

    for ($i = $_curYear; $i >= $_yearToRun; $i--) :
        $_active = '';
        if (!is_null($_sval) && ($_sval == $i)) {
            $_active = ' selected';
        }
        $_str .= "<option value='{$i}' {$_active}>{$i}</option>";
    endfor;

    $_str .= "</select>";
    return $_str;
}

function salesCommissionPerchantageList() {
    return [
        '100' => '100%',
        '90' => '90%',
        '80' => '80%',
        '70' => '70%',
        '60' => '60%',
        '50' => '50%',
        '40' => '40%',
        '30' => '30%',
        '20' => '20%',
        '10' => '10%',
    ];
}

function commissionCalculaton($percentage) {
    $data = 0;
    switch ($percentage) {
        case (($percentage >= 10) && ($percentage < 20)):
            $data = 10;
            break;

        case (($percentage >= 20) && ($percentage < 30)):
            $data = 20;
            break;

        case (($percentage >= 30) && ($percentage < 40)):
            $data = 30;
            break;

        case (($percentage >= 40) && ($percentage < 50)):
            $data = 40;
            break;

        case (($percentage >= 50) && ($percentage < 60)):
            $data = 50;
            break;

        case (($percentage >= 60) && ($percentage < 70)):
            $data = 60;
            break;

        case (($percentage >= 70) && ($percentage < 80)):
            $data = 70;
            break;

        case (($percentage >= 80) && ($percentage < 90)):
            $data = 80;
            break;

        case (($percentage >= 90) && ($percentage < 100)):
            $data = 90;
            break;

        case (($percentage >= 100)):
            $data = 100;
            break;

        default:
            $data = 0;
            break;
    }
    return $data;
}

function category_type_list() {
    $unit = array(
        'Raw' => 'Raw',
        'Finish' => 'Finish',
        'Others' => 'Others',
    );
    return $unit;
}

function bank_account_type_list() {
    $bank_type = array(
        'CC Hypo' => 'CC Hypo Account',
        'CC Place' => 'CC Place Account',
        'Current' => 'Current Account',
        'LC' => 'LC Account',
        'Personal' => 'Personal Account',
        'Savings' => 'Savings Account',
        'SME' => 'SME Account',
    );
    return $bank_type;
}

function user_role_list() {
    $user_role = array(
        '1' => 'Super Admin',
        '2' => 'Admin',
        '3' => 'Editor',
    );
    return $user_role;
}

function sub($a, $b) {
    return ($a - $b);
}

function sub3($a, $b, $c) {
    return ($a - $b - $c);
}

function uniqueKey() {
    return strtoupper(uniqid() . date('s'));
}

function agent_code() {
    return strtoupper(substr(uniqid() . date('s'), 10, 5));
}

function has_flash_message() {
    return Session::has('success') or Session::has('info') or Session::has('warning') or Session::has('danger');
}

function view_flash_message() {
    $retVal = "";
    if (Session::get('success')) {
        $retVal = "<div class='alert alert-success'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>" . Session::get('success') . "</div>";
    } elseif (Session::get('info')) {
        $retVal = "<div class='alert alert-info'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>" . Session::get('info') . "</div>";
    } elseif (Session::get('warning')) {
        $retVal = "<div class='alert alert-warning'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>" . Session::get('warning') . "</div>";
    } elseif (Session::get('danger')) {
        $retVal = "<div class='alert alert-danger'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>" . Session::get('danger') . "</div>";
    } else {
        $retVal = "";
    }

    return $retVal;
}

function get_payment_method() {
    $paymode = array(
        'cash_payment' => 'Cash Payment',
        'check_payment' => 'Check Payment',
        'bkash' => 'Bkash',
        'rocket' => 'Rocket',
        'mkash' => 'Mcash',
        'tt' => 'TT',
        'ukash' => 'Ucash',
    );

    return $paymode;
}

function get_vehicle_type() {
    $vehicle = array(
        'truck' => 'Truck',
        'pickup' => 'Pickup',
        'trolly' => 'Trolly',
        'van' => 'Van',
        'rickshaw' => 'Rickshaw'
    );
    return $vehicle;
}

function show_float_number($number) {
    $result = number_format((float) $number, 2, '.', '');
    return $result;
}

function number_dropdown($start, $end, $jump, $page_size = null, $xsw_cls = '') {
    $setting = \App\Models\Setting::get_setting();
    $_page = $setting->pagesize;
    if (!is_null($page_size)) {
        $_page = $page_size;
    }
    $_xscls = !empty($xsw_cls) ? $xsw_cls : 'xsw_20';
    $_str = "<select id='itemCount' class='form-control {$_xscls}' name='item_count' style='width:55px'>";

    for ($i = $start; $i <= $end; $i += $jump) :
        if ($i == $_page) {
            $_str .= "<option value='{$i}' selected='selected'>{$i}</option>";
        } else {
            $_str .= "<option value='{$i}'>{$i}</option>";
        }
    endfor;

    $_str .= "</select>";
    return $_str;
}

function page_size_dropdown($xsw_cls = '') {
    $_pageSize = \App\Models\Setting::get_setting()->pagesize;
    $_numbers = [25, 50, 100, 200, 300, 400, 500];
    $_xscls = !empty($xsw_cls) ? $xsw_cls : 'xsw_20';
    $_str = "<select class='form-control {$_xscls}' id='itemCount' name='item_count' style='width:55px'>";
    foreach ($_numbers as $_key => $val) {
        if ($val == $_pageSize) {
            $_str .= "<option value='{$val}' selected>{$val}</option>";
        } else {
            $_str .= "<option value='{$val}'>{$val}</option>";
        }
    }
    $_str .= "</select>";
    return $_str;
}

function permission_modules() {
    return [
        "module_permission_settings" => "Settings",
        "module_permission_hr" => "HR Management",
        "module_permission_account" => "Account",
        "module_permission_inventory" => "Inventory",
        "module_permission_production" => "Production",
        "module_permission_distribution" => "Distriburion",
        "module_permission_depo" => "Depo",
        "module_permission_sr" => "SR",
        "module_permission_user" => "User",
    ];
}

function module_permission_settings() {
    return [
        "generel_setting" => "General Setting",
        "generel_setting_update" => "General Setting Update",
        "balancesheet_setting_list" => "Balance Sheet Setting list",
        "invoice_discount_list" => "Invoice Discount List",
        "invoice_discount_create" => "Invoice Discount create",
        "invoice_discount_edit" => "Invoice Discount Edit",
        "invoice_discount_delete" => "Invoice Discount Delete",
        "sale_person_setting_list" => "Sale Person Setting List",
        "sale_person_setting_create" => "Sale Person Setting Create",
        "sale_person_setting_edit" => "Sale Person Edit",
        "sale_person_setting_delete" => "Sale Person Delete",
        "dealer_setting_list" => "Dealer Setting List",
        "dealer_setting_create" => "Dealer Setting Create",
        "dealer_setting_edit" => "Dealer Setting Edit",
        "dealer_setting_delete" => "Dealer Setting Delete",
    ];
}

function module_permission_hr() {
    return [
        "designation_list" => "Designation List",
        "designation_create" => "Designation Create",
        "designation_edit" => "Designation Edit",
        "designation_delete" => "Designation Delete",
        "employee_list" => "Employee List",
        "employee_create" => "Employee Create",
        "employee_edit" => "Employee Edit",
        "employee_delete" => "Employee Delete",
        "employee_details" => "Employee Details",
        "employee_ledger" => "Employee Ledger",
        "salary_list" => "Salary List",
        "salary_create" => "Salary Create",
        "salary_edit" => "Salary Edit",
        "salary_delete" => "Salary Delete",
        "salary_ledger" => "Salary Ledger",
    ];
}

function module_permission_account() {
    return [
        "balancesheet_view" => "Balancesheet View",
        "daily_report" => "Daily Report View",
        "dsrdaily_report" => "DSR Daily Report View",
        "ledger_list" => "Ledger List",
        "ledger_view" => "Ledger View",
        "financial_statement" => "Financial Statement",
        "profit_loss" => "Profit/Loss Statement",
        "head_list" => "Head List",
        "head_create" => "Head Create",
        "head_edit" => "Head Edit",
        "head_delete" => "Head Delete",
        "head_ledger" => "Head Ledger",
        "subhead_list" => "Subhead List",
        "subhead_create" => "Subhead Create",
        "subhead_edit" => "Subhead Edit",
        "subhead_delete" => "Subhead Delete",
        "subhead_ledger" => "Subhead Ledger",
        "subhead_opening" => "Subhead Opening",
        "particular_list" => "Particular List",
        "particular_create" => "Particular Create",
        "particular_edit" => "Particular Edit",
        "particular_delete" => "Particular Delete",
        "particular_ledger" => "Particular Ledger",
        "particular_opening" => "Particular Opening",
        "purpose_list" => "Purpose List",
        "purpose_create" => "Purpose Create",
        "purpose_edit" => "Purpose Edit",
        "purpose_delete" => "Purpose Delete",
        "purpose_opening" => "Purpose Opening",
        "transaction_list" => "Transaction List",
        "transaction_create" => "Transaction Create",
        "transaction_edit" => "Transaction Edit",
        "transaction_delete" => "Transaction Delete",
        "transaction_detail" => "Transaction Detail",
    ];
}

function module_permission_inventory() {
    return [
        "product_type_list" => "Product Type List",
        "product_type_create" => "Product Type Create",
        "product_type_edit" => "Product Type Edit",
        "product_type_delete" => "Product Type Delete",
        "category_list" => "Category List",
        "category_create" => "Category Create",
        "category_edit" => "Category Edit",
        "category_delete" => "Category Delete",
        "product_list" => "Product List",
        "product_create" => "Product Create",
        "product_edit" => "Product Edit",
        "product_delete" => "Product Delete",
        "product_description_list" => "Product Description List",
        "product_description_create" => "Product Description Create",
        "product_description_edit" => "Product Description Edit",
        "product_description_delete" => "Product Description Delete",
        "purchase_price_setting" => "Purchase Price Setting",
        "purchase_price_setting_create" => "Purchase Price Setting Create",
        "purchase_price_setting_edit" => "Purchase Price Setting Edit",
        "purchase_price_setting_delete" => "Purchase Price Setting Delete",
        "sale_price_setting" => "Sale Price Setting",
        "sale_price_setting_create" => "Sale Price Setting Create",
        "sale_price_setting_edit" => "Sale Price Setting Edit",
        "sale_price_setting_delete" => "Sale Price Setting Delete",
        "purchase_list" => "Purchase List",
        "purchase_create" => "Purchase Create",
        "purchase_edit" => "Purchase Edit",
        "purchase_delete" => "Purchase Delete",
        "purchase_items" => "Purchase Items",
        "purchase_ledger" => "Purchase Ledger",
        "purchase_detail" => "Purchase Detail",
        "purchase_process" => "Purchase Process",
        "purchase_reset" => "Purchase Reset",
        "sales_list" => "Sales List",
        "sales_create" => "Sales Create",
        "sales_edit" => "Sales Edit",
        "sales_delete" => "Sales Delete",
        "sales_detail" => "Sales Detail",
        "sales_items" => "Sales Items",
        "sales_ledger" => "Sales Ledger",
        "sales_process" => "Sales Process",
        "sales_reset" => "Sales Reset",
        "purchase_return_list" => "Purchase Return List",
        "purchase_return_create" => "Purchase Return Create",
        "purchase_return_edit" => "Purchase Return Edit",
        "purchase_return_delete" => "Purchase Return Delete",
        "purchase_return_detail" => "Purchase Return Detail",
        "purchase_return_items" => "Purchase Return Items",
        "purchase_return_ledger" => "Purchase Return Ledger",
        "purchase_return_process" => "Purchase Return Process",
        "purchase_return_reset" => "Purchase Return Reset",
        "sales_return_list" => "Sales Return List",
        "sales_return_create" => "Sales Return Create",
        "sales_return_edit" => "Sales Return Edit",
        "sales_return_delete" => "Sales Return Delete",
        "sales_return_detail" => "Sales Return Detail",
        "sales_return_items" => "Sales Return Items",
        "sales_return_ledger" => "Sales Return Ledger",
        "sales_return_process" => "Sales Return Process",
        "sales_return_reset" => "Sales Return Reset",
        "stocks_view" => "Stocks View",
    ];
}

function module_permission_production() {
    return [
        "production_type_list" => "Production Type List",
        "production_type_create" => "Production Type Create",
        "production_type_edit" => "Production Type Edit",
        "production_type_delete" => "Production Type Delete",
        "production_order_list" => "Production Order List",
        "production_order_create" => "Production Order Create",
        "production_order_edit" => "Production Order Edit",
        "production_order_delete" => "Production Order Delete",
        "production_order_details" => "Production Order Details",
        "production_order_items" => "Production Order Items",
        "production_order_process" => "Production Order Process",
        "production_order_reset" => "Production Order Reset",
        "production_depot_list" => "Production Depot List",
        "production_depot_create" => "Production Depot Create",
        "production_depot_edit" => "Production Depot Edit",
        "production_depot_delete" => "Production Depot Delete",
        "production_depot_details" => "Production Depot Details",
        "production_depot_items" => "Production Depot Items",
        "production_depot_process" => "Production Depot Process",
        "production_depot_reset" => "Production Depot Reset",
        "raw_setting_list" => "Raw Setting List",
        "raw_setting_create" => "Raw Setting Create",
        "raw_setting_edit" => "Raw Setting Edit",
        "raw_setting_delete" => "Raw Setting Delete",
        "raw_setting_details" => "Raw Setting Details",
        "raw_setting_items" => "Raw Setting Items",
        "cost_setting_list" => "Cost Setting List",
        "cost_setting_create" => "Cost Setting Create",
        "cost_setting_edit" => "Cost Setting Edit",
        "cost_setting_delete" => "Cost Setting Delete",
        "cost_setting_details" => "Cost Setting Details",
        "cost_setting_items" => "Cost Setting Items",
    ];
}

function module_permission_distribution() {
    return [
        "distribution_type_list" => "Distribution Type List",
        "distribution_type_create" => "Distribution Type Create",
        "distribution_type_edit" => "Distribution Type Edit",
        "distribution_type_delete" => "Distribution Type Delete",
        "distribution_setting_list" => "Distribution Setting List",
        "distribution_setting_create" => "Distribution Setting Create",
        "distribution_setting_edit" => "Distribution Setting Edit",
        "distribution_setting_delete" => "Distribution Setting Delete",
        "distribution_setting_detail" => "Distribution Setting Detail",
        "target_list" => "Target List",
        "target_create" => "Target Create",
        "target_edit" => "Target Edit",
        "target_delete" => "Target Delete",
        "target_detail" => "Target Detail",
        "target_items" => "Target Items",
        "commission_list" => "Commission List",
        "commission_create" => "Commission Create",
        "commission_edit" => "Commission Edit",
        "commission_delete" => "Commission Delete",
        "commission_detail" => "Commission Detail",
        "monthly_commission_report" => "Monthly Commission Report",
        /* my code start here */
        "region_list" => "Region List",
        "region_edit" => "Region Update",
        "region_create" => "New Region",
        "region_delete" => "Delete Region",
        "zone_list" => "Zone List",
        "zone_edit" => "Update Zone",
        "zone_create" => "New Zone",
        "zone_delete" => "Delete Zone",
        "base_list" => "Base List",
        "base_create" => "New Base",
        "base_edit" => "Update Base",
        "base_delete" => "Delete Base",
    ];
}

function module_permission_depo() {
    return [
        "depo_stocks_view" => "Stocks View",
        "depo_sales_order_list" => "Sales Order List",
        "depo_delivery_list" => "Delivery List",
        "depo_delivery_create" => "Delivery Create",
        "depo_delivery_edit" => "Delivery Edit",
        "depo_delivery_delete" => "Delivery Delete",
        "depo_delivery_items" => "Delivery Item List",
        "depo_delivery_ledger" => "Delivery Ledger",
        "depo_delivery_detail" => "Delivery Detail",
        "depo_delivery_process" => "Delivery Process",
        "depo_delivery_reset" => "Delivery Reset",
        "monthly_sales_report" => "Monthly Sales Report",
        "product_sales_report" => "Product Sales Report",
    ];
}

function module_permission_sr() {
    return [
        "sr_sales_list" => "Sales List",
        "sr_sales_create" => "Sales Create",
        "sr_sales_edit" => "Sales Edit",
        "sr_sales_delete" => "Sales Delete",
        "sr_sales_detail" => "Sales Detail",
        "sr_sales_ledger" => "Sales Ledger",
        "sr_sales_process" => "Sales Process",
        "sr_sales_reset" => "Sales Reset",
        "sr_sales_item_list" => "Sales Item List",
    ];
}

function module_permission_user() {
    return [
        "user_list" => "User List",
        "user_create" => "User Create",
        "user_edit" => "User Edit",
        "user_delete" => "User Delete",
        "user_access_control" => "User Access Control",
        "user_status" => "User Status",
    ];
}

function user_access_items() {
    $access_items = array(
        "setting" => "Setting",
        "generel_setting" => "General Setting",
        "generel_setting_update" => "General Setting Update",
        "category_list" => "Category List",
        "category_create" => "Category Create",
        "category_edit" => "Category Edit",
        "category_delete" => "Category Delete",
        "product_list" => "Product List",
        "product_create" => "Product Create",
        "product_edit" => "Product Edit",
        "product_delete" => "Product Delete",
        "purchase_list" => "Purchase List",
        "purchase_create" => "Purchase Create",
        "purchase_edit" => "Purchase Edit",
        "purchase_delete" => "Purchase Delete",
        "purchase_detail" => "Purchase Detail",
        "purchase_process" => "Purchase Process",
        "purchase_reset" => "Purchase Reset",
        "sales_list" => "Sales List",
        "sales_create" => "Sales Create",
        "sales_edit" => "Sales Edit",
        "sales_delete" => "Sales Delete",
        "sales_detail" => "Sales Detail",
        "sales_process" => "Sales Process",
        "sales_reset" => "Sales Reset",
        "stocks_view" => "Stocks View",
        "manage_user" => "Manage User",
        "user_create" => "User Create",
        "user_edit" => "User Edit",
        "user_delete" => "User Delete",
        "user_access_control" => "User Access Control",
        "user_status" => "User Status",
        "manage_accounts" => "Manage Accounts",
        "head_list" => "Head List",
        "head_create" => "Head Create",
        "head_edit" => "Head Edit",
        "head_delete" => "Head Delete",
        "head_ledger" => "Head Ledger",
        "subhead_list" => "Subhead List",
        "subhead_create" => "Subhead Create",
        "subhead_edit" => "Subhead Edit",
        "subhead_delete" => "Subhead Delete",
        "subhead_ledger" => "Subhead Ledger",
        "particular_list" => "Particular List",
        "particular_create" => "Particular Create",
        "particular_edit" => "Particular Edit",
        "particular_delete" => "Particular Delete",
        "particular_ledger" => "Particular Ledger",
        "transaction_list" => "Transaction List",
        "transaction_create" => "Transaction Create",
        "transaction_edit" => "Transaction Edit",
        "transaction_delete" => "Transaction Delete",
        "transaction_detail" => "Transaction Detail",
        "manage_reporting" => "Manage Reporting",
        "retailer_list" => "Retailer Setting",
        "retailer_create" => "Retailer Create",
        "retailer_edit" => "Retailer Edit",
        "retailer_delete" => "Retailer Delete",
        "retailer_detail" => "Retailer Detail",
        "brand_list" => "Brand Setting",
        "brand_create" => "Brand Create",
        "brand_edit" => "Brand Edit",
        "brand_delete" => "Brand Delete",
        "brand_detail" => "Brand Detail",
        "target_list" => "Target Setting",
        "target_create" => "Target Create",
        "target_edit" => "Target Edit",
        "target_delete" => "Target Delete",
        "target_detail" => "Target Detail",
        "dsr_sales_list" => "Dsr Sales List",
        "dsr_sales_create" => "Dsr Sales Create",
        "dsr_sales_edit" => "Dsr Sales Edit",
        "dsr_sales_delete" => "Dsr Sales Delete",
        "dsr_sales_process" => "Dsr Sales Process",
        "dsr_sales_detail" => "Dsr Sales Detail",
    );
    return $access_items;
}

function default_user_access_items() {
    $access_items = array(
        "setting" => "Setting",
        "generel_setting" => "General Setting",
    );
    return $access_items;
}

function default_dsr_access_items() {
    $access_items = array(
        "module_permission_dsr" => "Sales Person",
        "retailer_list" => "Retailer List",
        "retailer_detail" => "Retailer Detail",
        "brand_list" => "Brand List",
        "brand_detail" => "Brand Detail",
        "commission_list" => "Commission List",
        "commission_detail" => "Commission Detail",
        "target_list" => "Target List",
        "target_detail" => "Target Detail",
        "dsr_stocks_view" => "Stocks View",
        "dsr_sales_list" => "Sales List",
        "dsr_sales_create" => "Sales Create",
        "dsr_sales_edit" => "Sales Edit",
        "dsr_sales_delete" => "Sales Delete",
        "dsr_sales_detail" => "Sales Detail",
        "dsr_sales_ledger" => "Sales Ledger",
        "dsr_sales_process" => "Sales Process",
        "dsr_sales_item_list" => "Sales Item List",
        "dsr_monthly_sales_report" => "Monthly Sales Report",
        "dsr_sales_invoice_list" => "Sales Invoice List",
    );
    return $access_items;
}

function has_user_access($access) {
    $id = Auth::id();
    $permissions = \App\Models\UserPermission::where('user_id', '=', $id)->first()->items;
    $access_items = json_decode($permissions, true);
    if (!empty($access_items)) {
        if (array_key_exists($access, $access_items) || in_array($id, LEGEND_USERS)) {
            return true;
        } else {
            return false;
        }
    } else {
        return false;
    }
}

function check_user_access($access) {
    $_backUrl = '/home';
    if (isset($_SERVER['HTTP_REFERER']) && !empty($_SERVER['HTTP_REFERER'])) {
        $_backUrl = $_SERVER['HTTP_REFERER'];
    }

    if (has_user_access($access)) {
        return true;
    } else {
        return redirect($_backUrl)->with('danger', 'You are not authorized to perform this action')->send();
    }
}

function show_percentage($total, $percentage) {
    return ($percentage / 100) * $total;
}

function get_division_floor($div, $divider) {
    if ($divider == 0) {
        return null;
    } else {
        return floor($div / $divider);
    }
}

function get_modulus($div, $divider) {
    if ($divider == 0) {
        return null;
    } else {
        return $div % $divider;
    }
}

// Functions copied from Rakib Vai's Project
function cleanValue($data) {
    if (isset($data) && !empty($data)) {
        return trim($data);
    }
}

function dbTimestamp() {
    date_default_timezone_set('Asia/Dhaka');
    return date('Y-m-d h:i:s');
}

function dbDate() {
    date_default_timezone_set('Asia/Dhaka');
    return date('Y-m-d');
}

function getIp() {
    if (!empty($_SERVER['HTTP_CLIENT_IP'])) {   //check ip from share internet
        $ip = $_SERVER['HTTP_CLIENT_IP'];
    } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {   //to check ip is pass from proxy
        $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
    } else {
        $ip = $_SERVER['REMOTE_ADDR'];
    }
    return $ip;
}

function getBrowser($retType = '') {
    $u_agent = $_SERVER['HTTP_USER_AGENT'];
    $bname = 'Unknown';
    $platform = 'Unknown';
    $version = "";

    //First get the platform?
    if (preg_match('/linux/i', $u_agent)) {
        $platform = 'linux';
    } elseif (preg_match('/macintosh|mac os x/i', $u_agent)) {
        $platform = 'mac';
    } elseif (preg_match('/windows|win32/i', $u_agent)) {
        $platform = 'Win';
        if (preg_match('/NT 6.2/i', $u_agent)) {
            $platform .= ' 8';
        } elseif (preg_match('/NT 6.3/i', $u_agent)) {
            $platform .= ' 8.1';
        } elseif (preg_match('/NT 6.1/i', $u_agent)) {
            $platform .= ' 7';
        } elseif (preg_match('/NT 6.0/i', $u_agent)) {
            $platform .= ' Vista';
        } elseif (preg_match('/NT 5.1/i', $u_agent)) {
            $platform .= ' XP';
        } elseif (preg_match('/NT 5.0/i', $u_agent)) {
            $platform .= ' 2000';
        }
        if (preg_match('/WOW64/i', $u_agent) || preg_match('/x64/i', $u_agent)) {
            $platform .= ' (x64)';
        }
    }

    $ub = '';
    // Next get the name of the useragent yes seperately and for good reason
    if (preg_match('/msie/i', $u_agent)) {
        $bname = 'MSIE';
        $ub = "MSIE";
    } elseif (preg_match('/Firefox/i', $u_agent)) {
        $bname = 'Firefox';
        $ub = "Firefox";
    } elseif (preg_match('/Chrome/i', $u_agent)) {
        $bname = 'Chrome';
        $ub = "Chrome";
    } elseif (preg_match('/Safari/i', $u_agent)) {
        $bname = 'Safari';
        $ub = "Safari";
    } elseif (preg_match('/Opera/i', $u_agent)) {
        $bname = 'Opera';
        $ub = "Opera";
    } elseif (preg_match('/Netscape/i', $u_agent)) {
        $bname = 'Netscape';
        $ub = "Netscape";
    }

    // finally get the correct version number
    $known = array('Version', $ub, 'other');
    $pattern = '#(?<browser>' . join('|', $known) .
            ')[/ ]+(?<version>[0-9.|a-zA-Z.]*)#';
    if (!preg_match_all($pattern, $u_agent, $matches)) {
        // we have no matching number just continue
    }

    // see how many we have
    $i = count($matches['browser']);
    if ($i != 1) {
        //we will have two since we are not using 'other' argument yet
        //see if version is before or after the name
        if (strripos($u_agent, "Version") < strripos($u_agent, $ub)) {
            $version = $matches['version'][0];
        } else {
            $version = $matches['version'][1];
        }
    } else {
        $version = $matches['version'][0];
    }

    // check if we have a number
    if ($version == null || $version == "") {
        $version = "?";
    }

    if ($retType == 'json') {
        $_browser = array();
        $_browser['userAgent'] = $u_agent;
        $_browser['name'] = $bname;
        $_browser['version'] = $version;
        $_browser['platform'] = $platform;
        $_browser['pattern'] = $pattern;
        return json_encode($_browser);
    } else {
        $_browser = new stdClass();
        $_browser->userAgent = $u_agent;
        $_browser->name = $bname;
        $_browser->version = $version;
        $_browser->platform = $platform;
        $_browser->pattern = $pattern;
    }

    return $_browser;
}

function print_header($title, $_mpmt = true, $_visible = false) {
    $_sip = ($_visible == true) ? '' : 'show_in_print';
    $_mt = ($_mpmt == false) ? '' : 'mpmt';

    $setting = \App\Models\Setting::get_setting();
    $str = "<div class='invoice_heading {$_mt} {$_sip}'>";
    $str .= "<div class='text-center'>";
    $str .= "<h3 class='print_title' style='margin:0px;'>{$setting->title}</h3>";
    $str .= "<span><strong>" . trans('words.proprietor') . " :</strong> {$setting->owner}</span><br/>";
    $str .= "<span><strong>" . trans('words.address') . " :</strong> {$setting->address}</span><br/>";
    $str .= "<span><strong>" . trans('words.contact') . " :</strong> " . change_lang($setting->mobile) . "</span><br/>";
    $str .= "<span class='print_subtitle' style='display:block;text-decoration:underline;'>{$title}</span>";
    $str .= "</div>";
    $str .= "</div>";
    echo $str;
}

function int_to_words($x) {
    $nwords = array("zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten", "eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen", "nineteen", "twenty", 30 => "thirty", 40 => "forty", 50 => "fifty", 60 => "sixty", 70 => "seventy", 80 => "eighty", 90 => "ninety");
    if (!is_numeric($x)) {
        $w = '#';
    } else if (fmod($x, 1) != 0) {
        $w = '#';
    } else {
        if ($x < 0) {
            $w = 'minus ';
            $x = -$x;
        } else {
            $w = '';
        }
        if ($x < 21) {
            $w .= $nwords[floor($x)];
        } else if ($x < 100) {
            $w .= $nwords[10 * floor($x / 10)];
            $r = fmod($x, 10);
            if ($r > 0) {
                $w .= ' ' . $nwords[$r];
            }
        } else if ($x < 1000) {
            $w .= $nwords[floor($x / 100)] . ' hundred';
            $r = fmod($x, 100);
            if ($r > 0) {
                $w .= ' and ' . int_to_words($r);
            }
        } else if ($x < 100000) {
            $w .= int_to_words(floor($x / 1000)) . ' thousand';
            $r = fmod($x, 1000);
            if ($r > 0) {
                $w .= ' ';
                if ($r < 100) {
                    $w .= 'and ';
                }
                $w .= int_to_words($r);
            }
        } else {
            $w .= int_to_words(floor($x / 100000)) . ' lakh';
            $r = fmod($x, 100000);
            if ($r > 0) {
                $w .= ' ';
                if ($r < 100) {
                    $word .= 'and ';
                }
                $w .= int_to_words($r);
            }
        }
    }
    return $w;
}

function isAdminUser() {
    if (in_array(Auth::id(), ADMIN_USERS)) {
        return true;
    }
    return false;
}

function hasUserAccess() {
    if (in_array(Auth::id(), ADMIN_USERS)) {
        return true;
    }
    return false;
}

function isLegend() {
    if (in_array(Auth::id(), LEGEND_USERS)) {
        return true;
    }
    return false;
}

function multi_array_to_single_array($array) {
    if (!is_array($array)) {
        return FALSE;
    }
    $result = array();
    foreach ($array as $key => $value) {
        if (is_array($value)) {
            $result = array_merge($result, array_flatten($value));
        } else {
            $result[$key] = $value;
        }
    }
    return $result;
}

function read_barcode($catId) {
    $_category = App\Models\Category::find($catId);
    return $_category->read_barcode;
}

function hasBarcode($catId) {
    $_value = App\Models\Category::find($catId)->read_barcode;
    if ($_value == CONST_YES) {
        return true;
    }
    return false;
}

function random_number($length) {
    $chars = array_merge(range(0, 9), range(9, 0));
    shuffle($chars);
    $_number = implode(array_slice($chars, 0, $length));
    return $_number;
}

function limit_text_length($text, $len) {
    if (strlen($text) < $len) {
        return $text;
    }
    $text_words = explode(' ', $text);
    $out = null;

    foreach ($text_words as $word) {
        if ((strlen($word) > $len) && $out == null) {
            return substr($word, 0, $len) . "...";
        }
        if ((strlen($out) + strlen($word)) > $len) {
            return $out . "...";
        }
        $out .= " " . $word;
    }
    return $out;
}
