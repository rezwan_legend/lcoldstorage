<?php

function isLegend() {
    if (in_array(Auth::id(), LEGEND_USERS)) {
        return true;
    }
    return false;
}

function isAdmin() {
    if (in_array(Auth::id(), ADMIN_USERS)) {
        return true;
    }
    return false;
}

function pr($object, $exit = true) {
    echo '<pre>';
    print_r($object);
    echo '</pre>';

    if ($exit == true) {
        exit;
    }
}

function urlEncrypt($id) {
    return Crypt::encrypt($id);
}

function urlDecrypt($id) {
    return Crypt::decrypt($id);
}

function get_months($_id = null) {
    $_months = [
        '1' => 'January',
        '2' => 'February',
        '3' => 'March',
        '4' => 'April',
        '5' => 'May',
        '6' => 'June',
        '7' => 'July',
        '8' => 'August',
        '9' => 'September',
        '10' => 'October',
        '11' => 'November',
        '12' => 'December'
    ];
    if (!is_null($_id)) {
        return $_months[$_id];
    } else {
        return $_months;
    }
}

function year_list() {
    $_arr = [];
    $_curYear = date('Y');
    $_yearToRun = 2020;

    for ($i = $_curYear; $i >= $_yearToRun; $i--):
        $_arr[$i] = $i;
    endfor;
    return $_arr;
}

function adminMsg() {
    return redirect()->back()->with('danger', 'Please contact with Head office to perform this action')->send();
}

function date_ymd($value, $sy = false) {
    $_retVal = "";
    $_format = "Y-m-d";
    if ($sy == true) {
        $_format = "y-m-d";
    }
    if ($value > "0000-00-00") {
        $_retVal = date($_format, strtotime($value));
    }
    return !empty($_retVal) ? $_retVal : '';
}

function date_dmy($value, $sy = false) {
    $_retVal = "";
    $_format = "d-m-Y";
    if ($sy == true) {
        $_format = "d-m-y";
    }
    if ($value > "0000-00-00") {
        $_retVal = date($_format, strtotime($value));
    }
    return !empty($_retVal) ? $_retVal : '';
}

function date_mm($value) {
    return date('m', strtotime($value));
}

function date_yy($value) {
    return date('Y', strtotime($value));
}

function uniqueKey() {
    return strtoupper(uniqid() . date('s'));
}

function has_flash_message() {
    return Session::has('success') OR Session::has('info') OR Session::has('warning') OR Session::has('danger');
}

function view_flash_message() {
    $retVal = "";
    if (Session::get('success')) {
        $retVal = "<div class='alert alert-success no_mrgn'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>" . Session::get('success') . "</div>";
    } elseif (Session::get('info')) {
        $retVal = "<div class='alert alert-info no_mrgn'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>" . Session::get('info') . "</div>";
    } elseif (Session::get('warning')) {
        $retVal = "<div class='alert alert-warning no_mrgn'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>" . Session::get('warning') . "</div>";
    } elseif (Session::get('danger')) {
        $retVal = "<div class='alert alert-danger no_mrgn'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>" . Session::get('danger') . "</div>";
    } else {
        $retVal = "";
    }

    return $retVal;
}

function user_access_item_by_institute($id) {
    $permissions = DB::table('institute_permissions')->where('institute_id', '=', $id)->first()->permissions;
    $access_items = json_decode($permissions, true);
    return $access_items;
}

function permission_admin() {
    $access_items = array(
        'setting' => 'Setting',
        'generel_setting' => 'General Setting',
        'generel_setting_update' => 'General Setting Update',
        'manage_user' => 'Manage User',
        'user_access' => 'User Access',
        'user_status' => 'User Status',
        'manage_institute' => 'Manage Institute',
        'institute_access' => 'Institute Access',
        'institute_status' => 'Institute Status',
        'reset_order' => 'Reset Order',
        'manage_category' => 'Manage Category',
        'manage_product' => 'Manage Product',
        'category_create' => 'Category Create',
        'category_edit' => 'Category Edit',
        'category_delete' => 'Category Delete',
        'product_create' => 'Product Create',
        'product_edit' => 'Product Edit',
        'product_delete' => 'Product Delete',
    );
    return $access_items;
}

function permission_filling() {
    $access_items = array(
        'filling_category' => 'filling Category',
        'filling_category_create' => 'filling Category Create',
        'filling_category_edit' => 'filling Category Edit',
        'filling_category_delete' => 'filling Category Delete',
        'filling_product' => 'filling Product',
        'filling_product_create' => 'filling Product Create',
        'filling_product_edit' => 'filling Product Edit',
        'filling_product_delete' => 'filling Product Delete',
        'filling_stocks' => 'filling Stocks',
        'filling_purchase' => 'filling Purchase',
        'filling_purchase_create' => 'filling Purchase Create',
        'filling_purchase_edit' => 'filling Purchase Edit',
        'filling_purchase_delete' => 'filling Purchase Delete',
        'filling_sale' => 'filling Delivery',
        'filling_sale_create' => 'filling Sale Create',
        'filling_sale_edit' => 'filling Sale Edit',
        'filling_sale_delete' => 'filling Sale Delete',
        'filling_report' => 'filling Report',
        'filling_purchase_challan' => 'filling purchase Challan',
        'filling_purchase_challan_create' => 'filling Purchase Challan Create',
        'filling_purchase_challan_delete' => 'filling Purchase Challan Delete',
        'filling_sale_challan' => 'filling Sale Challan',
        'filling_sale_challan_create' => 'filling Sale Challan Create',
        'filling_sale_challan_edit' => 'filling Sale Challan Edit',
        'filling_sale_challan_delete' => 'filling Sale Challan Delete',
        'filling_reset_order' => 'filling Reset Order',
        'filling_sales_order' => 'filling Sales Order',
        'filling_sales_order_delete' => 'filling Saler Order Delete',
    );
    return $access_items;
}

function default_user_access_items() {
    $access_items = array(
        'setting' => 'Setting',
        'generel_setting' => 'General Setting',
        'generel_setting_update' => 'General Setting Update',
    );
    return $access_items;
}

function institute_access_items() {
    $access_items = array(
        "permission_common" => "Common Permissions",
        "permission_hr" => "HR Management",
        "permission_user" => "User Management",
        "permission_accounts" => "Accounts Management",
        "permission_products" => "Product Management",
        "permission_main" => "Main Management",
        "permission_ricemill" => "Ricemill Management",
        "permission_trading" => "Trading Management",
        "permission_transport" => "Transport Management",
    );
    return $access_items;
}

function default_institute_access_items() {
    $access_items = array(
        'setting' => 'Setting',
        'generel_setting' => 'General Setting',
        'generel_setting_update' => 'General Setting Update',
        'manage_user' => 'Manage User',
    );
    return $access_items;
}

function allAccessItems($jsn = FALSE) {
    $items = [];
    $items[] = permission_accounts();
    $items[] = permission_bricks();
    $items[] = permission_transport();
    $items[] = permission_construction();
    $result = call_user_func_array("array_merge", $items);

    if ($jsn == TRUE) {
        $data = json_encode($result);
    } else {
        $data = $result;
    }

    return $data;
}

function has_user_access($access) {
    $id = Auth::id();
    $permissions = App\Models\UserPermission::where('user_id', $id)->first()->permissions;
    $access_items = json_decode($permissions, true);
    if (!empty($access_items)) {
        if (array_key_exists($access, $access_items) || isLegend() || isAdmin()) {
            return true;
        } else {
            return false;
        }
    } else {
        return false;
    }
}

function check_user_access($access) {
    if (has_user_access($access)) {
        return true;
    } else {
        return redirect()->back()->with('danger', 'You are not authorized to perform this action.')->send();
    }
}

function is_Admin() {
    if (Auth::user()->type == 'admin') {
        return true;
    } else {
        return false;
    }
}

function is_Ajax($request) {
    if ($request->ajax()) {
        return true;
    } else {
        return redirect()->back()->with('danger', 'Invalid URL')->send();
    }
}

function show_hide() {
    return is_Admin() ? "" : "hide_column";
}

function showFloatValue($val) {
    return number_format((float) $val, 2, '.', '');
}

function colspan($a, $b) {
    return is_Admin() ? $a : $b;
}

function institute_id() {
    return Auth::user()->institute_id;
}

function print_header($title, $_mpmt = true, $_visible = false, $additionals = [], $company_id = null) {
    $_sip = ($_visible == true) ? '' : 'show_in_print';
    $_mt = ($_mpmt == false) ? '' : 'mpmt';
    $companyId = !is_null($company_id) ? $company_id : Auth::user()->institute_id;
    $setting = \App\Models\GeneralSetting::get_setting($companyId);
    $str = "<div class='mb_10 invoice_heading {$_mt} {$_sip}'>";
    $str .= "<div class='text-center'>";
    $str .= "<h3 style='font-size:20px;margin:3px 0px;'>{$setting->title}</h3>";
    $str .= "<span>{$setting->address}</span>";
    $str .= "<span>" . trans('words.contact') . " : " . change_lang($setting->mobile) . "</span>";
    $str .= "<strong class='print_sub_title'>{$title}</strong>";
    if (!empty($additionals)) {
        foreach ($additionals as $akey => $aval) {
            $str .= "<br><strong style=''><u>{$akey}</u> :</strong> <span>{$aval}</span>";
        }
    }
    $str .= "</div>";
    $str .= "</div>";
    echo $str;
}

function print_head($title, $_mpmt = true, $_visible = false, $companyId = null) {
    $_sip = ($_visible == true) ? '' : 'show_in_print';
    $_mt = ($_mpmt == false) ? '' : 'mpmt';

    $setting = \App\Models\GeneralSetting::get_setting($companyId);
    $str = "<div class='{$_mt} {$_sip}'>";
    $str .= "<div class='text-left'>";
    $str .= "<h3 class='print_title' style='margin:0px;'>{$setting->title}</h3>";
    $str .= "<span><strong>" . trans('words.proprietor') . " :</strong> {$setting->owner}</span><br/>";
    $str .= "<span><strong>" . trans('words.address') . " :</strong> {$setting->address}</span><br/>";
    $str .= "<span><strong>" . trans('words.contact') . " :</strong> " . change_lang($setting->mobile) . "</span><br/>";
    $str .= "<span class='print_subtitle' style='display:block;text-decoration:underline;'>{$title}</span>";
    $str .= "</div>";
    $str .= "</div>";
    echo $str;
}

function number_dropdown($start, $end, $jump, $page_size = null, $xs_cls = null) {
    $setting = \App\Models\GeneralSetting::get_setting();
    $_page = $setting->pagesize;
    if (!is_null($page_size)) {
        $_page = $page_size;
    }
    $cls_xs = "";
    if (!is_null($xs_cls)) {
        $cls_xs = $xs_cls;
    }
    $_str = "<select class='form-control {$xs_cls}' id='itemCount' name='item_count' style='width:58px'>";

    for ($i = $start; $i <= $end; $i += $jump):
        if ($i == $_page) {
            $_str .= "<option value='{$i}' selected='selected'>{$i}</option>";
        } else {
            $_str .= "<option value='{$i}'>{$i}</option>";
        }
    endfor;

    $_str .= "</select>";
    return $_str;
}

function int_to_words($x) {
    $nwords = array("zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten", "eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen", "nineteen", "twenty", 30 => "thirty", 40 => "forty", 50 => "fifty", 60 => "sixty", 70 => "seventy", 80 => "eighty", 90 => "ninety");
    if (!is_numeric($x)) {
        $w = '#';
    } else if (fmod($x, 1) != 0) {
        $w = '#';
    } else {
        if ($x < 0) {
            $w = 'minus ';
            $x = -$x;
        } else {
            $w = '';
        }
        if ($x < 21) {
            $w .= $nwords[floor($x)];
        } else if ($x < 100) {
            $w .= $nwords[10 * floor($x / 10)];
            $r = fmod($x, 10);
            if ($r > 0) {
                $w .= '-' . $nwords[$r];
            }
        } else if ($x < 1000) {
            $w .= $nwords[floor($x / 100)] . ' hundred';
            $r = fmod($x, 100);
            if ($r > 0) {
                $w .= ' and ' . int_to_words($r);
            }
        } else if ($x < 100000) {
            $w .= int_to_words(floor($x / 1000)) . ' thousand';
            $r = fmod($x, 1000);
            if ($r > 0) {
                $w .= ' ';
                if ($r < 100) {
                    $w .= 'and ';
                }
                $w .= int_to_words($r);
            }
        } else {
            $w .= int_to_words(floor($x / 100000)) . ' lakh';
            $r = fmod($x, 100000);
            if ($r > 0) {
                $w .= ' ';
                if ($r < 100) {
                    $word .= 'and ';
                }
                $w .= int_to_words($r);
            }
        }
    }
    return $w;
}

function cur_date_time() {
    date_default_timezone_set("Asia/Dhaka");
    return date('Y-m-d H:i:s');
}

function cur_date() {
    date_default_timezone_set("Asia/Dhaka");
    return date('Y-m-d');
}

function print_date() {
    date_default_timezone_set("Asia/Dhaka");
    return date("d/m/Y g:i A", strtotime(date("Y-m-d H:i:s")));
}

function unit_list() {
    return [
        "Kg" => "Kg",
        "Liter" => "Liter",
        "Bag" => "Bag",
        "Cartoon" => "Cartoon",
        "Piece" => "Piece",
        "Packet" => "Packet",
        "Meter" => "Meter",
        "Foot" => "Foot",
        "Inch" => "Inch",
        "Drum" => "Drum",
        "Khaca" => "Khaca",
        "Tin" => "Tin",
        "Mon" => "Mon",
        "Ton" => "Ton",
        "SFT" => "SFT",
    ];
}

function unit_short_list() {
    $_list = [
        "m" => "Meter",
        "h" => "Hour",
        "ft" => "Foot",
        "day" => "Day",
        "cont" => "Contract",
        "rent" => "Rent",
        "kg" => "Kg",
        "gm" => "Gram",
        "l" => "Liter",
        "in" => "Inch"
    ];
    asort($_list);
    return $_list;
}

function type_list() {
    return [
        "Raw" => "Raw",
        "Finish" => "Finish",
        "Others" => "Others",
    ];
}

function country_list() {
    return [
        "india" => "India",
        "bhutan" => "Bhutan",
    ];
}

function bank_account_type_list() {
    return [
        "CC loan A/C" => "CC loan A/C",
        "Savings A/C" => "Savings A/C",
        "Current A/C" => "Current A/C",
        "LC A/C" => "LC A/C",
        "LTR A/C" => "LTR A/C",
        "Loan A/C (Monthly Installment)" => "Loan A/C (Monthly Installment)",
    ];
}

function printed_list() {
    return [
        "Graviour" => "Graviour",
        "Blocked" => "Blocked",
        "No" => "No",
    ];
}

function yes_no_list() {
    return [
        "Yes" => "Yes",
        "No" => "No",
    ];
}

function full_url() {
    return Request::url();
}

function companyPrefix($id) {
    $company = App\Models\Institute::find($id);
    return strtolower($company->short_name);
}

function company_route_name($id) {
    $company = App\Models\Institute::find($id);
    return strtolower($company->route_name);
}

function route_prefixes_old() {
    return $_route_prefix = ['equipment', 'inv', 'rice', 'transport', 'cons', 'trade'];
}

function route_prefixes() {
    $companies = App\Models\Institute::get();
    $_route_prefix = [];
    foreach ($companies as $val) {
        $_route_prefix[] = strtolower($val->route_name);
    }
    $_extra_prefix = ['equipment'];
    $_all_prefix = array_merge($_route_prefix, $_extra_prefix);
    return $_all_prefix;
}

function body_class() {
    $_fullUri = full_url();
    $_uriArr = explode('/', $_fullUri);

    $_clsName = "column1";
    $_route_prefix = route_prefixes();
    foreach ($_route_prefix as $_rpkey => $_rpval) {
        if (array_search($_rpval, $_uriArr)) {
            $_clsName = "column2";
        }
    }

    return $_clsName;
}

function en2bnNumber($number) {
    $bn = array("১", "২", "৩", "৪", "৫", "৬", "৭", "৮", "৯", "০");
    $en = array("1", "2", "3", "4", "5", "6", "7", "8", "9", "0");
    $bn_number = str_replace($en, $bn, $number);
    return $bn_number;
}

function en2bnDate($date) {
    $bn = array('১', '২', '৩', '৪', '৫', '৬', '৭', '৮', '৯', '০', 'জানুয়ারী', 'ফেব্রুয়ারী', 'মার্চ', 'এপ্রিল', 'মে', 'জুন', 'জুলাই', 'আগস্ট', 'সেপ্টেম্বর', 'অক্টোবর', 'নভেম্বর', 'ডিসেম্বর', 'শনিবার', 'রবিবার', 'সোমবার', 'মঙ্গলবার', '
        বুধবার', 'বৃহস্পতিবার', 'শুক্রবার'
    );
    $en = array('1', '2', '3', '4', '5', '6', '7', '8', '9', '0', 'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December', 'Saturday', 'Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday');
    $bn_date = str_replace($en, $bn, $date);
    return $bn_date;
}

function change_lang($str) {
    $_lang = App::getLocale();
    $en = array('0', '1', '2', '3', '4', '5', '6', '7', '8', '9');
    $bn = array('o', '১', '২', '৩', '৪', '৫', '৬', '৭', '৮', '৯');
    if ($_lang == 'bn') {
        $tstr = str_replace($en, $bn, $str);
    } else {
        $tstr = str_replace($bn, $en, $str);
    }
    return $tstr;
}

function to_eng($str) {
    $bn = array('o', '১', '২', '৩', '৪', '৫', '৬', '৭', '৮', '৯');
    $en = array('0', '1', '2', '3', '4', '5', '6', '7', '8', '9');
    $tstr = str_replace($bn, $en, $str);
    return $tstr;
}

function printf_number($value) {
    return sprintf("%'.08d\n", $value);
}

function kgToMon($kg) {
    $mon = $kg / 40;
    return $mon;
}

function kgToMonKg($weight) {
    $result = '';
    if ($weight < 40) {
        $result = 0;
    } else {
        $mon = round($weight / 40);
        $kg = $weight % 40;
        $result = $mon . "." . $kg;
    }

    return $result;
}

function MonToKg($mon) {
    $kg = $mon * 40;
    return $kg;
}

function MonToKgPrice($mon_price) {
    $kg_price = $mon_price / 40;
    return $kg_price;
}

function kgToSher($kg) {
    $sher = $kg % 40;
    return $sher;
}

function get_inst_id_from_arr($id) {
    $institute_arr = [
        86 => 13,
        87 => 15,
        88 => 14,
        89 => 18,
        90 => 17,
        91 => 16,
        92 => 12,
        93 => 1,
        94 => 1,
        95 => 1,
        97 => 20,
    ];
    return $institute_arr[$id];
}

// Method for Adding Multiple Time

function addMultipleTime($time) {
    $seconds = 0;
    foreach ($time as $t) {
        $timeArr = array_reverse(explode(":", $t));

        foreach ($timeArr as $key => $value) {
            if ($key > 2)
                break;
            $seconds += pow(60, $key) * $value;
        }
    }

    $hours = floor($seconds / 3600);
    $mins = floor(($seconds - ($hours * 3600)) / 60);
    $secs = floor($seconds % 60);

    return $hours . ':' . $mins . ':' . $secs;
}

function kgToTon($kg) {
    $ton = ($kg / 1000);
    return $ton;
}

function tonToKg($ton) {
    $kg = ($ton * 1000);
    return $kg;
}

// month dropdown with integer value
function months() {
    return [
        1 => 'January',
        2 => 'February',
        3 => 'March',
        4 => 'April',
        5 => 'May',
        6 => 'June',
        7 => 'July',
        8 => 'August',
        9 => 'September',
        10 => 'October',
        11 => 'November',
        12 => 'December'
    ];
}

function get_month_value($id) {
    $months = months();
    return $months[$id];
}

function years_dropdown($_id = null, $_name = null, $_end = null, $_sval = null) {
    $_curYear = date('Y');
    $_yearToRun = !is_null($_end) ? $_end : 2015;

    $_str = "<select class='form-control' ";
    $_str .= !is_null($_id) ? " id='{$_id}' " : " id='year' ";
    $_str .= !is_null($_name) ? " name='{$_name}' " : " name='year' ";
    $_str .= ">";

    for ($i = $_curYear; $i >= $_yearToRun; $i--):
        $_active = '';
        if (!is_null($_sval) && ($_sval == $i)) {
            $_active = ' selected';
        }
        $_str .= "<option value='{$i}' {$_active}>{$i}</option>";
    endfor;

    $_str .= "</select>";
    return $_str;
}

function years_val($_end = null) {
    $_curYear = date('Y');
    $_yearToRun = !is_null($_end) ? $_end : 2015;
    $_retval = [];

    for ($i = $_curYear; $i >= $_yearToRun; $i--):
        $_retval[] = $i;
    endfor;

    return $_retval;
}

function limit_text_length($text, $len) {
    if (strlen($text) < $len) {
        return $text;
    }
    $text_words = explode(' ', $text);
    $out = null;

    foreach ($text_words as $word) {
        if ((strlen($word) > $len) && $out == null) {
            return substr($word, 0, $len) . "...";
        }
        if ((strlen($out) + strlen($word)) > $len) {
            return $out . "...";
        }
        $out .= " " . $word;
    }
    return $out;
}

function isAuthor($authorId) {
    if ($authorId == Auth::id() || is_Admin()) {
        return true;
    } else {
        return false;
    }
}

function onlyAuthor($authorId) {
    if ($authorId == Auth::id()) {
        return true;
    } else {
        return false;
    }
}

function isDeveloper() {
    if (isLegend()) {
        return true;
    } else {
        return false;
    }
}

function random_number($length) {
    $chars = array_merge(range(0, 9), range(9, 0));
    shuffle($chars);
    $_number = implode(array_slice($chars, 0, $length));
    return $_number;
}

function permission_list() {
    return [
        "permission_common" => "Common Permissions",
        "permission_hr" => "HR Management",
        "permission_user" => "User Management",
        "permission_accounts" => "Accounts Management",
        "permission_products" => "Product Management",
        "permission_equipment" => "Equipment Management",
        "permission_main" => "Main Management",
        "permission_ricemill" => "Ricemill Management",
        "permission_trading" => "Trading Management",
        "permission_transport" => "Transport Management",
    ];
}

function permission_common() {
    return array(
        'generel_setting' => 'General Setting',
        'generel_setting_update' => 'General Setting Update',
        'process_order' => 'Process Order',
        'reset_order' => 'Reset Order',
        'approve_order' => 'Approve Order',
    );
}

function permission_hr() {
    return [
        "designation_list" => "Designation List",
        "designation_create" => "Designation Create",
        "designation_edit" => "Designation Edit",
        "designation_delete" => "Designation Delete",
        "employee_list" => "Employee List",
        "employee_create" => "Employee Create",
        "employee_edit" => "Employee Edit",
        "employee_delete" => "Employee Delete",
        "employee_detail" => "Employee Detail",
        "employee_ledger" => "Employee Ledger",
        "salary_list" => "Salary List",
        "salary_create" => "Salary Create",
        "salary_edit" => "Salary Edit",
        "salary_delete" => "Salary Delete",
        "salary_items" => "Salary Ledger",
        "salary_items_edit" => "Salary Ledger Edit",
        "salary_items_delete" => "Salary Ledger Delete",
    ];
}

function permission_user() {
    return array(
        'user_list' => 'User List',
        'user_create' => 'User Create',
        'user_edit' => 'User Edit',
        'user_delete' => 'User Delete',
        'user_activate_deactivate' => 'User Activate/De-activate',
        'user_access_control' => 'User Access Control',
        'user_password_change' => 'User Password Change',
    );
}

function permission_accounts() {
    return array(
        'head_list' => 'Head List',
        'head_create' => 'Head Create',
        'head_edit' => 'Head Edit',
        'head_delete' => 'Head Delete',
        'subhead_list' => 'Subhead List',
        'subhead_create' => 'Subhead Create',
        'subhead_edit' => 'Subhead Edit',
        'subhead_delete' => 'Subhead Delete',
        'particular_list' => 'Particular List',
        'particular_create' => 'Particular Create',
        'particular_edit' => 'Particular Edit',
        'particular_delete' => 'Particular Delete',
        'journal_list' => 'Journal List',
        'journal_create' => 'Journal List',
        'journal_edit' => 'Journal List',
        'journal_delete' => 'Journal List',
        'journal_process' => 'Journal Process',
        'journal_details' => 'Journal Details',
        'transaction_list' => 'Transaction List',
        'transaction_create' => 'Transaction Create',
        'transaction_edit' => 'Transaction Edit',
        'transaction_delete' => 'Transaction Delete',
        'check_register_list' => 'Check Register List',
        'check_register_create' => 'Check Register Create',
        'check_register_edit' => 'Check Register Edit',
        'check_register_delete' => 'Check Register Delete',
        'daily_sheet' => 'Daily Sheet',
        'daily_report' => 'Daily Report',
        'summary_report' => 'Summary Report',
        'ledger_report' => 'Ledger Report',
    );
}

function permission_products() {
    return array(
        'product_type_list' => 'Product Type List',
        'product_type_create' => 'Product Type Create',
        'product_type_edit' => 'Product Type Edit',
        'product_type_delete' => 'Product Type Delete',
        'category_list' => 'Category List',
        'category_create' => 'Category Create',
        'category_edit' => 'Category Edit',
        'category_delete' => 'Category Delete',
        'product_list' => 'Product List',
        'product_create' => 'Product Create',
        'product_edit' => 'Product Edit',
        'product_delete' => 'Product Delete',
        'product_stock' => 'Product Stock',
        'product_ledger' => 'Product Ledger',
        'size_list' => 'Product Size List',
        'size_create' => 'Product Size Create',
        'size_edit' => 'Product Size Edit',
        'size_delete' => 'Product Size Delete',
        'product_unit_list' => 'Product Unit List',
        'product_unit_create' => 'Product Unit Create',
        'product_unit_edit' => 'Product Unit Edit',
        'product_unit_delete' => 'Product Unit Delete',
        'price_unit_list' => 'Price Unit List',
        'price_unit_create' => 'Price Unit Create',
        'price_unit_edit' => 'Price Unit Edit',
        'price_unit_delete' => 'Price Unit Delete',
    );
}

function permission_equipment() {
    return array(
        'equipment_purchase_list' => 'Purchase List',
        'equipment_purchase_create' => 'Purchase Create',
        'equipment_purchase_edit' => 'Purchase Edit',
        'equipment_purchase_delete' => 'Purchase Delete',
        'equipment_purchase_item_list' => 'Purchase Item List',
        'equipment_purchase_process' => 'Purchase Process',
        'equipment_purchase_reset' => 'Purchase Reset',
        'equipment_delivery_list' => 'delivery List',
        'equipment_delivery_create' => 'delivery Create',
        'equipment_delivery_edit' => 'delivery Edit',
        'equipment_delivery_delete' => 'delivery Delete',
        'equipment_delivery_item_list' => 'delivery Item List',
        'equipment_delivery_process' => 'delivery Process',
        'equipment_delivery_reset' => 'delivery Reset',
    );
}

function permission_main() {
    return array(
        'main_purchase' => 'Purchase',
        'main_purchase_create' => 'Purchase Create',
        'main_purchase_edit' => 'Purchase Edit',
        'main_purchase_delete' => 'Purchase Delete',
        'main_sales' => 'Sales',
        'main_sales_create' => 'Sales Create',
        'main_sales_edit' => 'Sales Edit',
        'main_sales_delete' => 'Sales Delete',
        'main_purchase_return' => 'Purchase Return',
        'main_purchase_return_create' => 'Purchase Return Create',
        'main_purchase_return_edit' => 'Purchase Return Edit',
        'main_purchase_return_delete' => 'Purchase Return Delete',
        'main_sales_return' => 'Sales Return',
        'main_sales_return_create' => 'Sales Return Create',
        'main_sales_return_edit' => 'Sales Return Edit',
        'main_sales_return_delete' => 'Sales Return Delete',
        'main_stocks' => 'Stocks',
    );
}

function permission_brick() {
    return array(
        'bricks_chamber_list' => 'Chamber List',
        'bricks_chamber_create' => 'Chamber Create',
        'bricks_chamber_edit' => 'Chamber Edit',
        'bricks_chamber_delete' => 'Chamber Delete',
        'bricks_mill_list' => 'Mill List',
        'bricks_mill_create' => 'Mill Create',
        'bricks_mill_edit' => 'Mill Edit',
        'bricks_mill_delete' => 'Mill Delete',
        'bricks_purchase_list' => 'Purchase List',
        'bricks_purchase_item_list' => 'Purchase Item List',
        'bricks_purchase_create' => 'Purchase Create',
        'bricks_purchase_edit' => 'Purchase Edit',
        'bricks_purchase_delete' => 'Purchase Delete',
        'bricks_sales_list' => 'Sales List',
        'bricks_sales_item_list' => 'Sales Item List',
        'bricks_sales_create' => 'Sales Create',
        'bricks_sales_edit' => 'Sales Edit',
        'bricks_sales_delete' => 'Sales Delete',
        'bricks_delivery_list' => 'Delivery List',
        'bricks_delivery_item_list' => 'Delivery Item List',
        'bricks_delivery_create' => 'Delivery Create',
        'bricks_delivery_edit' => 'Delivery Edit',
        'bricks_delivery_delete' => 'Delivery Delete',
        'bricks_production_list' => 'Production List',
        'bricks_production_create' => 'Production Create',
        'bricks_production_edit' => 'Production Edit',
        'bricks_production_delete' => 'Production Delete',
        'bricks_loading_list' => 'Loading List',
        'bricks_loading_create' => 'Loading Create',
        'bricks_loading_edit' => 'Loading Edit',
        'bricks_loading_delete' => 'Loading Delete',
        'bricks_loading_process' => 'Loading Process',
        'bricks_unloading_list' => 'Unloading List',
        'bricks_unloading_create' => 'Unloading Create',
        'bricks_unloading_edit' => 'Unloading Edit',
        'bricks_unloading_delete' => 'Unloading Delete',
        'bricks_unloading_process' => 'Unloading Process',
        'bricks_stocks' => 'Stocks',
        'bricks_report' => 'Report',
    );
}

function permission_construction() {
    return array(
        'cons_department_setting' => 'Department List',
        'cons_department_create' => 'Department Create',
        'cons_department_edit' => 'Department Edit',
        'cons_department_delete' => 'Department Delete',
        'cons_partnership_setting' => 'Partnership List',
        'cons_partnership_create' => 'Partnership Create',
        'cons_partnership_edit' => 'Partnership Edit',
        'cons_partnership_delete' => 'Partnership Delete',
        'cons_contract_person_setting' => 'Contract Person List',
        'cons_contract_person_create' => 'Contract Person Create',
        'cons_contract_person_edit' => 'Contract Person Edit',
        'cons_contract_person_delete' => 'Contract Person Delete',
        'cons_contract_setting' => 'Contract List',
        'cons_contract_create' => 'Contract Create',
        'cons_contract_edit' => 'Contract Edit',
        'cons_contract_delete' => 'Contract Delete',
        'cons_project_type_setting' => 'Project Type',
        'cons_project_type_create' => 'Project Type Create',
        'cons_project_type_edit' => 'Project Type Edit',
        'cons_project_type_delete' => 'Project Type Delete',
        'cons_license_list' => 'License List',
        'cons_license_create' => 'License create',
        'cons_license_edit' => 'License Edit',
        'cons_license_delete' => 'License Delete',
        'cons_project_list' => 'Project List',
        'cons_project_create' => 'Project Create',
        'cons_project_edit' => 'Project Edit',
        'cons_project_delete' => 'Project Delete',
        'cons_report' => 'Report',
    );
}

function permission_ricemill() {
    return array(
        'ricemill_diarparty_service_charge_setting_list' => 'Service Charge List',
        'ricemill_diarparty_service_charge_setting_create' => 'Service Charge Create',
        'ricemill_diarparty_service_charge_setting_edit' => 'Service Charge Edit',
        'ricemill_diarparty_service_charge_setting_delete' => 'Service Charge Delete',
        'ricemill_price_setting' => 'Price Setting',
        'ricemill_price_setting_list' => 'Price Setting List',
        'ricemill_price_setting_create' => 'Price Create',
        'ricemill_price_setting_edit' => 'Price Edit',
        'ricemill_price_setting_delete' => 'Price Delete',
        'ricemill_emptybag_manage' => 'Manage Empty Bag',
        'ricemill_emptybag_purchase' => 'Emptybag Purchase',
        'ricemill_emptybag_purchase_list' => 'Emptybag Purchase List',
        'ricemill_emptybag_purchase_create' => 'Emptybag Purchase Create',
        'ricemill_emptybag_purchase_edit' => 'Emptybag Purchase Edit',
        'ricemill_emptybag_purchase_item_edit' => 'Emptybag Purchase Item Edit',
        'ricemill_emptybag_purchase_delete' => 'Emptybag Purchase Delete',
        'ricemill_emptybag_purchase_process' => 'Emptybag Purchase Process',
        'ricemill_emptybag_purchase_reset' => 'Emptybag Purchase Reset',
        'ricemill_emptybag_purchase_items' => 'Emptybag Purchase Items List',
        'ricemill_emptybag_purchase_edit_items' => 'Emptybag Purchase Edit Items',
        'ricemill_emptybag_purchase_gatepass' => 'Emptybag Purchase Gatepass',
        'ricemill_emptybag_purchase_details' => 'Emptybag Purchase Details',
        'ricemill_emptybag_sales_list' => 'Emptybag Sales List',
        'ricemill_emptybag_sales_create' => 'Emptybag Sales Create',
        'ricemill_emptybag_sales_edit' => 'Emptybag Sales Edit',
        'ricemill_emptybag_sales_update' => 'Emptybag Sales Update',
        'ricemill_emptybag_sales_delete' => 'Emptybag Sales Delete',
        'ricemill_emptybag_sales_process' => 'Emptybag Sales Process',
        'ricemill_emptybag_sales_reset' => 'Emptybag Sales Reset',
        'ricemill_emptybag_sales_items' => 'Emptybag Sales Items',
        'ricemill_emptybag_sales_items_list' => 'Emptybag Sales Items List',
        'ricemill_emptybag_sales_edit_items' => 'Emptybag  Sales Edit Items',
        'ricemill_emptybag_sales_gatepass' => 'Emptybag Sales Gatepass',
        'ricemill_emptybag_sales_details' => 'Emptybag Sales Details',
        'ricemill_emptybag_stock' => 'Emptybag Stock',
        'ricemill_emptybag_opening_stock' => 'Emptybag Opening Stock',
        'ricemill_emptybag_opening_stock_create' => 'Emptybag Opening Stock Create',
        'ricemill_emptybag_opening_stock_edit' => 'Emptybag Opening Stock Edit',
        'ricemill_emptybag_opening_stock_delete' => 'Emptybag Opening Stock Delete',
        'ricemill_emptybag_opening_stock_list' => 'Emptybag Opening Stock List',
        'ricemill_machinaries_use_list' => 'Emptybag use List',
        'ricemill_machinaries_use_create' => 'Emptybag use Create',
        'ricemill_machinaries_use_edit' => 'Emptybag use  Edit',
        'ricemill_machinaries_use_delete' => 'Emptybag use Delete',
        'ricemill_machinaries_use_view' => 'Emptybag use View',
        'ricemill_machinaries_use_gatepass' => 'Emptybag use Gatepass',
        'ricemill_machinaries_use_process' => 'Emptybag use Process',
        'ricemill_machinaries_use_reset' => 'Emptybag use Reset',
        'ricemill_machinaries_use_items' => 'Emptybag use items',
        'ricemill_machinaries_use_item_list' => 'Emptybag use items List',
        'ricemill_manage_purchase' => 'Manage Purchase',
        'ricemill_purchase' => 'Purchase',
        'ricemill_purchase_list' => 'Purchase List',
        'ricemill_purchase_create' => 'Purchase Create',
        'ricemill_purchase_details' => 'Purchase Bag Details',
        'ricemill_purchase_gatepass' => 'Purchase Bag Gatepass',
        'ricemill_purchase_process' => 'Purchase Process',
        'ricemill_purchase_reset' => 'Purchase Reset',
        'ricemill_purchase_edit' => 'Purchase Edit',
        'ricemill_purchase_items' => 'Purchase Items',
        'ricemill_purchase_delete' => 'Delelte Purchase',
        'ricemill_purchase_edit_items' => 'Purchase items Edit',
        'ricemill_purchase_items' => 'Purchase items',
        'ricemill_purchase_items_list' => 'Purchase items List',
        'ricemill_sales_manage' => 'Manage Sales',
        'ricemill_sales_list' => 'Sales List',
        'ricemill_sales_create' => 'Sales Create',
        'ricemill_sales_edit' => 'Edit Sales',
        'ricemill_sales_delete' => 'Delete Sales',
        'ricemill_sales_details' => 'Sales Details',
        'ricemill_sales_process' => 'Process Sales',
        'ricemill_sales_reset' => 'Reset Sales',
        'ricemill_sales_items' => 'Items Sales',
        'ricemill_sales_items_list' => 'Sales Items List',
        'ricemill_sales_edit_items' => 'Sales Items Edit',
        'ricemill_sales_gatepass' => 'Sales Gatepass',
        'ricemill_delivery_manage' => 'Manage Delivery',
        'ricemill_delivery_list' => 'Delivery List',
        'ricemill_delivery_create' => 'Create Delivery',
        'ricemill_delivery_details' => 'Delivery Details',
        'ricemill_delivery_edit' => 'Edit Delivery',
        'ricemill_delivery_update' => 'Update Delivery',
        'ricemill_delivery_delete' => 'Delete Delivery',
        'ricemill_delivery_process' => 'Process Delivery',
        'ricemill_delivery_reset' => 'Reset Delivery',
        'ricemill_delivery_items' => 'Items Delivery',
        'ricemill_delivery_items_list' => 'Delivery Items List',
        'ricemill_delivery_edit_items' => 'Delivery Items Edit',
        'ricemill_delivery_gatepass' => 'Delivery Gatepass',
        'ricemill_productions_manage' => 'Manage Productions',
        'ricemill_productions_type_list' => 'Productions Type List',
        'ricemill_productions_type_create' => 'Productions Type Create',
        'ricemill_productions_type_edit' => 'Productions Type Edit',
        'ricemill_productions_type_delete' => 'Productions Type Delete',
        'ricemill_productions_order' => 'Productions Order',
        'ricemill_productions_list' => 'Productions List',
        'ricemill_productions_create' => 'Productions Create',
        'ricemill_productions_edit' => 'Productions Edit',
        'ricemill_productions_delete' => 'Productions Delete',
        'ricemill_productions_details' => 'Productions Details',
        'ricemill_productions_process' => 'Productions Process',
        'ricemill_productions_reset' => 'Reset Productions',
        'ricemill_productions_cost_analysis' => 'Production Cost Analysis',
        'ricemill_productions_stock_done' => 'Productions Stock Done',
        'ricemill_productions_items' => 'Items Productions',
        'ricemill_productions_details' => 'Productions View',
        'ricemill_productions_order_list' => 'Productions Item List',
        'ricemill_finish_product' => 'Finish Product',
        'ricemill_finish_product_list' => 'Finish Product List',
        'ricemill_finish_product_create' => 'Finish Product Create',
        'ricemill_finish_product_edit' => 'Finish Product Edit',
        'ricemill_finish_product_delete' => 'Finish Product Delete',
        'ricemill_finish_product_process' => 'Finish Product Process',
        'ricemill_finish_product_reset' => 'Reset Finish Product',
        'ricemill_finish_product_details' => 'Finish Product Details',
        'ricemill_finish_product_items' => ' Finish Product Items',
        'ricemill_finish_product_items_list' => 'Finish Product Item List',
        'ricemill_finish_product_opening_stock' => 'Finish Product Opening Stock',
        'ricemill_finish_product_opening_stock_list' => 'Finish Product Opening Stock List',
        'milling_charge_setting_list' => 'Milling Charge List',
        'milling_charge_setting_create' => 'Milling Charge Create',
        'milling_charge_setting_edit' => 'Milling Charge Edit',
        'milling_charge_setting_delete' => 'Milling Charge Delete',
        "portta_calculation_list" => "Production Cost Calculation",
        "ricemill_daily_laborbill_setting" => "Daily Labor Bill Setting",
        'ricemill_product_transfer_list' => 'Product Transfer List',
        'ricemill_product_transfer_create' => 'Product Transfer Create',
        'ricemill_product_transfer_edit' => 'Product Transfer Edit',
        'ricemill_product_transfer_delete' => 'Product Transfer Delete',
        'ricemill_product_transfer_process' => 'Product Transfer Process',
        'ricemill_product_transfer_reset' => 'Product Transfer Reset',
        'ricemill_product_transfer_details' => 'Product Transfer Details',
        'ricemill_diarparty_manage' => 'Diarparty Manage',
        'ricemill_diarparty_paddy_receive' => 'Diarparty Paddy Receive',
        'ricemill_diarparty_paddy_receive_list' => 'Diarparty Paddy Receive List',
        'ricemill_diarparty_paddy_receive_create' => 'Diarparty Paddy Receive Create',
        'ricemill_diarparty_paddy_receive_edit' => 'Diarparty Paddy Receive Edit',
        'ricemill_diarparty_paddy_receive_delete' => 'Diarparty Paddy Receive Delete',
        'ricemill_diarparty_paddy_receive_details' => 'Diarparty Paddy Receive Details',
        'ricemill_diarparty_paddy_receive_gatepass' => 'Diarparty Paddy Receive gatepass',
        'ricemill_diarparty_paddy_receive_items' => 'Diarparty Paddy Receive Items',
        'ricemill_diarparty_paddy_receive_items_list' => 'Diarparty Paddy Receive Items List',
        'ricemill_diarparty_paddy_receive_process' => 'Diarparty Paddy Receive Process',
        'ricemill_diarparty_paddy_receive_reset' => 'Diarparty Paddy Receive Reset',
        'ricemill_diarparty_crassing' => 'Diarparty Crassing',
        'ricemill_diarparty_crassing_list' => 'Diarparty Crassing List',
        'ricemill_diarparty_crassing_create' => 'Diarparty Crassing  Create',
        'ricemill_diarparty_crassing_edit' => 'Diarparty Crassing  Edit',
        'ricemill_diarparty_crassing_delete' => 'Diarparty Crassing  Delete',
        'ricemill_diarparty_crassing_details' => 'Diarparty Crassing  Details',
        'ricemill_diarparty_crassing_items' => 'Diarparty Crassing  Items',
        'ricemill_diarparty_crassing_process' => 'Diarparty Crassing  Process',
        'ricemill_diarparty_crassing_reset' => 'Diarparty Crassing  Reset',
        'ricemill_diarparty_crassing_items_list' => 'Diarparty Crassing Items List',
        'ricemill_diarparty_crassing_stock_done' => 'Diarparty Crassing Stock Done',
        'ricemill_diarparty_finish_list' => 'Diarparty Finish List',
        'ricemill_diarparty_finish_create' => 'Diarparty Finish  Create',
        'ricemill_diarparty_finish_edit' => 'Diarparty Finish  Edit',
        'ricemill_diarparty_finish_delete' => 'Diarparty Finish  Delete',
        'ricemill_diarparty_finish_items' => 'Diarparty Finish  Items',
        'ricemill_diarparty_finish_items_list' => 'Diarparty Finish  Items List',
        'ricemill_diarparty_finish_process' => 'Diarparty Finish  Process',
        'ricemill_diarparty_finish_reset' => 'Diarparty Finish  Reset',
        'ricemill_diarparty_finish_delivery' => 'Diarparty Finish  Delivery',
        'ricemill_diarparty_finish_details' => 'Diarparty Finish  Details',
        'ricemill_diarparty_riceprodive' => 'Diarparty Rice Provide',
        'ricemill_diarparty_riceprodive_list' => 'Diarparty Rice Provide List',
        'ricemill_diarparty_riceprodive_create' => 'Diarparty Rice Provide  Create',
        'ricemill_diarparty_riceprodive_edit' => 'Diarparty Rice Provide  Edit',
        'ricemill_diarparty_riceprodive_delete' => 'Diarparty Rice Provide  Delete',
        'ricemill_diarparty_riceprodive_details' => 'Diarparty Rice Provide  Details',
        'ricemill_diarparty_riceprodive_gatepass' => 'Diarparty Rice Provide  Gatepass',
        'ricemill_diarparty_riceprodive_items' => 'Diarparty Rice Provide Items',
        'ricemill_diarparty_riceprodive_items_edit' => 'Diarparty Rice Provide Edit Items',
        'ricemill_diarparty_riceprodive_items_list' => 'Diarparty Rice Provide  Items List',
        'ricemill_diarparty_riceprodive_process' => 'Diarparty Rice Provide  Process',
        'ricemill_diarparty_riceprodive_reset' => 'Diarparty Rice Provide  Reset',
        'ricemill_diarparty_stock' => 'Diarparty Stock',
        'ricemill_diarparty_stock_list' => 'Diarparty Stock List',
        'ricemill_diarparty_stock_opening' => 'Diarparty Stock  Opening',
        'ricemill_diarparty_stock_create' => 'Diarparty Stock  Create',
        'ricemill_diarparty_stock_edit' => 'Diarparty Stock  Edit',
        'ricemill_diarparty_stock_delete' => 'Diarparty Stock  Delete',
        'ricemill_diarparty_stock_items' => 'Diarparty Stock  Items',
        'ricemill_diarparty_stock_details' => 'Diarparty Stock  Details',
        'ricemill_diarparty_stock_items_list' => 'Diarparty Stock  Items List',
        'ricemill_diarparty_stock_process' => 'Diarparty Stock  Process',
        'ricemill_diarparty_stock_reset' => 'Diarparty Stock  Reset',
        'ricemill_diarparty_stock_register' => 'Diarparty Stock Register',
        'ricemill_diarparty_dailty_report' => 'Diarparty Daily Report',
        'ricemill_diarparty_product_register' => 'Diarparty Product Register',
        'ricemill_raw_stock_manage' => 'Raw Stock Manage',
        'ricemill_raw_stock_' => 'Raw Stock',
        'ricemill_raw_opening_stock' => 'Raw Opening Stock',
        'ricemill_raw_opening_stock_edit' => 'Raw Opening Stock Edit',
        'ricemill_raw_opening_stock_delete' => 'Raw Opening Stock delete',
        'ricemill_raw_opening_stock_list' => 'Raw Opening Stock List',
        'ricemill_finish_stock_manage' => 'Finish Stock Manage',
        'ricemill_raw_stock_list' => 'Raw Stock List',
        'ricemill_finish_stock' => 'Finish Stock',
        'ricemill_finish_opening_stock' => 'Finish Opening Stock',
        'ricemill_finish_opening_stock_list' => 'Finish Opening Stock List',
        'ricemill_manage_report' => 'Manage Report',
        'ricemill_dailty_report' => 'Daily Report',
        'ricemill_stock_register' => 'Stock Register',
        'ricemill_product_register' => 'Products Register',
        'ricemill_profitloss' => 'Profit Loss',
        'ricemill_party_leager' => 'Party Leager',
        'ricemill_emptybag_opening_stock_edit' => 'Emptybag Opening Edit',
        'ricemill_emptybag_opening_stock_delete' => 'Emptybag Opening Delete',
        "production_costing_list" => "Production Costing List",
        "production_costing_create" => "Production Costing Create",
        "production_costing_edit" => "Production Costing Edit",
        "production_costing_delete" => "Production Costing Delete",
        "production_costing_details" => "Production Costing Details",
        "production_product_costing_list" => "Production Product Costing List",
        "production_product_costing_create" => "Production Product Costing Create",
        "production_product_costing_edit" => "Production Product Costing Edit",
        "production_product_costing_delete" => "Production Product Costing Delete",
        "production_product_costing_details" => "Production Product Costing Details",
    );
}

function permission_flourmill() {
    return array(
        'flourmill_price_setting' => 'Price Setting',
        'flourmill_price_setting_list' => 'Price Setting List',
        'flourmill_price_setting_create' => 'Price Create',
        'flourmill_price_setting_edit' => 'Price Edit',
        'flourmill_price_setting_delete' => 'Price Delete',
        'flourmill_emptybag_manage' => 'Manage Empty Bag',
        'flourmill_emptybag_purchase' => 'Empty Bag Purchase',
        'flourmill_emptybag_purchase_list' => 'Empty Bag Purchase List',
        'flourmill_emptybag_purchase_create' => 'Empty Bag Purchase Create',
        'flourmill_emptybag_purchase_edit' => 'Empty Bag Purchase Edit',
        'flourmill_emptybag_purchase_item_edit' => 'Empty Bag Purchase Item Edit',
        'flourmill_emptybag_purchase_update' => 'Empty Bag Purchase Update',
        'flourmill_emptybag_purchase_delete' => 'Empty Bag Purchase Delete',
        'flourmill_emptybag_purchase_process' => 'Empty Bag Purchase Process',
        'flourmill_emptybag_purchase_reset' => 'Empty Bag Purchase Reset',
        'flourmill_emptybag_purchase_item_list' => 'Empty Bag Purchase Items List',
        'flourmill_emptybag_purchase_items' => 'Empty Bag Purchase Items',
        'flourmill_emptybag_purchase_edit_items' => 'Empty Bag Purchase Edit Items',
        'flourmill_emptybag_purchase_gatepass' => 'Empty Bag Purchase Gatepass',
        'flourmill_emptybag_purchase_details' => 'Empty Bag Purchase Details',
        'flourmill_emptybag_sales_list' => 'Empty Sales Bag List',
        'flourmill_emptybag_sales_create' => 'Empty Bag Sales Create',
        'flourmill_emptybag_sales_edit' => 'Empty Bag Sales Edit',
        'flourmill_emptybag_sales_update' => 'Empty Bag Sales Update',
        'flourmill_emptybag_sales_delete' => 'Empty Bag Sales Delete',
        'flourmill_emptybag_sales_process' => 'Empty Bag Sales Process',
        'flourmill_emptybag_sales_reset' => 'Empty Bag Sales Reset',
        'flourmill_emptybag_sales_items' => 'Empty Bag Sales Items',
        'flourmill_emptybag_sales_items_list' => ' Empty Bag Items List',
        'flourmill_emptybag_sales_edit_items' => 'Empty Bag Edit Items',
        'flourmill_emptybag_sales_gatepass' => 'Empty Bag Gatepass',
        'flourmill_emptybag_sales_details' => 'Empty Bag Details',
        'flourmill_emptybag_stock' => 'Empty Bag Stock',
        'flourmill_emptybag_opening_stock' => 'Empty Bag Opening Stock',
        'flourmill_emptybag_opening_stock_create' => 'Opening Stock Create',
        'flourmill_emptybag_opening_stock_edit' => 'Opening Stock Edit',
        'flourmill_emptybag_opening_stock_delete' => 'Opening Stock Delete',
        'flourmill_emptybag_opening_stock_list' => 'Empty Bag Opening Stock List',
        'flourmill_manage_purchase' => 'Manage Purchase',
        'flourmill_purchase' => 'Purchase',
        'flourmill_purchase_list' => 'Purchase List',
        'flourmill_purchase_create' => 'Create Purchase',
        'flourmill_purchase_edit' => 'Edit Purchase',
        'flourmill_purchase_update' => 'Update Purchase',
        'flourmill_purchase_delete' => 'Delelte Purchase',
        'flourmill_purchase_items' => 'Purchase Items',
        'flourmill_purchase_process' => 'Purchase Process',
        'flourmill_purchase_reset' => 'Purchase Reset',
        'flourmill_purchase_edit_items' => 'Purchase Edit items',
        'flourmill_purchase_items' => 'Purchase items',
        'flourmill_purchase_items_list' => 'Purchase items List',
        'flourmill_purchase_gatepass' => 'Purchase Bag Gatepass',
        'flourmill_purchase_details' => 'Purchase Bag Details',
        'flourmill_sales_manage' => 'Manage Sales',
        'flourmill_sales_list' => 'Sales List',
        'flourmill_sales_create' => 'Create Sales',
        'flourmill_sales_edit' => 'Edit Sales',
        'flourmill_sales_update' => 'Update Sales',
        'flourmill_sales_delete' => 'Delete Sales',
        'flourmill_sales_process' => 'Process Sales',
        'flourmill_sales_reset' => 'Reset Sales',
        'flourmill_sales_items' => 'Items Sales',
        'flourmill_sales_items_list' => 'Items Sales',
        'flourmill_sales_edit_items' => 'Sales Items Edit',
        'flourmill_sales_gatepass' => 'Sales Gatepass',
        'flourmill_sales_details' => 'Sales Details',
        'flourmill_productions_manage' => 'Manage Productions',
        'flourmill_productions_order' => 'Productions Order',
        'flourmill_productions_list' => 'Productions List',
        'flourmill_productions_create' => 'Create Productions',
        'flourmill_productions_edit' => 'Productions Edit',
        'flourmill_productions_update' => 'Productions Update',
        'flourmill_productions_delete' => 'Productions Delete',
        'flourmill_productions_process' => 'Productions Process',
        'flourmill_productions_stock_done' => 'Productions Stock Done',
        'flourmill_productions_reset' => 'Reset Productions',
        'flourmill_productions_items' => 'Items Productions',
        'flourmill_productions_items_list' => ' Productions Items List',
        'flourmill_productions_details' => 'Productions Details',
        'flourmill_productions_order_list' => 'Productions Order List',
        'flourmill_productions_type_list' => 'Productions Type List',
        'flourmill_productions_type_create' => 'Productions Type Create',
        'flourmill_productions_type_edit' => 'Productions Type Edit',
        'flourmill_productions_type_update' => 'Productions Type Update',
        'flourmill_productions_type_delete' => 'Productions Type Delete',
        'flourmill_finish_product' => 'Finish Product',
        'flourmill_finish_product_list' => 'Finish Product List',
        'flourmill_finish_product_create' => 'Finish Product Create',
        'flourmill_finish_product_edit' => 'Finish Product Edit',
        'flourmill_finish_product_update' => 'Finish Product Update',
        'flourmill_finish_product_delete' => 'Finish Product Delete',
        'flourmill_finish_product_details' => 'Finish Product Details',
        'flourmill_finish_product_process' => 'Finish Product Process',
        'flourmill_finish_product_reset' => 'Reset Finish Product',
        'flourmill_finish_product_items' => 'Items Finish Product',
        'flourmill_finish_product_opening_stock' => 'Finish Product Opening Stock',
        'flourmill_finish_product_opening_stock_list' => 'Finish Product Opening Stock List',
        'flourmill_product_transfer_manage' => 'Manage Product Transfer',
        'flourmill_product_transfer_list' => 'Product Transfer List',
        'flourmill_product_transfer_create' => 'Product Transfer Create',
        'flourmill_product_transfer_edit' => 'Product Transfer Edit',
        'flourmill_product_transfer_update' => 'Product Transfer Update',
        'flourmill_product_transfer_delete' => 'Product Transfer Delete',
        'flourmill_product_transfer_process' => 'Product Transfer Process',
        'flourmill_product_transfer_reset' => 'Product Transfer Reset',
        'flourmill_raw_stock_manage' => 'Raw Stock Manage',
        'flourmill_raw_stock_' => 'Raw Stock',
        'flourmill_raw_opening_stock' => 'Raw Opening Stock',
        'flourmill_raw_opening_stock_edit' => 'Raw Opening Stock Edit',
        'flourmill_raw_opening_stock_list' => 'Raw Opening Stock List',
        'flourmill_finish_stock_manage' => 'Finish Stock Manage',
        'flourmill_finish_stock_' => 'Finish Stock',
        'flourmill_finish_opening_stock' => 'Finish Opening Stock',
        'flourmill_finish_opening_stock_list' => 'Finish Opening Stock List',
        'flourmill_manage_report' => 'Manage Report',
        'flourmill_dailty_report' => 'Daily Report',
        'flourmill_stock_register' => 'Stock Register',
        'flourmill_product_register' => 'Products Register',
        'flourmill_profitloss' => 'Profit Loss',
        'flourmill_party_leager' => 'Party Leager',
        'flourmill_emptybag_opening_stock_edit' => 'Emptybag Opening Edit',
        'flourmill_emptybag_opening_stock_delete' => 'Emptybag Opening Delete',
    );
}

function permission_jutemill() {
    return array(
        'jutemill_price_setting' => 'Price Setting',
        'jutemill_price_setting_list' => 'Price Setting List',
        'jutemill_price_setting_create' => 'Price Create',
        'jutemill_price_setting_edit' => 'Price Edit',
        'jutemill_price_setting_delete' => 'Price Delete',
        'jutemill_emptybag_manage' => 'Manage Empty Bag',
        'jutemill_emptybag_purchase' => 'Empty Bag Purchase',
        'jutemill_emptybag_purchase_list' => 'Empty Bag List',
        'jutemill_emptybag_purchase_create' => 'Create Empty Bag',
        'jutemill_emptybag_purchase_edit' => 'Edit Empty Bag',
        'jutemill_emptybag_purchase_item_edit' => 'Empty Bag Item Edit',
        'jutemill_emptybag_purchase_update' => 'Update Empty Bag',
        'jutemill_emptybag_purchase_delete' => 'Delete Empty Bag',
        'jutemill_emptybag_purchase_process' => 'Process Empty Bag',
        'jutemill_emptybag_purchase_reset' => 'Reset Empty Bag',
        'jutemill_emptybag_purchase_item_list' => 'Empty Bag Items List',
        'jutemill_emptybag_purchase_items' => 'Items Empty Bag',
        'jutemill_emptybag_purchase_edit_items' => 'Empty Bag Edit Items',
        'jutemill_emptybag_purchase_gatepass' => 'Empty Bag Gatepass',
        'jutemill_emptybag_purchase_details' => 'Empty Bag Details',
        'jutemill_emptybag_sales' => 'Empty Bag Sales',
        'jutemill_emptybag_sales_list' => 'Empty Bag List',
        'jutemill_emptybag_sales_create' => 'Create Empty Bag',
        'jutemill_emptybag_sales_edit' => 'Edit Empty Bag',
        'jutemill_emptybag_sales_update' => 'Update Empty Bag',
        'jutemill_emptybag_sales_delete' => 'Delete Empty Bag',
        'jutemill_emptybag_sales_process' => 'Process Empty Bag',
        'jutemill_emptybag_sales_reset' => 'Reset Empty Bag',
        'jutemill_emptybag_sales_items' => 'Items Empty Bag',
        'jutemill_emptybag_sales_items_list' => ' Empty Bag Items List',
        'jutemill_emptybag_sales_edit_items' => 'Empty Bag Edit Items',
        'jutemill_emptybag_sales_gatepass' => 'Empty Bag Gatepass',
        'jutemill_emptybag_sales_details' => 'Empty Bag Details',
        'jutemill_emptybag_stock' => 'Empty Bag Stock',
        'jutemill_emptybag_opening_stock' => 'Empty Bag Opening Stock',
        'jutemill_emptybag_opening_stock_create' => 'Opening Stock Create',
        'jutemill_emptybag_opening_stock_edit' => 'Opening Stock Edit',
        'jutemill_emptybag_opening_stock_delete' => 'Opening Stock Delete',
        'jutemill_emptybag_opening_stock_list' => 'Empty Bag Opening Stock List',
        'jutemill_manage_purchase' => 'Manage Purchase',
        'jutemill_purchase' => 'Purchase',
        'jutemill_purchase_list' => 'List Purchase',
        'jutemill_purchase_create' => 'Create Purchase',
        'jutemill_purchase_edit' => 'Edit Purchase',
        'jutemill_purchase_update' => 'Update Purchase',
        'jutemill_purchase_delete' => 'Delelte Purchase',
        'jutemill_purchase_items' => 'Purchase Items',
        'jutemill_purchase_process' => 'Purchase Process',
        'jutemill_purchase_reset' => 'Purchase Reset',
        'jutemill_purchase_edit_items' => 'Purchase Edit items',
        'jutemill_purchase_items' => 'Purchase items',
        'jutemill_purchase_gatepass' => 'Purchase Bag Gatepass',
        'jutemill_purchase_details' => 'Purchase Bag Details',
        'jutemill_sales_manage' => 'Manage Sales',
        'jutemill_sales' => 'Sales',
        'jutemill_sales_list' => 'Sales List',
        'jutemill_sales_create' => 'Create Sales',
        'jutemill_sales_edit' => 'Edit Sales',
        'jutemill_sales_update' => 'Update Sales',
        'jutemill_sales_delete' => 'Delete Sales',
        'jutemill_sales_process' => 'Process Sales',
        'jutemill_sales_reset' => 'Reset Sales',
        'jutemill_sales_items' => 'Items Sales',
        'jutemill_sales_edit_items' => 'Sales Items Edit',
        'jutemill_sales_gatepass' => 'Sales Gatepass',
        'jutemill_sales_details' => 'Sales Details',
        'jutemill_productions_manage' => 'Manage Productions',
        'jutemill_productions_order' => 'Productions Order',
        'jutemill_productions_list' => 'Productions List',
        'jutemill_productions_create' => 'Create Productions',
        'jutemill_productions_edit' => 'Productions Edit',
        'jutemill_productions_update' => 'Productions Update',
        'jutemill_productions_delete' => 'Productions Delete',
        'jutemill_productions_process' => 'Productions Process',
        'jutemill_productions_stock_done' => 'Productions Stock Done',
        'jutemill_productions_reset' => 'Reset Productions',
        'jutemill_productions_items' => 'Items Productions',
        'jutemill_productions_items_list' => ' Productions Items List',
        'jutemill_productions_details' => 'Productions Details',
        'jutemill_productions_order_list' => 'Productions Order List',
        'jutemill_productions_type_list' => 'Productions Type List',
        'jutemill_productions_type_create' => 'Productions Type Create',
        'jutemill_productions_type_edit' => 'Productions Type Edit',
        'jutemill_productions_type_update' => 'Productions Type Update',
        'jutemill_productions_type_delete' => 'Productions Type Delete',
        'jutemill_finish_product' => 'Finish Product',
        'jutemill_finish_product_list' => 'Finish Product List',
        'jutemill_finish_product_create' => 'Finish Product Create',
        'jutemill_finish_product_edit' => 'Finish Product Edit',
        'jutemill_finish_product_update' => 'Finish Product Update',
        'jutemill_finish_product_delete' => 'Finish Product Delete',
        'jutemill_finish_product_details' => 'Finish Product Details',
        'jutemill_finish_product_process' => 'Finish Product Process',
        'jutemill_finish_product_reset' => 'Reset Finish Product',
        'jutemill_finish_product_items' => 'Items Finish Product',
        'jutemill_finish_product_opening_stock' => 'Finish Product Opening Stock',
        'jutemill_finish_product_opening_stock_list' => 'Finish Product Opening Stock List',
        'jutemill_product_transfer_manage' => 'Manage Product Transfer',
        'jutemill_product_transfer_list' => 'Product Transfer List',
        'jutemill_product_transfer_create' => 'Product Transfer Create',
        'jutemill_product_transfer_edit' => 'Product Transfer Edit',
        'jutemill_product_transfer_update' => 'Product Transfer Update',
        'jutemill_product_transfer_delete' => 'Product Transfer Delete',
        'jutemill_product_transfer_process' => 'Product Transfer Process',
        'jutemill_product_transfer_reset' => 'Product Transfer Reset',
        'jutemill_raw_stock_manage' => 'Raw Stock Manage',
        'jutemill_raw_stock_' => 'Raw Stock',
        'jutemill_raw_opening_stock' => 'Raw Opening Stock',
        'jutemill_raw_opening_stock_edit' => 'Raw Opening Stock Edit',
        'jutemill_raw_opening_stock_list' => 'Raw Opening Stock List',
        'jutemill_finish_stock_manage' => 'Finish Stock Manage',
        'jutemill_finish_stock_' => 'Finish Stock',
        'jutemill_finish_opening_stock' => 'Finish Opening Stock',
        'jutemill_finish_opening_stock_list' => 'Finish Opening Stock List',
        'jutemill_manage_report' => 'Manage Report',
        'jutemill_dailty_report' => 'Daily Report',
        'jutemill_stock_register' => 'Stock Register',
        'jutemill_product_register' => 'Products Register',
        'jutemill_profitloss' => 'Profit Loss',
        'jutemill_party_leager' => 'Party Leager',
        'jutemill_emptybag_opening_stock_edit' => 'Emptybag Opening Edit',
        'jutemill_emptybag_opening_stock_delete' => 'Emptybag Opening Delete',
    );
}

function permission_trading() {
    return array(
        'trading_purchase_manage' => 'Purchase Manage',
        'trade_purchase_list' => 'Purchase List',
        'trade_purchase_create' => 'Purchase Create',
        'trade_purchase_edit' => 'Purchase Edit',
        'trade_purchase_delete' => 'Purchase Delete',
        'trade_purchase_details' => 'Purchase View',
        'trade_purchase_gatepass' => 'Purchase Gatepass',
        'trade_purchase_process' => 'Purchase Process',
        'trade_purchase_reset' => 'Purchase Reset',
        'trade_purchase_items_list' => 'Purchase Itemlist',
        'trade_purchase_items' => 'Purchase items',
        'trade_sales_manage' => 'Sale Manage',
        'trade_sales_list' => 'Sale List',
        'trade_sales_create' => 'Sale Create',
        'trade_sales_edit' => 'Sale Edit',
        'trade_sales_delete' => 'Sale Delete',
        'trade_sales_details' => 'Sale View',
        'trade_sales_gatepass' => 'Sale Gatepass',
        'trade_sales_process' => 'Sale Process',
        'trade_sales_reset' => 'Sale Reset',
        'trade_sales_items_list' => 'Sale Itemlist',
        'trade_sales_items' => 'Sale items',
        'trading_stocks' => 'Stocks',
        'trading_report_manage' => 'Reports',
        'trading_process_order' => 'Process Order',
        'trading_reset_order' => 'Reset Order',
        'trade_stock' => 'Stocks',
    );
}

function permission_transport() {
    return array(
        'transport_unit_setting' => 'Unit Setting',
        'transport_unit_create' => 'Unit Create',
        'transport_unit_edit' => 'Unit Edit',
        'transport_unit_delete' => 'Unit Delete',
        'transport_rent_unit_setting' => 'Rent Unit Setting',
        'transport_rent_unit_create' => 'Rent Unit Create',
        'transport_rent_unit_edit' => 'Rent Unit Edit',
        'transport_rent_unit_delete' => 'Rent Unit Delete',
        'transport_vehicle_type' => 'Vehicle Type',
        'transport_vehicle_type_create' => 'Vehicle Type Create',
        'transport_vehicle_type_edit' => 'Vehicle Type Edit',
        'transport_vehicle_type_delete' => 'Vehicle Type Delete',
        'transport_vehicle_list' => 'Vehicle List',
        'transport_vehicle_create' => 'Vehicle Create',
        'transport_vehicle_edit' => 'Vehicle Edit',
        'transport_vehicle_delete' => 'Vehicle Delete',
        'transport_trip_list' => 'Trip List',
        'transport_trip_item_list' => 'Trip Item List',
        'transport_trip_create' => 'Trip Create',
        'transport_trip_edit' => 'Trip Edit',
        'transport_trip_delete' => 'Trip Delete',
        'transport_maintenance_type' => 'Maintenance Type List',
        'transport_maintenance_type_create' => 'Maintenance Type Create',
        'transport_maintenance_type_edit' => 'Maintenance Type Edit',
        'transport_maintenance_type_delete' => 'Maintenance Type Delete',
        'transport_maintenance_report' => 'Maintenance Report List',
        'transport_maintenance_report_create' => 'Maintenance Report Create',
        'transport_maintenance_report_edit' => 'Maintenance Report Edit',
        'transport_maintenance_report_delete' => 'Maintenance Report Delete',
        'transport_report_detail' => 'Report Detail',
        'transport_manage_inventory' => 'Manage Inventory',
        'transport_purchase_list' => 'Purchase List',
        'transport_purchase_create' => 'Purchase Create',
        'transport_purchase_edit' => 'Purchase Edit',
        'transport_purchase_delete' => 'Purchase Delete',
        'transport_purchase_details' => 'Purchase View',
        'transport_purchase_gatepass' => 'Purchase Gatepass',
        'transport_purchase_process' => 'Purchase Process',
        'transport_purchase_reset' => 'Purchase Reset',
        'transport_purchase_items' => 'Purchase items',
        'transport_purchase_items_list' => 'Purchase Itemlist',
        'transport_sales_list' => 'Sale List',
        'transport_sales_create' => 'Sale Create',
        'transport_sales_edit' => 'Sale Edit',
        'transport_sales_delete' => 'Sale Delete',
        'transport_sales_details' => 'Sale View',
        'transport_sales_gatepass' => 'Sale Gatepass',
        'transport_sales_process' => 'Sale Process',
        'transport_sales_reset' => 'Sale Reset',
        'transport_sales_items' => 'Sale items',
        'transport_sales_items_list' => 'Sale Itemlist',
        'transport_stock' => 'Stocks',
        'transport_opening_stock' => 'Opening Stocks',
        'transport_report' => 'Report',
    );
}

function str_to_array($_str_open, $_str_close, $_str, $_delimeter) {
    $_find = [$_str_open, $_str_close];
    $_replace = ['', ''];
    $_strDiar_no = str_replace($_find, $_replace, $_str);
    return explode($_delimeter, $_strDiar_no);
}

function string_to_array($_str_open, $_sec_open, $_sec_close, $_str_close, $_str, $_delimeter) {
    $_find = [$_str_open, $_sec_open, $_sec_close, $_str_close];
    $_replace = ['', ''];
    $_strDiar_no = str_replace($_find, $_replace, $_str);
    return explode($_delimeter, $_strDiar_no);
}
