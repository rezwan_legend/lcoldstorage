<?php

namespace App\Models\Setting\LocationSetting;

use Illuminate\Database\Eloquent\Model;

class Floor extends Model {

    protected $table = 'floors';
    public $timestamps = false;

    public static function model($className = __CLASS__) {
        return new $className();
    }

    public function common() {
        return $this->has('common_id', 'id')->where('is_deleted', 0);
    }

    public function chambers() {
        return $this->belongsTo('App\Models\Setting\LocationSetting\ChamberSetting', 'chamber_id', 'id')->where('is_deleted', 0);
    }

        public function pockets() {
        return $this->hasMany('App\Models\Setting\LocationSetting\Pocket', 'floor_id', 'id')->where('is_deleted',0);
    }

    public function particulars() {
        return $this->hasMany('App\Models\Account\Particular', 'head_id', 'id')->where('is_deleted', 0);
    }

    public function report_setting() {
        return $this->hasMany('App\Models\ReportSetting', 'head_id', 'id')->where('is_deleted', 0);
    }

}
