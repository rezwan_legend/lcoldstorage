<?php

namespace App\Models\Setting\LocationSetting;

use Illuminate\Database\Eloquent\Model;

class Pocket extends Model {

    protected $table = 'pockets';
    public $timestamps = false;

    public static function model($className = __CLASS__) {
        return new $className();
    }

    public function common() {
        return $this->has('common_id', 'id')->where('is_deleted', 0);
    }

    public function chambers() {
        return $this->belongsTo('App\Models\Setting\LocationSetting\ChamberSetting', 'chamber_id', 'id')->where('is_deleted', 0);
    }
   
        public function floor() {
        return $this->belongsTo('App\Models\Setting\LocationSetting\Poket', 'floor_id', 'id')->where('is_deleted', 0);
    }

    public function particulars() {
        return $this->hasMany('App\Models\Account\Particular', 'head_id', 'id')->where('is_deleted', 0);
    }

    public function report_setting() {
        return $this->hasMany('App\Models\ReportSetting', 'head_id', 'id')->where('is_deleted', 0);
    }

}
