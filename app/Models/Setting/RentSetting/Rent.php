<?php

namespace App\Models\Setting\RentSetting;

use Illuminate\Database\Eloquent\Model;

class Rent extends Model {

    protected $table = 'rent';
    public $timestamps = false;

    public static function model($className = __CLASS__) {
        return new $className();
    }

    public function company() {
        return $this->belongsTo('App\Models\Institute', 'company_id', 'id');
    }

    public function product_type() {
        return $this->belongsTo('App\Models\Setting\ProductModule\ProductType', 'product_type_id', 'id');
    }
    public function categories() {
        return $this->belongsTo('App\Models\Setting\ProductModule\Category', 'category_id', 'id');
    }
    public function products() {
        return $this->belongsTo('App\Models\Setting\ProductModule\Product', 'product_id', 'id');
    }

    public function units() {
        return $this->belongsTo('App\Models\Setting\ProductModule\Unit', 'unit_id', 'id');
    }
    public function unitsize() {
        return $this->belongsTo('App\Models\Setting\ProductModule\UnitSize', 'unit_size', 'id');
    }

    public function rent_types(){
        return $this->belongsTo('App\Models\Setting\RentSetting\RentType', 'rent_type_id', 'id');
    }
}