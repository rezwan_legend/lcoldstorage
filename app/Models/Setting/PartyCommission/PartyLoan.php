<?php

namespace App\Models\Setting\PartyCommission;

use Illuminate\Database\Eloquent\Model;

class PartyLoan extends Model {

    protected $table = 'party_loan';
    public $timestamps = false;

    public static function model($className = __CLASS__) {
        return new $className();
    }

     public function party_type() {
        return $this->belongsTo('App\Models\Setting\Party\PartyType', 'party_type_id', 'id');
    }
    public function party() {
        return $this->belongsTo('App\Models\Setting\Party\Party', 'party_id', 'id');
    }
    public function categories() {
        return $this->belongsTo('App\Models\Setting\ProductModule\Category', 'category_id', 'id');
    }
    public function products() {
        return $this->belongsTo('App\Models\Setting\ProductModule\Product', 'product_id', 'id');
    }

    public function units() {
        return $this->belongsTo('App\Models\Setting\ProductModule\Unit', 'unit_id', 'id');
    }
     public function UnitSize() {
        return $this->belongsTo('App\Models\Setting\ProductModule\UnitSize', 'unit_size_id', 'id');
    }
 
    public function rent_types(){
        return $this->belongsTo('App\Models\Setting\RentSetting\RentType', 'rent_type_id', 'id');
    }
     public function rent(){
        return $this->belongsTo('App\Models\Setting\RentSetting\Rent', 'rent_id', 'id');
    }

    public function stock() {
        return $this->hasMany('App\Models\Main\Stock', 'category_id', 'id');
    }

    public function product_transfer() {
        return $this->hasMany('App\Models\Main\ProductTransfer', 'category_id', 'id');
    }

    public function ricesales_item() {
        return $this->belongsTo('App\Models\Main\SalesOrderItem', 'category_id', 'id');
    }

    public function ricepurchases_item() {
        return $this->hasMany('App\Models\Main\RicePurchasesItem', 'category_id', 'id');
    }

    public function rawsales_item() {
        return $this->hasMany('App\Models\Main\RawSalesItem', 'category_id', 'id');
    }

    public function paddyPurchases_item() {
        return $this->hasMany('App\Models\Main\PurchaseItem', 'category_id', 'id');
    }

    public function getList($comId = null) {
        $_query = $this->where('is_deleted', 0);
        if (!is_null($comId)) {
            $_query->where('company_id', $comId);
        }
        $_dataset = $_query->get();
        return !empty($_dataset) ? $_dataset : null;
    }

    public function custom_labels() {
        return [
            "id" => "ID",
            "business_type_id" => "Business Type",
            "type" => "Type",
            "name" => "Name",
            "unit" => "Unit",
            "description" => "Description",
        ];
    }

    public function tableColumnsAsLabel() {
        $_arr = [];
        $columns = $this->getTableColumns();
        foreach ($columns as $ck => $cv) {
            $_arr[$cv] = $cv;
        }
        return $_arr;
    }

    public function getTableColumns() {
        return $this->getConnection()->getSchemaBuilder()->getColumnListing($this->getTable());
    }

}
