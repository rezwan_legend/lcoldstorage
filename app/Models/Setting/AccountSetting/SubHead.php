<?php

namespace App\Models\Setting\AccountSetting;

use Illuminate\Database\Eloquent\Model;

class SubHead extends Model {

    protected $table = 'subheads';
    public $timestamps = false;

    public static function model($className = __CLASS__) {
        return new $className();
    }

    public function head() {
        return $this->belongsTo('App\Models\Setting\AccountSetting\Head', 'head_id');
    }

    public function particulars() {
        return $this->hasMany('App\Models\Account\Particular', 'subhead_id', 'id')->where('is_deleted', 0);
    }

    public function suppliers() {
        return $this->hasMany('App\Models\FillingStation\Supplier', 'subhead_id', 'id');
    }

    public function customers() {
        return $this->hasMany('App\Models\FillingStation\Customer', 'subhead_id', 'id');
    }

    public function bank() {
        return $this->belongsTo('App\Models\FillingStation\Bank', 'bank_id');
    }

    public function getList($_company = null, $_head = null) {
        $query = $this->where('is_deleted', 0);
        $query->where(function ($q) {
            $q->where('common', COMMON_YES);
        });
        if (!is_null($_company)) {
            $query->where('institute_id', $_company);
        }
        if (!is_null($_head)) {
            $query->where('head_id', $_head);
        }
        $query->orderBy("name", "ASC");
        $_dataset = $query->get();
        return $_dataset;
    }

}
