<?php

namespace App\Models\Setting\AccountSetting;

use Illuminate\Database\Eloquent\Model;

class Head extends Model {

    protected $table = 'heads';
    public $timestamps = false;

    public static function model($className = __CLASS__) {
        return new $className();
    }

    public function common() {
        return $this->has('common_id', 'id')->where('is_deleted', 0);
    }

    public function subheads() {
        return $this->hasMany('App\Models\Account\SubHead', 'head_id', 'id')->where('is_deleted', 0);
    }

    public function particulars() {
        return $this->hasMany('App\Models\Account\Particular', 'head_id', 'id')->where('is_deleted', 0);
    }

    public function report_setting() {
        return $this->hasMany('App\Models\ReportSetting', 'head_id', 'id')->where('is_deleted', 0);
    }

}
