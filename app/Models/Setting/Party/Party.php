<?php

namespace App\Models\Setting\Party;

use Illuminate\Database\Eloquent\Model;

class Party extends Model {

    protected $table = 'party';
    public $timestamps = false;

    public static function model($className = __CLASS__) {
        return new $className();
    }

    public function tablename() {
        return $this->table;
    }
    public function party_types() {
        return $this->belongsTo('App\Models\Setting\Party\PartyType', 'party_type_id', 'id');
    }

    public function company() {
        return $this->belongsTo('App\Models\Institute', 'company_id', 'id');
    }

    public function products() {
        return $this->hasMany('App\Models\Product', 'product_type_id', 'id')->where('is_deleted', 0)->orderBy('name', 'asc');
    }

    public function paddy_purchasesitem() {
        return $this->hasMany('App\Models\Main\PurchaseItem', 'product_type_id', 'id');
    }

    public function rice_salesitem() {
        return $this->hasMany('App\Models\Main\SalesOrderItem', 'product_type_id', 'id');
    }

    public function price_setting() {
        return $this->hasMany('App\Models\Ricemill\PriceSetting', 'company_id', 'id');
    }

    public function raw_salesitem() {
        return $this->hasMany('App\Models\Main\RawSalesItem', 'product_type_id', 'id');
    }

    public function size() {
        return $this->hasMany('App\Models\UnitSize', 'product_type_id', 'id');
    }

    public function UnitSize() {
        return $this->hasMany('App\Models\Product', 'product_type_id', 'id');
    }

    public function dry_receive() {
        return $this->hasMany('App\Models\Main\DryReceive', 'prod_type', 'id');
    }

    public function transfer() {
        return $this->hasMany('App\Models\Main\Transfer', 'prod_type', 'id');
    }

    public function stock() {
        return $this->hasMany('App\Models\Main\Stock', 'product_type_id', 'id');
    }

    public function producton() {
        return $this->hasMany('App\Models\Main\Production', 'product_type', 'id');
    }

    public function ricepurchases_item() {
        return $this->hasMany('App\Models\Main\RicePurchasesItem', 'product_type_id', 'id');
    }

    public function production_item() {
        return $this->hasMany('App\Models\Main\ProductionItem', 'product_type_id', 'id');
    }

    public function finish_stock_item() {
        return $this->belongsTo(' App\Models\Main\FinishStockItem', 'product_type_id', 'id');
    }

    public function getList($comId = null) {
        $_query = $this->where('is_deleted', 0);
        if (!is_null($comId)) {
            $_query->where('company_id', $comId);
        }
        $_dataset = $_query->get();
        return !empty($_dataset) ? $_dataset : null;
    }

}
