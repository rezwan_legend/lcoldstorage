<?php

namespace App\Models\Setting\ProductModule;

use Illuminate\Database\Eloquent\Model;

class Product extends Model {

    protected $table = 'product';
    public $timestamps = false;

    public static function model($className = __CLASS__) {
        return new $className();
    }

    public function company() {
        return $this->belongsTo('App\Models\Institute', 'company_id');
    }

    public function product_type() {
        return $this->belongsTo('App\Models\Setting\ProductModule\ProductType', 'product_type_id');
    }

    public function category() {
        return $this->belongsTo('App\Models\Setting\ProductModule\Category', 'category_id');
    }

    public function stock() {
        return $this->hasMany('App\Models\Main\Stock', 'product_id', 'id');
    }

    public function UnitSize() {
        return $this->hasMany('App\Models\UnitSize', 'product_id ', 'id');
    }

    public function transfer() {
        return $this->hasMany('App\Models\Main\Transfer', 'product_id ', 'id');
    }

    public function itmes() {
        return $this->hasMany('App\Models\Main\EmptyBagPaymentItem', 'product_id', 'id');
    }

    public function purchases_item() {
        return $this->hasMany('App\Models\Main\PurchaseItem', 'product_id', 'id');
    }

    public function emptybag_itmes() {
        return $this->hasMany('App\Models\Main\EmptyBagItem', 'product_id', 'id');
    }

    public function emptybag_sales_itmes() {
        return $this->hasMany('App\Models\Main\EmptybagSalesItem', 'product_id', 'id');
    }

    public function emptybag_payment_item() {
        return $this->hasMany('App\Models\Main\EmptybagPurchaseItem', 'product_id', 'id');
    }

    public function sales_item() {
        return $this->hasMany('App\Models\Main\SalesOrderItem', 'sales_id', 'id');
    }

    public function rice_purchases_item() {
        return $this->hasMany('App\Models\Main\RicePurchasesItem', 'product_id', 'id');
    }

    public function rawsales_items() {
        return $this->hasMany('App\Models\Main\SalesOrderItem', 'product_id', 'id');
    }

    public function subhead() {
        return $this->belongsTo('App\Models\Account\SubHead', 'subhead_id');
    }

    public function getList($id = null) {
        $query = $this->where('is_deleted', 0);
        if (!is_null($id)) {
            $query->where('category_id', $id);
        }
        $query->orderBy("name", "ASC");
        $_dataset = $query->get();
        return $_dataset;
    }

    public function name($id = null) {
        $query = $this->where('is_deleted', 0);
        if (!is_null($id)) {
            $query->where('id', $id);
        }
        $_data = $query->first();
        return !empty($_data) ? $_data->name : "";
    }

}
