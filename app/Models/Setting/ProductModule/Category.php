<?php

namespace App\Models\Setting\ProductModule;

use Illuminate\Database\Eloquent\Model;

class Category extends Model {

    protected $table = 'category';
    public $timestamps = false;

    public static function model($className = __CLASS__) {
        return new $className();
    }

    public function company() {
        return $this->belongsTo('App\Models\Institute', 'company_id', 'id');
    }

    public function product_type() {
        return $this->belongsTo('App\Models\Setting\ProductModule\ProductType', 'product_type_id', 'id');
    }

    public function products() {
        return $this->hasMany('App\Models\Product', 'category_id', 'id')->where('is_deleted', 0)->orderBy('name', 'asc');
    }

    public function stock() {
        return $this->hasMany('App\Models\Main\Stock', 'category_id', 'id');
    }

    public function product_transfer() {
        return $this->hasMany('App\Models\Main\ProductTransfer', 'category_id', 'id');
    }

    public function ricesales_item() {
        return $this->belongsTo('App\Models\Main\SalesOrderItem', 'category_id', 'id');
    }

    public function ricepurchases_item() {
        return $this->hasMany('App\Models\Main\RicePurchasesItem', 'category_id', 'id');
    }

    public function rawsales_item() {
        return $this->hasMany('App\Models\Main\RawSalesItem', 'category_id', 'id');
    }

    public function paddyPurchases_item() {
        return $this->hasMany('App\Models\Main\PurchaseItem', 'category_id', 'id');
    }

    public function getList($comId = null) {
        $_query = $this->where('is_deleted', 0);
        if (!is_null($comId)) {
            $_query->where('company_id', $comId);
        }
        $_dataset = $_query->get();
        return !empty($_dataset) ? $_dataset : null;
    }

    public function custom_labels() {
        return [
            "id" => "ID",
            "business_type_id" => "Business Type",
            "type" => "Type",
            "name" => "Name",
            "unit" => "Unit",
            "description" => "Description",
        ];
    }

    public function tableColumnsAsLabel() {
        $_arr = [];
        $columns = $this->getTableColumns();
        foreach ($columns as $ck => $cv) {
            $_arr[$cv] = $cv;
        }
        return $_arr;
    }

    public function getTableColumns() {
        return $this->getConnection()->getSchemaBuilder()->getColumnListing($this->getTable());
    }

}
