<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\PurchaseItem;

class Purchase extends Model {

    protected $table = 'purchase_order';
    public $timestamps = false;

    public static function model($className = __CLASS__) {
        $model = new $className();
        return $model;
    }

    public function tablename() {
        return $this->table;
    }

    public function items() {
        return $this->hasMany('App\Models\PurchaseItem', 'purchase_id', 'id');
    }

    public function company() {
        return $this->belongsTo('App\Models\Institute', 'company_id');
    }

    public function from_head() {
        return $this->belongsTo('App\Models\Account\Head', 'head_id');
    }

    public function from_subhead() {
        return $this->belongsTo('App\Models\Account\SubHead', 'subhead_id');
    }

    public function particular() {
        return $this->belongsTo('App\Models\Account\Particular', 'particular_id');
    }

    public function diar_party() {
        return $this->belongsTo('App\Models\Particular', 'diar_party_id');
    }

    public function to_head() {
        return $this->belongsTo('App\Models\Account\Head', 'to_head_id');
    }

    public function to_subhead() {
        return $this->belongsTo('App\Models\Account\SubHead', 'to_subhead_id');
    }

    public function getOrderNo($institute_id) {
        $data = $this->where([['institute_id', $institute_id], ['is_deleted', 0]])->orderBy('order_no', 'desc')->first();
        $num = !empty($data) ? ($data->order_no + 1) : 1;
        return $num;
    }

    public function getInvoiceNo() {
        $data = $this->where('is_deleted', 0)->orderBy('invoice_no', 'desc')->first();
        $num = !empty($data) ? ($data->invoice_no + 1) : 1;
        return $num;
    }

    public function sum_order_qty($id) {
        $sum = VehiclePurchaseItem::where([['purchase_id', $id], ['is_deleted', 0]])->sum('quantity');
        return !empty($sum) ? $sum : 0;
    }

    public function count($id = null) {
        if (!empty($id)) {
            $data = PurchaseItem::where([['is_deleted', 0], ['purchase_id', $id]])->count('id');
        }
        return !empty($data) ? doubleval($data) : 0;
    }

    public function total_price($id = null) {
        $query = PurchaseItem::where('is_deleted', 0);
        if (!is_null($id)) {
            $query->where('purchase_id', $id);
        }
        $_sum = $query->sum('price');
        return !empty($_sum) ? round($_sum, 2) : 0;
    }

    public function total_qty($id = null) {
        $query = PurchaseItem::where('is_deleted', 0);
        if (!is_null($id)) {
            $query->where('purchase_id', $id);
        }
        $_sum = $query->sum('quantity');
        return !empty($_sum) ? round($_sum, 2) : 0;
    }

    public function total_net_weight($id = null) {
        $query = PurchaseItem::where('is_deleted', 0);
        if (!is_null($id)) {
            $query->where('purchase_id', $id);
        }
        $_sum = $query->sum('net_weight');
        return !empty($_sum) ? round($_sum, 2) : 0;
    }

    public function stocks() {
        return $this->hasMany('App\Models\Ricemill\Stock', 'purchases_order_id', 'id');
    }

    public function transactions() {
        return $this->hasMany('App\Models\Account\Transaction', 'purchase_id', 'id')->where('institute_id', $this->company_id);
    }

    public function oredrNo() {
        $chapre = 'Order-';
        $data = $this->where('is_deleted', 0)->orderBy('id', 'desc')->first();
        $oredrNo = !empty($data) ? $data->order_no : '';
        $lastNo = !empty($oredrNo) ? ltrim($oredrNo, $chapre) : 0;
        $nextVal = $lastNo + 1;
        return $chapre . $nextVal;
    }

    public function get_challanno($partyId = null) {
        $_query = $this->where('is_deleted', 0);
        if (!is_null($partyId)) {
            $_query->where('particular_id', $partyId);
        }
        $data = $_query->orderBy('id', 'desc')->first();
        $num = !empty($data) ? ($data->challan_no + 1) : 1;
        return $num;
    }

}
