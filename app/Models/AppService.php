<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AppService extends Model {

    protected $table = 'app_service';
    public $timestamps = false;

    public static function model($className = __CLASS__) {
        $model = new $className();
        return $model;
    }

    public function tablename() {
        return $this->table;
    }

}
