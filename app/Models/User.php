<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable {

    use Notifiable;

    protected $fillable = [
        'name', 'email', 'password', 'institute_id',
    ];
    protected $hidden = [
        'password', 'remember_token',
    ];
    protected $table = 'users';
    public $timestamps = false;

    public static function model($className = __CLASS__) {
        return new $className();
    }

    public function institute() {
        return $this->belongsTo('App\Models\Institute', 'institute_id');
    }

    public static function get_user_info_by_id($id) {
        $user = User::findOrfail($id);
        return $user;
    }

    public static function get_user_permisstion_by_id($id) {
        $permissions = UserPermission::where('user_id', $id)->first()->permissions;
        return json_decode($permissions, true);
    }

    public static function get_user_company_permisstion($id) {
        if (in_array($id, LEGEND_USERS)) {
            return Institute::where([['is_deleted', 0]])->pluck('id')->toArray();
        } else {
            $permissions = UserPermission::where('user_id', $id)->first()->companies;
            return json_decode($permissions, true);
        }
    }

}
