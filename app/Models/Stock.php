<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Stock extends Model {

    protected $table = "stock";
    public $timestamps = false;

    public static function model($className = __CLASS__) {
        $model = new $className();
        return $model;
    }

    public function tablename() {
        return $this->table;
    }

    public function productType() {
        return $this->belongsTo('App\Models\ProductType', 'product_type_id');
    }

    public function category() {
        return $this->belongsTo('App\Models\Category', 'category_id');
    }

    public function institute() {
        return $this->belongsTo('App\Models\Institute', 'company_id');
    }

    public function product() {
        return $this->belongsTo('App\Models\Product', 'product_id');
    }

    public function UnitSize() {
        return $this->belongsTo('App\Models\UnitSize', 'size_id');
    }

    public function productUnit() {
        return $this->belongsTo('App\Models\ProductUnit', 'unit_id');
    }

    public static function get_paddy_dry_in_invoice() {
        $invpre = 'DIN-';
        $data = Stock::where('weight_type', 'dryin')->orderBy('id', 'desc')->first();
        $orderNo = !empty($data) ? $data->invoice_no : '';
        $lastOrd = !empty($orderNo) ? ltrim($orderNo, $invpre) : 0;
        $nextVal = $lastOrd + 1;
        return $invpre . $nextVal;
    }

    public function productName($id) {
        return Product::find($id)->name;
    }

    public function categoryName($id) {
        return Category::find($id)->name;
    }

    public function godownName($id) {
        return Godown::find($id)->name;
    }

    public function sumInQuantity($product_id = null) {
        if (!is_null($product_id)) {
            $data = Stock::where([['weight_type', 'in'], ['product_id', $product_id]])->sum('quantity');
        } else {
            $data = Stock::where('weight_type', 'in')->sum('quantity');
        }
        return !empty($data) ? doubleval($data) : 0;
    }

    public function sumInNetQuantity($product_id = null) {
        if (!is_null($product_id)) {
            $data = Stock::where([['weight_type', 'in'], ['product_id', $product_id]])->sum('net_quantity');
        } else {
            $data = Stock::where('weight_type', 'in')->sum('net_quantity');
        }
        return !empty($data) ? doubleval($data) : 0;
    }

    public function sumOsQuantity($product_id = null) {
        if (!is_null($product_id)) {
            $data = Stock::where([['weight_type', 'os'], ['product_id', $product_id]])->sum('quantity');
        } else {
            $data = Stock::where('weight_type', 'os')->sum('quantity');
        }
        return !empty($data) ? doubleval($data) : 0;
    }

    public function sumOpeningQuantity($product_id, $from_date = null, $end_date = null) {
        $query = $this->where([['product_id', $product_id], ['weight_type', 'os']]);
        if (!is_null($from_date || $end_date)) {
            $query->whereBetween('date', [$from_date, $end_date]);
        }
        $sum = $query->sum('quantity');
        return !empty($sum) ? doubleval($sum) : 0;
    }

    public function purchaseQuantity($product_id, $from_date = null, $end_date = null) {
        $query = $this->where([['product_id', $product_id], ['weight_type', 'in']]);
        if (!is_null($from_date || $end_date)) {
            $query->whereBetween('date', [$from_date, $end_date]);
        }
        $sum = $query->sum('quantity');
        return !empty($sum) ? doubleval($sum) : 0;
    }

    public function saleQuantity($product_id, $from_date = null, $end_date = null) {
        $query = $this->where([['product_id', $product_id], ['weight_type', 'out']]);
        if (!is_null($from_date || $end_date)) {
            $query->whereBetween('date', [$from_date, $end_date]);
        }
        $sum = $query->sum('quantity');
        return !empty($sum) ? doubleval($sum) : 0;
    }

    public function productionQuantity($product_id, $from_date = null, $end_date = null) {
        $query = $this->where([['product_id', $product_id], ['weight_type', 'process']]);
        if (!is_null($from_date || $end_date)) {
            $query->whereBetween('date', [$from_date, $end_date]);
        }
        $sum = $query->sum('quantity');
        return !empty($sum) ? doubleval($sum) : 0;
    }

    public function sumInWeight($product_id = null) {
        if (!is_null($product_id)) {
            $data = Stock::where([['weight_type', 'in'], ['product_id', $product_id]])->sum('weight');
        } else {
            $data = Stock::where('weight_type', 'in')->sum('weight');
        }
        return !empty($data) ? doubleval($data) : 0;
    }

    public function sumOsWeight($product_id = null) {
        if (!is_null($product_id)) {
            $data = Stock::where([['weight_type', 'os'], ['product_id', $product_id]])->sum('weight');
        } else {
            $data = Stock::where('weight_type', 'os')->sum('weight');
        }
        return !empty($data) ? doubleval($data) : 0;
    }

    public function sumOsMon($product_id = null) {
        if (!is_null($product_id)) {
            $data = Stock::where([['weight_type', 'os'], ['product_id', $product_id]])->sum('mon');
        } else {
            $data = Stock::where('weight_type', 'os')->sum('mon');
        }
        return !empty($data) ? doubleval($data) : 0;
    }

    public function sumOsKg($product_id = null) {
        if (!is_null($product_id)) {
            $data = Stock::where([['weight_type', 'os'], ['product_id', $product_id]])->sum('kg');
        } else {
            $data = Stock::where('weight_type', 'os')->sum('kg');
        }
        return !empty($data) ? doubleval($data) : 0;
    }

    public function sumOutQuantity($product_id = null) {
        if (!is_null($product_id)) {
            $data = Stock::where([['weight_type', 'out'], ['product_id', $product_id]])->sum('quantity');
        } else {
            $data = Stock::where('weight_type', 'out')->sum('quantity');
        }
        return !empty($data) ? doubleval($data) : 0;
    }

    public function sumOutNetQuantity($product_id = null) {
        if (!is_null($product_id)) {
            $data = Stock::where([['weight_type', 'out'], ['product_id', $product_id]])->sum('net_quantity');
        } else {
            $data = Stock::where('weight_type', 'out')->sum('net_quantity');
        }
        return !empty($data) ? doubleval($data) : 0;
    }

    public function sumOutWeight($product_id = null) {
        if (!is_null($product_id)) {
            $data = Stock::where([['weight_type', 'out'], ['product_id', $product_id]])->sum('weight');
        } else {
            $data = Stock::where('weight_type', 'out')->sum('weight');
        }
        return !empty($data) ? doubleval($data) : 0;
    }

    public function sumProcessQuantity($product_id = null) {
        if (!is_null($product_id)) {
            $data = Stock::where([['weight_type', 'process'], ['product_id', $product_id]])->sum('quantity');
        } else {
            $data = Stock::where('weight_type', 'process')->sum('quantity');
        }
        return !empty($data) ? doubleval($data) : 0;
    }

    public function sumProcessWeight($product_id = null) {
        if (!is_null($product_id)) {
            $data = Stock::where([['weight_type', 'process'], ['product_id', $product_id]])->sum('weight');
        } else {
            $data = Stock::where('weight_type', 'process')->sum('weight');
        }
        return !empty($data) ? doubleval($data) : 0;
    }

    public function sumDryInQuantity($product_id = null) {
        if (!is_null($product_id)) {
            $data = Stock::where([['weight_type', 'dryin'], ['product_id', $product_id]])->sum('quantity');
        } else {
            $data = Stock::where('weight_type', 'dryin')->sum('quantity');
        }
        return !empty($data) ? doubleval($data) : 0;
    }

    public function sumDryInWeight($product_id = null) {
        if (!is_null($product_id)) {
            $data = Stock::where([['weight_type', 'dryin'], ['product_id', $product_id]])->sum('weight');
        } else {
            $data = Stock::where('weight_type', 'dryin')->sum('weight');
        }
        return !empty($data) ? doubleval($data) : 0;
    }

    public function sumDryOutQuantity($product_id = null) {
        if (!is_null($product_id)) {
            $data = Stock::where([['weight_type', 'dryout'], ['product_id', $product_id]])->sum('quantity');
        } else {
            $data = Stock::where('weight_type', 'dryout')->sum('quantity');
        }
        return !empty($data) ? doubleval($data) : 0;
    }

    public function sumDryOutWeight($product_id = null) {
        if (!is_null($product_id)) {
            $data = Stock::where([['weight_type', 'dryout'], ['product_id', $product_id]])->sum('weight');
        } else {
            $data = Stock::where('weight_type', 'dryout')->sum('weight');
        }
        return !empty($data) ? doubleval($data) : 0;
    }

    public function sumStockQuantity($product_id = null) {
        if (!is_null($product_id)) {
            $data = ( $this->sumInQuantity($product_id) + $this->sumOsQuantity($product_id) + $this->sumDryInQuantity($product_id) ) - ($this->sumOutQuantity($product_id) + $this->sumProcessQuantity($product_id) + $this->sumDryOutQuantity($product_id));
        } else {
            $data = ( $this->sumInQuantity() + $this->sumOsQuantity() + $this->sumDryInQuantity() ) - ($this->sumOutQuantity() + $this->sumProcessQuantity() + $this->sumDryOutQuantity());
        }
        return !empty($data) ? doubleval($data) : 0;
    }

    public function sumStockQuantityByDate($product_id = null, $from_date, $end_date) {
        if (!is_null($product_id)) {
            $data = ( $this->purchaseQuantity($product_id, $from_date, $end_date) + $this->sumOpeningQuantity($product_id, $from_date, $end_date) ) - ($this->saleQuantity($product_id, $from_date, $end_date) + $this->productionQuantity($product_id, $from_date, $end_date) );
        } else {
            $data = ( $this->purchaseQuantity($product_id, $from_date, $end_date) + $this->sumOpeningQuantity($product_id, $from_date, $end_date) ) - ($this->saleQuantity($product_id, $from_date, $end_date) + $this->productionQuantity($product_id, $from_date, $end_date) );
        }
        return !empty($data) ? doubleval($data) : 0;
    }

    public function sumStockNetQuantity($product_id = null) {
        if (!is_null($product_id)) {
            $data = ( $this->sumInNetQuantity($product_id) + $this->sumOsQuantity($product_id) + $this->sumDryInQuantity($product_id) ) - ($this->sumOutQuantity($product_id) + $this->sumProcessQuantity($product_id) + $this->sumDryOutQuantity($product_id));
        } else {
            $data = ( $this->sumInNetQuantity() + $this->sumOsQuantity() + $this->sumDryInQuantity() ) - ($this->sumOutQuantity() + $this->sumProcessQuantity() + $this->sumDryOutQuantity());
        }
        return !empty($data) ? doubleval($data) : 0;
    }

    public function sumStockNetQuantityByDate($product_id = null, $from_date, $end_date) {
        if (!is_null($product_id)) {
            $data = ( $this->purchaseQuantity($product_id, $from_date, $end_date) + $this->sumOpeningQuantity($product_id, $from_date, $end_date) ) - ($this->saleQuantity($product_id, $from_date, $end_date) + $this->productionQuantity($product_id, $from_date, $end_date) );
        } else {
            $data = ( $this->purchaseQuantity($product_id, $from_date, $end_date) + $this->sumOpeningQuantity($product_id, $from_date, $end_date) ) - ($this->saleQuantity($product_id, $from_date, $end_date) + $this->productionQuantity($product_id, $from_date, $end_date) );
        }
        return !empty($data) ? doubleval($data) : 0;
    }

    public function sumStockWeight($product_id = null) {
        if (!is_null($product_id)) {
            $data = ( $this->sumInWeight($product_id) + $this->sumOsWeight($product_id) + $this->sumDryInWeight($product_id) ) - ($this->sumOutWeight($product_id) + $this->sumProcessWeight($product_id) + $this->sumDryOutWeight($product_id));
        } else {
            $data = ( $this->sumInWeight() + $this->sumOsWeight() + $this->sumDryInWeight() ) - ($this->sumOutWeight() + $this->sumProcessWeight() + $this->sumDryOutWeight());
        }
        return !empty($data) ? doubleval($data) : 0;
    }

    public function sumInNetPrice($product_id = null) {
        if (!is_null($product_id)) {
            $data = Weight::where([['weight_type', 'paddyin'], ['product_id', $product_id]])->sum('net_payable');
        } else {
            $data = Weight::where('weight_type', 'paddyin')->sum('net_payable');
        }
        return !empty($data) ? doubleval($data) : 0;
    }

    public function sumInMonPrice($product_id = null) {
        if (!is_null($product_id)) {
            $data = WeightItem::where([['weight_type', 'paddyin'], ['product_id', $product_id]])->sum('per_bag_price');
        } else {
            $data = WeightItem::where('weight_type', 'paddyin')->sum('per_bag_price');
        }
        return !empty($data) ? doubleval($data) : 0;
    }

    public function sumInPrice($product_id = null) {
        if (!is_null($product_id)) {
            $data = $this->where([['weight_type', 'in'], ['product_id', $product_id]])->sum('total_price');
        } else {
            $data = $this->where('weight_type', 'in')->sum('total_price');
        }
        return !empty($data) ? doubleval($data) : 0;
    }

    public function sumInPriceByDate($product_id = null, $from_date, $end_date) {
        if (!is_null($product_id)) {
            $data = $this->where([['weight_type', 'in'], ['product_id', $product_id]])->whereBetween('date', [$from_date, $end_date])->sum('total_price');
        } else {
            $data = $this->where('weight_type', 'in')->whereBetween('date', [$from_date, $end_date])->sum('total_price');
        }
        return !empty($data) ? doubleval($data) : 0;
    }

    public function sumOsTotalPrice($product_id = null) {
        if (!is_null($product_id)) {
            $data = $this->where([['weight_type', 'os'], ['product_id', $product_id]])->sum('total_price');
        } else {
            $data = $this->where('weight_type', 'os')->sum('total_price');
        }
        return !empty($data) ? doubleval($data) : 0;
    }

    public function sumOsPerKgPrice($product_id = null) {
        if (!is_null($product_id)) {
            $data = $this->where([['weight_type', 'os'], ['product_id', $product_id]])->sum('per_kg_price');
        } else {
            $data = $this->where('weight_type', 'os')->sum('per_kg_price');
        }
        return !empty($data) ? doubleval($data) : 0;
    }

    public function sumOsMonPrice($product_id = null) {
        if (!is_null($product_id)) {
            $data = $this->where([['weight_type', 'os'], ['product_id', $product_id]])->sum('per_mon_price');
        } else {
            $data = $this->where('weight_type', 'os')->sum('per_mon_price');
        }
        return !empty($data) ? doubleval($data) : 0;
    }

    public function sumOsPerBagPrice($product_id = null) {
        if (!is_null($product_id)) {
            $data = $this->where([['weight_type', 'os'], ['product_id', $product_id]])->sum('per_bag_price');
        } else {
            $data = $this->where('weight_type', 'os')->sum('per_bag_price');
        }
        return !empty($data) ? doubleval($data) : 0;
    }

    public function sumOsTotalPriceByDate($product_id = null, $from_date, $end_date) {
        if (!is_null($product_id)) {
            $data = $this->where([['weight_type', 'os'], ['product_id', $product_id]])->whereBetween('date', [$from_date, $end_date])->sum('total_price');
        } else {
            $data = $this->where('weight_type', 'os')->whereBetween('date', [$from_date, $end_date])->sum('total_price');
        }
        return !empty($data) ? doubleval($data) : 0;
    }

    public function sumOutPrice($product_id = null) {
        if (!is_null($product_id)) {
            $data = $this->where([['weight_type', 'out'], ['product_id', $product_id]])->sum('total_price');
        } else {
            $data = $this->where('weight_type', 'out')->sum('total_price');
        }
        return !empty($data) ? doubleval($data) : 0;
    }

    public function sumOutPriceByDate($product_id = null, $from_date, $end_date) {
        if (!is_null($product_id)) {
            $data = $this->where([['weight_type', 'out'], ['product_id', $product_id]])->whereBetween('date', [$from_date, $end_date])->sum('total_price');
        } else {
            $data = $this->where('weight_type', 'out')->whereBetween('date', [$from_date, $end_date])->sum('total_price');
        }
        return !empty($data) ? doubleval($data) : 0;
    }

    public function sumProcessPrice($product_id = null) {
        if (!is_null($product_id)) {
            $data = $this->where([['weight_type', 'process'], ['product_id', $product_id]])->sum('total_price');
        } else {
            $data = $this->where('weight_type', 'process')->sum('total_price');
        }
        return !empty($data) ? doubleval($data) : 0;
    }

    public function sumProcessPriceByDate($product_id = null, $from_date, $end_date) {
        if (!is_null($product_id)) {
            $data = $this->where([['weight_type', 'process'], ['product_id', $product_id]])->whereBetween('date', [$from_date, $end_date])->sum('total_price');
        } else {
            $data = $this->where('weight_type', 'process')->whereBetween('date', [$from_date, $end_date])->sum('total_price');
        }
        return !empty($data) ? doubleval($data) : 0;
    }

    public function sumStockPrice($product_id = null) {
        if (!is_null($product_id)) {
            $data = ( $this->sumInPrice($product_id) + $this->sumOsTotalPrice($product_id) ) - ($this->sumOutPrice($product_id) + $this->sumProcessPrice($product_id));
        } else {
            $data = ( $this->sumInPrice() + $this->sumOsTotalPrice()) - ($this->sumOutPrice() + $this->sumProcessPrice());
        }
        return !empty($data) ? doubleval($data) : 0;
    }

    public function sumStockPriceByDate($product_id = null, $from_date, $end_date) {
        if (!is_null($product_id)) {
            $data = ( $this->sumInPriceByDate($product_id, $from_date, $end_date) + $this->sumOsTotalPriceByDate($product_id, $from_date, $end_date) + $this->sumDryInPriceByDate($product_id, $from_date, $end_date) ) - ($this->sumOutPriceByDate($product_id, $from_date, $end_date) + $this->sumProcessPriceByDate($product_id, $from_date, $end_date) + $this->sumDryOutPriceByDate($product_id, $from_date, $end_date));
        } else {
            $data = ( $this->sumInPriceByDate($product_id, $from_date, $end_date) + $this->sumOsTotalPriceByDate($product_id, $from_date, $end_date) + $this->sumDryInPriceByDate($product_id, $from_date, $end_date) ) - ($this->sumOutPriceByDate($product_id, $from_date, $end_date) + $this->sumProcessPriceByDate($product_id, $from_date, $end_date) + $this->sumDryOutPriceByDate($product_id, $from_date, $end_date));
        }
        return !empty($data) ? doubleval($data) : 0;
    }

    public function sumInOthersQuantity($godown_id, $product_id = null) {
        if (!is_null($product_id)) {
            $data = Stock::where([['weight_type', 'tin'], ['product_id', $product_id], ['godown_id', $godown_id]])->sum('quantity');
        } else {
            $data = Stock::where([['weight_type', 'tin'], ['godown_id', $godown_id]])->sum('quantity');
        }
        return !empty($data) ? doubleval($data) : 0;
    }

    public function sumInOthersWeight($godown_id, $product_id = null) {
        if (!is_null($product_id)) {
            $data = Stock::where([['weight_type', 'tin'], ['product_id', $product_id], ['godown_id', $godown_id]])->sum('weight');
        } else {
            $data = Stock::where([['weight_type', 'tin'], ['godown_id', $godown_id]])->sum('weight');
        }
        return !empty($data) ? doubleval($data) : 0;
    }

    public function sumOutOthersQuantity($godown_id, $product_id = null) {
        if (!is_null($product_id)) {
            $data = Stock::where([['weight_type', 'tout'], ['product_id', $product_id], ['godown_id', $godown_id]])->sum('quantity');
        } else {
            $data = Stock::where([['weight_type', 'tout'], ['godown_id', $godown_id]])->sum('quantity');
        }
        return !empty($data) ? doubleval($data) : 0;
    }

    public function sumOutOthersWeight($godown_id, $product_id = null) {
        if (!is_null($product_id)) {
            $data = Stock::where([['weight_type', 'tout'], ['product_id', $product_id], ['godown_id', $godown_id]])->sum('weight');
        } else {
            $data = Stock::where([['weight_type', 'tout'], ['godown_id', $godown_id]])->sum('weight');
        }
        return !empty($data) ? doubleval($data) : 0;
    }

    public function sumStockOthersQuantity($godown_id, $product_id = null) {
        if (!is_null($product_id)) {
            $data = ( $this->sumInOthersQuantity($godown_id, $product_id) - $this->sumOutOthersQuantity($godown_id, $product_id) );
        } else {
            $data = ( $this->sumInOthersQuantity($godown_id) - $this->sumOutOthersQuantity($godown_id) );
        }
        return !empty($data) ? doubleval($data) : 0;
    }

    public function sumStockOthersWeight($godown_id, $product_id = null) {
        if (!is_null($product_id)) {
            $data = ( $this->sumInOthersWeight($godown_id, $product_id) - $this->sumOutOthersWeight($godown_id, $product_id) );
        } else {
            $data = ( $this->sumInOthersWeight($godown_id) - $this->sumOutOthersWeight($godown_id) );
        }
        return !empty($data) ? doubleval($data) : 0;
    }

    public function sumInGodownQuantity($godown_id, $product_id) {
        $query = Stock::where([['weight_type', 'tin'], ['godown_id', $godown_id], ['product_id', $product_id]]);
        $data = $query->sum('quantity');
        return !empty($data) ? doubleval($data) : 0;
    }

    public function sumOutGodownQuantity($godown_id, $product_id) {
        $query = Stock::where([['weight_type', 'tout'], ['godown_id', $godown_id], ['product_id', $product_id]]);
        $data = $query->sum('quantity');
        return !empty($data) ? doubleval($data) : 0;
    }

    public function sumStockGodownQuantity($godown_id, $product_id) {
        $data = ( $this->sumInGodownQuantity($godown_id, $product_id) - $this->sumOutGodownQuantity($godown_id, $product_id) );
        return !empty($data) ? doubleval($data) : 0;
    }

    public function stockInQuantity($product_id = null, $godown_id = null, $from_date = null, $end_date = null) {
        $query = $this->where('stock_type', 'in');
        if (!is_null($product_id)) {
            $query->where('product_id', $product_id);
        }
        if (!is_null($godown_id)) {
            $query->where('godown_id', $godown_id);
        }
        if (!is_null($from_date)) {
            $query->whereBetween('date', [$from_date, $end_date]);
        }
        $sum = $query->sum('quantity');
        return !empty($sum) ? $sum : 0;
    }

    public function stockOutQuantity($product_id = null, $godown_id = null, $from_date = null, $end_date = null) {
        $query = $this->where('stock_type', 'out');
        if (!is_null($product_id)) {
            $query->where('product_id', $product_id);
        }
        if (!is_null($godown_id)) {
            $query->where('godown_id', $godown_id);
        }
        if (!is_null($from_date)) {
            $query->whereBetween('date', [$from_date, $end_date]);
        }
        $sum = $query->sum('quantity');
        return !empty($sum) ? $sum : 0;
    }

    public function stockQuantity($product_id = null, $godown_id = null, $from_date = null, $end_date = null) {
        $stockInQuantity = $this->stockInQuantity($product_id, $godown_id, $from_date, $end_date);
        $stockOutQuantity = $this->stockOutQuantity($product_id, $godown_id, $from_date, $end_date);
        $stock = $stockInQuantity - $stockOutQuantity;
        return !empty($stock) ? $stock : 0;
    }

    public function stockInNetQuantity($product_id = null, $godown_id = null, $from_date = null, $end_date = null) {
        $query = $this->where('stock_type', 'in');
        if (!is_null($product_id)) {
            $query->where('product_id', $product_id);
        }
        if (!is_null($godown_id)) {
            $query->where('godown_id', $godown_id);
        }
        if (!is_null($from_date)) {
            $query->whereBetween('date', [$from_date, $end_date]);
        }
        $sum = $query->sum('net_quantity');
        return !empty($sum) ? $sum : 0;
    }

    public function stockOutNetQuantity($product_id = null, $godown_id = null, $from_date = null, $end_date = null) {
        $query = $this->where('stock_type', 'out');
        if (!is_null($product_id)) {
            $query->where('product_id', $product_id);
        }
        if (!is_null($godown_id)) {
            $query->where('godown_id', $godown_id);
        }
        if (!is_null($from_date)) {
            $query->whereBetween('date', [$from_date, $end_date]);
        }
        $sum = $query->sum('net_quantity');
        return !empty($sum) ? $sum : 0;
    }

    public function stockNetQuantity($product_id = null, $godown_id = null, $from_date = null, $end_date = null) {
        $stockInNetQuantity = $this->stockInNetQuantity($product_id, $godown_id, $from_date, $end_date);
        $stockOutNetQuantity = $this->stockOutNetQuantity($product_id, $godown_id, $from_date, $end_date);
        $stock = $stockInNetQuantity - $stockOutNetQuantity;
        return !empty($stock) ? $stock : 0;
    }

    public function stockInWeight($product_id = null, $godown_id = null, $from_date = null, $end_date = null) {
        $query = $this->where('stock_type', 'in');
        if (!is_null($product_id)) {
            $query->where('product_id', $product_id);
        }
        if (!is_null($godown_id)) {
            $query->where('godown_id', $godown_id);
        }
        if (!is_null($from_date)) {
            $query->whereBetween('date', [$from_date, $end_date]);
        }
        $sum = $query->sum('weight');
        return !empty($sum) ? $sum : 0;
    }

    public function stockOutWeight($product_id = null, $godown_id = null, $from_date = null, $end_date = null) {
        $query = $this->where('stock_type', 'out');
        if (!is_null($product_id)) {
            $query->where('product_id', $product_id);
        }
        if (!is_null($godown_id)) {
            $query->where('godown_id', $godown_id);
        }
        if (!is_null($from_date)) {
            $query->whereBetween('date', [$from_date, $end_date]);
        }
        $sum = $query->sum('weight');
        return !empty($sum) ? $sum : 0;
    }

    public function stockWeight($product_id = null, $godown_id = null, $from_date = null, $end_date = null) {
        $stockInWeight = $this->stockInWeight($product_id, $godown_id, $from_date, $end_date);
        $stockOutWeight = $this->stockOutWeight($product_id, $godown_id, $from_date, $end_date);
        $stock = $stockInWeight - $stockOutWeight;
        return !empty($stock) ? $stock : 0;
    }

    public function stockInPrice($product_id = null, $godown_id = null, $from_date = null, $end_date = null) {
        $query = $this->where('stock_type', 'in');
        if (!is_null($product_id)) {
            $query->where('product_id', $product_id);
        }
        if (!is_null($godown_id)) {
            $query->where('godown_id', $godown_id);
        }
        if (!is_null($from_date)) {
            $query->whereBetween('date', [$from_date, $end_date]);
        }
        $sum = $query->sum('total_price');
        return !empty($sum) ? $sum : 0;
    }

    public function stockOutPrice($product_id = null, $godown_id = null, $from_date = null, $end_date = null) {
        $query = $this->where('stock_type', 'out');
        if (!is_null($product_id)) {
            $query->where('product_id', $product_id);
        }
        if (!is_null($godown_id)) {
            $query->where('godown_id', $godown_id);
        }
        if (!is_null($from_date)) {
            $query->whereBetween('date', [$from_date, $end_date]);
        }
        $sum = $query->sum('total_price');
        return !empty($sum) ? $sum : 0;
    }

    public function stockPrice($product_id = null, $godown_id = null, $from_date = null, $end_date = null) {
        $stockInPrice = $this->stockInPrice($product_id, $godown_id, $from_date, $end_date);
        $stockOutPrice = $this->stockOutPrice($product_id, $godown_id, $from_date, $end_date);
        $stock = $stockInPrice - $stockOutPrice;
        return !empty($stock) ? $stock : 0;
    }

    public function avgStockWeight($product_id = null, $godown_id = null, $from_date = null, $end_date = null) {
        $weight = $this->stockWeight($product_id, $godown_id, $from_date, $end_date);
        $quantity = $this->stockQuantity($product_id, $godown_id, $from_date, $end_date);
        if ($weight == 0) {
            $weight = 1;
        }
        if ($quantity == 0) {
            $quantity = 1;
        }
        $data = !empty($weight) ? $weight / $quantity : 0;
        return !empty($data) ? doubleval($data) : 0;
    }

    public function avgKgPrice($product_id = null, $godown_id = null, $from_date = null, $end_date = null) {
        $price = $this->stockPrice($product_id, $godown_id, $from_date, $end_date);
        $weight = $this->stockWeight($product_id, $godown_id, $from_date, $end_date);
        $data = !empty($price) ? $price / $weight : 0;
        return !empty($data) ? doubleval($data) : 0;
    }

    public function avgBagPrice($product_id = null, $godown_id = null, $from_date = null, $end_date = null) {
        $price = $this->stockPrice($product_id, $godown_id, $from_date, $end_date);
        $quantity = $this->stockQuantity($product_id, $godown_id, $from_date, $end_date);
        if ($price == 0) {
            $price = 1;
        }
        if ($quantity == 0) {
            $quantity = 1;
        }
        $data = !empty($price) ? $price / $quantity : 0;
        return !empty($data) ? doubleval($data) : 0;
    }

    public function quantityToNetQuantity($product, $value) {
        $quantity = !empty($this->stockQuantity($product)) ? $this->stockQuantity($product) : 0;
        $net_quantity = !empty($this->stockNetQuantity($product)) ? $this->stockNetQuantity($product) : 0;
        $ratio = !empty($net_quantity) ? $net_quantity / $quantity : 0;
        $data = $ratio * $value;
        return !empty($data) ? doubleval($data) : 0;
    }

    public function netQuantityToQuantity($product, $value) {
        $quantity = !empty($this->stockQuantity($product)) ? $this->stockQuantity($product) : 0;
        $net_quantity = !empty($this->stockNetQuantity($product)) ? $this->stockNetQuantity($product) : 0;
        $ratio = !empty($quantity) ? $quantity / $net_quantity : 0;
        $data = $ratio * $value;
        return !empty($data) ? doubleval($data) : 0;
    }

    public function stock_in_qty($institute_id, $type, $category, $product, $size) {
        if (!empty(company_id) && !empty($product)) {
            $data = $this->where([['company_id', $institute_id], ['product_type_id', $type], ['category_id', $category], ['product_id', $product], ['size_id', $size], ['stock_type', STOCK_TYPE_IN]])->sum('quantity');
        }
        return !empty($data) ? doubleval($data) : 0;
    }

    public function stock_out_qty($institute_id, $type, $category, $product, $size) {
        if (!empty(company_id) && !empty($product)) {
            $data = $this->where([['company_id', $institute_id], ['product_type_id', $type], ['category_id', $category], ['product_id', $product], ['size_id', $size], ['stock_type', STOCK_TYPE_OUT]])->sum('quantity');
        }
        return !empty($data) ? doubleval($data) : 0;
    }

    public function stock_in_weight($institute_id, $product) {
        if (!empty(company_id) && !empty($product)) {
            $data = $this->where([['stock_type', STOCK_TYPE_IN], ['product_id', $product], ['company_id', $institute_id]])->sum('net_weight');
        }
        return !empty($data) ? doubleval($data) : 0;
    }

    public function stock_out_weight($institute_id, $product) {
        if (!empty(company_id) && !empty($product)) {
            $data = $this->where([['stock_type', STOCK_TYPE_OUT], ['product_id', $product], ['company_id', $institute_id]])->sum('net_weight');
        }
        return !empty($data) ? doubleval($data) : 0;
    }

    public function last_avgrate($institute_id, $_cate_id, $product, $size) {
        $data = $this->where([['company_id', $institute_id], ['category_id', $_cate_id], ['product_id', $product], ['size_id', $size]])->orderBy('id', 'desc')->first();
        return !empty($data->avg_price) ? $data->avg_price : 0;
    }

    public function emptybag_in_qty($prod_id, $size_id) {
        if (!empty($prod_id) && !empty($size_id)) {
            $data = $this->where([['size_id', $size_id], ['product_id', $prod_id], ['stock_type', STOCK_TYPE_IN], ['bag_type', STOCK_EMPTY_BAG]])->sum('quantity');
        }
        return !empty($data) ? doubleval($data) : 0;
    }

    public function emptybag_out_qty($prod_id, $size_id) {
        if (!empty($prod_id) && !empty($size_id)) {
            $data = $this->where([['product_id', $prod_id], ['size_id', $size_id], ['bag_type', STOCK_EMPTY_BAG], ['stock_type', STOCK_TYPE_IN]])->sum('quantity');
        }
        return !empty($data) ? doubleval($data) : 0;
    }

    public function sum_value_stock_in($prod_id, $size_id) {
        if (!empty($prod_id) && !empty($size_id)) {
            $data = $this->where([['product_id', $prod_id], ['size_id', $size_id], ['stock_type', STOCK_TYPE_IN], ['bag_type', STOCK_EMPTY_BAG]])->sum('quantity');
        } else {
            $data = $this->where([['stock_type', STOCK_TYPE_IN], ['bag_type', STOCK_EMPTY_BAG]])->sum('quantity');
        }
        return !empty($data) ? doubleval($data) : 0;
    }

    public function sum_value_stock_out($prod_id, $size_id) {
        if (!empty($prod_id) && !empty($size_id)) {
            $data = $this->where([['product_id', $prod_id], ['size_id', $size_id], ['stock_type', STOCK_TYPE_OUT], ['bag_type', STOCK_EMPTY_BAG]])->sum('quantity');
        } else {
            $data = $this->where([['stock_type', STOCK_TYPE_OUT], ['bag_type', STOCK_EMPTY_BAG]])->sum('quantity');
        }
        return !empty($data) ? doubleval($data) : 0;
    }

    public function product_in_qty($institue, $prod_id, $size_id) {
        if (!empty($institue) && !empty($prod_id) && !empty($size_id)) {
            $data = $this->where([['company_id', $institue], ['product_id', $prod_id], ['size_id', $size_id], ['stock_type', STOCK_TYPE_IN]])->sum('quantity');
        }
        return !empty($data) ? doubleval($data) : 0;
    }

    public function product_in_out($institue, $prod_id, $size_id) {
        if (!empty($institue) && !empty($prod_id) && !empty($size_id)) {
            $data = $this->where([['company_id', $institue], ['product_id', $prod_id], ['size_id', $size_id], ['stock_type', STOCK_TYPE_OUT], ['status', '!=', TRANSFER]])->sum('quantity');
        }
        return !empty($data) ? doubleval($data) : 0;
    }

    public function sum_in($institueId, $productId, $sizeId, $_column, $_conditions = [], $datesArr = []) {
        $query = $this->where([['company_id', $institueId], ['product_id', $productId], ['size_id', $sizeId], ['stock_type', 'In']]);
        if (is_array($_conditions) && !empty($_conditions)) {
            foreach ($_conditions as $_key => $_val) {
                $query->where("{$_key}", $_val);
            }
        }
        if (is_array($datesArr) && !empty($datesArr)) {
            $query->whereBetween('date', [date_ymd($datesArr[0]), date_ymd($datesArr[1])]);
        }
        $sum = $query->sum("{$_column}");
        return !empty($sum) ? $sum : 0;
    }

    public function sum_out($institueId, $productId, $sizeId, $_column, $_conditions = [], $datesArr = []) {
        $query = $this->where([['company_id', $institueId], ['product_id', $productId], ['size_id', $sizeId], ['stock_type', 'Out']]);
        if (is_array($_conditions) && !empty($_conditions)) {
            foreach ($_conditions as $_key => $_val) {
                $query->where("{$_key}", $_val);
            }
        }
        if (is_array($datesArr) && !empty($datesArr)) {
            $query->whereBetween('date', [date_ymd($datesArr[0]), date_ymd($datesArr[1])]);
        }
        $sum = $query->sum("{$_column}");
        return !empty($sum) ? $sum : 0;
    }

    public function sum_in_production($_column, $_conditions = [], $datesArr = []) {
        $query = $this->where('stock_type', 'In');
        if (is_array($_conditions) && !empty($_conditions)) {
            foreach ($_conditions as $_key => $_val) {
                $query->where("{$_key}", $_val);
            }
        }
        if (is_array($datesArr) && !empty($datesArr)) {
            $query->whereBetween('date', [date_ymd($datesArr[0]), date_ymd($datesArr[1])]);
        }
        $sum = $query->sum("{$_column}");
        return !empty($sum) ? $sum : 0;
    }

    public function sum_out_production($_column, $_conditions = [], $datesArr = []) {
        $query = $this->where('stock_type', 'Out');
        if (is_array($_conditions) && !empty($_conditions)) {
            foreach ($_conditions as $_key => $_val) {
                $query->where("{$_key}", $_val);
            }
        }
        if (is_array($datesArr) && !empty($datesArr)) {
            $query->whereBetween('date', [date_ymd($datesArr[0]), date_ymd($datesArr[1])]);
        }
        $sum = $query->sum("{$_column}");
        return !empty($sum) ? $sum : 0;
    }

    public function sum_weight_remain($_column, $conditions = [], $dateArr = []) {
        $_in = $this->sum_in_production($_column, $conditions, $dateArr);
        $_out = $this->sum_out_production($_column, $conditions, $dateArr);
        $sum = ($_in - $_out);
        return !empty($sum) ? $sum : 0;
    }

    public function sum_price_remain($_column, $conditions = [], $dateArr = []) {
        $_in = $this->sum_in_production($_column, $conditions, $dateArr);
        $_out = $this->sum_out_production($_column, $conditions, $dateArr);
        $sum = ($_in - $_out);
        return !empty($sum) ? $sum : 0;
    }

    public function sum_value_in($conditions = [], $dateArr = []) {
        $query = $this->where('stock_type', STOCK_TYPE_IN);
        if (!empty($conditions) && is_array($conditions)) {
            foreach ($conditions as $_column => $_value) {
                $query->where("{$_column}", $_value);
            }
        }
        if (!empty($dateArr) && is_array($dateArr)) {
            $query->whereBetween('date', [date_ymd($dateArr[0]), date_ymd($dateArr[1])]);
        }
        $sum = $query->sum('quantity');
        return !empty($sum) ? $sum : 0;
    }

    public function sum_value_out($conditions = [], $dateArr = []) {
        $query = $this->where('stock_type', STOCK_TYPE_OUT);
        if (!empty($conditions) && is_array($conditions)) {
            foreach ($conditions as $_column => $_value) {
                $query->where("{$_column}", $_value);
            }
        }
        if (!empty($dateArr) && is_array($dateArr)) {
            $query->whereBetween('date', [date_ymd($dateArr[0]), date_ymd($dateArr[1])]);
        }
        $sum = $query->sum('quantity');
        return !empty($sum) ? $sum : 0;
    }

    public function sum_value_remain($conditions = [], $dateArr = []) {
        $_in = $this->sum_value_in($conditions, $dateArr);
        $_out = $this->sum_value_out($conditions, $dateArr);
        $sum = ($_in - $_out);
        return !empty($sum) ? $sum : 0;
    }

    public function purchase_qty_old($conditions = [], $dateArr = []) {
        $query = $this->where([['stock_type', STOCK_TYPE_IN], ['type', STOCK_TYPE_PURCHASE]]);
        if (!empty($conditions) && is_array($conditions)) {
            foreach ($conditions as $_column => $_value) {
                $query->where("{$_column}", $_value);
            }
        }
        if (!empty($dateArr)) {
            $query->whereBetween('date', [date_ymd($dateArr[0]), date_ymd($dateArr[1])]);
        }
        $sum = $query->sum('quantity');
        return !empty($sum) ? $sum : 0;
    }

    public function sale_qty_old($instituteId, $productId, $dateArr = []) {
        $query = $this->where([['company_id', $instituteId], ['product_id', $productId], ['stock_type', STOCK_TYPE_OUT], ['type', STOCK_TYPE_SALE]]);
        if (!empty($dateArr)) {
            $query->whereBetween('date', [date_ymd($dateArr[0]), date_ymd($dateArr[1])]);
        }
        $sum = $query->sum('quantity');
        return !empty($sum) ? $sum : 0;
    }

    public function production_qty($conditions = [], $date) {
        $query = $this->where([['type', STOCK_TYPE_PRODUCTION], ['stock_type', STOCK_TYPE_OUT]]);
        if (!empty($conditions) && is_array($conditions)) {
            foreach ($conditions as $_column => $_value) {
                $query->where("{$_column}", $_value);
            }
        }
        if (!empty($date)) {
            $query->where('date', $date);
        }
        $sum = $query->sum('quantity');
        return !empty($sum) ? $sum : 0;
    }

    public function stock_reg_production_qty($conditions = [], $dateArr = []) {
        $query = $this->where([['type', STOCK_TYPE_PRODUCTION], ['stock_type', STOCK_TYPE_OUT]]);
        if (!empty($conditions) && is_array($conditions)) {
            foreach ($conditions as $_column => $_value) {
                $query->where("{$_column}", $_value);
            }
        }
        if (!empty($dateArr) && is_array($dateArr)) {
            $query->whereBetween('date', [date_ymd($dateArr[0]), date_ymd($dateArr[1])]);
        }
        $sum = $query->sum('quantity');
        return !empty($sum) ? $sum : 0;
    }

    public function stockRegProductTransferQtyOut($conditions = [], $dateArr = []) {
        $query = $this->where([['type', TRANSFER], ['stock_type', STOCK_TYPE_OUT]]);
        if (!empty($conditions) && is_array($conditions)) {
            foreach ($conditions as $_column => $_value) {
                $query->where("{$_column}", $_value);
            }
        }
        if (!empty($dateArr) && is_array($dateArr)) {
            $query->whereBetween('date', [date_ymd($dateArr[0]), date_ymd($dateArr[1])]);
        }
        $sum = $query->sum('quantity');
        return !empty($sum) ? $sum : 0;
    }

    public function stockRegProductTransferQtyIn($conditions = [], $dateArr = []) {
        $query = $this->where([['type', TRANSFER], ['stock_type', STOCK_TYPE_IN]]);
        if (!empty($conditions) && is_array($conditions)) {
            foreach ($conditions as $_column => $_value) {
                $query->where("{$_column}", $_value);
            }
        }
        if (!empty($dateArr) && is_array($dateArr)) {
            $query->whereBetween('date', [date_ymd($dateArr[0]), date_ymd($dateArr[1])]);
        }
        $sum = $query->sum('quantity');
        return !empty($sum) ? $sum : 0;
    }

    public function stock_reg_production_rec($conditions = [], $dateArr = []) {
        $query = $this->where([['type', STOCK_TYPE_FINISH_STOCK], ['stock_type', STOCK_TYPE_IN]]);
        if (!empty($conditions) && is_array($conditions)) {
            foreach ($conditions as $_column => $_value) {
                $query->where("{$_column}", $_value);
            }
        }
        if (!empty($dateArr) && is_array($dateArr)) {
            $query->whereBetween('date', [date_ymd($dateArr[0]), date_ymd($dateArr[1])]);
        }
        $sum = $query->sum('quantity');
        return !empty($sum) ? $sum : 0;
    }

    public function stock_reg_purchase_qty($conditions = [], $dateArr = []) {
        $query = $this->where([['type', STOCK_TYPE_PURCHASE], ['stock_type', STOCK_TYPE_IN]]);
        if (!empty($conditions) && is_array($conditions)) {
            foreach ($conditions as $_column => $_value) {
                $query->where("{$_column}", $_value);
            }
        }
        if (!empty($dateArr) && is_array($dateArr)) {
            $query->whereBetween('date', [date_ymd($dateArr[0]), date_ymd($dateArr[1])]);
        }
        $sum = $query->sum('quantity');
        return !empty($sum) ? $sum : 0;
    }

    public function sr_sale_qty($conditions = [], $dateArr = []) {
        $query = $this->where([['type', STOCK_TYPE_SALE], ['stock_type', STOCK_TYPE_OUT]]);
        if (!empty($conditions) && is_array($conditions)) {
            foreach ($conditions as $_column => $_value) {
                $query->where("{$_column}", $_value);
            }
        }
        if (!empty($dateArr) && is_array($dateArr)) {
            $query->whereBetween('date', [date_ymd($dateArr[0]), date_ymd($dateArr[1])]);
        }
        $sum = $query->sum('quantity');
        return !empty($sum) ? $sum : 0;
    }

    public function purchase_qty($conditions = [], $dateArr = []) {
        $query = $this->where([['type', STOCK_TYPE_PURCHASE], ['stock_type', STOCK_TYPE_IN]]);
        if (!empty($conditions) && is_array($conditions)) {
            foreach ($conditions as $_column => $_value) {
                $query->where("{$_column}", $_value);
            }
        }
        if (!empty($dateArr) && is_array($dateArr)) {
            $query->whereBetween('date', [date_ymd($dateArr[0]), date_ymd($dateArr[1])]);
        }
        $sum = $query->sum('quantity');
        return !empty($sum) ? $sum : 0;
    }

    public function sum_opening($conditions = [], $dateArr = []) {
        $query = $this->where([['type', STOCK_TYPE_OPENING], ['stock_type', STOCK_TYPE_IN]]);
        if (!empty($conditions) && is_array($conditions)) {
            foreach ($conditions as $_column => $_value) {
                $query->where("{$_column}", $_value);
            }
        }
        if (!empty($dateArr) && is_array($dateArr)) {
            $query->whereBetween('date', [date_ymd($dateArr[0]), date_ymd($dateArr[1])]);
        }
        $sum = $query->sum('quantity');
        return !empty($sum) ? $sum : 0;
    }

    public function opening_qty($conditions = [], $date) {
        $query = $this->where([['type', STOCK_TYPE_OPENING], ['stock_type', STOCK_TYPE_IN]]);
        if (!empty($conditions) && is_array($conditions)) {
            foreach ($conditions as $_column => $_value) {
                $query->where("{$_column}", $_value);
            }
        }
        if (!empty($date)) {
            $query->where('date', $date);
        }
        $sum = $query->sum('quantity');
        return !empty($sum) ? $sum : 0;
    }

    public function sale_qty($conditions = [], $date) {
        $query = $this->where([['type', STOCK_TYPE_SALE], ['stock_type', STOCK_TYPE_OUT]]);
        if (!empty($conditions) && is_array($conditions)) {
            foreach ($conditions as $_column => $_value) {
                $query->where("{$_column}", $_value);
            }
        }
        if (!empty($date)) {
            $query->where('date', $date);
        }
        $sum = $query->sum('quantity');
        return !empty($sum) ? $sum : 0;
    }

    public function sum_value_sales_price($conditions = [], $dateArr = []) {
        $query = SalesItem::where([['is_deleted', 0], ['process_status', PROCESS_COMPLETE]]);
        if (!empty($conditions) && is_array($conditions)) {
            foreach ($conditions as $_column => $_value) {
                $query->where("{$_column}", $_value);
            }
        }
        if (!empty($dateArr) && is_array($dateArr)) {
            $query->whereBetween('date', [date_ymd($dateArr[0]), date_ymd($dateArr[1])]);
        }
        $sum = $query->sum('price');
        return !empty($sum) ? $sum : 0;
    }

    public function sum_value_in_price($conditions = [], $dateArr = []) {
        $query = $this->where('stock_type', STOCK_TYPE_IN);
        if (!empty($conditions) && is_array($conditions)) {
            foreach ($conditions as $_column => $_value) {
                $query->where("{$_column}", $_value);
            }
        }
        if (!empty($dateArr) && is_array($dateArr)) {
            $query->whereBetween('date', [date_ymd($dateArr[0]), date_ymd($dateArr[1])]);
        }
        $sum = $query->sum('price');
        return !empty($sum) ? $sum : 0;
    }

    public function sum_value_out_price($conditions = [], $dateArr = []) {
        $query = $this->where('stock_type', STOCK_TYPE_OUT);
        if (!empty($conditions) && is_array($conditions)) {
            foreach ($conditions as $_column => $_value) {
                $query->where("{$_column}", $_value);
            }
        }
        if (!empty($dateArr) && is_array($dateArr)) {
            $query->whereBetween('date', [date_ymd($dateArr[0]), date_ymd($dateArr[1])]);
        }
        $sum = $query->sum('price');
        return !empty($sum) ? $sum : 0;
    }

    public function sum_value_remain_price($conditions = [], $dateArr = []) {
        $_in = $this->sum_value_in_price($conditions, $dateArr);
        $_out = $this->sum_value_out_price($conditions, $dateArr);
        $sum = ($_in - $_out);
        return !empty($sum) ? $sum : 0;
    }

    public function purchase_qty_price($conditions = [], $date) {
        $query = $this->where([['type', STOCK_TYPE_PURCHASE], ['stock_type', STOCK_TYPE_IN]]);
        if (!empty($conditions) && is_array($conditions)) {
            foreach ($conditions as $_column => $_value) {
                $query->where("{$_column}", $_value);
            }
        }
        if (!empty($date)) {
            $query->where('date', $date);
        }
        $sum = $query->sum('price');
        return !empty($sum) ? $sum : 0;
    }

    public function profit_loss_purchase_qty_price($conditions = [], $dateValues = []) {
        $query = $this->where('stock_type', STOCK_TYPE_IN);
        if (!empty($conditions) && is_array($conditions)) {
            foreach ($conditions as $_column => $_value) {
                $query->where("{$_column}", $_value);
            }
        }
        if (!empty($dateValues) && is_array($dateValues)) {
            $query->whereBetween('date', [date_ymd($dateValues[0]), date_ymd($dateValues[1])]);
        }
        $sum = $query->sum('price');
        return !empty($sum) ? $sum : 0;
    }

    public function sum_value_close_in($conditions = [], $dateArr = []) {
        $query = $this->where('type', STOCK_TYPE_IN);
        if (!empty($conditions) && is_array($conditions)) {
            foreach ($conditions as $_column => $_value) {
                $query->where("{$_column}", $_value);
            }
        }
        if (!empty($dateArr) && is_array($dateArr)) {
            $query->whereBetween('date', [date_ymd($dateArr[0]), date_ymd($dateArr[1])]);
        }
        $sum = $query->sum('price');
        return !empty($sum) ? $sum : 0;
    }

    public function sum_value_close_out($conditions = [], $dateArr = []) {
        $query = $this->where('type', STOCK_TYPE_OUT);
        if (!empty($conditions) && is_array($conditions)) {
            foreach ($conditions as $_column => $_value) {
                $query->where("{$_column}", $_value);
            }
        }
        if (!empty($dateArr) && is_array($dateArr)) {
            $query->whereBetween('date', [date_ymd($dateArr[0]), date_ymd($dateArr[1])]);
        }
        $sum = $query->sum('price');
        return !empty($sum) ? $sum : 0;
    }

    public function sum_value_close_value($conditions = [], $dateArr = []) {
        $_in = $this->sum_value_close_in($conditions, $dateArr);
        $_out = $this->sum_value_close_out($conditions, $dateArr);
        $sum = ($_in - $_out);
        return !empty($sum) ? doubleval($sum) : 0;
    }

    public function stockinfo_in_weight($_ins_id, $cate_id, $_prod_id, $_size_id) {
        $query = $this->where([['company_id', $_ins_id], ['category_id', $cate_id], ['product_id', $_prod_id], ['size_id', $_size_id], ['stock_type', STOCK_TYPE_IN]]);
        $sum = $query->sum('net_weight');
        return !empty($sum) ? $sum : 0;
    }

    public function stockinfo_out_weight($_ins_id, $cate_id, $_prod_id, $_size_id) {
        $query = $this->where([['company_id', $_ins_id], ['category_id', $cate_id], ['product_id', $_prod_id], ['size_id', $_size_id], ['stock_type', STOCK_TYPE_OUT]]);
        $sum = $query->sum('net_weight');
        return !empty($sum) ? $sum : 0;
    }

    public function stock_weight_info($_ins_id, $cate_id, $_prod_id, $_size_id) {
        $_in = $this->stockinfo_in_weight($_ins_id, $cate_id, $_prod_id, $_size_id);
        $_out = $this->stockinfo_out_weight($_ins_id, $cate_id, $_prod_id, $_size_id);
        $sum = ($_in - $_out);
        return !empty($sum) ? $sum : 0;
    }

    public function sum_stock_in($conditions = []) {
        $query = $this->where('stock_type', STOCK_TYPE_IN);
        if (!empty($conditions) && is_array($conditions)) {
            foreach ($conditions as $_column => $_value) {
                $query->where("{$_column}", $_value);
            }
        }

        $sum = $query->sum('quantity');
        return !empty($sum) ? $sum : 0;
    }

    public function sum_stock_out($conditions = []) {
        $query = $this->where('stock_type', STOCK_TYPE_OUT);
        if (!empty($conditions) && is_array($conditions)) {
            foreach ($conditions as $_column => $_value) {
                $query->where("{$_column}", $_value);
            }
        }

        $sum = $query->sum('quantity');
        return !empty($sum) ? $sum : 0;
    }

    public function sum_value_stock_info($conditions = []) {
        $_in = $this->sum_stock_in($conditions);
        $_out = $this->sum_stock_out($conditions);
        $sum = ($_in - $_out);
        return !empty($sum) ? $sum : 0;
    }

    public function in_price($conditions = []) {
        $query = $this->where('stock_type', STOCK_TYPE_IN);
        if (!empty($conditions) && is_array($conditions)) {
            foreach ($conditions as $_column => $_value) {
                $query->where("{$_column}", $_value);
            }
        }

        $sum = $query->sum('price');
        return !empty($sum) ? $sum : 0;
    }

    public function in_qty($conditions = []) {
        $query = $this->where('stock_type', STOCK_TYPE_IN);
        if (!empty($conditions) && is_array($conditions)) {
            foreach ($conditions as $_column => $_value) {
                $query->where("{$_column}", $_value);
            }
        }

        $sum = $query->sum('quantity');
        return !empty($sum) ? $sum : 0;
    }

    public function avg_price($conditions = []) {
        $_in_price = $this->in_price($conditions);
        $_in_qty = $this->in_qty($conditions);
        $avg_price = round(($_in_price / $_in_qty), 3);
        return !empty($avg_price) ? $avg_price : 0;
    }

    public function prod_reg_production_rec($size_id, $date, $conditions = []) {
        $query = $this->where([['type', STOCK_TYPE_FINISH_STOCK], ['stock_type', STOCK_TYPE_IN]]);
        if (!empty($conditions) && is_array($conditions)) {
            foreach ($conditions as $_column => $_value) {
                $query->where("{$_column}", $_value);
            }
        }
        if (!empty($date)) {
            $query->where('date', $date);
        }
        if (!empty($size_id)) {
            $query->where('size_id', $size_id);
        }
        $sum = $query->sum('quantity');
        return !empty($sum) ? $sum : 0;
    }

    public function prod_reg_production_qty($size_id, $date, $conditions = []) {
        $query = $this->where([['type', STOCK_TYPE_PRODUCTION], ['stock_type', STOCK_TYPE_OUT]]);
        if (!empty($conditions) && is_array($conditions)) {
            foreach ($conditions as $_column => $_value) {
                $query->where("{$_column}", $_value);
            }
        }
        if (!empty($date)) {
            $query->where('date', $date);
        }
        if (!empty($size_id)) {
            $query->where('size_id', $size_id);
        }
        $sum = $query->sum('quantity');
        return !empty($sum) ? $sum : 0;
    }

    public function prod_reg_sale_qty($size_id, $date, $conditions = []) {
        $query = $this->where([['type', STOCK_TYPE_SALE], ['stock_type', STOCK_TYPE_OUT]]);
        if (!empty($conditions) && is_array($conditions)) {
            foreach ($conditions as $_column => $_value) {
                $query->where("{$_column}", $_value);
            }
        }
        if (!empty($date)) {
            $query->where('date', $date);
        }
        if (!empty($size_id)) {
            $query->where('size_id', $size_id);
        }
        $sum = $query->sum('quantity');
        return !empty($sum) ? $sum : 0;
    }

    public function prod_reg_purchase_qty($size_id, $date, $conditions = []) {
        $query = $this->where([['type', STOCK_TYPE_PURCHASE], ['stock_type', STOCK_TYPE_IN]]);
        if (!empty($conditions) && is_array($conditions)) {
            foreach ($conditions as $_column => $_value) {
                $query->where("{$_column}", $_value);
            }
        }
        if (!empty($date)) {
            $query->where('date', $date);
        }
        if (!empty($size_id)) {
            $query->where('size_id', $size_id);
        }
        $sum = $query->sum('quantity');
        return !empty($sum) ? $sum : 0;
    }

    public function prod_reg_opening_qty($size_id, $date, $conditions = []) {
        $query = $this->where([['type', STOCK_TYPE_OPENING], ['stock_type', STOCK_TYPE_IN]]);
        if (!empty($conditions) && is_array($conditions)) {
            foreach ($conditions as $_column => $_value) {
                $query->where("{$_column}", $_value);
            }
        }
        if (!empty($date)) {
            $query->where('date', $date);
        }
        if (!empty($size_id)) {
            $query->where('size_id', $size_id);
        }
        $sum = $query->sum('quantity');
        return !empty($sum) ? $sum : 0;
    }

    public function stock_dataset($conditions = []) {
        $query = $this->where('stock_type', STOCK_TYPE_IN);
        if (!empty($conditions) && is_array($conditions)) {
            foreach ($conditions as $_column => $_value) {
                $query->where("{$_column}", $_value);
            }
        }

        $dataset = $query->Orderby('id', 'DESC')->first();
        return $dataset;
    }

    public function stock_dataset_forUpdate($conditions = []) {
        $query = $this->where('stock_type', STOCK_TYPE_IN);
        if (!empty($conditions) && is_array($conditions)) {
            foreach ($conditions as $_column => $_value) {
                $query->where("{$_column}", $_value);
            }
        }

        $dataset = $query->first();
        return $dataset;
    }

    public function price_info($conditions = []) {
        $query = $this->query();
        if (!empty($conditions) && is_array($conditions)) {
            foreach ($conditions as $_column => $_value) {
                $query->where("{$_column}", $_value);
            }
        }
        $rate = $query->orderBy('date', 'DESC')->first()->rate;
        return $rate;
    }

    public function out_price($conditions = []) {
        $query = $this->where('stock_type', STOCK_TYPE_OUT);
        if (!empty($conditions) && is_array($conditions)) {
            foreach ($conditions as $_column => $_value) {
                $query->where("{$_column}", $_value);
            }
        }

        $sum = $query->sum('price');
        return !empty($sum) ? $sum : 0;
    }

    public function in_net_weight($conditions = []) {
        $query = $this->where('stock_type', STOCK_TYPE_IN);
        if (!empty($conditions) && is_array($conditions)) {
            foreach ($conditions as $_column => $_value) {
                $query->where("{$_column}", $_value);
            }
        }

        $sum = $query->sum('net_weight');
        return !empty($sum) ? $sum : 0;
    }

    public function out_net_weight($conditions = []) {
        $query = $this->where('stock_type', STOCK_TYPE_OUT);
        if (!empty($conditions) && is_array($conditions)) {
            foreach ($conditions as $_column => $_value) {
                $query->where("{$_column}", $_value);
            }
        }

        $sum = $query->sum('net_weight');
        return !empty($sum) ? $sum : 0;
    }

    public function avg_rate($conditions = []) {
        $_in_price = $this->in_price($conditions);
        $_out_price = $this->out_price($conditions);


        $_in_net_weight = $this->in_net_weight($conditions);
        $_out_net_weight = $this->out_net_weight($conditions);

        $remain_price = $_in_price - $_out_price;
        $remain_net_weight = $_in_net_weight - $_out_net_weight;

        $avg_price = round(($remain_price / $remain_net_weight), 2);
        return !empty($avg_price) ? $avg_price : 0;
    }

}
