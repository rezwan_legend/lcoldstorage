<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SalesOrder extends Model {

    protected $table = 'sales_order';
    public $timestamps = false;

    public static function model($className = __CLASS__) {
        $model = new $className();
        return $model;
    }

    public function tablename() {
        return $this->table;
    }

    public function items() {
        return $this->hasMany('App\Models\Ricemill\SalesItem', 'sales_id', 'id');
    }

    public function subhead() {
        return $this->belongsTo('App\Models\Account\SubHead', 'head_id');
    }

    public function company() {
        return $this->belongsTo('App\Models\Institute', 'company_id');
    }

    public function particular() {
        return $this->belongsTo('App\Models\Account\Particular', 'to_particular_id', 'id');
    }

    public function getOrderNo() {
        $pre_order = "order-";
        $data = $this->where([['is_deleted', 0]])->orderBy('order_no', 'desc')->first();
        $num = !empty($data) ? ($data->order_no + 1) : 1;
        return $num;
    }

    public function transactions() {
        return $this->hasMany('App\Models\Account\Transaction', 'sale_id', 'id')->where('institute_id', $this->company_id);
    }

    public function from_head() {
        return $this->belongsTo('App\Models\Account\Head', 'from_head_id');
    }

    public function from_subhead() {
        return $this->belongsTo('App\Models\Account\SubHead', 'from_subhead_id');
    }

    public function getInvoiceNo() {
        $data = $this->where('is_deleted', 0)->orderBy('invoice_no', 'desc')->first();
        $num = !empty($data) ? ($data->invoice_no + 1) : 1;
        return $num;
    }

    public function sum_order_qty($id) {
        $sum = VehiclePurchaseItem::where([['purchase_id', $id], ['is_deleted', 0]])->sum('quantity');
        return !empty($sum) ? $sum : 0;
    }

    public function count($id = null) {
        if (!empty($id)) {
            $data = SalesItem::where([['is_deleted', 0], ['sales_id', $id]])->count('id');
        }
        return !empty($data) ? doubleval($data) : 0;
    }

    public function total_price($id = null) {
        if (!empty($id)) {
            $data = SalesItem::where([['is_deleted', 0], ['sales_id', $id]])->sum('price');
        }
        return !empty($data) ? doubleval($data) : 0;
    }

    public function total_net_weight($id = null) {
        if (!empty($id)) {
            $data = SalesItem::where([['is_deleted', 0], ['sales_id', $id]])->sum('net_weight');
        }
        return !empty($data) ? doubleval($data) : 0;
    }

    public function stocks() {
        return $this->hasMany('App\Models\Ricemill\Stock', 'sales_order_id', 'id');
    }

    public function challanNo() {
        $chapre = 'Challan-';
        $data = $this->where('is_deleted', 0)->orderBy('id', 'desc')->first();
        $challanNo = !empty($data) ? $data->challan_no : '';
        $lastNo = !empty($challanNo) ? ltrim($challanNo, $chapre) : 0;
        $nextVal = $lastNo + 1;
        return $chapre . $nextVal;
    }

    public function oredrNo() {
        $chapre = 'Order-';
        $data = $this->where('is_deleted', 0)->orderBy('id', 'desc')->first();
        $oredrNo = !empty($data) ? $data->Order_no : '';
        $lastNo = !empty($oredrNo) ? ltrim($oredrNo, $chapre) : 0;
        $nextVal = $lastNo + 1;
        return $chapre . $nextVal;
    }

    public function total_qty($id = null) {
        $query = SalesItem::where('is_deleted', 0);
        if (!is_null($id)) {
            $query->where('sales_id', $id);
        }
        $_sum = $query->sum('quantity');
        return !empty($_sum) ? round($_sum, 2) : 0;
    }

    public function get_challanno($partyId = null) {
        $_query = $this->where('is_deleted', 0);
        if (!is_null($partyId)) {
            $_query->where('from_particular_id', $partyId);
        }
        $data = $_query->orderBy('id', 'desc')->first();
        $num = !empty($data) ? ($data->challan_no + 1) : 1;
        return $num;
    }

}
