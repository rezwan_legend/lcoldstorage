<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PurchaseItem extends Model {

    protected $table = 'purchase_order_item';
    public $timestamps = false;

    public static function model($className = __CLASS__) {
        $model = new $className();
        return $model;
    }

    public function tablename() {
        return $this->table;
    }

    public function purchase() {
        return $this->belongsTo('App\Models\Ricemill\Purchase', 'purchase_id');
    }

    public function product_type() {
        return $this->belongsTo('App\Models\ProductType', 'product_type_id');
    }

    public function category() {
        return $this->belongsTo('App\Models\Category', 'category_id');
    }

    public function product() {
        return $this->belongsTo('App\Models\Product', 'product_id');
    }

    public function productUnit() {
        return $this->belongsTo('App\Models\ProductUnit', 'unit_id');
    }

    public function UnitSize() {
        return $this->belongsTo('App\Models\UnitSize', 'size_id');
    }

    public function unit_price() {
        return $this->belongsTo('App\Models\PriceUnit', 'price_unit_id');
    }

    public function sum_value($id, $field) {
        $query = $this->where([['purchase_id', $id], ['is_deleted', 0]]);
        $sum = $query->sum("$field");
        return !empty($sum) ? $sum : 0;
    }

    public function sum_value_count($_field, $conditions = [], $dateArr = []) {
        $query = $this->where([['is_deleted', 0], ['process_status', STATUS_PROCESED]]);
        if (!empty($conditions) && count($conditions) > 0) {
            foreach ($conditions as $_column => $_value) {
                $query->where("{$_column}", $_value);
            }
        }
        if (!empty($dateArr) && is_array($dateArr)) {
            $query->whereBetween('order_date', [date_ymd($dateArr[0]), date_ymd($dateArr[1])]);
        }
        $sum = $query->sum($_field);
        return !empty($sum) ? $sum : 0;
    }

    public function last_rate($institute_id = null, $product = null) {
        $data = $this->where([['product_id', $product], ['institute_id', $institute_id]])->orderBy('id', 'desc')->first();
        return $data;
    }

    public function current_qty($challan_id = null) {
        return $this->where([['is_deleted', 0], ['challan_no', $challan_id]])->sum('quantity');
    }

    public function current_stock($order_no = null) {
        if (!empty($order_no)) {
            $data = ProductionItem::where([['is_deleted', 0], ['production_order_no', $order_no]])->sum('quantity');
        }
        return !empty($data) ? doubleval($data) : 0;
    }

}
