<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserLogininfo extends Model {

    protected $table = "users_logininfo";
    public $timestamps = false;

    public static function model($className = __CLASS__) {
        return new $className();
    }

    public function tablename() {
        return $this->table;
    }

    public function user() {
        return $this->belongsTo('App\Models\User', 'user_id');
    }

}
