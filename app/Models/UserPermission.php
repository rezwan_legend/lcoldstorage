<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserPermission extends Model {

    protected $table = 'users_permission';
    public $timestamps = false;

    public static function model($className = __CLASS__) {
        $model = new $className();
        return $model;
    }

    public function tablename() {
        return $this->table;
    }

    public function user() {
        return $this->belongsTo('App\Models\User', 'user_id');
    }

}
