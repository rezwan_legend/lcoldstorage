<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Auth;

class GeneralSetting extends Model {

    protected $table = 'general_setting';
    public $timestamps = false;

    public static function model($className = __CLASS__) {
        return new $className();
    }

    public static function get_setting($companyId = null) {
        $institute_id = !is_null($companyId) ? $companyId : Auth::user()->institute_id;
        $_data = GeneralSetting::where('institute_id', $institute_id)->first();
        if (empty($_data)) {
            $_record = GeneralSetting::find(1);
        } else {
            $_record = $_data;
        }
        return $_record;
    }

}
