<?php

namespace App\Models\Account;

use Illuminate\Database\Eloquent\Model;

class CheckRegister extends Model {

    protected $table = 'check_register';
    public $timestamps = false;

    public static function model($className = __CLASS__) {
        return new $className();
    }

    public function company() {
        return $this->belongsTo('App\Models\Institute', 'institute_id');
    }

    public function institute() {
        return $this->belongsTo('App\Models\Institute', 'institute_id');
    }

    public function transaction() {
        return $this->belongsTo('App\Models\Account\Transaction', 'transaction_id');
    }

    public function head_name($id) {
        $data = "";
        if (!empty($id)) {
            $data = Head::find($id);
        }
        $_name = !empty($data) ? $data->name : '';
        return $_name;
    }

    public function subhead_name($id) {
        $data = "";
        if (!empty($id)) {
            $data = SubHead::find($id);
        }
        $_name = !empty($data) ? $data->name : '';
        return $_name;
    }

    public function particular_name($id) {
        $data = "";
        if (!empty($id)) {
            $data = Particular::find($id);
        }
        $_name = !empty($data) ? $data->name : '';
        return $_name;
    }

    public function sum_alldebit_between_date($dates = [], $head_id = null, $institute_id = null) {
        $query = $this->where('is_deleted', 0);
        $query->where('status', ORDER_PENDING);
        if (!empty($dates) && is_array($dates)) {
            $query->whereBetween('register_date', [$dates[0], $dates[1]]);
        }
        if (!is_null($head_id)) {
            $query->where('to_head_id', $head_id);
        }
        if (!is_null($institute_id)) {
            $query->where('institute_id', $institute_id);
        }

        $sum = $query->sum('debit');
        return !empty($sum) ? round($sum, 2) : 0;
    }

    public function sum_allcredit_between_date($dates = [], $head_id = null, $institute_id = null) {
        $query = $this->where('is_deleted', 0);
        $query->where('status', ORDER_PENDING);
        if (!empty($dates) && is_array($dates)) {
            $query->whereBetween('register_date', [$dates[0], $dates[1]]);
        }
        if (!is_null($head_id)) {
            $query->where('from_head_id', $head_id);
        }
        if (!is_null($institute_id)) {
            $query->where('institute_id', $institute_id);
        }

        $sum = $query->sum('credit');
        return !empty($sum) ? round($sum, 2) : 0;
    }

}
