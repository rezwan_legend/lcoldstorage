<?php

namespace App\Models\Account;

use Illuminate\Database\Eloquent\Model;

class VoucherItem extends Model {

    protected $table = 'voucher_transaction';
    public $timestamps = false;

    public static function model($className = __CLASS__) {
        $model = new $className();
        return $model;
    }

    public function tablename() {
        return $this->table;
    }

    public function voucher() {
        return $this->belongsTo('App\Models\Account\Voucher', 'order_id');
    }

    public function head() {
        return $this->belongsTo('App\Models\Account\Head', 'head_id');
    }

    public function subhead() {
        return $this->belongsTo('App\Models\Account\SubHead', 'subhead_id');
    }

    public function particular() {
        return $this->belongsTo('App\Models\Account\Particular', 'particular_id');
    }

}
