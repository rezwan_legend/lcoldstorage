<?php

namespace App\Models\Account;

use Illuminate\Database\Eloquent\Model;

class Voucher extends Model {

    protected $table = 'voucher';
    public $timestamps = false;

    public static function model($className = __CLASS__) {
        $model = new $className();
        return $model;
    }

    public function tablename() {
        return $this->table;
    }

    public function items() {
        return $this->hasMany('App\Models\Account\VoucherItem', 'order_id', 'id');
    }

    public function company() {
        return $this->belongsTo('App\Models\Institute', 'company_id');
    }

    public function transactions() {
        return $this->hasMany('App\Models\Account\Transaction', 'voucher_id', 'id')->where('institute_id', $this->company_id);
    }

    public function total_price($id = null) {
        $query = VoucherItem::where('is_deleted', 0);
        if (!is_null($id)) {
            $query->where('order_id', $id);
        }
        $_sum = $query->sum('debit_amount');
        return !empty($_sum) ? round($_sum, 2) : 0;
    }

}
