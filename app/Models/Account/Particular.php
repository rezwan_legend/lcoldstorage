<?php

namespace App\Models\Account;

use App\Models\Account\Transaction;
use Illuminate\Database\Eloquent\Model;

class Particular extends Model {

    protected $table = 'particulars';
    public $timestamps = false;

    public static function model($className = __CLASS__) {
        return new $className();
    }

    public function company() {
        return $this->belongsTo('App\Models\Institute', 'institute_id');
    }

    public function institute() {
        return $this->belongsTo('App\Models\Institute', 'institute_id');
    }

    public function head() {
        return $this->belongsTo('App\Models\Account\Head', 'head_id');
    }

    public function subhead() {
        return $this->belongsTo('App\Models\Account\SubHead', 'subhead_id');
    }

    public function account() {
        return $this->belongsTo('App\Models\FillingStation\Account', 'account_id');
    }

    public function customer() {
        return $this->belongsTo('App\Models\FillingStation\Customer', 'customer_id');
    }

    public function supplier() {
        return $this->belongsTo('App\Models\FillingStation\Supplier', 'supplier_id');
    }

    public function particularBalance($id) {
        $modelTrans = new Transaction();
        $data = $modelTrans->sumPartBalance($id);
        return !empty($data) ? doubleval($data) : 0;
    }

    public function particularPreviousBalance($company_id, $particular_id, $sale_id = null) {
        $modelTrans = new Transaction();
        $data = $modelTrans->sumParticularBalanceBeforeInvoice($company_id, $particular_id, $sale_id);
        return !empty($data) ? doubleval($data) : 0;
    }

    public function code_number() {
        $query = $this->max("code");
        $inv_no = $query;
        return !empty($inv_no) ? ($inv_no + 1) : random_number(5);
    }

    public function name_address() {
        return $this->name . "(" . $this->address . ")";
    }

    public function particularName($partyId = null) {
        $query = $this->where([['is_deleted', 0]]);

        if (!is_null($partyId)) {
            $query->where('id', $partyId);
        }
        $query->orderBy("name", "ASC");
        $data = $query->frist();
        $name_address = $data->name . "(" . $data->address . ")";
        return $name_address;
    }

    public function getList($_company = null, $_subhead = null) {
        $query = $this->where([['is_deleted', 0]]);
        if (!is_null($_company)) {
            $query->where('institute_id', $_company);
        }
        if (!is_null($_subhead)) {
            $query->where('subhead_id', $_subhead);
        }
        $query->orderBy("name", "ASC");
        $_dataset = $query->get();
        return $_dataset;
    }

    public function particular_balance() {
        return $this->hasOne('App\Models\ParticularBalance', 'particular_id', 'id');
    }

}
