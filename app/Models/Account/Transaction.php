<?php

namespace App\Models\Account;

use Illuminate\Database\Eloquent\Model;
use App\Models\Construction\Project;

class Transaction extends Model {

    protected $table = 'transactions';
    public $timestamps = false;

    public static function model($className = __CLASS__) {
        return new $className();
    }

    public function company() {
        return $this->belongsTo('App\Models\Institute', 'institute_id');
    }

    public function head_name($id) {
        $data = "";
        if (!empty($id)) {
            $data = Head::find($id);
        }
        $_name = !empty($data) ? $data->name : '';
        return $_name;
    }

    public function subhead_name($id) {
        $data = "";
        if (!empty($id)) {
            $data = SubHead::find($id);
        }
        $_name = !empty($data) ? $data->name : '';
        return $_name;
    }

    public function particular_name($id) {
        $data = "";
        if (!empty($id)) {
            $data = Particular::find($id);
        }
        $_name = !empty($data) ? $data->name : '';
        return $_name;
    }

    public function particularAddress($id) {
        $data = "";
        if (!empty($id)) {
            $data = Particular::find($id);
        }
        $_name = !empty($data) ? $data->address : '';
        return $_name;
    }

    public function vehicle_name($id) {
        $data = "";
        if (!empty($id)) {
            $data = \App\Models\Transport\Vehicle::find($id);
        }
        $vehicle_no = !empty($data) ? $data->vehicle_no : '';
        return $vehicle_no;
    }

    //  get first transaction date by project
    public function firstTransactionDateByProject($project_id = null) {
        $data = '';
        if (!is_null($project_id)) {
            $data = $this->where('project_id', $project_id)->orderBy('date', 'ASC')->first();
        }
        return !empty($data) ? $data->date : '';
    }

    public function institute() {
        return $this->belongsTo('App\Models\Institute', 'institute_id');
    }

    public function cr_head() {
        return $this->belongsTo('App\Models\Account\Head', 'cr_head_id');
    }

    public function cr_subhead() {
        return $this->belongsTo('App\Models\Account\SubHead', 'cr_subhead_id');
    }

    public function cr_particular() {
        return $this->belongsTo('App\Models\Account\Particular', 'cr_particular_id');
    }

    public function diarparty_production_item() {
        return $this->belongsTo('App\Models\Ricemill\DiarpartyProductionItem', 'ricemill_diarparty_production_order_item_id');
    }

    public function dr_head() {
        return $this->belongsTo('App\Models\Account\Head', 'dr_head_id');
    }

    public function dr_subhead() {
        return $this->belongsTo('App\Models\Account\SubHead', 'dr_subhead_id');
    }

    public function dr_particular() {
        return $this->belongsTo('App\Models\Account\Particular', 'dr_particular_id');
    }

    public function salesOrderNo() {
        return $this->belongsTo('App\Models\Trade\SalesOrder', 'sales_order_id');
    }

    public function salesChallanNo() {
        return $this->belongsTo('App\Models\Trade\SaleChallan', 'sale_challan_id');
    }

    // start, end date
    public function start_end_date($_end = false) {
        $query = $this->orderBy("date", "ASC");
        if ($_end == true) {
            $query = $this->orderBy("date", "DESC");
        }
        $query->limit(1);
        $_data = $query->first();
        return !empty($_data) ? date("Y-m-d", strtotime($_data->date)) : "";
    }

    public function headDebit($head_id, $from_date = NULL, $end_date = NULL) {
        $query = $this->where('is_deleted', 0);
        $query->where([['dr_head_id', $head_id], ['dr_subhead_id', NULL], ['dr_particular_id', NULL]]);
        if (!is_null($from_date)) {
            $query->whereBetween('date', [$from_date, $end_date]);
        }
        $data = $query->sum('debit');
        return !empty($data) ? round($data, 2) : 0;
    }

    public function headCredit($head_id, $from_date = NULL, $end_date = NULL) {
        $query = $this->where('is_deleted', 0);
        $query->where([['cr_head_id', $head_id], ['cr_subhead_id', NULL], ['cr_particular_id', NULL]]);
        if (!is_null($from_date)) {
            $query->whereBetween('date', [$from_date, $end_date]);
        }
        $data = $query->sum('credit');
        return !empty($data) ? round($data, 2) : 0;
    }

    public function sumHeadBalance($head_id, $from_date = NULL, $end_date = NULL) {
        if (!is_null($from_date)) {
            $data = $this->headDebit($head_id, $from_date, $end_date) - $this->headCredit($head_id, $from_date, $end_date);
        } else {
            $data = $this->headDebit($head_id) - $this->headCredit($head_id);
        }
        return !empty($data) ? round($data, 2) : 0;
    }

    public function subheadDebit($subhead_id, $from_date = NULL, $end_date = NULL) {
        $query = $this->where('is_deleted', 0);
        $query->where([['dr_subhead_id', $subhead_id], ['dr_particular_id', NULL]]);
        if (!is_null($from_date)) {
            $query->whereBetween('date', [$from_date, $end_date]);
        }
        $data = $query->sum('debit');
        return !empty($data) ? round($data, 2) : 0;
    }

    public function subheadCredit($subhead_id, $from_date = NULL, $end_date = NULL) {
        $query = $this->where('is_deleted', 0);
        $query->where([['cr_subhead_id', $subhead_id], ['cr_particular_id', NULL]]);
        if (!is_null($from_date)) {
            $query->whereBetween('date', [$from_date, $end_date]);
        }
        $data = $query->sum('credit');
        return !empty($data) ? round($data, 2) : 0;
    }

    public function sumsubHeadBalance($subhead_id, $from_date = NULL, $end_date = NULL) {
        if (!is_null($from_date)) {
            $data = $this->subheadDebit($subhead_id, $from_date, $end_date) - $this->subheadCredit($subhead_id, $from_date, $end_date);
        } else {
            $data = $this->subheadDebit($subhead_id) - $this->subheadCredit($subhead_id);
        }
        return !empty($data) ? round($data, 2) : 0;
    }

    public function sumDebit($head_id = null) {
        $query = $this->where('is_deleted', 0);
        if (!is_null($head_id)) {
            $query->where('dr_head_id', $head_id);
        }
        $data = $query->sum('debit');
        return !empty($data) ? round($data, 2) : 0;
    }

    public function sumDebitByDate($date, $head_id = null) {
        $query = $this->where('is_deleted', 0);
        if (!is_null($head_id)) {
            $query->where('head_id', $head_id);
        }
        $data = $query->sum('debit');
        return !empty($data) ? round($data, 2) : 0;
    }

    public function sumCredit($head_id = null) {
        $query = $this->where('is_deleted', 0);
        if (!is_null($head_id)) {
            $query->where('cr_head_id', $head_id);
        }
        $data = $query->sum('credit');
        return !empty($data) ? round($data, 2) : 0;
    }

    public function sumBalance($head_id = null) {
        if (!is_null($head_id)) {
            $data = $this->sumDebit($head_id) - $this->sumCredit($head_id);
        }
        return !empty($data) ? round($data, 2) : 0;
    }

    public function sumSubDebit($subhead_id = null, $_company_id = null) {
        $query = $this->where('is_deleted', 0);
        if (!is_null($subhead_id)) {
            $query->where('dr_subhead_id', $subhead_id);
        }
        if (!is_null($_company_id)) {
            $query->where('institute_id', $_company_id);
        }
        $data = $query->sum('debit');
        return !empty($data) ? round($data, 2) : 0;
    }

    public function sumSubCredit($subhead_id = null, $_company_id = null) {
        $query = $this->where('is_deleted', 0);
        if (!is_null($subhead_id)) {
            $query->where('cr_subhead_id', $subhead_id);
        }
        if (!is_null($_company_id)) {
            $query->where('institute_id', $_company_id);
        }
        $data = $query->sum('credit');
        return !empty($data) ? round($data, 2) : 0;
    }

    public function sumSubBalance($subhead_id = null, $_company_id = null) {
        if (!is_null($subhead_id)) {
            $data = $this->sumSubDebit($subhead_id, $_company_id) - $this->sumSubCredit($subhead_id, $_company_id);
        }
        return !empty($data) ? round($data, 2) : 0;
    }

    public function sumPartDebit($particular_id = null) {
        $query = $this->where('is_deleted', 0);
        if (!is_null($particular_id)) {
            $query->where('dr_particular_id', $particular_id);
        }
        $data = $query->sum('debit');
        return !empty($data) ? round($data, 2) : 0;
    }

    public function sumParticularDebitBeforeInvoice($company_id, $particular_id, $sale_id = null) {
        $query = $this->where([['is_deleted', 0], ['dr_particular_id', $particular_id], ['institute_id', $company_id]]);
        $query->orWhere([['is_deleted', 0], ['dr_particular_id', $particular_id], ['institute_id', $company_id]]);
        if (!is_null($sale_id)) {
            //$query->where('sale_challan_id', '!=', $sale_id);
            $query->where('id', '<', $this->get_pk_from_sale($sale_id));
        }
        $data = $query->sum('debit');
        return !empty($data) ? round($data, 2) : 0;
    }

    public function sumParticularCreditBeforeInvoice($company_id, $particular_id, $sale_id = null) {
        $query = $this->where([['is_deleted', 0], ['dr_particular_id', $particular_id], ['institute_id', $company_id]]);
        $query->orWhere([['is_deleted', 0], ['cr_particular_id', $particular_id], ['institute_id', $company_id]]);
        if (!is_null($sale_id)) {
            //$query->where('sale_challan_id', '!=', $sale_id);
            $query->where('id', '<', $this->get_pk_from_sale($sale_id));
        }
        $data = $query->sum('credit');
        return !empty($data) ? round($data, 2) : 0;
    }

    public function sumParticularBalanceBeforeInvoice($company_id, $particular_id, $sale_id = null) {
        $data = $this->sumParticularDebitBeforeInvoice($company_id, $particular_id, $sale_id) - $this->sumParticularCreditBeforeInvoice($company_id, $particular_id, $sale_id);
        return !empty($data) ? round($data, 2) : 0;
    }

    public function get_pk_from_sale($saleId) {
        $_record = $this->where([['is_deleted', 0], ['sale_challan_id', $saleId]])->first();
        return !empty($_record) ? $_record->id : null;
    }

    public function sum_particular_debit_before_invoice($company_id, $particular_id, $sale_id = null) {
        $query = $this->where([['is_deleted', 0], ['institute_id', $company_id], ['dr_particular_id', $particular_id]]);
        if (!is_null($sale_id)) {
            $query->whereNotNull('sale_challan_id')->where('sale_challan_id', '<', $sale_id);
        }
        $query->orderBy('id', 'ASC');
        $data = $query->sum('debit');
        return !empty($data) ? round($data, 2) : 0;
    }

    public function sum_particular_credit_before_invoice($company_id, $particular_id, $sale_id = null) {
        $query = $this->where([['is_deleted', 0], ['institute_id', $company_id], ['cr_particular_id', $particular_id]]);
        if (!is_null($sale_id)) {
            $query->whereNotNull('sale_challan_id')->where('sale_challan_id', '<', $sale_id);
            //$query->whereNotNull('sale_challan_id')->where('id', '<', $this->get_pk_from_sale($sale_id));
        }
        $query->orderBy('id', 'ASC');
        $data = $query->sum('credit');
        return !empty($data) ? round($data, 2) : 0;
    }

    public function sum_particular_balance_before_invoice($company_id, $particular_id, $sale_id = null) {
        $data = $this->sum_particular_debit_before_invoice($company_id, $particular_id, $sale_id) - $this->sum_particular_credit_before_invoice($company_id, $particular_id, $sale_id);
        return !empty($data) ? round($data, 2) : 0;
    }

    public function sumPartCredit($particular_id = null) {
        $query = $this->where('is_deleted', 0);
        if (!is_null($particular_id)) {
            $query->where('cr_particular_id', $particular_id);
        }
        $data = $query->sum('credit');
        return !empty($data) ? round($data, 2) : 0;
    }

    public function sumPartBalance($particular_id = null) {
        if (!is_null($particular_id)) {
            $data = $this->sumPartDebit($particular_id) - $this->sumPartCredit($particular_id);
        }
        return !empty($data) ? round($data, 2) : 0;
    }

    public function particularDebit($particularId, $dateArr = []) {
        $query = $this->where([['cr_particular_id', $particularId], ['is_deleted', 0]]);
        if (!empty($dateArr) && is_array($dateArr)) {
            $query->whereBetween('date', [date_ymd($dateArr[0]), date_ymd($dateArr[1])]);
        }
        $sum = $query->sum('debit');
        return !empty($sum) ? round($sum, 2) : 0;
    }

    public function particularCredit($particularId, $dateArr = []) {
        $query = $this->where([['dr_particular_id', $particularId], ['is_deleted', 0]]);
        if (!empty($dateArr) && is_array($dateArr)) {
            $query->whereBetween('date', [date_ymd($dateArr[0]), date_ymd($dateArr[1])]);
        }
        $sum = $query->sum('credit');
        return !empty($sum) ? round($sum, 2) : 0;
    }

    public function particularBalance($particularId, $dateArr = []) {
        $_dbt = $this->particularDebit($particularId, $dateArr);
        $_cdt = $this->particularCredit($particularId, $dateArr);
        $sum = ($_dbt - $_cdt);
        return !empty($sum) ? round($sum, 2) : 0;
    }

    public function head_debit_between_date($head_id, $from_date = null, $end_date = null) {
        $query = $this->where('is_deleted', 0);
        if (!is_null($head_id)) {
            $query->where('dr_head_id', $head_id)->orWhere('cr_head_id', $head_id);
        }
        $query->whereBetween('date', [$from_date, $end_date]);
        $data = $query->sum('debit');
        return !empty($data) ? round($data, 2) : 0;
    }

    public function sumDebitBetweenDate($from_date, $end_date, $head_id = null) {
        $query = $this->where('is_deleted', 0);
        $query->whereBetween('date', [$from_date, $end_date]);
        if (!is_null($head_id)) {
            $query->where('dr_head_id', $head_id);
        }
        $data = $query->sum('debit');
        return !empty($data) ? round($data, 2) : 0;
    }

    public function sumCreditBetweenDate($from_date, $end_date, $head_id = null) {
        $query = $this->where('is_deleted', 0);
        $query->whereBetween('date', [$from_date, $end_date]);
        if (!is_null($head_id)) {
            $query->where('cr_head_id', $head_id);
        }

        $data = $query->sum('credit');
        return !empty($data) ? round($data, 2) : 0;
    }

    public function sumCreditReceiveVoucher($from_date, $end_date, $head_id = null) {
        $query = $this->where('is_deleted', 0);
        $query->whereBetween('date', [$from_date, $end_date]);
        if (!is_null($head_id)) {
            $query->where([['voucher_type', RECEIVE_VOUCHER], ['cr_head_id', $head_id]]);
        } else {
            $query->where('voucher_type', RECEIVE_VOUCHER);
        }

        $data = $query->sum('credit');
        return !empty($data) ? round($data, 2) : 0;
    }

    public function sumDebitPaymentVoucher($from_date, $end_date, $head_id = null) {
        $query = $this->where('is_deleted', 0);
        $query->whereBetween('date', [$from_date, $end_date]);
        if (!is_null($head_id)) {
            $query->where([['voucher_type', PAYMENT_VOUCHER], ['dr_head_id', $head_id]]);
        } else {
            $query->where('voucher_type', PAYMENT_VOUCHER);
        }

        $data = $query->sum('debit');
        return !empty($data) ? round($data, 2) : 0;
    }

    public function totalPaymentByDate_Old($date = null) {
        $query = $this->where('is_deleted', 0);
        if (is_null($date)) {
            $dataset = $query->where([['voucher_type', 'Payment Voucher'], ['date', date('Y-m-d')]])->get();
        } else {
            $dataset = $query->where([['voucher_type', 'Payment Voucher'], ['date', $date]])->get();
        }
        return $dataset;
    }

    public function totalReceiveByDate_Old($date = null) {
        $query = $this->where('is_deleted', 0);
        if (is_null($date)) {
            $dataset = $query->where([['voucher_type', 'Receive Voucher'], ['date', date('Y-m-d')]])->get();
        } else {
            $dataset = $query->where([['voucher_type', 'Receive Voucher'], ['date', $date]])->get();
        }
        return $dataset;
    }

    public function totalPaymentByDate($date = null, $head = null, $institute_id) {
        $query = $this->where('institute_id', $institute_id)->where('is_deleted', 0);
        if (is_null($date)) {
            if (is_null($head)) {
                $query = $query->where([['voucher_type', 'Payment Voucher'], ['date', date('Y-m-d')]]);
            } else {
                $query = $query->where([['voucher_type', 'Payment Voucher'], ['date', date('Y-m-d')], ['cr_head_id', $head]]);
            }
        } else {
            if (is_null($head)) {
                $query = $query->where([['voucher_type', 'Payment Voucher'], ['date', $date]]);
            } else {
                $query = $query->where([['voucher_type', 'Payment Voucher'], ['date', $date], ['cr_head_id', $head]]);
            }
        }
        return $query->get();
    }

    public function allPaymentByDate($date = null, $payment_method = null, $institute_id = null) {
        $query = $this->where('type', 'C')->where('is_deleted', 0);

        if (!empty($institute_id)) {
            $query = $query->where('institute_id', $institute_id)->where('is_deleted', 0);
        }
        if (!empty($payment_method)) {
            $query = $query->where('payment_method', $payment_method);
        }

        if (!empty($date)) {
            $query = $query->where('date', $date);
        }

        return $query->get();
    }

    public function SummaryReceiveByDate($date = null, $institute_id = null) {
        $query = $this->where('type', 'D')->where('is_deleted', 0);


        if (!empty($institute_id)) {
            $query = $query->where('institute_id', $institute_id)->where('is_deleted', 0);
        }
        if (!empty($date)) {
            $query = $query->where('date', $date);
        }

        $query->groupBy('cr_particular_id');

        return $query->get();
    }

    public function summaryPaymentByDate($date = null, $institute_id = null) {
        $query = $this->where('type', 'C')->where('is_deleted', 0);

        if (!empty($institute_id)) {
            $query = $query->where('institute_id', $institute_id)->where('is_deleted', 0);
        }
        if (!empty($date)) {
            $query = $query->where('date', $date);
        }

        $query->groupBy('dr_particular_id');

        return $query->get();
    }

    public function summary_sum_particular_receive($cr_particular_id) {
        $query = $this->where('is_deleted', 0);
        $query->where('cr_particular_id', $cr_particular_id);
        $data = $query->sum('credit');
        return !empty($data) ? round($data, 2) : 0;
    }

    public function summary_sum_particular_payment($dr_particular_id) {
        $query = $this->where('is_deleted', 0);
        $query->where('dr_particular_id', $dr_particular_id);
        $data = $query->sum('debit');
        return !empty($data) ? round($data, 2) : 0;
    }

    public function allReceiveByDate($date = null, $payment_method = null, $institute_id = null) {
        $query = $this->where('type', 'D')->where('is_deleted', 0);

        if (!empty($institute_id)) {
            $query = $query->where('institute_id', $institute_id)->where('is_deleted', 0);
        }
        if (!empty($payment_method)) {
            $query = $query->where('payment_method', $payment_method);
        }
        if (!empty($date)) {
            $query = $query->where('date', $date);
        }

        return $query->get();
    }

    public function totalReceiveByDate($date = null, $head = null, $institute_id) {
        $query = $this->where('institute_id', $institute_id)->where('is_deleted', 0);
        if (is_null($date)) {
            if (is_null($head)) {
                $query = $query->where([['voucher_type', 'Receive Voucher'], ['date', date('Y-m-d')]]);
            } else {
                $query = $query->where([['voucher_type', 'Receive Voucher'], ['date', date('Y-m-d')], ['dr_head_id', $head]]);
            }
        } else {
            if (is_null($head)) {
                $query = $query->where([['voucher_type', 'Receive Voucher'], ['date', $date]]);
            } else {
                $query = $query->where([['voucher_type', 'Receive Voucher'], ['date', $date], ['dr_head_id', $head]]);
            }
        }
        return $query->get();
    }

    public function totalPaymentBetweenDate($from_date, $end_date, $head_id = null, $institute_id) {
        $query = $this->where('institute_id', $institute_id)->where('is_deleted', 0);
        $query->whereBetween('date', [$from_date, $end_date]);
        if (!is_null($head_id)) {
            $query->where([['voucher_type', PAYMENT_VOUCHER], ['cr_head_id', $head_id]]);
        } else {
            $query->where('voucher_type', PAYMENT_VOUCHER);
        }
        return $query;
    }

    public function totalReceiveBetweenDate($from_date, $end_date, $head_id = null, $institute_id) {
        $query = $this->where('institute_id', $institute_id)->where('is_deleted', 0);
        $query->whereBetween('date', [$from_date, $end_date]);
        if (!is_null($head_id)) {
            $query->where([['voucher_type', RECEIVE_VOUCHER], ['dr_head_id', $head_id]]);
        } else {
            $query->where('voucher_type', RECEIVE_VOUCHER);
        }
        return $query;
    }

    public function sumPaymentByDate($from_date, $end_date, $head_id = null, $institute_id) {
        $query = $this->where('institute_id', $institute_id)->where('is_deleted', 0);
        $query->whereBetween('date', [$from_date, $end_date]);
        if (!is_null($head_id)) {
            $query->where([['voucher_type', PAYMENT_VOUCHER], ['cr_head_id', $head_id]]);
        } else {
            $query->where('voucher_type', PAYMENT_VOUCHER);
        }

        $data = $query->sum('credit');
        return !empty($data) ? round($data, 2) : 0;
    }

    public function sumReceiveByDate($from_date, $end_date, $head_id = null, $institute_id) {
        $query = $this->where('institute_id', $institute_id)->where('is_deleted', 0);
        $query->whereBetween('date', [$from_date, $end_date]);
        if (!is_null($head_id)) {
            $query->where([['voucher_type', RECEIVE_VOUCHER], ['dr_head_id', $head_id]]);
        } else {
            $query->where('voucher_type', RECEIVE_VOUCHER);
        }

        $data = $query->sum('debit');
        return !empty($data) ? round($data, 2) : 0;
    }

    public function allSumPaymentByDate($from_date, $end_date, $payment_method = null, $institute_id = null) {
        $query = $this->where('type', 'C')->where('is_deleted', 0);

        if (!empty($end_date)) {
            $query->whereBetween('date', [$from_date, $end_date]);
        }
        if (!empty($payment_method)) {
            $query->where('payment_method', $payment_method);
        }
        if (!empty($institute_id)) {
            $query->where('institute_id', $institute_id);
        }
        $data = $query->sum('credit');
        return !empty($data) ? round($data, 2) : 0;
    }

    public function allSumReceiveByDate($from_date, $end_date, $payment_method = null, $institute_id = null) {
        $query = $this->where('type', 'D')->where('is_deleted', 0);
        if (!empty($end_date)) {
            $query->whereBetween('date', [$from_date, $end_date]);
        }
        if (!empty($payment_method)) {
            $query = $query->where('payment_method', $payment_method);
        }
        if (!empty($institute_id)) {
            $query->where('institute_id', $institute_id);
        }
        $data = $query->sum('debit');
        return !empty($data) ? round($data, 2) : 0;
    }

    public function sumSubPaymentVoucherBetweenDate($from_date, $end_date, $head_id = null) {
        $query = $this->where('is_deleted', 0);
        $query->whereBetween('date', [$from_date, $end_date]);
        if (!is_null($head_id)) {
            $query->where([['voucher_type', PAYMENT_VOUCHER], ['cr_subhead_id', $head_id]]);
        } else {
            $query->where('voucher_type', PAYMENT_VOUCHER);
        }

        $data = $query->sum('credit');
        return !empty($data) ? round($data, 2) : 0;
    }

    public function sumSubReceiveVoucherBetweenDate($from_date, $end_date, $head_id = null) {
        $query = $this->where('is_deleted', 0);
        $query->whereBetween('date', [$from_date, $end_date]);
        if (!is_null($head_id)) {
            $query->where([['voucher_type', RECEIVE_VOUCHER], ['dr_subhead_id', $head_id]]);
        } else {
            $query->where('voucher_type', RECEIVE_VOUCHER);
        }

        $data = $query->sum('debit');
        return !empty($data) ? round($data, 2) : 0;
    }

    public function debitClosingBySubHead($id) {
        $data = '';
        $sum_debit = $this->where('dr_subhead_id', $id)->where('is_deleted', 0)->sum('debit');
        $sum_credit = $this->where('cr_subhead_id', $id)->where('is_deleted', 0)->sum('credit');
        if ($sum_debit > $sum_credit) {
            $result = $sum_debit - $sum_credit;
        } else {
            $result = $data;
        }
        return $result;
    }

    public function creditClosingBySubHead($id) {
        $data = '';
        $sum_debit = $this->where('dr_subhead_id', $id)->where('is_deleted', 0)->sum('debit');
        $sum_credit = $this->where('cr_subhead_id', $id)->where('is_deleted', 0)->sum('credit');
        if ($sum_credit > $sum_debit) {
            $result = $sum_credit > $sum_debit;
        } else {
            $result = $data;
        }
        return $result;
    }

    public function sum_alldebit_between_date($dates = [], $head_id = null, $institute_id = null) {
        $query = $this->where('is_deleted', 0);
        $query->whereIn('voucher_type', [PAYMENT_VOUCHER, RECEIVE_VOUCHER]);
        $query->where('payment_method', PAYMENT_CASH);
        if (!empty($dates) && is_array($dates)) {
            $query->whereBetween('date', [$dates[0], $dates[1]]);
        }
        if (!is_null($head_id)) {
            $query->where('dr_head_id', $head_id);
        }
        if (!is_null($institute_id)) {
            $query->where('institute_id', $institute_id);
        }

        $sum = $query->sum('debit');
        return !empty($sum) ? round($sum, 2) : 0;
    }

    public function sum_allcredit_between_date($dates = [], $head_id = null, $institute_id = null) {
        $query = $this->where('is_deleted', 0);
        $query->whereIn('voucher_type', [PAYMENT_VOUCHER, RECEIVE_VOUCHER]);
        $query->where('payment_method', PAYMENT_CASH);
        if (!empty($dates) && is_array($dates)) {
            $query->whereBetween('date', [$dates[0], $dates[1]]);
        }
        if (!is_null($head_id)) {
            $query->where('cr_head_id', $head_id);
        }
        if (!is_null($institute_id)) {
            $query->where('institute_id', $institute_id);
        }

        $sum = $query->sum('credit');
        return !empty($sum) ? round($sum, 2) : 0;
    }

    public function sum_debit_bydate($date, $head_id = null) {
        $_dt = date("Y-m-d", strtotime($date));
        $query = $this->where('is_deleted', 0);
        $query->whereIn('voucher_type', [PAYMENT_VOUCHER]);
        $query->where('payment_method', PAYMENT_CASH);
        if (!is_null($head_id)) {
            $query->where('dr_subhead_id', $head_id);
        }
        $query->where('date', $_dt);
        $sum = $query->sum('debit');
        return !empty($sum) ? round($sum, 2) : 0;
    }

    public function sum_credit_bydate($date, $head_id = null) {
        $_dt = date("Y-m-d", strtotime($date));
        $query = $this->where('is_deleted', 0);
        $query->whereIn('voucher_type', [RECEIVE_VOUCHER]);
        $query->where('payment_method', PAYMENT_CASH);
        if (!is_null($head_id)) {
            $query->where('cr_subhead_id', $head_id);
        }
        $query->where('date', $_dt);
        $sum = $query->sum('credit');
        return !empty($sum) ? round($sum, 2) : 0;
    }

    // construction option
    public function project_name($id) {
        $data = "";
        if (!empty($id)) {
            $data = Project::find($id);
        }
        $_name = !empty($data) ? $data->project_name : '';
        return $_name;
    }

    public function consTotalPaymentByDate($from_date = null, $to_date = null, $license, $project, $department = null, $subhead = null, $institute_id) {
        $query = $this->where('institute_id', $institute_id)->where('is_deleted', 0);
        $query->where('type', 'C');
        if (!empty($license)) {
            $query->where('license_id', $license);
        }
        if (!empty($department)) {
            $query->where('department_id', $department);
        }
        if (!is_null($from_date) && !is_null($to_date)) {
            $query->whereBetween('date', [date_ymd($from_date), date_ymd($to_date)]);
        }
        if (!empty($project)) {
            $query->where('project_id', $project);
        }
        if (!empty($subhead)) {
            $query->where('dr_subhead_id', $subhead);
        }
        return $query->orderBy('date', 'ASC')->get();
    }

    public function consTotalReceiveByDate($from_date = null, $to_date = null, $license, $project, $department = null, $subhead = null, $institute_id) {
        $query = $this->where('institute_id', $institute_id)->where('is_deleted', 0);
        $query->where('type', 'D');
        if (!empty($license)) {
            $query->where('license_id', $license);
        }
        if (!empty($department)) {
            $query->where('department_id', $department);
        }
        if (!is_null($from_date) && !is_null($to_date)) {
            $query->whereBetween('date', [date_ymd($from_date), date_ymd($to_date)]);
        }
        if (!empty($project)) {
            $query->where('project_id', $project);
        }
        if (!empty($subhead)) {
            $query->where('cr_subhead_id', $subhead);
        }
        return $query->orderBy('date', 'ASC')->get();
    }

    public function consSumPaymentByDate($from_date = null, $to_date = null, $license, $project, $department = null, $subhead = null, $institute_id) {
        $query = $this->where('institute_id', $institute_id)->where('is_deleted', 0);
        $query->where('type', 'C');

        if (!empty($license)) {
            $query->where('license_id', $license);
        }
        if (!empty($department)) {
            $query->where('department_id', $department);
        }
        if (!empty($from_date) || !empty($to_date)) {
            $query->whereBetween('date', [$from_date, $to_date]);
        }
        if (!empty($project)) {
            $query->where('project_id', $project);
        }

        if (!empty($subhead)) {
            $query->where('dr_subhead_id', $subhead);
        }

        $data = $query->sum('credit');
        return !empty($data) ? round($data, 2) : 0;
    }

    public function consSumReceiveByDate($from_date = null, $to_date = null, $license, $project, $department = null, $subhead = null, $institute_id) {
        $query = $this->where('institute_id', $institute_id)->where('is_deleted', 0);
        $query->where('type', 'D');

        if (!empty($license)) {
            $query->where('license_id', $license);
        }
        if (!empty($department)) {
            $query->where('department_id', $department);
        }
        if (!empty($to_date)) {
            $query->whereBetween('date', [$from_date, $to_date]);
        }
        if (!empty($project)) {
            $query->where('project_id', $project);
        }
        if (!empty($subhead)) {
            $query->where('cr_subhead_id', $subhead);
        }

        $data = $query->sum('debit');
        return !empty($data) ? round($data, 2) : 0;
    }

    public function construction_sum_subhead_debit($subhead_id, $department_id = null, $license_id = null, $project_id = null, $_dates = []) {
        $query = $this->where('is_deleted', 0);
        $query->where('institute_id', CONS);
        $query->where('type', 'D');
        $query->where('cr_subhead_id', $subhead_id);
        if (!empty($project_id)) {
            $query->where('project_id', $project_id);
        }
        if (!empty($license_id)) {
            $query->where('license_id', $license_id);
        }
        if (!empty($department_id)) {
            $query->where('department_id', $department_id);
        }
        if (!empty($_dates) && is_array($_dates)) {
            $query->whereBetween('date', [$_dates[0], $_dates[1]]);
        }
        $data = $query->sum('debit');
        return !empty($data) ? round($data, 2) : 0;
    }

    public function construction_sum_subhead_credit($subhead_id, $department_id = null, $license_id = null, $project_id = null, $_dates = []) {
        $query = $this->where('is_deleted', 0);
        $query->where('institute_id', CONS);
        $query->where('type', 'C');
        $query->where('dr_subhead_id', $subhead_id);
        if (!empty($project_id)) {
            $query->where('project_id', $project_id);
        }
        if (!empty($license_id)) {
            $query->where('license_id', $license_id);
        }
        if (!empty($department_id)) {
            $query->where('department_id', $department_id);
        }
        if (!empty($_dates) && is_array($_dates)) {
            $query->whereBetween('date', [$_dates[0], $_dates[1]]);
        }
        $data = $query->sum('credit');
        return !empty($data) ? round($data, 2) : 0;
    }

    public function cons_department_sum_debit($department_id) {
        $query = $this->where('is_deleted', 0);
        $query->where('institute_id', CONS);
        $query->where('type', 'D');
        $query->where('department_id', $department_id);
        $data = $query->sum('debit');
        return !empty($data) ? round($data, 2) : 0;
    }

    public function cons_department_sum_credit($department_id) {
        $query = $this->where('is_deleted', 0);
        $query->where('institute_id', CONS);
        $query->where('type', 'C');
        $query->where('department_id', $department_id);
        $data = $query->sum('credit');
        return !empty($data) ? round($data, 2) : 0;
    }

    public function cons_department_sum_balance($department_id) {
        $data = $this->cons_department_sum_debit($department_id) - $this->cons_department_sum_credit($department_id);
        return round($data, 2);
    }

    public function cons_license_debit($license_id, $department_id = null) {
        $query = $this->where('is_deleted', 0);
        $query->where('institute_id', CONS);
        $query->where('type', 'D');
        $query->where('license_id', $license_id);
        if (!empty($department_id)) {
            $query->where('department_id', $department_id);
        }
        $data = $query->sum('debit');
        return !empty($data) ? round($data, 2) : 0;
    }

    public function cons_license_credit($license_id, $department_id = null) {
        $query = $this->where('is_deleted', 0);
        $query->where('institute_id', CONS);
        $query->where('type', 'C');
        $query->where('license_id', $license_id);
        if (!empty($department_id)) {
            $query->where('department_id', $department_id);
        }
        $data = $query->sum('credit');
        return !empty($data) ? round($data, 2) : 0;
    }

    public function cons_license_balance($license_id, $department_id = null) {
        $data = $this->cons_license_debit($license_id, $department_id) - $this->cons_license_credit($license_id, $department_id);
        return round($data, 2);
    }

    public function cons_project_debit($project_id, $license_id = null, $department_id = null) {
        $query = $this->where('is_deleted', 0);
        $query->where('institute_id', CONS);
        $query->where('type', 'D');
        $query->where('project_id', $project_id);
        if (!empty($license_id)) {
            $query->where('license_id', $license_id);
        }
        if (!empty($department_id)) {
            $query->where('department_id', $department_id);
        }
        $data = $query->sum('debit');
        return !empty($data) ? round($data, 2) : 0;
    }

    public function cons_project_credit($project_id, $license_id = null, $department_id = null) {
        $query = $this->where('is_deleted', 0);
        $query->where('institute_id', CONS);
        $query->where('type', 'C');
        $query->where('project_id', $project_id);
        if (!empty($license_id)) {
            $query->where('license_id', $license_id);
        }
        if (!empty($department_id)) {
            $query->where('department_id', $department_id);
        }
        $data = $query->sum('credit');
        return !empty($data) ? round($data, 2) : 0;
    }

    public function cons_project_balance($project_id, $license_id = null, $department_id = null) {
        $data = $this->cons_project_debit($project_id, $license_id, $department_id) - $this->cons_project_credit($project_id, $license_id, $department_id);
        return round($data, 2);
    }

    // transport option
    public function sum_vehicle_dbt($vehicleId, $date = null, $companyId = null) {
        $query = $this->where('is_deleted', 0);
        $query->where('type', 'D');
        $query->where('vehicle_id', $vehicleId);
        if (!is_null($date)) {
            $_dt = date("Y-m-d", strtotime($date));
            $query->where('date', $_dt);
        }
        if (!is_null($companyId)) {
            $query->where('institute_id', $companyId);
        }
        $sum = $query->sum('debit');
        return !empty($sum) ? round($sum, 2) : 0;
    }

    public function sum_vehicle_cdt($vehicleId, $date = null, $companyId = null) {
        $query = $this->where('is_deleted', 0);
        $query->where('type', 'C');
        $query->where('vehicle_id', $vehicleId);
        if (!is_null($date)) {
            $_dt = date("Y-m-d", strtotime($date));
            $query->where('date', $_dt);
        }
        if (!is_null($companyId)) {
            $query->where('institute_id', $companyId);
        }
        $sum = $query->sum('credit');
        return !empty($sum) ? round($sum, 2) : 0;
    }

    public function transport_sum_sub_debit($_company_id, $subhead_id, $_dates = []) {
        $query = $this->where('is_deleted', 0);
        $query->where('institute_id', $_company_id);
        $query->where('dr_subhead_id', $subhead_id);
        if (!empty($_dates) && is_array($_dates)) {
            $query->whereBetween('date', [$_dates[0], $_dates[1]]);
        }
        // pr($query->get());
        $data = $query->sum('debit');
        return !empty($data) ? round($data, 2) : 0;
    }

    public function transport_sum_sub_credit($_company_id, $subhead_id, $_dates = []) {
        $query = $this->where('is_deleted', 0);
        $query->where('institute_id', $_company_id);
        $query->where('cr_subhead_id', $subhead_id);
        if (!empty($_dates) && is_array($_dates)) {
            $query->whereBetween('date', [$_dates[0], $_dates[1]]);
        }
        $data = $query->sum('credit');
        return !empty($data) ? round($data, 2) : 0;
    }

    public function transport_sum_sub_balance($_company_id, $subhead_id, $_dates = []) {
        $data = $this->transport_sum_sub_debit($_company_id, $subhead_id, $_dates = []) - $this->transport_sum_sub_credit($_company_id, $subhead_id, $_dates = []);
        return !empty($data) ? round($data, 2) : 0;
    }

    public function transport_sum_particular_debit($_company_id, $particular_id, $vehicle_id = null, $_dates = []) {
        $query = $this->where('is_deleted', 0);
        $query->where('institute_id', $_company_id);
        $query->where('dr_particular_id', $particular_id);
        if (!is_null($vehicle_id)) {
            $query->where('vehicle_id', $vehicle_id);
        }
        if (!empty($_dates) && is_array($_dates)) {
            $query->whereBetween('date', [$_dates[0], $_dates[1]]);
        }
        $data = $query->sum('debit');
        return !empty($data) ? round($data, 2) : 0;
    }

    public function transport_sum_particular_credit($_company_id, $particular_id, $vehicle_id = null, $_dates = []) {
        $query = $this->where('is_deleted', 0);
        $query->where('institute_id', $_company_id);
        $query->where('cr_particular_id', $particular_id);
        if (!is_null($vehicle_id)) {
            $query->where('vehicle_id', $vehicle_id);
        }
        if (!empty($_dates) && is_array($_dates)) {
            $query->whereBetween('date', [$_dates[0], $_dates[1]]);
        }
        $data = $query->sum('credit');
        return !empty($data) ? round($data, 2) : 0;
    }

    public function transport_sum_alldebit_between_date($dates = [], $head_id = null, $institute_id = null, $vehicle_id = null) {
        $query = $this->where('is_deleted', 0);
        $query->where('type', 'D');
        if (!empty($dates) && is_array($dates)) {
            $query->whereBetween('date', [$dates[0], $dates[1]]);
        }
        if (!is_null($head_id)) {
            $query->where('dr_head_id', $head_id);
        }
        if (!is_null($institute_id)) {
            $query->where('institute_id', $institute_id);
        }
        if (!is_null($vehicle_id)) {
            $query->where('vehicle_id', $vehicle_id);
        }

        $sum = $query->sum('debit');
        return !empty($sum) ? round($sum, 2) : 0;
    }

    public function transport_sum_allcredit_between_date($dates = [], $head_id = null, $institute_id = null, $vehicle_id = null) {
        $query = $this->where('is_deleted', 0);
        $query->where('type', 'C');
        if (!empty($dates) && is_array($dates)) {
            $query->whereBetween('date', [$dates[0], $dates[1]]);
        }
        if (!is_null($head_id)) {
            $query->where('cr_head_id', $head_id);
        }
        if (!is_null($institute_id)) {
            $query->where('institute_id', $institute_id);
        }
        if (!is_null($vehicle_id)) {
            $query->where('vehicle_id', $vehicle_id);
        }

        $sum = $query->sum('credit');
        return !empty($sum) ? round($sum, 2) : 0;
    }

    public function transport_sum_subhead_debit($_company_id, $subhead_id, $vehicle_id = null, $_dates = []) {
        $query = $this->where('is_deleted', 0);
        $query->where('institute_id', $_company_id);
        $query->where('dr_subhead_id', $subhead_id);
        if (!is_null($vehicle_id)) {
            $query->where('vehicle_id', $vehicle_id);
        }
        if (!empty($_dates) && is_array($_dates)) {
            $query->whereBetween('date', [date_ymd($_dates[0]), date_ymd($_dates[1])]);
        }
        $data = $query->sum('debit');
        return !empty($data) ? round($data, 2) : 0;
    }

    public function transport_sum_subhead_credit($_company_id, $subhead_id, $vehicle_id = null, $_dates = []) {
        $query = $this->where('is_deleted', 0);
        $query->where('institute_id', $_company_id);
        $query->where('cr_subhead_id', $subhead_id);
        if (!is_null($vehicle_id)) {
            $query->where('vehicle_id', $vehicle_id);
        }
        if (!empty($_dates) && is_array($_dates)) {
            $query->whereBetween('date', [date_ymd($_dates[0]), date_ymd($_dates[1])]);
        }
        $data = $query->sum('credit');
        return !empty($data) ? round($data, 2) : 0;
    }

    // employee option
    public function sum_employee_dbt($employeeId, $dates = [], $companyId = null) {
        $query = $this->where('is_deleted', 0);
        $query->where('type', 'D');
        $query->where('employee_id', $employeeId);
        if (!empty($dates) && is_array($dates)) {
            $query->whereBetween('date', [$dates[0], $dates[1]]);
        }
        if (!is_null($companyId)) {
            $query->where('institute_id', $companyId);
        }
        $sum = $query->sum('debit');
        return !empty($sum) ? round($sum, 2) : 0;
    }

    public function sum_employee_cdt($employeeId, $dates = [], $companyId = null) {
        $query = $this->where('is_deleted', 0);
        $query->where('type', 'C');
        $query->where('employee_id', $employeeId);
        if (!empty($dates) && is_array($dates)) {
            $query->whereBetween('date', [$dates[0], $dates[1]]);
        }
        if (!is_null($companyId)) {
            $query->where('institute_id', $companyId);
        }
        $sum = $query->sum('credit');
        return !empty($sum) ? round($sum, 2) : 0;
    }

    public function companyReceiceByDate($insId, $dates = [], $vehicleId = [], $_head_id = null) {
        $query = $this->where([['institute_id', $insId], ['is_deleted', 0]]);
        $query->where('voucher_type', RECEIVE_VOUCHER);
        if (is_array($dates) && !empty($dates)) {
            $query->whereBetween('date', [$dates[0], $dates[1]]);
        }
        if (is_array($vehicleId) && !empty($vehicleId)) {
            $query->whereNotIn('vehicle_id', $vehicleId);
        }
        if (!is_null($_head_id)) {
            $query->where('dr_head_id', $_head_id);
        }
        $sum = $query->sum('amount');
        return !empty($sum) ? round($sum, 2) : 0;
    }

    public function companyPaymentByDate($insId, $dates = [], $vehicleId = [], $_head_id = null) {
        $query = $this->where([['institute_id', $insId], ['is_deleted', 0]]);
        $query->where('voucher_type', PAYMENT_VOUCHER);
        if (is_array($dates) && !empty($dates)) {
            $query->whereBetween('date', [$dates[0], $dates[1]]);
        }
        if (is_array($vehicleId) && !empty($vehicleId)) {
            $query->whereNotIn('vehicle_id', $vehicleId);
        }
        if (!is_null($_head_id)) {
            $query->where('cr_head_id', $_head_id);
        }
        $sum = $query->sum('amount');
        return !empty($sum) ? round($sum, 2) : 0;
    }

    public function allCompanyPaymentByDate($from_date, $end_date, $_companyId = null, $vehicleId = [], $_head_id = null) {
        $query = $this->where([['voucher_type', PAYMENT_VOUCHER], ['is_deleted', 0]]);
        if (!empty($from_date) && !empty($end_date)) {
            $query->whereBetween('date', [$from_date, $end_date]);
        }
        if (!is_null($_companyId)) {
            $query->where('institute_id', $_companyId);
        }
        if (is_array($vehicleId) && !empty($vehicleId)) {
            $query->whereNotIn('vehicle_id', $vehicleId);
        }
        if (!is_null($_head_id)) {
            $query->where('cr_head_id', $_head_id);
        }
        $data = $query->sum('amount');
        return !empty($data) ? round($data, 2) : 0;
    }

    public function allCompanyReceiveByDate($from_date, $end_date, $_companyId = null, $vehicleId = [], $_head_id = null) {
        $query = $this->where([['voucher_type', RECEIVE_VOUCHER], ['is_deleted', 0]]);
        if (!empty($from_date) && !empty($end_date)) {
            $query->whereBetween('date', [$from_date, $end_date]);
        }
        if (!is_null($_companyId)) {
            $query->where('institute_id', $_companyId);
        }
        if (is_array($vehicleId) && !empty($vehicleId)) {
            $query->whereNotIn('vehicle_id', $vehicleId);
        }
        if (!is_null($_head_id)) {
            $query->where('dr_head_id', $_head_id);
        }
        $data = $query->sum('amount');
        return !empty($data) ? round($data, 2) : 0;
    }

}
