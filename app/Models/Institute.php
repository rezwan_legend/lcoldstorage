<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Institute extends Model {

    protected $table = 'institutes';
    public $timestamps = false;

    public static function model($className = __CLASS__) {
        return new $className();
    }

    public function setting() {
        return $this->hasOne('App\Models\GeneralSetting', 'institute_id');
    }

    public function users() {
        return $this->hasMany('App\Models\User', 'institute_id', 'id');
    }

    public function chambers() {
        return $this->hasMany('App\Models\Chamber', 'institute_id', 'id');
    }

    public function mills() {
        return $this->hasMany('App\Models\Mill', 'institute_id', 'id');
    }

    public function heads() {
        return $this->hasMany('App\Models\Account\Head', 'institute_id', 'id')->where('is_deleted', 0);
    }

    public function subheads() {
        return $this->hasMany('App\Models\Account\SubHead', 'institute_id', 'id')->where('is_deleted', 0);
    }

    public function transactions() {
        return $this->hasMany('App\Models\CustomerTransaction', 'institute_id', 'id')->where('is_deleted', 0);
    }

    // added by Billah
    public function categories() {
        return $this->hasMany('App\Models\Category', 'institute_id', 'id')->where('is_deleted', 0);
    }

    // added by Billah
    public function products() {
        return $this->hasMany('App\Models\Product', 'institute_id', 'id')->where('is_deleted', 0);
    }

    public function stocks() {
        return $this->hasMany('App\Models\Stock', 'institute_id', 'id');
    }

    public function drawer() {
        return $this->hasMany('App\Models\Rice\Drawer', 'institute_id', 'id');
    }

    public function company() {
        return $this->hasMany('App\Models\FillingStation\Company', 'institute_id', 'id');
    }

    public static function get_institute_name_by_id($id) {
        $institute = Institute::find($id);
        return !empty($institute) ? $institute->name : '';
    }

    public static function get_institute_permission_by_id($id) {
        $permissions = DB::table('institute_permissions')->where('institute_id', '=', $id)->first()->permissions;
        return json_decode($permissions, true);
    }

    public function businessType($id) {
        $data = "";
        if (!empty($id)) {
            $data = BusinessType::find($id);
        }
        $_name = !empty($data) ? $data->business_type : '';
        return $_name;
    }

    public function businessPrefix($id) {
        $data = "";
        if (!empty($id)) {
            $data = BusinessType::find($id);
        }
        $_name = !empty($data) ? $data->prefix : '';
        return $_name;
    }

    public function getList($idArr = [], $_routeName = null) {
        $query = $this->where([["status", 1], ["is_deleted", 0]]);
        if (!empty($idArr) && is_array($idArr)) {
            $query->whereIn("id", $idArr);
        }
        if (!is_null($_routeName)) {
            $query->where('route_name', $_routeName);
        }
        $query->orderBy("name", "ASC");
        $_dataset = $query->get();
        return $_dataset;
    }

    public static function get_list($idArr = [], $_routeName = null) {
        $query = Institute::where([["status", 1], ["is_deleted", 0]]);
        if (!empty($idArr) && is_array($idArr)) {
            $query->whereIn("id", $idArr);
        }
        if (!is_null($_routeName)) {
            $query->where('route_name', $_routeName);
        }
        $query->orderBy("name", "ASC");
        $_dataset = $query->get();
        return $_dataset;
    }

}
