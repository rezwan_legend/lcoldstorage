<?php

namespace App\Models\EssentialSetting;

use Illuminate\Database\Eloquent\Model;

class AgentParty extends Model {

    protected $table = "agent_party";
    public $timestamps = false;

    public static function model($className = __CLASS__) {
        $model = new $className();
        return $model;
    }

    public function tablename() {
        return $this->table;
    }

    public function productType() {
        return $this->belongsTo('App\Models\ProductType', 'product_type_id');
    }

    public function category() {
        return $this->belongsTo('App\Models\Category', 'category_id');
    }

    public function institute() {
        return $this->belongsTo('App\Models\Institute', 'company_id');
    }

    public function product() {
        return $this->belongsTo('App\Models\Product', 'product_id');
    }

    public function UnitSize() {
        return $this->belongsTo('App\Models\UnitSize', 'size_id');
    }

    public function productUnit() {
        return $this->belongsTo('App\Models\ProductUnit', 'unit_id');
    }



}