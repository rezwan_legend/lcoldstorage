<?php
namespace App\Exports;

use App\Models\Inventory\ProductType;
use Maatwebsite\Excel\Concerns\FromCollection;
class ProductTypeExport implements FromCollection
{
    public function collection()
    {
        return ProductType::all();
    }
}