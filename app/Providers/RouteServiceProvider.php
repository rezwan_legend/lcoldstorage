<?php

namespace App\Providers;

use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider {

    protected $namespace = 'App\Http\Controllers';

    public function boot() {
        parent::boot();
    }

    public function map() {
        $this->mapApiRoutes();
        $this->mapWebRoutes();
        $this->mapHrRoutes();
        $this->mapAccountRoutes();
        $this->mapInventoryRoutes();
        $this->mapProductionRoutes();
        $this->mapDistributionRoutes();
        $this->mapEssentialRoutes();
    }

    protected function mapApiRoutes() {
        Route::prefix('api')
                ->middleware('api')
                ->namespace($this->namespace)
                ->group(base_path('routes/api.php'));
    }

    protected function mapWebRoutes() {
        Route::middleware('web')
                ->namespace($this->namespace)
                ->group(base_path('routes/web.php'));
    }

    protected function mapHrRoutes() {
        Route::middleware('web')
                ->namespace($this->namespace)
                ->group(base_path('routes/hr.php'));
    }

    protected function mapAccountRoutes() {
        Route::middleware('web')
                ->namespace($this->namespace)
                ->group(base_path('routes/account.php'));
    }

    protected function mapInventoryRoutes() {
        Route::middleware('web')
                ->namespace($this->namespace)
                ->group(base_path('routes/inventory.php'));
    }

    protected function mapProductionRoutes() {
        Route::middleware('web')
                ->namespace($this->namespace)
                ->group(base_path('routes/production.php'));
    }

    protected function mapDistributionRoutes() {
        Route::middleware('web')
                ->namespace($this->namespace)
                ->group(base_path('routes/distribution.php'));
    }

      protected function mapEssentialRoutes() {
        Route::middleware('web')
                ->namespace($this->namespace)
                ->group(base_path('routes/essential.php'));
    }

}
