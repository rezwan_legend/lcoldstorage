<?php

namespace App\Http\Controllers;

use Auth;
use DB;
use Exception;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\HomeController;
use App\Models\Account\SubHead;
use App\Models\Account\Transaction;
use App\Models\Account\Particular;
use App\Models\Institute;
use App\Models\UnitSize;
use App\Models\ProductUnit;
use App\Models\PriceUnit;
use App\Models\ProductType;
use App\Models\Category;
use App\Models\Product;
use App\Models\Delivery;
use App\Models\SalesOrder;

class DeliveryController extends HomeController {

    public function index() {
        check_user_access('ricemill_delivery_list');
        $subhead = new SubHead();
        $dataset = Delivery::where([['sales_type', NULL], ['is_deleted', 0]])->orderBy('date', 'DESC')->paginate($this->getSettings()->pagesize);

        $this->model['businessList'] = $this->userInstitutes($this->currentRouteFile());
        $this->model['page_title'] = "Product Delivery List";
        $this->model['subhead'] = $subhead;
        $this->model['dataset'] = $dataset;
        return view('delivery.index', $this->model);
    }

    public function create() {
        check_user_access('ricemill_delivery_create');
        $deliveryNo = Delivery :: model()->deliveryNo();
        $salesOrder = SalesOrder ::where([['is_deleted', 0], ['process_status', PROCESS_COMPLETE]])->get();
        $subheads = SubHead::where('is_deleted', 0)->get();
        $this->model['page_title'] = "Product Delivery Order Create";
        $this->model['businessList'] = $this->userInstitutes($this->currentRouteFile());
        $this->model['subheads'] = $subheads;
        $this->model['deliveryNo'] = $deliveryNo;
        $this->model['salesOrder'] = $salesOrder;
        return view('delivery.create', $this->model);
    }

    public function store(Request $r) {
        //  pr($_POST);
        $input = $r->all();
        $rule = array(
            'party_name' => 'required',
            'delivery_weight' => 'required',
        );
        $messages = array(
            'party_name.required' => 'Please select a Supplier.',
            'delivery_weight.required' => 'Delivery weight required',
        );

        $valid = Validator::make($input, $rule, $messages);
        if ($valid->fails()) {
            return redirect()->back()->withErrors($valid)->withInput();
        }

        DB::beginTransaction();
        try {
            $head_id = SubHead::where('id', $r->party_name)->first();
            $model = new Delivery();
            $model->company_id = $r->company_id;
            $model->order_no = $r->order_no;
            $model->sales_id = $r->sales_id;
            $model->challan_no = $r->challan_no;
            $model->date = !empty($r->order_date) ? date_ymd($r->order_date) : date('Y-m-d');
            $model->from_head_id = $head_id->head_id;
            $model->from_subhead_id = $r->party_name;
            $model->from_particular_id = $r->party_name;
            $model->sales_weight = $r->sales_weight;
            $model->delivery_weight = $r->delivery_weight;
            $model->remain_weight = $r->remain_weight;
            $model->vehicle_no = $r->vehicle_no;
            $model->person_name = $r->person_name;
            $model->note = $r->note;
            $model->created_by = Auth::id();
            $model->created_at = cur_date_time();
            $model->_key = uniqueKey();
            if (!$model->save()) {
                throw new Exception("Error! while creating purchase.");
            }

            DB::commit();
            return redirect("/dellivery/itemlist/{$model->_key}")->with('success', 'Sales Order Create Successfully.');
        } catch (Exception $e) {
            DB::rollback();
            return redirect()->back()->with('danger', $e->getMessage());
        }
    }

    public function show($id) {
        check_user_access('ricemill_delivery_details');
        $data = Delivery::find($id);
        $itemDataset = DeliveryItem::where('sales_id', $data->id)->get();
        $tmodel = new Transaction();
        $particularModel = new Particular();
        $sale_id = Transaction::where('sale_id', $data->id)->orderBy('id', 'DESC')->first();

        $this->model['sale_id'] = !empty($sale_id) ? $sale_id->id : NULL;
        $this->model['data'] = $data;
        $this->model['tmodel'] = $tmodel;
        $this->model['particularModel'] = $particularModel;
        $this->model['itemDataset'] = $itemDataset;
        return view('ricemill/delivery.details', $this->model);
    }

    public function gatepass($id) {
        check_user_access('ricemill_delivery_gatepass');
        $data = Delivery::find($id);
        $itemDataset = DeliveryItem::where('sales_id', $data->id)->get();

        $this->model['data'] = $data;
        $this->model['itemDataset'] = $itemDataset;
        return view('delivery.gatepass', $this->model);
    }

    public function edit($id) {
        check_user_access('ricemill_delivery_edit');
        $data = Delivery::where('id', $id)->first();
        $particulars = Particular::where('is_deleted', 0)->get();
        $subheads = SubHead::where('is_deleted', 0)->get();

        $this->model['data'] = $data;
        $this->model['subheads'] = $subheads;
        $this->model['businessList'] = $this->userInstitutes($this->currentRouteFile());
        $this->model['particulars'] = $particulars;
        return view('delivery.edit', $this->model);
    }

    public function update(Request $r, $id) {
//        pr($_POST);
        $input = $r->all();
        $rule = array(
            'from_subhead_id' => 'required',
            'to_subhead_id' => 'required',
            'challan_weight' => 'required',
            'scale_weight' => 'required',
            'challan_no' => 'required',
            'bag_quantity' => 'required',
        );
        $messages = array(
            'from_subhead_id.required' => 'Please select a Supplier.',
            'to_subhead_id.required' => 'Please select a Purchases account.',
            'scale_weight.required' => 'Required',
            'challan_no.required' => 'Challan No Required',
            'bag_quantity.required' => 'Bag Quantity Required',
        );

        $valid = Validator::make($input, $rule, $messages);
        if ($valid->fails()) {
            return redirect()->back()->withErrors($valid)->withInput();
        }

        DB::beginTransaction();
        try {
            $_from_head_id = SubHead::where('id', $r->from_subhead_id)->first();
            $_to_head_id = SubHead::where('id', $r->to_subhead_id)->first();

            $model = Delivery::find($id);
            $model->date = !empty($r->order_date) ? date_ymd($r->order_date) : date('Y-m-d');
            $model->company_id = $r->company_id;
            $model->from_head_id = $_to_head_id->head_id;
            $model->from_subhead_id = $r->to_subhead_id;
            $model->from_particular_id = $r->particular;
            $model->to_head_id = $_from_head_id->head_id;
            $model->to_subhead_id = $r->from_subhead_id;
            $model->to_particular_id = $r->supplier_particular;
            $model->challan_no = $r->challan_no;
            $model->order_no = $r->order_no;
            $model->scal_weight = $r->scale_weight;
            $model->challan_weight = $r->challan_weight;
            $model->quantity = $r->bag_quantity;
            $model->less_weight = $r->less_weight;
            $model->net_weight = $r->net_weight;
            $model->avg_weight = $r->avg_weight;
            $model->vehicle_no = $r->vehicle_no;
            $model->person_name = $r->person_name;
            $model->note = $r->note;
            $model->modified_at = cur_date_time();
            $model->modified_by = Auth::id();
            if (!$model->save()) {
                throw new Exception("Error! while updating Sale.");
            }

            foreach ($model->items as $item) {
                $item->date = $model->date;
                $item->company_id = $model->company_id;
                if (!$item->save()) {
                    throw new Exception("Error while processing itmes.");
                }
            }

            DB::commit();
            return redirect('delivery')->with('success', 'Sales Updated Successfully.');
        } catch (Exception $e) {
            DB::rollback();
            return redirect()->back()->with('danger', $e->getMessage());
        }
    }

    public function confirm($id) {
        check_user_access('ricemill_delivery_process');
        $data = Delivery::find($id);
        DB::beginTransaction();
        try {
            if (count($data->items) <= 0) {
                throw new Exception("Sorry !! There is no items for processing....");
            }
            $descriptionArr = [];
            foreach ($data->items as $item) {
                $descriptionArr[] = $item->product->name . "(" . $item->price_unit_qty . "*" . $item->rate . ")";
            }

            $descriptionArr[] = $data->note;
            $description = implode(",", $descriptionArr);
            $transaction = new Transaction();
            $transaction->institute_id = $modelStock->company_id;
            $transaction->vehicle_no = $data->vehicle_no;
            $transaction->date = $data->date;
            $transaction->sale_id = $modelStock->sales_order_id;
            $transaction->type = 'C';
            $transaction->voucher_type = SALES_VOUCHER;
            $transaction->payment_method = PAYMENT_NO;
            $transaction->dr_head_id = $data->to_head_id;
            $transaction->dr_subhead_id = $data->to_subhead_id;
            $transaction->dr_particular_id = $data->to_particular_id;
            $transaction->cr_head_id = $data->from_head_id;
            $transaction->cr_subhead_id = $data->from_subhead_id;
            $transaction->cr_particular_id = $data->from_particular_id;
            $transaction->by_whom = Auth::user()->name;
            $transaction->amount = $data->total_price($data->id);
            $transaction->debit = $transaction->amount;
            $transaction->credit = $transaction->amount;
            $transaction->description = $description;
            $transaction->note = $description;
            $transaction->created_by = Auth::id();
            $transaction->_key = uniqueKey();
            if (!$transaction->save()) {
                throw new Exception("Error while saving data in Transaction.");
            }
            $data->modified_by = Auth::id();
            $data->process_status = PROCESS_COMPLETE;
            if (!$data->save()) {
                throw new Exception("Something Went Wrong. Please Re Submit your request.");
            }

            DB::commit();
            return redirect('delivery')->with('success', 'Sales Order Processed Successfully');
        } catch (Exception $e) {
            DB::rollback();
            return redirect('delivery')->with('danger', $e->getMessage());
        }
    }

    public function delivery($id) {
        check_user_access('ricemill_delivery_process');
        $data = Delivery::find($id);
        DB::beginTransaction();
        try {
            if (count($data->items) <= 0) {
                throw new Exception("Sorry !! There is no items for processing....");
            }
            foreach ($data->items as $item) {
                $condArr = ['category_id' => $item->category_id, 'product_type_id' => $item->product_type_id, 'category_id' => $item->category_id];
                if ($item->product_type_id != RAW_TYPE) {
                    $condArr['size_id'] = $item->size_id;
                }
                $_qty_in = Stock::model()->sum_in_production($item->company_id, $item->product_id, 'quantity', $condArr);
                $_qty_out = Stock::model()->sum_out_production($item->company_id, $item->product_id, 'quantity', $condArr);
                $_qty_remain = ($_qty_in - $_qty_out);

                $price_in = Stock::model()->sum_in_production($item->company_id, $item->product_id, 'price', $condArr);
                $price_out = Stock::model()->sum_out_production($item->company_id, $item->product_id, 'price', $condArr);
                $total_price_remain = ($price_in - $price_out);

                $avg_price = round(($total_price_remain / $_qty_remain), 3);

                $_price_data = PriceSetting::where([['company_id', $item->company_id], ['category_id', $item->category_id], ['product_id', $item->product_id]])->orderBy('date', 'DESC')->first();
                $_avgPorCostP = !empty($avg_price) ? $avg_price : (!empty($_price_data) ? $_price_data->cost_price : 0);
                $type = $item->product_type_id;
                $net_weight = $item->net_weight;
                $modelStock = new Stock();
                if ($type == RAW_TYPE) {
                    $modelStock->size_id = DEFAULT_SIZE;
                    $net_qty = ($net_weight / DEFAULT_WEIGHT);
                    $modelStock->net_qty = round($net_qty, 2);
                } else {
                    $modelStock->size_id = $item->size_id;
                    $modelStock->net_qty = $item->quantity;
                }
                $modelStock->date = $item->date;
                $modelStock->company_id = $item->company_id;
                $modelStock->sales_order_id = $item->sales_id;
                $modelStock->sales_item_id = $item->id;
                $modelStock->product_type_id = $item->product_type_id;
                $modelStock->category_id = $item->category_id;
                $modelStock->product_id = $item->product_id;
                $modelStock->product_quality_id = $item->quality_id;
                $modelStock->rate = $_avgPorCostP;
                $modelStock->price = round(($_avgPorCostP * $item->quantity), 2);
                $modelStock->cost_price = $item->cost_price;
                $modelStock->sale_price = $item->sale_price;
                $modelStock->net_weight = $item->net_weight;
                $modelStock->unit_id = $item->unit_id;
                $modelStock->price_unit_id = $item->price_unit_id;
                $modelStock->price_unit_qty = $item->price_unit_qty;
                $modelStock->quantity = $item->quantity;
                $modelStock->avg_price = $_avgPorCostP;
                $modelStock->stock_type = STOCK_TYPE_OUT;
                $modelStock->type = STOCK_TYPE_SALE;
                $modelStock->created_by = Auth::id();
                if (!$modelStock->save()) {
                    throw new Exception("Query Problem on Creating Stocks.");
                }
                $item->process_status = PROCESS_COMPLETE;
                if (!$item->save()) {
                    throw new Exception("Error while processing itmes.");
                }
            }

            $data->modified_by = Auth::id();
            $data->process_status = PROCESS_COMPLETE;
            if (!$data->save()) {
                throw new Exception("Something Went Wrong. Please Re Submit your request.");
            }

            DB::commit();
            return redirect('delivery')->with('success', 'Sales Order Processed Successfully');
        } catch (Exception $e) {
            DB::rollback();
            return redirect('delivery')->with('danger', $e->getMessage());
        }
    }

    public function reset($id) {
        check_user_access('ricemill_delivery_reset');
        $data = Delivery::find($id);

        DB::beginTransaction();
        try {
            if (!empty($data->stocks)) {
                foreach ($data->stocks as $stock) {
                    if (!$stock->delete()) {
                        throw new Exception("Error while deleting records from Production Stocks.");
                    }
                }
            }

            if (!empty($data->sales_transactions)) {
                foreach ($data->sales_transactions as $transaction) {
                    if (!$transaction->delete()) {
                        throw new Exception("Error while deleting records from Transaction.");
                    }
                }
            }

            if (!empty($data->items)) {
                foreach ($data->items as $_item) {
                    $_item->process_status = PROCESS_PENDING;
                    if (!$_item->save()) {
                        throw new Exception("Error while reset items.");
                    }
                }
            }

            $data->process_status = ORDER_PENDING;
            $data->modified_by = Auth::id();
            if (!$data->save()) {
                throw new Exception("Something Went Wrong. Please Re Submit your request.");
            }

            DB::commit();
            return redirect('delivery')->with('success', 'Sales Order Reset Successfully');
        } catch (Exception $e) {
            DB::rollback();
            return redirect('delivery')->with('danger', $e->getMessage());
        }
    }

    // Ajax Functions
    public function search(Request $r) {
        // pr($_POST);
        $item_count = !empty($r->item_count) ? $r->item_count : $this->getSettings()->pagesize;
        $from_date = date_ymd($r->from_date);
        $companyId = $r->company_id;
        $end_date = !empty($r->end_date) ? date_ymd($r->end_date) : date('Y-m-d');

        $query = Delivery::where([['sales_type', NULL], ['is_deleted', 0]]);
        if (!empty($companyId)) {
            $query->where('company_id', $companyId);
        }
        if (!empty($from_date)) {
            $query->whereBetween('date', [$from_date, $end_date]);
        }
        if ($r->search_by) {
            $query->where('process_status', $r->search_by)->get();
        }
        if ($r->search) {
            $query->where('challan_no', 'like', '%' . trim($r->search) . '%')->get();
        }

        if (!empty($r->_search_supplier)) {
            $_partyList = Particular::where('name', 'like', '%' . trim($r->_search_supplier) . '%')->get();
            foreach ($_partyList as $_party) {
                $partyId[] = $_party->id;
            }
            $query->whereIn('to_particular_id', $partyId);
        }
        $dataset = $query->orderBy('date', 'DESC')->paginate($item_count);
        return view('delivery._list', compact('dataset'));
    }

    public function sales_itmes() {
        check_user_access('ricemill_delivery_items_list');
        $dataset = DeliveryItem::where([['sales_type', NULL], ['is_deleted', 0]])->orderBy('date', 'DESC')->paginate($this->getSettings()->pagesize);
        $_product_type = ProductType::where([['is_deleted', 0]])->get();

        $this->model['businessList'] = $this->userInstitutes($this->currentRouteFile());
        $this->model['dataset'] = $dataset;
        $this->model['product_type'] = $_product_type;
        return view('delivery.sales_list', $this->model);
    }

    public function edit_item($id) {
        check_user_access('ricemill_delivery_items_list');
        $data = DeliveryItem::find($id);
        $this->model['data'] = $data;
        return view('delivery.items_edit', $this->model);
    }

    public function update_item(Request $r) {
        DB::beginTransaction();
        try {
            $model = DeliveryItem::find($r->sales_id);
            $model->rate = $r->rate;
            $model->price = $r->price;
            if (!$model->save()) {
                throw new Exception("Error! while updating purchase item.");
            }
            DB::commit();
            return redirect('delivery/items')->with('success', 'Sales Item Updated Successfully.');
        } catch (Exception $e) {
            DB::rollback();
            return redirect()->back()->with('danger', $e->getMessage());
        }
    }

    public function sales_itmes_search(Request $r) {
        $item_count = !empty($r->item_count) ? $r->item_count : $this->getSettings()->pagesize;
        $product_type = $r->product_type;
        $category = $r->category;
        $companyId = $r->company_id;
        $product = $r->product_id;
        $_size_id = $r->size_id;
        $search_supplier = $r->srcParty;
        $end_date = !empty($r->end_date) ? date_ymd($r->end_date) : date('Y-m-d');

        $query = DeliveryItem::where([['sales_type', NULL], ['is_deleted', 0]]);
        if (!empty($r->from_date)) {
            $query->whereBetween('date', [$r->from_date, $end_date]);
            $this->searchValues['Dates'] = "{$r->from_date} | {$end_date}";
        }
        if (!empty($companyId)) {
            $query->where('company_id', $companyId);
            $this->searchValues['Company'] = Institute::find($companyId)->name;
        }
        if (!empty($product_type)) {
            $query->where('product_type_id', $product_type);
            $this->searchValues['Product Type'] = ProductType::find($product_type)->name;
        }
        if (!empty($category)) {
            $query->where('category_id', $category);
            $this->searchValues['Category'] = Category::find($category)->name;
        }
        if (!empty($product)) {
            $query->where('product_id', $product);
            $this->searchValues['Product'] = Product::find($product)->name;
        }
        if (!empty($_size_id)) {
            $query->where('size_id', $_size_id);
            $this->searchValues['Size'] = Size::find($_size_id)->name;
        }
        if (!empty($search_supplier)) {
            $_partyList = Particular::where('name', 'like', '%' . trim($search_supplier) . '%')
                    ->orWhere('address', 'like', '%' . trim($search_supplier) . '%')
                    ->orWhere('mobile', 'like', '%' . to_eng($search_supplier) . '%')
                    ->get();
            foreach ($_partyList as $_party) {
                $particularId[] = $_party->id;
            }

            $idArr = [];
            $_dataList = Delivery::where('is_deleted', 0)->whereIn('to_particular_id', $particularId)->get();
            foreach ($_dataList as $_data) {
                $idArr[] = $_data->id;
            }
            $query->whereIn('sales_id', $idArr);
            $this->searchValues['Party'] = $search_supplier;
        }
        $dataset = $query->orderBy('date', 'DESC')->paginate($item_count);

        $this->model['dataset'] = $dataset;
        $this->model['searchValues'] = $this->searchValues;
        return view('delivery.sales_list_search', $this->model);
    }

    public function delete() {
        $resp = array();
        DB::beginTransaction();
        try {
            foreach ($_POST['data'] as $id) {
                $data = Delivery::find($id);

                if (!empty($data->stocks)) {
                    foreach ($data->stocks as $stock) {
                        if (!$stock->delete()) {
                            throw new Exception("Error while deleting records from Production Stocks.");
                        }
                    }
                }

                if (!empty($data->sales_transactions)) {
                    foreach ($data->sales_transactions as $transaction) {
                        if (!$transaction->delete()) {
                            throw new Exception("Error while deleting records from Transaction.");
                        }
                    }
                }

                if (!empty($data->items)) {
                    foreach ($data->items as $_item) {
                        $_item->is_deleted = 1;
                        $_item->deleted_at = cur_date_time();
                        $_item->deleted_by = Auth::id();
                        if (!$_item->save()) {
                            throw new Exception("Error while deleting records.");
                        }
                    }
                }

                $data->is_deleted = 1;
                $data->deleted_at = cur_date_time();
                $data->deleted_by = Auth::id();
                if (!$data->save()) {
                    throw new Exception("Error while deleting records.");
                }
            }

            DB::commit();
            $resp['success'] = true;
            $resp['message'] = 'Item Deleted Successfully';
        } catch (Exception $e) {
            DB::rollback();
            $resp['success'] = false;
            $resp['message'] = $e->getMessage();
        }
        return $resp;
    }



    public function search_product(Request $r) {
        $orderNo = $r->orderNo;
        $query = Product::where(['is_deleted', 0]);
        $sizeList = UnitSize::where([['is_deleted', 0], ['order_no', $orderNo]])->get();

        if (!empty($product_type)) {
            $query->where('product_type_id', $product_type);
        }
        if (!empty($category_id)) {
            $query->where('category_id', $category_id);
        }
        if (!empty($product_name)) {
            $query->where('name', 'like', '%' . $product_name . '%');
        }
        $dataset = $query->get();

        $this->model['dataset'] = $dataset;
        $this->model['sizes'] = $sizeList;
        return view('sale_price_setting._productlist', $this->model);
    }

    public function add_item(Request $r) {
        //pr($_POST);
        $input = $r->all();
        $rule = array(
            'challan_id' => 'required',
            'product_id' => 'required',
        );
        $messages = array(
            'challan_id.required' => 'order number required',
            'product_id.required' => 'Order Date required',
        );

        $valid = Validator::make($input, $rule, $messages);
        if ($valid->fails()) {
            return redirect()->back()->withErrors($valid)->withInput();
        }

        DB::beginTransaction();
        try {
            $model = Delivery::where('id', $r->sales_id)->first();
            $sale_qty = DeliveryItem::where([['company_id', $r->company_id], ['sales_id', $r->sales_id], ['product_type_id', $r->product_type], ['product_id', $r->product_id], ['size_id', $r->size_id]])->sum('quantity');
            $qty = $r->qty;
            $total_sale = $sale_qty + $qty;
            $available_stock = $r->available_stock;
            if ($available_stock < $total_sale) {
                throw new Exception("<b>Quantity must be less than $available_stock</b>");
            }

            $modelItem = new DeliveryItem();
            $modelItem->company_id = $model->company_id;
            $modelItem->sales_id = $model->id;
            $modelItem->date = $model->date;
            $modelItem->challan_no = $model->challan_no;
            $modelItem->price_unit_qty = $r->price_qty_unit;
            $modelItem->net_qty = $r->net_qty;
            if ($r->product_type == EMPTY_BAG) {
                $modelItem->sales_type = SALE_EMPTYBAG;
                $modelItem->price_unit_qty = $r->qty;
                $modelItem->net_qty = $r->qty;
            }
            $modelItem->product_type_id = $r->product_type;
            $modelItem->category_id = $r->category;
            $modelItem->product_id = $r->product_id;
            $modelItem->size_id = $r->size_id;
            $modelItem->unit_id = $r->unit_id;
            $modelItem->net_weight = $r->net_weight;
            $modelItem->quality_id = $r->quality_id;
            $modelItem->price_unit_id = $r->price_unit_id;
            $modelItem->quantity = $r->qty;
            $modelItem->cost_price = $r->last_cost_price;
            $modelItem->sale_price = $r->last_sale_price;
            $modelItem->rate = $r->rate;
            $modelItem->price = $r->price;
            $modelItem->profit = $modelItem->price - ($modelItem->cost_price * $modelItem->quantity);
            $modelItem->avg_price = $r->avg_price;
            $modelItem->created_by = Auth::id();
            $modelItem->created_at = cur_date_time();
            $modelItem->_key = uniqueKey();
            if (!$modelItem->save()) {
                throw new Exception("Error! while saving order items record.");
            }
            DB::commit();
            $this->response['success'] = true;
            $this->response['message'] = 'Item saved successfully.';
        } catch (Exception $e) {
            DB::rollback();
            $this->response['success'] = false;
            $this->response['message'] = $e->getMessage();
        }
        return $this->response;
    }

    public function search_itemlist(Request $r) {
        $_itemDataset = DeliveryItem::where([['company_id', $r->company_id], ['sales_id', $r->sales_id], ['is_deleted', 0]])->get();
        $this->model['itemDataset'] = $_itemDataset;
        return view('delivery._itemlist_partial', $this->model);
    }

    public function remove_item($id) {
        DB::beginTransaction();
        try {
            $data = DeliveryItem::find($id);
            if (!$data->delete()) {
                throw new Exception("Error while removing record.");
            }
            DB::commit();
            return redirect()->back()->with('success', 'Item removed successfully.');
        } catch (Exception $e) {
            DB::rollback();
            return redirect()->back()->with('danger', $e->getMessage());
        }
    }

}
