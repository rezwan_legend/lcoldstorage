<?php

namespace App\Http\Controllers\Account;

use Illuminate\Http\Request;
use App\Http\Controllers\HomeController;
use App\Models\Institute;
use App\Models\Account\Transaction;

class ReportController extends HomeController {

    public function index() {
        $this->model['breadcrumb_title'] = "Report";
        return view('account.report.index', $this->model);
    }

    public function daily() {
        $this->model['breadcrumb_title'] = "Daily Report";
        $this->model['insList'] = $this->userInstitutes();
        return view('account.report.daily', $this->model);
    }

    public function search_daily(Request $r) {
        //pr($_POST);
        $institute_id = !empty($r->institute_id) ? $r->institute_id : NULL;
        $head_id = !empty($r->head_id) ? $r->head_id : NULL;
        $date = !empty($r->date) ? date_ymd($r->date) : NULL;

        $modelTrans = new Transaction();
        $queryPayments = $modelTrans->where([['institute_id', $institute_id], ['is_deleted', 0], ['voucher_type', 'Payment Voucher']]);
        if (!empty($head_id)) {
            $queryPayments->where('cr_head_id', $head_id);
        }
        if (!empty($date)) {
            $queryPayments->where('date', $date);
        }
        $payments = $queryPayments->get();

        $queryReceives = $modelTrans->where([['institute_id', $institute_id], ['is_deleted', 0], ['voucher_type', 'Receive Voucher']]);
        if (!empty($head_id)) {
            $queryReceives->where('dr_head_id', $head_id);
        }
        if (!empty($date)) {
            $queryReceives->where('date', $date);
        }
        $receives = $queryReceives->get();

        $from_date = '2021-01-01';
        $end_date = date('Y-m-d', strtotime($date . ' -1 day'));

        $inslist = Institute::where([['is_deleted', 0], ['id', '!=', MAIN]])->get();

        $opening_balance = 0;
        if ($institute_id == MAIN) {
            $sumReceive = $modelTrans->allCompanyReceiveByDate($from_date, $end_date, null, [], $head_id);
            $sumPayment = $modelTrans->allCompanyPaymentByDate($from_date, $end_date, null, [], $head_id);
            $opening_balance = ($sumReceive - $sumPayment);
            $this->model['opening_balance'] = $opening_balance;
        }

        $this->model['payments'] = $payments;
        $this->model['receives'] = $receives;
        $this->model['date'] = $date;
        $this->model['from_date'] = $from_date;
        $this->model['end_date'] = $end_date;
        $this->model['inslist'] = $inslist;
        $this->model['modelTrans'] = $modelTrans;
        $this->model['institute_id'] = $institute_id;
        $this->model['head_id'] = $head_id;
        return view('account.report._daily', $this->model);
    }

    public function summary_index() {
        $insList = Institute::get_list();
        //pr($institute_id);
        //$modelWeightItem = new WeightItem();
        $modelTrans = new Transaction();
        $institute_id = institute_id();
        //$modelEmptyBag = new EmptyBag();
        //$purchases = $modelWeightItem->totalPurchaseByDate();
        //$empty_bag_rec = $modelEmptyBag->totalReceiveByDate();
        //$empty_bag_pay = $modelEmptyBag->totalPaymentByDate();
        //$sales = $modelWeightItem->totalSaleByDate();
        $receiveQuery = $modelTrans->where('is_deleted', 0);
        $receiveQuery->where('type', 'D');
        $receiveQuery->orderBy('date', 'DESC');
        $receiveQuery->groupBy('cr_particular_id');
        $receives = $receiveQuery->get();

        $paymentQuery = $modelTrans->where('is_deleted', 0);
        $paymentQuery->where('type', 'C');
        $paymentQuery->orderBy('date', 'DESC');
        $paymentQuery->groupBy('dr_particular_id');
        $payments = $paymentQuery->get();

        $date = date('Y-m-d');
        $from_date = '1970-01-01';
        $end_date = date('Y-m-d', strtotime($date . ' -1 day'));
        $sumReceive = $modelTrans->allSumReceiveByDate($from_date, $end_date, null);
        $sumPayment = $modelTrans->allSumPaymentByDate($from_date, $end_date, null);
        $sumReceiveCurDate = $modelTrans->allSumReceiveByDate($date, $date, null);
        $sumPaymentCurDate = $modelTrans->allSumPaymentByDate($date, $date, null);
        $opening_balance = ($sumReceive - $sumPayment);
        $closing_balance = ( ($opening_balance + $sumReceiveCurDate) - $sumPaymentCurDate );
        $opening_balance = 0;

        $this->model['breadcrumb_title'] = "Account Summary";
        $this->model['date'] = $date;
        $this->model['insList'] = $insList;
        $this->model['opening_balance'] = $opening_balance;
        $this->model['closing_balance'] = $closing_balance;
        $this->model['payments'] = $payments;
        $this->model['receives'] = $receives;
        $this->model['institute_id'] = $institute_id;
        return view('institute.summary_report.index', $this->model);
    }

    public function summary_search(Request $r) {
        $institute_id = $r->institute_id;
        //pr($institute_id);
        //$modelWeightItem = new WeightItem();
        $modelTrans = new Transaction();
        //$modelEmptyBag = new EmptyBag();
        $date = date_ymd($r->date);
        //$purchases = $modelWeightItem->totalPurchaseByDate($date);
        //$empty_bag_rec = $modelEmptyBag->totalReceiveByDate($date);
        //$empty_bag_pay = $modelEmptyBag->totalPaymentByDate($date);
        //$sales = $modelWeightItem->totalSaleByDate($date);
        $receiveQuery = $modelTrans->where('is_deleted', 0);
        if (!empty($institute_id)) {
            $receiveQuery = $receiveQuery->where('institute_id', $institute_id);
        }
        if (!empty($date)) {
            $receiveQuery = $receiveQuery->where('date', $date);
        }
        $receiveQuery->where('type', 'D');
        $receiveQuery->orderBy('date', 'DESC');
        $receiveQuery->groupBy('cr_particular_id');
        $receives = $receiveQuery->get();

        $paymentQuery = $modelTrans->where('is_deleted', 0);
        if (!empty($institute_id)) {
            $paymentQuery->where('institute_id', $institute_id);
        }
        if (!empty($date)) {
            $paymentQuery = $paymentQuery->where('date', $date);
        }
        $paymentQuery->where('type', 'C');
        $paymentQuery->orderBy('date', 'DESC');
        $paymentQuery->groupBy('dr_particular_id');
        $payments = $paymentQuery->get();

        $from_date = '1970-01-01';
        $end_date = date('Y-m-d', strtotime($date . ' -1 day'));
        $sumReceive = $modelTrans->allSumReceiveByDate($from_date, $end_date, $institute_id);
        $sumPayment = $modelTrans->allSumPaymentByDate($from_date, $end_date, $institute_id);
        $sumReceiveCurDate = $modelTrans->allSumReceiveByDate($date, $date, $institute_id);
        $sumPaymentCurDate = $modelTrans->allSumPaymentByDate($date, $date, $institute_id);
        $opening_balance = ($sumReceive - $sumPayment);
        $closing_balance = ( ($opening_balance + $sumReceiveCurDate) - $sumPaymentCurDate );
        if (empty($date)) {
            $opening_balance = 0;
        }

        $this->model['date'] = $date;
        $this->model['opening_balance'] = $opening_balance;
        $this->model['closing_balance'] = $closing_balance;
        $this->model['payments'] = $payments;
        $this->model['receives'] = $receives;

        return view('institute.summary_report._list', $this->model);
    }

}
