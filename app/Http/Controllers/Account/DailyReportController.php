<?php

namespace App\Http\Controllers\Account;

use Illuminate\Http\Request;
use App\Http\Controllers\HomeController;
use App\Models\Account\Transaction;
use App\Models\Institute;

class DailyReportController extends HomeController {

    public function index() {
        $insList = Institute::get_list();
        $institute_id = institute_id();

        $modelTrans = new Transaction();
        $payments = $modelTrans->allPaymentByDate(null, null);
        $receives = $modelTrans->allReceiveByDate(null, null);

        $date = date('Y-m-d');
        $from_date = '1970-01-01';
        $end_date = date('Y-m-d', strtotime($date . ' -1 day'));

        $sumReceive = $modelTrans->allSumReceiveByDate($from_date, $end_date, null);
        $sumPayment = $modelTrans->allSumPaymentByDate($from_date, $end_date, null);
        $sumReceiveCurDate = $modelTrans->allSumReceiveByDate($date, $date, null);
        $sumPaymentCurDate = $modelTrans->allSumPaymentByDate($date, $date, null);

        $opening_balance = ($sumReceive - $sumPayment);
        $closing_balance = ( ($opening_balance + $sumReceiveCurDate) - $sumPaymentCurDate );
        $opening_balance = 0;

        $this->model['breadcrumb_title'] = "Daily Report";
        $this->model['date'] = $date;
        $this->model['insList'] = $insList;
        $this->model['opening_balance'] = $opening_balance;
        $this->model['closing_balance'] = $closing_balance;
        $this->model['payments'] = $payments;
        $this->model['receives'] = $receives;
        return view('account.dailyreport.index', $this->model);
    }

    public function search(Request $r) {
        $institute_id = !empty($r->institute_id) ? $r->institute_id : NULL;
        $payment_method = !empty($r->payment_method) ? $r->payment_method : NULL;
        $date = date_ymd($r->date);

//        if ($institute_id == SHAMIM_AND_BROTHERS) {
//            $institute_id = null;
//        }

        $modelTrans = new Transaction();
        $payments = $modelTrans->allPaymentByDate($date, $payment_method, $institute_id);
        $receives = $modelTrans->allReceiveByDate($date, $payment_method, $institute_id);

        $from_date = '1970-01-01';
        $end_date = date('Y-m-d', strtotime($date . ' -1 day'));

        $sumReceive = $modelTrans->allSumReceiveByDate($from_date, $end_date, $payment_method, $institute_id);
        $sumPayment = $modelTrans->allSumPaymentByDate($from_date, $end_date, $payment_method, $institute_id);
        $sumReceiveCurDate = $modelTrans->allSumReceiveByDate($date, $date, $payment_method, $institute_id);
        $sumPaymentCurDate = $modelTrans->allSumPaymentByDate($date, $date, $payment_method, $institute_id);

        $opening_balance = ($sumReceive - $sumPayment);
        $closing_balance = ( ($opening_balance + $sumReceiveCurDate) - $sumPaymentCurDate );
        if (empty($date)) {
            $opening_balance = 0;
        }

        $this->model['payments'] = $payments;
        $this->model['receives'] = $receives;
        $this->model['opening_balance'] = $opening_balance;
        $this->model['closing_balance'] = $closing_balance;
        $this->model['date'] = $date;
        return view('account.dailyreport._list', $this->model);
    }

    public function summary_index() {
        $insList = Institute::get_list();
        //pr($institute_id);
        //$modelWeightItem = new WeightItem();
        $modelTrans = new Transaction();
        $institute_id = institute_id();
        //$modelEmptyBag = new EmptyBag();
        //$purchases = $modelWeightItem->totalPurchaseByDate();
        //$empty_bag_rec = $modelEmptyBag->totalReceiveByDate();
        //$empty_bag_pay = $modelEmptyBag->totalPaymentByDate();
        //$sales = $modelWeightItem->totalSaleByDate();
        $receiveQuery = $modelTrans->where('is_deleted', 0);
        $receiveQuery->where('type', 'D');
        $receiveQuery->orderBy('date', 'DESC');
        $receiveQuery->groupBy('cr_particular_id');
        $receives = $receiveQuery->get();

        $paymentQuery = $modelTrans->where('is_deleted', 0);
        $paymentQuery->where('type', 'C');
        $paymentQuery->orderBy('date', 'DESC');
        $paymentQuery->groupBy('dr_particular_id');
        $payments = $paymentQuery->get();

        $date = date('Y-m-d');
        $from_date = '1970-01-01';
        $end_date = date('Y-m-d', strtotime($date . ' -1 day'));
        $sumReceive = $modelTrans->allSumReceiveByDate($from_date, $end_date, null);
        $sumPayment = $modelTrans->allSumPaymentByDate($from_date, $end_date, null);
        $sumReceiveCurDate = $modelTrans->allSumReceiveByDate($date, $date, null);
        $sumPaymentCurDate = $modelTrans->allSumPaymentByDate($date, $date, null);
        $opening_balance = ($sumReceive - $sumPayment);
        $closing_balance = ( ($opening_balance + $sumReceiveCurDate) - $sumPaymentCurDate );
        $opening_balance = 0;

        $this->model['breadcrumb_title'] = "Account Summary";
        $this->model['date'] = $date;
        $this->model['insList'] = $insList;
        $this->model['opening_balance'] = $opening_balance;
        $this->model['closing_balance'] = $closing_balance;
        $this->model['payments'] = $payments;
        $this->model['receives'] = $receives;
        $this->model['institute_id'] = $institute_id;
        return view('account.summary_report.index', $this->model);
    }

    public function summary_search(Request $r) {
        $institute_id = $r->institute_id;
        //pr($institute_id);
        //$modelWeightItem = new WeightItem();
        $modelTrans = new Transaction();
        //$modelEmptyBag = new EmptyBag();
        $date = date_ymd($r->date);
        //$purchases = $modelWeightItem->totalPurchaseByDate($date);
        //$empty_bag_rec = $modelEmptyBag->totalReceiveByDate($date);
        //$empty_bag_pay = $modelEmptyBag->totalPaymentByDate($date);
        //$sales = $modelWeightItem->totalSaleByDate($date);
        $receiveQuery = $modelTrans->where('is_deleted', 0);
        if (!empty($institute_id)) {
            $receiveQuery = $receiveQuery->where('institute_id', $institute_id);
        }
        if (!empty($date)) {
            $receiveQuery = $receiveQuery->where('date', $date);
        }
        $receiveQuery->where('type', 'D');
        $receiveQuery->orderBy('date', 'DESC');
        $receiveQuery->groupBy('cr_particular_id');
        $receives = $receiveQuery->get();

        $paymentQuery = $modelTrans->where('is_deleted', 0);
        if (!empty($institute_id)) {
            $paymentQuery->where('institute_id', $institute_id);
        }
        if (!empty($date)) {
            $paymentQuery = $paymentQuery->where('date', $date);
        }
        $paymentQuery->where('type', 'C');
        $paymentQuery->orderBy('date', 'DESC');
        $paymentQuery->groupBy('dr_particular_id');
        $payments = $paymentQuery->get();

        $from_date = '1970-01-01';
        $end_date = date('Y-m-d', strtotime($date . ' -1 day'));
        $sumReceive = $modelTrans->allSumReceiveByDate($from_date, $end_date, $institute_id);
        $sumPayment = $modelTrans->allSumPaymentByDate($from_date, $end_date, $institute_id);
        $sumReceiveCurDate = $modelTrans->allSumReceiveByDate($date, $date, $institute_id);
        $sumPaymentCurDate = $modelTrans->allSumPaymentByDate($date, $date, $institute_id);
        $opening_balance = ($sumReceive - $sumPayment);
        $closing_balance = ( ($opening_balance + $sumReceiveCurDate) - $sumPaymentCurDate );
        if (empty($date)) {
            $opening_balance = 0;
        }

        $this->model['date'] = $date;
        $this->model['opening_balance'] = $opening_balance;
        $this->model['closing_balance'] = $closing_balance;
        $this->model['payments'] = $payments;
        $this->model['receives'] = $receives;

        return view('account.summary_report._list', $this->model);
    }

}
