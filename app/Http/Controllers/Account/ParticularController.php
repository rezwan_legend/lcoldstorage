<?php

namespace App\Http\Controllers\Account;

use Auth;
use DB;
use Exception;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\HomeController;
use App\Models\Account\SubHead;
use App\Models\Account\Particular;
use App\Models\Account\Transaction;
use App\Models\ParticularBalance;

class ParticularController extends HomeController {

    public function index() {
        check_user_access('particular_list');
        $subheads = SubHead::where('is_deleted', 0)->orWhere('common', COMMON_YES)->get();
        $tmodel = new Transaction();

        $model = new Particular();
        $query = $model->where('is_deleted', 0);
        $query->orderBy('code', 'ASC');
        $dataset = $query->paginate($this->getSettings()->pagesize);

        $this->model['page_title'] = "Particular List";
        $this->model['subheads'] = $subheads;
        $this->model['tmodel'] = $tmodel;
        $this->model['dataset'] = $dataset;
        return view('account.particular.index', $this->model);
    }

    public function customer_due() {
        $dataset = Particular::where('head_id', RICE_HEAD_DEBTORS)->orderBy('name', 'ASC')->paginate($this->getSettings()->pagesize);
        $tmodel = new Transaction();
        return view('account.particular.customer_due', compact('dataset', 'tmodel'));
    }

    public function supplier_due() {
        $dataset = Particular::where('head_id', RICE_HEAD_CREDITORS)->orderBy('name', 'ASC')->paginate($this->getSettings()->pagesize);
        $tmodel = new Transaction();
        return view('account.particular.supplier_due', compact('dataset', 'tmodel'));
    }

    public function customer_debit() {
        $dataset = Particular::where('head_id', RICE_HEAD_DEBTORS)->orderBy('name', 'ASC')->paginate($this->getSettings()->pagesize);
        $tmodel = new Transaction();
        return view('account.particular.customer_debit', compact('dataset', 'tmodel'));
    }

    public function supplier_debit() {
        $dataset = Particular::where('head_id', RICE_HEAD_CREDITORS)->orderBy('name', 'ASC')->paginate($this->getSettings()->pagesize);
        $tmodel = new Transaction();
        return view('account.particular.supplier_debit', compact('dataset', 'tmodel'));
    }

    public function create() {
        check_user_access('particular_create');
        $subheads = SubHead::where('is_deleted', 0)->get();
        $codeNo = Particular::model()->code_number();

        $this->model['page_title'] = "Particular Information";
        $this->model['subheads'] = $subheads;
        $this->model['codeNo'] = $codeNo;
        return view('account.particular.create', $this->model);
    }

    public function create_particular($id) {
        check_user_access('particular_create');
        $subheads = SubHead::where('is_deleted', 0)->get();
        $id = $id;
        return view('account.particular.create', compact('subheads', 'id'));
    }

    public function store(Request $r) {
        //pr($_POST);
        $input = $r->all();
        $rule = array(
            'subhead_id' => 'required',
            'name' => 'required',
        );
        $messages = array(
            'subhead_id.required' => 'Sub Head must be selected.',
            'name.required' => 'Name Should not be empty.',
        );

        $valid = Validator::make($input, $rule, $messages);
        if ($valid->fails()) {
            return redirect()->back()->withErrors($valid)->withInput();
        }

        DB::beginTransaction();
        try {
            $head = SubHead::find($r->subhead_id)->head_id;
            $model = new Particular();
            $model->head_id = $head;
            $model->subhead_id = $r->subhead_id;
            $model->name = $r->name;
            $model->code = !empty($_POST['code']) ? $_POST['code'] : time();
            $model->bank_name = $r->bank_name;
            $model->account_name = $r->account_name;
            $model->account_no = $r->account_no;
            $model->mobile = $r->mobile;
            $model->account_type = $r->account_type;
            $model->cc_loan_amount = $r->cc_loan_amount;
            $model->address = $r->address;
            $model->_key = uniqueKey();
            if (!$model->save()) {
                throw new Exception("Error while Creating Records.");
            }

            DB::commit();
            if (!empty($r->id)) {
                if ($r->id == 'p') {
                    return redirect('/ricemill/purchase-challan/create')->with('success', 'New Record Created Successfully.');
                } elseif ($r->id == 's') {
                    return redirect('/ricemill/sales-challan/create')->with('success', 'New Record Created Successfully.');
                } else {
                    return redirect('/particulars')->with('success', 'New Record Created Successfully.');
                }
            } else {
                return redirect('/particulars')->with('success', 'New Record Created Successfully.');
            }
        } catch (Exception $e) {
            DB::rollback();
            return redirect()->back()->with('danger', $e->getMessage());
        }
    }

    public function edit($id) {
        check_user_access('particular_edit');
        $data = Particular::where('_key', $id)->first();
        $heads = SubHead::where('is_deleted', 0)->whereIn('institute_id', $this->user_companies())->get();

        $this->model['page_title'] = "Particular Information";
        $this->model['data'] = $data;
        $this->model['heads'] = $heads;
        return view('account.particular.edit', $this->model);
    }

    public function update(Request $r, $id) {
        // pr($_POST);
        $input = $r->all();
        $rule = array(
            'name' => 'required',
            'subhead_id' => 'required',
        );
        $messages = array(
            'name.required' => 'Name Should not be empty.',
            'subhead_id.required' => 'Please select a subhead.',
        );

        $valid = Validator::make($input, $rule, $messages);
        if ($valid->fails()) {
            return redirect()->back()->withErrors($valid)->withInput();
        }

        DB::beginTransaction();
        try {
            $data = Particular::find($id);
            $data->name = $r->name;
            $data->code = !empty($_POST['code']) ? $_POST['code'] : time();
            $data->subhead_id = $r->subhead_id;
            $data->mobile = $r->mobile;
            $data->account_type = $r->account_type;
            $data->cc_loan_amount = $r->cc_loan_amount;
            $data->address = $r->address;
            if (!$data->save()) {
                throw new Exception("Query Problem on Updating Record.");
            }

            DB::commit();
            return redirect('/particulars')->with('success', 'Record Updated Successfully.');
        } catch (Exception $e) {
            DB::rollback();
            return redirect()->back()->with('danger', $e->getMessage());
        }
    }

    public function ledger($id) {
        $particular = Particular::find($id);
        $query = Transaction::where([['cr_particular_id', $id], ['is_deleted', 0]])->orWhere([['dr_particular_id', $id], ['is_deleted', 0]])->orderBy('date', 'ASC');
        $_dataset = $query->paginate($this->getSettings()->pagesize);
        $startDate = Transaction::model()->start_end_date();
        $_curPage = isset($_GET['page']) ? $_GET['page'] : 1;
        $_prev_dataset = [];
        if ($_curPage > 1) {
            $prev_query = $query;
            $_prev_dataset = $prev_query->skip(0)->take(($_curPage - 1) * $this->getSettings()->pagesize)->get();
        }

        $this->model['page_title'] = "Particular Ledger";
        $this->model['id'] = $id;
        $this->model['particular'] = $particular;
        $this->model['prevDataset'] = $_prev_dataset;
        $this->model['dataset'] = $_dataset;
        $this->model['prev_date'] = $startDate;
        $this->model['curPage'] = $_curPage;
        return view('account.particular.ledger', $this->model);
    }

    public function search_ledger(Request $r) {
        $item_count = !empty($r->item_count) ? $r->item_count : $this->getSettings()->pagesize;
        $id = $r->particularId;
        $from_date = $r->from_date;
        $end_date = $r->end_date;
        $particular = Particular::find($id);

        $query = Transaction::where('is_deleted', 0);
        $query->where(function ($q) use ($id) {
            $q->where('cr_particular_id', $id)->orWhere('dr_particular_id', $id);
        });
        if (!empty($from_date) && !empty($end_date)) {
            $query->whereBetween('date', [date_ymd($from_date), date_ymd($end_date)]);
        }
        $query->orderBy('date', 'ASC');
        $dataset = $query->paginate($item_count);

        $startDate = Transaction::model()->start_end_date();
        $prevDate = !empty($from_date) ? date("Y-m-d", strtotime($from_date . ' -1 day')) : $startDate;
        $_particularBalance = Transaction::model()->particularBalance($id, [$startDate, $prevDate]);
        $_particularDebit = ($_particularBalance < 0) ? abs($_particularBalance) : 0;
        $_particularCredit = ($_particularBalance > 0) ? abs($_particularBalance) : 0;

        $_curPage = $r->has('page') ? $r->get('page') : 1;
        $_prev_dataset = [];
        if ($_curPage > 1) {
            $prev_query = $query;
            $_prev_dataset = $prev_query->skip(0)->take(($_curPage - 1) * $item_count)->get();
        }

        $this->model['id'] = $id;
        $this->model['particular'] = $particular;
        $this->model['particularBalance'] = $_particularBalance;
        $this->model['particular_debit'] = ($_curPage == 1) ? $_particularDebit : 0;
        $this->model['particular_credit'] = ($_curPage == 1) ? $_particularCredit : 0;
        $this->model['prevDataset'] = $_prev_dataset;
        $this->model['dataset'] = $dataset;
        $this->model['prev_date'] = !empty($from_date) ? date("Y-m-d", strtotime($from_date . ' -1 day')) : $startDate;
        $this->model['from_date'] = $from_date;
        $this->model['end_date'] = $end_date;
        $this->model['curPage'] = $_curPage;
        return view('account.particular._ledger', $this->model);
    }

    public function openingBalance() {
        $insList = $this->userInstitutes();
        return view('account.particular.opening_balance', compact('insList'));
    }

    public function getOpeningBalanceForm(Request $r) {
        $particulars = Particular::where([['subhead_id', $r->subhead_id], ['is_deleted', 0]])->get();
        return view('account.particular.opening_balance_form', compact('particulars'));
    }

    public function storeOpeningBalance(Request $r) {
        $modelSubhead = new Subhead();
        $resp = array();

        DB::beginTransaction();
        try {
            $existTrans = Transaction::whereNotNull('particular_balance_id')->delete();
            foreach ($_POST['particular_id'] as $key => $value) {
                $existBalance = ParticularBalance::where('particular_id', $key)->first();

                if (!empty($existBalance)) {
                    $model = $existBalance;
                } else {
                    $model = new ParticularBalance();
                }

                $opening_head = HEAD_CAPITAL;
                $opening_subhead = SUBHEAD_OPENING_BALANCE;

                $model->date = date('Y-m-d');
                $model->type = $_POST['type'][$key];
                $model->opening_date = date_ymd($r->date);
                $model->head_id = $modelSubhead->find($r->subhead_id)->head_id;
                $model->subhead_id = $r->subhead_id;
                $model->particular_id = $key;
                $model->opening_balance = $_POST['opening_balance'][$key];

                if (!$model->save()) {
                    throw new Exception("Error while Creating records.");
                }

                if ($model->type == 'Debit') {
                    $dr_head = $model->head_id;
                    $dr_subhead = $model->subhead_id;
                    $dr_particular = $model->particular_id;
                    $cr_head = $opening_head;
                    $cr_subhead = $opening_subhead;
                    $cr_particular = NULL;
                } else {
                    $dr_head = $opening_head;
                    $dr_subhead = $opening_subhead;
                    $dr_particular = NULL;
                    $cr_head = $model->head_id;
                    $cr_subhead = $model->subhead_id;
                    $cr_particular = $model->particular_id;
                }

                $modelTrans = new Transaction();
                $modelTrans->particular_balance_id = $model->id;
                $modelTrans->institute_id = institute_id();
                $modelTrans->type = NULL;
                $modelTrans->voucher_type = 'Due Voucher';
                $modelTrans->date = $model->opening_date;
                $modelTrans->payment_method = NULL;
                $modelTrans->dr_head_id = $dr_head;
                $modelTrans->dr_subhead_id = $dr_subhead;
                $modelTrans->dr_particular_id = $dr_particular;
                $modelTrans->cr_head_id = $cr_head;
                $modelTrans->cr_subhead_id = $cr_subhead;
                $modelTrans->cr_particular_id = $cr_particular;
                $modelTrans->by_whom = $r->by_whom;
                $modelTrans->description = 'Party Opening Balance';
                $modelTrans->debit = $model->opening_balance;
                $modelTrans->credit = $model->opening_balance;
                $modelTrans->amount = $model->opening_balance;
                $modelTrans->note = $modelTrans->description;
                $modelTrans->created_by = Auth::id();
                $modelTrans->_key = uniqueKey();
                if ($modelTrans->amount > 0) {
                    if (!$modelTrans->save()) {
                        throw new Exception("Error while Creating Particular Balance Transaction.");
                    }
                }
            }
            DB::commit();
            $resp['success'] = true;
            $resp['message'] = 'Data Inserted successfully';
        } catch (Exception $e) {
            DB::rollback();
            $resp['success'] = false;
            $resp['message'] = $e->getMessage();
        }

        return $resp;
    }

    // Ajax Functions
    public function search(Request $r) {
        $item_count = !empty($r->item_count) ? $r->item_count : $this->getSettings()->pagesize;
        $subhead = $r->subhead_id;
        $search = $r->search;
        $tmodel = new Transaction();

        $model = new Particular();
        $query = $model->where('is_deleted', 0);

        if (!empty($subhead)) {
            $query->where('subhead_id', $subhead);
        }
        if (!empty($search)) {
            $query->where('name', 'like', '%' . $search . '%')->orWhere('code', 'like', '%' . $search . '%');
        }
        $query->orderBy('id', 'DESC');
        $dataset = $query->paginate($item_count);

        $this->model['dataset'] = $dataset;
        $this->model['tmodel'] = $tmodel;
        return view('account.particular._list', $this->model);
    }

    public function customer_due_search(Request $r) {
        $item_count = !empty($r->item_count) ? $r->item_count : $this->getSettings()->pagesize;
        $sort_by = 'name';
        $sort_type = $r->input('sort_type');
        $search = $r->input('search');
        $query = Particular::where('head_id', HEAD_DEBTORS);
        if (!empty($search)) {
            $query->where('name', 'like', '%' . $search . '%');
        }
        $query->orderBy($sort_by, $sort_type);
        $dataset = $query->paginate($item_count);
        $tmodel = new Transaction();
        return view('account.particular.customer_due_search', compact('dataset', 'tmodel'));
    }

    public function customer_debit_search(Request $r) {
        $item_count = !empty($r->item_count) ? $r->item_count : $this->getSettings()->pagesize;
        $sort_by = 'name';
        $sort_type = $r->input('sort_type');
        $search = $r->input('search');
        $query = Particular::where('head_id', HEAD_DEBTORS);
        if (!empty($search)) {
            $query->where('name', 'like', '%' . $search . '%');
        }
        $query->orderBy($sort_by, $sort_type);
        $dataset = $query->paginate($item_count);
        $tmodel = new Transaction();
        return view('account.particular.customer_debit_search', compact('dataset', 'tmodel'));
    }

    public function supplier_due_search(Request $r) {
        $item_count = !empty($r->item_count) ? $r->item_count : $this->getSettings()->pagesize;
        $sort_by = 'name';
        $sort_type = $r->input('sort_type');
        $search = $r->input('search');
        $query = Particular::where('head_id', HEAD_CREDITORS);
        if (!empty($search)) {
            $query->where('name', 'like', '%' . $search . '%');
        }
        $query->orderBy($sort_by, $sort_type);
        $dataset = $query->paginate($item_count);
        $tmodel = new Transaction();
        return view('account.particular.supplier_due_search', compact('dataset', 'tmodel'));
    }

    public function supplier_debit_search(Request $r) {
        $item_count = !empty($r->item_count) ? $r->item_count : $this->getSettings()->pagesize;
        $sort_by = 'name';
        $sort_type = $r->input('sort_type');
        $search = $r->input('search');
        $query = Particular::where('head_id', HEAD_CREDITORS);
        if (!empty($search)) {
            $query->where('name', 'like', '%' . $search . '%');
        }
        $query->orderBy($sort_by, $sort_type);
        $dataset = $query->paginate($item_count);
        $tmodel = new Transaction();
        return view('account.particular.supplier_debit_search', compact('dataset', 'tmodel'));
    }

    public function delete() {
        //pr($_POST);
        $tmodel = new Transaction();

        DB::beginTransaction();
        try {
            foreach ($_POST['data'] as $id) {
                $data = Particular::find($id);

                $existTrans = $tmodel->where([['dr_particular_id', $id], ['is_deleted', 0]])->orWhere([['cr_particular_id', $id], ['is_deleted', 0]])->first();
                if (!empty($existTrans)) {
                    throw new Exception("Warning! One or More Transaction Exist. Please Delete Transaction First.");
                }

                $data->is_deleted = 1;
                $data->deleted_by = Auth::id();
                $data->deleted_at = cur_date_time();
                if (!$data->save()) {
                    throw new Exception("Error while deleting records.");
                }
            }

            DB::commit();
            $this->response['success'] = true;
            $this->response['message'] = 'Record/s deleted successfully.';
        } catch (Exception $e) {
            DB::rollback();
            $this->response['success'] = false;
            $this->response['message'] = $e->getMessage();
        }

        return $this->response;
    }

    public function get_sub_head(Request $r) {
        //$dataset = SubHead::where([['head_id', $r->head], ['is_deleted', 0]])->get();
        $querySubhead = SubHead::where('is_deleted', 0);
        if (!empty($r->head)) {
            $querySubhead->where('head_id', $r->head)->orWhere('common', COMMON_YES);
        }
        $dataset = $querySubhead->orderBy('name', 'ASC')->get();
        $str = ["<option value=''>Select Subhead</option>"];
        if (!empty($dataset)) {
            foreach ($dataset as $data) {
                $str[] = "<option value='$data->id'>{$data->name}</option>";
            }
            return $str;
        }
    }

    public function get_particular(Request $r) {
        $dataset = Particular::where([['subhead_id', $r->head], ['is_deleted', 0]])->get();
        $str = ["<option value=''>Select Particular</option>"];
        if (!empty($dataset)) {
            foreach ($dataset as $data) {
                $str[] = "<option value='$data->id'>{$data->name} ({$data->address})</option>";
            }
            return $str;
        }
    }

}
