<?php

namespace App\Http\Controllers\Account;

use Auth;
use DB;
use Exception;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\HomeController;
use App\Models\Institute;
use App\Models\Account\CheckRegister;
use App\Models\Account\Head;
use App\Models\Account\SubHead;
use App\Models\Account\Particular;
use App\Models\Account\Transaction;

class CheckRegisterController extends HomeController {

    public function index() {
        $model = new CheckRegister();
        $query = $model->where('is_deleted', 0)->whereIn('institute_id', $this->user_companies());
        $query->orderBy('register_date', 'DESC');
        $dataset = $query->paginate($this->getSettings()->pagesize);

        $insList = Institute::get();
        return view('account.check_register.index', compact('dataset', 'insList'));
    }

    public function create($tp) {
        $model = new SubHead();
        $insList = $this->userInstitutes();
        $heads = $model->where('institute_id', institute_id())->get();
        return view('account.check_register.create', compact('tp', 'heads', 'insList'));
    }

    public function store(Request $r) {
        //pr($_POST);
        $input = $r->all();
        $rules = array(
            'date' => 'required',
            'frm_subhead' => 'required',
            'to_subhead' => 'required',
            'amount' => 'required',
        );
        if (is_Admin()) {
            $rules['institute_id'] = 'required';
        }
        $messages = array(
            'frm_subhead.required' => 'Please select From Head',
            'to_subhead.required' => 'Please select To Head',
            'subhead.required' => 'Please select a subhead',
            'amount.required' => 'You must provide a amount',
        );

        $valid = Validator::make($input, $rules, $messages);
        if ($valid->fails()) {
            return redirect()->back()->withErrors($valid)->withInput();
        }

        if ($r->type == 'd') {
            $type = strtoupper($r->type);
            $voucher_type = RECEIVE_VOUCHER;
        } elseif ($r->type == 'c') {
            $type = strtoupper($r->type);
            $voucher_type = PAYMENT_VOUCHER;
        } else {
            $type = NULL;
            $voucher_type = JOURNAL_VOUCHER;
        }

        $mSubhead = new SubHead();
        $cr_subhead = $r->frm_subhead;
        $cr_head = $mSubhead->find($cr_subhead)->head_id;
        $cr_particular = $r->frm_particular;
        $dr_subhead = $r->to_subhead;
        $dr_head = $mSubhead->find($dr_subhead)->head_id;
        $dr_particular = $r->to_particular;

        $model = new Transaction();
        $model->type = $type;
        $model->institute_id = !empty($r->institute_id) ? $r->institute_id : institute_id();
        $model->voucher_type = $voucher_type;
        $model->date = date_ymd($r->date);
        $model->payment_method = $r->payment_method;
        $model->bank_info = isset($r->bank_info) ? $r->bank_info : NULL;
        $model->check_issue_date = isset($r->check_issue_date) ? date_ymd($r->check_issue_date) : NULL;
        $model->dr_head_id = $dr_head;
        $model->dr_subhead_id = $dr_subhead;
        $model->dr_particular_id = $dr_particular;
        $model->cr_head_id = $cr_head;
        $model->cr_subhead_id = $cr_subhead;
        $model->cr_particular_id = $cr_particular;
        $model->by_whom = $r->by_whom;
        $model->description = $r->description;
        $model->debit = $r->amount;
        $model->credit = $r->amount;
        $model->amount = $r->amount;
        $model->note = $r->description;
        $model->created_by = Auth::id();
        $model->is_edible = 1;
        $model->_key = uniqueKey();

        DB::beginTransaction();
        try {
            if (!$model->save()) {
                throw new Exception("Query Problem on Creating Transaction.");
            }

            $_transactionID = DB::getPdo()->lastInsertId();
            if ($model->payment_method == PAYMENT_BANK) {
                $_checkRegister = new CheckRegister();
                $_checkRegister->transaction_id = $_transactionID;
                $_checkRegister->institute_id = !empty($r->institute_id) ? $r->institute_id : institute_id();
                $_checkRegister->from_head_id = $dr_head;
                $_checkRegister->from_subhead_id = $dr_subhead;
                $_checkRegister->from_particular_id = $dr_particular;
                $_checkRegister->to_head_id = $cr_head;
                $_checkRegister->to_subhead_id = $cr_subhead;
                $_checkRegister->to_particular_id = $cr_particular;
                $_checkRegister->register_date = date_ymd($r->date);
                $_checkRegister->issue_date = isset($r->check_issue_date) ? date_ymd($r->check_issue_date) : NULL;
                $_checkRegister->bank_info = isset($r->bank_info) ? $r->bank_info : NULL;
                $_checkRegister->note = $r->description;
                $_checkRegister->by_whom = $r->by_whom;
                $_checkRegister->amount = $r->amount;
                $_checkRegister->status = ORDER_PENDING;
                $_checkRegister->created_at = cur_date_time();
                $_checkRegister->created_by = Auth::id();
                $_checkRegister->_key = $model->_key;
                if (!$_checkRegister->save()) {
                    throw new Exception("Error while saving check register record.");
                }
            }

            DB::commit();
            return redirect("/transactions/create/{$r->type}")->with('success', 'New Record Created Successfully.');
        } catch (Exception $e) {
            DB::rollback();
            return redirect()->back()->with('danger', $e->getMessage());
        }
    }

    public function edit($id) {
        $insList = $this->userInstitutes();
        $data = Transaction::where('_key', $id)->first();
        $heads = SubHead::where('institute_id', $data->institute_id)->get();
        $particular = new Particular ();
        return view('account.check_register.edit', compact('insList', 'data', 'heads', 'particular'));
    }

    public function show($id) {
        $data = Transaction::where('_key', $id)->first();
        $subhead = new SubHead();
        return view('account.check_register.view', compact('data', 'subhead'));
    }

    public function update(Request $r, $id) {
        $input = $r->all();
        $rules = array(
            'date' => 'required',
            'frm_subhead' => 'required',
            'to_subhead' => 'required',
            'amount' => 'required',
        );
        $messages = array(
            'frm_subhead.required' => 'Please select From Head',
            'to_subhead.required' => 'Please select To Head',
            'subhead.required' => 'Please select a subhead',
            'amount.required' => 'You must provide a amount',
        );

        $valid = Validator::make($input, $rules, $messages);
        if ($valid->fails()) {
            return redirect()->back()->withErrors($valid)->withInput();
        }

        $mSubhead = new SubHead();
        $cr_subhead = $r->frm_subhead;
        $cr_head = $mSubhead->find($cr_subhead)->head_id;
        $cr_particular = $r->frm_particular;
        $dr_subhead = $r->to_subhead;
        $dr_head = $mSubhead->find($dr_subhead)->head_id;
        $dr_particular = $r->to_particular;

        $model = Transaction::find($id);
        $model->date = date_ymd($r->date);
        $model->dr_head_id = $dr_head;
        $model->dr_subhead_id = $dr_subhead;
        $model->dr_particular_id = $dr_particular;
        $model->cr_head_id = $cr_head;
        $model->cr_subhead_id = $cr_subhead;
        $model->cr_particular_id = $cr_particular;
        $model->payment_method = $r->payment_method;
        $model->bank_info = isset($r->bank_info) ? $r->bank_info : NULL;
        $model->check_issue_date = isset($r->check_issue_date) ? date_ymd($r->check_issue_date) : NULL;
        $model->by_whom = $r->by_whom;
        $model->description = $r->description;
        $model->debit = $r->amount;
        $model->credit = $r->amount;
        $model->amount = $r->amount;
        $model->note = $r->description;
        $model->modified_by = Auth::id();

        DB::beginTransaction();
        try {
            if ($model->payment_method == PAYMENT_BANK) {
                if (!empty($model->check_register)) {
                    $_checkRegister = $model->check_register;
                    $_checkRegister->from_head_id = $dr_head;
                    $_checkRegister->from_subhead_id = $dr_subhead;
                    $_checkRegister->from_particular_id = $dr_particular;
                    $_checkRegister->to_head_id = $cr_head;
                    $_checkRegister->to_subhead_id = $cr_subhead;
                    $_checkRegister->to_particular_id = $cr_particular;
                    $_checkRegister->register_date = date_ymd($r->date);
                    $_checkRegister->issue_date = isset($r->check_issue_date) ? date_ymd($r->check_issue_date) : NULL;
                    $_checkRegister->bank_info = isset($r->bank_info) ? $r->bank_info : NULL;
                    $_checkRegister->note = $r->description;
                    $_checkRegister->by_whom = $r->by_whom;
                    $_checkRegister->amount = $r->amount;
                    $_checkRegister->status = ORDER_PENDING;
                    $_checkRegister->modified_at = cur_date_time();
                    $_checkRegister->modified_by = $model->modified_by;
                    $_checkRegister->_key = $model->_key;
                    if (!$_checkRegister->save()) {
                        throw new Exception("Error while saving check register record.");
                    }
                } else {
                    $_checkRegister = new CheckRegister();
                    $_checkRegister->transaction_id = $model->id;
                    $_checkRegister->institute_id = $model->institute_id;
                    $_checkRegister->from_head_id = $dr_head;
                    $_checkRegister->from_subhead_id = $dr_subhead;
                    $_checkRegister->from_particular_id = $dr_particular;
                    $_checkRegister->to_head_id = $cr_head;
                    $_checkRegister->to_subhead_id = $cr_subhead;
                    $_checkRegister->to_particular_id = $cr_particular;
                    $_checkRegister->register_date = date_ymd($r->date);
                    $_checkRegister->issue_date = isset($r->check_issue_date) ? date_ymd($r->check_issue_date) : NULL;
                    $_checkRegister->bank_info = isset($r->bank_info) ? $r->bank_info : NULL;
                    $_checkRegister->note = $r->description;
                    $_checkRegister->by_whom = $r->by_whom;
                    $_checkRegister->amount = $r->amount;
                    $_checkRegister->status = ORDER_PENDING;
                    $_checkRegister->created_at = cur_date_time();
                    $_checkRegister->created_by = $model->modified_by;
                    $_checkRegister->_key = uniqueKey();
                    if (!$_checkRegister->save()) {
                        throw new Exception("Error while updating check register record.");
                    }
                }
            }

            if (!$model->save()) {
                throw new Exception("Query Problem on Updating Record.");
            }

            DB::commit();
            return redirect('/transactions')->with('success', 'Record Updated Successfully.');
        } catch (Exception $e) {
            DB::rollback();
            return redirect()->back()->with('danger', $e->getMessage());
        }
    }

    // Ajax Functions
    public function search(Request $r) {
        $item_count = !empty($r->item_count) ? $r->item_count : $this->getSettings()->pagesize;
        $institute = $r->institute_id;
        $_status = $r->status;
        $sort_by = 'register_date';
        $sort_type = 'DESC';
        $from_date = date_ymd($r->from_date);
        $end_date = !empty($r->end_date) ? date_ymd($r->end_date) : date('Y-m-d');

        $model = new CheckRegister();
        $query = $model->where('is_deleted', 0)->whereIn('institute_id', $this->user_companies());
        if (!empty($institute)) {
            $query->where('institute_id', $institute);
        }
        if (!empty($_status)) {
            $query->where('status', $_status);
        }
        if (!empty($from_date)) {
            $query->whereBetween('register_date', [$from_date, $end_date]);
        }
        $query->orderBy($sort_by, $sort_type);
        $dataset = $query->paginate($item_count);
        return view('account.check_register._list', compact('dataset'));
    }

    public function delete() {
        $resp = array();

        DB::beginTransaction();
        try {
            foreach ($_POST['data'] as $id) {
                $data = CheckRegister::find($id);
                $data->status = ORDER_PENDING;
                $data->is_deleted = 1;
                $data->deleted_at = cur_date_time();
                $data->deleted_by = Auth::id();
                if (!$data->save()) {
                    throw new Exception("Error while deleting records.");
                }
            }

            DB::commit();
            $resp['success'] = true;
            $resp['message'] = 'Record has been deleted successfully.';
        } catch (Exception $e) {
            DB::rollback();
            $resp['success'] = false;
            $resp['message'] = $e->getMessage();
        }

        return $resp;
    }

    public function process() {
        $resp = array();

        DB::beginTransaction();
        try {
            foreach ($_POST['data'] as $id) {
                $data = CheckRegister::find($id);
                $_trnData = Transaction::find($data->transaction_id);
                $_trnData->payment_method = PAYMENT_CASH;
                if (!$_trnData->save()) {
                    throw new Exception("Error while processing transaction record.");
                }

                $data->status = ORDER_PROCESSED;
                if (!$data->save()) {
                    throw new Exception("Error while processing records.");
                }
            }

            DB::commit();
            $resp['success'] = true;
            $resp['message'] = 'Record has been processed successfully.';
        } catch (Exception $e) {
            DB::rollback();
            $resp['success'] = false;
            $resp['message'] = $e->getMessage();
        }

        return $resp;
    }

    public function revert() {
        $resp = array();

        DB::beginTransaction();
        try {
            foreach ($_POST['data'] as $id) {
                $data = CheckRegister::find($id);
                $_trnData = Transaction::find($data->transaction_id);
                $_trnData->payment_method = PAYMENT_BANK;
                if (!$_trnData->save()) {
                    throw new Exception("Error while reverting transaction record.");
                }

                $data->status = ORDER_PENDING;
                if (!$data->save()) {
                    throw new Exception("Error while reverting records.");
                }
            }

            DB::commit();
            $resp['success'] = true;
            $resp['message'] = 'Record has been reverted successfully.';
        } catch (Exception $e) {
            DB::rollback();
            $resp['success'] = false;
            $resp['message'] = $e->getMessage();
        }

        return $resp;
    }

}
