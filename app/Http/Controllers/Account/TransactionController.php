<?php

namespace App\Http\Controllers\Account;

use Auth;
use DB;
use Exception;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\HomeController;
use App\Models\Hr\Designation;
use App\Models\Hr\Employee;
use App\Models\Account\Head;
use App\Models\Account\SubHead;
use App\Models\Account\Particular;
use App\Models\Account\CheckRegister;
use App\Models\Account\Transaction;
use App\Models\Transport\Vehicle;
use App\Models\Trade\SalesOrder;

//use App\Models\Trade\SaleChallan;

class TransactionController extends HomeController {

    public function index() {
        check_user_access('transaction_list');
        $insList = $this->userInstitutes();
        $heads = SubHead::where('is_deleted', 0)->get();

        $model = new Transaction();
        $query = $model->where('is_deleted', 0);
        $query->whereIn('institute_id', $this->user_companies());
        $dataset = $query->orderBy('date', 'DESC')->paginate($this->getSettings()->pagesize);

        $this->model['page_title'] = "Transaction List";
        $this->model['insList'] = $insList;
        $this->model['heads'] = $heads;
        $this->model['dataset'] = $dataset;
        return view('account.transactions.index', $this->model);
    }

    public function create($tp) {
        check_user_access('transaction_create');
        $insList = $this->userInstitutes();
        $heads = SubHead::where('is_deleted', 0)->get();
        $orders = SalesOrder::where('is_deleted', 0)->get();
        $designations = Designation::model()->getList();
        $employees = Employee::model()->getList();
        $vehicles = Vehicle::model()->getList();

        $this->model['page_title'] = "Transaction Information";
        $this->model['tp'] = $tp;
        $this->model['insList'] = $insList;
        $this->model['heads'] = $heads;
        $this->model['designations'] = $designations;
        $this->model['employees'] = $employees;
        $this->model['vehicles'] = $vehicles;
        $this->model['orders'] = $orders;
        return view('account.transactions.create', $this->model);
    }

    public function store(Request $r) {
        $input = $r->all();
        $rules = array(
            'date' => 'required',
            'frm_subhead' => 'required',
            'to_subhead' => 'required',
            'amount' => 'required',
        );
        if (is_Admin()) {
            $rules['institute_id'] = 'required';
        }
        $messages = array(
            'frm_subhead.required' => 'Please select From Head',
            'to_subhead.required' => 'Please select To Head',
            'subhead.required' => 'Please select a subhead',
            'amount.required' => 'You must provide a amount',
        );

        $valid = Validator::make($input, $rules, $messages);
        if ($valid->fails()) {
            return redirect()->back()->withErrors($valid)->withInput();
        }

        if ($r->type == 'd') {
            $type = strtoupper($r->type);
            $voucher_type = RECEIVE_VOUCHER;
        } elseif ($r->type == 'c') {
            $type = strtoupper($r->type);
            $voucher_type = PAYMENT_VOUCHER;
        } else {
            $type = NULL;
            $voucher_type = JOURNAL_VOUCHER;
        }

        $mSubhead = new SubHead();
        $cr_subhead = $r->frm_subhead;
        $cr_head = $mSubhead->find($cr_subhead)->head_id;
        $cr_particular = $r->frm_particular;
        $dr_subhead = $r->to_subhead;
        $dr_head = $mSubhead->find($dr_subhead)->head_id;
        $dr_particular = $r->to_particular;

        DB::beginTransaction();
        try {
            $model = new Transaction();
            $model->type = $type;
            $model->institute_id = !empty($r->institute_id) ? $r->institute_id : institute_id();
            $model->designation_id = !empty($r->designation_id) ? $r->designation_id : NULL;
            $model->employee_id = $r->employee_id;
            $model->vehicle_id = $r->vehicle_id;
            $model->vehicle_no = !empty($r->vehicle_id) ? Vehicle::find($r->vehicle_id)->vehicle_no : NULL;
            $model->voucher_type = $voucher_type;
            $model->date = date_ymd($r->date);
            $model->payment_method = $r->payment_method;
            $model->bank_info = isset($r->bank_info) ? $r->bank_info : NULL;
            $model->check_issue_date = isset($r->check_issue_date) ? date_ymd($r->check_issue_date) : NULL;
            $model->dr_head_id = $dr_head;
            $model->dr_subhead_id = $dr_subhead;
            $model->dr_particular_id = $dr_particular;
            $model->cr_head_id = $cr_head;
            $model->cr_subhead_id = $cr_subhead;
            $model->cr_particular_id = $cr_particular;
            $model->by_whom = $r->by_whom;
            $model->description = $r->description;
            $model->debit = $r->amount;
            $model->credit = $r->amount;
            $model->amount = $r->amount;
            $model->note = $r->description;
            $model->is_edible = 1;
            $model->created = cur_date_time();
            $model->created_by = Auth::id();
            $model->_key = uniqueKey();
            if (!$model->save()) {
                throw new Exception("Query Problem on Creating Transaction.");
            }

            $_transactionID = DB::getPdo()->lastInsertId();
            if ($model->payment_method == PAYMENT_BANK) {
                $_checkRegister = new CheckRegister();
                $_checkRegister->transaction_id = $_transactionID;
                $_checkRegister->institute_id = $model->institute_id;
                $_checkRegister->from_head_id = $dr_head;
                $_checkRegister->from_subhead_id = $dr_subhead;
                $_checkRegister->from_particular_id = $dr_particular;
                $_checkRegister->to_head_id = $cr_head;
                $_checkRegister->to_subhead_id = $cr_subhead;
                $_checkRegister->to_particular_id = $cr_particular;
                $_checkRegister->voucher_type = $model->voucher_type;
                $_checkRegister->register_date = $model->date;
                $_checkRegister->issue_date = $model->check_issue_date;
                $_checkRegister->bank_info = $model->bank_info;
                $_checkRegister->note = $model->note;
                $_checkRegister->by_whom = $model->by_whom;
                $_checkRegister->debit = $model->debit;
                $_checkRegister->credit = $model->credit;
                $_checkRegister->amount = $model->amount;
                $_checkRegister->status = ORDER_PENDING;
                $_checkRegister->created_at = cur_date_time();
                $_checkRegister->created_by = $model->created_by;
                $_checkRegister->_key = $model->_key;
                if (!$_checkRegister->save()) {
                    throw new Exception("Error while saving check register record.");
                }
            }

            DB::commit();
            return redirect("/transactions/create/{$r->type}")->with('success', 'Record Created Successfully.');
        } catch (Exception $e) {
            DB::rollback();
            return redirect()->back()->with('danger', $e->getMessage());
        }
    }

    public function edit($id) {
        check_user_access('transaction_create');
        $data = Transaction::where('_key', $id)->first();
        $insList = $this->userInstitutes();
        $heads = SubHead::where('institute_id', $data->institute_id)->get();
        $designations = Designation::model()->getList();
        $employees = Employee::model()->getList($data->designation_id);
        $vehicles = Vehicle::model()->getList();
        $particular = new Particular ();

        $this->model['page_title'] = "Transaction Information";
        $this->model['data'] = $data;
        $this->model['insList'] = $insList;
        $this->model['heads'] = $heads;
        $this->model['designations'] = $designations;
        $this->model['employees'] = $employees;
        $this->model['vehicles'] = $vehicles;
        $this->model['particular'] = $particular;
        return view('account.transactions.edit', $this->model);
    }

    public function update(Request $r, $id) {
        // pr($_POST);
        $input = $r->all();
        $rules = array(
            'date' => 'required',
            'frm_subhead' => 'required',
            'to_subhead' => 'required',
            'amount' => 'required',
        );
        $messages = array(
            'frm_subhead.required' => 'Please select From Head',
            'to_subhead.required' => 'Please select To Head',
            'subhead.required' => 'Please select a subhead',
            'amount.required' => 'You must provide a amount',
        );

        $valid = Validator::make($input, $rules, $messages);
        if ($valid->fails()) {
            return redirect()->back()->withErrors($valid)->withInput();
        }
        $mSubhead = new SubHead();
        $cr_subhead = $r->frm_subhead;
        $cr_head = $mSubhead->find($cr_subhead)->head_id;
        $cr_particular = $r->frm_particular;
        $dr_subhead = $r->to_subhead;
        $dr_head = $mSubhead->find($dr_subhead)->head_id;
        $dr_particular = $r->to_particular;

        DB::beginTransaction();
        try {
            $model = Transaction::find($id);
            $model->institute_id = !empty($r->institute_id) ? $r->institute_id : institute_id();
            $model->designation_id = !empty($r->designation_id) ? $r->designation_id : NULL;
            $model->employee_id = $r->employee_id;
            $model->vehicle_id = $r->vehicle_id;
            $model->vehicle_no = !empty($r->vehicle_id) ? Vehicle::find($r->vehicle_id)->vehicle_no : NULL;
            $model->date = date_ymd($r->date);
            $model->dr_head_id = $dr_head;
            $model->dr_subhead_id = $dr_subhead;
            $model->dr_particular_id = $dr_particular;
            $model->cr_head_id = $cr_head;
            $model->cr_subhead_id = $cr_subhead;
            $model->cr_particular_id = $cr_particular;
            $model->payment_method = $r->payment_method;
            $model->bank_info = isset($r->bank_info) ? $r->bank_info : NULL;
            $model->check_issue_date = isset($r->check_issue_date) ? date_ymd($r->check_issue_date) : NULL;
            $model->by_whom = $r->by_whom;
            $model->description = $r->description;
            $model->debit = $r->amount;
            $model->credit = $r->amount;
            $model->amount = $r->amount;
            $model->note = $r->description;
            //$model->license_id = $r->license_no;
            $model->sales_order_id = $r->order_no;
            $model->challan_no = $r->challan_no;
            //$model->project_id = $r->project_name;
            //$model->department_id = $r->department_id;
            //$model->contract_person_id = $r->contract_person_name;
            $model->modified = cur_date_time();
            $model->modified_by = Auth::id();
            if ($model->payment_method == PAYMENT_BANK) {
                if (!empty($model->check_register)) {
                    $_checkRegister = $model->check_register;
                    $_checkRegister->from_head_id = $dr_head;
                    $_checkRegister->from_subhead_id = $dr_subhead;
                    $_checkRegister->from_particular_id = $dr_particular;
                    $_checkRegister->to_head_id = $cr_head;
                    $_checkRegister->to_subhead_id = $cr_subhead;
                    $_checkRegister->to_particular_id = $cr_particular;
                    $_checkRegister->voucher_type = $model->voucher_type;
                    $_checkRegister->register_date = $model->date;
                    $_checkRegister->issue_date = $model->check_issue_date;
                    $_checkRegister->bank_info = $model->bank_info;
                    $_checkRegister->note = $model->note;
                    $_checkRegister->by_whom = $model->by_whom;
                    $_checkRegister->debit = $model->debit;
                    $_checkRegister->credit = $model->credit;
                    $_checkRegister->amount = $model->amount;
                    $_checkRegister->status = ORDER_PENDING;
                    $_checkRegister->modified_at = cur_date_time();
                    $_checkRegister->modified_by = $model->modified_by;
                    $_checkRegister->_key = $model->_key;
                    if (!$_checkRegister->save()) {
                        throw new Exception("Error while saving check register record.");
                    }
                } else {
                    $_checkRegister = new CheckRegister();
                    $_checkRegister->transaction_id = $model->id;
                    $_checkRegister->institute_id = $model->institute_id;
                    $_checkRegister->from_head_id = $dr_head;
                    $_checkRegister->from_subhead_id = $dr_subhead;
                    $_checkRegister->from_particular_id = $dr_particular;
                    $_checkRegister->to_head_id = $cr_head;
                    $_checkRegister->to_subhead_id = $cr_subhead;
                    $_checkRegister->to_particular_id = $cr_particular;
                    $_checkRegister->register_date = $model->date;
                    $_checkRegister->issue_date = $model->check_issue_date;
                    $_checkRegister->bank_info = $model->bank_info;
                    $_checkRegister->note = $model->description;
                    $_checkRegister->by_whom = $model->by_whom;
                    $_checkRegister->debit = $model->debit;
                    $_checkRegister->credit = $model->credit;
                    $_checkRegister->amount = $model->amount;
                    $_checkRegister->status = ORDER_PENDING;
                    $_checkRegister->created_at = cur_date_time();
                    $_checkRegister->created_by = $model->modified_by;
                    $_checkRegister->_key = uniqueKey();
                    if (!$_checkRegister->save()) {
                        throw new Exception("Error while updating check register record.");
                    }
                }
            } else {
                if (!empty($model->check_register)) {
                    if (!$model->check_register->delete()) {
                        throw new Exception("Error while removing check register record.");
                    }
                }
            }
            if (!$model->save()) {
                throw new Exception("Query Problem on Updating Record.");
            }

            DB::commit();
            return redirect('/transactions')->with('success', 'Record Updated Successfully.');
        } catch (Exception $e) {
            DB::rollback();
            return redirect()->back()->with('danger', $e->getMessage());
        }
    }

    public function show($id) {
        $data = Transaction::where('_key', $id)->first();
        $subhead = new SubHead();
        $this->model['data'] = $data;
        $this->model['subhead'] = $subhead;
        return view('account.transactions.view', $this->model);
    }

    // Ajax Functions
    public function search(Request $r) {
        $item_count = !empty($r->item_count) ? $r->item_count : $this->getSettings()->pagesize;
        $institute = $r->institute_id;
        $subheadID = $r->head_id;
        $voucher = $r->voucher_type;
        $pay_method = $r->pay_method;
        $from_date = date_ymd($r->from_date);
        $end_date = !empty($r->end_date) ? date_ymd($r->end_date) : date('Y-m-d');
        $search = $r->search;
        $heads = Head::where('institute_id', institute_id())->get();

        $model = new Transaction();
        $query = $model->where('is_deleted', 0);
        $query->whereIn('institute_id', $this->user_companies());
        if (!empty($institute)) {
            $query->where('institute_id', $institute);
        }
        if (!empty($subheadID)) {
            if (!empty($voucher)) {
                $query->where('voucher_type', $voucher);
            }
            if (!empty($from_date) && !empty($end_date)) {
                $query->whereBetween('date', [$from_date, $end_date]);
            }
            $query->where([['dr_subhead_id', $subheadID], ['is_deleted', 0]])->orWhere([['cr_subhead_id', $subheadID], ['is_deleted', 0]]);
        }
        if (!empty($voucher)) {
            $query->where('voucher_type', $voucher);
        }
        if (!empty($pay_method)) {
            $query->where('payment_method', $pay_method);
        }
        if (!empty($from_date) && !empty($end_date)) {
            $query->whereBetween('date', [$from_date, $end_date]);
        }
        if (!empty($search)) {
            $headarr = [];
            $dr_head = Particular::where('name', 'like', '%' . $search . '%')->get();
            foreach ($dr_head as $head) {
                $headarr[] = $head->id;
            }
            $query->whereIn('cr_particular_id', $headarr)->orWhereIn('dr_particular_id', $headarr)->where('is_deleted', 0);
        }
        $query->orderBy('date', 'DESC');
        $dataset = $query->paginate($item_count);

        $this->model['dataset'] = $dataset;
        $this->model['heads'] = $heads;
        return view('account.transactions._list', $this->model);
    }

    public function delete() {
        //pr($_POST);
        DB::beginTransaction();
        try {
            foreach ($_POST['data'] as $id) {
                $_check_registers = CheckRegister::where("transaction_id", $id)->get();
                foreach ($_check_registers as $_register) {
                    if (!$_register->delete()) {
                        throw new Exception("Error while deleting check register records.");
                    }
                }

                $data = Transaction::find($id);
                $data->is_deleted = 1;
                $data->deleted_by = Auth::id();
                $data->deleted_at = cur_date_time();
                if (!$data->save()) {
                    throw new Exception("Error while deleting records.");
                }
            }

            DB::commit();
            $this->response['success'] = true;
            $this->response['message'] = 'Records has been deleted successfully';
        } catch (Exception $e) {
            DB::rollback();
            $this->response['success'] = false;
            $this->response['message'] = $e->getMessage();
        }

        return $this->response;
    }

    public function expenses() {
        //check_user_access('transaction_expense');
        $dataset = Transaction::where('voucher_type', PAYMENT_VOUCHER)->get();
        $head = new Head();
        $subhead = new SubHead();

        $this->model['dataset'] = $dataset;
        $this->model['head'] = $head;
        $this->model['subhead'] = $subhead;
        return view('account.transactions.expense', $this->model);
    }

    public function expense_search(Request $r) {
        $item_count = $r->item_count;
        $search_by = $r->head_id;
        $from_date = date_ymd($r->from_date);
        $end_date = !empty($r->end_date) ? date_ymd($r->end_date) : date('Y-m-d');
        $search = $r->search;
        $head = new Head();
        $subhead = new SubHead();

        $query = Transaction::where('voucher_type', PAYMENT_VOUCHER);
        if (!empty($from_date)) {
            $query->whereBetween('pay_date', [$from_date, $end_date]);
        }
        if (!empty($search_by)) {
            $query->where('ledger_head_id', $search_by);
        }
        if (!empty($search)) {
            $headarr = [];
            $dr_head = SubHead::where('name', 'like', '%' . $search . '%')->get();
            foreach ($dr_head as $head) {
                $headarr[] = $head->id;
            }
            $query->whereIn('sub_head_id', $headarr);
        }
        $query->orderBy('pay_date', 'DESC');
        $dataset = $query->paginate($item_count);

        $this->model['dataset'] = $dataset;
        $this->model['head'] = $head;
        $this->model['subhead'] = $subhead;
        return view('account.transactions._expense_list', $this->model);
    }

}
