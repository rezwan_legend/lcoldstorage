<?php

namespace App\Http\Controllers\Account;

use Auth;
use DB;
use Exception;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\HomeController;
use App\Models\Hr\Designation;
use App\Models\Hr\Employee;
use App\Models\Account\SubHead;
use App\Models\Transport\Vehicle;
use App\Models\Account\Voucher;
use App\Models\Account\VoucherItem;
use App\Models\Account\Particular;
use App\Models\Account\Transaction;

class VoucherController extends HomeController {

    public function index() {
        check_user_access('journal_list');
        $insList = $this->userInstitutes();
        $heads = SubHead::where('is_deleted', 0)->get();
        $designations = Designation::model()->getList();
        $employees = Employee::model()->getList();
        $vehicles = Vehicle::model()->getList();
        $dataset = Voucher::where('is_deleted', 0)->orderBy('date', 'DESC')->get();

        $this->model['page_title'] = "Accunt Jurnal";
        $this->model['insList'] = $insList;
        $this->model['heads'] = $heads;
        $this->model['designations'] = $designations;
        $this->model['employees'] = $employees;
        $this->model['vehicles'] = $vehicles;
        $this->model['dataset'] = $dataset;
        return view('account.voucher.index', $this->model);
    }

    public function create() {
        check_user_access('journal_create');
        $insList = $this->userInstitutes();
        $heads = SubHead::where('is_deleted', 0)->get();
        $designations = Designation::model()->getList();
        $employees = Employee::model()->getList();
        $vehicles = Vehicle::model()->getList();

        $this->model['page_title'] = "Create Voucher";
        $this->model['insList'] = $insList;
        $this->model['heads'] = $heads;
        $this->model['designations'] = $designations;
        $this->model['employees'] = $employees;
        $this->model['vehicles'] = $vehicles;
        return view('account.voucher.create', $this->model);
    }

    public function store(Request $r) {
        // pr($r->institute_id);
        DB::beginTransaction();
        try {
            $model = new Voucher();
            $model->date = !empty($r->date) ? date_ymd($r->date) : date('Y-m-d');
            $model->company_id = $r->institute_id;
            $model->voucher_no = $r->voucher_no;
            $model->by_whom = $r->by_whom;
            $model->designation_id = $r->designation_id;
            $model->employee_id = $r->employee_id;
            $model->vehicle_no = $r->vehicle_id;
            $model->description = $r->description;
            $model->created_by = Auth::id();
            $model->created_at = cur_date_time();
            $model->_key = uniqueKey();
            if (!$model->save()) {
                throw new Exception("Error! while creating purchase.");
            }
            DB::commit();
            return redirect("/account/voucher/voucherlist/{$model->_key}")->with('success', 'Voucher order create Successfully.');
        } catch (Exception $e) {
            DB::rollback();
            return redirect()->back()->with('danger', $e->getMessage());
        }
    }

    public function show($id) {
        check_user_access('journal_details');
        $data = Voucher::find($id);
        $_itemDataset = VoucherItem::where('order_id', $data->id)->get();
        $this->model['itemDataset'] = $_itemDataset;
        $this->model['data'] = $data;
        return view('account.voucher.details', $this->model);
    }

    public function edit($id) {
        check_user_access('journal_edit');
        $data = Voucher::where('_key', $id)->first();
        $insList = $this->userInstitutes();
        $heads = SubHead::where('is_deleted', 0)->get();
        $designations = Designation::model()->getList();
        $employees = Employee::model()->getList();
        $vehicles = Vehicle::model()->getList();
//        pr($data);
        $this->model['page_title'] = "Voucher Edit";
        $this->model['insList'] = $insList;
        $this->model['heads'] = $heads;
        $this->model['designations'] = $designations;
        $this->model['employees'] = $employees;
        $this->model['vehicles'] = $vehicles;
        $this->model['data'] = $data;
        return view('account.voucher.edit', $this->model);
    }

    public function update(Request $r, $id) {
        // pr($_POST);
        $input = $r->all();
        $rule = array(
            'institute_id' => 'required',
            'voucher_no' => 'required',
        );
        $messages = array(
            'institute_id.required' => 'Required',
            'voucher_no.required' => 'Required',
        );

        $valid = Validator::make($input, $rule, $messages);
        if ($valid->fails()) {
            return redirect()->back()->withErrors($valid)->withInput();
        }
        DB::beginTransaction();
        try {
            $model = Voucher::find($id);
            $model->date = date_ymd($r->date);
            $model->company_id = $r->institute_id;
            $model->voucher_no = $r->voucher_no;
            $model->by_whom = $r->by_whom;
            $model->designation_id = $r->designation_id;
            $model->employee_id = $r->employee_id;
            $model->vehicle_no = $r->vehicle_id;
            $model->description = $r->description;
            $model->modified_by = Auth::id();
            if (!$model->save()) {
                throw new Exception("Error! while updating voucher.");
            }

            foreach ($model->items as $item) {
                $item->date = $model->date;
                $item->company_id = $model->company_id;
                $item->description = $model->description;
                $item->voucher_no = $model->voucher_no;
                $item->employee_id = $model->employee_id;
                $item->vehicle_no = $model->vehicle_no;
                if (!$item->save()) {
                    throw new Exception("Error while processing itmes.");
                }
            }
            DB::commit();
            return redirect('account/voucher/')->with('success', 'Voucher Updated Successfully.');
        } catch (Exception $e) {
            DB::rollback();
            return redirect()->back()->with('danger', $e->getMessage());
        }
    }

    public function search(Request $r) {
        //pr($_POST);
        $item_count = !empty($r->item_count) ? $r->item_count : $this->getSettings()->pagesize;
        $institute = $r->institute_id;
        $from_date = date_ymd($r->from_date);
        $end_date = !empty($r->end_date) ? date_ymd($r->end_date) : date('Y-m-d');
        $search = $r->search;

        $model = new Voucher();
        $query = $model->where('is_deleted', 0);
        if (!empty($institute)) {
            $query->where('company_id', $institute);
        }
        if (!empty($search)) {
            $query->where('by_whom', 'like', '%' . $search . '%')
                    ->orWhere('voucher_no', 'like', '%' . $search . '%')
                    ->get();
        }
        if (!empty($from_date) && !empty($end_date)) {
            $query->whereBetween('date', [$from_date, $end_date]);
        }

        $query->orderBy('date', 'DESC');
        $dataset = $query->paginate($item_count);
        $this->model['dataset'] = $dataset;
        return view('account.voucher._list', $this->model);
    }

    public function confirm($id) {
        check_user_access('journal_process');
        $data = Voucher::find($id);
        DB::beginTransaction();
        try {
            if (count($data->items) <= 0) {
                throw new Exception("Sorry !! There is no items for processing....");
            }
            //pr($data->items);
            $debitCount = 0;
            $creditCount = 0;
            if (!empty($data->items) && count($data->items) > 0) {
                foreach ($data->items as $items) {
                    if ($items->debit_amount) {
                        $debitCount++;
                    }
                    if ($items->creadit_amount) {
                        $creditCount++;
                    }
                    for ($strt = 0; $strt < $debitCount; $strt++) {
                        $transaction = new Transaction();
                        $transaction->institute_id = $data->company_id;
                        $transaction->voucher_id = $data->id;
                        $transaction->type = 'D';
                        $transaction->voucher_type = RECEIVE_VOUCHER;
                        $transaction->payment_method = PAYMENT_NO;
                        $transaction->date = $items->date;
                        $transaction->dr_head_id = $items->head_id;
                        $transaction->dr_subhead_id = $items->subhead_id;
                        $transaction->dr_particular_id = $items->particular_id;
                        $transaction->cr_head_id = $items->head_id;
//                $transaction->cr_subhead_id = $data->subhead_id;
//                $transaction->cr_particular_id = $data->particular_id;
                        $transaction->by_whom = Auth::user()->name;
                        $transaction->description = "Journal";
                        $transaction->amount = $items->debit_amount;
                        $transaction->debit = $items->debit_amount;
                        $transaction->credit = $items->debit_amount;
                        $transaction->note = "Journal";
                        $transaction->created_by = Auth::id();
                        $transaction->_key = uniqueKey();
                        if (!$transaction->save()) {
                            throw new Exception("Error while saving data in Transaction.");
                        }
                    }
                    for ($start = 0; $start < $creditCount; $start++) {
                        $transaction = new Transaction();
                        $transaction->institute_id = $items->company_id;
                        $transaction->voucher_id = $data->id;
                        $transaction->type = 'D';
                        $transaction->voucher_type = PAYMENT_VOUCHER;
                        $transaction->payment_method = PAYMENT_NO;
                        $transaction->date = $items->date;
//                $transaction->dr_head_id = $data->head_id;
//                $transaction->dr_subhead_id = $data->subhead_id;
//                $transaction->dr_particular_id = $data->particular_id;
                        $transaction->cr_head_id = $items->head_id;
                        $transaction->cr_subhead_id = $items->subhead_id;
                        $transaction->cr_particular_id = $items->particular_id;
                        $transaction->by_whom = Auth::user()->name;
                        $transaction->description = "Journal";
                        $transaction->amount = $items->creadit_amount;
                        $transaction->debit = $items->creadit_amount;
                        $transaction->credit = $items->creadit_amount;
                        $transaction->note = "Journal";
                        $transaction->created_by = Auth::id();
                        $transaction->_key = uniqueKey();
                        if (!$transaction->save()) {
                            throw new Exception("Error while saving data in Transaction.");
                        }
                    }
                }


                $data->modified_by = Auth::id();
                $data->process_status = PROCESS_COMPLETE;
                if (!$data->save()) {
                    throw new Exception("Something Went Wrong. Please Re Submit your request.");
                }
            }


            $data->modified_by = Auth::id();
            $data->process_status = PROCESS_COMPLETE;
            if (!$data->save()) {
                throw new Exception("Something Went Wrong. Please Re Submit your request.");
            }

            DB::commit();
            return redirect('account/voucher/')->with('success', 'Journal Successfully');
        } catch (Exception $e) {
            DB::rollback();
            return redirect('account/voucher/')->with('danger', $e->getMessage());
        }
    }

    public function reset($id) {
//        pr($id);
        check_user_access('journal_process_reset');
        $data = Voucher::find($id);
        DB::beginTransaction();
        try {
            if (!empty($data->transactions)) {
                foreach ($data->transactions as $transaction) {
                    if (!$transaction->delete()) {
                        throw new Exception("Error while deleting records from Transaction.");
                    }
                }
            }

            if (!empty($data->items)) {
                foreach ($data->items as $_item) {
                    $_item->process_status = PROCESS_PENDING;
                    if (!$_item->save()) {
                        throw new Exception("Error while processing items.");
                    }
                }
            }

            $data->process_status = PROCESS_PENDING;
            $data->modified_by = Auth::id();
            if (!$data->save()) {
                throw new Exception("Something Went Wrong. Please Re Submit your request.");
            }

            DB::commit();
            return redirect('account/voucher/')->with('success', 'Purchase Order Reset Successfully');
        } catch (Exception $e) {
            DB::rollback();
            return redirect('account/voucher/')->with('danger', $e->getMessage());
        }
    }

    public function delete() {
        $resp = array();
        DB::beginTransaction();
        try {
            foreach ($_POST['data'] as $id) {
                $data = Voucher::find($id);

                if (!empty($data->transactions)) {
                    foreach ($data->transactions as $transaction) {
                        if (!$transaction->delete()) {
                            throw new Exception("Error while deleting records from Transaction.");
                        }
                    }
                }

                if (!empty($data->items)) {
                    foreach ($data->items as $_item) {
                        $_item->is_deleted = 1;
                        $_item->deleted_at = cur_date_time();
                        $_item->deleted_by = Auth::id();
                        if (!$_item->save()) {
                            throw new Exception("Error while deleting records.");
                        }
                    }
                }

                $data->is_deleted = 1;
                $data->deleted_at = cur_date_time();
                $data->deleted_by = Auth::id();
                if (!$data->save()) {
                    throw new Exception("Error while deleting records.");
                }
            }

            DB::commit();
            $resp['success'] = true;
            $resp['message'] = 'Voucher Deleted Successfully';
        } catch (Exception $e) {
            DB::rollback();
            $resp['success'] = false;
            $resp['message'] = $e->getMessage();
        }
        return $resp;
    }

    public function journalList() {
        check_user_access('journal_details');
        $_itemDataset = VoucherItem::where('is_deleted', 0)->orderBy('date', 'DESC')->get();
        $subheads = SubHead::where('is_deleted', 0)->get();
        $insList = $this->userInstitutes();

        $this->model['itemDataset'] = $_itemDataset;
        $this->model['subheads'] = $subheads;
        $this->model['insList'] = $insList;
        return view('account.voucher.journallist', $this->model);
    }

    public function search_journal(Request $r) {
//        pr($_POST);
        $item_count = !empty($r->item_count) ? $r->item_count : $this->getSettings()->pagesize;
        $institute = $r->institute_id;
        $from_date = date_ymd($r->from_date);
        $end_date = !empty($r->end_date) ? date_ymd($r->end_date) : date('Y-m-d');
        $type = $r->transction_type;
        $search = $r->search;

        $model = new VoucherItem();
        $query = $model->where('is_deleted', 0);
        if (!empty($institute)) {
            $query->where('company_id', $institute);
        }
        if (!empty($type)) {
            $query->where('transaction_type', 'like', '%' . $type . '%')
                    ->get();
        }
        if (!empty($from_date) && !empty($end_date)) {
            $query->whereBetween('date', [$from_date, $end_date]);
        }

        if (!empty($search)) {
            $headarr = [];
            $dr_head = Particular::where('name', 'like', '%' . $search . '%')->get();
            foreach ($dr_head as $head) {
                $headarr[] = $head->id;
            }
            $query->whereIn('particular_id', $headarr)->where('is_deleted', 0);
        }
        $query->orderBy('date', 'DESC');
        $dataset = $query->paginate($item_count);
//        pr($dataset);
        $this->model['itemDataset'] = $dataset;
        return view('account.voucher._journallist', $this->model);
    }

    public function voucherList($id) {
        check_user_access('account_voucher_items');
        $insList = $this->userInstitutes();
        $employees = Employee::model()->getList();
        $vehicles = Vehicle::model()->getList();
        $designations = Designation::model()->getList();
        $_data = Voucher::where('_key', $id)->first();
        $_itemDataset = VoucherItem::where([['company_id', $_data->company_id], ['order_id', $_data->id]])->get();
        $subheads = SubHead::where('is_deleted', 0)->get();
        $this->model['data'] = $_data;
        $this->model['insList'] = $insList;
        $this->model['designations'] = $designations;
        $this->model['itemDataset'] = $_itemDataset;
        $this->model['employees'] = $employees;
        $this->model['vehicles'] = $vehicles;
        $this->model['subheads'] = $subheads;
        return view('account.voucher.voucherlist', $this->model);
    }

    public function add_item(Request $r) {
        // pr($_POST);
        $input = $r->all();
        $rule = array(
            'subhead' => 'required',
            'voucher_type' => 'required',
        );
        $messages = array(
            'subhead.required' => 'Required',
            'voucher_type.required' => 'Required',
        );

        $valid = Validator::make($input, $rule, $messages);
        if ($valid->fails()) {
            return redirect()->back()->withErrors($valid)->withInput();
        }

        DB::beginTransaction();
        try {
            $_sub_head = SubHead::where('id', $r->subhead)->first();
            $model = Voucher::find($r->voucher_id);
            $modelItem = new VoucherItem();
            $modelItem->order_id = $model->id;
            $modelItem->company_id = $model->company_id;
            $modelItem->head_id = $_sub_head->head_id;
            $modelItem->date = $model->date;
            $modelItem->employee_id = $model->employee_id;
            $modelItem->vehicle_no = $model->vehicle_no;
            $modelItem->voucher_no = $model->voucher_no;
            $modelItem->description = $model->description;
            $modelItem->subhead_id = $r->subhead;
            $modelItem->particular_id = $r->particular;
            $modelItem->transaction_type = $r->voucher_type;
            if ($modelItem->transaction_type == FROM_VOUCHER) {
                $modelItem->creadit_amount = $r->amount;
            } else {
                $modelItem->debit_amount = $r->amount;
            }
            $modelItem->created_by = Auth::id();
            $modelItem->created_at = cur_date_time();
            $modelItem->_key = uniqueKey();
            if (!$modelItem->save()) {
                throw new Exception("Error! while saving order items record.");
            }

            DB::commit();
            $this->response['success'] = true;
            $this->response['message'] = 'Item saved successfully.';
        } catch (Exception $e) {
            DB::rollback();
            $this->response['success'] = false;
            $this->response['message'] = $e->getMessage();
        }
        return $this->response;
    }

    public function search_itemlist(Request $r) {
        $_itemDataset = VoucherItem::where([['company_id', $r->company_id], ['order_id', $r->voucherId]])->get();
//        pr($_itemDataset, false);
        $this->model['itemDataset'] = $_itemDataset;
        return view('account.voucher._itemlist_partial', $this->model);
    }

    public function remove_item($id) {
        DB::beginTransaction();
        try {
            $data = VoucherItem::find($id);
            if (!$data->delete()) {
                throw new Exception("Error while removing record.");
            }

            DB::commit();
            return redirect()->back()->with('success', 'Item removed successfully.');
        } catch (Exception $e) {
            DB::rollback();
            return redirect()->back()->with('danger', $e->getMessage());
        }
    }

}
