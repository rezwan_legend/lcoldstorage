<?php

namespace App\Http\Controllers;

use Auth;
use DB;
use Exception;
use Validator;
use App\Models\Sr;
use Illuminate\Http\Request;

class SrController extends HomeController {

    public function index() {
        check_user_access('sr_list');
        $dataset = Sr::where('is_deleted', 0)->paginate($this->getSettings()->pagesize);

        $this->model['page_title'] = "SR List";
        $this->model['dataset'] = $dataset;
        return view('sr.index', $this->model);
    }

    public function create() {
        check_user_access('sr_create');

        $this->model['page_title'] = "Sr Information";
        return view('sr.create', $this->model);
    }

    public function store(Request $r) {
        $input = $r->all();
        $rules = array(
            'sr_no' => 'required',
            'lot_no' => 'required',
            'customer' => 'required',
        );
        $messages = array(
            'sr_no.required' => 'Sr Number required.',
            'lot_no.required' => 'Lot Number required.',
            'customer.required' => 'Customer Name required.',
        );

        $valid = Validator::make($input, $rules, $messages);
        if ($valid->fails()) {
            return redirect()->back()->withErrors($valid)->withInput();
        }

        DB::beginTransaction();
        try {
            $model                = new Sr();
            $model->sr_no         = $r->sr_no;
            $model->lot_no        = $r->lot_no;
            $model->customer_name = $r->customer;
            $model->father_name   = $r->fname;
            $model->village       = $r->village;
            $model->created_at    = cur_date_time();
            $model->created_by    = Auth::id();
            $model->_key          = uniqueKey();
            if (!$model->save()) {
                throw new Exception("Error while saving record.");
            }
            DB::commit();
            return redirect('/sr')->with('success', 'Record saved successfully.');
        } catch (Exception $e) {
            DB::rollback();
            return redirect()->back()->with('danger', $e->getMessage());
        }
    }

    public function edit($key) {
        check_user_access('sr_edit');
        $data = Sr::where('_key', $key)->first();

        $this->model['page_title'] = "Sr Edit";
        $this->model['data'] = $data;
        return view('sr.edit', $this->model);
    }

    public function update(Request $r, $key) {
        // pr($r);
        $input = $r->all();
        $rules = array(
            'sr_no' => 'required',
            'lot_no' => 'required',
            'customer' => 'required',
        );
        $messages = array(
            'sr_no.required' => 'Sr Number required.',
            'lot_no.required' => 'Lot Number required.',
            'customer.required' => 'Customer Name required.',
        );

        $valid = Validator::make($input, $rules, $messages);
        if ($valid->fails()) {
            return redirect()->back()->withErrors($valid)->withInput();
        }

        DB::beginTransaction();
        try {
            $model = Sr::where('_key', $key)->firstOrFail();
            $model->sr_no         = $r->sr_no;
            $model->lot_no        = $r->lot_no;
            $model->customer_name = $r->customer;
            $model->father_name   = $r->fname;
            $model->village       = $r->village;
            $model->modified_at   = cur_date_time();
            $model->modified_by   = Auth::id();
            if (!$model->save()) {
                throw new Exception("Error while updating record.");
            }

            DB::commit();
            return redirect('/sr')->with('success', 'Record updated successfully.');
        } catch (Exception $e) {
            DB::rollback();
            return redirect()->back()->with('danger', $e->getMessage());
        }
    }

    // Ajax Functions
    public function search(Request $r) {
        $item_count = !empty($r->item_count) ? $r->item_count : $this->getSettings()->pagesize;
        $sr_no = $r->sr_no;
        $lot_no = $r->lot_no;
        $customer = $r->customer;

        $query = Sr::where('is_deleted', 0);
        if (!empty($sr_no)) {
            $query->where('sr_no', $sr_no);
        }
        if (!empty($lot_no)) {
            $query->where('lot_no', $lot_no);
        }
        if (!empty($customer)) {
            $query->where('customer_name', 'like', '%' . $customer . '%');
        }
        $query->orderBy('sr_no', 'asc');
        $dataset = $query->paginate($item_count);

        $this->model['dataset'] = $dataset;
        return view('sr._list', $this->model);
    }
    
    public function delete() {
        $resp = array();
        DB::beginTransaction();
        try {
            foreach ($_POST['data'] as $id) {
                $data = Sr::find($id);
                $data->is_deleted = 1;
                $data->deleted_by = Auth::id();
                $data->deleted_at = cur_date_time();
                if (!$data->save()) {
                    throw new Exception("Error while deleting records.");
                }
            }
            DB::commit();
            $resp['success'] = true;
            $resp['message'] = 'Record has been deleted successfully.';
        } catch (Exception $e) {
            DB::rollback();
            $resp['success'] = false;
            $resp['message'] = $e->getMessage();
        }
        return $resp;
    }

}
