<?php

namespace App\Http\Controllers\Accounts;

use Auth;
use Exception;
use Validator;
use App\Http\Controllers\HomeController;
use App\Models\Accounts\CashTransaction;
use App\Models\Accounts\BankTransaction;
use App\Models\Setting\AccountSetting\SubHead;
use App\Models\EssentialSetting\BankSetting;
use App\Models\EssentialSetting\Branches;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CashTransactionController extends HomeController
{

    public function index()
    {
        check_user_access('cash_transaction');
        $dataset = CashTransaction::where('is_deleted', 0)->get();
        $this->model['page_title'] = "Cash transaction Information";
        $this->model['dataset'] = $dataset;
        //pr($dataset);
        return view('cash_transaction.index', $this->model);
    }

    public function create()
    {
        check_user_access('cash_transaction_create');

        $_bank_t = BankTransaction::where('is_deleted', 0)->get();
        $_bank_s = BankSetting::where('is_deleted', 0)->get();
        $_head = SubHead::where('is_deleted', 0)->get();
        $this->model['head'] = $_head;
        $this->model['bank_transaction'] = $_bank_t;
        $this->model['bank_setting'] = $_bank_s;
        $this->model['page_title'] = "Cash transaction create";
        return view('cash_transaction.create', $this->model);
    }

    public function store(Request $r)
    {
        //  pr($_POST);
        $input = $r->all();
        $rules = array(
            'by_whom' => 'required',
        );
        $messages = array(
            'by_whom.required' => 'By Whom Required.',
        );
        $valid = Validator::make($input, $rules, $messages);
        if ($valid->fails()) {
            return redirect()->back()->withErrors($valid)->withInput();
        }
        try {
            $model = new CashTransaction();
            $model->date = $r->date;
            $model->sub_head_id = $r->head;
            $model->by_whom = $r->by_whom;
            $model->debit = $r->debit;
            $model->descption = $r->description;
            $model->transaction_type = $r->transaction_type;
            $model->bank_id = $r->bank;
            $model->account	 = $r->account;
            $model->check_number= $r->check_number;
            $model->created_at = cur_date_time();
            $model->created_by = Auth::id();
            $model->_key = uniqueKey();
            if (!$model->save()) {
                throw new Exception("Error while saving record.");
            }
            DB::commit();
            return redirect('/cash_transaction')->with('success', 'Record saved successfully.');
        } catch (Exception $e) {
            DB::rollback();
            return redirect()->back()->with('danger', $e->getMessage());
        }
    }

    public function edit($id)
    {
        check_user_access('cash_transaction_edit');
        $data = CashTransaction::where('_key', $id)->first();
        $_banks = BankSetting::where('is_deleted', 0)->get();
        $_branches = Branches::where('is_deleted', 0)->get();
        $this->model['branches'] = $_branches;
        $this->model['banks'] = $_banks;

        $this->model['page_title'] = "Cash transaction edit";
        $this->model['data'] = $data;
        return view('cash_transaction.edit', $this->model);
    }

    public function update(Request $r, $id)
    {


        $input = $r->all();
        $rules = array(
            'account_name' => 'required',
        );
        $messages = array(
            'account_name.required' => 'Account Name required.',
        );

        $valid = Validator::make($input, $rules, $messages);
        if ($valid->fails()) {
            return redirect()->back()->withErrors($valid)->withInput();
        }
        try {
            $model = CashTransaction::where('_key', $id)->firstOrFail();
            $model->branch_id = $r->branches;
            $model->bank_id = $r->banks;
            $model->branch_name = $r->branche_name;
            $model->account_type = $r->account_type;
            $model->account_name = $r->account_name;
            $model->account_number = $r->account_number;
            $model->address = $r->address;
            $model->modified_at = cur_date_time();
            $model->modified_by = Auth::id();
            if (!$model->save()) {
                throw new Exception("Error while updating record.");
            }

            DB::commit();
            return redirect('/cash_transaction')->with('success', 'Record updated successfully.');
        } catch (Exception $e) {
            DB::rollback();
            return redirect()->back()->with('danger', $e->getMessage());
        }
    }

    // Ajax Functions
    public function search(Request $r)
    {
        $item_count = $r->item_count;
        $srch = $r->search_info;
        $query = CashTransaction::where('is_deleted', 0);

        if (!empty($srch)) {
            $query->where('descption', 'like', '%' . trim($srch) . '%')->get();
        }
        $query->orderBy('descption', 'asc');
        $dataset = $query->paginate($item_count);
   
        $this->model['dataset'] = $dataset;

        return view('cash_transaction._list', $this->model);
    }

    public function delete()
    {
        $resp = array();
        try {
            foreach ($_POST['data'] as $id) {
                $data = CashTransaction::find($id);
                $data->is_deleted = 1;
                $data->deleted_by = Auth::id();
                $data->deleted_at = cur_date_time();
                if (!$data->save()) {
                    throw new Exception("Error while deleting records.");
                }
            }
            DB::commit();
            $resp['success'] = true;
            $resp['message'] = 'Record has been deleted successfully.';
        } catch (Exception $e) {
            DB::rollback();
            $resp['success'] = false;
            $resp['message'] = $e->getMessage();
        }
        return $resp;
    }
    public function show()
    {

        $this->model['page_title'] = "Cash transaction Balance";
        return view('/cash_transaction/ledger', $this->model);
    }



    public function AccountByBank(Request $r)
    {
      
        $dataset = BankTransaction::where([['is_deleted', 0], ['bank_id', $r->BankId]])->get();
   

        $str = "";
        if (!empty($dataset)) {
            $str .= "<option value=''>Account</option>";
            foreach ($dataset as $data) {
                $str .= "<option value='{$data->id}'>{$data->account_name}</option>";
            }
        } else {
            $str .= "<option value=''>No Party Found</option>";
        }

        return $str;
    }
}
