<?php

namespace App\Http\Controllers\Accounts;

use Auth;
use DB;
use Exception;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\HomeController;
use App\Models\Accounts\AccountHead;

class AccountHeadController extends HomeController {

    public function index() {
        check_user_access('account_head_list');

        $model = new AccountHead();
        $query = $model->where('is_deleted', 0);
        $dataset = $query->paginate($this->getSettings()->pagesize);

        $this->model['page_title'] = "Account Head List";
        $this->model['dataset'] = $dataset;
        return view('account_head.index', $this->model);
    }

    public function create() {
        check_user_access('account_head_create');

        $this->model['page_title'] = "Account Head Information";
        return view('account_head.create', $this->model);
    }

    public function store(Request $r) {
        // pr($_POST);
        DB::beginTransaction();
        try {
                $model             = new AccountHead();
                $model->type       = $r->type;
                $model->head_type  = $r->htype;
                $model->name       = $r->name;
                $model->common     = $r->common;
                $model->created_at = cur_date_time();
                $model->created_by = Auth::id();
                $model->_key       = uniqueKey();
                if (!$model->save()) {
                    throw new Exception("Error while Creating Records.");
                }

            DB::commit();
            return redirect("/account_head")->with(['alert-type' => 'success', 'message' => 'Records Created Successfully.']);
        } catch (Exception $e) {
            DB::rollback();
            return redirect()->back()->with(['alert-type' => 'error', 'message' => $e->getMessage()]);
        }
    }

    public function edit($key) {
        check_user_access('account_head_edit');
        $data = AccountHead::where('_key', $key)->first();

        $this->model['page_title'] = "Account Head Information";
        $this->model['data'] = $data;
        return view('account_head.edit', $this->model);
    }

    public function update(Request $r, $key) {
        //pr($_POST);
        $input = $r->all();
        $rule = array(
            'name' => 'required',
        );
        $messages = array(
            'name.required' => 'Name Should not be empty.',
        );

        $valid = Validator::make($input, $rule, $messages);
        if ($valid->fails()) {
            return redirect()->back()->withErrors($valid)->withInput();
        }

        DB::beginTransaction();
        try {
            $data = AccountHead::where('_key', $key)->firstOrFail();
            $data->name = $r->name;
            $data->type = $r->type;
            $data->head_type = $r->htype;
            $data->common = $r->common;
            $data->modified_at = cur_date_time();
            $data->modified_by = Auth::id();
            if (!$data->save()) {
                throw new Exception("Query Problem on Updating Record.");
            }

            DB::commit();
            return redirect("/account_head")->with(['alert-type' => 'success', 'message' => 'Records Updated Successfully.']);
        } catch (Exception $e) {
            DB::rollback();
            return redirect()->back()->with(['alert-type' => 'error', 'message' => $e->getMessage()]);
        }
    }

    public function show($id) {
        $dataset = AccountHead::where('_key', $id)->first();
//        pr($dataset);
        $this->model['page_title'] = "Head Information";
        $this->model['dataset'] = $dataset;
        return view('account_head.ledger', $this->model);
    }

    // Ajax Functions
    public function search(Request $r) {
        //pr($_POST);
        $item_count = !empty($r->item_count) ? $r->item_count : $this->getSettings()->pagesize;
        $name = $r->name;
        $search = $r->search;
        $sort_by = $r->sort_by;
        $sort_type = $r->sort_type;

        $model = new AccountHead();
        $query = $model->where('is_deleted', 0);
        if (!empty($name)) {
            $query->where('name', 'like', '%' . $name . '%');
        }
        if (!empty($search)) {
            $query->where('type', 'like', '%' . $search . '%')->orWhere('head_type', 'like', '%' . $search . '%');
        }
        $query->orderBy($sort_by, $sort_type);
        $dataset = $query->paginate($item_count);

        $this->model['dataset'] = $dataset;
        return view('account_head._list', $this->model);
    }

    public function delete() {
        //pr($_POST);
        DB::beginTransaction();
        try {
            foreach ($_POST['data'] as $id) {
                $data = AccountHead::find($id);
                $data->is_deleted = 1;
                $data->deleted_by = Auth::id();
                $data->deleted_at = cur_date_time();
                if (!$data->save()) {
                    throw new Exception("Error while deleting records.");
                }
            }
            DB::commit();
            $resp['success'] = true;
            $resp['message'] = 'Record has been deleted successfully.';
        } catch (Exception $e) {
            DB::rollback();
            $resp['success'] = false;
            $resp['message'] = $e->getMessage();
        }
        return $resp;
    }
 

}
