<?php

namespace App\Http\Controllers\Accounts;

use Auth;
use Exception;
use Validator;
use App\Http\Controllers\HomeController;
use App\Models\Accounts\BankTransaction;
use App\Models\EssentialSetting\BankSetting;
use App\Models\EssentialSetting\Branches;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BankTransactionController extends HomeController {

    public function index() {
        check_user_access('bank_transaction');
        $dataset = BankTransaction::where('is_deleted', 0)->get();
        $this->model['page_title'] = "Bank transaction Information";
        $this->model['dataset'] = $dataset;
        //pr($dataset);
        return view('bank_transaction.index', $this->model);
    }

    public function create() {
        check_user_access('bank_transaction_create');
        
        $_banks = BankSetting::where('is_deleted', 0)->get();
        $_branches = Branches::where('is_deleted', 0)->get();
        $this->model['branches'] = $_branches;
        $this->model['banks'] = $_banks;
        $this->model['page_title'] = "Bank transaction create";
        return view('bank_transaction.create', $this->model);
    }

    public function store(Request $r) {
        $input = $r->all();
        $rules = array(
            'account_name' => 'required',
        );
        $messages = array(
            'account_name.required' => 'Account Name required.',
        );
        $valid = Validator::make($input, $rules, $messages);
        if ($valid->fails()) {
            return redirect()->back()->withErrors($valid)->withInput();
        }
        try {
            $model = new BankTransaction();
            $model->branch_id = $r->branches;
            $model->bank_id = $r->banks;
            $model->branch_name = $r->branche_name;
            $model->account_type = $r->account_type;
            $model->account_name = $r->account_name;
            $model->account_number = $r->account_number;
            $model->address = $r->address;
            $model->created_at = cur_date_time();
            $model->created_by = Auth::id();
            $model->_key = uniqueKey();
            if (!$model->save()) {
                throw new Exception("Error while saving record.");
            }
            DB::commit();
            return redirect('/bank_transaction')->with('success', 'Record saved successfully.');
        } catch (Exception $e) {
            DB::rollback();
            return redirect()->back()->with('danger', $e->getMessage());
        }
    }

    public function edit($id) {
        check_user_access('bank_transaction_edit');
        $data = BankTransaction::where('_key', $id)->first();
        $_banks = BankSetting::where('is_deleted', 0)->get();
        $_branches = Branches::where('is_deleted', 0)->get();
        $this->model['branches'] = $_branches;
        $this->model['banks'] = $_banks;

        $this->model['page_title'] = "Bank transaction edit";
        $this->model['data'] = $data;
        return view('bank_transaction.edit', $this->model);
    }

       public function update(Request $r, $id) {

    
        $input = $r->all();
        $rules = array(
            'account_name' => 'required',
        );
        $messages = array(
            'account_name.required' => 'Account Name required.',
        );

        $valid = Validator::make($input, $rules, $messages);
        if ($valid->fails()) {
            return redirect()->back()->withErrors($valid)->withInput();
        }
        try {
            $model = BankTransaction::where('_key',$id)->firstOrFail();
            $model->branch_id = $r->branches;
            $model->bank_id = $r->banks;
            $model->branch_name = $r->branche_name;
            $model->account_type = $r->account_type;
            $model->account_name = $r->account_name;
            $model->account_number = $r->account_number;
            $model->address = $r->address;
            $model->modified_at = cur_date_time();
            $model->modified_by = Auth::id();
            if (!$model->save()) {
                throw new Exception("Error while updating record.");
            }

            DB::commit();
            return redirect('/bank_transaction')->with('success', 'Record updated successfully.');
        } catch (Exception $e) {
            DB::rollback();
            return redirect()->back()->with('danger', $e->getMessage());
        }
    }

    // Ajax Functions
    public function search(Request $r) {
        $item_count = !empty($r->item_count) ? $r->item_count : $this->gettransaction()->pagesize;
        $srch = $r->srch;
        $query = BankTransaction::where('is_deleted', 0);
    
        if (!empty($srch)) {
            $query->where('account_name', 'like', '%' . $srch . '%');
        }
        $query->orderBy('account_name', 'asc');
        $dataset = $query->paginate($item_count);

        $this->model['dataset'] = $dataset;
        return view('bank_transaction._list', $this->model);
    }

    public function delete() {
        $resp = array();
        try {
            foreach ($_POST['data'] as $id) {
                $data = BankTransaction::find($id);
                $data->is_deleted = 1;
                $data->deleted_by = Auth::id();
                $data->deleted_at = cur_date_time();
                if (!$data->save()) {
                    throw new Exception("Error while deleting records.");
                }
            }
            DB::commit();
            $resp['success'] = true;
            $resp['message'] = 'Record has been deleted successfully.';
        } catch (Exception $e) {
            DB::rollback();
            $resp['success'] = false;
            $resp['message'] = $e->getMessage();
        }
        return $resp;
    }
    public function show(){
        
        $this->model['page_title'] = "Bank transaction Balance";
        return view('/bank_transaction/ledger',$this->model);
    }


}
