<?php

namespace App\Http\Controllers\Accounts;

use Auth;
use Exception;
use Validator;
use App\Http\Controllers\HomeController;
use App\Models\Accounts\Requisition;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class RequisitionController extends HomeController {

    public function index() {
        check_user_access('requisition');
        $dataset = Requisition::where('is_deleted', 0)->get();
        $this->model['page_title'] = "Requisition Information";
        $this->model['dataset'] = $dataset;
        //pr($dataset);
        return view('requisition.index', $this->model);
    }

    public function create() {
        check_user_access('requisition_create');    
        $this->model['page_title'] = "requisition create";
        return view('requisition.create', $this->model);
    }

    public function store(Request $r) {
        
        $input = $r->all();
        $rules = array(
            'title' => 'required',
        );
        $messages = array(
            'title.required' => 'Title required.',
        );
        $valid = Validator::make($input, $rules, $messages);
        if ($valid->fails()) {
            return redirect()->back()->withErrors($valid)->withInput();
        }
        try {
            $model = new Requisition();
            $model->session = $r->session;
            $model->date = $r->date;
            $model->requisition_no = $r->requisition_no;
            $model->requisition_by = $r->requisition_by;
            $model->title = $r->title;
            $model->created_at = cur_date_time();
            $model->created_by = Auth::id();
            $model->_key = uniqueKey();
            if (!$model->save()) {
                throw new Exception("Error while saving record.");
            }
            DB::commit();
            return redirect('/requisition')->with('success', 'Record saved successfully.');
        } catch (Exception $e) {
            DB::rollback();
            return redirect()->back()->with('danger', $e->getMessage());
        }
    }

    public function edit($id) {
        check_user_access('requisition_edit');
        $data = Requisition::where('_key', $id)->first();
        $_banks = BankSetting::where('is_deleted', 0)->get();
        $_branches = Branches::where('is_deleted', 0)->get();
        $this->model['branches'] = $_branches;
        $this->model['banks'] = $_banks;

        $this->model['page_title'] = "requisition edit";
        $this->model['data'] = $data;
        return view('requisition.edit', $this->model);
    }

       public function update(Request $r, $id) {

    
        $input = $r->all();
        $rules = array(
            'account_name' => 'required',
        );
        $messages = array(
            'account_name.required' => 'Account Name required.',
        );

        $valid = Validator::make($input, $rules, $messages);
        if ($valid->fails()) {
            return redirect()->back()->withErrors($valid)->withInput();
        }
        try {
            $model = Requisition::where('_key',$id)->firstOrFail();
            $model->branch_id = $r->branches;
            $model->bank_id = $r->banks;
            $model->branch_name = $r->branche_name;
            $model->account_type = $r->account_type;
            $model->account_name = $r->account_name;
            $model->account_number = $r->account_number;
            $model->address = $r->address;
            $model->modified_at = cur_date_time();
            $model->modified_by = Auth::id();
            if (!$model->save()) {
                throw new Exception("Error while updating record.");
            }

            DB::commit();
            return redirect('/requisition')->with('success', 'Record updated successfully.');
        } catch (Exception $e) {
            DB::rollback();
            return redirect()->back()->with('danger', $e->getMessage());
        }
    }

    // Ajax Functions
    public function search(Request $r) {
        $item_count = !empty($r->item_count) ? $r->item_count : $this->gettransaction()->pagesize;
        $srch = $r->srch;
        $session=$r->session;
        $query = Requisition::where('is_deleted', 0);
    
        if (!empty($srch)) {
            $query->where('title', 'like', '%' . $srch . '%');
        }

         if (!empty($srch)) {
            $query->where('session', 'like', '%' . $session . '%');
        }
        $query->orderBy('title', 'asc');
        $dataset = $query->paginate($item_count);

        $this->model['dataset'] = $dataset;
        return view('requisition._list', $this->model);
    }

    public function delete() {
        $resp = array();
        try {
            foreach ($_POST['data'] as $id) {
                $data = Requisition::find($id);
                $data->is_deleted = 1;
                $data->deleted_by = Auth::id();
                $data->deleted_at = cur_date_time();
                if (!$data->save()) {
                    throw new Exception("Error while deleting records.");
                }
            }
            DB::commit();
            $resp['success'] = true;
            $resp['message'] = 'Record has been deleted successfully.';
        } catch (Exception $e) {
            DB::rollback();
            $resp['success'] = false;
            $resp['message'] = $e->getMessage();
        }
        return $resp;
    }
    public function show(){
        
        $this->model['page_title'] = "requisition Balance";
        return view('/requisition/ledger',$this->model);
    }


}
