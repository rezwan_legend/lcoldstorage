<?php

namespace App\Http\Controllers;

use Auth;
use DB;
use Exception;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\HomeController;
use App\Models\ProductType;
use App\Models\Category;
use App\Models\Product;
use App\Models\ProductUnit;
use App\Models\Institute;
use App\Models\PriceUnit;
use App\Models\UnitSize;
use App\Models\Stock;
use App\Models\Purchase;
use App\Models\PurchaseItem;
use App\Models\Account\SubHead;
use App\Models\Account\Particular;
use App\Models\Account\Transaction;
use App\Models\SalesOrder;

class PurchaseController extends HomeController {

    public function index() {
        check_user_access('purchase_list');
        $dataset = Purchase::where([['purchases_type', NULL], ['is_deleted', 0]])->orderBy('date', 'DESC')->paginate($this->getSettings()->pagesize);

        $this->model['businessList'] = $this->userInstitutes($this->currentRouteFile());
        $this->model['dataset'] = $dataset;
        return view('purchases.index', $this->model);
    }

    public function create() {
        check_user_access('purchase_create');
        $subheads = SubHead::where('is_deleted', 0)->get();
        $order_no = Purchase::model()->oredrNo();

        $this->model['businessList'] = $this->userInstitutes($this->currentRouteFile());
        $this->model['subheads'] = $subheads;
        $this->model['order_no'] = $order_no;
        return view('purchases.create', $this->model);
    }

    public function store(Request $r) {
        //  pr($_POST);
        DB::beginTransaction();
        try {
            $model = new Purchase();
            $_head_id = SubHead::where('id', $r->subhead_id)->first();
            $_to_head_id = SubHead::where('id', $r->to_subhead_id)->first();
            $model->order_no = $r->order_no;
            $model->date = !empty($r->order_date) ? date_ymd($r->order_date) : date('Y-m-d');
            $model->challan_no = $r->challan_no;
            $model->head_id = $_head_id->head_id;
            $model->subhead_id = $r->subhead_id;
            $model->particular_id = $r->supplier_particular;
            $model->challan_weight = $r->challan_weight;
            $model->to_head_id = $_to_head_id->head_id;
            $model->to_subhead_id = $r->to_subhead_id;
            $model->to_particular_id = $r->particular;
            $model->scal_weight = $r->scale_weight;
            $model->quantity = $r->bag_quantity;
            $model->less_weight = $r->less_weight;
            $model->net_weight = $r->net_weight;
            $model->avg_weight = $r->avg_weight;
            $model->vehicle_no = $r->vehicle_no;
            $model->vehicle_rent = $r->vehicle_rent;
            $model->person_name = $r->person_name;
            $model->note = $r->note;
            $model->created_by = Auth::id();
            $model->created_at = cur_date_time();
            $model->_key = uniqueKey();
            if (!$model->save()) {
                throw new Exception("Error! while creating purchase.");
            }
            DB::commit();
            return redirect("/purchase")->with('success', 'Purchase Order Successfully.');
        } catch (Exception $e) {
            DB::rollback();
            return redirect()->back()->with('danger', $e->getMessage());
        }
    }

    public function show($id) {
        check_user_access('purchase_details');
        $data = Purchase::find($id);
        $itemDataset = PurchaseItem::where('purchase_id', $data->id)->get();
        $tmodel = new Transaction();
        $last_tr_id = Transaction::where('purchase_id', $data->id)->orderBy('id', 'DESC')->first();
        if (!empty($last_tr_id)) {
            $lastTrBalance = Transaction::where([['purchase_id', $data->id], ['dr_particular_id', $data->to_particular_id]])->orderBy('id', 'DESC')->first()->debit;
        }
        $this->model['lastTrBalance'] = !empty($lastTrBalance) ? $lastTrBalance : 0;
        $this->model['tr_sale_id'] = !empty($last_tr_id) ? $last_tr_id->id : NULL;
        $this->model['data'] = $data;
        $this->model['tmodel'] = $tmodel;
        $this->model['itemDataset'] = $itemDataset;
        return view('purchases.details', $this->model);
    }

    public function gatepass($id) {
        check_user_access('purchase_gatepass');
        $data = Purchase::find($id);
        $itemDataset = PurchaseItem::where('purchase_id', $data->id)->get();

        $this->model['data'] = $data;
        $this->model['itemDataset'] = $itemDataset;
        return view('purchases.gatepass', $this->model);
    }

    public function edit($id) {
        check_user_access('purchase_edit');
        // $order_no = SalesOrder :: model()->oredrNo();
        $data = Purchase::where('id', $id)->first();
        $particulars = Particular::where('is_deleted', 0)->get();
        $subheads = SubHead::where('is_deleted', 0)->get();

        $this->model['particulars'] = $particulars;
        $this->model['subheads'] = $subheads;
        $this->model['data'] = $data;
        // $this->model['order_no'] = $order_no;
        $this->model['businessList'] = $this->userInstitutes($this->currentRouteFile());
        return view('purchases.edit', $this->model);
    }

    public function save_order(Request $r) {
        // pr($_POST);
        DB::beginTransaction();
        try {
            $model = Purchase::find($r->purchaseId);
            //pr($model);
            $itemModel = PurchaseItem::find($r->purchaseId);
            $_totalPrice = Purchase::model()->total_price($r->purchaseId);
            $_net_weight = Purchase::model()->total_net_weight($r->purchaseId);
            if ($r->vehicleRent > 0) {
                $_totalAmount = $_totalPrice + $r->vehicleRent;
                $per_kg_rate = round($_totalAmount / $_net_weight, 5);
                foreach ($model->items as $item) {
                    $item->per_kg_rate = $per_kg_rate;
                    $item->price = $item->per_kg_rate * $itemModel->net_weight;
                    if (!$item->save()) {
                        throw new Exception("Error while processing itmes.");
                    }
                }
            }
            $model->invoice_amount = $r->invoice_amount;
            $model->discount_amount = $r->discount_amount;
            $model->net_amount = $r->net_amount;
            $model->previous_amount = $r->previous_amount;
            $model->payable_amount = $r->payable_amount;
            $model->paid_amount = $r->paid_amount;
            $model->due_amount = $r->due_amount;
            $model->balance_amount = $r->balance_amount;
            if (!$model->save()) {
                throw new Exception("Error! while saving order record.");
            }
            DB::commit();
            return redirect("purchase")->with('success', 'Item saved successfully');
        } catch (Exception $e) {
            DB::rollback();
            return redirect()->back()->with('danger', $e->getMessage());
        }
    }

    public function update(Request $r, $id) {
        //pr($_POST);
        $input = $r->all();
        $rule = array(
            'from_subhead_id' => 'required',
            'scale_weight' => 'required',
            'challan_no' => 'required',
        );
        $messages = array(
            'from_subhead_id.required' => 'Required',
            'scale_weight.required' => 'Required',
            'challan_no.required' => 'Required',
        );

        $valid = Validator::make($input, $rule, $messages);
        if ($valid->fails()) {
            return redirect()->back()->withErrors($valid)->withInput();
        }

        DB::beginTransaction();
        try {
//            $_head_id = SubHead::where('id', $r->subhead_id)->first();
            $_to_head_id = SubHead::where('id', $r->to_subhead_id)->first();
            $_from_head_id = SubHead::where('id', $r->from_subhead_id)->first();

            $model = Purchase::find($id);
            $model->company_id = $r->company_id;
            $model->head_id = $_from_head_id->head_id;
            $model->subhead_id = $r->from_subhead_id;
            $model->particular_id = $r->supplier_particular;

            $model->to_head_id = $_to_head_id->head_id;
            $model->to_subhead_id = $r->to_subhead_id;
            $model->particular_id = $r->supplier_particular;
            $model->date = date_ymd($r->order_date);
            $model->challan_no = $r->challan_no;
            $model->order_no = $r->order_no;
            $model->challan_weight = $r->challan_weight;
            $model->scal_weight = $r->scale_weight;
            $model->quantity = $r->bag_quantity;
            $model->less_weight = $r->less_weight;
            $model->net_weight = $r->net_weight;
            $model->avg_weight = $r->avg_weight;
            $model->vehicle_no = $r->vehicle_no;
            $model->person_name = $r->person_name;
            $model->vehicle_rent = $r->vehicle_rent;
            $model->note = $r->note;
            $model->modified_by = Auth::id();
            if (!$model->save()) {
                throw new Exception("Error! while updating purchase.");
            }

            foreach ($model->items as $item) {
                $item->date = $model->date;
                $item->year = date_yy($model->date);
                $item->month = date_mm($model->date);
                $item->company_id = $model->company_id;
                $item->challan_no = $model->challan_no;
                $item->order_no = $model->order_no;
                if (!$item->save()) {
                    throw new Exception("Error while processing itmes.");
                }
            }
            DB::commit();
            return redirect('purchase')->with('success', 'Purchases Updated Successfully.');
        } catch (Exception $e) {
            DB::rollback();
            return redirect()->back()->with('danger', $e->getMessage());
        }
    }

    public function confirm($id) {
        check_user_access('ricemill_purchase_process');
        $data = Purchase::find($id);
        DB::beginTransaction();
        try {
            if (count($data->items) <= 0) {
                throw new Exception("Sorry !! There is no items for processing....");
            }
            $descriptionArr = [];
            foreach ($data->items as $item) {
                $descriptionArr[] = $item->product->name . "(" . $item->price_unit_qty . "*" . $item->rate . ")";
                $modelStock = new Stock();
                $modelStock->size_id = $item->size_id;
                $modelStock->net_qty = $item->quantity;
                $modelStock->company_id = $item->company_id;
                $modelStock->purchases_order_id = $item->purchase_id;
                $modelStock->purchases_item_id = $item->id;
                $modelStock->product_type_id = $item->product_type_id;
                $modelStock->category_id = $item->category_id;
                $modelStock->product_id = $item->product_id;
                $modelStock->date = $data->date;
                $modelStock->net_weight = $item->net_weight;
                $modelStock->rate = $item->per_kg_rate;
                $modelStock->price = $modelStock->net_weight * $item->per_kg_rate;
                $modelStock->unit_id = $item->unit_id;
                $modelStock->price_unit_id = $item->price_unit_id;
                $modelStock->price_unit_qty = $item->price_unit_qty;
                $modelStock->quantity = $item->quantity;
                $modelStock->stock_type = STOCK_TYPE_IN;
                $modelStock->type = STOCK_TYPE_PURCHASE;
                $modelStock->created_by = Auth::id();
                if (!$modelStock->save()) {
                    throw new Exception("Query Problem on Creating Stocks.");
                }

                $item->process_status = PROCESS_COMPLETE;
                if (!$item->save()) {
                    throw new Exception("Error while processing itmes.");
                }
            }

            $descriptionArr[] = $data->note;
            $description = implode(",", $descriptionArr);
            $transaction = new Transaction();
            $transaction->institute_id = $data->company_id;
            $transaction->purchase_id = $data->id;
            $transaction->vehicle_no = $data->vehicle_no;
            $transaction->type = 'D';
            $transaction->voucher_type = PURCHASE_VOUCHER;
            $transaction->payment_method = PAYMENT_NO;
            $transaction->date = $data->date;
            $transaction->dr_head_id = $data->to_head_id;
            $transaction->dr_subhead_id = $data->to_subhead_id;
            $transaction->dr_particular_id = $data->to_particular_id;
            $transaction->cr_head_id = $data->head_id;
            $transaction->cr_subhead_id = $data->subhead_id;
            $transaction->cr_particular_id = $data->particular_id;
            $transaction->by_whom = Auth::user()->name;
            $transaction->description = $description;
//            $transaction->amount = $data->total_price($item->purchase_id);
            $transaction->amount = $data->net_amount;
            $transaction->debit = $data->net_amount;
            $transaction->credit = $data->net_amount;
            $transaction->note = $description;
            $transaction->created_by = Auth::id();
            $transaction->_key = uniqueKey();
            if (!$transaction->save()) {
                throw new Exception("Error while saving data in Transaction.");
            }

            $data->modified_by = Auth::id();
            $data->process_status = PROCESS_COMPLETE;
            if (!$data->save()) {
                throw new Exception("Something Went Wrong. Please Re Submit your request.");
            }

            DB::commit();
            return redirect('ricemill/purchases/')->with('success', 'Purchase Order Processed Successfully');
        } catch (Exception $e) {
            DB::rollback();
            return redirect('ricemill/purchases/')->with('danger', $e->getMessage());
        }
    }

    public function reset($id) {
        check_user_access('ricemill_purchase_reset');
        $data = Purchase::find($id);
        DB::beginTransaction();
        try {
            if (!empty($data->stocks)) {
                foreach ($data->stocks as $stock) {
                    if (!$stock->delete()) {
                        throw new Exception("Error while deleting records from Production Stocks.");
                    }
                }
            }

            if (!empty($data->transactions)) {
                foreach ($data->transactions as $transaction) {
                    if (!$transaction->delete()) {
                        throw new Exception("Error while deleting records from Transaction.");
                    }
                }
            }

            if (!empty($data->items)) {
                foreach ($data->items as $_item) {
                    $_item->process_status = PROCESS_PENDING;
                    if (!$_item->save()) {
                        throw new Exception("Error while processing items.");
                    }
                }
            }

            $data->process_status = PROCESS_PENDING;
            $data->modified_by = Auth::id();
            if (!$data->save()) {
                throw new Exception("Something Went Wrong. Please Re Submit your request.");
            }

            DB::commit();
            return redirect('ricemill/purchases/')->with('success', 'Purchase Order Reset Successfully');
        } catch (Exception $e) {
            DB::rollback();
            return redirect('ricemill/purchases/')->with('danger', $e->getMessage());
        }
    }

    // Ajax Functions
    public function search(Request $r) {
        // pr($_POST);
        $item_count = !empty($r->item_count) ? $r->item_count : $this->getSettings()->pagesize;
        $end_date = !empty($r->input('end_date')) ? date_ymd($r->input('end_date')) : date('Y-m-d');
        $search = $r->search;

        $query = Purchase::where('is_deleted', 0);

        if (!empty($r->from_date)) {
            $query->whereBetween('date', [$r->from_date, $end_date]);
        }
        if (!empty($search)) {
            $query->where('challan_no', 'like', '%' . $search . '%');
        }
        if (!empty($r->supplier)) {
            $_partyList = Particular::where('name', 'like', '%' . trim($r->supplier) . '%')->get();
            foreach ($_partyList as $_party) {
                $partyId[] = $_party->id;
            }
            $query->whereIn('particular_id', $partyId);
        }
        $dataset = $query->orderBy('date', 'DESC')->paginate($item_count);
        return view('purchases._list', compact('dataset'));
    }

    public function purchase_items() {
        check_user_access('ricemill_purchase_items_list');
        $dataset = PurchaseItem::where([['purchases_type', NULL], ['is_deleted', 0]])->orderBy('date', 'DESC')->paginate($this->getSettings()->pagesize);
        $_particulars = Particular::where('is_deleted', 0)->get();
        $_product_type = ProductType::where([['is_deleted', 0], ['company_id', RICEMILL], ['id', '!=', RICEMILL_PRODUCT_TYPE_EMPTY_BAG], ['id', '!=', RICEMILL_PRODUCT_TYPE_EMPTY_OTHERS]])->get();

        $this->model['businesslist'] = $this->userInstitutes($this->currentRouteFile());
        $this->model['particulars'] = $_particulars;
        $this->model['product_type'] = $_product_type;
        $this->model['dataset'] = $dataset;
        return view('ricemill.purchases.purchases_list', $this->model);
    }

    public function purchase_items_search(Request $r) {
        $item_count = !empty($r->item_count) ? $r->item_count : $this->getSettings()->pagesize;
        $end_date = !empty($r->end_date) ? date_ymd($r->end_date) : date('Y-m-d');
        $companyId = $r->business;
        $product_type = $r->product_type;
        $category = $r->category;
        $product = $r->product_id;
        $_size_id = $r->size_id;
        $search_supplier = $r->srcParty;

        $query = PurchaseItem::where([['purchases_type', NULL], ['is_deleted', 0]]);
        if (!empty($companyId)) {
            $query->where('company_id', $companyId);
            $this->searchValues['Company'] = Institute::find($companyId)->name;
        }
        if (!empty($r->from_date)) {
            $query->whereBetween('date', [$r->from_date, $end_date]);
            $this->searchValues['Dates'] = "{$r->from_date} | {$end_date}";
        }
        if (!empty($product_type)) {
            $query->where('product_type_id', $product_type);
            $this->searchValues['Product Type'] = ProductType::find($product_type)->name;
        }
        if (!empty($category)) {
            $query->where('category_id', $category);
            $this->searchValues['Category'] = Category::find($category)->name;
        }
        if (!empty($product)) {
            $query->where('product_id', $product);
            $this->searchValues['Category'] = Product::find($product)->name;
        }
        if (!empty($_size_id)) {
            $query->where('size_id', $_size_id);
            $this->searchValues['Size'] = UnitSize::find($_size_id)->name;
        }
        if (!empty($search_supplier)) {
            $_partyList = Particular::where('name', 'like', '%' . trim($search_supplier) . '%')
                    ->orWhere('address', 'like', '%' . trim($search_supplier) . '%')
                    ->orWhere('mobile', 'like', '%' . to_eng($search_supplier) . '%')
                    ->get();
            foreach ($_partyList as $_party) {
                $particularId[] = $_party->id;
            }

            $idArr = [];
            $_dataList = Purchase::where('is_deleted', 0)->whereIn('particular_id', $particularId)->get();
            foreach ($_dataList as $_data) {
                $idArr[] = $_data->id;
            }
            $query->whereIn('purchase_id', $idArr);
            $this->searchValues['Party'] = $search_supplier;
        }
        $dataset = $query->orderBy('date', 'DESC')->paginate($item_count);

        $this->model['dataset'] = $dataset;
        $this->model['searchValues'] = $this->searchValues;
        return view('purchases.purchases_list_search', $this->model);
    }

    public function delete() {
        $resp = array();
        DB::beginTransaction();
        try {
            foreach ($_POST['data'] as $id) {
                $data = Purchase::find($id);

                // if (!empty($data->stocks)) {
                //     foreach ($data->stocks as $stock) {
                //         if (!$stock->delete()) {
                //             throw new Exception("Error while deleting records from Production Stocks.");
                //         }
                //     }
                // }

                // if (!empty($data->transactions)) {
                //     foreach ($data->transactions as $transaction) {
                //         if (!$transaction->delete()) {
                //             throw new Exception("Error while deleting records from Transaction.");
                //         }
                //     }
                // }

                // if (!empty($data->items)) {
                //     foreach ($data->items as $_item) {
                //         $_item->is_deleted = 1;
                //         $_item->deleted_at = cur_date_time();
                //         $_item->deleted_by = Auth::id();
                //         if (!$_item->save()) {
                //             throw new Exception("Error while deleting records.");
                //         }
                //     }
                // }

                $data->is_deleted = 1;
                $data->deleted_at = cur_date_time();
                $data->deleted_by = Auth::id();
                if (!$data->save()) {
                    throw new Exception("Error while deleting records.");
                }
            }

            DB::commit();
            $resp['success'] = true;
            $resp['message'] = 'Item Deleted Successfully';
        } catch (Exception $e) {
            DB::rollback();
            $resp['success'] = false;
            $resp['message'] = $e->getMessage();
        }
        return $resp;
    }

    public function itemlist($id) {
        check_user_access('ricemill_purchase_items');
        $_data = Purchase::where('_key', $id)->first();
        $_prod_type = ProductType::where([['is_deleted', 0], ['company_id', RICEMILL]])->get();
        $_unitList = ProductUnit::where([['is_deleted', 0], ['company_id', RICEMILL]])->get();
        $_price_unit = PriceUnit::where('is_deleted', 0)->get();
        $_itemDataset = PurchaseItem::where([['purchase_id', $_data->id], ['is_deleted', 0]])->get();
        $_current_qty = PurchaseItem::model()->current_qty($_data->challan_no);
        $_previous_amount = Transaction::model()->particularBalance($_data->particular_id);

        $this->model['product_type'] = $_prod_type;
        $this->model['current_qty'] = $_current_qty;
        $this->model['itemDataset'] = $_itemDataset;
        $this->model['data'] = $_data;
        $this->model['unitList'] = $_unitList;
        $this->model['vehicle_rent'] = $_vehicleRent = $_data->vehicle_rent;
        $this->model['purchaseId'] = $_vehicleRent = $_data->id;
        $this->model['previous_amount'] = $_previous_amount;
        $this->model['price_unit'] = $_price_unit;
        return view('ricemill.purchases.itemlist', $this->model);
    }

    public function add_item(Request $r) {
        //pr($_POST);
        $input = $r->all();
        $rule = array(
            'product_id' => 'required',
        );
        $messages = array(
            'product_id.required' => 'Order Date required',
        );

        $valid = Validator::make($input, $rule, $messages);
        if ($valid->fails()) {
            return redirect()->back()->withErrors($valid)->withInput();
        }

        DB::beginTransaction();
        try {
            $model = Purchase::find($r->purchases_id);
            $modelItem = new PurchaseItem();
            $modelItem->company_id = $model->company_id;
            if ($r->product_type != EMPTY_BAG) {
                $item_qty = PurchaseItem::where([['company_id', $r->company_id], ['purchase_id', $r->purchases_id], ['is_deleted', 0], ['product_type_id', '!=', EMPTY_BAG]])->sum('quantity');
                $total_qty = $item_qty + $r->qty;
                $_bag_qty = $r->bag_qty;
                if ($total_qty > $_bag_qty) {
                    throw new Exception("<b>Quantity must be equal $_bag_qty</b>");
                }
            }
            $modelItem->purchase_id = $model->id;
            $modelItem->date = $model->date;
            $modelItem->year = date_yy($model->date);
            $modelItem->month = date_mm($model->date);
            $modelItem->challan_no = $model->challan_no;
            $modelItem->order_no = $model->order_no;
            $modelItem->price_unit_qty = $r->price_qty_unit;
            $modelItem->net_qty = $r->net_qty;
            if ($r->product_type == EMPTY_BAG) {
                $modelItem->purchases_type = PURCHASES_EMPTYBAG;
                $modelItem->price_unit_qty = $r->qty;
                $modelItem->net_qty = $r->qty;
            }
            $modelItem->product_type_id = $r->product_type;
            $modelItem->category_id = $r->category;
            $modelItem->product_id = $r->product_id;
            $modelItem->size_id = $r->size_id;
            $modelItem->unit_id = $r->unit_id;
            $modelItem->quality_id = $r->quality_id;
            $modelItem->price_unit_id = $r->price_unit_id;
            $modelItem->quantity = $r->qty;
            $modelItem->avg_weight = $r->_avg_weight;
            $modelItem->net_weight = $r->net_weight;
            $modelItem->rate = $r->rate;
            $modelItem->per_kg_rate = $r->per_kg_rate;
            $modelItem->price = $r->price;
            $modelItem->size_weight = $r->size_weight;
            $modelItem->avg_price = $r->avg_price;
            $modelItem->created_by = Auth::id();
            $modelItem->created_at = cur_date_time();
            $modelItem->_key = uniqueKey();
            if (!$modelItem->save()) {
                throw new Exception("Error! while saving order items record.");
            }

            DB::commit();
            $this->response['success'] = true;
            $this->response['message'] = 'Item saved successfully.';
        } catch (Exception $e) {
            DB::rollback();
            $this->response['success'] = false;
            $this->response['message'] = $e->getMessage();
        }
        return $this->response;
    }

    public function search_itemlist(Request $r) {
        //pr($_POST);
        $_itemDataset = PurchaseItem::where([['company_id', $r->company_id], ['purchase_id', $r->purchases_id], ['is_deleted', 0]])->get();
        $this->model['itemDataset'] = $_itemDataset;
        return view('ricemill.purchases._itemlist_partial', $this->model);
    }

    public function remove_item($id) {
        DB::beginTransaction();
        try {
            $data = PurchaseItem::find($id);
            if (!$data->delete()) {
                throw new Exception("Error while removing record.");
            }

            DB::commit();
            return redirect()->back()->with('success', 'Item removed successfully.');
        } catch (Exception $e) {
            DB::rollback();
            return redirect()->back()->with('danger', $e->getMessage());
        }
    }

    public function edit_item($id) {
        $data = PurchaseItem::find($id);
        $this->model['data'] = $data;
        return view('ricemill.purchases.items_edit', $this->model);
    }

    public function update_item(Request $r) {
        DB::beginTransaction();
        try {
            $model = PurchaseItem::find($r->purchases_id);
            $model->rate = $r->rate;
            $model->price = $r->price;
            if (!$model->save()) {
                throw new Exception("Error! while updating purchase item.");
            }
            DB::commit();
            return redirect('ricemill/purchase/items')->with('success', 'Purchases Updated Successfully.');
        } catch (Exception $e) {
            DB::rollback();
            return redirect()->back()->with('danger', $e->getMessage());
        }
    }

    public function get_challanno(Request $r) {
        $num = Purchase::model()->get_challanno($r->particularId);
        return $num;
    }

}
