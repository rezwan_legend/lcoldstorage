<?php

namespace App\Http\Controllers;
use Exception;
use DB;
use App\Http\Controllers\HomeController;
use Illuminate\Http\Request;
use App\Models\ProductType;
use App\Models\Product;
use App\Models\Category;
use App\Models\PriceSetting;
use App\Models\Stock;
use App\Models\ProductUnit;
use App\Models\PriceUnit;
use App\Models\UnitSize;

class StockController extends HomeController {

    public function index() {
        check_user_access('stock_list');
        $_product_type = Stock::where('is_deleted', 0)->get();
         $institutes = $this->userInstitutes($this->currentRouteFile());

        $this->model['institutes'] = $institutes;
        $this->model['product_type'] = $_product_type;
        return view('stocks.index', $this->model);
    }

    // Ajax Functions
    public function search(Request $r) {
        //  pr($_POST);
        $institute_id = $r->institute_id;
        $item_count = $r->item_count;
        $_type = $r->prod_type;
        $_category = $r->category;
        $_product_id = $r->product_id;
        $_size = $r->size_id;

        $query = Stock::query();
        if (!empty($institute_id)) {
            $query->where('company_id', $institute_id);
            $this->searchValues["Product Type"] = $_type;
        }
        if (!empty($_type)) {
            $query->where('product_type_id', $_type);
            $this->searchValues["Product Type"] = $_type;
        }
        if (!empty($_category)) {
            $query->where('category_id', $_category);
            $this->searchValues["Category"] = $_category;
        }
        if (!empty($_product_id)) {
            $query->where('product_id', $_product_id);
            $this->searchValues["Product"] = $_product_id;
        }
        if (!empty($_size)) {
            $query->where('size_id', $_size);
            $this->searchValues["Size"] = $_size;
        }
        $query->groupBy('product_id', 'size_id');
        $dataset = $query->paginate($item_count);
        $this->model['productTypeId'] = !empty($_type) ? $_type : null;
        $this->model['categoryId'] = !empty($_category) ? $_category : null;
        $this->model['productId'] = !empty($_product_id) ? $_product_id : null;
        $this->model['sizeId'] = !empty($_size) ? $_size : null;
        $this->model['institute_id'] = $institute_id;
        $this->model['dataset'] = $dataset;
        return view('ricemill.stocks._list', $this->model);
    }

    public function ebagStockInfo(Request $r) {
        try {
            $condArr = ['company_id' => $r->companyId, 'product_type_id' => $r->product_type, 'category_id' => $r->category_id, 'product_id' => $r->product_id];
            $size_id = isset($r->size_id) ? $r->size_id : NULL;
            if (!is_null($size_id)) {
                $condArr['size_id'] = $size_id;
            }

            $available_stock = Stock::model()->sum_value_stock_info($condArr);
            if ($available_stock <= 0) {
                throw new Exception("<b>Stock is not available </b>");
            }
            $_remain_price = Stock::model()->sum_price_remain('price', $condArr);
            $rate = round($_remain_price / $available_stock, 2);

            $this->response['success'] = true;
            $this->response['sale_price'] = !empty($rate) ? $rate : 0;
            $this->response['available_stock'] = $available_stock;
        } catch (Exception $e) {
            $this->response['success'] = false;
            $this->response['message'] = $e->getMessage();
        }
        return $this->response;
    }

    public function productStockInfo(Request $r) {
        //pr($_POST);
        try {
            $condArr = ['company_id' => $r->companyId, 'category_id' => $r->category_id, 'product_id' => $r->product_id];
            $size_id = isset($r->size_id) ? $r->size_id : NULL;
            if (!is_null($size_id)) {
                $condArr['size_id'] = $size_id;
            }
            $_stock_available_qty = Stock::model()->sum_value_stock_info($condArr);

            if ($_stock_available_qty <= 0) {
                throw new Exception("<b>Stock is not available</b>");
            }
            $_remain_weight = Stock::model()->sum_weight_remain('net_weight', $condArr);
            if ($_remain_weight > 0) {
                $avg_weight = round(( $_remain_weight / $_stock_available_qty), 2);
                $this->response['avg_weight'] = $avg_weight;
            }

            $_avg_rate = Stock::model()->avg_rate($condArr);
            $this->response['success'] = true;
        } catch (Exception $e) {
            $this->response['success'] = false;
            $this->response['message'] = $e->getMessage();
        }
        return $this->response;
    }

    public function machineries_stock_info(Request $r) {
        //pr($_POST);
        try {
            $condArr = ['company_id' => $r->instiute_id, 'product_type_id' => $r->product_type, 'category_id' => $r->category_id, 'product_id' => $r->product_id];
            $size_id = isset($r->size_id) ? $r->size_id : NULL;
            if (!is_null($size_id)) {
                $condArr['size_id'] = $size_id;
            }
            $_stock_available_qty = Stock::model()->sum_value_stock_info($condArr);

            if ($_stock_available_qty <= 0) {
                throw new Exception("<b>Stock is not available</b>");
            }
            $remain_price = Stock::model()->sum_weight_remain('price', $condArr);

            $_data = PriceSetting::model()->price_info($condArr);
            if ($_stock_available_qty > 0) {
                $_avg_price = round(($remain_price / $_stock_available_qty), 3);
                $this->response['avg_rate'] = !empty($_avg_price) ? $_avg_price : $_data->cost_price;
            }

            $this->response['available_stock'] = $_stock_available_qty;
            $this->response['success'] = true;
        } catch (Exception $e) {
            $this->response['success'] = false;
            $this->response['message'] = $e->getMessage();
        }
        return $this->response;
    }

    public function productionStockInfo(Request $r) {
        try {
            $condArr = ['company_id' => $r->companyId, 'product_type_id' => $r->productType, 'category_id' => $r->category, 'product_id' => $r->product_id];
            $size_id = isset($r->size_id) ? $r->size_id : NULL;
            if (!is_null($size_id)) {
                $condArr['size_id'] = $size_id;
            }
            $_stock_available_qty = Stock::model()->sum_value_stock_info($condArr);

            if ($_stock_available_qty <= 0) {
                throw new Exception("<b>Stock is not available</b>");
            }
            if ($_stock_available_qty > 0) {
                $_net_weight_remain = Stock::model()->sum_weight_remain('net_weight', $condArr);
                $total_price_remain = Stock::model()->sum_weight_remain('price', $condArr);

                $avg_weight = round(($_net_weight_remain / $_stock_available_qty), 2);
                $avg_price = round(($total_price_remain / $_stock_available_qty), 2);

                $this->response['avg_price'] = $avg_price;
                $this->response['avg_weight'] = $avg_weight;
            }
            $this->response['success'] = true;
            $this->response['available_stock'] = $_stock_available_qty;
        } catch (Exception $e) {
            $this->response['success'] = false;
            $this->response['message'] = $e->getMessage();
        }
        return $this->response;
    }

    public function prodTransfer_stock_info(Request $r) {
        try {
            $condArr = ['category_id' => $r->category_id];
            $size_id = isset($r->size_id) ? $r->size_id : NULL;
            if (!is_null($size_id)) {
                $condArr['size_id'] = $size_id;
            }

            $_qty_in = Stock::model()->sum_in_production($r->companyId, $r->product_id, 'quantity', $condArr);
            $_qty_out = Stock::model()->sum_out_production($r->companyId, $r->product_id, 'quantity', $condArr);
            $_qty_remain = ($_qty_in - $_qty_out);

            $_net_weight_in = Stock::model()->sum_in_production($r->companyId, $r->product_id, 'net_weight', $condArr);
            $_net_weight_out = Stock::model()->sum_out_production($r->companyId, $r->product_id, 'net_weight', $condArr);
            $_net_weight_remain = ($_net_weight_in - $_net_weight_out);
            if ($_qty_remain <= 0) {
                throw new Exception("<b>Stock is not available</b>");
            }
            $avg_weight = round(($_net_weight_remain / $_qty_remain), 3);
            $condArr['institute_id'] = $r->companyId;
            $condArr['product_id'] = $r->product_id;
            $_stock_available_qty = Stock::model()->sum_value_stock_info($condArr);
            if ($_stock_available_qty <= 0) {
                throw new Exception("<b>Stock is not available</b>");
            }

            $_rate = Stock::model()->last_avgrate($r->companyId, $r->category_id, $r->product_id, $size_id);
            $condArr['institute_id'] = MAIN_INSTITUTE;
            $_data = PriceSetting::model()->price_info($condArr);
            $this->response['sale_price'] = !empty($_data) ? $_data->sale_price : $_rate;
            $this->response['cost_price'] = !empty($_data) ? $_data->cost_price : $_rate;

            $this->response['available_stock'] = $_stock_available_qty;
            $this->response['avg_weight'] = $avg_weight;

            $this->response['success'] = true;
        } catch (Exception $e) {
            $this->response['success'] = false;
            $this->response['message'] = $e->getMessage();
        }
        return $this->response;
    }



    public function openingStocks() {
        check_user_access('ricemill_raw_opening_stock');
        $stock = new Stock();
        $_itemDataset = Stock::where([['company_id', RICEMILL], ['type', STOCK_TYPE_OPENING]])->get();
        $product_type = ProductType::where([['is_deleted', 0], ['company_id', RICEMILL]])->get();
        $institutes = $this->userInstitutes($this->currentRouteFile());
        $this->model['product_type'] = $product_type;
        $this->model['stock'] = $stock;
        $this->model['itemDataset'] = $_itemDataset;
        $this->model['institutes'] = $institutes;
        return view('stocks.itemlist', $this->model);
    }

    public function add_item(Request $r) {
        //  pr($_POST);
        DB::beginTransaction();
        try {
            $deletePreOpeingStock = Stock::where([['company_id', $r->institute], ['category_id', $r->category], ['product_id', $r->product_id], ['size_id', $r->size_id], ['type', STOCK_TYPE_OPENING]])->delete();

            $model = new Stock();
            $model->company_id = $r->institute;
            $model->size_id = $r->size_id;
            $model->date = !empty($r->opening_date) ? date_ymd($r->opening_date) : date('Y-m-d');
            $model->product_type_id = $r->prod_type;
            $model->category_id = $r->category;
            $model->product_id = $r->product_id;
            $model->unit_id = $r->unit_id;
            $model->quantity = $r->qty;
            $model->net_weight = $r->net_weight;
            $model->price_unit_id = $r->price_unit_id;
            $model->price_unit_qty = $r->price_qty_unit;
            $model->rate = $r->rate;
            $model->price = $r->price;
            $model->avg_weight = round($model->net_weight / $model->quantity, 3);
            $model->type = STOCK_TYPE_OPENING;
            if (!$model->save()) {
                throw new Exception("Error while opening stock");
            }
            DB::commit();
            $this->response['success'] = true;
            $this->response['message'] = 'Item saved successfully.';
        } catch (Exception $e) {
            DB::rollback();
            $this->response['success'] = false;
            $this->response['message'] = $e->getMessage();
        }
        return $this->response;
    }

    public function search_itemlist(Request $r) {
        $_itemDataset = Stock::where([['date', date_ymd($r->date)], ['company_id', $r->institute_id], ['type', STOCK_TYPE_OPENING]])->get();
        $this->model['itemDataset'] = $_itemDataset;
        return view('ricemill.stocks._itemlist_partial', $this->model);
    }

    public function remove_item($id) {
        DB::beginTransaction();
        try {
            $data = Stock::find($id);
            if (!$data->delete()) {
                throw new Exception("Error while removing record.");
            }
            DB::commit();
            return redirect()->back()->with('success', 'Item removed successfully.');
        } catch (Exception $e) {
            DB::rollback();
            return redirect()->back()->with('danger', $e->getMessage());
        }
    }



    public function edit_item($id) {
        $data = Stock::find($id);
        $categories = Category::where('is_deleted', 0)->get();
        $products = Product::where('is_deleted', 0)->get();
        $sizes = UnitSize::where('is_deleted', 0)->get();
        $sizeWeight = UnitSize::find($data->size_id)->weight;
        $this->model['data'] = $data;
        $this->model['categories'] = $categories;
        $this->model['products'] = $products;
        $this->model['sizes'] = $sizes;
        $this->model['sizeWeight'] = $sizeWeight;
        return view('ricemill.stocks.items_edit', $this->model);
    }

    public function update_item(Request $r) {
        // pr($_POST);
        DB::beginTransaction();
        try {
            $model = Stock::find($r->stock_id);
            $model->company_id = $r->institute;
            $model->size_id = $r->size_id;
            $model->date = !empty($r->opening_date) ? date_ymd($r->opening_date) : date('Y-m-d');
            $model->product_type_id = $r->prod_type;
            $model->category_id = $r->category;
            $model->product_id = $r->product_id;
            $model->unit_id = $r->unit_id;
            $model->quantity = $r->qty;
            $model->net_weight = $r->net_weight;
            $model->price_unit_id = $r->price_unit_id;
            $model->price_unit_qty = $r->price_qty_unit;
            $model->rate = $r->rate;
            $model->price = $r->price;
            $model->avg_weight = round($model->net_weight / $model->quantity, 3);
            if (!$model->save()) {
                throw new Exception("Error! while updating purchase item.");
            }
            DB::commit();
            return redirect('ricemill/stocks')->with('success', 'Purchases Updated Successfully.');
        } catch (Exception $e) {
            DB::rollback();
            return redirect()->back()->with('danger', $e->getMessage());
        }
    }

}
