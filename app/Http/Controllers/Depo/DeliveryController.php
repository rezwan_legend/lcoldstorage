<?php

namespace App\Http\Controllers\Depo;

use Auth;
use DB;
use Exception;
use Validator;
use Illuminate\Http\Request;
use App\Models\Account\Particular;
use App\Models\Account\Transaction;
use App\Models\ProductType;
use App\Models\Category;
use App\Models\Product;
use App\Models\Sr\Sale;
use App\Models\Sr\SaleItem;
use App\Models\Depo\Delivery;
use App\Models\Depo\DeliveryItem;
use App\Models\Depo\Stock as DepoStock;
use App\Models\Distribution\Setting as DistributionSetting;
use App\Http\Controllers\HomeController;

class DeliveryController extends HomeController {

    public function index() {
        check_user_access('depo_delivery_list');
        $query = Delivery::query();
        if ($this->currentUser->user_type != USER_TYPE_ADMIN) {
            if ($this->currentUser->user_type == USER_TYPE_SR) {
                $this->idArray[] = $this->currentUser->particular_id;
            } else {
                $this->idArray = DistributionSetting::model()->childIdArr($this->currentUser->particular_id);
            }
            $query->whereIn('sr_id', $this->idArray);
        }
        $_dataset = $query->orderBy('challan_no', 'DESC')->paginate($this->getSettings()->pagesize);

        $this->model['page_title'] = "Delivery List";
        $this->model['dataset'] = $_dataset;
        return view('depo.delivery.index', $this->model);
    }

    public function create($id) {
        check_user_access('depo_delivery_create');
        $_model = Sale::where('_key', $id)->first();
        $_modelDelivery = new Delivery();
        $_modelDeliveryItem = new DeliveryItem();
        $_depoStockModel = new DepoStock();

        $this->model['page_title'] = "Delivery Information";
        $this->model['model'] = $_model;
        $this->model['challan_no'] = $_modelDelivery->getChallanNo();
        $this->model['delivery_item'] = $_modelDeliveryItem;
        $this->model['depo_stock'] = $_depoStockModel;
        return view('depo.delivery.create', $this->model);
    }

    public function store(Request $r) {
        //pr($_POST);
        $input = $r->all();
        $rule = array(
            'challan_date' => 'required',
            'challan_no' => 'required'
        );
        $messages = array(
            'challan_date.required' => 'order date required',
            'challan_no.required' => 'order number required'
        );

        $valid = Validator::make($input, $rule, $messages);
        if ($valid->fails()) {
            return redirect()->back()->withErrors($valid)->withInput();
        }

        DB::beginTransaction();
        try {
            $model = new Delivery();
            $challan = $model->where('challan_no', $r->challan_no)->first();
            if (!empty($challan)) {
                throw new Exception("Challan number {$challan} already taken.");
            }

            $model->depo_id = $r->depo_id;
            $model->sr_id = $r->sr_id;
            $model->sr_sale_id = $r->sr_sale_id;
            $model->customer_id = $r->customer_id;
            $model->challan_date = !empty($r->challan_date) ? date_ymd($r->challan_date) : date('Y-m-d');
            $model->challan_no = $r->challan_no;
            $model->note = $r->note;
            $model->total_order_qty = $r->torder_qty;
            $model->total_stock_qty = $r->tstock_qty;
            $model->total_challan_qty = $r->tentry_qty;
            $model->total_remain_qty = $r->tremain_qty;
            $model->created_at = dbTimestamp();
            $model->created_by = Auth::id();
            $model->_key = uniqueKey();
            if (!$model->save()) {
                throw new Exception("Error! while saving record.");
            }
            $lastId = DB::getPdo()->lastInsertId();

            if (isset($r->item_id) && count($r->item_id) > 0) {
                foreach ($r->item_id as $key => $value) {
                    if ($r->entry_qty[$value] <= 0) {
                        throw new Exception("Submitted quantity must be > 0.");
                    }

                    $saleItem = SaleItem::find($value);
                    $modelItem = new DeliveryItem();
                    $modelItem->depo_id = $model->depo_id;
                    $modelItem->sr_id = $model->sr_id;
                    $modelItem->sr_sale_id = $model->sr_sale_id;
                    $modelItem->sr_sale_item_id = $saleItem->id;
                    $modelItem->delivery_id = $lastId;
                    $modelItem->challan_date = $model->challan_date;
                    $modelItem->product_type_id = $saleItem->product_type_id;
                    $modelItem->category_id = $saleItem->category_id;
                    $modelItem->product_id = $saleItem->product_id;
                    $modelItem->product_description_id = $saleItem->product_description_id;
                    $modelItem->barcode = !empty($saleItem->barcode) ? $saleItem->barcode : NULL;
                    $modelItem->order_qty = $r->order_qty[$value];
                    $modelItem->stock_qty = $r->stock_qty[$value];
                    $modelItem->challan_qty = $r->entry_qty[$value];
                    $modelItem->remain_qty = $r->remain_qty[$value];
                    if (!$modelItem->save()) {
                        throw new Exception("Error! while savings items record.");
                    }
                }
            }

            DB::commit();
            return redirect('/depo/delivery')->with('success', 'Record saved successfully.');
        } catch (Exception $e) {
            DB::rollback();
            return redirect()->back()->with('danger', $e->getMessage());
        }
    }

    public function edit($id) {
        check_user_access('depo_delivery_edit');
        $model = Delivery::find($id);
        $_depoStockModel = new DepoStock();

        $this->model['page_title'] = "Delivery Information";
        $this->model['model'] = $model;
        $this->model['depo_stock'] = $_depoStockModel;
        return view('depo.delivery.edit', $this->model);
    }

    public function update(Request $r, $id) {
        //pr($_POST);
        $input = $r->all();
        $rule = array(
            'challan_date' => 'required',
            'challan_no' => 'required'
        );
        $messages = array(
            'challan_date.required' => 'order date required',
            'challan_no.required' => 'order number required'
        );

        $valid = Validator::make($input, $rule, $messages);
        if ($valid->fails()) {
            return redirect()->back()->withErrors($valid)->withInput();
        }

        DB::beginTransaction();
        try {
            $model = Delivery::find($id);
            $model->challan_date = !empty($r->challan_date) ? date_ymd($r->challan_date) : date('Y-m-d');
            $model->challan_no = $r->challan_no;
            $model->note = $r->note;
            $model->total_order_qty = $r->torder_qty;
            $model->total_stock_qty = $r->tstock_qty;
            $model->total_challan_qty = $r->tentry_qty;
            $model->total_remain_qty = $r->tremain_qty;
            $model->modified_at = dbTimestamp();
            $model->modified_by = Auth::id();
            if (!$model->save()) {
                throw new Exception("Error! while updating record.");
            }

            if (isset($r->item_id) && count($r->item_id) > 0) {
                foreach ($r->item_id as $key => $value) {
                    $modelItem = DeliveryItem::find($value);
                    $modelItem->challan_date = $model->challan_date;
                    //$modelItem->product_description_id = $r->product_description_id[$value];
                    //$modelItem->barcode = !empty($r->barcode) ? $r->barcode[$value] : NULL;
                    $modelItem->order_qty = $r->order_qty[$value];
                    $modelItem->stock_qty = $r->stock_qty[$value];
                    $modelItem->challan_qty = $r->entry_qty[$value];
                    $modelItem->remain_qty = $r->remain_qty[$value];
                    if (!$modelItem->save()) {
                        throw new Exception("Error! while updating item records.");
                    }
                }
            }

            DB::commit();
            return redirect('/depo/delivery')->with('success', 'Record updated successfully.');
        } catch (Exception $e) {
            DB::rollback();
            return redirect()->back()->with('danger', $e->getMessage());
        }
    }

    public function show($id) {
        check_user_access('depo_delivery_detail');
        $data = Delivery::find($id);
        //pr($data);

        $this->model['page_title'] = "Delivery Information";
        $this->model['data'] = $data;
        return view('depo.delivery.details', $this->model);
    }

    public function confirm($id) {
        check_user_access('depo_delivery_process');
        $data = Delivery::find($id);

        DB::beginTransaction();
        try {
            if ($data->process_status == STATUS_PROCESED) {
                return redirect('delivery')->with("info", "Challan {$data->challan_no} already processed.");
            }
            if (empty($data->items) && count($data->items) < 1) {
                throw new Exception("No items found to process.");
            }

            foreach ($data->items as $item) {
                $_stock = new DepoStock();
                $_stock->order_date = $item->challan_date;
                $_stock->month = date_mm($item->challan_date);
                $_stock->year = date_yy($item->challan_date);
                $_stock->depo_id = $data->depo_id;
                //$_stock->head_id = $data->challan_date;
                //$_stock->subhead_id = $data->challan_date;
                //$_stock->particular_id = $data->challan_date;
                $_stock->sr_sale_id = $item->sr_sale_id;
                $_stock->sr_sale_item_id = $item->sr_sale_item_id;
                $_stock->delivery_id = $data->id;
                $_stock->delivery_item_id = $item->id;
                $_stock->product_type_id = $item->product_type_id;
                $_stock->category_id = $item->category_id;
                $_stock->product_id = $item->product_id;
                $_stock->product_description_id = $item->product_description_id;
                $_stock->barcode = $item->barcode;
                $_stock->stock_type = STOCK_TYPE_DELIVERY;
                $_stock->type = STOCK_OUT;
                $_stock->quantity = $item->challan_qty;
                $_stock->created_at = dbTimestamp();
                $_stock->created_by = Auth::id();
                $_stock->_key = uniqueKey() . $item->id;
                if (!$_stock->save()) {
                    throw new Exception("Error while processing stock record.");
                }

                $item->process_status = STATUS_PROCESED;
                if (!$item->save()) {
                    throw new Exception("Error while processing items record.");
                }
            }

            $_transModel = new Transaction();
            $_transModel->head_id = $data->sale->head_id;
            $_transModel->subhead_id = $data->sale->subhead_id;
            $_transModel->particular_id = $data->sale->particular_id;
            $_transModel->sr_sale_id = $data->sale->id;
            $_transModel->depo_id = $data->sale->depo_id;
            $_transModel->type = 'C';
            $_transModel->voucher_type = SALES_VOUCHER;
            $_transModel->voucher_no = $data->sale->order_no;
            $_transModel->payment_method = PAYMENT_NO;
            $_transModel->from_head_id = $data->sale->from_head_id;
            $_transModel->from_subhead_id = $data->sale->from_subhead_id;
            $_transModel->from_particular_id = $data->sale->from_particular_id;
            $_transModel->to_head_id = $data->sale->to_head_id;
            $_transModel->to_subhead_id = $data->sale->to_subhead_id;
            $_transModel->to_particular_id = $data->sale->to_particular_id;
            $_transModel->date = $data->sale->order_date;
            $_transModel->description = $data->sale->note;
            $_transModel->by_whom = Auth::user()->name;
            $_transModel->debit = $data->sale->net_amount;
            $_transModel->credit = $data->sale->net_amount;
            $_transModel->amount = $data->sale->net_amount;
            $_transModel->created_at = dbTimestamp();
            $_transModel->created_by = Auth::id();
            $_transModel->_key = $data->_key;
            if (!$_transModel->save()) {
                throw new Exception("Error while processing delivery transaction.");
            }

            $data->process_status = STATUS_PROCESED;
            if (!$data->save()) {
                throw new Exception("Error while processing record.");
            }

            DB::commit();
            return redirect('/depo/delivery')->with('success', 'Record processed successfully.');
        } catch (Exception $e) {
            DB::rollback();
            return redirect('/depo/delivery')->with('danger', $e->getMessage());
        }
    }

    public function items() {
        check_user_access('depo_delivery_items');
        $_product_types = ProductType::model()->getList();
        $_categories = Category::model()->getList();
        $_depoList = Particular::model()->getListByHeadId(HEAD_DEPO_ACCOUNT);
        $_srList = Particular::model()->getListByHeadId(HEAD_SR_ACCOUNT);

        $query = DeliveryItem::query();
        if ($this->currentUser->user_type != USER_TYPE_ADMIN) {
            if ($this->currentUser->user_type == USER_TYPE_SR) {
                $this->idArray[] = $this->currentUser->particular_id;
            } else {
                $this->idArray = DistributionSetting::model()->childIdArr($this->currentUser->particular_id);
            }
            $query->whereIn('sr_id', $this->idArray);
        }
        $_dataset = $query->orderBy('challan_date', 'DESC')->paginate($this->getSettings()->pagesize);

        $this->model['page_title'] = "Delivery Items";
        $this->model['product_types'] = $_product_types;
        $this->model['categories'] = $_categories;
        $this->model['depoList'] = $_depoList;
        $this->model['srList'] = $_srList;
        $this->model['dataset'] = $_dataset;
        return view('depo.delivery.items', $this->model);
    }

    public function ledger() {
        check_user_access('depo_delivery_ledger');
        $_brands = Particular::model()->getList(SUBHEAD_COMPANY);
        $_product_types = ProductType::model()->getList();
        $_categories = Category::model()->getList();

        $query = DeliveryItem::query();
        if ($this->currentUser->user_type != USER_TYPE_ADMIN) {
            if ($this->currentUser->user_type == USER_TYPE_SR) {
                $this->idArray[] = $this->currentUser->particular_id;
            } else {
                $this->idArray = DistributionSetting::model()->childIdArr($this->currentUser->particular_id);
            }
            $query->whereIn('sr_id', $this->idArray);
        }
        $_dataset = $query->orderBy('challan_date', 'desc')->paginate($this->getSettings()->pagesize);

        $this->model['page_title'] = "Delivery Ledger";
        $this->model['brands'] = $_brands;
        $this->model['product_types'] = $_product_types;
        $this->model['categories'] = $_categories;
        $this->model['dataset'] = $_dataset;
        return view('depo.delivery.ledger', $this->model);
    }

    // Ajax Functions
    public function search(Request $r) {
        $item_count = !empty($r->item_count) ? $r->item_count : $this->getSettings()->pagesize;
        $_search_challan = $r->search_challan;
        $_status = $r->status;
        $from_date = $r->from_date;
        $end_date = $r->end_date;

        $query = Delivery::query();
        if ($this->currentUser->user_type != USER_TYPE_ADMIN) {
            if ($this->currentUser->user_type == USER_TYPE_SR) {
                $this->idArray[] = $this->currentUser->particular_id;
            } else {
                $this->idArray = DistributionSetting::model()->childIdArr($this->currentUser->particular_id);
            }
            $query->whereIn('sr_id', $this->idArray);
        }
        if (!empty($_search_challan)) {
            $query->where('challan_no', $_search_challan);
        }
        if (!empty($_status)) {
            $query->where('process_status', $_status);
        }
        if (!empty($from_date) && !empty($end_date)) {
            $query->whereBetween('challan_date', [date_ymd($from_date), date_ymd($end_date)]);
        }
        $query->orderBy('challan_no', 'DESC');
        $dataset = $query->paginate($item_count);

        $this->model['dataset'] = $dataset;
        return view('depo.delivery._list', $this->model);
    }

    public function search_items(Request $r) {
        $item_count = !empty($r->item_count) ? $r->item_count : $this->getSettings()->pagesize;
        $_product_type_id = $r->product_type_id;
        $_category_id = $r->category_id;
        $_depo_id = $r->depo_id;
        $_sr_id = $r->sr_id;
        $_search_customer = $r->search_customer;
        $from_date = $r->from_date;
        $end_date = $r->end_date;
        $idArr = [];

        $query = DeliveryItem::query();
        if ($this->currentUser->user_type != USER_TYPE_ADMIN) {
            if ($this->currentUser->user_type == USER_TYPE_SR) {
                $this->idArray[] = $this->currentUser->particular_id;
            } else {
                $this->idArray = DistributionSetting::model()->childIdArr($this->currentUser->particular_id);
            }
            $query->whereIn('sr_id', $this->idArray);
        }
        if (!empty($_product_type_id)) {
            $query->where('product_type_id', $_product_type_id);
        }
        if (!empty($_category_id)) {
            $query->where('category_id', $_category_id);
        }
        if (!empty($_depo_id)) {
            $query->where('depo_id', $_depo_id);
        }
        if (!empty($_sr_id)) {
            $_srSaleList = Sale::where([['particular_id', $_sr_id], ['is_deleted', 0]])->get();
            foreach ($_srSaleList as $_srSale) {
                $idArr[] = $_srSale->id;
            }
            $query->whereIn('sr_sale_id', $idArr);
        }
        if (!empty($_search_customer)) {
            $_cutomerList = Particular::where('is_deleted', 0)
                    ->where('name', 'like', '%' . $_search_customer . '%')
                    ->orWhere('mobile', 'like', '%' . to_eng($_search_customer) . '%')
                    ->orWhere('code', 'like', '%' . to_eng($_search_customer) . '%')
                    ->orWhere('address', 'like', '%' . to_eng($_search_customer) . '%')
                    ->get();

            $_cidArr = [];
            foreach ($_cutomerList as $_cutomer) {
                $_cidArr[] = $_cutomer->id;
            }

            $_deliveryList = Delivery::whereIn('customer_id', $_cidArr)->get();
            foreach ($_deliveryList as $_delivery) {
                $idArr[] = $_delivery->id;
            }
            $query->whereIn('delivery_id', $idArr);
        }
        if (!empty($from_date) && !empty($end_date)) {
            $query->whereBetween('challan_date', [date_ymd($from_date), date_ymd($end_date)]);
        }
        $query->orderBy('challan_date', 'DESC');
        $dataset = $query->paginate($item_count);

        $this->model['dataset'] = $dataset;
        return view('depo.delivery._items', $this->model);
    }

    public function ledger_search(Request $r) {
        $item_count = !empty($r->item_count) ? $r->item_count : $this->getSettings()->pagesize;
        $search_brand = $r->search_brand;
        $search_category = $r->search_category;
        $search_product = $r->search_product;
        $search_supplier = $r->search_supplier;
        $from_date = $r->from_date;
        $end_date = $r->end_date;
        $productId = [];
        $particularId = [];
        $purchaseId = [];

        $query = DeliveryItem::query();
        if ($this->currentUser->user_type != USER_TYPE_ADMIN) {
            if ($this->currentUser->user_type == USER_TYPE_SR) {
                $this->idArray[] = $this->currentUser->particular_id;
            } else {
                $this->idArray = DistributionSetting::model()->childIdArr($this->currentUser->particular_id);
            }
            $query->whereIn('sr_id', $this->idArray);
        }
        if (!empty($search_brand)) {
            $_productList = Product::where('company', $search_brand)->get();
            foreach ($_productList as $_product) {
                $productId[] = $_product->id;
            }
            $query->whereIn('product_id', $productId);
        }
        if (!empty($search_category)) {
            $query->where('category_id', $search_category);
        }
        if (!empty($search_product)) {
            $_proList = Product::where('name', 'like', '%' . $search_product . '%')->get();
            foreach ($_proList as $_prod) {
                $productId[] = $_prod->id;
            }
            $query->whereIn('product_id', $productId);
        }
        if (!empty($search_supplier)) {
            $_partyList = Particular::where('name', 'like', '%' . trim($search_supplier) . '%')
                    ->orWhere('address', 'like', '%' . trim($search_supplier) . '%')
                    ->orWhere('mobile', 'like', '%' . to_eng($search_supplier) . '%')
                    ->get();
            foreach ($_partyList as $_party) {
                $particularId[] = $_party->id;
            }

            $_purchaseList = Sale::where('is_deleted', 0)->whereIn('to_particular_id', $particularId)->get();
            foreach ($_purchaseList as $_purchase) {
                $purchaseId[] = $_purchase->id;
            }
            $query->whereIn('sale_id', $purchaseId);
        }
        if (!empty($from_date) && !empty($end_date)) {
            $query->whereBetween('order_date', [date_ymd($from_date), date_ymd($end_date)]);
        }
        $dataset = $query->orderBy('challan_date', 'desc')->paginate($item_count);

        $this->model['dataset'] = $dataset;
        return view('depo.delivery._ledger', $this->model);
    }

    public function delete() {
        DB::beginTransaction();
        try {
            foreach ($_POST['data'] as $id) {
                $data = Delivery::find($id);
                if (!empty($data->stocks) && count($data->stocks) > 0) {
                    foreach ($data->stocks as $stock) {
                        if (!$stock->delete()) {
                            throw new Exception("Error while deleting stock records.");
                        }
                    }
                }

                if (!empty($data->items) && count($data->items) > 0) {
                    foreach ($data->items as $item) {
                        if (!$item->delete()) {
                            throw new Exception("Error while deleting item records.");
                        }
                    }
                }

                if (!$data->delete()) {
                    throw new Exception("Error while deleting records.");
                }
            }

            DB::commit();
            $this->response['success'] = true;
            $this->response['message'] = 'Record/s deleted successfully.';
        } catch (Exception $e) {
            DB::rollback();
            $this->response['success'] = false;
            $this->response['message'] = $e->getMessage();
        }
        return $this->response;
    }

    public function delete_item(Request $r) {
        $_itemID = $_POST['itemid'];

        DB::beginTransaction();
        try {
            $data = DeliveryItem::find($_itemID);
            if (!$data->delete()) {
                throw new Exception("Error while deleting item.");
            }

            DB::commit();
            $this->response['success'] = true;
            $this->response['message'] = 'Item deleted successfully.';
        } catch (Exception $e) {
            DB::rollback();
            $this->response['success'] = false;
            $this->response['message'] = $e->getMessage();
        }

        return $this->response;
    }

}
