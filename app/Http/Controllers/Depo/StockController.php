<?php

namespace App\Http\Controllers\Depo;

use Illuminate\Http\Request;
use App\Http\Controllers\HomeController;
use App\Models\Category;
use App\Models\Product;
use App\Models\Account\SubHead;
use App\Models\Account\Particular;
use App\Models\Depo\Stock;
use App\Models\Depo\DeliveryItem;
use App\Models\Distribution\Setting as DistributionSetting;
use App\Models\PriceSettingPurchase;
use App\Models\PriceSettingSale;
use Illuminate\Support\Collection;

class StockController extends HomeController {

    public function index() {
        check_user_access('depo_stocks_view');
        $_subheadList = SubHead::model()->getList(HEAD_DEPO_ACCOUNT);
        $_categories = Category::model()->getList(PRODUCT_TYPE_FINISH);

        $this->model['page_title'] = "Depo Stocks";
        $this->model['subheadList'] = $_subheadList;
        $this->model['categories'] = $_categories;
        return view('depo.stocks.index', $this->model);
    }

    public function search(Request $r) {
        $item_count = $r->item_count;
        $subhead_id = $r->subhead_id;
        $particular_id = $r->particular_id;
        $category_id = $r->category_id;
        $search_product = $r->product_name;
        $salePersonId = null;
        $_stock = new Stock();
        $_deliveryItem = new DeliveryItem();

        $query = Stock::query();
        if ($this->currentUser->user_type != USER_TYPE_ADMIN) {
            //$query->where('particular_id', $this->currentUser->particular_id);
            //$salePersonId = $this->currentUser->particular_id;
            if ($this->currentUser->user_type == USER_TYPE_SR) {
                $this->idArray[] = $this->currentUser->particular_id;
            } else {
                $this->idArray = DistributionSetting::model()->childIdArr($this->currentUser->particular_id);
            }
            $query->whereIn('depo_id', $this->idArray);
        }
        if (!empty($subhead_id)) {
            $query->where('subhead_id', $subhead_id);
            $this->searchValues['Depo Account'] = SubHead::find($subhead_id)->name;
        }
        if (!empty($particular_id)) {
            $query->where('particular_id', $particular_id);
            $this->searchValues['Account Name'] = Particular::find($particular_id)->name;
            $salePersonId = $particular_id;
        }
        if (!empty($category_id)) {
            $query->where('category_id', $category_id);
            $this->searchValues['Category'] = Category::find($category_id)->name;
        }
        if (!empty($search_product)) {
            $_productList = Product::where('is_deleted', 0)->where('name', 'like', '%' . $search_product . '%')->get();
            $idArr = [];
            foreach ($_productList as $_product) {
                $idArr[] = $_product->id;
            }
            $query->whereIn('product_id', $idArr);
            $this->searchValues['Product'] = $search_product;
        }
        $query->groupBy('product_id');
        $query->orderBy('category_id', 'DESC');
        $_resultSet = $query->get();

        $_dataArr = [];
        foreach ($_resultSet as $data) {
            $_in = $data->in_qty($data->product_id, $salePersonId);
            $_out = $data->out_qty($data->product_id, $salePersonId);
            $_sret = $data->sale_return_qty($data->product_id, $salePersonId);
            $_stock = $_in - ($_out + $_sret);
            if ($_stock > 0) {
                $_dataArr[] = $data;
            }
        }
        $_dataset = (new Collection($_dataArr))->paginate($item_count);

        $this->model['searchValues'] = $this->searchValues;
        $this->model['dataset'] = $_dataset;
        $this->model['salePersonId'] = $salePersonId;
        $this->model['stock'] = $_stock;
        $this->model['deliveryItem'] = $_deliveryItem;
        return view('depo.stocks._list', $this->model);
    }

    public function detail() {
        check_user_access('depo_stocks_view');
        $_subheadList = SubHead::model()->getList(HEAD_DEPO_ACCOUNT);
        $_categories = Category::model()->getList(PRODUCT_TYPE_FINISH);

        $this->model['page_title'] = "Depo Stocks Register";
        $this->model['subheadList'] = $_subheadList;
        $this->model['categories'] = $_categories;
        return view('depo.stocks.details', $this->model);
    }

    public function search_detail(Request $r) {
        $item_count = $r->item_count;
        $subhead_id = $r->subhead_id;
        $particular_id = $r->particular_id;
        $category_id = $r->category_id;
        $search_product = $r->product_name;
        $from_date = $r->from_date;
        $end_date = $r->end_date;
        $salePersonId = null;

        $_modelStock = new Stock();
        $priceSettingPurchase = new PriceSettingPurchase();
        $priceSettingSale = new PriceSettingSale();

        $currentDate = date("Y-m-d");
        $startDate = "2021-01-01";
        $endDate = !empty($from_date) ? date("Y-m-d", strtotime($from_date . "-1 day")) : date("Y-m-d", strtotime($currentDate . "-1 day"));

        $query = Stock::query();
        if ($this->currentUser->user_type != USER_TYPE_ADMIN) {
            $salePersonId = $this->currentUser->particular_id;
        }
        if (!empty($subhead_id)) {
            $query->where('subhead_id', $subhead_id);
            $this->searchValues['Depo Account'] = SubHead::find($subhead_id)->name;
        }
        if (!empty($particular_id)) {
            $query->where('particular_id', $particular_id);
            $salePersonId = $particular_id;
            $this->searchValues['Account Name'] = Particular::find($particular_id)->name;
        }
        if (!empty($category_id)) {
            $query->where('category_id', $category_id);
            $this->searchValues['Category'] = Category::find($category_id)->name;
        }
        if (!empty($search_product)) {
            $productList = Product::where('is_deleted', 0)->where('name', 'like', '%' . $search_product . '%')->get();
            $_pid = [];
            foreach ($productList as $product) {
                $_pid[] = $product->id;
            }
            $query->whereIn('product_id', $_pid);
            $this->searchValues['Product'] = $search_product;
        }
        if (!empty($from_date) && !empty($end_date)) {
            $this->searchValues['Dates'] = "{$from_date} | {$end_date}";
            $this->dateValues[0] = $from_date;
            $this->dateValues[1] = $end_date;
        } else {
            $_curDate = date('Y-m-d');
            $this->dateValues[0] = $_curDate;
            $this->dateValues[1] = $_curDate;
        }
        $query->groupBy('product_id');
        $query->orderBy('category_id', 'DESC');
        $_dataset = $query->paginate($item_count);

        $_curPage = $r->has('page') ? $r->get('page') : 1;
        $_prev_dataset = [];
        if ($_curPage > 1) {
            $prev_query = $query;
            $_prev_dataset = $prev_query->skip(0)->take(($_curPage - 1) * $item_count)->get();
        }

        $this->model['prevDataset'] = $_prev_dataset;
        $this->model['searchValues'] = $this->searchValues;
        $this->model['dateVal'] = !empty($from_date) ? $from_date : null;
        $this->model['startDate'] = $startDate;
        $this->model['endDate'] = $endDate;
        $this->model['dateValues'] = $this->dateValues;
        $this->model['dataset'] = $_dataset;
        $this->model['modelStock'] = $_modelStock;
        $this->model['salePersonId'] = $salePersonId;
        $this->model['priceSettingPurchase'] = $priceSettingPurchase;
        $this->model['priceSettingSale'] = $priceSettingSale;
        return view('depo.stocks._details', $this->model);
    }

}
