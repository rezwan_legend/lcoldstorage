<?php

namespace App\Http\Controllers\Depo;

use Illuminate\Http\Request;
use App\Models\Sr\Sale;
use App\Models\Depo\DeliveryItem;
use App\Models\Account\Particular;
use App\Models\Distribution\Setting as DistributionSetting;
use App\Http\Controllers\HomeController;

class SaleOrderController extends HomeController {

    public function index() {
        check_user_access('depo_sales_order_list');
        $_depoParticulars = Particular::model()->getListByHead(HEAD_DEPO_ACCOUNT);
        $_dsmParticulars = Particular::model()->getListByHead(HEAD_DSM_ACCOUNT);
        $_zmParticulars = Particular::model()->getListByHead(HEAD_ZM_ACCOUNT);
        $_srParticulars = Particular::model()->getListByHead(HEAD_SR_ACCOUNT);

        $this->model['page_title'] = "Depo Sale Orders";
        $this->model['depoParticulars'] = $_depoParticulars;
        $this->model['dsmParticulars'] = $_dsmParticulars;
        $this->model['zmParticulars'] = $_zmParticulars;
        $this->model['srParticulars'] = $_srParticulars;
        return view('depo.sale_order.index', $this->model);
    }

    // Ajax Functions
    public function search(Request $r) {
        $item_count = !empty($r->item_count) ? $r->item_count : $this->getSettings()->pagesize;
        $depo_value = $r->depo_value;
        $dsm_value = $r->dsm_value;
        $zm_value = $r->zm_value;
        $sr_value = $r->sr_value;
        $_search_order = $r->search_order;
        $_customer = $r->search_customer;
        $from_date = $r->from_date;
        $end_date = $r->end_date;
        $partyId = [];

        $query = Sale::where([['process_status', STATUS_PROCESED], ['is_deleted', 0]]);
        if ($this->currentUser->user_type != USER_TYPE_ADMIN) {
            $query->where('particular_id', $this->currentUser->particular_id);
        }
        if (!empty($depo_value)) {
            $idArr = DistributionSetting::model()->childIdArr($depo_value);
            $query->whereIn('particular_id', $idArr);
        }
        if (!empty($dsm_value)) {
            $idArr = DistributionSetting::model()->childIdArr($dsm_value);
            $query->whereIn('particular_id', $idArr);
        }
        if (!empty($zm_value)) {
            $idArr = DistributionSetting::model()->childIdArr($zm_value);
            $query->whereIn('particular_id', $idArr);
        }
        if (!empty($sr_value)) {
            $query->where('particular_id', $sr_value);
        }
        if (!empty($_search_order)) {
            $query->where('order_no', $_search_order);
        }
        if (!empty($_customer)) {
            $_partyList = Particular::where('name', 'like', '%' . trim($_customer) . '%')
                    ->orWhere('address', 'like', '%' . trim($_customer) . '%')
                    ->orWhere('mobile', 'like', '%' . to_eng($_customer) . '%')
                    ->get();
            foreach ($_partyList as $_party) {
                $partyId[] = $_party->id;
            }
            $query->whereIn('to_particular_id', $partyId);
        }
        if (!empty($from_date) && !empty($end_date)) {
            $query->whereBetween('order_date', [date_ymd($from_date), date_ymd($end_date)]);
        }
        $query->orderBy('order_date', 'DESC');
        $dataset = $query->paginate($item_count);

        $_modelItem = new DeliveryItem();
        $this->model['dataset'] = $dataset;
        $this->model['deliveryItem'] = $_modelItem;
        return view('depo.sale_order._list', $this->model);
    }

}
