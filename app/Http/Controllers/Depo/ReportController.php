<?php

namespace App\Http\Controllers\Depo;

use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\Distribution\Setting as DistributionSetting;
use App\Models\Account\SubHead;
use App\Models\Account\Particular;
use App\Models\Account\Transaction;
use App\Models\Sr\SaleItem;
use App\Http\Controllers\HomeController;

class ReportController extends HomeController {

    public function index() {
        $modelTrans = new Transaction();
        $receives = $modelTrans->totalReceiveByDate();
        $payments = $modelTrans->totalPaymentByDate();

        $date = date('Y-m-d');
        $from_date = '1970-01-01';
        $end_date = date('Y-m-d', strtotime($date . ' -1 day'));

        $sumReceive = $modelTrans->sumReceiveByDate($from_date, $end_date);
        $sumPayment = $modelTrans->sumPaymentByDate($from_date, $end_date);
        $opening_balance = ($sumReceive - $sumPayment);

        $this->model['date'] = $date;
        $this->model['opening_balance'] = $opening_balance;
        $this->model['receives'] = $receives;
        $this->model['payments'] = $payments;
        return view('Admin.dailyreport.index', $this->model);
    }

    public function monthly_sales() {
        check_user_access('monthly_sales_report');
        $_subheads = SubHead::model()->getList(HEAD_DEPO_ACCOUNT);
        $_categories = Category::model()->getList(PRODUCT_TYPE_FINISH);
        $_months = get_months();

        $this->model['page_title'] = "Depo Monthly Sales Report";
        $this->model['subheads'] = $_subheads;
        $this->model['categories'] = $_categories;
        $this->model['months'] = $_months;
        return view('depo.report.monthly_sales', $this->model);
    }

    public function search_monthly_sales(Request $r) {
        $_curYear = 2020;
        $_subhead_id = $r->subhead_id;
        $_sale_person_id = $r->sale_person_id;
        $_category_id = $r->category_id;
        $_month_id = $r->month_id;
        $_monthName = get_months($_month_id);
        $_days = cal_days_in_month(CAL_GREGORIAN, $_month_id, $_curYear);

        $_idArr = DistributionSetting::model()->childIdArr($_sale_person_id);
        $_retailers = Particular::whereIn('id', $_idArr)->get();

        if (!empty($_subhead_id)) {
            $this->searchValues['Sale Person A/C'] = SubHead::find($_subhead_id)->name;
        }
        if (!empty($_sale_person_id)) {
            $this->searchValues['Person Name'] = Particular::find($_sale_person_id)->name;
        }
        if (!empty($_category_id)) {
            $this->searchValues['Category'] = Category::find($_category_id)->name;
        }
        if (!empty($_month_id)) {
            $this->searchValues['Month'] = $_monthName . ', ' . $_curYear;
        }

        $this->model['searchValues'] = $this->searchValues;
        $this->model['monthName'] = $_monthName;
        $this->model['days'] = $_days;
        $this->model['curMonth'] = $_month_id;
        $this->model['curYear'] = $_curYear;
        $this->model['retailers'] = $_retailers;
        $this->model['subhead_id'] = $_subhead_id;
        $this->model['particular_id'] = $_sale_person_id;
        $this->model['category_id'] = !empty($_category_id) ? $_category_id : null;
        return view('depo.report._monthly_sales', $this->model);
    }

    public function product_sale() {
        check_user_access('product_sales_report');
        $_subheads = SubHead::model()->getList(HEAD_DEPO_ACCOUNT);
        $_categories = Category::model()->getList(PRODUCT_TYPE_FINISH);

        $this->model['page_title'] = "Depo Product Sales Report";
        $this->model['subheads'] = $_subheads;
        $this->model['categories'] = $_categories;
        return view('depo.report.product_sale', $this->model);
    }

    public function search_product_sale(Request $r) {
        $_subhead_id = $r->subhead_id;
        $_sale_person_id = $r->sale_person_id;
        $_category_id = $r->category_id;
        $_from_date = $r->from_date;
        $_end_date = $r->end_date;
        if (!empty($_subhead_id)) {
            $this->searchValues['Sale Person A/C'] = SubHead::find($_subhead_id)->name;
        }
        if (!empty($_sale_person_id)) {
            $this->searchValues['Person Name'] = Particular::find($_sale_person_id)->name;
        }

        $query = SaleItem::where([['process_status', STATUS_PROCESED], ['is_deleted', 0]]);
        if ($this->currentUser->user_type != USER_TYPE_ADMIN) {
            $query->where('sr_id', $_sale_person_id);
        }
        if (!empty($_category_id)) {
            $query->where('category_id', $_category_id);
            $this->searchValues['Category'] = Category::find($_category_id)->name;
        }
        if (!empty($_from_date) && !empty($_end_date)) {
            $query->whereBetween('order_date', [date_ymd($_from_date), date_ymd($_end_date)]);
            $this->dateValues[0] = $_from_date;
            $this->dateValues[1] = $_end_date;
            $this->searchValues['Dates'] = "{$_from_date} | {$_end_date}";
        }
        $query->orderBy('product_type_id', 'ASC');
        $query->groupBy('product_id');
        $dataset = $query->paginate($this->getSettings()->pagesize);

        $this->model['searchValues'] = $this->searchValues;
        $this->model['dateValues'] = $this->dateValues;
        $this->model['subhead_id'] = !empty($_subhead_id) ? $_subhead_id : '';
        $this->model['particular_id'] = !empty($_sale_person_id) ? $_sale_person_id : '';
        $this->model['category_id'] = !empty($_category_id) ? $_category_id : '';
        $this->model['dataset'] = $dataset;
        return view('depo.report._product_sale', $this->model);
    }

}
