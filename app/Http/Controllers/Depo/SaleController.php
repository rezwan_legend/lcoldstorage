<?php

namespace App\Http\Controllers\Depo;

use Auth;
use DB;
use Exception;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\HomeController;
use App\Models\AvgPrice;
use App\Models\ProductType;
use App\Models\Category;
use App\Models\Product;
use App\Models\Depo\Sale;
use App\Models\Depo\SaleItem;
use App\Models\Sr\SaleItem as SrSaleItem;
use App\Models\SaleReturnItem;
use App\Models\Depo\Stock;
use App\Models\Dealar\Stock as DealerStock;
use App\Models\Account\SubHead;
use App\Models\Account\Particular;
use App\Models\PriceSettingPurchase;
use App\Models\Account\Transaction;

class SaleController extends HomeController {

    public function index() {
        check_user_access('depo_sales_list');
        $dataset = Sale::where('is_deleted', 0)->orderBy('order_date', 'DESC')->paginate($this->getSettings()->pagesize);
        $modelItem = new SaleItem();

        $this->model['page_title'] = "Depo Sale List";
        $this->model['dataset'] = $dataset;
        $this->model['modelItem'] = $modelItem;
        return view('depo.sale.index', $this->model);
    }

    public function create() {
        check_user_access('depo_sales_create');
        $order_no = Sale::model()->getOrderNo();
        $_depoList = SubHead::model()->getList(HEAD_DEPO_ACCOUNT);
        $_subheads = SubHead::model()->getList(HEAD_DEALER_ACCOUNT);

        $this->model['page_title'] = "Depo Sale Information";
        $this->model['order_no'] = $order_no;
        $this->model['depoList'] = $_depoList;
        $this->model['subheads'] = $_subheads;
        return view('depo.sale.create', $this->model);
    }

    public function store(Request $r) {
        //pr($_POST);
        $input = $r->all();
        $rule = array(
            'order_no' => 'required',
            'order_date' => 'required',
            'from_subhead_id' => 'required',
            'from_particular_id' => 'required',
            'to_subhead_id' => 'required',
            'to_particular_id' => 'required'
        );
        $messages = array(
            'order_no.required' => 'Order number required',
            'order_date.required' => 'Order date required',
            'from_subhead_id.required' => 'Depo Account required',
            'from_particular_id.required' => 'Depo Name required',
            'to_subhead_id.required' => 'Dealer Account required',
            'to_particular_id.required' => 'Dealer Name required'
        );

        $valid = Validator::make($input, $rule, $messages);
        if ($valid->fails()) {
            return redirect()->back()->withErrors($valid)->withInput();
        }

        DB::beginTransaction();
        try {
            $model = new Sale();
            $orderObj = $model->where([['order_no', $r->order_no], ['is_deleted', 0]])->first();
            if (!empty($orderObj)) {
                throw new Exception("Order number {$r->order_no} already taken.");
            }
            if (empty($r->from_subhead_id)) {
                throw new Exception("Please Select Depo Account.");
            }
            if (empty($r->from_particular_id)) {
                throw new Exception("Please Select Depo Name.");
            }
            if (empty($r->to_subhead_id)) {
                throw new Exception("Please Select Dealer Account.");
            }
            if (empty($r->to_particular_id)) {
                throw new Exception("Please Select Dealer Name.");
            }

            $model->order_date = !empty($r->order_date) ? date_ymd($r->order_date) : date('Y-m-d');
            $model->month = date_mm($model->order_date);
            $model->year = date_yy($model->order_date);
            $model->order_no = $r->order_no;
            $model->from_head_id = SubHead::find($r->from_subhead_id)->head_id;
            $model->from_subhead_id = $r->from_subhead_id;
            $model->from_particular_id = $r->from_particular_id;
            $model->to_head_id = SubHead::find($r->to_subhead_id)->head_id;
            $model->to_subhead_id = $r->to_subhead_id;
            $model->to_particular_id = !empty($r->to_particular_id) ? $r->to_particular_id : NULL;
            $model->note = $r->note;
            $model->previous_amount = Transaction::model()->particularBalance($r->to_particular_id);
            $model->created_at = dbTimestamp();
            $model->created_by = Auth::id();
            $model->_key = uniqueKey();
            if (!$model->save()) {
                throw new Exception("Error! while saving order record.");
            }

            DB::commit();
            return redirect("/depo/sale/itemlist/{$model->_key}")->with('success', 'Ordered saved successfully.');
        } catch (Exception $e) {
            DB::rollback();
            return redirect()->back()->with('danger', $e->getMessage());
        }
    }

    public function edit($id) {
        check_user_access('depo_sales_edit');
        $model = Sale::find($id);
        //$_depoList = Particular::model()->getListByHeadId(HEAD_DEALER_ACCOUNT);
        $_depoList = SubHead::model()->getList(HEAD_DEPO_ACCOUNT);
        $_depoNames = Particular::model()->getList($model->from_subhead_id);
        $subheads = SubHead::model()->getList(HEAD_DEALER_ACCOUNT);
        $particulars = Particular::model()->getList($model->to_subhead_id);

        $this->model['page_title'] = "Depo Sale Information";
        $this->model['model'] = $model;
        $this->model['depoList'] = $_depoList;
        $this->model['depoNames'] = $_depoNames;
        $this->model['subheads'] = $subheads;
        $this->model['particulars'] = $particulars;
        return view('depo.sale.edit', $this->model);
    }

    public function update(Request $r, $id) {
        //pr($_POST);
        $input = $r->all();
        $rule = array(
            'order_no' => 'required',
            'order_date' => 'required',
            'from_subhead_id' => 'required',
            'from_particular_id' => 'required',
            'to_subhead_id' => 'required',
            'to_particular_id' => 'required'
        );
        $messages = array(
            'order_no.required' => 'Order number required',
            'order_date.required' => 'Order date required',
            'from_subhead_id.required' => 'Depo Account required',
            'from_particular_id.required' => 'Depo Name required',
            'to_subhead_id.required' => 'Dealer Account required',
            'to_particular_id.required' => 'Dealer Name required'
        );

        $valid = Validator::make($input, $rule, $messages);
        if ($valid->fails()) {
            return redirect()->back()->withErrors($valid)->withInput();
        }

        DB::beginTransaction();
        try {
            $model = Sale::find($id);
            $model->order_date = !empty($r->order_date) ? date_ymd($r->order_date) : date('Y-m-d');
            $model->month = date_mm($model->order_date);
            $model->year = date_yy($model->order_date);
            $model->order_no = $r->order_no;
            $model->from_head_id = SubHead::find($r->from_subhead_id)->head_id;
            $model->from_subhead_id = $r->from_subhead_id;
            $model->from_particular_id = $r->from_particular_id;
            $model->to_head_id = SubHead::find($r->to_subhead_id)->head_id;
            $model->to_subhead_id = $r->to_subhead_id;
            $model->to_particular_id = !empty($r->to_particular_id) ? $r->to_particular_id : NULL;
            $model->note = $r->note;
            $model->previous_amount = Transaction::model()->particularBalance($r->to_particular_id);
            $model->modified_at = dbTimestamp();
            $model->modified_by = Auth::id();
            if (!$model->save()) {
                throw new Exception("Error! while updating record.");
            }

            if (isset($r->item_id) && count($r->item_id) > 0) {
                foreach ($r->item_id as $key => $value) {
                    $modelItem = SaleItem::find($value);
                    $modelItem->order_date = $model->order_date;
                    $modelItem->month = $model->month;
                    $modelItem->year = $model->year;
                    $modelItem->barcode = $r->imei[$value];
                    $modelItem->quantity = $r->quantity[$value];
                    $modelItem->rate = $r->unitPrice[$value];
                    $modelItem->amount = $r->totalPrice[$value];
                    $modelItem->purchase_rate = PriceSettingPurchase::model()->getLastSettingPrice($modelItem->product_id);
                    $modelItem->purchase_amount = ($modelItem->quantity * $modelItem->purchase_rate);
                    $modelItem->profit = ($modelItem->amount - $modelItem->purchase_amount);
                    $modelItem->avg_price = NULL;
                    if (!$modelItem->save()) {
                        throw new Exception("Error! while updating items record.");
                    }
                }
            }

            DB::commit();
            return redirect("/depo/sale")->with('success', 'Record updated successfully.');
        } catch (Exception $e) {
            DB::rollback();
            return redirect()->back()->with('danger', $e->getMessage());
        }
    }

    public function show($id) {
        check_user_access('depo_sales_detail');
        $data = Sale::find($id);

        $this->model['page_title'] = "Depo Sale Details";
        $this->model['data'] = $data;
        return view('depo.sale.details', $this->model);
    }

    public function save_order(Request $r) {
        //pr($_POST);
        $input = $r->all();
        $rule = array(
            'invoice_amount' => 'required',
        );
        $messages = array(
            'invoice_amount.required' => 'Amount required',
        );

        $valid = Validator::make($input, $rule, $messages);
        if ($valid->fails()) {
            return redirect()->back()->withErrors($valid)->withInput();
        }

        DB::beginTransaction();
        try {
            $model = Sale::find($r->payment_order_id);
            $model->invoice_amount = $r->invoice_amount;
            $model->discount = ($r->discount_amount > 0) ? YES : NO;
            $model->discount_amount = !empty($r->discount_amount) ? $r->discount_amount : NULL;
            $model->net_amount = $r->net_amount;
            $model->previous_amount = $r->previous_amount;
            $model->receivable_amount = $r->receivable_amount;
            $model->paid_amount = $r->paid_amount;
            $model->due_amount = !empty($r->due_amount) ? $r->due_amount : NULL;
            $model->balance_amount = !empty($r->balance_amount) ? $r->balance_amount : NULL;
            $model->payment_status = ($model->paid_amount >= $model->net_amount) ? PAYMENT_PAID : PAYMENT_PAID_PARTIAL;
            if (!$model->save()) {
                throw new Exception("Error! while saving order record.");
            }

            DB::commit();
            return redirect("/depo/sale")->with('success', 'Record saved successfully.');
        } catch (Exception $e) {
            DB::rollback();
            return redirect()->back()->with('danger', $e->getMessage());
        }
    }

    public function itemlist($id) {
        check_user_access('depo_sales_items');
        $model = Sale::where('_key', $id)->first();
        $_itemDataset = SaleItem::where([['depo_sale_id', $model->id], ['is_deleted', 0]])->groupBy('product_id')->orderBy('id', 'DESC')->get();
        $_product_types = ProductType::model()->getList();
        $_categories = Category::model()->getList();
        $_subheads = SubHead::model()->getList();
        $_previous_amount = Transaction::model()->particularBalance($model->to_particular_id);

        $this->model['page_title'] = "Depo Sale Items";
        $this->model['data'] = $model;
        $this->model['itemDataset'] = $_itemDataset;
        $this->model['product_types'] = $_product_types;
        $this->model['categories'] = $_categories;
        $this->model['subheads'] = $_subheads;
        $this->model['previous_amount'] = $_previous_amount;
        return view('depo.sale.itemlist', $this->model);
    }

    public function item_detail($id, $productId) {
        check_user_access('depo_sales_items');
        $model = Sale::find($id);
        $_itemDataset = SaleItem::where([['depo_sale_id', $model->id], ['product_id', $productId], ['is_deleted', 0]])->orderBy('id', 'DESC')->get();

        $this->model['page_title'] = "Depo Sale Item Details";
        $this->model['data'] = $model;
        $this->model['itemDataset'] = $_itemDataset;
        return view('depo.sale.item_detail', $this->model);
    }

    public function items() {
        check_user_access('depo_sales_items');
        $_itemDataset = SaleItem::where([['is_deleted', 0]])->groupBy(['depo_sale_id', 'product_id'])->paginate($this->getSettings()->pagesize);
        $_product_types = ProductType::model()->getList();
        $_categories = Category::model()->getList();

        $this->model['page_title'] = "Depo Sale Items";
        $this->model['itemDataset'] = $_itemDataset;
        $this->model['product_types'] = $_product_types;
        $this->model['categories'] = $_categories;
        return view('depo.sale.items', $this->model);
    }

    public function confirm($id) {
        check_user_access('depo_sales_process');
        $data = Sale::find($id);

        DB::beginTransaction();
        try {
            if ($data->process_status == STATUS_PROCESED) {
                return redirect('/depo/sale')->with("info", "Order {$data->order_no} already processed.");
            }
            if (empty($data->net_amount) || $data->net_amount <= 0) {
                return redirect('/depo/sale')->with("warning", "Invoice amount should not be empty. Save order no {$data->order_no} properly.");
            }

            foreach ($data->items as $item) {
                $_stock = new Stock();
                $_stock->order_date = $data->order_date;
                $_stock->month = date_mm($data->order_date);
                $_stock->year = date_yy($data->order_date);
                $_stock->depo_id = $data->from_particular_id;
                $_stock->dealer_id = $data->to_particular_id;
                $_stock->head_id = $data->from_head_id;
                $_stock->subhead_id = $data->from_subhead_id;
                $_stock->particular_id = $data->from_particular_id;
                $_stock->depo_sale_id = $data->id;
                $_stock->depo_sale_item_id = $item->id;
                $_stock->product_type_id = $item->product_type_id;
                $_stock->category_id = $item->category_id;
                $_stock->product_id = $item->product_id;
                $_stock->product_description_id = $item->product_description_id;
                $_stock->barcode = $item->barcode;
                $_stock->stock_type = STOCK_TYPE_SALE;
                $_stock->type = STOCK_OUT;
                $_stock->quantity = $item->quantity;
                $_stock->rate = $item->rate;
                $_stock->price = $item->amount;
                $_stock->created_at = dbTimestamp();
                $_stock->created_by = Auth::id();
                $_stock->_key = uniqueKey() . $item->id;
                if (!$_stock->save()) {
                    throw new Exception("Error while processing stock record.");
                }
                $_stockId = DB::getPdo()->lastInsertId();

                $_depoStock = new DealerStock();
                $_depoStock->depo_stock_id = $_stockId;
                $_depoStock->order_date = $_stock->order_date;
                $_depoStock->month = date_mm($data->order_date);
                $_depoStock->year = date_yy($data->order_date);
                $_depoStock->dealer_id = $data->to_particular_id;
                $_depoStock->head_id = $data->to_head_id;
                $_depoStock->subhead_id = $data->to_subhead_id;
                $_depoStock->particular_id = $data->to_particular_id;
                $_depoStock->depo_sale_id = $_stock->depo_sale_id;
                $_depoStock->depo_sale_item_id = $_stock->depo_sale_item_id;
                $_depoStock->product_type_id = $_stock->product_type_id;
                $_depoStock->category_id = $_stock->category_id;
                $_depoStock->product_id = $_stock->product_id;
                $_depoStock->product_description_id = $_stock->product_description_id;
                $_depoStock->barcode = $_stock->barcode;
                $_depoStock->stock_type = STOCK_TYPE_PURCHASE;
                $_depoStock->type = STOCK_IN;
                $_depoStock->quantity = $_stock->quantity;
                $_depoStock->rate = $_stock->rate;
                $_depoStock->price = $_stock->price;
                $_depoStock->created_at = $_stock->created_at;
                $_depoStock->created_by = $_stock->created_by;
                $_depoStock->_key = $_stock->_key;
                if (!$_depoStock->save()) {
                    throw new Exception("Error while processing sale person stock record.");
                }

                /* $this->dataArr['stock_id'] = $_stockId;
                  $this->dataArr['stock_type'] = $_stock->stock_type;
                  $this->dataArr['order_date'] = $_stock->order_date;
                  $this->dataArr['month'] = $_stock->month;
                  $this->dataArr['year'] = $_stock->year;
                  $this->dataArr['product_type_id'] = $_stock->product_type_id;
                  $this->dataArr['category_id'] = $_stock->category_id;
                  $this->dataArr['product_id'] = $_stock->product_id;
                  $this->dataArr['total_quantity'] = $_stock->quantity;
                  $this->dataArr['total_price'] = $item->purchase_amount;
                  $this->dataArr['avg_price'] = $item->purchase_rate;
                  AvgPrice::model()->saveData($this->dataArr); */

                $item->process_status = STATUS_PROCESED;
                $item->save();
            }

            $_transaction = new Transaction();
            $_transaction->depo_sale_id = $data->id;
            $_transaction->type = 'C';
            $_transaction->voucher_type = SALES_VOUCHER;
            $_transaction->payment_method = PAYMENT_NO;
            $_transaction->from_head_id = $data->from_head_id;
            $_transaction->from_subhead_id = $data->from_subhead_id;
            $_transaction->from_particular_id = $data->from_particular_id;
            $_transaction->to_head_id = $data->to_head_id;
            $_transaction->to_subhead_id = $data->to_subhead_id;
            $_transaction->to_particular_id = $data->to_particular_id;
            $_transaction->date = $data->order_date;
            $_transaction->description = $data->note;
            $_transaction->by_whom = Auth::user()->name;
            $_transaction->debit = $data->net_amount;
            $_transaction->credit = $data->net_amount;
            $_transaction->amount = $data->net_amount;
            $_transaction->created_at = dbTimestamp();
            $_transaction->created_by = Auth::id();
            $_transaction->_key = $data->_key;
            if (!$_transaction->save()) {
                throw new Exception("Error while processing order transaction.");
            }

            if ($data->paid_amount > 0) {
                $_modelTrans = new Transaction();
                $_modelTrans->depo_sale_id = $data->id;
                $_modelTrans->type = 'D';
                $_modelTrans->voucher_type = RECEIVE_VOUCHER;
                $_modelTrans->payment_method = PAYMENT_CASH;
                $_modelTrans->from_head_id = $data->to_head_id;
                $_modelTrans->from_subhead_id = $data->to_subhead_id;
                $_modelTrans->from_particular_id = $data->to_particular_id;
                $_modelTrans->to_head_id = HEAD_CASH_IN_HAND;
                $_modelTrans->to_subhead_id = SUBHEAD_CASH;
                $_modelTrans->to_particular_id = NULL;
                $_modelTrans->date = $data->order_date;
                $_modelTrans->description = $data->note;
                $_modelTrans->by_whom = Auth::user()->name;
                $_modelTrans->debit = $data->paid_amount;
                $_modelTrans->credit = $data->paid_amount;
                $_modelTrans->amount = $data->paid_amount;
                $_modelTrans->created_at = dbTimestamp();
                $_modelTrans->created_by = Auth::id();
                $_modelTrans->_key = $data->_key;
                if (!$_modelTrans->save()) {
                    throw new Exception("Error while processing order receive.");
                }
            }

            $data->process_status = STATUS_PROCESED;
            if (!$data->save()) {
                throw new Exception("Error while processing record.");
            }

            DB::commit();
            return redirect('/depo/sale')->with('success', 'Order processed successfully.');
        } catch (Exception $e) {
            DB::rollback();
            return redirect('sale')->with('danger', $e->getMessage());
        }
    }

    public function ledger() {
        check_user_access('depo_sales_ledger');
        $_product_types = ProductType::model()->getList();
        $_categories = Category::model()->getList();
        $dataset = SaleItem::where([['process_status', STATUS_PROCESED], ['is_deleted', 0]])->paginate($this->getSettings()->pagesize);

        $this->model['page_title'] = "Depo Sale Ledger";
        $this->model['product_types'] = $_product_types;
        $this->model['categories'] = $_categories;
        $this->model['dataset'] = $dataset;
        return view('depo.sale.ledger', $this->model);
    }

    // Ajax Functions
    public function search(Request $r) {
        $item_count = !empty($r->item_count) ? $r->item_count : $this->getSettings()->pagesize;
        $_search_order = $r->search_order;
        $_search_supplier = $r->search_supplier;
        $_status = $r->status;
        $from_date = $r->from_date;
        $end_date = $r->end_date;
        $partyId = [];

        $query = Sale::where('is_deleted', 0);
        if (!empty($_search_order)) {
            $query->where('order_no', $_search_order);
        }
        if (!empty($_search_supplier)) {
            $_partyList = Particular::where('name', 'like', '%' . trim($_search_supplier) . '%')
                    ->orWhere('address', 'like', '%' . trim($_search_supplier) . '%')
                    ->orWhere('mobile', 'like', '%' . to_eng($_search_supplier) . '%')
                    ->get();
            foreach ($_partyList as $_party) {
                $partyId[] = $_party->id;
            }
            $query->whereIn('to_particular_id', $partyId);
        }
        if (!empty($_status)) {
            $query->where('process_status', $_status);
        }
        if (!empty($from_date) && !empty($end_date)) {
            $query->whereBetween('order_date', [date_ymd($from_date), date_ymd($end_date)]);
        }
        $query->orderBy('order_date', 'DESC');
        $dataset = $query->paginate($item_count);

        $modelItem = new SaleItem();
        $this->model['dataset'] = $dataset;
        $this->model['modelItem'] = $modelItem;
        return view('depo.sale._list', $this->model);
    }

    public function search_items(Request $r) {
        $item_count = !empty($r->item_count) ? $r->item_count : $this->getSettings()->pagesize;
        $product_type_id = $r->product_type_id;
        $category_id = $r->category_id;
        $search_product = $r->search_product;
        $search_supplier = $r->search_supplier;
        $from_date = $r->from_date;
        $end_date = $r->end_date;
        $productId = [];
        $particularId = [];
        $idArr = [];

        $query = SaleItem::where('is_deleted', 0);
        if (!empty($product_type_id)) {
            $query->where('product_type_id', $product_type_id);
            $this->searchValues['Type'] = ProductType::find($product_type_id)->name;
        }
        if (!empty($category_id)) {
            $query->where('category_id', $category_id);
            $this->searchValues['Category'] = Category::find($category_id)->name;
        }
        if (!empty($search_product)) {
            $_proList = Product::where('name', 'like', '%' . $search_product . '%')->get();
            foreach ($_proList as $_prod) {
                $productId[] = $_prod->id;
            }
            $query->whereIn('product_id', $productId);
            $this->searchValues['Product'] = $search_product;
        }
        if (!empty($search_supplier)) {
            $_partyList = Particular::where('name', 'like', '%' . trim($search_supplier) . '%')
                    ->orWhere('address', 'like', '%' . trim($search_supplier) . '%')
                    ->orWhere('mobile', 'like', '%' . to_eng($search_supplier) . '%')
                    ->get();
            foreach ($_partyList as $_party) {
                $particularId[] = $_party->id;
            }

            $_dataList = Sale::where('is_deleted', 0)->whereIn('to_particular_id', $particularId)->get();
            foreach ($_dataList as $_data) {
                $idArr[] = $_data->id;
            }
            $query->whereIn('sale_id', $idArr);
            $this->searchValues['Customer'] = $search_supplier;
        }
        if (!empty($from_date) && !empty($end_date)) {
            $query->whereBetween('order_date', [date_ymd($from_date), date_ymd($end_date)]);
            $this->searchValues['Dates'] = "{$from_date} | {$end_date}";
        }
        $query->groupBy(['depo_sale_id', 'product_id']);
        $_itemDataset = $query->paginate($item_count);

        $this->model['itemDataset'] = $_itemDataset;
        $this->model['searchValues'] = $this->searchValues;
        return view('depo.sale._items', $this->model);
    }

    public function ledger_search(Request $r) {
        $item_count = !empty($r->item_count) ? $r->item_count : $this->getSettings()->pagesize;
        $product_type_id = $r->product_type_id;
        $category_id = $r->category_id;
        $product_id = $r->product_id;
        $search_info = $r->search_info;
        $search_barcode = $r->search_barcode;
        $from_date = $r->from_date;
        $end_date = $r->end_date;
        $particularId = [];
        $idArr = [];

        $query = SaleItem::where([['process_status', STATUS_PROCESED], ['is_deleted', 0]]);
        if (!empty($product_type_id)) {
            $query->where('product_type_id', $product_type_id);
            $this->searchValues['Brand'] = ProductType::find($product_type_id)->name;
        }
        if (!empty($category_id)) {
            $query->where('category_id', $category_id);
            $this->searchValues['Category'] = Category::find($category_id)->name;
        }
        if (!empty($product_id)) {
            $query->where('product_id', $product_id);
            $this->searchValues['Product'] = Product::find($product_id)->name;
        }
        if (!empty($search_info)) {
            $_partyList = Particular::where('name', 'like', '%' . trim($search_info) . '%')
                    ->orWhere('address', 'like', '%' . trim($search_info) . '%')
                    ->orWhere('mobile', 'like', '%' . to_eng($search_info) . '%')
                    ->get();
            foreach ($_partyList as $_party) {
                $particularId[] = $_party->id;
            }

            $_dataList = Sale::where('is_deleted', 0)->whereIn('to_particular_id', $particularId)->get();
            foreach ($_dataList as $_data) {
                $idArr[] = $_data->id;
            }
            $query->whereIn('sale_id', $idArr);
            $this->searchValues['Customer'] = $search_info;
        }
        if (!empty($search_barcode)) {
            $query->where('barcode', 'like', '%' . $search_barcode . '%');
            $this->searchValues['Barcode'] = $search_barcode;
        }
        if (!empty($from_date) && !empty($end_date)) {
            $query->whereBetween('order_date', [date_ymd($from_date), date_ymd($end_date)]);
            $this->searchValues['Dates'] = "{$from_date} | {$end_date}";
        }
        $dataset = $query->paginate($item_count);

        $this->model['dataset'] = $dataset;
        $this->model['searchValues'] = $this->searchValues;
        return view('depo.sale._ledger', $this->model);
    }

    public function delete() {
        DB::beginTransaction();
        try {
            foreach ($_POST['data'] as $id) {
                $data = Sale::find($id);

                if (!empty($data->stocks) && count($data->stocks) > 0) {
                    foreach ($data->stocks as $stock) {
                        if (!empty($stock->avg_price)) {
                            $stock->avg_price->delete();
                        }
                        if (!$stock->delete()) {
                            throw new Exception("Error while deleting stock records.");
                        }
                    }
                }

                if (!empty($data->dealer_stocks) && count($data->dealer_stocks) > 0) {
                    foreach ($data->dealer_stocks as $depo_stock) {
                        if (!$depo_stock->delete()) {
                            throw new Exception("Error while reseting dealer stock records.");
                        }
                    }
                }

                if (!empty($data->transDataset) && count($data->transDataset) > 0) {
                    foreach ($data->transDataset as $_transData) {
                        if (!$_transData->delete()) {
                            throw new Exception("Error while deleting transaction record.");
                        }
                    }
                }

                if (!empty($data->items) && count($data->items) > 0) {
                    foreach ($data->items as $item) {
                        $item->process_status = STATUS_PENDING;
                        $item->is_deleted = 1;
                        if (!$item->save()) {
                            throw new Exception("Error while deleting item records.");
                        }
                    }
                }

                $data->process_status = STATUS_PENDING;
                $data->is_deleted = 1;
                $data->deleted_at = dbTimestamp();
                $data->deleted_by = Auth::id();
                if (!$data->save()) {
                    throw new Exception("Error while deleting records.");
                }
            }

            DB::commit();
            $this->response['success'] = true;
            $this->response['message'] = 'Record deleted successfully.';
        } catch (Exception $e) {
            DB::rollback();
            $this->response['success'] = false;
            $this->response['message'] = $e->getMessage();
        }
        return $this->response;
    }

    public function delete_item(Request $r) {
        $_itemID = $_POST['itemid'];

        DB::beginTransaction();
        try {
            $data = SaleItem::find($_itemID);
            if (!$data->delete()) {
                throw new Exception("Error while deleting record.");
            }

            DB::commit();
            $this->response['success'] = true;
            $this->response['message'] = 'Record delete successfull.';
        } catch (Exception $e) {
            DB::rollback();
            $this->response['success'] = false;
            $this->response['message'] = $e->getMessage();
        }

        return $this->response;
    }

    public function remove_item($id) {
        DB::beginTransaction();
        try {
            $data = SaleItem::find($id);
            if (!$data->delete()) {
                throw new Exception("Error while removing record.");
            }

            DB::commit();
            return redirect()->back()->with('success', 'Item removed successfully.');
        } catch (Exception $e) {
            DB::rollback();
            return redirect()->back()->with('danger', $e->getMessage());
        }
    }

    public function reset() {
        DB::beginTransaction();
        try {
            foreach ($_POST['data'] as $id) {
                $data = Sale::find($id);

                if (!empty($data->stocks) && count($data->stocks) > 0) {
                    foreach ($data->stocks as $stock) {
                        if (!empty($stock->avg_price)) {
                            $stock->avg_price->delete();
                        }
                        if (!$stock->delete()) {
                            throw new Exception("Error while reseting stock records.");
                        }
                    }
                }

                if (!empty($data->dealer_stocks) && count($data->dealer_stocks) > 0) {
                    foreach ($data->dealer_stocks as $depo_stock) {
                        if (!$depo_stock->delete()) {
                            throw new Exception("Error while reseting depo stock records.");
                        }
                    }
                }

                if (!empty($data->transDataset) && count($data->transDataset) > 0) {
                    foreach ($data->transDataset as $_transData) {
                        if (!$_transData->delete()) {
                            throw new Exception("Error while reseting transaction record.");
                        }
                    }
                }

                if (!empty($data->items) && count($data->items) > 0) {
                    foreach ($data->items as $item) {
                        $item->process_status = STATUS_PENDING;
                        if (!$item->save()) {
                            throw new Exception("Error while reseting item records.");
                        }
                    }
                }

                $data->process_status = STATUS_PENDING;
                if (!$data->save()) {
                    throw new Exception("Error while reseting records.");
                }
            }

            DB::commit();
            $this->response['success'] = true;
            $this->response['message'] = 'Record reset successfull.';
        } catch (Exception $e) {
            DB::rollback();
            $this->response['success'] = false;
            $this->response['message'] = $e->getMessage();
        }
        return $this->response;
    }

    public function search_product(Request $r) {
        $brand = $r->brand;
        $category = $r->category;
        $product_name = $r->product_name;

        $query = Product::where('is_deleted', 0);
        if (!empty($brand)) {
            $query->where('company', $brand);
        }
        if (!empty($category)) {
            $query->where('category_id', $category);
        }
        if (!empty($product_name)) {
            $query->where('name', 'like', '%' . $product_name . '%');
        }
        $dataset = $query->get();
        $priceSetting = new PriceSettingPurchase();

        $this->model['dataset'] = $dataset;
        $this->model['priceSetting'] = $priceSetting;
        return view('depo.sale._productlist', $this->model);
    }

    public function check_imei_sold(Request $r) {
        $data = SaleItem::where('barcode', $r->imei)->first();
        if (!empty($data)) {
            $this->response['success'] = true;
        } else {
            $this->response['success'] = false;
        }
        return $this->response;
    }

    public function add_item(Request $r) {
        //pr($_POST);
        $modelItem = new SaleItem();
        $_product = Product::find($r->product_id);
        $_orderObj = Sale::find($r->order_id);

        DB::beginTransaction();
        try {
            if (!empty($r->barcode)) {
                $_itemRecord = Stock::model()->get_barcode_entry_info($r->barcode);
                if (!empty($_itemRecord)) {
                    if ($r->product_id != $_itemRecord->product_id) {
                        throw new Exception("This barcode <b>{$r->barcode}</b> belongs to <b>{$_itemRecord->category->name}->{$_itemRecord->brand->name}->{$_itemRecord->product->name}</b>.");
                    }
                } else {
                    throw new Exception("Sorry! barcode <b>{$r->barcode}</b> has no stock record.");
                }
                $_dsrSaleItem = SrSaleItem::where([['barcode', $r->barcode], ['is_deleted', 0]])->first();
                if (!empty($_dsrSaleItem)) {
                    throw new Exception("Sorry! barcode <b>{$r->barcode}</b> has already been sold to <b>{$_dsrSaleItem->particular->name}</b>.");
                }
                $_saleItem = SaleItem::where([['barcode', $r->barcode], ['is_deleted', 0]])->first();
                if (!empty($_saleItem)) {
                    $_saleReturnItem = SaleReturnItem::where([['barcode', $r->barcode], ['is_deleted', 0]])->first();
                    if (empty($_saleReturnItem)) {
                        throw new Exception("Sorry! barcode <b>{$r->barcode}</b> is already in items list.");
                    }
                }
            }

            $modelItem->depo_sale_id = $r->order_id;
            $modelItem->product_type_id = !empty($r->product_type_id) ? $r->product_type_id : $_product->product_type_id;
            $modelItem->category_id = !empty($r->category_id) ? $r->category_id : $_product->category_id;
            $modelItem->product_id = $r->product_id;
            $modelItem->product_description_id = !empty($r->product_description_id) ? $r->product_description_id : NULL;
            $modelItem->order_date = $_orderObj->order_date;
            $modelItem->month = $_orderObj->month;
            $modelItem->year = $_orderObj->year;
            $modelItem->barcode = (read_barcode($r->category_id) == CONST_YES) ? $r->barcode : NULL;
            $modelItem->quantity = (read_barcode($r->category_id) == CONST_YES) ? 1 : $r->quantity;
            $modelItem->rate = $r->rate;
            $modelItem->amount = (read_barcode($r->category_id) == CONST_YES) ? $r->rate : $r->price;
            $modelItem->purchase_rate = AvgPrice::model()->product_last_avg($modelItem->product_id);
            $modelItem->purchase_amount = ($modelItem->quantity * $modelItem->purchase_rate);
            $modelItem->profit = ($modelItem->amount - $modelItem->purchase_amount);
            if (!$modelItem->save()) {
                throw new Exception("Error! while saving order items record.");
            }

            DB::commit();
            $this->response['success'] = true;
            $this->response['message'] = 'Item saved successfully.';
        } catch (Exception $e) {
            DB::rollback();
            $this->response['success'] = false;
            $this->response['message'] = $e->getMessage();
        }
        return $this->response;
    }

    public function search_itemlist(Request $r) {
        $_itemDataset = SaleItem::where([['depo_sale_id', $r->orderId], ['is_deleted', 0]])->groupBy('product_id')->orderBy('id', 'DESC')->get();
        $this->model['itemDataset'] = $_itemDataset;
        return view('depo.sale._itemlist_partial', $this->model);
    }

}
