<?php

namespace App\Http\Controllers\DailyWork;

use Auth;
use DB;
use Exception;
use Validator;
Use App\Http\Controllers\HomeController;
use App\Models\EssentialSetting\AgentParty;
use App\Models\EssentialSetting\BagType;
use App\Models\Setting\ProductModule\Category;
use App\Models\DailyWork\ProductInCreate;
use Illuminate\Http\Request;

class ProductInCreateController extends HomeController {

    public function index() {

        check_user_access('product_in_create');
        $dataset = ProductInCreate::where('is_deleted', 0)->get();
        $this->model['page_title'] = "Product in create Information";
        $this->model['dataset'] = $dataset;
        //pr($dataset);
        return view('product_in_create.index', $this->model);
    }

    public function create() {
        check_user_access('product_in_create_create');

        $_agent=AgentParty::where('is_deleted',0)->get();
        $_category=Category::where('is_deleted',0)->get();
        $_bag_type=BagType::where('is_deleted',0)->get();

        $this->model['agent']=$_agent;   
        $this->model['category']=$_category;   
        $this->model['bag_type']=$_bag_type;   

        $this->model['page_title'] = "Product in create create";
        return view('product_in_create.create', $this->model);
    }

    public function store(Request $r) {
        // pr($_POST);
        $input = $r->all();
        $rules = array(
            'sr_no' => 'required',
        );
        $messages = array(
            'sr_no.required' => 'SR no  required.',
        );
        $valid = Validator::make($input, $rules, $messages);
        if ($valid->fails()) {
            return redirect()->back()->withErrors($valid)->withInput();
        }
        DB::beginTransaction();
        try {
            $model = new ProductInCreate();
            $model->date = $r->date;
            $model->sr_number = $r->sr_no;
            $model->booking_no = $r->booking_no;
            $model->agent_code = $r->agent;
            $model->customer_name = $r->name;
            $model->father_name = $r->fname;
            $model->address = $r->address;
            $model->submited_bag_quantity = $r->bag_quantity;
            $model->empty_bag = $r->empty_bag;
            $model->carrying = $r->carrying;
            $model->net_weight = $r->net_weight;
            $model->mobile = $r->mobile;
            $model->remark = $r->remarks;
           
            $model->lot_number = $r->lot_number;
            $model->category_id = $r->category;
            $model->bagtype_id = $r->bag_type;
            $model->bag_rent = $r->bag_rent;
            $model->total_rent = $r->total_rent;
            $model->advance_rent_receive = $r->advance_rent_receive;
            $model->advance_rent_adjust = $r->advance_rent_adjust;
            $model->remain_rent = $r->remain_rent;
            $model->created_at = cur_date_time();
            $model->created_by = Auth::id();
            $model->_key = uniqueKey();
            if (!$model->save()) {
                throw new Exception("Error while saving record.");
            }
            DB::commit();
            return redirect('product_in_create')->with('success', 'Record saved successfully.');
        } catch (Exception $e) {
            DB::rollback();
            return redirect()->back()->with('danger', $e->getMessage());
        }
    }

    public function edit($id) {
        check_user_access('product_in_create_edit');
        $data = ProductInCreate::where('_key', $id)->first();
        //  pr($data->toArray());  
        $this->model['page_title'] = "product_in_create edit";
        $this->model['data'] = $data;
        return view('product_in_create.edit', $this->model);
    }

    public function update(Request $r, $id) {
        $input = $r->all();
        $rules = array(
            'name' => 'required',
        );
        $messages = array(
            'name.required' => 'Name required.',
        );

        $valid = Validator::make($input, $rules, $messages);
        if ($valid->fails()) {
            return redirect()->back()->withErrors($valid)->withInput();
        }
        try {
            $model = ProductInCreate::find($id);
            $model->session = $r->session;
            $model->name = $r->name;
            $model->per_bag_rent = $r->per_bag_rent;
            $model->per_kg_rent = $r->per_kg_rent;
            $model->agent_bag_rent = $r->agent_bag_rent;
            $model->agent_kg_rent = $r->agent_kg_rent;
            $model->party_bag_rent = $r->party_bag_rent;
            $model->party_kg_rent = $r->party_kg_rent;
            $model->per_bag_loan = $r->per_bag_loan;
            $model->empty_bag_rent = $r->empty_bag_rate;
            $model->fan_charge = $r->fan_charge;
            $model->modified_at = cur_date_time();
            $model->modified_by = Auth::id();
            if (!$model->save()) {
                throw new Exception("Error while updating record.");
            }

            DB::commit();
            return redirect(' product_in_create')->with('success', 'Record updated successfully.');
        } catch (Exception $e) {
            DB::rollback();
            return redirect()->back()->with('danger', $e->getMessage());
        }
    }






    // Ajax Functions

     public function AgentsInfo( Request $r )
    {
         $dataset = AgentParty::where([['is_deleted', 0], ['id', $r->AgentId]])->first();
    
      

        return $dataset;
    }






    public function search(Request $r) {
        $item_count = !empty($r->item_count) ? $r->item_count : $this->getSettings()->pagesize;
        $srch = $r->srch;
        $query = ProductInCreate::where('is_deleted', 0);
    
        if (!empty($srch)) {
            $query->where('name', 'like', '%' . $srch . '%');
        }
        $query->orderBy('name', 'asc');
        $dataset = $query->paginate($item_count);

        $this->model['dataset'] = $dataset;
        return view('product_in_create._list', $this->model);
    }

    public function delete() {
        $resp = array();
        try {
            foreach ($_POST['data'] as $id) {
                $data = ProductInCreate::find($id);
                $data->is_deleted = 1;
                $data->deleted_by = Auth::id();
                $data->deleted_at = cur_date_time();
                if (!$data->save()) {
                    throw new Exception("Error while deleting records.");
                }
            }
            DB::commit();
            $resp['success'] = true;
            $resp['message'] = 'Record has been deleted successfully.';
        } catch (Exception $e) {
            DB::rollback();
            $resp['success'] = false;
            $resp['message'] = $e->getMessage();
        }
        return $resp;
    }


}
