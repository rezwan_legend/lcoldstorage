<?php

namespace App\Http\Controllers;

use Auth;
use DB;
use Illuminate\Http\Request;
use App\Models\GeneralSetting;
use Illuminate\Support\Facades\Input;

class GeneralSettingController extends HomeController {

    public function index($id = null) {
        $setting = GeneralSetting::where('institute_id', $id)->first();
        return view('institute.settings.setting', compact('setting'));
    }

    public function update_setting(Request $r, $id) {
        //pr($_POST);
        DB::beginTransaction();
        try {
            $model = GeneralSetting::find($id);
            $model->title = $r->site_name;
            $model->owner = $r->author_name;
            $model->address = $r->author_address;
            $model->description = $r->site_description;
            $model->email = $r->author_email;
            $model->mobile = $r->author_mobile;
            $model->phone = $r->author_phone;
            $model->copyright = $r->copyright;
            $model->pagesize = $r->pagesize;
            $model->other_contact = $r->other_contacts;
            $model->modified_by = Auth::user()->id;
            if (Input::hasFile('logo')) {
                $logo = Input::file('logo');
                $logo_name = 'logo' . '.' . $logo->getClientOriginalExtension();
                $path = public_path('uploads/');
                $model->logo = $logo_name;
                Input::file('logo')->move($path, $logo_name);
            }
            if (Input::hasFile('favicon')) {
                $fav = Input::file('favicon');
                $fav_name = 'fav' . '.' . $fav->getClientOriginalExtension();
                $path = public_path('uploads/');
                $model->favicon = $fav_name;
                Input::file('favicon')->move($path, $fav_name);
            }
            if (!$model->save()) {
                throw new Exception("Error! while updating record.");
            }

            DB::commit();
            return redirect()->back()->with('success', 'Record Updated Successfully.');
        } catch (Exception $e) {
            DB::rollback();
            return redirect()->back()->with('danger', $e->getMessage());
        }
    }

}
