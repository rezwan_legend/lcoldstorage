<?php

namespace App\Http\Controllers;

use App\Http\Controllers\HomeController;
use App\Models\AdvanceLoanPayment;
use App\Models\AgentParty;
use App\Models\BagType;
use App\Models\Category;
use App\Models\Party;
use App\Models\PartyCommission;
use App\Models\PartyType;
use Exception;
use Auth;
use App\Models\Product;
use App\Models\RentType;
use App\Models\Unit;
use App\Models\UnitSize;
use DB;
use Validator;
use Illuminate\Http\Request;

class AdvanceLoanPaymentController extends HomeController {

    public function index() {
        check_user_access('advance_loan_payment_list');
        $model = new AdvanceLoanPayment();
        $query = $model->where('is_deleted', 0);
        $dataset = $query->paginate($this->getSettings()->pagesize);
        $_agents = AgentParty::where('is_deleted', 0)->get();
        $_bags = BagType::where('is_deleted', 0)->get();

        $this->model['page_title'] = "Advance Loan Information(Payment)";
        $this->model['dataset'] = $dataset;
        $this->model['agents'] = $_agents;
        $this->model['bags'] = $_bags;
        return view('advance_loan_payment.create', $this->model);
    }

    public function agentInformation(Request $r){
        $dataset = AgentParty::where([['is_deleted', 0], ['id' ,$r->agent_id]])->get();
        return $dataset;
    }
    public function getBagTypeInformation(Request $r){
        $dataset = BagType::where([['is_deleted', 0], ['id' ,$r->bagId]])->get();
        return $dataset;
    }

    public function create() {
        check_user_access('party_commission_create');
        $businessList = $this->userInstitutes();
        $_party_type = PartyType::where('is_deleted', 0)->get();
        $_categories = Category::where('is_deleted', 0)->get();
        $_products = Product::where('is_deleted', 0)->get();
        $_unit = Unit::where('is_deleted', 0)->get();
        
        $this->model['breadcrumb_title'] = "Party Commission Information";
        $this->model['businessList'] = $businessList;
        $this->model['party_type'] = $_party_type;
        $this->model['categories'] = $_categories;
        $this->model['products'] = $_products;
        $this->model['units'] = $_unit;
        return view('party_commission.create', $this->model);
    }
    
    public function store(Request $r) {
//        pr($_POST);
        $input = $r->all();
        $rules = array(
            'commission' => 'required'
        );
        $messages = array(
            'commission.required' => 'Commission Required.'
        );

        $valid = Validator::make($input, $rules, $messages);
        if ($valid->fails()) {
            return redirect()->back()->withErrors($valid)->withInput();
        }

        DB::beginTransaction();
        try {
            $model = new PartyCommission();
            $model->party_type_id = $r->party_type_id;
            $model->party_id = $r->party_id;
            $model->category_id = $r->category_id;
            $model->product_id = $r->product_id;
            $model->unit_id = $r->unit_id;
            $model->unit_size = $r->unit_size;
            $model->commission = $r->commission;
            $model->created_at = cur_date_time();
            $model->created_by = Auth::id();
            $model->_key = uniqueKey();
            if (!$model->save()) {
                throw new Exception("Query problem on creating record.");
            }

            DB::commit();
            return redirect('/party_commission')->with('success', 'New record created successfully.');
        } catch (Exception $e) {
            DB::rollback();
            return redirect()->back()->with('danger', $e->getMessage());
        }
    }

    public function edit($id) {
        check_user_access('party_commission_edit');
        $data = PartyCommission::find($id);
        $businessList = $this->userInstitutes();
        $_party_type = PartyType::where('is_deleted', 0)->get();
        $_party = Party::where('is_deleted', 0)->get();
        $_categories = Category::where('is_deleted', 0)->get();
        $_products = Product::where('is_deleted', 0)->get();
        $_units = Unit::where('is_deleted', 0)->get();
        $_unit_size = UnitSize::where('is_deleted', 0)->get();

        $this->model['party_type'] = $_party_type;
        $this->model['party'] = $_party;
        $this->model['categories'] = $_categories;
        $this->model['products'] = $_products;
        $this->model['products'] = $_products;
        $this->model['units'] = $_units;
        $this->model['unitsize'] = $_unit_size;
        $this->model['page_title'] = "Party Commission Information";
        $this->model['data'] = $data;
        return view('party_commission.edit', $this->model);
    }

    public function update(Request $r, $id) {
    //    pr($_POST);
        $input = $r->all();
        $rules = array(
            'commission' => 'required'
        );
        $messages = array(
            'commission.required' => 'Commission Required.'
        );

        $valid = Validator::make($input, $rules, $messages);
        if ($valid->fails()) {
            return redirect()->back()->withErrors($valid)->withInput();
        }

        DB::beginTransaction();
        try {
            $model = PartyCommission::find($id);
            $model->party_type_id = $r->party_type_id;
            $model->party_id = $r->party_id;
            $model->category_id = $r->category_id;
            $model->product_id = $r->product_id;
            $model->unit_id = $r->unit_id;
            $model->unit_size = $r->unit_size;
            $model->commission = $r->commission;
            $model->modified_at = cur_date_time();
            $model->modified_by = Auth::id();
            if (!$model->save()) {
                throw new Exception("Query problem on updating record.");
            }

            DB::commit();
            return redirect('/party_commission')->with('success', 'Record updated successfully.');
        } catch (Exception $e) {
            DB::rollback();
            return redirect()->back()->with('danger', $e->getMessage());
        }
    }

    public function show() {
        pr("Silence is the best.");
    }

    // Ajax Functions
    public function search(Request $r) {
    //    pr($_POST);
        $item_count = $r->input('item_count');
        $party_type_id = $r->input('party_type_id');
        $party_id = $r->input('party_id');
        $category = $r->input('category_id');
        $product = $r->input('product_id');
        $unit_size = $r->input('unit_size');
        $party_type_id = $r->input('party_type_id');
        $unit_id = $r->input('unit_id');
        $search = $r->input('search');

        $query = PartyCommission::where('is_deleted', 0);
        if (!empty($party_type_id)) {
            $query->where('party_type_id', 'like', '%' . trim($party_type_id) . '%');
        }
        if (!empty($party_id)) {
            $query->where('party_id', 'like', '%' . trim($party_id) . '%');
        }
        if (!empty($category)) {
            $query->where('category_id', 'like', '%' . trim($category) . '%');
        }
        if (!empty($product)) {
            $query->where('product_id', 'like', '%' . trim($product) . '%');
        }
        if (!empty($unit_id)) {
            $query->where('unit_id', 'like', '%' . trim($unit_id) . '%');
        }
        if (!empty($unit_size)) {
            $query->where('unit_size', 'like', '%' . trim($unit_size) . '%');
        }
        if (!empty($search)) {
            $query->where('commission', 'like', '%' . trim($search) . '%');
        }
        $dataset = $query->paginate($item_count);
        return view('party_commission._list', compact('dataset'));
    }

    public function delete() {
        $resp = array();
        DB::beginTransaction();
        try {
            foreach ($_POST['data'] as $id) {
                $data = PartyCommission::find($id);
                $data->is_deleted = 1;
                $data->deleted_by = Auth::id();
                $data->deleted_at = cur_date_time();
                if (!$data->save()) {
                    throw new Exception("Error while deleting records.");
                }
            }

            DB::commit();
            $resp['success'] = true;
            $resp['message'] = 'Record has been deleted successfully.';
        } catch (Exception $e) {
            DB::rollback();
            $resp['success'] = false;
            $resp['message'] = $e->getMessage();
        }
        return $resp;
    }

    public function dropdown_by_buisness(Request $r) {
        $dataset = Category::where([['is_deleted', 0], ['business_type_id', $r->business_type]])->get();
        $str = "";
        if (!empty($dataset)) {
            $str .= "<option value=''>Category</option>";
            foreach ($dataset as $data) {
                $str .= "<option value='{$data->id}'>{$data->name}</option>";
            }
        } else {
            $str .= "<option value=''>No Category Found</option>";
        }

        return $str;
    }

    // added by Billah
    public function dropdown_by_company(Request $r) {
        $dataset = Category::where([['is_deleted', 0], ['institute_id', $this->user_companies()]])->get();
        $str = "";
        if (!empty($dataset)) {
            $str .= "<option value=''>Category</option>";
            foreach ($dataset as $data) {
                $str .= "<option value='{$data->id}'>{$data->name}</option>";
            }
        } else {
            $str .= "<option value=''>No Category Found</option>";
        }

        return $str;
    }

    public function list_category_by_buisness(Request $r) {
        $dataset = Category::where([['is_deleted', 0], ['business_type_id', $r->business_type]])->get();
        $str = "";
        if (!empty($dataset)) {
            $str .= "<option value=''>Category</option>";
            foreach ($dataset as $data) {
                $str .= "<option value='{$data->id}'>{$data->name}</option>";
            }
        } else {
            $str .= "<option value=''>No Category Found</option>";
        }

        return $str;
    }

    //added by Billah
    public function list_category_by_company(Request $r) {
        $dataset = Category::where([['is_deleted', 0], ['institute_id', $this->user_companies()]])->get();
        $str = "";
        if (!empty($dataset)) {
            $str .= "<option value=''>Category</option>";
            foreach ($dataset as $data) {
                $str .= "<option value='{$data->id}'>{$data->name}</option>";
            }
        } else {
            $str .= "<option value=''>No Category Found</option>";
        }

        return $str;
    }

    public function partyListByType(Request $r) {

        $dataset = Party::where([['is_deleted', 0], ['party_type_id', $r->partyTypeId]])->get();
        $str = "";
        if (!empty($dataset)) {
            $str .= "<option value=''>Party</option>";
            foreach ($dataset as $data) {
                $str .= "<option value='{$data->id}'>{$data->name}</option>";
            }
        } else {
            $str .= "<option value=''>No Party Found</option>";
        }

        return $str;
    }
    public function categoryListByType(Request $r) {

        $dataset = Category::where([['is_deleted', 0], ['product_type_id', $r->productTypeId]])->get();
        $str = "";
        if (!empty($dataset)) {
            $str .= "<option value=''>Category</option>";
            foreach ($dataset as $data) {
                $str .= "<option value='{$data->id}'>{$data->name}</option>";
            }
        } else {
            $str .= "<option value=''>No Category Found</option>";
        }

        return $str;
    }
    public function productListByCategory(Request $r) {

        $dataset = Product::where([['is_deleted', 0], ['category_id', $r->product_id]])->get();
        $str = "";
        if (!empty($dataset)) {
            $str .= "<option value=''>Product</option>";
            foreach ($dataset as $data) {
                $str .= "<option value='{$data->id}'>{$data->name}</option>";
            }
        } else {
            $str .= "<option value=''>No Product Found</option>";
        }

        return $str;
    }
    public function unitSizeByUnit(Request $r) {

        $dataset = UnitSize::where([['is_deleted', 0], ['unit_id', $r->unit_id]])->get();
        $str = "";
        if (!empty($dataset)) {
            $str .= "<option value=''>Unit Size</option>";
            foreach ($dataset as $data) {
                $str .= "<option value='{$data->id}'>{$data->weight}</option>";
            }
        } else {
            $str .= "<option value=''>No Unit Size Found</option>";
        }

        return $str;
    }

    public function list_product_by_category(Request $r) {
        $stock = new Stock();
        $query = Product::where([['is_deleted', 0], ['category_id', $r->category]]);
        if (!empty($r->type)) {
            $query->where('type', $r->type);
        }
        $dataset = $query->get();
        $str = "";
        if (!empty($dataset)) {
            $str .= "<option value=''>Select Product</option>";
            foreach ($dataset as $data) {
                if ($r->type == RAW) {
                    $avg_weight = $stock->averageWeight($data->id);
                } else {
                    $avg_weight = !empty($data->weight) ? $data->weight : 1;
                }
                $str .= "<option data-weight='{$avg_weight}' value='{$data->id}'>{$data->name}</option>";
            }
        } else {
            $str .= "<option value=''>No Product Found</option>";
        }

        return $str;
    }

}
