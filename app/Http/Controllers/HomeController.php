<?php

namespace App\Http\Controllers;

use Auth;
use App\Models\User;
use App\Models\Institute;
use App\Models\Setting;
use App\Models\Account\Particular;
use Illuminate\Http\Request;

class HomeController extends Controller {

    public $model = [];
    public $dataArr = [];
    public $response = [];
    public $dateValues = [];
    public $searchValues = [];
    public $currentUser = null;
    public $idArray = [];

    public function __construct() {
        date_default_timezone_set('Asia/Dhaka');
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            $this->currentUser = Auth::user();
            view()->composer('*', function ($view) {
                $view->with('currentUser', $this->currentUser);
            });
            if ($this->currentUser->status != 1) {
                auth()->guard()->logout();
                $request->session()->invalidate();
                return redirect('/login')->with("warning", "You are not authorized to access this Application. Please contact with your service provider.");
            }
            if ($this->maintenance_mode() && !isLegend()) {
                return redirect('service/maintenance');
            }
            return $next($request);
        });
    }

    public function index() {
        $_categoryCount = Category::where('is_deleted', 0)->count();
        $_productCount = Product::where('is_deleted', 0)->count();
        //$_dsrCount = RetailerSetting::groupBy('sales_person_particular')->count();
        $_dsrCount = User::where([['is_deleted', 0], ['user_type', USER_TYPE_SR]])->count();

        $customerQuery = RetailerSetting::query();
        if ($this->currentUser->user_type != USER_TYPE_ADMIN) {
            $customerQuery->where('sales_person_particular', $this->currentUser->particular_id);
        }
        $_customerCount = $customerQuery->count();

        $this->model['categoryCount'] = $_categoryCount;
        $this->model['productCount'] = $_productCount;
        $this->model['dsrCount'] = $_dsrCount;
        $this->model['customerCount'] = $_customerCount;
        return view('home', $this->model);
    }

    public function dsr_index() {
        return view('dsr_home');
    }

    public function getSettings() {
        return Setting::get_setting();
    }

    public function is_ajax_request(Request $request) {
        if ($request->ajax()) { // check if the request is ajax
            return true;
        } else {
            return false;
        }
    }

    public function get_particulars_by_setting($id) {
        $_idArr = RetailerSetting::model()->get_customer_ids($id);
        $_particulars = Particular::whereIn('id', $_idArr)->get();
        return $_particulars;
    }

    public function demonstration_mode() {
        $_mode = \App\Models\AppService::find(1)->demo_mode;
        if ($_mode == YES) {
            return true;
        }
        return false;
    }

    public function maintenance_mode() {
        $_mode = \App\Models\AppService::find(1)->maintenance_mode;
        if ($_mode == YES) {
            return true;
        }
        return false;
    }

    public function userInstitutes($_routeName = null) {
        return Institute::get_list($this->user_companies(), $_routeName);
    }

    public function user_companies() {
        return User::get_user_company_permisstion(Auth::id());
    }

    public function ledger_view() {
        return \App\Models\AppService::find(1)->ledger_view;
    }

    public function currentRouteFile() {
        return request()->segment(1);
    }

}
