<?php

namespace App\Http\Controllers;

use Auth;
use DB;
use Exception;
use Hash;
use Image;
use Validator;
use App\Models\User;
use App\Models\UserPermission;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class UserController extends HomeController {

    public function index() {
        check_user_access('user_list');
        $dataset = User::where([['id', '!=', Auth::user()->id], ['is_deleted', 0]])->whereNotIn('id', LEGEND_USERS)->orderBy('name', 'ASC')->paginate($this->getSettings()->pagesize);
        $this->model['page_title'] = "User List";
        $this->model['dataset'] = $dataset;
        return view('Admin.user.index', $this->model);
    }

    public function create() {
        check_user_access('user_create');
        $this->model['page_title'] = "User Information";
        return view('Admin.user.create_user', $this->model);
    }

    public function store(Request $request) {
        //
    }

    public function show($id) {
        $user = User::where('_key', $id)->first();
        $this->model['page_title'] = "User Details";
        $this->model['user'] = $user;
        return view('Admin.user.user', $this->model);
    }

    public function edit($id) {
        $user = User::where('_key', $id)->first();
        $this->model['page_title'] = "User Information";
        $this->model['user'] = $user;
        return view('Admin.user.edit', $this->model);
    }

    public function admin_edit($id) {
        check_user_access('user_edit');
        $user = User::where('_key', $id)->first();
        $this->model['page_title'] = "User Information";
        $this->model['user'] = $user;
        return view('Admin.user.edit', $this->model);
    }

    public function update(Request $request, $id) {
        //pr($_POST);
        $input = $request->all();
        $rule = array(
            'name' => 'required',
            'email' => 'required|email',
        );

        $valid = Validator::make($input, $rule);
        if ($valid->fails()) {
            return redirect()->back()->withErrors($valid)->withInput();
        }

        DB::beginTransaction();
        try {
            $user = User::find($id);
            if (isAdminUser()) {
                $user->user_type = $request->user_type;
            }
            $user->name = $request->name;
            $user->email = (!$this->demonstration_mode()) ? $request->email : 'demoadmin@gmail.com';
            $user->locale = $request->locale;
            $user->updated_by = Auth::id();
            if (!empty($_FILES['file']['name'])) {
                $_savepath = public_path() . "/uploads/";
                if (!File::exists($_savepath)) {
                    File::makeDirectory($_savepath, 0755, true, true);
                }
                $_filename_full = $request->file('file')->getClientOriginalName(); //get filename with extension
                $_filename = pathinfo($_filename_full, PATHINFO_FILENAME); //get filename without extension                
                $_ext = $request->file('file')->getClientOriginalExtension(); //get file extension              
                $_newFilename = $user->_key . '.' . $_ext; //filename to store
                $thumb_img = Image::make($request->file('file')->getRealPath())->resize(200, 200); //Resize image here
                //Remove File If Exist
                if (!empty($user->avatar)) {
                    unlink($_savepath . $user->avatar);
                }
                $thumb_img->save($_savepath . $_newFilename, 80); //Upload File
                $user->avatar = $_newFilename;
                //move_uploaded_file($_tmpfilename, $_savepath . $_newFilename);
            }
            $user->save();

            DB::commit();
            return redirect('/user')->with('success', 'Record updated successfully.');
        } catch (Exception $e) {
            DB::rollback();
            return redirect()->back()->with('danger', $e->getMessage());
        }
    }

    public function destroy($id) {
        //
    }

    public function search(Request $r) {
        $item_count = !empty($r->item_count) ? $r->item_count : $this->getSettings()->pagesize;
        $srch = $r->search;
        $sort_by = $r->sort_by;
        $sort_type = $r->sort_type;

        $query = User::where([['id', '!=', Auth::user()->id], ['is_deleted', 0]])->whereNotIn('id', LEGEND_USERS);
        if (!empty($srch)) {
            $query->where('name', 'like', '%' . $srch . '%')->orWhere('email', 'like', '%' . $srch . '%');
        }
        $query->orderBy($sort_by, $sort_type);
        $dataset = $query->paginate($item_count);

        $this->model['dataset'] = $dataset;
        return view('Admin.user._list', $this->model);
    }

    public function delete() {
        DB::beginTransaction();
        try {
            foreach ($_POST['data'] as $id) {
                $data = User::find($id);
                $data->is_deleted = 1;
                if (!$data->save()) {
                    throw new Exception("Error while deleting record.");
                }
            }

            DB::commit();
            $this->response['success'] = true;
            $this->response['message'] = 'Record deleted successfully.';
        } catch (Exception $e) {
            DB::rollback();
            $this->response['success'] = false;
            $this->response['message'] = $e->getMessage();
        }
        return $this->response;
    }

    public function password() {
        $this->model['page_title'] = "Security Information";
        return view('Admin.user.password', $this->model);
    }

    public function update_user_password(Request $r) {
        $id = Auth::id();
        $input = $r->all();
        $rule = array(
            'old_password' => 'required',
            'password' => 'required|min:6|confirmed',
        );

        $valid = Validator::make($input, $rule);
        if ($valid->fails()) {
            return redirect()->back()->withErrors($valid)->withInput();
        } else {
            $user = User::find($id);
            $old = $user->password;
            $kk = $r->input('old_password');

            if (Hash::check($kk, $old)) {
                $user->password = (!$this->demonstration_mode()) ? bcrypt($r->input('password')) : bcrypt('123456');
                $user->updated_by = Auth::id();
                $user->save();
                return redirect()->back()->with('success', 'Password has been Changed Successfully.');
            } else {
                return redirect()->back()->with('danger', 'You are not authorized no change the Password.');
            }
        }
    }

    public function user_active($id) {
        DB::beginTransaction();
        try {
            $data = User::find($id);
            $data->status = 1;
            if (!$data->save()) {
                throw new Exception("Error while Updating Records from Users.");
            }

            DB::commit();
            $this->response['success'] = true;
            $this->response['message'] = 'User has been Activated Successfully';
        } catch (Exception $e) {
            DB::rollback();
            $this->response['success'] = false;
            $this->response['message'] = $e->getMessage();
        }
        return $this->response;
    }

    public function user_inactive($id) {
        DB::beginTransaction();
        try {
            $data = User::find($id);
            $data->status = 0;
            if (!$data->save()) {
                throw new Exception("Error while Updating Records from Users.");
            }

            DB::commit();
            $this->response['success'] = true;
            $this->response['message'] = 'User has been Inactivated Successfully';
        } catch (Exception $e) {
            DB::rollback();
            $this->response['success'] = false;
            $this->response['message'] = $e->getMessage();
        }
        return $this->response;
    }

    public function user_access_by_id($id) {
        check_user_access('user_access_control');
        $modelUser = new User();
        $user = $modelUser->where('_key', $id)->first();

        $this->model['page_title'] = "User Access Control";
        $this->model['user'] = $user;
        $this->model['modelUser'] = $modelUser;
        return view('Admin.user.user_access', $this->model);
    }

    public function update_user_access_by_id(Request $r) {
        //pr($_POST);
        check_user_access('user_access');
        $user_id = $r->input('user_id');
        $new_access = $r->input('access');
        $access_item = json_encode($new_access);
        $items = array('items' => $access_item);

        $update_access_item = UserPermission::where('user_id', '=', $user_id)->update($items);
        return redirect('user')->with('success', 'User Permission has been Updated Successfully.');
    }

    public function update_key() {
        $_countSuccess = 0;
        $_countFailed = 0;

        $dataset = User::all();
        foreach ($dataset as $data) {
            $data->_key = uniqueKey() . $data->id;
            if ($data->save()) {
                $_countSuccess++;
            } else {
                $_countFailed++;
            }
        }

        echo "Success = {$_countSuccess}<br>";
        echo "Failed = {$_countFailed}<br>";
        exit;
    }

}
