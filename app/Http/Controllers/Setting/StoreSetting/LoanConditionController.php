<?php

namespace App\Http\Controllers\Setting\StoreSetting;

use Auth;
use DB;
use Exception;
use Validator;
use App\Http\Controllers\HomeController;
use App\Models\Setting\StoreSetting\LoanCondition;
use Illuminate\Http\Request;

class LoanConditionController extends HomeController {

    public function index() {
        check_user_access('loan_condition_list');
        $dataset = LoanCondition::where('is_deleted', 0)->orderBy('description', 'asc')->paginate($this->getSettings()->pagesize);

        $this->model['companyList'] = $this->userInstitutes();
        $this->model['page_title'] = "Loan Condition List";
        $this->model['dataset'] = $dataset;
//        pr($dataset);
        return view('loan_condition.index', $this->model);
    }

    public function create() {
        check_user_access('loan_condition_create');
        $_companyList = $this->userInstitutes();

        $this->model['page_title'] = "Loan Condition Information";
        $this->model['companyList'] = $_companyList;
        return view('loan_condition.create', $this->model);
    }

    public function store(Request $r) {
        $input = $r->all();
        $rules = array(
            'description' => 'required'
        );
        $messages = array(
            'description.required' => 'Description required.'
        );

        $valid = Validator::make($input, $rules, $messages);
        if ($valid->fails()) {
            return redirect()->back()->withErrors($valid)->withInput();
        }

        DB::beginTransaction();
        try {
            $model = new LoanCondition();
            $model->description = $r->description;
            $model->created_at = cur_date_time();
            $model->created_by = Auth::id();
            $model->_key = uniqueKey();
            if (!$model->save()) {
                throw new Exception("Error while saving record.");
            }
            DB::commit();
            return redirect('/loan_condition')->with('success', 'Record saved successfully.');
        } catch (Exception $e) {
            DB::rollback();
            return redirect()->back()->with('danger', $e->getMessage());
        }
    }

    public function edit($id) {
        check_user_access('store_condition_edit');
        $data = LoanCondition::where('_key', $id)->first();
        $_companyList = $this->userInstitutes();

        $this->model['page_title'] = "Loan Condition Edit";
        $this->model['data'] = $data;
        $this->model['companyList'] = $_companyList;
        return view('loan_condition.edit', $this->model);
    }

    public function update(Request $r, $id) {
        $input = $r->all();
        $rules = array(
            'description' => 'required'
        );
        $messages = array(
            'description.required' => 'Description required.'
        );

        $valid = Validator::make($input, $rules, $messages);
        if ($valid->fails()) {
            return redirect()->back()->withErrors($valid)->withInput();
        }

        DB::beginTransaction();
        try {
            $model = LoanCondition::find($id);
            $model->description = $r->description;
            $model->modified_at = cur_date_time();
            $model->modified_by = Auth::id();
            if (!$model->save()) {
                throw new Exception("Error while updating record.");
            }

            DB::commit();
            return redirect('/loan_condition')->with('success', 'Record updated successfully.');
        } catch (Exception $e) {
            DB::rollback();
            return redirect()->back()->with('danger', $e->getMessage());
        }
    }

    // Ajax Functions
    public function search(Request $r) {
        $item_count = !empty($r->item_count) ? $r->item_count : $this->getSettings()->pagesize;
        $srch = $r->srch;

        $query = LoanCondition::where('is_deleted', 0);
        if (!empty($srch)) {
            $query->where('description', 'like', '%' . $srch . '%');
        }
        $query->orderBy('description', 'asc');
        $dataset = $query->paginate($item_count);

        $this->model['dataset'] = $dataset;
        return view('loan_condition._list', $this->model);
    }

    public function delete() {
        $resp = array();
        DB::beginTransaction();
        try {
            foreach ($_POST['data'] as $id) {
                $data = LoanCondition::find($id);
                $data->is_deleted = 1;
                $data->deleted_by = Auth::id();
                $data->deleted_at = cur_date_time();
                if (!$data->save()) {
                    throw new Exception("Error while deleting records.");
                }
            }
            DB::commit();
            $resp['success'] = true;
            $resp['message'] = 'Record has been deleted successfully.';
        } catch (Exception $e) {
            DB::rollback();
            $resp['success'] = false;
            $resp['message'] = $e->getMessage();
        }
        return $resp;
    }

}
