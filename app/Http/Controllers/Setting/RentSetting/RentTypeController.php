<?php

namespace App\Http\Controllers\Setting\RentSetting;

use Auth;
use DB;
use Exception;
use Validator;
use App\Http\Controllers\HomeController;
use App\Models\Setting\RentSetting\RentType;
use Illuminate\Http\Request;

class RentTypeController extends HomeController {

    public function index() {
        check_user_access('rent_type_list');
        $dataset = RentType::where('is_deleted', 0)->orderBy('rent_type', 'asc')->paginate($this->getSettings()->pagesize);

        $this->model['page_title'] = "Rent Type Information";
        $this->model['dataset'] = $dataset;
// dd($dataset);
        return view('rent_type.index', $this->model);
    }

    public function create() {
        check_user_access('rent_type_create');
        $_companyList = $this->userInstitutes();

        $this->model['page_title'] = "Rent Type Create ";
        return view('rent_type.create', $this->model);
    }

    public function store(Request $r) {

        $input = $r->all();
        $rules = array(
            'rent_type' => 'required',
        );
        $messages = array(
            'rent_type.required' => 'Rent type  required.',
        );

        $valid = Validator::make($input, $rules, $messages);
        if ($valid->fails()) {
            return redirect()->back()->withErrors($valid)->withInput();
        }

        DB::beginTransaction();
        try {
            $model = new RentType();
            $model->rent_type = $r->rent_type;
            $model->description = $r->description;
            $model->created_at = cur_date_time();
            $model->created_by = Auth::id();
            $model->_key = uniqueKey();
            if (!$model->save()) {
                throw new Exception("Error while saving record.");
            }
            DB::commit();
            return redirect('/rent_type')->with('success', 'Record saved successfully.');
        } catch (Exception $e) {
            DB::rollback();
            return redirect()->back()->with('danger', $e->getMessage());
        }
    }

    public function edit($id) {
        check_user_access('rent_type_edit');
        $data = RentType::where('_key', $id)->first();

        $this->model['page_title'] = "Rent Type Edit";
        $this->model['data'] = $data;
        return view('rent_type.edit', $this->model);
    }

    public function update(Request $r, $id) {
        $input = $r->all();
        $rules = array(
            'rent_type' => 'required',
        );
        $messages = array(
            'rent_type.required' => 'Rent type required.',
        );

        $valid = Validator::make($input, $rules, $messages);
        if ($valid->fails()) {
            return redirect()->back()->withErrors($valid)->withInput();
        }

        DB::beginTransaction();
        try {
            $model = RentType::find($id);
            $model->rent_type = $r->rent_type;
            $model->description = $r->description;
            $model->modifyed_at = cur_date_time();
            $model->modifyed_by = Auth::id();
            if (!$model->save()) {
                throw new Exception("Error while updating record.");
            }

            DB::commit();
            return redirect('/rent_type')->with('success', 'Record updated successfully.');
        } catch (Exception $e) {
            DB::rollback();
            return redirect()->back()->with('danger', $e->getMessage());
        }
    }

    // Ajax Functions
    public function search(Request $r) {
        $item_count = !empty($r->item_count) ? $r->item_count : $this->getSettings()->pagesize;
        $srch = $r->srch;

        $query = RentType::where('is_deleted', 0);
     
        if (!empty($srch)) {
            $query->where('rent_type', 'like', '%' . $srch . '%');
        }
        $query->orderBy('rent_type', 'asc');
        $dataset = $query->paginate($item_count);

        $this->model['dataset'] = $dataset;
        return view('rent_type._list', $this->model);
    }

    public function delete() {
        $resp = array();
        DB::beginTransaction();
        try {
            foreach ($_POST['data'] as $id) {
                $data = RentType::find($id);
                $data->is_deleted = 1;
                $data->deleted_by = Auth::id();
                $data->deleted_at = cur_date_time();
                if (!$data->save()) {
                    throw new Exception("Error while deleting records.");
                }
            }
            DB::commit();
            $resp['success'] = true;
            $resp['message'] = 'Record has been deleted successfully.';
        } catch (Exception $e) {
            DB::rollback();
            $resp['success'] = false;
            $resp['message'] = $e->getMessage();
        }
        return $resp;
    }



}
