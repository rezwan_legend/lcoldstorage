<?php

namespace App\Http\Controllers\Setting\RentSetting;

use App\Http\Controllers\HomeController;
use App\Models\Setting\ProductModule\Category;
use Exception;
use Auth;
use App\Models\Setting\RentSetting\Rent;
use App\Models\Setting\ProductModule\Product;
use App\Models\Setting\ProductModule\UnitSize;
use App\Models\Setting\ProductModule\ProductType;
use App\Models\Setting\RentSetting\RentType;
use App\Models\Setting\ProductModule\Unit;
use DB;
use Validator;
use Illuminate\Http\Request;

class RentController extends HomeController {

    public function index() {
        check_user_access('rent_list');
        $model = new Rent();
        $query = $model->where('is_deleted', 0);
        $dataset = $query->paginate($this->getSettings()->pagesize);
        $_product_type = ProductType::where('is_deleted', 0)->get();
        $_category = Category::where('is_deleted', 0)->get();
        $_rent_type = RentType::where('is_deleted', 0)->get();
        $UnitSize = UnitSize::where('is_deleted', 0)->get();
        $_unit = Unit::where('is_deleted', 0)->get();
        $businessList = $this->userInstitutes();

        $this->model['breadcrumb_title'] = "Rent List";
        $this->model['dataset'] = $dataset;
        $this->model['businessList'] = $businessList;
        $this->model['product_type'] = $_product_type;
        $this->model['category'] = $_category;
        $this->model['units'] = $_unit;
        $this->model['rentTypes'] = $_rent_type;
        $this->model['UnitSize'] = $UnitSize;
        return view('rent.index', $this->model);
    }

    public function create() {
        check_user_access('rent_create');
        $businessList = $this->userInstitutes();
        $_product_type = ProductType::where('is_deleted', 0)->get();
        $categories = Category::where('is_deleted', 0)->get();
        $_unit = Unit::where('is_deleted', 0)->get();
        $_rent_type = RentType::where('is_deleted', 0)->get();
        $_produt_size = UnitSize::where('is_deleted', 0)->get();
        $this->model['product_type'] = $_product_type;

        $this->model['breadcrumb_title'] = "Rent Information";
        $this->model['businessList'] = $businessList;
        $this->model['categories'] = $categories;
        $this->model['units'] = $_unit;
        $this->model['rentTypes'] = $_rent_type;
        $this->model['UnitSize'] = $_produt_size;
        return view('rent.create', $this->model);
    }
    
    public function store(Request $r) {
//        pr($_POST);
        $input = $r->all();
        $rules = array(
            'rent' => 'required'
        );
        $messages = array(
            'rent.required' => 'Rent Required.'
        );

        $valid = Validator::make($input, $rules, $messages);
        if ($valid->fails()) {
            return redirect()->back()->withErrors($valid)->withInput();
        }

        DB::beginTransaction();
        try {
            $model = new Rent();
            $model->product_type_id = $r->product_type_id;
            $model->product_id = $r->product_id;
            $model->category_id = $r->category_id;
            $model->unit_id = $r->unit_id;
            $model->unit_size = $r->unit_size;
            $model->rent_type_id = $r->rent_type_id;
            $model->rent = $r->rent;
            $model->created_at = cur_date_time();
            $model->created_by = Auth::id();
            $model->_key = uniqueKey();
            if (!$model->save()) {
                throw new Exception("Query problem on creating record.");
            }

            DB::commit();
            return redirect('/rent')->with('success', 'New record created successfully.');
        } catch (Exception $e) {
            DB::rollback();
            return redirect()->back()->with('danger', $e->getMessage());
        }
    }

    public function edit($id) {
        check_user_access('rent_edit');
        $data = Rent::find($id);
        $businessList = $this->userInstitutes();
        $_product_type = ProductType::where('is_deleted', 0)->get();
        $_categories = Category::where('is_deleted', 0)->get();
        $_products = Product::where('is_deleted', 0)->get();
        $_units = Unit::where('is_deleted', 0)->get();
        $_unitSize = UnitSize::where('is_deleted', 0)->get();
        $_rent_types = RentType::where('is_deleted', 0)->get();
        $this->model['product_type'] = $_product_type;
        $this->model['categories'] = $_categories;
        $this->model['products'] = $_products;
        $this->model['products'] = $_products;
        $this->model['units'] = $_units;
        $this->model['unitSize'] = $_unitSize;
        $this->model['rent_types'] = $_rent_types;

        $this->model['page_title'] = "Rent Information";
        $this->model['businessList'] = $businessList;
        $this->model['data'] = $data;
        return view('rent.edit', $this->model);
    }

    public function update(Request $r, $id) {
    //    pr($_POST);
        $input = $r->all();
        $rules = array(
            'rent' => 'required'
        );
        $messages = array(
            'rent.required' => 'Rent Required.'
        );

        $valid = Validator::make($input, $rules, $messages);
        if ($valid->fails()) {
            return redirect()->back()->withErrors($valid)->withInput();
        }

        DB::beginTransaction();
        try {
            $model = Rent::find($id);
            $model->product_type_id = $r->product_type_id;
            $model->product_id = $r->product_id;
            $model->category_id = $r->category_id;
            $model->unit_id = $r->unit_id;
            $model->unit_size = $r->unit_size;
            $model->rent_type_id = $r->rent_type_id;
            $model->rent = $r->rent;
            $model->modified_at = cur_date_time();
            $model->modified_by = Auth::id();
            if (!$model->save()) {
                throw new Exception("Query problem on updating record.");
            }

            DB::commit();
            return redirect('/rent')->with('success', 'Record updated successfully.');
        } catch (Exception $e) {
            DB::rollback();
            return redirect()->back()->with('danger', $e->getMessage());
        }
    }

    public function show() {
        pr("Silence is the best.");
    }

    // Ajax Functions
    public function search(Request $r) {
    //    pr($_POST);
        $item_count = $r->item_count;
        $product_type = $r->product_type_id;
        $category = $r->category_id;
        $product = $r->product_id;
        $unit_size = $r->unit_size;
        $rent_type_id = $r->rent_type_id;
        $unit = $r->unit;
        $search = $r->search;

        $query = Rent::where('is_deleted', 0);
        if (!empty($product_type)) {
            $query->where('product_type_id', 'like', '%' . trim($product_type) . '%');
        }
        if (!empty($category)) {
            $query->where('category_id', 'like', '%' . trim($category) . '%');
        }
        if (!empty($product)) {
            $query->where('product_id', 'like', '%' . trim($product) . '%');
        }
        if (!empty($unit_size)) {
            $query->where('unit_size', 'like', '%' . trim($unit_size) . '%');
        }
        if (!empty($rent_type_id)) {
            $query->where('rent_type_id', 'like', '%' . trim($rent_type_id) . '%');
        }
        if (!empty($unit)) {
            $query->where('unit_id', 'like', '%' . trim($unit) . '%');
        }
        if (!empty($search)) {
            $query->where('rent', 'like', '%' . trim($search) . '%');
        }
        $dataset = $query->paginate($item_count);
        return view('rent._list', compact('dataset'));
    }

    public function delete() {
        $resp = array();
        DB::beginTransaction();
        try {
            foreach ($_POST['data'] as $id) {
                $data = Rent::find($id);


                $data->is_deleted = 1;
                $data->deleted_by = Auth::id();
                $data->deleted_at = cur_date_time();
                if (!$data->save()) {
                    throw new Exception("Error while deleting records.");
                }
            }

            DB::commit();
            $resp['success'] = true;
            $resp['message'] = 'Record has been deleted successfully.';
        } catch (Exception $e) {
            DB::rollback();
            $resp['success'] = false;
            $resp['message'] = $e->getMessage();
        }
        return $resp;
    }




    public function categoryListByType(Request $r) {

        $dataset = Category::where([['is_deleted', 0], ['product_type_id', $r->productTypeId]])->get();
        $str = "";
        if (!empty($dataset)) {
            $str .= "<option value=''>Category</option>";
            foreach ($dataset as $data) {
                $str .= "<option value='{$data->id}'>{$data->name}</option>";
            }
        } else {
            $str .= "<option value=''>No Category Found</option>";
        }

        return $str;
    }
    public function productListByCategory(Request $r) {

        $dataset = Product::where([['is_deleted', 0], ['category_id', $r->product_id]])->get();
        $str = "";
        if (!empty($dataset)) {
            $str .= "<option value=''>Product</option>";
            foreach ($dataset as $data) {
                $str .= "<option value='{$data->id}'>{$data->name}</option>";
            }
        } else {
            $str .= "<option value=''>No Product Found</option>";
        }

        return $str;
    }

    public function product_sizeByUnit(Request $r) {

        $dataset = UnitSize::where([['is_deleted', 0], ['unit_id', $r->UnitSizeId]])->get();
        $str = "";
        if (!empty($dataset)) {
            $str .= "<option value=''>Unit Size</option>";
            foreach ($dataset as $data) {
               
                $str .= "<option value='{$data->id}'>{$data->name}</option>";
            }
        } else {
            $str .= "<option value=''>No unit size Found</option>";
        }

        return $str;
    }



 
}
