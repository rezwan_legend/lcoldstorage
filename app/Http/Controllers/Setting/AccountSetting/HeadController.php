<?php

namespace App\Http\Controllers\Setting\AccountSetting;

use Auth;
use DB;
use Exception;
use Validator;
use App\Http\Controllers\HomeController;
use Illuminate\Http\Request;
use App\Models\Setting\AccountSetting\Head;
use App\Models\Account\Transaction;

class HeadController extends HomeController {

    public function index() {
        check_user_access('head_list');
        $tmodel = new Transaction();

        $model = new Head();
        $query = $model->where('is_deleted', 0);
        $query->orWhere('common', COMMON_YES);
        $query->orderBy('code', 'ASC');
        $dataset = $query->paginate($this->getSettings()->pagesize);

        $this->model['page_title'] = "Head List";
        $this->model['tmodel'] = $tmodel;
        $this->model['dataset'] = $dataset;
        return view('account.head.index', $this->model);
    }

    public function create() {
        check_user_access('head_create');

        $this->model['page_title'] = "Head Information";
        return view('account.head.create', $this->model);
    }

    public function store(Request $r) {
        // pr($_POST);
        DB::beginTransaction();
        try {
            foreach ($_POST['name'] as $key => $value) {
                $model = new Head();
                $model->name = $r->name[$key];
                $model->code = !empty($_POST['code'][$key]) ? $_POST['code'][$key] : time() + $key;
                $model->common = $r->common_id[$key];
                $model->_key = uniqueKey() . $key;
                if (!$model->save()) {
                    throw new Exception("Error while Creating Records.");
                }
            }

            DB::commit();
            return response()->json('Records Created Successfully.');
        } catch (Exception $e) {
            DB::rollback();
            return response()->json($e->getMessage());
        }
    }

    public function edit($id) {
        check_user_access('head_edit');
        $data = Head::where('_key', $id)->first();

        $this->model['page_title'] = "Head Information";
        $this->model['data'] = $data;
        return view('account.head.edit', $this->model);
    }

    public function update(Request $r, $id) {
        //pr($_POST);
        $input = $r->all();
        $rule = array(
            'name' => 'required',
        );
        $messages = array(
            'name.required' => 'Name Should not be empty.',
        );

        $valid = Validator::make($input, $rule, $messages);
        if ($valid->fails()) {
            return redirect()->back()->withErrors($valid)->withInput();
        }

        DB::beginTransaction();
        try {
            $data = Head::find($id);
            $data->name = $r->name;
            $data->code = $r->code;
            $data->common = $r->common;
            if (!$data->save()) {
                throw new Exception("Query Problem on Updating Record.");
            }

            DB::commit();
            return redirect('/head')->with('success', 'Record Updated Successfully.');
        } catch (Exception $e) {
            DB::rollback();
            return redirect()->back()->with('danger', $e->getMessage());
        }
    }

    public function show($id) {
        $dataset = Head::where('_key', $id)->first();
//        pr($dataset);
        $this->model['page_title'] = "Head Information";
        $this->model['dataset'] = $dataset;
        return view('account.head.ledger', $this->model);
    }

    // Ajax Functions
    public function search(Request $r) {
        //pr($_POST);
        $item_count = !empty($r->item_count) ? $r->item_count : $this->getSettings()->pagesize;
        $head = $r->head_id;
        $search = $r->search;
        $sort_by = $r->sort_by;
        $sort_type = $r->sort_type;
        $tmodel = new Transaction();

        $model = new Head();
        $query = $model->where('is_deleted', 0);
        if (!empty($head)) {
            $query->where('id', $head);
        }
        if (!empty($search)) {
            $query->where('name', 'like', '%' . $search . '%')->orWhere('code', 'like', '%' . $search . '%');
        }
        $query->orderBy($sort_by, $sort_type);
        $dataset = $query->paginate($item_count);

        $this->model['dataset'] = $dataset;
        $this->model['tmodel'] = $tmodel;
        return view('account.head._list', $this->model);
    }

    public function delete() {
        //pr($_POST);
        $tmodel = new Transaction();

        DB::beginTransaction();
        try {
            foreach ($_POST['data'] as $id) {
                $data = Head::find($id);

                $trans_head = $tmodel->where([['dr_head_id', $id], ['is_deleted', 0]])->orWhere([['cr_head_id', $id], ['is_deleted', 0]])->first();
                if (!empty($trans_head)) {
                    throw new Exception("Warning! One or More Transaction Exist. Please Delete Transaction First.");
                }

                if (!empty($data->subheads)) {
                    foreach ($data->subheads as $subhead) {
                        $trans_subhead = $tmodel->where([['dr_subhead_id', $subhead->id], ['is_deleted', 0]])->orWhere([['cr_subhead_id', $subhead->id], ['is_deleted', 0]])->first();
                        if (!empty($trans_subhead)) {
                            throw new Exception("Warning! One or More Transaction Exist. Please Delete Transaction First.");
                        }

                        if (!empty($subhead->particulars)) {
                            foreach ($subhead->particulars as $particular) {
                                $trans_particular = $tmodel->where([['dr_particular_id', $particular->id], ['is_deleted', 0]])->orWhere([['cr_particular_id', $particular->id], ['is_deleted', 0]])->first();
                                if (!empty($trans_particular)) {
                                    throw new Exception("Warning! One or More Transaction Exist. Please Delete Transaction First.");
                                }

                                $particular->is_deleted = 1;
                                $particular->deleted_by = Auth::id();
                                $particular->deleted_at = cur_date_time();
                                if (!$particular->save()) {
                                    throw new Exception("Error while deleting particular records.");
                                }
                            }
                        }

                        $subhead->is_deleted = 1;
                        $subhead->deleted_by = Auth::id();
                        $subhead->deleted_at = cur_date_time();
                        if (!$subhead->save()) {
                            throw new Exception("Error while deleting subheadv records.");
                        }
                    }
                }

                $data->is_deleted = 1;
                $data->deleted_by = Auth::id();
                $data->deleted_at = cur_date_time();
                if (!$data->save()) {
                    throw new Exception("Error while deleting records.");
                }
            }

            DB::commit();
            $this->response['success'] = true;
            $this->response['message'] = 'Record/s deleted successfully.';
        } catch (Exception $e) {
            DB::rollback();
            $this->response['success'] = false;
            $this->response['message'] = $e->getMessage();
        }
        return $this->response;
    }

}
