<?php

namespace App\Http\Controllers\Setting\AccountSetting;

use Auth;
use DB;
use Exception;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\HomeController;
use App\Models\Setting\AccountSetting\Head;
use App\Models\Setting\AccountSetting\SubHead;
use App\Models\Account\Particular;
use App\Models\Account\Transaction;

class SubHeadController extends HomeController {

    public function index() {
        check_user_access('subhead_list');
        $heads = Head::where('is_deleted', 0)->get();
        $tmodel = new Transaction();
        $insList = $this->userInstitutes();

        $model = new SubHead();
        $query = $model->where('is_deleted', 0);
        $query->orderBy('code', 'ASC');
        $dataset = $query->paginate($this->getSettings()->pagesize);

        $this->model['page_title'] = "Subhead List";
        $this->model['heads'] = $heads;
        $this->model['tmodel'] = $tmodel;
        $this->model['dataset'] = $dataset;
        return view('account.subhead.index', $this->model);
    }

    public function create() {
        check_user_access('subhead_create');
        $insList = $this->userInstitutes();
        $heads = Head::where('is_deleted', 0)->get();

        $this->model['page_title'] = "Subhead Information";
        $this->model['insList'] = $insList;
        $this->model['heads'] = $heads;
        return view('account.subhead.create', $this->model);
    }

    public function store(Request $r) {
        //pr($_POST);
        DB::beginTransaction();
        try {
            foreach ($_POST['name'] as $key => $value) {

                $model = new SubHead();
                $model->head_id = $r->head[$key];
                $model->name = $r->name[$key];
                $model->code = !empty($_POST['code'][$key]) ? $_POST['code'][$key] : time() + $key;
                $model->common = $r->common_id[$key];
                $model->_key = uniqueKey() . $key;
                if (!$model->save()) {
                    throw new Exception("Error while Creating Records.");
                }
            }

            DB::commit();
            return response()->json('New Record Created Successfully.');
        } catch (Exception $e) {
            DB::rollback();
            return response()->json($e->getMessage());
        }
    }

    public function edit($id) {
        check_user_access('subhead_edit');
        $data = SubHead::where('_key', $id)->first();
        $head_set = Head::where([['institute_id', $data->institute_id], ['is_deleted', 0]])->orWhere('common', COMMON_YES)->get();

        $this->model['page_title'] = "Subhead Information";
        $this->model['data'] = $data;
        $this->model['head_set'] = $head_set;
        return view('account.subhead.edit', $this->model);
    }

    public function update(Request $r, $id) {
        //pr($_POST);
        $input = $r->all();
        $rule = array(
            'head_id' => 'required',
            'name' => 'required',
        );
        $messages = array(
            'head_id.required' => 'Head must be selected.',
            'name.required' => 'Name Should not be empty.',
        );

        $valid = Validator::make($input, $rule, $messages);
        if ($valid->fails()) {
            return redirect()->back()->withErrors($valid)->withInput();
        }

        DB::beginTransaction();
        try {
            $data = SubHead::find($id);
            $data->head_id = $r->head_id;
            $data->name = $r->name;
            $data->code = $r->code;
            $data->common = $r->common;
            if (!$data->save()) {
                throw new Exception("Query Problem on Updating Record.");
            }

            DB::commit();
            return redirect('/subhead')->with('success', 'Record Updated Successfully.');
        } catch (Exception $e) {
            DB::rollback();
            return redirect()->back()->with('danger', $e->getMessage());
        }
    }

    // Ajax Functions
    public function search(Request $r) {
//        pr($_POST);
        $item_count = !empty($r->item_count) ? $r->item_count : $this->getSettings()->pagesize;
        $head = $r->head_id;
        $search = $r->search;
        $sort_by = $r->sort_by;
        $sort_type = $r->sort_type;
        $tmodel = new Transaction();
        $model = new SubHead();
        $query = $model->where('is_deleted', 0);

        if (!empty($head)) {
            $query->where('head_id', $head);
        }
        if (!empty($search)) {
            $query->where('name', 'like', '%' . $search . '%')->orWhere('code', 'like', '%' . $search . '%');
        }
        $query->orderBy($sort_by, $sort_type);
        $dataset = $query->paginate($item_count);
        $this->model['dataset'] = $dataset;
        $this->model['tmodel'] = $tmodel;
        return view('account.subhead._list', $this->model);
    }

    public function delete() {
        //pr($_POST);
        $tmodel = new Transaction();

        DB::beginTransaction();
        try {
            foreach ($_POST['data'] as $id) {
                $data = SubHead::find($id);

                $existTrans = $tmodel->where([['dr_subhead_id', $id], ['is_deleted', 0]])->orWhere([['cr_subhead_id', $id], ['is_deleted', 0]])->first();
                if (!empty($existTrans)) {
                    throw new Exception("Warning! One or More Transaction Exist. Please Delete Transaction First.");
                }

                if (!empty($data->particulars)) {
                    foreach ($data->particulars as $item) {
                        $existpartTrans = $tmodel->where([['dr_particular_id', $item->id], ['is_deleted', 0]])->orWhere([['cr_particular_id', $item->id], ['is_deleted', 0]])->first();
                        if (!empty($existpartTrans)) {
                            throw new Exception("Warning! One or More Transaction Exist. Please Delete Transaction First.");
                        }

                        $item->is_deleted = 1;
                        $item->deleted_by = Auth::id();
                        $item->deleted_at = cur_date_time();
                        if (!$item->save()) {
                            throw new Exception("Error while deleting subhead records.");
                        }
                    }
                }

                $data->is_deleted = 1;
                $data->deleted_by = Auth::id();
                $data->deleted_at = cur_date_time();
                if (!$data->save()) {
                    throw new Exception("Error while deleting records.");
                }
            }

            DB::commit();
            $this->response['success'] = true;
            $this->response['message'] = 'Record/s deleted successfully.';
        } catch (Exception $e) {
            DB::rollback();
            $this->response['success'] = false;
            $this->response['message'] = $e->getMessage();
        }

        return $this->response;
    }

    public function get_sub_head(Request $r) {
        //$dataset = SubHead::where([['is_deleted', 0], ['head_id', $r->head]])->get();
        $querySubhead = SubHead::where('is_deleted', 0);
        if (!empty($r->head)) {
            $querySubhead->where('head_id', $r->head)->orWhere('common', COMMON_YES);
        }
        $dataset = $querySubhead->orderBy('name', 'ASC')->get();
        $str = ["<option value=''>Select Subhead</option>"];
        if (!empty($dataset)) {
            foreach ($dataset as $data) {
                $str[] = "<option value='$data->id'>{$data->name}</option>";
            }
            return $str;
        }
    }

    public function get_particular(Request $r) {
        $dataset = Particular::where([['is_deleted', 0], ['subhead_id', $r->head]])->get();
        $str = ["<option value=''>Select Particular</option>"];
        if (!empty($dataset)) {
            foreach ($dataset as $data) {
                $str[] = "<option value='$data->id'>{$data->name} ($data->address)</option>";
            }
            return $str;
        }
    }

}
