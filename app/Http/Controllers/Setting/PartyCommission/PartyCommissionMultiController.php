<?php

namespace App\Http\Controllers\Setting\PartyCommission;

use App\Http\Controllers\HomeController;
use App\Models\Setting\ProductModule\Category;
use Exception;
use Auth;
use App\Models\Setting\ProductModule\Product;
use App\Models\Setting\ProductModule\UnitSize;
use App\Models\Setting\ProductModule\Unit;
use App\Models\Setting\Party\PartyType;
use App\Models\Setting\Party\Party;
use App\Models\Setting\PartyCommission\PartyCommissionMulti;
use DB;
use Validator;
use Illuminate\Http\Request;

class PartyCommissionMultiController extends HomeController {

    public function index() {
        check_user_access('commission_list');
        $model = new PartyCommissionMulti();
        $query = $model->where('is_deleted', 0);
        $dataset = $query->paginate($this->getSettings()->pagesize);
        $_category = Category::where('is_deleted', 0)->get();
        $_product_size = UnitSize::where('is_deleted', 0)->get();
        $_unit = Unit::where('is_deleted', 0)->get();
        $_party_type = PartyType::where('is_deleted', 0)->get();
        $_party= Party::where('is_deleted', 0)->get();
        $_Product = Product::where('is_deleted', 0)->get();


        $this->model['page_title'] = "Party Commission List";
        $this->model['dataset'] = $dataset;
        $this->model['party_type'] = $_party_type;
        $this->model['party'] = $_party;
        $this->model['category'] = $_category;
        $this->model['product'] = $_Product;
        $this->model['units'] = $_unit;
        $this->model['product_size'] = $_product_size;
        
        return view('party_commission_multi.index', $this->model);
    }

    public function create() {
        check_user_access('party_commission_multi_create');
        $_party_type = PartyType::where('is_deleted', 0)->get();
        $_party= Party::where('is_deleted', 0)->get();
        $categories = Category::where('is_deleted', 0)->get();
        $_Product = Product::where('is_deleted', 0)->get();
        $_unit = Unit::where('is_deleted', 0)->get();
        $_unit_size = UnitSize::where('is_deleted', 0)->get();
        $this->model['page_title'] = "Party Commission Create";
        $this->model['breadcrumb_title'] = "Party Commission Create";
        $this->model['party_type'] = $_party_type;
        $this->model['party'] = $_party;
        $this->model['categories'] = $categories;
        $this->model['products'] = $_Product;
        $this->model['units'] = $_unit;
        $this->model['unitSize'] = $_unit_size;
        return view('party_commission_multi.create', $this->model);
    }


    public function edit($id) {
        check_user_access('rent_edit');
        $data = PartyCommissionMulti::find($id);
        $_party_type = PartyType::where('is_deleted', 0)->get();
        $_party= Party::where('is_deleted', 0)->get();
        $categories = Category::where('is_deleted', 0)->get();
        $_Product = Product::where('is_deleted', 0)->get();
        $_unit = Unit::where('is_deleted', 0)->get();
        $_unit_size = UnitSize::where('is_deleted', 0)->get();
        $this->model['page_title'] = "Party Commission Edit";
        $this->model['breadcrumb_title'] = "Party Commission Edit";
        $this->model['party_type'] = $_party_type;
        $this->model['party'] = $_party;
        $this->model['categories'] = $categories;
        $this->model['products'] = $_Product;
        $this->model['units'] = $_unit;
        $this->model['unitSize'] = $_unit_size;
        $this->model['data'] = $data;
        return view('party_commission_multi.edit', $this->model);
    }

    public function update(Request $r, $id) {
    //    pr($_POST);
        $input = $r->all();
        $rules = array(
            'commission' => 'required'
        );
        $messages = array(
            'commission.required' => 'commission Required.'
        );

        $valid = Validator::make($input, $rules, $messages);
        if ($valid->fails()) {
            return redirect()->back()->withErrors($valid)->withInput();
        }

        DB::beginTransaction();
        try {
            $model = PartyCommissionMulti::find($id);
            $model->party_type_id = $r->party_type_id;
            $model->party_id = $r->party_id;
            $model->category_id = $r->category_id;
            $model->product_id = $r->product_id;
            $model->unit_id = $r->unit_id;
            $model->unit_size_id = $r->unit_size_id;
            $model->commission = $r->commission;
            $model->modified_at = cur_date_time();
            $model->modified_by = Auth::id();
            if (!$model->save()) {
                throw new Exception("Query problem on updating record.");
            }

            DB::commit();
            return redirect('/party_commission_multi')->with('success', 'Record updated successfully.');
        } catch (Exception $e) {
            DB::rollback();
            return redirect()->back()->with('danger', $e->getMessage());
        }
    }

        public function store(Request $r) {
        DB::beginTransaction();
        try {
            $data = [];
            $party_type_id = $r->party_type_id;
            $party_id = $r->party_id;
            $category_id = $r->category_id;
            $product_id  = $r->product_id;
            $unit_id = $r->unit_id;
            $unit_size_id = $r->unit_size_id;
            $commission = $r->commission;
            
            for($i=0; $i<count($r->category_id); $i++){
                $temp = [
                    "party_type_id" => $party_type_id[$i],
                    "party_id" => $party_id[$i],
                    "category_id" => $category_id[$i],
                    "product_id"  => $product_id[$i],
                    "unit_id"  => $unit_id[$i],
                    "unit_size_id"  => $unit_size_id[$i],
                    "commission"  => $commission[$i],
                    "created_at" => cur_date_time(),
                    "created_by" => Auth::id()
                ];

                $data[] = $temp;

            }
            $insert = PartyCommissionMulti::insert($data);
            if (!$insert) {
                throw new Exception("Something went wrong while inserting order items");
            }
            DB::commit();
                return response()->json('New Records Created Successfully.');
        } catch (Exception $error) {
            DB::rollback();
            return response()->json($error->getMessage(), $error->getCode());
        }
        }

    




    public function show() {
        pr("From show method");
    }

    // Ajax Functions
    public function search(Request $r) {
    //    pr($_POST);
        $item_count = $r->input('item_count');
        $party_type_id = $r->input('party_type_id');
        $party_id = $r->input('party_id');
        $category_id = $r->input('category_id');
        $product_id = $r->input('Product_id');
        $unit_id = $r->input('unit_id');
        $unit_size_id = $r->input('unit_size_id');
        $commission_id = $r->input('commission');

        $query = PartyCommissionMulti::where('is_deleted', 0);
        if (!empty($party_type_id)) {
            $query->where('party_type_id', 'like', '%' . trim($party_type_id) . '%');
        }
        if (!empty($party_id)) {
            $query->where('party_id', 'like', '%' . trim($party_id) . '%');
        }
        if (!empty($category_id)) {
            $query->where('category_id', 'like', '%' . trim($category_id) . '%');
        }
        if (!empty($product_id)) {
            $query->where('product_id', 'like', '%' . trim($product_id) . '%');
        }
        if (!empty($unit_id)) {
            $query->where('unit_id', 'like', '%' . trim($unit_id) . '%');
        }
        if (!empty($unit_size_id)) {
            $query->where('unit_size_id', 'like', '%' . trim($unit_size_id) . '%');
        }
        if (!empty($commission_id)) {
            $query->where('commission', 'like', '%' . trim($commission_id) . '%');
        }
        $dataset = $query->paginate($item_count);
        return view('party_commission_multi._list', compact('dataset'));
    }


        public function PartyNameByType(Request $r) {

        $dataset = Party::where([['is_deleted', 0], ['party_type_id', $r->partyTypeId]])->get();
        $str = "";
        if (!empty($dataset)) {
            $str .= "<option value=''>Party Name</option>";
            foreach ($dataset as $data) {
                $str .= "<option value='{$data->id}'>{$data->name}</option>";
            }
        } else {
            $str .= "<option value=''>No Party Found</option>";
        }

        return $str;
    }


        public function ProductByCategory(Request $r) {
        $dataset = Product::where([['is_deleted', 0], ['category_id', $r->productTypeId]])->get();
        $str = "";
        if (!empty($dataset)) {
            $str .= "<option value=''>product name</option>";
            foreach ($dataset as $data) {
                $str .= "<option value='{$data->id}'>{$data->name}</option>";
            }
        } else {
            $str .= "<option value=''>No Category Found</option>";
        }

        return $str;
    }
         public function UnitSizeByUnit(Request $r) {

        $dataset = UnitSize::where([['is_deleted', 0], ['unit_id', $r->UnitSizeId]])->get();
        $str = "";
        if (!empty($dataset)) {
            $str .= "<option value=''>Unit Size</option>";
            foreach ($dataset as $data) {
               
                $str .= "<option value='{$data->id}'>{$data->name}</option>";
            }
        } else {
            $str .= "<option value=''>No unit size Found</option>";
        }

        return $str;
    }


    public function DataSave(Request $r) {
        //pr($_POST);
        $input = $r->all();
        $rules = array(
            'commission' => 'required'
        );
        $messages = array(
            'commission.required' => 'Commission Required.'
        );

        $valid = Validator::make($input, $rules, $messages);
        if ($valid->fails()) {
            return redirect()->back()->withErrors($valid)->withInput();
        }

        DB::beginTransaction();
        try {
            $model = new PartyCommissionMulti();
            $model->party_type_id = $r->party_type_id;
            $model->party_id = $r->party_id;
            $model->category_id = $r->category_id;
            $model->product_id = $r->product_id;
            $model->unit_id = $r->unit_id;
            $model->unit_size_id = $r->unit_size_id;
            $model->commission = $r->commission;
            $model->created_at = cur_date_time();
            $model->created_by = Auth::id();
            $model->_key = uniqueKey();
            if (!$model->save()) {
                throw new Exception("Query problem on creating record.");
            }
              DB::commit();
            $this->response['success'] = true;
            $this->response['message'] = 'Item saved successfully.';
        } catch (Exception $e) {
            DB::rollback();
            $this->response['success'] = false;
            $this->response['message'] = $e->getMessage();
        }
        return $this->response;
    }


    public function search_itemlist(Request $r) {

        $_itemDataset = PartyCommissionMulti::where([['party_type_id', $r->party_type_id], ['party_id', $r->party_id], ['is_deleted', 0]])->get();
        $this->model['itemDataset'] = $_itemDataset;

        return view('party_commission_multi._partial_list', $this->model);
    }


     public function remove_item($id) {
        DB::beginTransaction();
        try {
            $data = PartyCommissionMulti::find($id);
            if (!$data->delete()) {
                throw new Exception("Error while removing record.");
            }
            PartyCommissionMulti::where([['party_type_id', $data->party_type_id], ['party_id', $data->party_id],['id', $data->id]])->delete();

            DB::commit();
            return redirect()->back()->with('success', 'Item removed successfully.');
        } catch (Exception $e) {
            DB::rollback();
            return redirect()->back()->with('danger', $e->getMessage());
        }
    }

    

    public function delete() {
        $resp = array();
        DB::beginTransaction();
        try {
            foreach ($_POST['data'] as $id) {
                $data = PartyCommissionMulti::find($id);
                $data->is_deleted = 1;
                $data->deleted_by = Auth::id();
                $data->deleted_at = cur_date_time();
                if (!$data->save()) {
                    throw new Exception("Error while deleting records.");
                }
            }

            DB::commit();
            $resp['success'] = true;
            $resp['message'] = 'Record has been deleted successfully.';
        } catch (Exception $e) {
            DB::rollback();
            $resp['success'] = false;
            $resp['message'] = $e->getMessage();
        }
        return $resp;
    }



}
