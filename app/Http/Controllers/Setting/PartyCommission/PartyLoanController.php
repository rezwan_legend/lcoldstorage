<?php

namespace App\Http\Controllers\Setting\PartyCommission;

use App\Http\Controllers\HomeController;
use App\Models\Setting\ProductModule\Category;
use Exception;
use Auth;
use App\Models\Rent;
use App\Models\Setting\ProductModule\Product;
use App\Models\Setting\ProductModule\UnitSize;
use App\Models\ProductType;
use App\Models\RentType;
use App\Models\Setting\ProductModule\Unit;
use App\Models\Setting\Party\PartyType;
use App\Models\Setting\Party\Party;
use App\Models\Setting\PartyCommission\PartyLoan;
use DB;
use Validator;
use Illuminate\Http\Request;

class PartyLoanController extends HomeController {

    public function index() {
        check_user_access('party_loan_list');
        $model = new PartyLoan();
        $query = $model->where('is_deleted', 0);
        $dataset = $query->paginate($this->getSettings()->pagesize);
        $_party_type = PartyType::where('is_deleted', 0)->get();
        $_party= Party::where('is_deleted', 0)->get();
        $_category = Category::where('is_deleted', 0)->get();
        $_Product = Product::where('is_deleted', 0)->get();
        $_unit = Unit::where('is_deleted', 0)->get();
        $_unit_size = UnitSize::where('is_deleted', 0)->get();


        $this->model['page_title'] = "Party Loan List";
        $this->model['dataset'] = $dataset;
        $this->model['party_type'] = $_party_type;
        $this->model['party'] = $_party;
        $this->model['category'] = $_category;
        $this->model['product'] = $_Product;
        $this->model['units'] = $_unit;
        $this->model['unitsize'] = $_unit_size;
        
        return view('party_loan.index', $this->model);
    }

    public function create() {
        check_user_access('party_loan_create');
        $_party_loan = PartyLoan::where('is_deleted',0)->get();
        $_party_type = PartyType::where('is_deleted', 0)->get();
        $_party= Party::where('is_deleted', 0)->get();
        $categories = Category::where('is_deleted', 0)->get();
        $_Product = Product::where('is_deleted', 0)->get();
        $_unit = Unit::where('is_deleted', 0)->get();
        $_unit_size = UnitSize::where('is_deleted', 0)->get();
        $this->model['page_title'] = "Party Loan Create";
        $this->model['breadcrumb_title'] = "Party Loan Create";
        $this->model['dataset'] = $_party_loan;
        $this->model['party_type'] = $_party_type;
        $this->model['party'] = $_party;
        $this->model['categories'] = $categories;
        $this->model['products'] = $_Product;
        $this->model['units'] = $_unit;
        $this->model['unitSize'] = $_unit_size;
        return view('party_loan.create', $this->model);
    }
    
    public function store(Request $r) {
        DB::beginTransaction();
        try {
            $data = [];
            $party_type_id = $r->party_type_id;
            $party_id = $r->party_id;
            $category_id = $r->category_id;
            $product_id  = $r->product_id;
            $unit_id = $r->unit_id;
            $unit_size_id = $r->unit_size_id;
            $loan = $r->loan;
            
            for($i=0; $i<count($r->category_id); $i++){
                $temp = [
                    "party_type_id" => $party_type_id[$i],
                    "party_id" => $party_id[$i],
                    "category_id" => $category_id[$i],
                    "product_id"  => $product_id[$i],
                    "unit_id"  => $unit_id[$i],
                    "unit_size_id"  => $unit_size_id[$i],
                    "loan"  => $loan[$i],
                    "created_at" => cur_date_time(),
                        "created_by" => Auth::id()
                ];

                $data[] = $temp;

            }
            $insert = PartyLoan::insert($data);
            if (!$insert) {
                throw new Exception("Something went wrong while inserting order items");
            }
            DB::commit();
                return response()->json('New Records Created Successfully.');
        } catch (Exception $error) {
            DB::rollback();
            return response()->json($error->getMessage(), $error->getCode());
        }
       
        
    }

    public function edit($id) {
        check_user_access('party_loan_edit');
        $data = PartyLoan::find($id);
        $_party_type = PartyType::where('is_deleted', 0)->get();
        $_party= Party::where('is_deleted', 0)->get();
        $categories = Category::where('is_deleted', 0)->get();
        $_Product = Product::where('is_deleted', 0)->get();
        $_unit = Unit::where('is_deleted', 0)->get();
        $_unit_size = UnitSize::where('is_deleted', 0)->get();
        $this->model['page_title'] = "Party Loan Edit";
        $this->model['breadcrumb_title'] = "Party Laon Edit";
        $this->model['party_type'] = $_party_type;
        $this->model['party'] = $_party;
        $this->model['categories'] = $categories;
        $this->model['products'] = $_Product;
        $this->model['units'] = $_unit;
        $this->model['unitSize'] = $_unit_size;
        $this->model['data'] = $data;
        return view('party_loan.edit', $this->model);
    }

    public function update(Request $r, $id) {
    //    pr($_POST);
        $input = $r->all();
        $rules = array(
            'loan' => 'required'
        );
        $messages = array(
            'loan.required' => 'Loan Required.'
        );

        $valid = Validator::make($input, $rules, $messages);
        if ($valid->fails()) {
            return redirect()->back()->withErrors($valid)->withInput();
        }

        DB::beginTransaction();
        try {
            $model = PartyLoan::find($id);
            $model->party_type_id = $r->party_type_id;
            $model->party_id = $r->party_id;
            $model->category_id = $r->category_id;
            $model->product_id = $r->product_id;
            $model->unit_id = $r->unit_id;
            $model->unit_size_id = $r->unit_size_id;
            $model->loan = $r->loan;
            $model->modified_at = cur_date_time();
            $model->modified_by = Auth::id();
            if (!$model->save()) {
                throw new Exception("Query problem on updating record.");
            }

            DB::commit();
            return redirect('/party_loan')->with('success', 'Record updated successfully.');
        } catch (Exception $e) {
            DB::rollback();
            return redirect()->back()->with('danger', $e->getMessage());
        }
    }

    public function show() {
        pr("Silence is the best.");
    }

    // Ajax Functions
    public function search(Request $r) {
    //    pr($_POST);
        $item_count = $r->input('item_count');
        $party_type_id = $r->input('party_type_id');
        $party_id = $r->input('party_id');
        $category_id = $r->input('category_id');
        $product_id = $r->input('Product_id');
        $unit_id = $r->input('unit_id');
        $unit_size_id = $r->input('unit_size_id');
        $loan = $r->input('loan');

        $query = PartyLoan::where('is_deleted', 0);
        if (!empty($party_type_id)) {
            $query->where('party_type_id', 'like', '%' . trim($party_type_id) . '%');
        }
        if (!empty($party_id)) {
            $query->where('party_id', 'like', '%' . trim($party_id) . '%');
        }
        if (!empty($category_id)) {
            $query->where('category_id', 'like', '%' . trim($category_id) . '%');
        }
        if (!empty($product_id)) {
            $query->where('product_id', 'like', '%' . trim($product_id) . '%');
        }
        if (!empty($unit_id)) {
            $query->where('unit_id', 'like', '%' . trim($unit_id) . '%');
        }
        if (!empty($unit_size_id)) {
            $query->where('unit_size_id', 'like', '%' . trim($unit_size_id) . '%');
        }
        if (!empty($loan)) {
            $query->where('loan', 'like', '%' . trim($loan) . '%');
        }
        $dataset = $query->paginate($item_count);
        return view('party_loan._list', compact('dataset'));
    }


    public function partyListByType(Request $r) {

        $dataset = Party::where([['is_deleted', 0], ['party_type_id', $r->partyTypeId]])->get();
        $str = "";
        if (!empty($dataset)) {
            $str .= "<option value=''>Party</option>";
            foreach ($dataset as $data) {
                $str .= "<option value='{$data->id}'>{$data->name}</option>";
            }
        } else {
            $str .= "<option value=''>No Party Found</option>";
        }

        return $str;
    }

    public function ProductByCategory(Request $r) {
        $dataset = Product::where([['is_deleted', 0], ['category_id', $r->productTypeId]])->get();
        $str = "";
        if (!empty($dataset)) {
            $str .= "<option value=''>product name</option>";
            foreach ($dataset as $data) {
                $str .= "<option value='{$data->id}'>{$data->name}</option>";
            }
        } else {
            $str .= "<option value=''>No Category Found</option>";
        }

        return $str;
    }
    public function UnitSizeByUnit(Request $r) {

        $dataset = UnitSize::where([['is_deleted', 0], ['unit_id', $r->UnitSizeId]])->get();
        $str = "";
        if (!empty($dataset)) {
            $str .= "<option value=''>Unit Size</option>";
            foreach ($dataset as $data) {
               
                $str .= "<option value='{$data->id}'>{$data->name}</option>";
            }
        } else {
            $str .= "<option value=''>No unit size Found</option>";
        }

        return $str;
    }

    public function delete() {
        // pr($_POST);
        $resp = array();
        DB::beginTransaction();
        try {
            foreach ($_POST['data'] as $id) {
                $data = PartyLoan::find($id);

                $data->is_deleted = 1;
                $data->deleted_by = Auth::id();
                $data->deleted_at = cur_date_time();
                if (!$data->save()) {
                    throw new Exception("Error while deleting records.");
                }
            }

            DB::commit();
            $resp['success'] = true;
            $resp['message'] = 'Record has been deleted successfully.';
        } catch (Exception $e) {
            DB::rollback();
            $resp['success'] = false;
            $resp['message'] = $e->getMessage();
        }
        return $resp;
    }
}
