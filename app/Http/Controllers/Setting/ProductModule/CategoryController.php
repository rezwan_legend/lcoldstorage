<?php

namespace App\Http\Controllers\Setting\ProductModule;

use Exception;
use Auth;
use App\Http\Controllers\HomeController;
use App\Models\Setting\ProductModule\Category;
use App\Models\Setting\ProductModule\Product;
use App\Models\Setting\ProductModule\ProductType;
use App\Models\Stock;
use DB;
use Validator;
use Illuminate\Http\Request;

class CategoryController extends HomeController {

    public function index() {
        check_user_access('category_list');
        $model = new Category();
        $query = $model->where('is_deleted', 0);
        $dataset = $query->paginate($this->getSettings()->pagesize);
        $_product_type = ProductType::where('is_deleted', 0)->get();
        $businessList = $this->userInstitutes();

        $this->model['breadcrumb_title'] = "Product Category List";
        $this->model['dataset'] = $dataset;
        $this->model['businessList'] = $businessList;
        $this->model['product_type'] = $_product_type;
        return view('category.index', $this->model);
    }

    public function create() {
        check_user_access('category_create');
        $_product_type = ProductType::where('is_deleted', 0)->get();
        $this->model['product_type'] = $_product_type;
        $this->model['breadcrumb_title'] = "Category Information";
        return view('category.create', $this->model);
    }

    public function store(Request $r) {
//        pr($_POST);
        $input = $r->all();
        $rules = array(
            'product_type' => 'required',
            'category_name' => 'required',
        );
        $messages = array(
            'product_type.required' => 'Product Type Required.',
            'category_name.required' => 'Name required.',
        );

        $valid = Validator::make($input, $rules, $messages);
        if ($valid->fails()) {
            return redirect()->back()->withErrors($valid)->withInput();
        }

        DB::beginTransaction();
        try {
            $model = new Category();
            $model->product_type_id = $r->product_type;
            $model->name = $r->category_name;
            $model->description = $r->description;
            $model->created_at = cur_date_time();
            $model->created_by = Auth::id();
            $model->_key = uniqueKey();
            if (!$model->save()) {
                throw new Exception("Query problem on creating record.");
            }

            DB::commit();
            return redirect('/category')->with('success', 'New record created successfully.');
        } catch (Exception $e) {
            DB::rollback();
            return redirect()->back()->with('danger', $e->getMessage());
        }
    }

    public function edit($id) {
        check_user_access('category_edit');
        $data = Category::find($id);
        $businessList = $this->userInstitutes();
        $_product_type = ProductType::where('is_deleted', 0)->get();
        $this->model['product_type'] = $_product_type;

        $this->model['breadcrumb_title'] = "Category Information";
        $this->model['businessList'] = $businessList;
        $this->model['data'] = $data;
        return view('category.edit', $this->model);
    }

    public function update(Request $r, $id) {
//        pr($_POST);
        $input = $r->all();
        $rules = array(
            'product_type_id' => 'required',
            'category_name' => 'required',
        );
        $messages = array(
            'product_type.required' => 'Product Type Required.',
            'category_name.required' => 'Name required.',
        );

        $valid = Validator::make($input, $rules, $messages);
        if ($valid->fails()) {
            return redirect()->back()->withErrors($valid)->withInput();
        }

        DB::beginTransaction();
        try {
            $model = Category::find($id);
            $model->product_type_id = $r->product_type_id;
            $model->name = $r->category_name;
            $model->description = $r->description;
            $model->modified_at = cur_date_time();
            $model->modified_by = Auth::id();
            if (!$model->save()) {
                throw new Exception("Query problem on updating record.");
            }

            DB::commit();
            return redirect('/category')->with('success', 'Record updated successfully.');
        } catch (Exception $e) {
            DB::rollback();
            return redirect()->back()->with('danger', $e->getMessage());
        }
    }

    public function show() {
        pr("Silence is the best.");
    }

    // Ajax Functions
    public function search(Request $r) {
//        pr($_POST);
        $item_count = $r->input('item_count');
        $product_type = $r->input('product_type_id');
        $search = $r->input('search');

        $query = Category::where('is_deleted', 0);
        if (!empty($product_type)) {
            $query->where('product_type_id', $product_type);
        }
        if (!empty($search)) {
            $query->where('name', 'like', '%' . trim($search) . '%');
        }
        $dataset = $query->paginate($item_count);
        return view('category._list', compact('dataset'));
    }

    public function delete() {
        $resp = array();
        DB::beginTransaction();
        try {
            foreach ($_POST['data'] as $id) {
                $data = Category::find($id);

                if (!empty($data->products)) {
                    foreach ($data->products as $product) {
                        $product->is_deleted = 1;
                        $product->deleted_by = Auth::id();
                        $product->deleted_at = cur_date_time();
                        if (!$product->save()) {
                            throw new Exception("Error while deleting records.");
                        }
                    }
                }

                $data->is_deleted = 1;
                $data->deleted_by = Auth::id();
                $data->deleted_at = cur_date_time();
                if (!$data->save()) {
                    throw new Exception("Error while deleting records.");
                }
            }

            DB::commit();
            $resp['success'] = true;
            $resp['message'] = 'Record has been deleted successfully.';
        } catch (Exception $e) {
            DB::rollback();
            $resp['success'] = false;
            $resp['message'] = $e->getMessage();
        }
        return $resp;
    }

    public function dropdown_by_buisness(Request $r) {
        $dataset = Category::where([['is_deleted', 0], ['business_type_id', $r->business_type]])->get();
        $str = "";
        if (!empty($dataset)) {
            $str .= "<option value=''>Category</option>";
            foreach ($dataset as $data) {
                $str .= "<option value='{$data->id}'>{$data->name}</option>";
            }
        } else {
            $str .= "<option value=''>No Category Found</option>";
        }

        return $str;
    }

    // added by Billah
    public function dropdown_by_company(Request $r) {
        $dataset = Category::where([['is_deleted', 0], ['institute_id', $this->user_companies()]])->get();
        $str = "";
        if (!empty($dataset)) {
            $str .= "<option value=''>Category</option>";
            foreach ($dataset as $data) {
                $str .= "<option value='{$data->id}'>{$data->name}</option>";
            }
        } else {
            $str .= "<option value=''>No Category Found</option>";
        }

        return $str;
    }

    public function list_category_by_buisness(Request $r) {
        $dataset = Category::where([['is_deleted', 0], ['business_type_id', $r->business_type]])->get();
        $str = "";
        if (!empty($dataset)) {
            $str .= "<option value=''>Category</option>";
            foreach ($dataset as $data) {
                $str .= "<option value='{$data->id}'>{$data->name}</option>";
            }
        } else {
            $str .= "<option value=''>No Category Found</option>";
        }

        return $str;
    }

    //added by Billah
    public function list_category_by_company(Request $r) {
        $dataset = Category::where([['is_deleted', 0], ['institute_id', $this->user_companies()]])->get();
        $str = "";
        if (!empty($dataset)) {
            $str .= "<option value=''>Category</option>";
            foreach ($dataset as $data) {
                $str .= "<option value='{$data->id}'>{$data->name}</option>";
            }
        } else {
            $str .= "<option value=''>No Category Found</option>";
        }

        return $str;
    }

    public function categoryListByType(Request $r) {

        $dataset = Category::where([['is_deleted', 0], ['product_type_id', $r->productTypeId]])->get();
        $str = "";
        if (!empty($dataset)) {
            $str .= "<option value=''>Category</option>";
            foreach ($dataset as $data) {
                $str .= "<option value='{$data->id}'>{$data->name}</option>";
            }
        } else {
            $str .= "<option value=''>No Category Found</option>";
        }

        return $str;
    }

    public function list_product_by_category(Request $r) {
        $stock = new Stock();
        $query = Product::where([['is_deleted', 0], ['category_id', $r->category]]);
        if (!empty($r->type)) {
            $query->where('type', $r->type);
        }
        $dataset = $query->get();
        $str = "";
        if (!empty($dataset)) {
            $str .= "<option value=''>Select Product</option>";
            foreach ($dataset as $data) {
                if ($r->type == RAW) {
                    $avg_weight = $stock->averageWeight($data->id);
                } else {
                    $avg_weight = !empty($data->weight) ? $data->weight : 1;
                }
                $str .= "<option data-weight='{$avg_weight}' value='{$data->id}'>{$data->name}</option>";
            }
        } else {
            $str .= "<option value=''>No Product Found</option>";
        }

        return $str;
    }

    public function list_product_type_by_company(Request $r) {
        // pr($r->institute_id);
        $query = ProductType::where('is_deleted', 0);
        $models = $query->get();
        return response()->json($models);
    }

}
