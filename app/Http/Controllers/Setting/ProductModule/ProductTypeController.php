<?php
namespace App\Http\Controllers\Setting\ProductModule;
use Auth;
use DB;
use Exception;
use Validator;

use App\Http\Controllers\HomeController;
use App\Models\Setting\ProductModule\ProductType;
use Illuminate\Http\Request;

class ProductTypeController extends HomeController {

    public function index() {
        check_user_access('product_type_list');
        $dataset = ProductType::where('is_deleted', 0)->orderBy('name', 'asc')->paginate($this->getSettings()->pagesize);

        $this->model['companyList'] = $this->userInstitutes();
        $this->model['page_title'] = "Product Type List";
        $this->model['dataset'] = $dataset;
//        pr($dataset);
        return view('product_type.index', $this->model);
    }

    public function create() {
        check_user_access('product_type_create');
        $_companyList = $this->userInstitutes();

        $this->model['page_title'] = "Product Type Information";
        $this->model['companyList'] = $_companyList;
        return view('product_type.create', $this->model);
    }

    public function store(Request $r) {
        $input = $r->all();
        $rules = array(
            'name' => 'required',
        );
        $messages = array(
            'name.required' => 'Name required.',
        );

        $valid = Validator::make($input, $rules, $messages);
        if ($valid->fails()) {
            return redirect()->back()->withErrors($valid)->withInput();
        }

        DB::beginTransaction();
        try {
            $model = new ProductType();
            $model->company_id = $r->company_id;
            $model->name = $r->name;
            $model->description = $r->description;
            $model->created_at = cur_date_time();
            $model->created_by = Auth::id();
            $model->_key = uniqueKey();
            if (!$model->save()) {
                throw new Exception("Error while saving record.");
            }
            DB::commit();
            return redirect('/product_type')->with('success', 'Record saved successfully.');
        } catch (Exception $e) {
            DB::rollback();
            return redirect()->back()->with('danger', $e->getMessage());
        }
    }

    public function edit($id) {
        check_user_access('product_type_edit');
        $data = ProductType::where('_key', $id)->first();
        $_companyList = $this->userInstitutes();

        $this->model['page_title'] = "Product Type Information";
        $this->model['data'] = $data;
        $this->model['companyList'] = $_companyList;
        return view('product_type.edit', $this->model);
    }

    public function update(Request $r, $id) {
        $input = $r->all();
        $rules = array(
            'name' => 'required',
            'description' => 'required',
        );
        $messages = array(
            'name.required' => 'Name required.',
            'description' => 'description.',
        );

        $valid = Validator::make($input, $rules, $messages);
        if ($valid->fails()) {
            return redirect()->back()->withErrors($valid)->withInput();
        }

        DB::beginTransaction();
        try {
            $model = ProductType::find($id);
            $model->name = $r->name;
            $model->description = $r->description;
            $model->modified_at = cur_date_time();
            $model->modified_by = Auth::id();
            if (!$model->save()) {
                throw new Exception("Error while updating record.");
            }

            DB::commit();
            return redirect('/product_type')->with('success', 'Record updated successfully.');
        } catch (Exception $e) {
            DB::rollback();
            return redirect()->back()->with('danger', $e->getMessage());
        }
    }

    // Ajax Functions
    public function search(Request $r) {
        $item_count = !empty($r->item_count) ? $r->item_count : $this->getSettings()->pagesize;
        $srch = $r->srch;
        $_businessId = $r->company_id;

        $query = ProductType::where('is_deleted', 0);
       
        if (!empty($srch)) {
            $query->where('name', 'like', '%' . $srch . '%');
        }
        $query->orderBy('name', 'asc');
        $dataset = $query->paginate($item_count);

        $this->model['dataset'] = $dataset;
        return view('product_type._list', $this->model);
    }

    public function delete() {
        $resp = array();
        DB::beginTransaction();
        try {
            foreach ($_POST['data'] as $id) {
                $data = ProductType::find($id);
                $data->is_deleted = 1;
                $data->deleted_by = Auth::id();
                $data->deleted_at = cur_date_time();
                if (!$data->save()) {
                    throw new Exception("Error while deleting records.");
                }
            }
            DB::commit();
            $resp['success'] = true;
            $resp['message'] = 'Record has been deleted successfully.';
        } catch (Exception $e) {
            DB::rollback();
            $resp['success'] = false;
            $resp['message'] = $e->getMessage();
        }
        return $resp;
    }

    public function productTypeByBusiness(Request $r) {
        $dataset = ProductType::where([['is_deleted', 0], ['company_id', $r->businessId]])->get();
        $str = "";
        if (!empty($dataset)) {
            $str .= "<option value=''>All Product</option>";
            foreach ($dataset as $data) {
                $str .= "<option value='{$data->id}'>{$data->name}</option>";
            }
        } else {
            $str .= "<option value=''>No Found</option>";
        }

        return $str;
    }

}
