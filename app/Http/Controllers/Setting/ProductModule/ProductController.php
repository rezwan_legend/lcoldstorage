<?php

namespace App\Http\Controllers\Setting\ProductModule;
use App\Http\Controllers\HomeController;
use Auth;
use DB;
use Exception;
use Validator;
use Illuminate\Http\Request;
use App\Models\Setting\ProductModule\ProductType;
use App\Models\Setting\ProductModule\Category;
use App\Models\Setting\ProductModule\Product;
use App\Models\PriceSetting;

class ProductController extends HomeController {

    public function index() {
        check_user_access('product_list');
        $model = new Product();
        $query = $model->where('is_deleted', 0)->orderBy('id', 'dsc');
        $dataset = $query->paginate($this->getSettings()->pagesize);
        $categories = Category::where('is_deleted', 0)->get();
        $_product_type = ProductType::where('is_deleted', 0)->get();

        $this->model['breadcrumb_title'] = "Product List";
        $this->model['dataset'] = $dataset;
        $this->model['product_type'] = $_product_type;
        $this->model['categories'] = $categories;
        return view('product.index', $this->model);
    }

    public function create() {
        check_user_access('product_create');
        $businesses = $this->userInstitutes();
        $categories = Category::where('is_deleted', 0)->get();

        $this->model['breadcrumb_title'] = "Product Information";
        $this->model['businesses'] = $businesses;
        $this->model['categories'] = $categories;
        return view('product.create', $this->model);
    }

    public function store(Request $r) {
        $input = $r->all();
        $rules = array(
            'category_id' => 'required',
            'name' => 'required |unique:product',
        );
        $messages = array(
            'category_id.required' => 'Please select categoty.',
            'name.required' => 'Name required.',
            'name.unique'=>'This name already exist',
        );

        $valid = Validator::make($input, $rules, $messages);
        if ($valid->fails()) {
            return redirect()->back()->withErrors($valid)->withInput();
        }

        DB::beginTransaction();
        try {
            $model = new Product();
            $model->category_id = $r->category_id;
            $model->product_type_id = Category::find($r->category_id)->product_type_id;
            $model->name = $r->name;
            $model->product_code = $r->product_code;
            $model->description = $r->description;
            $model->created_at = cur_date_time();
            $model->created_by = Auth::id();
            $model->_key = uniqueKey();
            if (!$model->save()) {
                throw new Exception("Query problem on creating record.");
            }

            DB::commit();
            return redirect('/products')->with('success', 'New record created successfully.');
        } catch (Exception $e) {
            DB::rollback();
            return redirect()->back()->with('danger', $e->getMessage());
        }
    }

    public function edit($id) {
        check_user_access('product_edit');
        $_id = ($id);
        $data = Product::find($_id);
        $types = ProductType::where('is_deleted', 0)->orderBy('name', 'asc')->get();
        $categories = Category::where([['is_deleted', 0], ['company_id', $data->company_id]])->get();

        $this->model['breadcrumb_title'] = "Product Information";
        $this->model['types'] = $types;
        $this->model['categories'] = $categories;
        $this->model['data'] = $data;
        return view('product.edit', $this->model);
    }

    public function update(Request $r, $id) {
        //pr($_POST);
        $input = $r->all();
        $rules = array(
            'category_id' => 'required',
            'name' => 'required',
        );
        $messages = array(
            'category_id.required' => 'Please select categoty.',
            'name.required' => 'Name required.',
        );

        $valid = Validator::make($input, $rules, $messages);
        if ($valid->fails()) {
            return redirect()->back()->withErrors($valid)->withInput();
        }

        DB::beginTransaction();
        try {
            $model = Product::find($id);
            $model->category_id = $r->category_id;
            $model->product_type_id = $r->product_type;
            $model->name = $r->name;
            $model->product_code = $r->product_code;
            $model->description = $r->description;
            $model->cost_analysis = $r->cost_analysis;
            $model->modified_at = cur_date_time();
            $model->modified_by = Auth::id();
            if (!$model->save()) {
                throw new Exception("Query problem on updating record.");
            }

            DB::commit();
            return redirect('/products')->with('success', 'Record updated successfully.');
        } catch (Exception $e) {
            DB::rollback();
            return redirect()->back()->with('danger', $e->getMessage());
        }
    }

    public function show() {
        //silent is better than grubhub
    }

    // Ajax Functions
    public function search(Request $r) {
        //pr($_POST);
        $item_count = $r->item_count;
        $_type = $r->product_type_id;
        $_category = $r->category_id;
        $search = $r->search;

        $query = Product::where('is_deleted', 0);
        if (!empty($_type)) {
              $query->where('product_type_id', 'like', '%' . trim($_type) . '%');
        }
        if (!empty($_category)) {
              $query->where('category_id', 'like', '%' . trim($_category) . '%');
        }
        if (!empty($search)) {
            $query->where('name', 'like', '%' . trim($search) . '%');
        }
        $query->orderBy('id', 'dsc');
        $dataset = $query->paginate($item_count);
        return view('product._list', compact('dataset'));
    }

    public function delete() {
        //pr($_POST);
        $resp = array();
        DB::beginTransaction();
        try {
            foreach ($_POST['data'] as $id) {
                $data = Product::find($id);
                $data->is_deleted = 1;
                $data->deleted_by = Auth::id();
                $data->deleted_at = cur_date_time();
                if (!$data->save()) {
                    throw new Exception("Error while deleting records.");
                }
            }

            DB::commit();
            $resp['success'] = true;
            $resp['message'] = 'Record has been deleted successfully.';
        } catch (Exception $e) {
            DB::rollback();
            $resp['success'] = false;
            $resp['message'] = $e->getMessage();
        }
        return $resp;
    }

    public function dropdown_by_category(Request $r) {
//        pr($r);
        $query = Product::where([['is_deleted', 0], ['category_id', $r->category]]);
        if (!empty($r->type)) {
            $query->where('type', $r->type);
        }
        $dataset = $query->get();
        $str = "";
        if (!empty($dataset)) {
            $str .= "<option value=''>Select Product</option>";
            foreach ($dataset as $data) {
                $str .= "<option value='{$data->id}'>{$data->name}</option>";
            }
        } else {
            $str .= "<option value=''>No Product Found</option>";
        }

        return $str;
    }

    public function weightBysize(Request $r) {
        $_set_prices = PriceSetting::where([['product_type_id', $r->prod_type], ['category_id', $r->category_id], ['product_id', $r->product_id], ['size_id', $r->id], ['is_deleted', 0]])->where('institute_id', MAIN_INSTITUTE)->orderBy('date', 'DESC')->first();
        $this->response['lst_costP'] = !empty($_set_prices->cost_price) ? $_set_prices->cost_price : NULL;
        $this->response['lst_saleP'] = !empty($_set_prices->sale_price) ? $_set_prices->sale_price : NULL;
        $this->response['default_weight'] = !empty($default_weight) ? $default_weight->weight : NULL;
        return $this->response;
    }

    public function ProductByCategory(Request $r) {
        $dataset = Product::where([['company_id', $r->companyId], ['product_type_id', $r->productType], ['category_id', $r->categoryId], ['is_deleted', 0]])->get();
        $str = "";
        if (!empty($dataset)) {
            $str .= "<option value=''>All Product</option>";
            foreach ($dataset as $data) {
                $str .= "<option value='{$data->id}'>{$data->name}</option>";
            }
        } else {
            $str .= "<option value=''>No Product Found</option>";
        }

        return $str;
    }

    public function data_by_type(Request $r) {
        $dataset = Product::where([['company_id', $r->company], ['product_type_id', $r->product_type], ['is_deleted', 0]])->get();
        return view('product._partial_data', compact('dataset'));
    }

}
