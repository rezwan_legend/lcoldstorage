<?php

namespace App\Http\Controllers\Setting\ProductModule;
use App\Http\Controllers\HomeController;
use Auth;
use DB;
use Exception;
use Validator;
use Illuminate\Http\Request;
use App\Models\Setting\ProductModule\ProductType;
use App\Models\Setting\ProductModule\Unit;
use App\Models\Setting\ProductModule\UnitSize;

class UnitSizeController extends HomeController {

    public function index() {
        check_user_access('unit_size_list');
        $query = UnitSize::where('is_deleted', 0);
        $dataset = $query->paginate($this->getSettings()->pagesize);
        $_unit = Unit::where('is_deleted', 0)->get();
        
        $this->model['page_title'] = "Unit Size List";
        $this->model['businessList'] = $this->userInstitutes();
        $this->model['dataset'] = $dataset;
        $this->model['units'] = $_unit;
        return view('unit_size.index', $this->model);
    }

    public function create() {
        check_user_access('unit_size_create');
        $_product_type = ProductType::where('is_deleted', 0)->get();
        $_unit = Unit::where('is_deleted', 0)->get();
        $this->model['page_title'] = "Unit Size Information";
        $this->model['product_type'] = $_product_type;
        $this->model['units'] = $_unit;
        return view('unit_size.create', $this->model);
    }

    public function store(Request $r) {
        //pr($_POST);
        $input = $r->all();
        $rules = array(
            'name' => 'required|unique:unit_size',
            'weight' => 'required',
        );
        $messages = array(
            'name.required' => 'Name required.',
            'weight.required' => 'weight required.',
            'name.unique' => 'This Size Already exist.',
        );

        $valid = Validator::make($input, $rules, $messages);
        if ($valid->fails()) {
            return redirect()->back()->withErrors($valid)->withInput();
        }

        DB::beginTransaction();
        try {
            $model = new UnitSize();
            $model->unit_id = $r->unit_id;
            $model->name = $r->name;
            $model->weight = $r->weight;
            $model->created_at = cur_date_time();
            $model->created_by = Auth::id();
            $model->_key = uniqueKey();
            if (!$model->save()) {
                throw new Exception("Query problem on creating record.");
            }

            DB::commit();
            return redirect('/unit_size')->with('success', 'New record created successfully.');
        } catch (Exception $e) {
            DB::rollback();
            return redirect()->back()->with('danger', $e->getMessage());
        }
    }

    public function edit($id) {
        check_user_access('unit_size_edit');
        $data = UnitSize::where('_key', $id)->first();
        $_unit = Unit::where('is_deleted', 0)->get();

        $this->model['page_title'] = "Update Unit Information";
        $this->model['data'] = $data;
        $this->model['units'] = $_unit;
        return view('unit_size.edit', $this->model);
    }

    public function update(Request $r, $id) {
        //pr($_POST);
        $input = $r->all();
        $rules = array(
            'name' => 'required',
            'weight' => 'required',
        );
        $messages = array(
            'name.required' => 'Name required.',
            'weight.required' => 'weight required.',
        );

        $valid = Validator::make($input, $rules, $messages);
        if ($valid->fails()) {
            return redirect()->back()->withErrors($valid)->withInput();
        }

        DB::beginTransaction();
        try {
            $model = UnitSize::find($id);
            $model->unit_id = $r->unit_id;
            $model->name = $r->name;
            $model->weight = $r->weight;
            $model->modified_at = cur_date_time();
            $model->modified_by = Auth::id();
            if (!$model->save()) {
                throw new Exception("Query problem on updating record.");
            }

            DB::commit();
            return redirect('/unit_size')->with('success', 'Record updated successfully.');
        } catch (Exception $e) {
            DB::rollback();
            return redirect()->back()->with('danger', $e->getMessage());
        }
    }

    public function show() {
        pr("Silence is the best.");
    }

    // Ajax Functions
    public function search(Request $r) {
        //pr($_POST);
        $item_count = !empty($r->item_count) ? $r->item_count : $this->getSettings()->pagesize;

        $query = UnitSize::where('is_deleted', 0);
        if (!empty($r->unit_id)) {
            $query->where('unit_id', 'like', '%' . trim($r->unit_id) . '%');
        }
        if (!empty($r->srch)) {
            $query->where('name', 'like', '%' . trim($r->srch) . '%')->orWhere('weight', 'like', '%' . trim($r->srch) . '%');
        }
        $query->orderBy('id', 'asc');
        $dataset = $query->paginate($item_count);

        $this->model['dataset'] = $dataset;
        return view('unit_size._list', $this->model);
    }

    public function delete() {
        DB::beginTransaction();
        try {
            foreach ($_POST['data'] as $id) {
                $data = UnitSize::find($id);
                $data->is_deleted = 1;
                $data->deleted_at = cur_date_time();
                $data->deleted_by = Auth::id();
                if (!$data->save()) {
                    throw new Exception("Error while deleting records.");
                }
            }

            DB::commit();
            $this->response['success'] = true;
            $this->response['message'] = 'Record has been deleted successfully.';
        } catch (Exception $e) {
            DB::rollback();
            $this->response['success'] = false;
            $this->response['message'] = $e->getMessage();
        }
        return $this->response;
    }
}
