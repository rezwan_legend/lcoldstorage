<?php

namespace App\Http\Controllers\Setting\ProductModule;
use DB;use App\Http\Controllers\HomeController;
use Exception;
use Validator;
use App\Models\Setting\ProductModule\Unit;
use Illuminate\Http\Request;
use Auth;

class UnitController extends HomeController {

    public function index() {
        check_user_access('unit');
         $dataset = unit::where('is_deleted', 0)->get();
         $this->model['page_title'] = "Unit";
         $this->model['dataset'] = $dataset;
        //   pr($dataset);
        return view('unit.index',$this->model);
    }

    public function create() {
        check_user_access('unit_create');

        $this->model['page_title'] = "Unit";
        return view('unit.create', $this->model);
    }

    public function store(Request $r) {
        $input = $r->all();
        $rules = array(
            'unit' => 'required',
        );
        $messages = array(
            'unit.required' => 'Unit required.',
        );

        $valid = Validator::make($input, $rules, $messages);
        if ($valid->fails()) {
            return redirect()->back()->withErrors($valid)->withInput();
        }

        DB::beginTransaction();
        try {
            $model = new unit();
            $model->unit = $r->unit;
            $model->short_name = $r->short_name;
            $model->created_at = cur_date_time();
            $model->created_by = Auth::id();
            $model->_key = uniqueKey();
            if (!$model->save()) {
                throw new Exception("Error while saving record.");
            }
            DB::commit();
            return redirect('/unit')->with('success', 'Record saved successfully.');
        } catch (Exception $e) {
            DB::rollback();
            return redirect()->back()->with('danger', $e->getMessage());
        }
    }

    public function edit($id) {
        check_user_access('unit_edit');
        $data = unit::where('_key', $id)->first();
        $_companyList = $this->userInstitutes();

        $this->model['page_title'] = "unit Information";
        $this->model['data'] = $data;
        return view('unit.edit', $this->model);
    }

    public function update(Request $r, $id) {
        $input = $r->all();
        $rules = array(
            'unit' => 'required',

        );
        $messages = array(
            'unit.required' => 'Unit required.',

        );

        $valid = Validator::make($input, $rules, $messages);
        if ($valid->fails()) {
            return redirect()->back()->withErrors($valid)->withInput();
        }

        DB::beginTransaction();
        try {
            $model = unit::find($id);
            $model->unit = $r->unit;
            $model->short_name = $r->short_name;
            $model->modified_at = cur_date_time();
            $model->modified_by= Auth::id();
            if (!$model->save()) {
                throw new Exception("Error while updating record.");
            }

            DB::commit();
            return redirect('/unit')->with('success', 'Record updated successfully.');
        } catch (Exception $e) {
            DB::rollback();
            return redirect()->back()->with('danger', $e->getMessage());
        }
    }

    // Ajax Functions
    public function search(Request $r) {
        $item_count = $r->item_count;
        $srch = $r->srch;
        $query = unit::where('is_deleted', 0);
        if (!empty($srch)) {
            $query->where('unit', 'like', '%' . $srch . '%');
        }
        $query->orderBy('id', 'asc');
        $dataset = $query->paginate($item_count);

        $this->model['dataset'] = $dataset;
        return view('unit._list', $this->model);
    }

    public function delete() {
        $resp = array();
        DB::beginTransaction();
        try {
            foreach ($_POST['data'] as $id) {
                $data = unit::find($id);
                $data->is_deleted = 1;
                $data->deleted_by = Auth::id();
                $data->deleted_at = cur_date_time();
                if (!$data->save()) {
                    throw new Exception("Error while deleting records.");
                }
            }
            DB::commit();
            $resp['success'] = true;
            $resp['message'] = 'Record has been deleted successfully.';
        } catch (Exception $e) {
            DB::rollback();
            $resp['success'] = false;
            $resp['message'] = $e->getMessage();
        }
        return $resp;
    }

    public function productTypeByBusiness(Request $r) {
        $dataset = unit::where([['is_deleted', 0], ['company_id', $r->businessId]])->get();
        $str = "";
        if (!empty($dataset)) {
            $str .= "<option value=''>All Unit</option>";
            foreach ($dataset as $data) {
                $str .= "<option value='{$data->id}'>{$data->name}</option>";
            }
        } else {
            $str .= "<option value=''>No Found</option>";
        }

        return $str;
    }

}
