<?php

namespace App\Http\Controllers\Setting\LocationSetting;

use Auth;
use DB;
use Exception;
use Validator;
use App\Http\Controllers\HomeController;
use App\Models\Setting\LocationSetting\ChamberSetting;
use Illuminate\Http\Request;

class ChamberSettingController extends HomeController {

    public function index() {
        check_user_access('chamber_setting_list');
        $dataset = ChamberSetting::where('is_deleted', 0)->orderBy('chamber', 'asc')->paginate($this->getSettings()->pagesize);

        $this->model['companyList'] = $this->userInstitutes();
        $this->model['page_title'] = "Chamber Setting List";
        $this->model['dataset'] = $dataset;
//        pr($dataset);
        return view('chamber_setting.index', $this->model);
    }

    public function create() {
        check_user_access('chamber_setting_create');
        $_companyList = $this->userInstitutes();

        $this->model['page_title'] = "Chamber Setting Information";
        $this->model['companyList'] = $_companyList;
        return view('chamber_setting.create', $this->model);
    }

    public function store(Request $r) {
        $input = $r->all();
        $rules = array(
            'chamber' => 'required',
            'capacity' => 'required',
        );
        $messages = array(
            'chamber.required' => 'Chamber required.',
            'capacity.required' => 'Chamber Capacity required.',
        );

        $valid = Validator::make($input, $rules, $messages);
        if ($valid->fails()) {
            return redirect()->back()->withErrors($valid)->withInput();
        }

        DB::beginTransaction();
        try {
            $model = new ChamberSetting();
            $model->chamber = $r->chamber;
            $model->capacity = $r->capacity;
            $model->created_at = cur_date_time();
            $model->created_by = Auth::id();
            $model->_key = uniqueKey();
            if (!$model->save()) {
                throw new Exception("Error while saving record.");
            }
            DB::commit();
            return redirect('/chamber_setting')->with('success', 'Record saved successfully.');
        } catch (Exception $e) {
            DB::rollback();
            return redirect()->back()->with('danger', $e->getMessage());
        }
    }

    public function edit($id) {
        check_user_access('chamber_setting_edit');
        $data = ChamberSetting::where('_key', $id)->first();
        $_companyList = $this->userInstitutes();

        $this->model['page_title'] = "Chamber Setting Edit";
        $this->model['data'] = $data;
        $this->model['companyList'] = $_companyList;
        return view('chamber_setting.edit', $this->model);
    }

    public function update(Request $r, $id) {
        $input = $r->all();
        $rules = array(
            'chamber' => 'required',
            'capacity' => 'required',
        );
        $messages = array(
            'chamber.required' => 'Chamber required.',
            'capacity.required' => 'Chamber Capacity required.',
        );

        $valid = Validator::make($input, $rules, $messages);
        if ($valid->fails()) {
            return redirect()->back()->withErrors($valid)->withInput();
        }

        DB::beginTransaction();
        try {
            $model = ChamberSetting::find($id);
            $model->chamber = $r->chamber;
            $model->capacity = $r->capacity;
            $model->modified_at = cur_date_time();
            $model->modified_by = Auth::id();
            if (!$model->save()) {
                throw new Exception("Error while updating record.");
            }

            DB::commit();
            return redirect('/chamber_setting')->with('success', 'Record updated successfully.');
        } catch (Exception $e) {
            DB::rollback();
            return redirect()->back()->with('danger', $e->getMessage());
        }
    }

    // Ajax Functions
    public function search(Request $r) {
        $item_count = !empty($r->item_count) ? $r->item_count : $this->getSettings()->pagesize;
        $srch = $r->srch;

        $query = ChamberSetting::where('is_deleted', 0);
        if (!empty($srch)) {
            $query->where('chamber', 'like', '%' . $srch . '%');
        }
        $query->orderBy('chamber', 'asc');
        $dataset = $query->paginate($item_count);

        $this->model['dataset'] = $dataset;
        return view('chamber_setting._list', $this->model);
    }
    
    public function delete() {
        $resp = array();
        DB::beginTransaction();
        try {
            foreach ($_POST['data'] as $id) {
                $data = ChamberSetting::find($id);
                $data->is_deleted = 1;
                $data->deleted_by = Auth::id();
                $data->deleted_at = cur_date_time();
                if (!$data->save()) {
                    throw new Exception("Error while deleting records.");
                }
            }
            DB::commit();
            $resp['success'] = true;
            $resp['message'] = 'Record has been deleted successfully.';
        } catch (Exception $e) {
            DB::rollback();
            $resp['success'] = false;
            $resp['message'] = $e->getMessage();
        }
        return $resp;
    }

}
