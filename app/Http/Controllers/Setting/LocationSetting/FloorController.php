<?php

namespace App\Http\Controllers\Setting\LocationSetting;


use Auth;
use DB;
use Exception;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\HomeController;
use App\Models\Setting\LocationSetting\Floor;
use App\Models\Account\Transaction;
use App\Models\Setting\LocationSetting\ChamberSetting;

class FloorController extends HomeController {

    public function index() {
        check_user_access('floor_list');
        $model = new Floor();
        $query = $model->where('is_deleted', 0);
        // $query->orWhere('common', COMMON_YES);
        $_chamber = ChamberSetting::where('is_deleted', 0)->get();
        $query->orderBy('name', 'ASC');
        $dataset = $query->paginate($this->getSettings()->pagesize);

        $this->model['page_title'] = "Floor List";
        $this->model['dataset'] = $dataset;
        $this->model['chambers'] = $_chamber;
        return view('floor.index', $this->model);
    }

    public function create() {
        check_user_access('floor_create');
        $_floors = Floor::where('is_deleted', 0)->get();
        $_chamber = ChamberSetting::where('is_deleted', 0)->get();

        $this->model['page_title'] = "Floor Information";
        $this->model['dataset'] = $_floors;
        $this->model['chambers'] = $_chamber;
        return view('floor.create', $this->model);
    }

    public function store(Request $r) {
        // pr($_POST);
        DB::beginTransaction();
        try {
            foreach ($_POST['name'] as $key => $value) {

                if (empty($_POST['name'][$key])) {
                    throw new Exception("Floor Name is Required");
                }
                $model = new Floor();
                $model->chamber_id = $r->chamber_id;
                $model->name = $r->name[$key];
                $model->capacity = $r->capacity[$key];
                $model->created_at = cur_date_time();
                $model->created_by = Auth::id();
                $model->_key = uniqueKey() . $key;
                if (!$model->save()) {
                    throw new Exception("Error while Creating Records.");
                }
            }

            DB::commit();
            return redirect('/floor_setting')->with('success', 'New Record Created Successfully.');
        } catch (Exception $e) {
            DB::rollback();
            return redirect()->back()->with('danger', $e->getMessage());
        }
    }

    public function edit($id) {
        check_user_access('floor_edit');
        $data = Floor::where('_key', $id)->first();
        $_chamber = ChamberSetting::where('is_deleted', 0)->get();

        $this->model['page_title'] = "Floor Information";
        $this->model['data'] = $data;
        $this->model['chambers'] = $_chamber;
        return view('floor.edit', $this->model);
    }

    public function update(Request $r, $id) {
        // pr($_POST);
        $input = $r->all();
        $rule = array(
            'name' => 'required',
            'capacity' => 'required',
        );
        $messages = array(
            'capacity.required' => 'Name Should not be empty.',
            'capacity.required' => 'Capacity Should not be empty.',
        );

        $valid = Validator::make($input, $rule, $messages);
        if ($valid->fails()) {
            return redirect()->back()->withErrors($valid)->withInput();
        }

        DB::beginTransaction();
        try {
            $data = Floor::find($id);
            $data->chamber_id = $r->chamber_id;
            $data->name = $r->name;
            $data->capacity = $r->capacity;
            $data->modified_at = cur_date_time();
            $data->modified_by = Auth::id();
            if (!$data->save()) {
                throw new Exception("Query Problem on Updating Record.");
            }

            DB::commit();
            return redirect('/floor_setting')->with('success', 'Record Updated Successfully.');
        } catch (Exception $e) {
            DB::rollback();
            return redirect()->back()->with('danger', $e->getMessage());
        }
    }

    public function show($id) {
        $dataset = Head::where('_key', $id)->first();
//        pr($dataset);
        $this->model['page_title'] = "Head Information";
        $this->model['dataset'] = $dataset;
        return view('account.head.ledger', $this->model);
    }

    // Ajax Functions
    public function search(Request $r) {
        //pr($_POST);
        $item_count = !empty($r->item_count) ? $r->item_count : $this->getSettings()->pagesize;
        $search = $r->search;
        $sort_by = $r->sort_by;
        $sort_type = $r->sort_type;
        $chamber_id = $r->chamber_id;
        // $tmodel = new Transaction();

        $model = new Floor();
        $query = $model->where('is_deleted', 0);
//        $query->whereIn('institute_id', $this->user_companies())->orWhere('common', COMMON_YES);
        // $query->whereIn('institute_id', $this->user_companies());
        // if (!empty($institute)) {
        //     $query->where('institute_id', $institute);
        // }
        if (!empty($chamber_id)) {
            $query->where('chamber_id', 'like', '%'. $chamber_id . '%');
        }
        if (!empty($search)) {
            $query->where('name', 'like', '%' . $search . '%')->orWhere('capacity', 'like', '%' . $search . '%');
        }
        $query->orderBy($sort_by, $sort_type);
        $dataset = $query->paginate($item_count);

        $this->model['dataset'] = $dataset;
        // $this->model['tmodel'] = $tmodel;
        return view('floor._list', $this->model);
    }

    public function delete() {
        //pr($_POST);
        $tmodel = new Transaction();

        DB::beginTransaction();
        try {
            foreach ($_POST['data'] as $id) {
                $data = Floor::find($id);
                $data->is_deleted = 1;
                $data->deleted_by = Auth::id();
                $data->deleted_at = cur_date_time();
                if (!$data->save()) {
                    throw new Exception("Error while deleting records.");
                }
            }

            DB::commit();
            $this->response['success'] = true;
            $this->response['message'] = 'Record/s deleted successfully.';
        } catch (Exception $e) {
            DB::rollback();
            $this->response['success'] = false;
            $this->response['message'] = $e->getMessage();
        }
        return $this->response;
    }

    public function finalFloor(Request $r){
        dd($r);
        $_floors = Floor::where('is_deleted', 0)->update(['current_create_id', 0]);
        return back();
    }
}
