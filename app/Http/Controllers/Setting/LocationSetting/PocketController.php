<?php

namespace App\Http\Controllers\Setting\LocationSetting;


use Auth;
use DB;
use Exception;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\HomeController;
use App\Models\Setting\LocationSetting\Pocket;
use App\Models\Account\Transaction;
use App\Models\Setting\LocationSetting\ChamberSetting;
use App\Models\Setting\LocationSetting\Floor;

class PocketController extends HomeController {

    public function index() {
        check_user_access('floor_list');
        $model = new Pocket();
        $query = $model->where('is_deleted', 0);
        $_chamber = ChamberSetting::with('floors')->where('is_deleted', 0)->get();
        
        $_floor = Floor::with(['pockets'=>function($query){
            $query->where('is_deleted',0);
        }])->where('is_deleted', 0)->get();

        $query->orderBy('name', 'ASC');
        $dataset = $query->paginate($this->getSettings()->pagesize);

        $this->model['page_title'] = "Floor List";
        $this->model['dataset'] = $dataset;
        $this->model['chambers'] = $_chamber;
        $this->model['floors'] = $_floor;
        return view('pocket.index', $this->model);
    }

    public function create() {
        check_user_access('pocket_create');
        $_pocket = Pocket::where('is_deleted', 0)->get();
        $_chamber = ChamberSetting::where('is_deleted', 0)->get();
        $_floor = Floor::where('is_deleted', 0)->get();

        $this->model['page_title'] = "Pocket Information";
        $this->model['dataset'] = $_pocket;
        $this->model['chambers'] = $_chamber;
        $this->model['floors'] = $_floor;
        return view('pocket.create', $this->model);
    }

    public function store(Request $r) {
        // pr($_POST);
        DB::beginTransaction();
        try {
            foreach ($_POST['name'] as $key => $value) {

                if (empty($_POST['name'][$key])) {
                    throw new Exception("Floor Name is Required");
                }
                $model = new Pocket();
                $model->chamber_id = $r->chamber_id;
                $model->floor_id = $r->floor_id;
                $model->name = $r->name[$key];
                $model->capacity = $r->capacity[$key];
                $model->created_at = cur_date_time();
                $model->created_by = Auth::id();
                $model->_key = uniqueKey() . $key;
                if (!$model->save()) {
                    throw new Exception("Error while Creating Records.");
                }
            }

            DB::commit();
            return redirect('/pocket_setting')->with('success', 'New Record Created Successfully.');
        } catch (Exception $e) {
            DB::rollback();
            return redirect()->back()->with('danger', $e->getMessage());
        }
    }

    public function edit($id) {
        check_user_access('pocket_edit');
        $data = Pocket::where('_key', $id)->first();
        $_chamber = ChamberSetting::where('is_deleted', 0)->get();
        $_floor = Floor::where('is_deleted', 0)->get();

        $this->model['page_title'] = "Pocket Information";
        $this->model['data'] = $data;
        $this->model['chambers'] = $_chamber;
        $this->model['floors'] = $_floor;
        return view('pocket.edit', $this->model);
    }

    public function update(Request $r, $id) {
        // pr($_POST);
        $input = $r->all();
        $rule = array(
            'name' => 'required',
            'capacity' => 'required',
        );
        $messages = array(
            'capacity.required' => 'Name Should not be empty.',
            'capacity.required' => 'Capacity Should not be empty.',
        );

        $valid = Validator::make($input, $rule, $messages);
        if ($valid->fails()) {
            return redirect()->back()->withErrors($valid)->withInput();
        }

        DB::beginTransaction();
        try {
            $data = Pocket::find($id);
            $data->chamber_id = $r->chamber_id;
            $data->floor_id = $r->floor_id;
            $data->name = $r->name;
            $data->capacity = $r->capacity;
            $data->modified_at = cur_date_time();
            $data->modified_by = Auth::id();
            if (!$data->save()) {
                throw new Exception("Query Problem on Updating Record.");
            }

            DB::commit();
            return redirect('/pocket_setting')->with('success', 'Record Updated Successfully.');
        } catch (Exception $e) {
            DB::rollback();
            return redirect()->back()->with('danger', $e->getMessage());
        }
    }

    public function show($id) {
        return "Hello i am from show";
    }

    // Ajax Functions
    public function search(Request $r) {
        //pr($_POST);
        $item_count = !empty($r->item_count) ? $r->item_count : $this->getSettings()->pagesize;
        $search = $r->search;
        $sort_by = $r->sort_by;
        $sort_type = $r->sort_type;
        $chamber_id = $r->chamber_id;
        $floor_id = $r->floor_id;

        $model = new Pocket();
        $query = $model->where('is_deleted', 0);
        if (!empty($chamber_id)) {
            $query->where('chamber_id', 'like', '%'. $chamber_id . '%');
        }
        if (!empty($floor_id)) {
            $query->where('floor_id', 'like', '%'. $floor_id . '%');
        }
        if (!empty($search)) {
            $query->where('name', 'like', '%' . $search . '%')->orWhere('capacity', 'like', '%' . $search . '%');
        }
        
        $query->orderBy($sort_by, $sort_type);
        $dataset = $query->paginate($item_count);

        $this->model['dataset'] = $dataset;
        // $this->model['tmodel'] = $tmodel;
        return view('pocket._list', $this->model);
    }

    public function delete(Request $request) {
        
        DB::beginTransaction();
        try {
            
                $data = Pocket::find($request->id);
                $data->is_deleted = 1;
                $data->deleted_by = Auth::id();
                $data->deleted_at = cur_date_time();
                if (!$data->save()) {
                    throw new Exception("Error while deleting records.");
                }
            

            DB::commit();
            $this->response['success'] = true;
            $this->response['message'] = 'Record/s deleted successfully.';
        } catch (Exception $e) {
            DB::rollback();
            $this->response['success'] = false;
            $this->response['message'] = $e->getMessage();
        }
        return $this->response;
    }

    public function floorListByChamber(Request $r) {

        $dataset = Floor::where([['is_deleted', 0], ['chamber_id', $r->chamber_id]])->get();
        $str = "";
        if (!empty($dataset)) {
            $str .= "<option value=''>Select Floor</option>";
            foreach ($dataset as $data) {
                $str .= "<option value='{$data->id}'>{$data->name}</option>";
            }
        } else {
            $str .= "<option value=''>No Party Found</option>";
        }

        return $str;
    }
}
