<?php

namespace App\Http\Controllers\Setting\Party;

use Auth;
use DB;
use Exception;
use Validator;
use App\Http\Controllers\HomeController;
use App\Models\Setting\Party\PartyType;
use Illuminate\Http\Request;

class PartyTypeController extends HomeController {

    public function index() {
        check_user_access('party_type_list');
        $dataset = PartyType::where('is_deleted', 0)->orderBy('party_type', 'asc')->paginate($this->getSettings()->pagesize);

        $this->model['companyList'] = $this->userInstitutes();
        $this->model['page_title'] = "Party Type List";
        $this->model['dataset'] = $dataset;
//        pr($dataset);
        return view('party_type.index', $this->model);
    }

    public function create() {
        check_user_access('party_type_create');
        $_companyList = $this->userInstitutes();

        $this->model['page_title'] = "Party Type Information";
        $this->model['companyList'] = $_companyList;
        return view('party_type.create', $this->model);
    }

    public function store(Request $r) {
        $input = $r->all();
        $rules = array(
            'party_type' => 'required'
        );
        $messages = array(
            'party_type.required' => 'Party Type required.'
        );

        $valid = Validator::make($input, $rules, $messages);
        if ($valid->fails()) {
            return redirect()->back()->withErrors($valid)->withInput();
        }

        DB::beginTransaction();
        try {
            $model = new PartyType();
            $model->party_type = $r->party_type;
            $model->created_at = cur_date_time();
            $model->created_by = Auth::id();
            $model->_key = uniqueKey();
            if (!$model->save()) {
                throw new Exception("Error while saving record.");
            }
            DB::commit();
            return redirect('/party_type')->with('success', 'Record saved successfully.');
        } catch (Exception $e) {
            DB::rollback();
            return redirect()->back()->with('danger', $e->getMessage());
        }
    }

    public function edit($id) {
        check_user_access('party_type_edit');
        $data = PartyType::where('_key', $id)->first();
        $_companyList = $this->userInstitutes();

        $this->model['page_title'] = "Party Type Edit";
        $this->model['data'] = $data;
        $this->model['companyList'] = $_companyList;
        return view('party_type.edit', $this->model);
    }

    public function update(Request $r, $id) {
        $input = $r->all();
        $rules = array(
            'party_type' => 'required'
        );
        $messages = array(
            'party_type.required' => 'Party Type required.'
        );

        $valid = Validator::make($input, $rules, $messages);
        if ($valid->fails()) {
            return redirect()->back()->withErrors($valid)->withInput();
        }

        DB::beginTransaction();
        try {
            $model = PartyType::find($id);
            $model->party_type = $r->party_type;
            $model->modified_at = cur_date_time();
            $model->modified_by = Auth::id();
            if (!$model->save()) {
                throw new Exception("Error while updating record.");
            }

            DB::commit();
            return redirect('/party_type')->with('success', 'Record updated successfully.');
        } catch (Exception $e) {
            DB::rollback();
            return redirect()->back()->with('danger', $e->getMessage());
        }
    }

    // Ajax Functions
    public function search(Request $r) {
        $item_count = !empty($r->item_count) ? $r->item_count : $this->getSettings()->pagesize;
        $srch = $r->srch;

        $query = PartyType::where('is_deleted', 0);
        if (!empty($srch)) {
            $query->where('party_type', 'like', '%' . $srch . '%');
        }
        $query->orderBy('party_type', 'asc');
        $dataset = $query->paginate($item_count);

        $this->model['dataset'] = $dataset;
        return view('party_type._list', $this->model);
    }

    public function delete() {
        $resp = array();
        DB::beginTransaction();
        try {
            foreach ($_POST['data'] as $id) {
                $data = PartyType::find($id);
                $data->is_deleted = 1;
                $data->deleted_by = Auth::id();
                $data->deleted_at = cur_date_time();
                if (!$data->save()) {
                    throw new Exception("Error while deleting records.");
                }
            }
            DB::commit();
            $resp['success'] = true;
            $resp['message'] = 'Record has been deleted successfully.';
        } catch (Exception $e) {
            DB::rollback();
            $resp['success'] = false;
            $resp['message'] = $e->getMessage();
        }
        return $resp;
    }

}
