<?php

namespace App\Http\Controllers\Setting\Party;

use Auth;
use DB;
use Exception;
use Validator;
use App\Http\Controllers\HomeController;
use App\Models\Setting\Party\Party;
use App\Models\Setting\Party\PartyType;
use Illuminate\Http\Request;

class PartyController extends HomeController {

    public function index() {
        check_user_access('party_list');
        // $model = new Party();
        $query = Party::where('is_deleted', 0)->get();
        $dataset = $query->paginate($this->getSettings()->pagesize);
        $_party_type = PartyType::where('is_deleted', 0)->get();

        $this->model['page_title'] = "Party List";
        $this->model['dataset'] = $dataset;
        $this->model['party_types'] = $_party_type;
        return view('party.index', $this->model);
    }

    public function create() {
        check_user_access('party_create');
        $_companyList = $this->userInstitutes();
        $_party_type = PartyType::where('is_deleted', 0)->get();
        $this->model['party_types'] = $_party_type;

        $this->model['page_title'] = "Party Information";
        $this->model['companyList'] = $_companyList;
        return view('party.create', $this->model);
    }

    public function store(Request $r) {
        $input = $r->all();
        $rules = array(
            'party_type_id' => 'required',
            'name' => 'required',
            'mobile' => 'required',
            'code' => 'required',
            'address' => 'required'
        );
        $messages = array(
            'party_type_id.required' => 'Party Type Required.',
            'name.required' => 'Party Name Required.',
            'mobile.required' => 'Mobile No Required.',
            'code.required' => 'Code Required.',
            'address.required' => 'Address Required.',
        );

        $valid = Validator::make($input, $rules, $messages);
        if ($valid->fails()) {
            return redirect()->back()->withErrors($valid)->withInput();
        }

        DB::beginTransaction();
        try {
            $model = new Party();
            $model->party_type_id = $r->party_type_id;
            $model->name = $r->name;
            $model->mobile = $r->mobile;
            $model->code = $r->code;
            $model->address = $r->address;
            $model->created_at = cur_date_time();
            $model->created_by = Auth::id();
            $model->_key = uniqueKey();
            if (!$model->save()) {
                throw new Exception("Error while saving record.");
            }
            DB::commit();
            return redirect('/party')->with('success', 'Record saved successfully.');
        } catch (Exception $e) {
            DB::rollback();
            return redirect()->back()->with('danger', $e->getMessage());
        }
    }

    public function edit($id) {
        check_user_access('party_edit');
        // $_id = urlDecrypt($id);
        $data = Party::find($id);
        $party_type = PartyType::where('is_deleted', 0)->get();
        
        $this->model['page_title'] = "Party Information";
        $this->model['party_type'] = $party_type;
        $this->model['data'] = $data;
        return view('party.edit', $this->model);
    }

    public function update(Request $r, $id) {
        $input = $r->all();
        $rules = array(
            'party_type_id' => 'required',
            'name' => 'required',
            'mobile' => 'required',
            'code' => 'required',
            'address' => 'required'
        );
        $messages = array(
            'party_type_id.required' => 'Party Type Required.',
            'name.required' => 'Party Name Required.',
            'mobile.required' => 'Mobile No Required.',
            'code.required' => 'Code Required.',
            'address.required' => 'Address Required.',
        );

        $valid = Validator::make($input, $rules, $messages);
        if ($valid->fails()) {
            return redirect()->back()->withErrors($valid)->withInput();
        }

        DB::beginTransaction();
        try {
            $model = Party::find($id);
            $model->party_type_id = $r->party_type_id;
            $model->name = $r->name;
            $model->mobile = $r->mobile;
            $model->code = $r->code;
            $model->address = $r->address;
            $model->modified_at = cur_date_time();
            $model->modified_by = Auth::id();
            if (!$model->save()) {
                throw new Exception("Error while updating record.");
            }

            DB::commit();
            return redirect('/party')->with('success', 'Record updated successfully.');
        } catch (Exception $e) {
            DB::rollback();
            return redirect()->back()->with('danger', $e->getMessage());
        }
    }

    // Ajax Functions
    // public function search(Request $r) {
    //     $item_count = !empty($r->item_count) ? $r->item_count : $this->getSettings()->pagesize;
    //     $srch = $r->srch;

    //     $query = StoreCondition::where('is_deleted', 0);
    //     if (!empty($srch)) {
    //         $query->where('description', 'like', '%' . $srch . '%');
    //     }
    //     $query->orderBy('description', 'asc');
    //     $dataset = $query->paginate($item_count);

    //     $this->model['dataset'] = $dataset;
    //     return view('loan_condition._list', $this->model);
    // }
    
    // Ajax Functions
    public function search(Request $r) {
        $item_count = !empty($r->item_count) ? $r->item_count : $this->getSettings()->pagesize;
        $srch = $r->srch;

        $query = Party::where('is_deleted', 0);
        if (!empty($srch)) {
            $query->where('name', 'like', '%' . $srch . '%');
        }
        $query->orderBy('name', 'asc');
        $dataset = $query->paginate($item_count);

        $this->model['dataset'] = $dataset;
        return view('party._list', $this->model);
    }
    
    public function delete() {
        $resp = array();
        DB::beginTransaction();
        try {
            foreach ($_POST['data'] as $id) {
                $data = Party::find($id);
                $data->is_deleted = 1;
                $data->deleted_by = Auth::id();
                $data->deleted_at = cur_date_time();
                if (!$data->save()) {
                    throw new Exception("Error while deleting records.");
                }
            }
            DB::commit();
            $resp['success'] = true;
            $resp['message'] = 'Record has been deleted successfully.';
        } catch (Exception $e) {
            DB::rollback();
            $resp['success'] = false;
            $resp['message'] = $e->getMessage();
        }
        return $resp;
    }
}
