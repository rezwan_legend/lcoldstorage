<?php

namespace App\Http\Controllers\Setting\DeleverySetting;

use Auth;
use DB;
use Exception;
use Validator;
use App\Http\Controllers\HomeController;
use App\Models\LoanCondition;
use App\Models\Setting\DeleverySetting\DeliveryCondition;
use Illuminate\Http\Request;

class DeliveryConditionController extends HomeController {

    public function index() {
        check_user_access('delivery_condition_list');
        $dataset = DeliveryCondition::where('is_deleted', 0)->orderBy('description', 'asc')->paginate($this->getSettings()->pagesize);

        $this->model['companyList'] = $this->userInstitutes();
        $this->model['page_title'] = "Delivery Condition List";
        $this->model['dataset'] = $dataset;
//        pr($dataset);
        return view('delivery_condition.index', $this->model);
    }

    public function create() {
        check_user_access('delivery_condition_create');
        $_companyList = $this->userInstitutes();

        $this->model['page_title'] = "Delivery Condition Information";
        $this->model['companyList'] = $_companyList;
        return view('delivery_condition.create', $this->model);
    }

    public function store(Request $r) {
        $input = $r->all();
        $rules = array(
            'description' => 'required'
        );
        $messages = array(
            'description.required' => 'Description required.'
        );

        $valid = Validator::make($input, $rules, $messages);
        if ($valid->fails()) {
            return redirect()->back()->withErrors($valid)->withInput();
        }

        DB::beginTransaction();
        try {
            $model = new DeliveryCondition();
            $model->description = $r->description;
            $model->created_at = cur_date_time();
            $model->created_by = Auth::id();
            $model->_key = uniqueKey();
            if (!$model->save()) {
                throw new Exception("Error while saving record.");
            }
            DB::commit();
            return redirect('/delivery_condition')->with('success', 'Record saved successfully.');
        } catch (Exception $e) {
            DB::rollback();
            return redirect()->back()->with('danger', $e->getMessage());
        }
    }

    public function edit($id) {
        check_user_access('delivery_condition_edit');
        $data = DeliveryCondition::where('_key', $id)->first();
        $_companyList = $this->userInstitutes();

        $this->model['page_title'] = "Delivery Condition Edit";
        $this->model['data'] = $data;
        $this->model['companyList'] = $_companyList;
        return view('delivery_condition.edit', $this->model);
    }

    public function update(Request $r, $id) {
        $input = $r->all();
        $rules = array(
            'description' => 'required'
        );
        $messages = array(
            'description.required' => 'Description required.'
        );

        $valid = Validator::make($input, $rules, $messages);
        if ($valid->fails()) {
            return redirect()->back()->withErrors($valid)->withInput();
        }

        DB::beginTransaction();
        try {
            $model = DeliveryCondition::find($id);
            $model->description = $r->description;
            $model->modified_at = cur_date_time();
            $model->modified_by = Auth::id();
            if (!$model->save()) {
                throw new Exception("Error while updating record.");
            }

            DB::commit();
            return redirect('/delivery_condition')->with('success', 'Record updated successfully.');
        } catch (Exception $e) {
            DB::rollback();
            return redirect()->back()->with('danger', $e->getMessage());
        }
    }

    // Ajax Functions
    public function search(Request $r) {
        $item_count = !empty($r->item_count) ? $r->item_count : $this->getSettings()->pagesize;
        $srch = $r->srch;

        $query = DeliveryCondition::where('is_deleted', 0);
        if (!empty($srch)) {
            $query->where('description', 'like', '%' . $srch . '%');
        }
        $query->orderBy('description', 'asc');
        $dataset = $query->paginate($item_count);

        $this->model['dataset'] = $dataset;
        return view('delivery_condition._list', $this->model);
    }

    public function delete() {
        $resp = array();
        DB::beginTransaction();
        try {
            foreach ($_POST['data'] as $id) {
                $data = DeliveryCondition::find($id);
                $data->is_deleted = 1;
                $data->deleted_by = Auth::id();
                $data->deleted_at = cur_date_time();
                if (!$data->save()) {
                    throw new Exception("Error while deleting records.");
                }
            }
            DB::commit();
            $resp['success'] = true;
            $resp['message'] = 'Record has been deleted successfully.';
        } catch (Exception $e) {
            DB::rollback();
            $resp['success'] = false;
            $resp['message'] = $e->getMessage();
        }
        return $resp;
    }

}
