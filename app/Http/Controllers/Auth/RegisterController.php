<?php

namespace App\Http\Controllers\Auth;

use Auth;
use DB;
use App\Models\User;
use App\Models\UserPermission;
use App\Http\Controllers\HomeController;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends HomeController {

    use RegistersUsers;

    protected $redirectTo = '/home';

    public function __construct() {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            $this->currentUser = Auth::user();
            view()->composer('*', function ($view) {
                $view->with('currentUser', $this->currentUser);
            });
            return $next($request);
        });
    }

    protected function validator(array $data) {
        return Validator::make($data, [
                    'name' => 'required|max:255',
                    'email' => 'required|email|max:255|unique:users',
                    'password' => 'required|min:6|confirmed',
        ]);
    }

    public function showRegistrationForm() {
        $subheads = \App\Models\Account\SubHead::where('is_deleted', 0)->whereIn('head_id', [HEAD_DEPO_ACCOUNT, HEAD_DSM_ACCOUNT, HEAD_ZM_ACCOUNT, HEAD_SR_ACCOUNT])->get();
        return view('auth.register', compact('subheads'));
    }

    // Customized User Create with User Permission
    protected function create(array $data) {
        DB::beginTransaction();
        try {
            $user_info = [
                'user_type' => $data['user_type'],
                'name' => $data['name'],
                'email' => $data['email'],
                'password' => bcrypt($data['password']),
                'created_by' => Auth::id(),
                '_key' => uniqueKey(),
            ];
            if ($data['user_type'] != USER_TYPE_ADMIN) {
                $user_info['subhead_id'] = $data['subhead_id'];
                $user_info['particular_id'] = $data['particular_id'];
            }

            $create_user = User::insert($user_info);
            if (!$create_user) {
                throw new Exception("Query Problem on Creating User");
            }
            $user_id = DB::getPdo()->lastInsertId();

            $default_access_items = default_user_access_items();
            $items = json_encode($default_access_items);
            $user_permisstion = array(
                'user_id' => $user_id,
                'items' => $items
            );

            $create_permisstion = UserPermission::insert($user_permisstion);
            if (!$create_permisstion) {
                throw new Exception("Query Problem on Creating Permission");
            }

            DB::commit();
            return redirect('user')->with('success', 'New User Created Successfully.');
        } catch (Exception $e) {
            DB::rollback();
            return redirect()->back()->with('danger', $e->getMessage());
        }
    }

}
