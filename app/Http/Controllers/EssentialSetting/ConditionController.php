<?php

namespace App\Http\Controllers\EssentialSetting;

use Auth;
use DB;
use Exception;
use Validator;
use App\Http\Controllers\HomeController;
use App\Models\EssentialSetting\Condition;
use Illuminate\Http\Request;

class ConditionController extends HomeController {

    public function index() {
        check_user_access('conditions');
        $dataset = Condition::where('is_deleted', 0)->get();
        $this->model['page_title'] = "Conditions Information";
        $this->model['dataset'] = $dataset;
        //pr($dataset);
        return view('condition.index', $this->model);
    }

    public function create() {
        check_user_access('conditions_create');
        $this->model['page_title'] = "Conditions create";
        return view('condition.create', $this->model);
    }

    public function store(Request $r) {
        $input = $r->all();
        $rules = array(
            'name' => 'required',
        );
        $messages = array(
            'name.required' => 'Name required.',
        );
        $valid = Validator::make($input, $rules, $messages);
        if ($valid->fails()) {
            return redirect()->back()->withErrors($valid)->withInput();
        }
        try {
            $model = new Condition();
            $model->name = $r->name;
            $model->type = $r->contidion_type;
            $model->created_at = cur_date_time();
            $model->created_by = Auth::id();
            $model->_key = uniqueKey();
            if (!$model->save()) {
                throw new Exception("Error while saving record.");
            }
            DB::commit();
            return redirect('/essential/conditions')->with('success', 'Record saved successfully.');
        } catch (Exception $e) {
            DB::rollback();
            return redirect()->back()->with('danger', $e->getMessage());
        }
    }

    public function edit($id) {
        check_user_access('conditions_edit');
        $data = Condition::where('_key', $id)->first();
        // pr($data->type);  
        $this->model['page_title'] = "Condition edit";
        $this->model['data'] = $data;
        return view('condition.edit', $this->model);
    }

    public function update(Request $r, $id) {
        $input = $r->all();
        $rules = array(
            'name' => 'required',
        );
        $messages = array(
            'name.required' => 'Name required.',
        );

        $valid = Validator::make($input, $rules, $messages);
        if ($valid->fails()) {
            return redirect()->back()->withErrors($valid)->withInput();
        }
        try {
            $model = Condition::find($id);
            $model->name = $r->name;
            $model->type = $r->contidion_type;
            $model->modified_at = cur_date_time();
            $model->modified_by = Auth::id();
            if (!$model->save()) {
                throw new Exception("Error while updating record.");
            }

            DB::commit();
            return redirect('/essential/conditions')->with('success', 'Record updated successfully.');
        } catch (Exception $e) {
            DB::rollback();
            return redirect()->back()->with('danger', $e->getMessage());
        }
    }

    // Ajax Functions
    public function search(Request $r) {
        $item_count = !empty($r->item_count) ? $r->item_count : $this->getSettings()->pagesize;
        $srch = $r->srch;
        $query = Condition::where('is_deleted', 0);
    
        if (!empty($srch)) {
            $query->where('name', 'like', '%' . $srch . '%');
        }
        $query->orderBy('name', 'asc');
        $dataset = $query->paginate($item_count);

        $this->model['dataset'] = $dataset;
        return view('condition._list', $this->model);
    }

    public function delete() {
        $resp = array();
        try {
            foreach ($_POST['data'] as $id) {
                $data = Condition::find($id);
                $data->is_deleted = 1;
                $data->deleted_by = Auth::id();
                $data->deleted_at = cur_date_time();
                if (!$data->save()) {
                    throw new Exception("Error while deleting records.");
                }
            }
            DB::commit();
            $resp['success'] = true;
            $resp['message'] = 'Record has been deleted successfully.';
        } catch (Exception $e) {
            DB::rollback();
            $resp['success'] = false;
            $resp['message'] = $e->getMessage();
        }
        return $resp;
    }


}
