<?php

namespace App\Http\Controllers\EssentialSetting;

use Auth;
use DB;
use Exception;
use Validator;
use App\Http\Controllers\HomeController;
use App\Models\EssentialSetting\AgentParty;
use Illuminate\Http\Request;

class AgentPartyController extends HomeController {

    public function index() {
        check_user_access('agent_party_list');
        $dataset = AgentParty::where('is_deleted', 0)->get();
        $this->model['page_title'] = "Agent party List";
        $this->model['breadcrumb_title'] = "Agent party List";
        
        $this->model['dataset'] = $dataset;
//        pr($dataset);
        return view('agent_party.index', $this->model);
    }

    public function create() {
        check_user_access('agent_party_create');
        $this->model['page_title'] = "Agent party Create";
        $this->model['breadcrumb_title'] = "Agent party Create";
        return view('agent_party.create', $this->model);
    }

    public function store(Request $r) {
        $input = $r->all();
        $rules = array(
            'agent_type'=>'required',
            'name'=>'required',
            'father_name'=>'required',
            'code'=>'required',
            'mobile'=>'required',
            'address'=>'required',
            'per_bag_commission'=>'required',
        );
        $messages = array(
            'name.required' => 'Name required.',
            'father_name' => 'Father Name required.',
            'code.required' => 'Code required.',
            'mobile.required' => 'Mobile Number required.',
            'address' => 'Address required.',
            'per_bag_commission.required' => 'Commision required.',
        );

        $valid = Validator::make($input, $rules, $messages);
        if ($valid->fails()) {
            return redirect()->back()->withErrors($valid)->withInput();
        }
        try {
            $model = new AgentParty();
            $model->name = $r->name;
            $model->agent_type=$r->agent_type;
            $model->name=$r->name;
            $model->father_name=$r->father_name;
            $model->code=$r->code;
            $model->mobile=$r->mobile;
            $model->address=$r->address;
            $model->per_bag_commission=$r->per_bag_commission;
            $model->interest_rate=$r->interest_rate;
            $model->created_at = cur_date_time();
            $model->created_by = Auth::id();
            $model->_key = uniqueKey();
            if (!$model->save()) {
                throw new Exception("Error while saving record.");
            }
            DB::commit();
            return redirect('/essential/agent_list')->with('success', 'Record saved successfully.');
        } catch (Exception $e) {
            DB::rollback();
            return redirect()->back()->with('danger', $e->getMessage());
        }
    }

    public function edit($id) {
        check_user_access('agent_party_edit');
        $data = AgentParty::where('_key', $id)->first();
        $this->model['page_title'] = "Agent and Party Edit";
        $this->model['breadcrumb_title'] = "Agent and Party Edit";
        $this->model['data'] = $data;
        return view('agent_party.edit', $this->model);
    }

    public function update(Request $r, $id) {
        // pr($_POST);
        $input = $r->all();
        $rules = array(
            'agent_type'=>'required',
            'name'=>'required',
            'father_name'=>'required',
            'code'=>'required|max:8',
            'mobile'=>'required|max:14',
            'per_bag_commission'=>'required|max:3',
            'interest_rate'=>'max:3',
        );
        $messages = array(
            'agent_type.required' => 'Agent Type required.',
            'name.required' => 'Name required.',
            'father_name.required' => 'Fathers Name required.',
            'code.required' => 'Code required.',
            'mobile.required' => 'Mobile Number required.',
            'per_bag_commission.required' => 'Commission per bag  required.',
            'interest_rate' => 'maximun 100.',
        );

        $valid = Validator::make($input, $rules, $messages);
        if ($valid->fails()) {
            return redirect()->back()->withErrors($valid)->withInput();
        }

        try {
            $model = AgentParty::find($id);
            $model->agent_type=$r->agent_type;
            $model->name=$r->name;
            $model->father_name=$r->father_name;
            $model->code=$r->code;
            $model->mobile=$r->mobile;
            $model->address=$r->address;
            $model->per_bag_commission=$r->per_bag_commission;
            $model->interest_rate=$r->interest_rate;
            $model->modified_at = cur_date_time();
            $model->modified_by = Auth::id();
            if (!$model->save()) {
                throw new Exception("Error while updating record.");
            }

            DB::commit();
            return redirect('/essential/agent_list')->with('success', 'Record updated successfully.');
        } catch (Exception $e) {
            DB::rollback();
            return redirect()->back()->with('danger', $e->getMessage());
        }
    }

    // Ajax Functions
    public function search(Request $r) {
        // pr($_POST);
        $item_count = !empty($r->item_count) ? $r->item_count : $this->getSettings()->pagesize;
        $search = $r->search;
        $party= $r->agent_party_id;
        $code= $r->code;

        $query = AgentParty::where('is_deleted', 0);
       
        if (!empty($search)) {
            $query->where('name', 'like', '%' . $search . '%');
        } 
        if (!empty($party)) {
            $query->where('agent_type', 'like', '%' . $party . '%');
        }
         if (!empty($code)) {
            $query->where('code', 'like', '%' . $code . '%');
        }
        
        
        $query->orderBy('name', 'asc');
        $dataset = $query->paginate($item_count);

        $this->model['dataset'] = $dataset;
        return view('agent_party._list', $this->model);
    }

    public function delete() {
        $resp = array();
        DB::beginTransaction();
        try {
            foreach ($_POST['data'] as $id) {
                $data = AgentParty::find($id);
                $data->is_deleted = 1;
                $data->deleted_by = Auth::id();
                $data->deleted_at = cur_date_time();
                if (!$data->save()) {
                    throw new Exception("Error while deleting records.");
                }
            }
            DB::commit();
            $resp['success'] = true;
            $resp['message'] = 'Record has been deleted successfully.';
        } catch (Exception $e) {
            DB::rollback();
            $resp['success'] = false;
            $resp['message'] = $e->getMessage();
        }
        return $resp;
    }


}
