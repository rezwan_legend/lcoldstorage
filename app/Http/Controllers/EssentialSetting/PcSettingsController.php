<?php

namespace App\Http\Controllers\EssentialSetting;

use Auth;
use DB;
use Exception;
use Validator;
use App\Http\Controllers\HomeController; 
use App\Models\EssentialSetting\PcSetting;
use Illuminate\Http\Request;

class PcSettingsController extends HomeController {

    public function index() {
        check_user_access('pc_settings');
        $dataset = PcSetting::where('is_deleted', 0)->get();
        $this->model['page_title'] = "Pc Settings Information";
        $this->model['dataset'] = $dataset;
        //pr($dataset);
        return view('pc_setting.index', $this->model);
    }

    public function create() {
        check_user_access('pc_settings_create');
        $this->model['page_title'] = "Pc settings create";
        return view('pc_setting.create', $this->model);
    }

    public function store(Request $r) {
        $input = $r->all();
        $rules = array(
            'name' => 'required',
        );
        $messages = array(
            'name.required' => 'Name required.',
        );
        $valid = Validator::make($input, $rules, $messages);
        if ($valid->fails()) {
            return redirect()->back()->withErrors($valid)->withInput();
        }
        DB::beginTransaction();
        try {
            $model = new PcSetting();
            $model->name = $r->name;
            $model->created_at = cur_date_time();
            $model->created_by = Auth::id();
            $model->_key = uniqueKey();
            if (!$model->save()) {
                throw new Exception("Error while saving record.");
            }
            DB::commit();
            return redirect('/essential/pc_settings')->with('success', 'Record saved successfully.');
        } catch (Exception $e) {
            DB::rollback();
            return redirect()->back()->with('danger', $e->getMessage());
        }
    }

    public function edit($id) {
        check_user_access('pc_settings_edit');
        $data = PcSetting::where('_key', $id)->first();

        $this->model['page_title'] = "Pc Setting edit";
        $this->model['data'] = $data;
        return view('pc_setting.edit', $this->model);
    }

    public function update(Request $r, $id) {
        $input = $r->all();
        $rules = array(
            'name' => 'required',
        );
        $messages = array(
            'name.required' => 'Name required.',
        );

        $valid = Validator::make($input, $rules, $messages);
        if ($valid->fails()) {
            return redirect()->back()->withErrors($valid)->withInput();
        }
        DB::beginTransaction();
        try {
            $model = PcSetting::find($id);
            $model->name = $r->name;
            $model->modified_at = cur_date_time();
            $model->modified_by = Auth::id();
            if (!$model->save()) {
                throw new Exception("Error while updating record.");
            }

            DB::commit();
            return redirect('/essential/pc_settings')->with('success', 'Record updated successfully.');
        } catch (Exception $e) {
            DB::rollback();
            return redirect()->back()->with('danger', $e->getMessage());
        }
    }

    // Ajax Functions
    public function search(Request $r) {
        $item_count = !empty($r->item_count) ? $r->item_count : $this->getSettings()->pagesize;
        $srch = $r->srch;
        $query = PcSetting::where('is_deleted', 0);
    
        if (!empty($srch)) {
            $query->where('name', 'like', '%' . $srch . '%');
        }
        $query->orderBy('name', 'asc');
        $dataset = $query->paginate($item_count);

        $this->model['dataset'] = $dataset;
        return view('pc_setting._list', $this->model);
    }

    public function delete() {
        $resp = array();
        try {
            foreach ($_POST['data'] as $id) {
                $data = PcSetting::find($id);
                $data->is_deleted = 1;
                $data->deleted_by = Auth::id();
                $data->deleted_at = cur_date_time();
                if (!$data->save()) {
                    throw new Exception("Error while deleting records.");
                }
            }
            DB::commit();
            $resp['success'] = true;
            $resp['message'] = 'Record has been deleted successfully.';
        } catch (Exception $e) {
            DB::rollback();
            $resp['success'] = false;
            $resp['message'] = $e->getMessage();
        }
        return $resp;
    }


}
