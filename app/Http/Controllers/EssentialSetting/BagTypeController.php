<?php

namespace App\Http\Controllers\EssentialSetting;

use Auth;
use DB;
use Exception;
use Validator;
use App\Http\Controllers\HomeController;
use App\Models\EssentialSetting\BagType;
use Illuminate\Http\Request;

class BagTypeController extends HomeController {

    public function index() {
        check_user_access('bag_type');
        $dataset = BagType::where('is_deleted', 0)->get();
        $this->model['page_title'] = "Bag type Information";
        $this->model['dataset'] = $dataset;
        //pr($dataset);
        return view('bag_type.index', $this->model);
    }

    public function create() {
        check_user_access('bag_type_create');
        $this->model['page_title'] = "Bag type create";
        return view('bag_type.create', $this->model);
    }

    public function store(Request $r) {
        $input = $r->all();
        $rules = array(
            'name' => 'required',
        );
        $messages = array(
            'name.required' => 'Name required.',
        );
        $valid = Validator::make($input, $rules, $messages);
        if ($valid->fails()) {
            return redirect()->back()->withErrors($valid)->withInput();
        }
        DB::beginTransaction();
        try {
            $model = new BagType();
            $model->session = $r->session;
            $model->name = $r->name;
            $model->per_bag_rent = $r->per_bag_rent;
            $model->per_kg_rent = $r->per_kg_rent;
            $model->agent_bag_rent = $r->agent_bag_rent;
            $model->agent_kg_rent = $r->agent_kg_rent;
            $model->party_bag_rent = $r->party_bag_rent;
            $model->party_kg_rent = $r->party_kg_rent;
            $model->per_bag_loan = $r->per_bag_loan;
            $model->empty_bag_rent = $r->empty_bag_rate;
            $model->fan_charge = $r->fan_charge;
            if(isset($r->is_default)){
                $model->default = $r->is_default;
            }
            $model->created_at = cur_date_time();
            $model->created_by = Auth::id();
            $model->_key = uniqueKey();
            if (!$model->save()) {
                throw new Exception("Error while saving record.");
            }
            DB::commit();
            return redirect('/essential/bag_type')->with('success', 'Record saved successfully.');
        } catch (Exception $e) {
            DB::rollback();
            return redirect()->back()->with('danger', $e->getMessage());
        }
    }

    public function edit($id) {
        check_user_access('bag_type_edit');
        $data = BagType::where('_key', $id)->first();
        //  pr($data->toArray());  
        $this->model['page_title'] = "bag_type edit";
        $this->model['data'] = $data;
        return view('bag_type.edit', $this->model);
    }

    public function update(Request $r, $id) {
        $input = $r->all();
        $rules = array(
            'name' => 'required',
        );
        $messages = array(
            'name.required' => 'Name required.',
        );

        $valid = Validator::make($input, $rules, $messages);
        if ($valid->fails()) {
            return redirect()->back()->withErrors($valid)->withInput();
        }
        try {
            $model = BagType::find($id);
            $model->session = $r->session;
            $model->name = $r->name;
            $model->per_bag_rent = $r->per_bag_rent;
            $model->per_kg_rent = $r->per_kg_rent;
            $model->agent_bag_rent = $r->agent_bag_rent;
            $model->agent_kg_rent = $r->agent_kg_rent;
            $model->party_bag_rent = $r->party_bag_rent;
            $model->party_kg_rent = $r->party_kg_rent;
            $model->per_bag_loan = $r->per_bag_loan;
            $model->empty_bag_rent = $r->empty_bag_rate;
            $model->fan_charge = $r->fan_charge;
            $model->modified_at = cur_date_time();
            $model->modified_by = Auth::id();
            if (!$model->save()) {
                throw new Exception("Error while updating record.");
            }

            DB::commit();
            return redirect('/essential/bag_type')->with('success', 'Record updated successfully.');
        } catch (Exception $e) {
            DB::rollback();
            return redirect()->back()->with('danger', $e->getMessage());
        }
    }

    // Ajax Functions
    public function search(Request $r) {
        $item_count = !empty($r->item_count) ? $r->item_count : $this->getSettings()->pagesize;
        $srch = $r->srch;
        $query = BagType::where('is_deleted', 0);
    
        if (!empty($srch)) {
            $query->where('name', 'like', '%' . $srch . '%');
        }
        $query->orderBy('name', 'asc');
        $dataset = $query->paginate($item_count);

        $this->model['dataset'] = $dataset;
        return view('bag_type._list', $this->model);
    }

    public function delete() {
        $resp = array();
        try {
            foreach ($_POST['data'] as $id) {
                $data = BagType::find($id);
                $data->is_deleted = 1;
                $data->deleted_by = Auth::id();
                $data->deleted_at = cur_date_time();
                if (!$data->save()) {
                    throw new Exception("Error while deleting records.");
                }
            }
            DB::commit();
            $resp['success'] = true;
            $resp['message'] = 'Record has been deleted successfully.';
        } catch (Exception $e) {
            DB::rollback();
            $resp['success'] = false;
            $resp['message'] = $e->getMessage();
        }
        return $resp;
    }


}
