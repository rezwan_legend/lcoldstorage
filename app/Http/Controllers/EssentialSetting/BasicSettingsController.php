<?php

namespace App\Http\Controllers\EssentialSetting;
use DB;
use Exception;
use Validator;
use App\Http\Controllers\HomeController;
use App\Models\EssentialSetting\BasicSettings;
use Illuminate\Http\Request;
use Auth;

class BasicSettingsController extends HomeController {

    public function index() {
        check_user_access('basic settings');
       $dataset = BasicSettings::where('is_deleted',0)->first();
         $this->model['page_title'] = "Basic Settings";
         $this->model['dataset'] = $dataset;
        //   pr($dataset);
        return view('basic_settings.index',$this->model);
    }

    public function change() {
        
       
        $data = BasicSettings::where('is_deleted', 0)->first();
        // dd($data);
        $this->model['data'] = $data;
         $this->model['page_title'] = "Basic Settings edit";
         return view('basic_settings.edit',$this->model);
    }

    public function store(Request $r) {

         return 'hello store';

  }
     public function show() {

         return 'hello show';

  }

    public function edit($id) {
        check_user_access('basic_settings_edit');
     $data = BasicSettings::where('is_deleted', 0)->get();
        $this->model['data'] = $data;
        $this->model['page_title'] = "Basic Settings Information";
        return view('basic.edit', $this->model);
    }

    public function update(Request $r) {

     
        $input = $r->all();
        $rules = array(
            'agent_commission' => 'required',

        );
        $messages = array(
            'agent_commission.required' => 'Agent commission required.',

        );

        $valid = Validator::make($input, $rules, $messages);
        if ($valid->fails()) {
            return redirect()->back()->withErrors($valid)->withInput();
        }

        DB::beginTransaction();
        try {
            $model = BasicSettings::first();
            $model->interest_rate = $r->interest_rate;
            $model->loan_duration = $r->period;
            $model->minimum_duration = $r->min_day;
            $model->per_bag_rent = $r->max_rent_per_qty;
            $model->per_kg_rent = $r->max_rent_per_kg;
            $model->per_bag_loan = $r->max_loan_per_qty;
            $model->empty_bag_price = $r->empty_bag_price;
            $model->fan_charge = $r->fan_charge;
            $model->agent_commission = $r->agent_commission;
            $model->empty_bag_as_loan = $r->ebag_count;
            $model->carring_as_loan = $r->carrying_count;
            $model->modified_at = cur_date_time();
            $model->modified_by= Auth::id();
            if (!$model->save()) {
                throw new Exception("Error while updating record.");
            }

            DB::commit();
            return redirect('/essential/basic_settings')->with('success', 'Record updated successfully.');
        } catch (Exception $e) {
            DB::rollback();
            return redirect()->back()->with('danger', $e->getMessage());
        }
    }






}
