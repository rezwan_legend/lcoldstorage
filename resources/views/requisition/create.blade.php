@extends('layouts.app')

@section('breadcrumbs')
<div id="breadcrumbBar" class="breadcrumb site_nav_links no_bdr_rad clearfix">
    <div class="col-md-3 col-sm-3 col-xs-2 no_pad">
        <button class="btn btn-info btn-xs" type="button" onclick="history.back()" title="Go Back"><span class="visible-xs"><i class="fa fa-arrow-left"></i></span><span class="hidden-xs">Back</span></button>
        <button class="btn btn-info btn-xs" onclick="redirectTo('<?= url('view-clear'); ?>')" title="Refresh" type="button"><span class="visible-xs"><i class="fa fa-refresh"></i></span><span class="hidden-xs">Refresh</span></button>
    </div>
    <div class="col-md-6 col-sm-6 hidden-xs text-center">
        <h2 class="page-title">{{$page_title}}</h2>
    </div>
    <div class="col-md-3 col-sm-3 col-xs-10 no_pad">
        <ul class="text-right no_mrgn no_pad">
            <li><a href="{{ url('/home') }}">Home</a> <i class="fa fa-angle-right"></i></li>
            <li><a href="{{ url('/requisition') }}">Requisition</a> <i class="fa fa-angle-right"></i></li>
            <li>Form</li>
        </ul>
    </div>
</div>
@endsection

@section('content')
<div class="col-md-6 col-md-offset-3 col-sm-6 col-sm-offset-3">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title">{{$page_title}}</h3>
        </div>
        <div class="panel-body">
            {!! Form::open(['method' => 'POST', 'url' => '/requisition', 'class' => 'form-horizontal']) !!}
            
        <div class="form-group">
            <label class="col-md-4 control-label" for="session">Session</label>
            <div class="col-md-6">
                <select class="form-control select2search" id="session" name="session" required>
                    <option value="">Session</option>
                    <option value="2021">2021</option>
                    <option value="2022">2022</option>
                    <option value="2023">2023</option>                   
                </select>
                <small class="text-danger">{{ $errors->first('session') }}</small>
            </div>
        </div>



            <div class="form-group">
                <label for="date" class="col-md-4 control-label">Branche Name</label>
                <div class="col-md-6">
                    <input type="date" class="form-control" id="date" name="date" required>
                    <small class="text-danger">{{ $errors->first('date') }}</small>
                </div>
            </div>


           

             <div class="form-group">
                <label for="requisition_no" class="col-md-4 control-label">Requisition No</label>
                <div class="col-md-6">
                    <input type="number" class="form-control" id="requisition_no" name="requisition_no" required>
                    <small class="text-danger">{{ $errors->first('requisition_no') }}</small>
                </div>
            </div>

             <div class="form-group">
                <label for="requisition_by" class="col-md-4 control-label">Requisition By</label>
                <div class="col-md-6">
                    <input type="text" class="form-control" id="requisition_by" name="requisition_by" required>
                    <small class="text-danger">{{ $errors->first('requisition_by') }}</small>
                </div>
            </div>    
             <div class="form-group">
                <label for="title" class="col-md-4 control-label">Title</label>
                <div class="col-md-6">
                    <input id="title" name="title"  class="form-control">
                    <small class="text-danger">{{ $errors->first('title') }}</small>
                </div>
            </div>           
            <div class="col-md-8 col-md-offset-4">
                <button type="button" class="btn btn-info" id="resetbtn">Reset</button>
                <button type="submit" class="btn btn-primary">Save</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>

@endsection