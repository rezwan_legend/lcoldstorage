@extends('layouts.app')

@section('breadcrumbs')
<div id="breadcrumbBar" class="breadcrumb site_nav_links no_bdr_rad clearfix">
    <div class="col-md-3 col-sm-3 col-xs-2 no_pad">
        <button class="btn btn-info btn-xs" type="button" onclick="history.back()" title="Go Back"><span class="visible-xs"><i class="fa fa-arrow-left"></i></span><span class="hidden-xs">Back</span></button>
        <button class="btn btn-info btn-xs" onclick="redirectTo('<?= url('delivery_condition/'.$data->_key.'/edit'); ?>')" title="Refresh" type="button"><span class="visible-xs"><i class="fa fa-refresh"></i></span><span class="hidden-xs">Refresh</span></button>
    </div>
    <div class="col-md-6 col-sm-6 hidden-xs text-center">
        <h2 class="page-title">{{$page_title}}</h2>
    </div>
    <div class="col-md-3 col-sm-3 col-xs-10 no_pad">
        <ul class="text-right no_mrgn no_pad">
            <li><a href="{{ url('/home') }}">Home</a> <i class="fa fa-angle-right"></i></li>
            <li><a href="{{ url('/delivery_condition') }}">Delivery Condition</a> <i class="fa fa-angle-right"></i></li>
            <li>Form</li>
        </ul>
    </div>
</div>
@endsection

@section('content')
<div class="col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title">{{$page_title}}</h3>
        </div>
        <div class="panel-body">
            {!! Form::open(['method' => 'PUT', 'url' => 'delivery_condition/'.$data->id, 'class' => 'form-horizontal']) !!}
            <div class="form-group">
                <label for="description" class="col-md-3 control-label">Description</label>
                <div class="col-md-8">
                    <textarea name="description" id="description" cols="5" rows="5" class="form-control" value="{{ $data->description }}">{{ $data->description }}</textarea>
                    <small class="text-danger">{{ $errors->first('description') }}</small>
                </div>
            </div>
            <div class="col-md-8 col-md-offset-4">
                <button type="button" class="btn btn-info" onclick="history.back()" title="Go Back">Cancel</button>
                <button type="submit" class="btn btn-primary">Update</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
@endsection