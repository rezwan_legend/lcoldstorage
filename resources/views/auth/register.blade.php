@extends('layouts.app')

@section('content')
<div id="breadcrumbBar" class="breadcrumb site_nav_links no_bdr_rad clearfix">
    <div class="col-md-3 col-sm-3 col-xs-2 cxs_2 no_pad">
        <button class="btn btn-info btn-xs" type="button" onclick="history.back()" title="Go Back"><span class="visible-xs"><i class="fa fa-arrow-left"></i></span><span class="hidden-xs">Back</span></button>
        <button class="btn btn-info btn-xs" onclick="redirectTo('<?= url('view-clear') ?>')" title="Refresh" type="button"><span class="visible-xs"><i class="fa fa-refresh"></i></span><span class="hidden-xs">Refresh</span></button>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-6 cxs_10 text-center">
        <h2 class="page-title">Users List</h2>
    </div>
    <div class="col-md-3 col-sm-3 col-xs-4 cxs_12 no_pad">
        <ul class="text-right no_mrgn">
            <li><a href="{{ url('/home') }}">Dashboard</a> <span class="fa fa-angle-right"></span></li>
            <li><a href="{{ url('/user') }}">User</a> <span class="fa fa-angle-right"></span></li>
            <li>Form</li>
        </ul>                         
    </div>
</div>

<div class="container-flued">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Register New User</div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ route('register') }}">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="user_type" class="col-md-4 control-label">User Type</label>
                            <div class="col-md-6">
                                <select class="form-control" id="user_type" name="user_type" required>
                                    <option value="">Select</option>
                                    @foreach(user_types_array() as $typeKey => $typeValue)
                                    <option value="{{ $typeKey }}">{{ $typeValue }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group user_types" style="display: none">
                            <label for="subhead_id" class="col-md-4 control-label">Subhead</label>
                            <div class="col-md-6">
                                <select class="form-control user_type_info" id="subhead_id" name="subhead_id" onchange="get_particulars(this, 'particular_id')">
                                    <option value="">Select</option>
                                    @foreach( $subheads as $subhead )
                                    <option value="{{ $subhead->id }}">{{ $subhead->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group user_types" style="display: none">
                            <label for="particular_id" class="col-md-4 control-label">Particular</label>
                            <div class="col-md-6">
                                <select class="form-control user_type_info" id="particular_id" name="particular_id">
                                    <option value="">Select</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Name</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" id="name" name="name" value="{{ old('name') }}" required autofocus>
                                @if ($errors->has('name'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>
                            <div class="col-md-6">
                                <input type="email" class="form-control" id="email" name="email" value="{{ old('email') }}" required>
                                @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>          
                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Password</label>
                            <div class="col-md-6">
                                <input type="password" class="form-control" id="password" name="password" required>
                                @if ($errors->has('password'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>
                            <div class="col-md-6">
                                <input type="password" class="form-control" id="password-confirm" name="password_confirmation" required>
                            </div>
                        </div>
                        <div class="clearfix">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">Register</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('page_script')
<script type="text/javascript">
    let userTypes = ['Depo', 'Dsm', 'Zm', 'Sr', 'Dealer'];
    $(document).on("change", "#user_type", function (event) {
        if (userTypes.includes(this.value)) {
            $(".user_types").show();
            $(".user_type_info").attr('required', 'required');
        } else {
            $(".user_type_info").removeAttr('required');
            document.getElementById("subhead_id").selectedIndex = 0;
            document.getElementById("particular_id").selectedIndex = 0;
            $(".user_types").hide();
        }
        event.preventDefault();
    });

    $(document).on("change", "#subhead_id", function (event) {
        $("#name").val('');
        event.preventDefault();
    });

    $(document).on("change", "#particular_id", function (event) {
        var _name = get_selected_option_info(this, 'data-name');
        $("#name").val(_name);
        event.preventDefault();
    });
</script>
@endsection
