
                <table class="table table-bordered table-striped">
                 
                    <tbody><tr class="bg_gray">
                        <th class="text-right" style="width:20%">Property</th>
                        <th>Value</th>
                    </tr>
                    <tr>
                       


                          <th class="text-right">Interest Rate</th>
                        <td>
                            <span class="show_input"  ><input type="text" id="interest_rate" name="interest_rate" min="0" value="{{$data->interest_rate}}"></span>
                            <span>%</span>
                        </td>
                    </tr>
                    <tr>
                        <th class="text-right">Loan Duration</th>
                        <td>
                     
                            <span class="show_input"  ><input type="number" id="period" name="period" min="0" value="{{$data->loan_duration}}"></span>
                            <span>Day(s)</span>
                        </td>
                    </tr>
                    <tr>
                        <th class="text-right">Minimum Duration</th>
                        <td>
                            
                            <span class="show_input"  ><input type="number" id="min_day" name="min_day" min="0" value="{{$data->minimum_duration}}"></span>
                            <span>Day(s)</span>
                        </td>
                    </tr>
                    <tr>
                        <th class="text-right">Per Bag Rent</th>
                        <td>
                       
                            <span class="show_input"  ><input type="number" id="max_rent_per_qty" name="max_rent_per_qty" min="0" value="{{$data->per_bag_rent}}"></span>
                            <span>Tk</span>
                        </td>
                    </tr>
                    <tr>
                        <th class="text-right">Per Kg Rent</th>
                        <td>
                      
                            <span class="show_input"  ><input type="number" id="max_rent_per_kg" name="max_rent_per_kg" min="0" value="{{$data->per_kg_rent}}"></span>
                            <span>Tk</span>
                        </td>
                    </tr>
                    <tr>
                        <th class="text-right">Per Bag Loan</th>
                        <td>
                      
                            <span class="show_input"  ><input type="number" id="max_loan_per_qty" name="max_loan_per_qty" min="0" value="{{$data->per_bag_loan}}"></span>
                            <span>Tk</span>
                        </td>
                    </tr>
                    <tr>
                        <th class="text-right">Empty Bag Price</th>
                        <td>
               
                            <span class="show_input"  ><input type="number" id="empty_bag_price" name="empty_bag_price" min="0" value="{{$data->empty_bag_price}}"></span>
                            <span>Tk</span>
                        </td>
                    </tr>
                    <tr>
                        <th class="text-right">Fan Charge</th>
                        <td>
                         
                            <span class="show_input"  ><input type="number" id="fan_charge" name="fan_charge" min="0" value="{{$data->fan_charge}}"></span>
                            <span>Tk</span>
                        </td>
                    </tr>
                    <tr>
                        <th class="text-right">Agent Commission</th>
                        <td>
                           
                            <span class="show_input"  ><input type="number" id="agent_commission" name="agent_commission" min="0" value="{{$data->agent_commission}}"></span>
                            <span>Tk</span>
                        </td>
                    </tr>
                    <tr>
                        <th class="text-right">Empty Bag As Loan</th>
                        <td>
                           
                            <span class="show_input"  >
                                <select id="ebag_count" name="ebag_count" style="width: 170px;">
                                    <option value="Yes" selected="">Yes</option>
                                    <option value="No">No</option>
                                </select>
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <th class="text-right">Carrying As Loan</th>
                        <td>
                            <span class="show_input"  >
                                <select id="carrying_count" name="carrying_count" style="width: 170px;">
                                    <option value="Yes" selected="">Yes</option>
                                    <option value="No">No</option>
                                </select>
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <td class="text-center" colspan="2">
                            <button class="btn btn-info btn-xs" id="change_setting" type="button" disabled="disabled">Change</button>
                            <button type="submit" class="btn btn-primary btn-xs">Update</button>
                            <button class="btn btn-warning btn-xs" id="cancle_setting" type="button" ><a href="/lcoldstorage/essential/basic_settings" style="color: #fff; text-decoration:none;">Cancel</a></button>
                        </td>
                    </tr>
                </tbody></table>
            
