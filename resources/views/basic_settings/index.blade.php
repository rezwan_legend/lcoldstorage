@extends('layouts.app')

@section('breadcrumbs')
<div id="breadcrumbBar" class="breadcrumb site_nav_links no_bdr_rad clearfix">
    <div class="col-md-2 col-sm-3 col-xs-2 no_pad">
        <button class="btn btn-info btn-xs" type="button" onclick="history.back()" title="Go Back"><span class="visible-xs"><i class="fa fa-arrow-left"></i></span><span class="hidden-xs">Back</span></button>
        <button class="btn btn-info btn-xs" onclick="redirectTo('<?= url('view-clear'); ?>')" title="Refresh" type="button"><span class="visible-xs"><i class="fa fa-refresh"></i></span><span class="hidden-xs">Refresh</span></button>
    </div>
    <div class="col-md-6 col-sm-6 hidden-xs text-center">
        <h2 class="page-title">{{$page_title}}</h2>
    </div>
    <div class="col-md-4 col-sm-3 col-xs-10 no_pad">
        <ul class="text-right no_mrgn no_pad">
            <li><a href="{{ url('/home') }}">Home</a> <i class="fa fa-angle-right"></i></li>
            <li>Basic settings</li>
        </ul>
    </div>
</div>
@endsection

@section('content')


<div id="print_area">
    <?php print_header("basic_settings List", true, false); ?>
    {!! Form::open(['method' => 'post', 'url' => route('settings.update'),  'class' => 'form-horizontal' ]) !!}
    
@if(!empty($dataset))
<div id="bodyPanel" style="min-height: 507px;"  >
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-md-12" id="admin_view">
                <div id="breadcrumbBar" class="breadcrumb site_nav_links clearfix">
                    <div class="col-md-3 col-sm-3 col-xs-3 no_pad">
                        <button type="button" class="btn btn-info btn-xs" onclick="redirectTo('/en/site/clear_cache')" title="Refresh"><i class="fa fa-refresh"></i> <span class="hidden-xs">Refresh</span></button>
                        <button type="button" class="btn btn-primary btn-xs" onclick="printDiv('printDiv')" title="Print"><i class="fa fa-print"></i> <span class="hidden-xs">Print</span></button>
                    </div>
                        <div class="col-md-6 col-sm-6 hidden-xs text-center">
                            <h2 class="page-title">Basic Setting</h2>
                        </div>
                            <div class="col-md-3 col-sm-3 col-xs-9 no_pad">
                                <ul class="text-right no_mrgn">
                                <li><a href="/">Dashboard</a> <span class="fa fa-angle-right"></span></li><li><a href="/en/loan">Loan</a> <span class="fa fa-angle-right"></span></li><li><span>Setting</span></li></ul>                            </div>
                        </div>
                       

                        
                        <div class="content-panel">
         
            
            <div class="table-responsive" id="ajax_content">
             
                <table class="table table-bordered table-striped" >
                    <tbody><tr class="bg-info">
                        <th class="text-right" style="width:20%">Property</th>
                        <th>Value</th>
                    </tr>
                    <tr>
                        <th class="text-right">Interest Rate</th>
                        <td>
                            <span class="show_txt" id="txtIntRate">{{$dataset->interest_rate ?? ''}}</span>
                             <span>%</span>
                        </td>
                    </tr>
                    <tr>
                        <th class="text-right">Loan Duration</th>
                        <td>
                            <span class="show_txt" id="txtPeriod">{{$dataset->loan_duration ?? ''}}</span>
                            <span>Day(s)</span>
                        </td>
                    </tr>
                    <tr>
                        <th class="text-right">Minimum Duration</th>
                        <td>
                            <span class="show_txt" id="txtMinDay">{{$dataset->minimum_duration ?? ''}}</span>
                            <span>Day(s)</span>
                        </td>
                    </tr>
                    <tr>
                        <th class="text-right">Per Bag Rent</th>
                        <td>
                            <span class="show_txt" id="txtMaxRentPerQty">{{$dataset->per_bag_rent ?? ''}}</span>
                            <span>Tk</span>
                        </td>
                    </tr>
                    <tr>
                        <th class="text-right">Per Kg Rent</th>
                        <td>
                            <span class="show_txt" id="txtMaxRentPerKg">{{$dataset->per_kg_rent ?? ''}}</span>
                            
                            <span>Tk</span>
                        </td>
                    </tr>
                    <tr>
                        <th class="text-right">Per Bag Loan</th>
                        <td>
                            <span class="show_txt" id="txtMaxLoanPerQty">{{$dataset->per_bag_loan ?? ''}}</span>
                         
                            <span>Tk</span>
                        </td>
                    </tr>
                    <tr>
                        <th class="text-right">Empty Bag Price</th>
                        <td>
                            <span class="show_txt" id="txtEmptyBagPrice">{{$dataset->empty_bag_price ?? ''}}</span>
                           
                            <span>Tk</span>
                        </td>
                    </tr>
                    <tr>
                        <th class="text-right">Fan Charge</th>
                        <td>
                            <span class="show_txt" id="txtFanCharge">{{$dataset->fan_charge ?? ''}}</span>
                           
                            <span>Tk</span>
                        </td>
                    </tr>
                    <tr>
                        <th class="text-right">Agent Commission</th>
                        <td>
                            <span class="show_txt" id="txtAgentCommission">{{$dataset->agent_commission ?? ''}}</span>
                          
                            <span>Tk</span>
                        </td>
                    </tr>
                    <tr>
                        <th class="text-right">Empty Bag As Loan</th>
                        <td>
                            <span class="show_txt" id="txtEbagCount">{{$dataset->empty_bag_as_loan ?? ''}}</span>
                               
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <th class="text-right">Carrying As Loan</th>
                        <td>
                            <span class="show_txt" id="txtCarryingCount">{{$dataset->carring_as_loan ?? ''}}</span>
                              
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <td class="text-center" colspan="2">
                            <button class="btn btn-info btn-xs" id="change_setting"  type="button"><i class="fa fa-edit"></i> Change</a></button>
                            <button class="btn btn-primary btn-xs" id="update_setting" type="button" disabled="">Update</button>
                            <button class="btn btn-warning btn-xs" id="cancle_setting" type="button" disabled="">Cancel</button>
                        </td>
                    </tr>
                </tbody>
                </table>
            </div>
    
    </div>
</div>
</div>
</div>
</div>
        @else
        <div class="alert alert-info">No records found.</div>
        @endif
    </div>
</div>
{!! Form::close() !!}

<script type="text/javascript">
    $(document).ready(function () {
    

            $(document).on("click", "#change_setting", function() {
                var change_setting = $("#change_setting").val();
                $.ajax({
                    url: "{{ URL::to('/essential/basic_setting/change') }}",
                    type: "post",
                    data: {
                        _token: csrfToken,
                        change_setting: change_setting
                    },
                    success: function(res) {
                        $('#ajax_content').html(res);
                    },
                    error: function(xhr, status) {
                        alert('There is some error.Try after some time.');
                    }
                });

            });

  
    
    });
</script>
@endsection