@extends('layouts.app')

@section('breadcrumbs')
<div id="breadcrumbBar" class="breadcrumb site_nav_links no_bdr_rad clearfix">
    <div class="col-md-3 col-sm-3 col-xs-2 no_pad">
        <button class="btn btn-info btn-xs" type="button" onclick="history.back()" title="Go Back"><span class="visible-xs"><i class="fa fa-arrow-left"></i></span><span class="hidden-xs">Back</span></button>
        <button class="btn btn-info btn-xs" onclick="redirectTo('<?= url('/loan/advance/payment_create'); ?>')" title="Refresh" type="button"><span class="visible-xs"><i class="fa fa-refresh"></i></span><span class="hidden-xs">Refresh</span></button>
    </div>
    <div class="col-md-6 col-sm-6 hidden-xs text-center">
        <h2 class="page-title">{{$page_title}}</h2>
    </div>
    <div class="col-md-3 col-sm-3 col-xs-10 no_pad">
        <ul class="text-right no_mrgn no_pad">
            <li><a href="{{ url('/home') }}">Home</a> <i class="fa fa-angle-right"></i></li>
            <li><a href="">Loan</a> <i class="fa fa-angle-right"></i></li>
            <li><a href="">Advance</a> <i class="fa fa-angle-right"></i></li>
            <li>Payment Form</li>
        </ul>
    </div>
</div>
@endsection

@section('content')
<div class="col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title">{{$page_title}}</h3>
        </div>
        <div class="panel-body">
            {!! Form::open(['method' => 'POST', 'url' => '/essential/bag_type', 'class' => 'form-horizontal']) !!}
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-6">
                        <label class="mb-2 fw-bold" for="date">Date</label>
                        <div class="input-group date" data-provide="datepicker">
                            <input type="text" class="form-control" name="date" id="date" value="02-21-2022" readonly>
                            <div class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <label class="mb-2 fw-bold" for="session">Session </label>
                        <select name="session" id="session" class="form-control" readonly>
                            <option value="2022">2022</option>
                        </select>
                    </div>
                    <div class="col-md-12">
                        <label class="mb-2 fw-bold" for="agent_id">Agent </label>
                        <select name="agent_id" id="agent_id" class="form-control select2search">
                            <option value="">Select Agent</option>
                            @foreach($agents as $agent)
                            <option value="{{$agent->id}}">{{ $agent->name}} [{{$agent->code}}] - {{$agent->agent_type}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-6">
                        <label class="mb-2 fw-bold" for="previous_date">Previous Date </label>
                        <input type="text" name="previous_date" id="previous_date" class="form-control" readonly>
                    </div>
                    <div class="col-md-6">
                        <label class="mb-2 fw-bold" for="previous_loan">Previous Loan </label>
                        <input type="text" name="previous_loan" id="previous_loan" class="form-control" readonly>
                    </div>
                    <div class="col-md-6">
                        <label class="mb-2 fw-bold" for="day_count">Day Count</label>
                        <input type="text" name="day_count" id="day_count" class="form-control" readonly>
                    </div>
                    <div class="col-md-6">
                        <label class="mb-2 fw-bold" for="interest_count">Interest Count</label>
                        <select name="interest_count" id="interest_count" class="form-control select2search" readonly>
                            <option value="">Select</option>
                            <option value="1">Yes</option>
                            <option value="0">No</option>
                        </select>
                    </div>
                    <div class="col-md-6">
                        <label class="mb-2 fw-bold" for="interest_rate">Interest Rate</label>
                        <input type="text" name="interest_rate" id="interest_rate" class="form-control" readonly>
                    </div>
                    <div class="col-md-6">
                        <label class="mb-2 fw-bold" for="interest_amount">Interest Amount</label>
                        <input type="text" name="interest_amount" id="interest_amount" class="form-control" readonly>
                    </div>
                    <div class="col-md-12">
                        <label class="mb-2 fw-bold" for="previous_total">Previous Total</label>
                        <input type="text" name="previous_total" id="previous_total" class="form-control" readonly>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-12">
                        <label class="mb-2 fw-bold" for="cash_amount">Cash Amount </label>
                        <div class="input-group">
                            <input type="number" class="form-control" id="cash_amount" name="cash_amount">
                            <div class="input-group-addon">Tk</div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <label class="mb-2 fw-bold" for="total_loan">Total Loan</label>
                        <div class="input-group">
                            <input type="number" class="form-control" id="total_loan" name="total_loan" readonly>
                            <div class="input-group-addon">Tk</div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <label class="mb-2 fw-bold" for="bag_id">Bag</label>
                        <select name="bag_id" id="bag_id" class="form-control select2search">
                            <option value="">Select Bag</option>
                            @foreach($bags as $bag)
                            <option value="{{ $bag->id }}">{{ $bag->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-6">
                        <label class="mb-2 fw-bold" for="bag_rate">Rate</label>
                        <div class="input-group">
                            <input type="number" class="form-control" id="bag_rate" name="bag_rate" value="" readonly>
                            <div class="input-group-addon">Tk</div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <label class="mb-2 fw-bold" for="bag_qty">Quantity</label>
                        <div class="input-group">
                            <input type="number" class="form-control" id="bag_qty" name="bag_qty">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <label class="mb-2 fw-bold" for="bag_amount">Bag Amount</label>
                        <div class="input-group">
                            <input type="number" class="form-control" id="bag_amount" name="bag_amount" readonly>
                            <div class="input-group-addon">Tk</div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <label class="mb-2 fw-bold" for="carrying_amount">Carrying Amount</label>
                        <div class="input-group">
                            <input type="number" class="form-control" id="carrying_amount" name="carrying_amount">
                            <div class="input-group-addon">Tk</div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <label class="mb-2 fw-bold" for="note">Note</label>
                        <div class="input-group">
                            <textarea name="note" id="note" class="form-control" cols="100"></textarea>
                        </div>
                    </div>
                </div>

            </div>
            <div class="col-md-8 col-md-offset-5" style="margin-top: 15px;">
                <button type="button" class="btn btn-info">Reset</button>
                <button type="submit" class="btn btn-primary">Save</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
<script>
    $(document).on("change", "#agent_id", function() {
        var _agentId = $("#agent_id").val();
        $.ajax({
            url: "{{ URL::to('advance/loan/payment/agent') }}",
            type: "post",
            data: {
                _token: csrfToken,
                agent_id: _agentId
            },
            success: function(data) {
                $("#interest_rate").val(data[0].interest_rate);
            },
            error: function(xhr, status) {
                alert('There is some error.Try after some time.');
            }
        });
    });
    $(document).on("change", "#bag_id", function() {
        var _bagId = $("#bag_id").val();
        $.ajax({
            url: "{{ URL::to('/advance/loan/payment/bag_type_ist') }}",
            type: "post",
            data: {
                _token: csrfToken,
                bagId: _bagId
            },
            success: function(data) {
                $("#bag_rate").val(data[0].per_bag_loan);
            },
            error: function(xhr, status) {
                alert('There is some error.Try after some time.');
            }
        });
    });
    var bag_per_loan = $("#bag_rate").val();
    alert(bag_per_loan)
    $(document).on("change", "#bag_qty", function(e) {
            let bag_qty = $(this).val();
        // let bag_qty = val();
        $("#bag_amount").val(bag_qty * bag_per_loan);
        e.preventDefault();
    })
</script>
@endsection