@if(!empty($dataset) && count($dataset) > 0)
<div class="table-responsive">
    <table class="table table-bordered tbl_thin" id="check">
        <tr class="bg-info" id="r_checkAll">
            <th class="text-center" style="width:3%;">#</th>
            <th>Product Type</th>
            <th>Category</th>
            <th>Product</th>
            <th>Unit</th>
            <th>Unit Size</th>
            <th>Rent Type</th>
            <th>Rent</th>
            <th class="text-center hip" style="width:10%;">Actions</th>
            <th class="text-center hip" style="width:3%;"><input type="checkbox" id="check_all" value="all"></th>
        </tr>
        <?php
        $counter = 0;
        if (isset($_GET['page']) && $_GET['page'] > 1) {
            $counter = ($_GET['page'] - 1) * $dataset->perPage();
        }
        ?>
        @foreach ($dataset as $data)
        <?php $counter++; ?>
        <tr onmouseover="change_color(this, true)" onmouseout="change_color(this, false)">
            <td class="text-center">{{ $counter }}</td>
            <td>{{ !empty($data->product_type) ? $data->product_type->name : "" }}</td>
            <td>{{ !empty($data->categories) ? $data->categories->name : "" }}</td>
            <td>{{ !empty($data->products) ? $data->products->name : "" }}</td>
            <td>{{ !empty($data->units) ? $data->units->unit : ""}}</td>
            <td>{{ !empty($data->unitsize) ? $data->unitsize->weight : ""}}</td>
            <td>{{ !empty($data->rent_types) ? $data->rent_types->rent_type : ""}}</td>
            <td>{{ $data->rent }}</td>
            <td class="text-center hip" style="width:10%;">
                @if(has_user_access('rent_edit'))
                <a class="btn btn-info btn-xs" href="{{ url('rent/'.$data->id.'/edit') }}"><i class="fa fa-edit"></i> Edit</a>
                @endif
            </td>
            <td class="text-center hip">
                @if(has_user_access('rent_delete'))
                <input type="checkbox" name="data[]" value="{{ $data->id }}">
                @endif
            </td>
        </tr>
        @endforeach
    </table>
</div>
<div class="mb_10 text-center hip" id="apaginate">
    {{ $dataset->render() }}
</div>
@else
<div class="alert alert-info">No records found.</div>
@endif