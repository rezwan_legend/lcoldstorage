@extends('layouts.app')

@section('breadcrumbs')
    <div id="breadcrumbBar" class="breadcrumb site_nav_links no_bdr_rad clearfix">
        <div class="col-md-3 col-sm-3 col-xs-2 no_pad">
            <button class="btn btn-info btn-xs" type="button" onclick="history.back()" title="Go Back"><span
                    class="visible-xs"><i class="fa fa-arrow-left"></i></span><span
                    class="hidden-xs">Back</span></button>
            <button class="btn btn-info btn-xs" onclick="redirectTo('<?= url('rent') ?>')" title="Refresh"
                type="button"><span class="visible-xs"><i class="fa fa-refresh"></i></span><span
                    class="hidden-xs">Refresh</span></button>
        </div>
        <div class="col-md-6 col-sm-6 hidden-xs text-center">
            <h2 class="page-title">{{ $breadcrumb_title }}</h2>
        </div>
        <div class="col-md-3 col-sm-3 col-xs-10 no_pad">
            <ul class="text-right no_mrgn no_pad">
                <li><a href="{{ url('/home') }}">Home</a> <i class="fa fa-angle-right"></i></li>
                <li>Rent</li>
            </ul>
        </div>
    </div>
@endsection

@section('content')
    <div class="well">
        <div class="clearfix mb_10">
            <div class="text-center">
                    @if (has_user_access('rent_create'))
                        <a class="btn btn-success btn-xs " href="{{ url('rent/create') }}"><i
                                class="fa fa-plus"></i> New</a>
                    @endif
                    @if (has_user_access('rent_delete'))
                        <button type="button" class="btn btn-danger btn-xs " id="Del_btn" disabled><i
                                class="fa fa-trash-o"></i> Delete</button>
                    @endif
                    <button class="btn btn-primary btn-xs " onclick="printDiv('print_area')"><i
                            class="fa fa-print"></i> Print</button>
               
            </div>
        </div>
        <table width="100%">
            <tr>
                <td class="wmd_70">
                    {!! Form::open(['method' => 'POST', 'class' => 'search-form', 'id' => 'frmSearch', 'name' => 'frmSearch']) !!}
                    <div class="input-group">
                        <div class="input-group-btn clearfix">
                            <?php echo number_dropdown(50, 500, 50, null, 'xsw_30'); ?>
                            <div class="col-md-2 col-sm-3 xsw_70 no_pad">
                                <select class="form-control select2search" id="product_type_id" name="product_type_id"
                                    required>
                                    <option value="">Select Type</option>
                                    @foreach ($product_type as $type)
                                        <option value="{{ $type->id }}">{{ $type->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-md-2 col-sm-3 xsw_50 no_pad">
                                <select class="form-control select2search" id="category_id" name="category_id">
                                    <option value="">Select Category</option>
                                </select>
                            </div>
                            <div class="col-md-2 col-sm-3 xsw_50 no_pad">
                                <select class="form-control select2search" id="product_id" name="product_id">
                                    <option value="">Select Product</option>
                                </select>
                            </div>
                            <div class="col-md-2 col-sm-3 xsw_50 no_pad">
                                <select class="form-control select2search" id="unit_id" name="unit_id" required>
                                    <option value="">Select Unit</option>
                                    @foreach ($units as $unit)
                                        <option value="{{ $unit->id }}"> {{ $unit->unit }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-md-2 col-sm-3 xsw_50 no_pad">
                                <select class="form-control select2search" id="unit_size" name="unit_size" required>
                                    <option value="">Select Unit Size</option>
                                </select>
                                <small class="text-danger">{{ $errors->first('unit_size') }}</small>
                            </div>
                            <div class="col-md-2 col-sm-3 xsw_50 no_pad">
                                <select class="form-control select2search" id="rent_type_id" name="rent_type_id" required>
                                    <option value="">Select Rent Type</option>
                                    @foreach ($rentTypes as $rentType)
                                        <option value="{{ $rentType->id }}">{{ $rentType->rent_type }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-md-2 col-sm-3 xsw_50 no_pad">
                                <input type="text" name="search" id="srch" class="form-control" placeholder="search"
                                    >
                            </div>
                            <div class="col-md-2 col-sm-3 xsw_100 no_pad">
                                <button type="button" id="search" class="btn btn-info xsw_50">Search</button>
                                <button type="button" id="clear_from" class="btn btn-warning xsw_50">Clear</button>
                            </div>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </td>
                
            </tr>
        </table>
    </div>

    {!! Form::open(['method' => 'POST', 'id' => 'frmList', 'name' => 'frmList']) !!}
    <div id="print_area">
        <?php print_header('Category List', true, false); ?>
        <div id="ajax_content">
            @if (!empty($dataset))
                <div class="table-responsive">
                    <table class="table table-bordered tbl_thin" id="check">
                        <tr class="bg-info" id="r_checkAll">
                            <th class="text-center" style="width:3%;">#</th>
                            <th>Product Type</th>
                            <th>Category</th>
                            <th>Product</th>
                            <th>Unit</th>
                            <th>Unit Size</th>
                            <th>Rent Type</th>
                            <th>Rent</th>
                            <th class="text-center hip">Actions</th>
                            <th class="text-center hip" style="width:3%;"><input type="checkbox" id="check_all" value="all">
                            </th>
                        </tr>
                        <?php
                        $counter = 0;
                        if (isset($_GET['page']) && $_GET['page'] > 1) {
                            $counter = ($_GET['page'] - 1) * $dataset->perPage();
                        }
                        ?>
                        @foreach ($dataset as $data)
                            <?php $counter++; ?>
                            <tr onmouseover="change_color(this, true)" onmouseout="change_color(this, false)">
                                <td class="text-center">{{ $counter }}</td>
                                <td>{{ !empty($data->product_type) ? $data->product_type->name : '' }}</td>
                                <td>{{ !empty($data->categories) ? $data->categories->name : '' }}</td>
                                <td>{{ !empty($data->products) ? $data->products->name : '' }}</td>
                                <td>{{ !empty($data->units) ? $data->units->unit : '' }}</td>
                                <td>{{ !empty($data->unitsize) ? $data->unitsize->name : '' }}</td>
                                <td>{{ !empty($data->rent_types) ? $data->rent_types->rent_type : '' }}</td>
                                <td>{{ $data->rent }}</td>
                                <td class="text-center hip">
                                    @if (has_user_access('rent_edit'))
                                        <a class="btn btn-info btn-xs" href="rent/{{ $data->id }}/edit"><i
                                                class="fa fa-edit"></i> Edit</a>
                                    @endif
                                </td>
                                <td class="text-center hip">
                                    @if (has_user_access('rent_delete'))
                                        <input type="checkbox" name="data[]" value="{{ $data->id }}">
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    </table>
                </div>
                <div class="mb_10 text-center hip">
                    {{ $dataset->render() }}
                </div>
            @else
                <div class="alert alert-info">No records found.</div>
            @endif
        </div>
    </div>
    {!! Form::close() !!}

    <script type="text/javascript">
        $(document).on("change", "#product_type_id", function() {
            var _productType = $("#product_type_id").val();
            $.ajax({
                url: "{{ URL::to('rent/categoryByType') }}",
                type: "post",
                data: {
                    _token: csrfToken,
                    productTypeId: _productType
                },
                success: function(res) {
                    $('#category_id').html(res);
                },
                error: function(xhr, status) {
                    alert('There is some error.Try after some time.');
                }
            });

        });
        $(document).on("change", "#category_id", function() {
            var _product_id = $("#category_id").val();
            $.ajax({
                url: "{{ URL::to('rent/productByCategory') }}",
                type: "post",
                data: {
                    _token: csrfToken,
                    product_id: _product_id
                },
                success: function(res) {
                    $('#product_id').html(res);
                },
                error: function(xhr, status) {
                    alert('There is some error.Try after some time.');
                }
            });

        });
        $(document).on("change", "#unit_id", function() {
            var _Unit_id = $("#unit_id").val();
            $.ajax({
                url: "{{ URL::to('rent/product_sizeByUnit') }}",
                type: "post",
                data: {
                    _token: csrfToken,
                    UnitSizeId: _Unit_id
                },
                success: function(res) {
                    $('#unit_size').html(res);
                },
                error: function(xhr, status) {
                    alert('There is some error.Try after some time.');
                }
            });

        });
    </script>
    <script type="text/javascript">
        $(document).ready(function() {
            $(document).on("click", "#search", function() {
                var _url = "{{ URL::to('rent/search') }}";
                var _form = $("#frmSearch");
                $.ajax({
                    url: _url,
                    type: "post",
                    data: _form.serialize(),
                    success: function(res) {
                        $('#ajax_content').html(res);
                    },
                    error: function(xhr, status) {
                        alert('There is some error.Try after some time.');
                    }
                });
            });

            $(document).on("click", "#Del_btn", function() {
                var _url = "{{ URL::to('rent/delete') }}";
                var _form = $("#frmList");
                var _rc = confirm("Are you sure about this action? This cannot be undone!");
                if (_rc == true) {
                    $.post(_url, _form.serialize(), function(resp) {
                        if (resp.success === true) {
                            $('#ajaxMessage').showAjaxMessage({
                                html: resp.message,
                                type: 'success'
                            });
                            $("#search").trigger("click");
                        } else {
                            $('#ajaxMessage').showAjaxMessage({
                                html: resp.message,
                                type: 'error'
                            });
                        }
                    }, "json");
                }
            });
        });
    </script>
@endsection
