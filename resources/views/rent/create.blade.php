@extends('layouts.app')

@section('breadcrumbs')
<div id="breadcrumbBar" class="breadcrumb site_nav_links no_bdr_rad clearfix">
    <div class="col-md-3 col-sm-3 col-xs-2 no_pad">
        <button class="btn btn-info btn-xs" type="button" onclick="history.back()" title="Go Back"><span class="visible-xs"><i class="fa fa-arrow-left"></i></span><span class="hidden-xs">Back</span></button>
        <button class="btn btn-info btn-xs" onclick="redirectTo('<?= url('/rent/create'); ?>')" title="Refresh" type="button"><span class="visible-xs"><i class="fa fa-refresh"></i></span><span class="hidden-xs">Refresh</span></button>
    </div>
    <div class="col-md-6 col-sm-6 hidden-xs text-center">
        <h2 class="page-title">{{$breadcrumb_title}}</h2>
    </div>
    <div class="col-md-3 col-sm-3 col-xs-10 no_pad">
        <ul class="text-right no_mrgn no_pad">
            <li><a href="{{ url('/home') }}">Home</a> <i class="fa fa-angle-right"></i></li>
            <li><a href="{{ url('/rent') }}">Rent</a> <i class="fa fa-angle-right"></i></li>
            <li>Form</li>
        </ul>
    </div>
</div>
@endsection

@section('content')
<div class="panel panel-primary">
    <div class="panel-heading">
        <h3 class="panel-title">Enter Rent Size</h3>
    </div>
    {!! Form::open(['method' => 'POST', 'url' => 'rent', 'class' => 'form-horizontal']) !!}
    <div class="panel-body">
        {!! Form::open(['method' => 'POST', 'class' => 'search-form', 'id' => 'frmSearch', 'name' => 'frmSearch']) !!}
        <div class="form-group">
            <label class="col-md-4 control-label" for="unit">Product Type</label>
            <div class="col-md-6">
                <select class="form-control select2search" id="product_type_id" name="product_type_id" required>
                    <option value="">Select Type</option>
                    @foreach($product_type as $type)
                    <option value="{{$type->id}}">{{$type->name}}</option>
                    @endforeach
                </select>
                <small class="text-danger">{{ $errors->first('product_type') }}</small>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-4 control-label" for="unit">Category</label>
            <div class="col-md-6">
                <select class="form-control select2search" id="category_id" name="category_id">
                    <option value="">Select Category</option>
                </select>
                <small class="text-danger">{{ $errors->first('category') }}</small>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-4 control-label" for="unit">Product</label>
            <div class="col-md-6">
                <select class="form-control select2search" id="product_id" name="product_id">
                    <option value="">Select Product</option>
                </select>
                <small class="text-danger">{{ $errors->first('product') }}</small>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-4 control-label" for="unit">Unit</label>
            <div class="col-md-6">
                <select class="form-control select2search" id="unit_id" name="unit_id" required>
                    <option value="">Select Unit</option>
                    @foreach($units as $unit)
                    <option value="{{$unit->id}}">{{$unit->unit}}</option>
                    @endforeach
                </select>
                <small class="text-danger">{{ $errors->first('unit_size') }}</small>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-4 control-label" for="unit_size">Unit Size</label>
            <div class="col-md-6">
                <select class="form-control select2search" id="unit_size" name="unit_size" required>
                    <option value="">Select Unit Size</option>
                </select>
                <small class="text-danger">{{ $errors->first('unit_size') }}</small>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-4 control-label" for="unit">Rent Type</label>
            <div class="col-md-6">
                <select class="form-control select2search" id="rent_type_id" name="rent_type_id" required>
                    <option value="">Select Rent Type</option>
                    @foreach($rentTypes as $rentType)
                    <option value="{{$rentType->id}}">{{$rentType->rent_type}}</option>
                    @endforeach
                </select>
                <small class="text-danger">{{ $errors->first('rent_type_id') }}</small>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-4 control-label" for="category_id">Rent</label>
            <div class="col-md-6">
                <input type="text" class="form-control" id="rent" name="rent" required>
                <small class="text-danger">{{ $errors->first('rent') }}</small>
            </div>
        </div>
        <div class="col-md-8 col-md-offset-4">
            <button type="button" class="btn btn-info" id="resetbtn">Reset</button>
            <button type="submit" class="btn btn-primary">Save</button>
        </div>
        {!! Form::close() !!}
    </div>
    {!! Form::close() !!}
</div>
<script type="text/javascript">
    $(document).on("change", "#product_type_id", function() {
        var _productType = $("#product_type_id").val();
        $.ajax({
            url: "{{ URL::to('rent/categoryByType') }}",
            type: "post",
            data: {
                _token: csrfToken,
                productTypeId: _productType
            },
            success: function(res) {
                $('#category_id').html(res);
            },
            error: function(xhr, status) {
                alert('There is some error.Try after some time.');
            }
        });

    });

    $(document).on("change", "#category_id", function() {
        var _product_id = $("#category_id").val();
        $.ajax({
            url: "{{ URL::to('rent/productByCategory') }}",
            type: "post",
            data: {
                _token: csrfToken,
                product_id: _product_id
            },
            success: function(res) {
                $('#product_id').html(res);
            },
            error: function(xhr, status) {
                alert('There is some error.Try after some time.');
            }
        });

    });
    $(document).on("change", "#unit_id", function() {
        var _Unit_id = $("#unit_id").val();
        $.ajax({
            url: "{{ URL::to('rent/product_sizeByUnit') }}",
            type: "post",
            data: {
                _token: csrfToken,
                UnitSizeId: _Unit_id
            },
            success: function(res) {
                $('#unit_size').html(res);
            },
            error: function(xhr, status) {
                alert('There is some error.Try after some time.');
            }
        });

    });
</script>
@endsection