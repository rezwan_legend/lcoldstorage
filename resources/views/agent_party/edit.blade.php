@extends('layouts.app')

@section('breadcrumbs')
<div id="breadcrumbBar" class="breadcrumb site_nav_links no_bdr_rad clearfix">
    <div class="col-md-3 col-sm-3 col-xs-2 no_pad">
        <button class="btn btn-info btn-xs" type="button" onclick="history.back()" title="Go Back"><span class="visible-xs"><i class="fa fa-arrow-left"></i></span><span class="hidden-xs">Back</span></button>
        <button class="btn btn-info btn-xs" onclick="redirectTo('<?= url('view-clear'); ?>')" title="Refresh" type="button"><span class="visible-xs"><i class="fa fa-refresh"></i></span><span class="hidden-xs">Refresh</span></button>
    </div>
    <div class="col-md-6 col-sm-6 hidden-xs text-center">
        <h2 class="page-title">{{$breadcrumb_title}}</h2>
    </div>
    <div class="col-md-3 col-sm-3 col-xs-10 no_pad">
        <ul class="text-right no_mrgn no_pad">
            <li><a href="{{ url('/home') }}">Home</a> <i class="fa fa-angle-right"></i></li>
            <li><a href="{{url('/essential/agent_list')}}">Agent/Party</a></li>
        </ul>
    </div>
</div>
@endsection

@section('content')
<div class="panel panel-primary">
    <div class="panel-heading">
        <h3 class="panel-title">Update Agent/Party Information</h3>
    </div>
    <div class="panel-body">
        {!! Form::open(['method' => 'PUT', 'url' => '/essential/agent_list/'.$data->id, 'class' => 'form-horizontal']) !!}

            <div class="col-md-6 col-md-offset-2">
                <div class="col-md-12 py-3">
                <label class="mb-2 " for="name">Select</label>
                    <select name="agent_type" id="" class="form-control">
                        <option >Select</option>
                        <option  {{ $data->agent_type == 'agent'?'selected':''}} value="agent">
                                Agent
                        </option>
                        <option 
                            {{ $data->agent_type == 'party'?'selected':''}}
                            value="party">Party
                        </option>
                    </select>
                </div>
                    <div class="col-md-12 py-3">
                        <label class="mb-2 " for="name">Name</label>
                        <input type="text" name="name" id="name" class="form-control" value="{{$data->name}}" required>
                    </div>
                    <div class="col-md-12 py-3">
                        <label class="mb-2 " for="fname">Father Name</label>
                        <input type="text" name="father_name" id="fname" class="form-control" value="{{$data->father_name}}" required>
                    </div>
                    <div class="col-md-12 py-3">
                        <label class="mb-2 " for="code">Code</label>
                        <input type="number" name="code" id="code" class="form-control" value="{{$data->code}}"  maxlength="10">
                    </div>
                    <div class="col-md-12 py-3">
                        <label class="mb-2 " for="mobile">Mobile</label>
                        <input type="text" name="mobile" id="mobile" class="form-control" value="{{$data->mobile}}" required maxlength="14">
                    </div>

                    <div class="col-md-12 py-3">
                        <label class="mb-2 " for="address">Address</label>
                        <textarea type="textarea" name="address" id="address" class="form-control">{{$data->address}}</textarea>
                    </div>

                    <div class="col-md-12 py-3">
                        <label class="mb-2 " for="Commission">Per Bag Commission</label>
                        <input type="text" name="per_bag_commission" id="Commission" class="form-control" value="{{$data->per_bag_commission}}" maxlength="4">
                    </div>
                    <div class="col-md-12 py-3">
                        <label class="mb-2 " for="Interest">Interest Rate</label>
                        <input type="text" name="interest_rate" id="Interest" class="form-control" value="{{$data->interest_rate}}" maxlength="4">
                    </div>   
            </div>

        <div class="col-md-8 col-md-offset-4">
            <button type="button" class="btn btn-info" onclick="history.back()" title="Go Back">Cancel</button>
            <button type="submit" class="btn btn-primary">Update</button>
        </div>
        {!! Form::close() !!}
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        var _branch_id = $("#branch_id").val();
        if (_branch_id === " ") {
            $("#conditional_branch").hide();
        }

        $(document).on("change", "#company_id", function () {
            $.ajax({
                url: "{{ URL::to('institute/components') }}",
                type: "post",
                data: {institute: this.value, _token: _token},
                success: function (res) {
                    $('#product_type_id').html(res.product_types);
                },
                error: function (xhr, status) {
                    alert('There is some error.Try after some time.');
                }
            });
        });

        $(document).on("change", "#product_type_id", function () {
            var _company_id = $("#company_id").val();
            $.ajax({
                url: "{{ URL::to('category/categoryByType') }}",
                type: "post",
                data: {companyId: _company_id, productTypeId: this.value, _token: _token},
                success: function (res) {
                    $('#category_id').html(res.category);
                },
                error: function (xhr, status) {
                    alert('There is some error.Try after some time.');
                }
            });
        });
    });
</script>
@endsection