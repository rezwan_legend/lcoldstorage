@if(!empty($dataset) && count($dataset) > 0)
<div class="table-responsive">
    <table class="table table-bordered tbl_thin" id="check">
        <tr class="bg-info" id="r_checkAll">
            <th class="text-center hip" style="width:3%;"><input type="checkbox" id="check_all" value="all"></th>
            <th>Product Name</th>
            <th class="text-center">Quantity (SFT)</th>
            <th class="text-center">Per SFT Price</th>
            <th class="text-center">Total Price(Tk)</th>
        </tr>
        <?php
        $counter = 0;
        if (isset($_GET['page']) && $_GET['page'] > 1) {
            $counter = ($_GET['page'] - 1) * $dataset->perPage();
        }
        ?>
        @foreach ($dataset as $data)
        <?php $counter++; ?>
        <tr onmouseover="change_color(this, true)" onmouseout="change_color(this, false)">
            <td class="text-center hip">
                <input type="checkbox" class="single_check" name="product[]" value="{{ $data->id }}">
                <input type="hidden" value="{{ $data->category_id }}" name="category_id[{{ $data->id }}]">
            </td>
            <td>{{ $data->name }}</td>
            <td class="text-center"><input type="number" id="quantity_{{ $data->id }}" data-id="{{ $data->id }}" step="any" name="quantity[{{ $data->id }}]" class="form-control text-center qty"></td>
            <td class="text-center"><input type="number" id="per_sft_price_{{ $data->id }}" data-id="{{ $data->id }}" step="any" name="per_sft_price[{{ $data->id }}]" class="form-control text-center qty"></td>
            <td class="text-center"><input type="number" id="total_price_{{ $data->id }}" data-id="{{ $data->id }}" step="any" name="total_price[{{ $data->id }}]" class="form-control text-center" readonly></td>
        </tr>
        @endforeach
    </table>
</div>
@else
<div class="alert alert-info">No records found.</div>
@endif