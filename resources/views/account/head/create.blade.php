@extends('layouts.app')

@section('breadcrumbs')
    <div id="breadcrumbBar" class="breadcrumb site_nav_links no_bdr_rad clearfix">
        <div class="col-md-3 col-sm-3 col-xs-2 no_pad">
            <button class="btn btn-info btn-xs" type="button" onclick="history.back()" title="Go Back"><span
                    class="visible-xs"><i class="fa fa-arrow-left"></i></span><span
                    class="hidden-xs">Back</span></button>
            <button class="btn btn-info btn-xs" onclick="redirectTo('<?= url('head/create') ?>')" title="Refresh"
                type="button"><span class="visible-xs"><i class="fa fa-refresh"></i></span><span
                    class="hidden-xs">Refresh</span></button>
        </div>
        <div class="col-md-6 col-sm-6 hidden-xs text-center">
            <h2 class="page-title">{{ $page_title }}</h2>
        </div>
        <div class="col-md-3 col-sm-3 col-xs-10 no_pad">
            <ul class="text-right no_mrgn no_pad">
                <li><a href="{{ url('/home') }}">Home</a> <i class="fa fa-angle-right"></i></li>
                <li><a href="{{ url('/head') }}">Head</a> <i class="fa fa-angle-right"></i></li>
                <li>Form</li>
            </ul>
        </div>
    </div>
@endsection

@section('content')
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title">Enter Head</h3>
        </div>
        <div class="panel-body">

            <div class="form-group align-">
                <div class="col-md-5">
                    <label for="name" class="control-label">Name</label>
                    <input type="text" class="form-control" name="name" id="name" placeholder="head name" required>
                </div>
                <div class="col-md-3">
                    <label for="code" class="control-label">Code</label>
                    <input type="number" class="form-control" name="code" id="code" placeholder="code no">

                </div>
                <div class="col-md-3">
                    <label for="common_id" class="control-label">Common</label>
                    <select class="form-control select2search" name="common_id" id="common_id" required>
                        <option value="">Select Common</option>
                        <option value="yes">Yes</option>
                        <option value="no">No</option>
                    </select>
                    <small class="text-danger">{{ $errors->first('category_id') }}</small>

                </div>
                <div class="form-group col-md-1">
                    <button style="padding: 5px; margin-top:25px;" class="text-center"><span id="addRowItem"
                            style="border-radius:50%;color:white; cursor: pointer; padding:5px;background:#383838"><i
                                class="fa fa-plus"></i></span></button>
                </div>
            </div>
        </div>
    </div>
    <div class="clearfix">
        <div class="clearfix" id="item_list">
            <table class="table table-bordered tbl_thin" style="margin: 0">
                <tr class="bg-primary">
                    <th class="text-center"><span>Head details</span></th>
                </tr>
            </table>
            <form id="frmList" action="{{ route('head.store') }}">
                <div class="table-responsive">
                    <table class="table table-bordered tbl_thin" id="check">
                        <thead>
                            <tr class="bg-info" id="r_checkAll">
                                <th>Name</th>
                                <th>Code</th>
                                <th>Common</th>
                                <th class="text-center" style="width: 3%;"></th>
                            </tr>
                        </thead>
                        <tbody id="headitems">

                        </tbody>
                    </table>
                </div>
            </form>
        </div>
    </div>

    <div class="text-center" style="margin-top:10px;">
        <button id="itemInsertBtn" class="btn btn-primary xsw_100" disabled>Save</button>
    </div>
    <script type="text/javascript">
        // Dropdown Validation
        const isDropdownSelected = () => {
            let status = true;
            if ($('#name').val() == '') {
                toastr.error('Name field must be Required');
                status = false;
            }
            if ($('#code').val() === '') {
                toastr.error('Code field must be Required');
                status = false;
            }
            if ($('#common_id').val() === '') {
                toastr.error('Please select Common dropdown');
                status = false;
            }
            return status;
        }

        var addedProductId = [];
        const addItemId = () => {
            let object = {};
            let name = $("#name").val();
            object["name"] = name;

            addedProductId.push(object);
             enable(itemInsertBtn);
        }

        // Table Data generate Function
        const getDomeUi = () => {
            addItemId();
            return `<tr>
        <td>${$('#name').val()}
            <input class="name" data-id="${$('#name').val()}"  type="hidden" name="name[]" value="${$('#name').val()}">
        </td>
        <td>${$('#code').val()}
            <input class="code" data-id="${$('#code').val()}"  type="hidden" name="code[]" value="${$('#code').val()}">
        </td>
        <td>${$('#common_id option:selected').text()}
            <input class="common_id" data-id="${$('#common_id').val()}"  type="hidden" name="common_id[]" value="${$('#common_id').val()}">
        </td>
        <td><i id='deleteItem' class='fa fa-trash' role='button'></i></td>
        </tr>`;
        }

        // Table Row Create function
        const addRow = () => {
            if (isDropdownSelected() === false) {
                return false;
            }
            for (let item of addedProductId) {

                if (item["name"] == $('#name').val()) {
                    toastr.warning('Item already added');
                    return false;
                }
            }
            document
                .querySelector("#headitems")
                .insertAdjacentHTML("beforeend", getDomeUi());
        }

        // Table Row Remove
        const removeRow = () => {
            $(document).on("click", "#deleteItem", function() {
                let name = $(this).parents('tr').find('.name').val();
                for (let index in addedProductId) {
                    if (addedProductId[index]["name"] == name) {
                        addedProductId.splice(index, 1)
                    }
                }
                $(this).parents("tr").remove();
                 disable(itemInsertBtn);
                toastr.success("Record Deleted Successfully!");
            })
        }

        // Table Row Insert Function
        const processOnSubmit = () => {
            let url = "{{ route('head.store') }}";
            let itemForm = $("#frmList").serialize();

            axios.post(url, itemForm)
                .then((response) => {
                    toastr.success(response.data);
           
                    window.location = "{{ route('head.index') }}";

                })
                .catch((error) => {
                    toastr.error("Something went Wrong!");
                })
        }
        


          $(document).on("click", "#itemInsertBtn", function(e) {
                disable(itemInsertBtn);
                return true;
                e.preventDefault();
            });

            // $(document).on("change", "#common_id", function() {
            //     enable(itemInsertBtn);
            //     return true;
            // });

        // Table Row Create and Save Btn click function
        $(document).ready(function() {
            $('#addRowItem').click(addRow)
            removeRow();
            $('#itemInsertBtn').click(function() {
                processOnSubmit();
            })



          


        })
    </script>
@endsection
