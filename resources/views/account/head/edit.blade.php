@extends('layouts.app')

@section('breadcrumbs')
<div id="breadcrumbBar" class="breadcrumb site_nav_links no_bdr_rad clearfix">
    <div class="col-md-3 col-sm-3 col-xs-2 no_pad">
        <button class="btn btn-info btn-xs" type="button" onclick="history.back()" title="Go Back"><span class="visible-xs"><i class="fa fa-arrow-left"></i></span><span class="hidden-xs">Back</span></button>
        <button class="btn btn-info btn-xs" onclick="redirectTo('<?= url('view-clear'); ?>')" title="Refresh" type="button"><span class="visible-xs"><i class="fa fa-refresh"></i></span><span class="hidden-xs">Refresh</span></button>
    </div>
    <div class="col-md-6 col-sm-6 hidden-xs text-center">
        <h2 class="page-title">Update Head </h2>
    </div>
    <div class="col-md-3 col-sm-3 col-xs-10 no_pad">
        <ul class="text-right no_mrgn no_pad">
            <li><a href="{{ url('/home') }}">Home</a> <span class="fa fa-angle-right"></span></li>
            <li><a href="{{ url('/head') }}">Head <span class="fa fa-angle-right"></span></a></li>
            <li>Form</li>
        </ul>
    </div>
</div>
@endsection

@section('content')
<div class="panel panel-primary">
    <div class="panel-heading">
        <h3 class="panel-title">Update Head Information</h3>
    </div>
    <div class="panel-body">
        {!! Form::open(['method' => 'PUT', 'url' => 'head/'.$data->id, 'class' => 'form-horizontal']) !!}
        <div class="form-group">
            <label for="name" class="col-md-4 control-label">Name</label>
            <div class="col-md-6">
                <input type="text" class="form-control" name="name" value="{{$data->name}}">
                <small class="text-danger">{{ $errors->first('name') }}</small>
            </div>
        </div>
        <div class="form-group">
            <label for="code" class="col-md-4 control-label">Code</label>
            <div class="col-md-6">
                <input type="text" class="form-control" name="code" value="{{$data->code}}">
                <small class="text-danger">{{ $errors->first('code') }}</small>
            </div>
        </div>
        <div class="form-group">
            <label for="common" class="col-md-4 control-label">Common</label>
            <div class="col-md-6">
                <select class="form-control" id="common" name="common">
                    <option value="{{ COMMON_YES }}" <?php if ($data->common == COMMON_YES) echo ' selected'; ?>>{{ COMMON_YES }}</option>
                    <option value="{{ COMMON_NO }}" <?php if ($data->common == COMMON_NO) echo ' selected'; ?>>{{ COMMON_NO }}</option>
                </select>
                <small class="text-danger">{{ $errors->first('common') }}</small>
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-8 col-md-offset-4">
                <input type="submit" class="btn btn-primary xsw_50" name="btnSave" value="Update">
            </div>
        </div>
        {!! Form::close() !!}
    </div>
</div>
@endsection