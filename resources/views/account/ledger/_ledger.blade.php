<?php if (!empty($dataset) && count($dataset) > 0) : ?>
    <div class="table-responsive">           
        <table class="table table-bordered tbl_thin" id="check">
            <tr class="bg-info" id="r_checkAll">
                <th class="text-center" style="width:5%;">SL#</th>
                <th>Head Name</th>
                <th class="text-right" style="width:130px">Debit</th>
                <th class="text-right" style="width:130px">Credit</th>
                <th class="text-right" style="width:130px">Balance</th>
            </tr>
            <?php
            $counter = 0;
            foreach ($dataset as $data):
                $counter++;
                $total_debit = 0;
                $total_credit = 0;
                $total_balance = 0;
                ?>   
                <tr onmouseover="change_color(this, true)" onmouseout="change_color(this, false)">
                    <td class="text-center">{{ change_lang($counter) }}</td>
                    <td title="View All Transaction">
                        <span style="font-weight: 600; color: #337ab7;" class="hip"><a href="{{ url('ledger/head/'.$data->id) }}">{{ $data->name }}</a></span>
                        <span class="show_in_print">{{ $data->name }}</span>
                    </td>
                    <td class="text-right">{{ change_lang($tmodel->headDebit($data->id)) }}</td>
                    <td class="text-right">{{ change_lang($tmodel->headCredit($data->id)) }}</td>
                    <td class="text-right">{{ change_lang($tmodel->sumHeadBalance($data->id)) }}</td>                    
                </tr>
                <?php
                if (!empty($data->subheads)):
                    $_subCount = 0;
                    foreach ($data->subheads as $item):
                        $_subCount++;
                        $total_debit += $_subdebit = $tmodel->sumSubDebit($item->id);
                        $total_credit += $_subcredit = $tmodel->sumSubCredit($item->id);
                        $total_balance += $_subbalance = $tmodel->sumSubBalance($item->id);
                        ?>
                        <tr onmouseover="change_color(this, true)" onmouseout="change_color(this, false)">
                            <td class="text-center">{{ change_lang($counter) }}.{{ change_lang($_subCount) }}</td>
                            <td style="padding-left: 30px;" title="View All Transaction">
                                <span class="hip"><a href="{{ url('ledger/subhead/'.$item->id) }}">{{ $item->name }}</a></span>
                                <span class="show_in_print">{{ $item->name }}</span>
                            </td>
                            <td class="text-right">{{ change_lang($_subdebit) }}</td>
                            <td class="text-right">{{ change_lang($_subcredit) }}</td>
                            <td class="text-right">{{ change_lang($_subbalance) }}</td>
                        </tr>
                    <?php endforeach; ?>
                    <tr class="bg-warning">
                        <th class="text-right" colspan="2">Sub Total:</th>
                        <th class="text-right">{{ change_lang($total_debit) }}</th>
                        <th class="text-right">{{ change_lang($total_credit) }}</th>
                        <th class="text-right">{{ change_lang($total_balance) }}</th>
                    </tr>
                <?php endif; ?> 
            <?php endforeach; ?>
        </table>
    </div>
<?php else: ?>
    <div class="alert alert-info">No records found!</div>
<?php endif; ?>