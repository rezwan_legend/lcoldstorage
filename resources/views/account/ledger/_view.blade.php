{{ print_header("Ledger Details") }}

<?php if (!empty($dataset) && count($dataset) > 0) : ?>
    <div class="table-responsive" id="print_area">
        <table class="table table-bordered tbl_thin" id="check">
            <tr class="bg-info" id="r_checkAll">
                <th class="text-center" style="width:5%;">#</th>
                <th>Head Name</th>
                <th class="text-right" style="width:130px;">Debit</th>
                <th class="text-right" style="width:130px;">Credit</th>
                <th class="text-right" style="width:130px;">Balance</th>
            </tr>
            <?php
            $counter = 0;
            foreach ($dataset as $data):
                $counter++;
                $total_debit = 0;
                $total_credit = 0;
                $total_balance = 0;
                $total_partdebit = 0;
                $total_partcredit = 0;
                $total_partbalance = 0;
                ?>   
                <tr onmouseover="change_color(this, true)" onmouseout="change_color(this, false)">
                    <td class="text-center">{{ change_lang($counter) }}</td>
                    <td style="font-weight: 600; color: #337ab7;">{{ $data->name }}</td>
                    <td class="text-right">{{ change_lang($tmodel->headDebit($data->id)) }}</td>
                    <td class="text-right">{{ change_lang($tmodel->headCredit($data->id)) }}</td>
                    <td class="text-right">{{ change_lang($tmodel->sumHeadBalance($data->id)) }}</td>                    
                </tr>
                <?php
                if (!empty($data->subheads)):
                    $sub_counter = 0;
                    foreach ($data->subheads as $item):
                        $sub_counter++;
                        $total_debit += $_subdebit = $tmodel->sumSubDebit($item->id);
                        $total_credit += $_subcredit = $tmodel->sumSubCredit($item->id);
                        $total_balance += $_subbalance = $tmodel->sumSubBalance($item->id);
                        ?>
                        <tr onmouseover="change_color(this, true)" onmouseout="change_color(this, false)">
                            <td class="text-center">{{ change_lang($counter) }}.{{ change_lang($sub_counter) }}</td>
                            <td title="View All Transaction" style="padding-left: 30px; font-weight: 600; color: #3ab733;">{{ $item->name }}</td>
                            <td class="text-right">{{ change_lang($_subdebit) }}</td>
                            <td class="text-right">{{ change_lang($_subcredit) }}</td>
                            <td class="text-right">{{ change_lang($_subbalance) }}</td>
                        </tr>
                        <?php
                        if (!empty($item->particulars)):
                            $part_counter = 0;
                            foreach ($item->particulars as $part):
                                $part_counter++;
                                $total_partdebit += $_subpartdebit = $tmodel->sumPartDebit($part->id);
                                $total_partcredit += $_subpartcredit = $tmodel->sumPartCredit($part->id);
                                $total_partbalance += $_subpartbalance = $tmodel->sumPartBalance($part->id);
                                ?>
                                <tr onmouseover="change_color(this, true)" onmouseout="change_color(this, false)">
                                    <td class="text-center">{{ change_lang($counter) }}.{{ change_lang($sub_counter) }}.{{ change_lang($part_counter) }}</td>
                                    <td style="padding-left: 60px; color: #ea2b2b;">{{ $part->name }}, {{ $part->address }}, {{ $part->mobile }}</td>
                                    <td class="text-right">{{ change_lang($_subpartdebit) }}</td>
                                    <td class="text-right">{{ change_lang($_subpartdebit) }}</td>
                                    <td class="text-right">{{ change_lang($_subpartdebit) }}</td>
                                </tr>
                            <?php endforeach; ?>
                        <?php endif; ?> 
                    <?php endforeach; ?>
                    <tr class="bg-warning">
                        <th class="text-right" colspan="2">Sub Total:</th>
                        <th class="text-right">{{ change_lang($total_debit) }}</th>
                        <th class="text-right">{{ change_lang($total_credit) }}</th>
                        <th class="text-right">{{ change_lang($total_balance) }}</th>
                    </tr>
                <?php endif; ?> 
            <?php endforeach; ?>
        </table>
    </div>
<?php else: ?>
    <div class="alert alert-info">No records found!</div>
<?php endif; ?>