@extends('layouts.app')

@section('page_style')
@media(max-width: 767px){
.invoice_heading .print_user_info{position: static}
div.pull-right{float: left!important;}
}
@endsection

@section('content')
<div id="breadcrumbBar" class="breadcrumb site_nav_links no_bto_rad clearfix">
    <div class="col-md-3 col-sm-3 col-xs-2 no_pad">
        <button class="btn btn-info btn-xs" type="button" onclick="history.back()" title="Go Back"><span class="visible-xs"><i class="fa fa-arrow-left"></i></span><span class="hidden-xs">Back</span></button>
        <button class="btn btn-info btn-xs" onclick="redirectTo('<?= url('view-clear'); ?>')" title="Refresh" type="button"><span class="visible-xs"><i class="fa fa-refresh"></i></span><span class="hidden-xs">Refresh</span></button>
    </div>
    <div class="col-md-6 col-sm-6 hidden-xs text-center">
        <h2 class="page-title">{{ $page_title }}</h2>
    </div>
    <div class="col-md-3 col-sm-3 col-xs-10 no_pad">
        <ul class="text-right no_mrgn no_pad">
            <li><a href="{{ url('/home') }}">Home</a> <span class="fa fa-angle-right"></span></li>
            <li><a href="{{ url('/particulars') }}">Particular</a> <span class="fa fa-angle-right"></span></li>
            <li>Ledger</li>
        </ul>
    </div>
</div>

<div class="well">
    <table width="100%">
        <tr>
            <td class="wmd_70">
                {!! Form::open(['method' => 'POST', 'id' => 'frmSearch', 'name' => 'frmSearch']) !!}
                <input type="hidden" id="particularId" name="particularId" value="{{ $particular->id }}">
                <div class="input-group">
                    <div class="input-group-btn clearfix">
                        <?php echo number_dropdown(50, 550, 50, null, 'xsw_30') ?>
                        <div class="col-md-2 col-sm-3 xsw_50 no_pad" style="width: 16%;">
                            <div class="input-group">
                                <input type="text" class="form-control pickdate" name="from_date" placeholder="dd-mm-yyyy" size="30" readonly>
                                <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                            </div>
                        </div>
                        <div class="col-md-1 col-sm-1 hidden-xs" style="padding: 7px;width: auto;">
                            <span style="font-size:14px;font-weight:600;">TO</span>
                        </div>
                        <div class="col-md-2 col-sm-3 xsw_50 no_pad" style="width: 16%;">
                            <div class="input-group">
                                <input type="text" class="form-control pickdate" name="end_date" placeholder="dd-mm-yyyy" size="30" readonly>
                                <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                            </div>
                        </div>
                        <div class="col-md-2 col-sm-3 xsw_100 no_pad" style="width: auto;">
                            <button type="button" id="search" class="btn btn-info xsw_50">Search</button>
                            <button type="button" id="clear_from" class="btn btn-warning xsw_50">Clear</button>
                        </div>
                    </div>
                </div>
                {!! Form::close() !!}
            </td>
            <td class="text-right wmd_30" style="width: 20%">
                <button class="btn btn-primary btn-xs xsw_33" onclick="printDiv('print_area')"><i class="fa fa-print"></i> Print</button>
            </td>
        </tr>
    </table>
</div>

<div id="print_area">
    <div id="ajax_content">
        <div class="row clearfix">
            <div class="col-md-8 mp_60">
                <div class="print_heading_left">
                    <?php print_header("", true, true); ?>
                </div>
            </div>
            <div class="col-md-4 pull-right mp_40">
                <div class="print_heading_left mpmt">
                    <strong>{{ $particular->name }}</strong><br>
                    <strong>Address:</strong> {{ $particular->address }}<br>
                    <strong>Mobile:</strong> {{ $particular->mobile }}
                </div>
            </div>
        </div>

        <?php
        $prev_total_debit = 0;
        $prev_total_credit = 0;
        $_sum['dbt'] = [];
        $_sum['cdt'] = [];
        $_total['debit'] = [];
        $_total['credit'] = [];
        ?>

        <div class="table-responsive" style="margin: 0;">
            <table class="table table-bordered tbl_thin">
                @if($curPage < 2)
                <tr class="bg-warning">
                    <th class="text-center" style="width: 3%;">#</th>
                    <th>{{ date_dmy($prev_date) }}</th>
                    <th class="text-center"></th>
                    <th class="text-right" colspan="4">Previous Balance</th>
                    <th class="text-right"></th>
                    <th class="text-right"></th>
                    <th class="text-right"></th>
                    <th class="text-right"></th>
                </tr>
                @endif
                @if(!empty($prevDataset) && count($prevDataset) > 0)
                @foreach($prevDataset as $prevData)
                <?php
                $prev_total_debit += $prev_dbt = ($prevData->dr_particular_id == $id) ? $prevData->amount : 0;
                $prev_total_credit += $prev_cdt = ($prevData->cr_particular_id == $id) ? $prevData->amount : 0;
                $prev_balance = ($prev_total_debit - $prev_total_credit);
                $_lp_pay = ($prev_balance < 0) ? abs($prev_balance) : 0;
                $_lp_rec = ($prev_balance > 0) ? abs($prev_balance) : 0;
                $_sum['dbt'][] = $prev_dbt;
                $_sum['cdt'][] = $prev_cdt;
                ?>
                @endforeach
                <tr class="bg-warning">
                    <th class="text-right" colspan="7">Last Page Balance</th>
                    <th class="text-right">{{ $prev_total_debit }}</th>
                    <th class="text-right">{{ $prev_total_credit }}</th>
                    <th class="text-right">{{ $_lp_pay }}</th>
                    <th class="text-right">{{ $_lp_rec }}</th>
                    <?php
                    array_push($_total['debit'], $prev_total_debit);
                    array_push($_total['credit'], $prev_total_credit);
                    ?>
                </tr>
                @endif
                <tr class="bg-info">
                    <th class="text-center" style="width: 3%;">#</th>
                    <th style="width: 90px;">Date</th>
                    <th class="text-center">Type</th>
                    <th style="width: 11%;">Voucher Type</th>
                    <th class="text-center">Voucher No</th>
                    <th style="width: 15%;">Account Name</th>
                    <th>Description</th>
                    <th class="text-right" style="width: 100px;">Debit</th>
                    <th class="text-right" style="width: 100px;">Credit</th>
                    <th class="text-right" style="width: 100px;">Payable (Cr)</th>
                    <th class="text-right" style="width: 100px;">Receivable (Dr)</th>
                </tr>
                <?php
                $counter = 0;
                $total_debit = 0;
                $total_credit = 0;
                if (isset($_GET['page']) && $_GET['page'] > 1) {
                    $counter = ($_GET['page'] - 1) * $dataset->perPage();
                }
                ?>
                @foreach ($dataset as $data)
                <?php
                $counter++;
                $_type = ($data->dr_particular_id == $id) ? 'Dr' : 'Cr';
                $total_debit += $_dbt = ($data->dr_particular_id == $id) ? $data->amount : 0;
                $total_credit += $_cdt = ($data->cr_particular_id == $id) ? $data->amount : 0;
                $_sum['dbt'][] = $_dbt;
                $_sum['cdt'][] = $_cdt;
                $_sumDbt = array_sum($_sum['dbt']);
                $_sumCdt = array_sum($_sum['cdt']);
                $_balance = ($_sumDbt - $_sumCdt);
                $_pay = ($_balance < 0) ? abs($_balance) : 0;
                $_rec = ($_balance > 0) ? abs($_balance) : 0;
                ?>
                <tr onmouseover="change_color(this, true)" onmouseout="change_color(this, false)">
                    <td class="text-center">{{ $counter }}</td>
                    <td>{{ date_dmy($data->date) }}</td>
                    <td class="text-center">{{ $_type }}</td>
                    <td>{{ $data->voucher_type }}</td>
                    <td class="text-center">{{ $data->voucher_no }}</td>
                    <td style="word-break: break-all">{{ !empty($data->dr_particular) ? $data->dr_particular->name : (!empty($data->dr_subhead) ? $data->dr_subhead->name : $data->dr_head->name) }}</td>
                    <td style="word-break: break-all">{{ $data->description }}</td>
                    <td class="text-right">{{ $_dbt }}</td>
                    <td class="text-right">{{ $_cdt }}</td>
                    <td class="text-right">{{ $_pay }}</td>
                    <td class="text-right">{{ $_rec }}</td>
                </tr>
                @endforeach
                <tr class="bg-info">
                    <th class="text-right" colspan="7">Total</th>
                    <th class="text-right">{{ $total_debit }}</th>
                    <th class="text-right">{{ $total_credit }}</th>
                    <th class="text-right"></th>
                    <th class="text-right"></th>
                    <?php
                    array_push($_total['debit'], $total_debit);
                    array_push($_total['credit'], $total_credit);
                    ?>
                </tr>
                <tr class="bg-warning">
                    <th class="text-right" colspan="7">Total Balance</th>
                    <th class="text-right">{{ array_sum($_total['debit']) }}</th>
                    <th class="text-right">{{ array_sum($_total['credit']) }}</th>
                    <th class="text-right"></th>
                    <th class="text-right"></th>
                </tr>
            </table>
        </div>
        <div class="text-center hip">
            {{ $dataset->render() }}
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $("#search").click(function () {
            var _url = "{{ URL::to('particular/ledger_search') }}";
            var _form = $("#frmSearch");
            $.ajax({
                url: _url,
                type: "POST",
                data: _form.serialize(),
                success: function (data) {
                    $("#ajax_content").html(data);
                },
                error: function () {
                    $("#ajaxMessage").showAjaxMessage({html: 'There is some error.Try after some time.', type: 'error'});
                }
            });
        });
    });
</script>
@endsection