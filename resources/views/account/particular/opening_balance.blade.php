@extends('layouts.app')

@section('content')

<div id="breadcrumbBar" class="breadcrumb site_nav_links no_bdr_rad clearfix">
    <div class="col-md-3 col-sm-3 col-xs-2 cxs_2 no_pad">
        <button class="btn btn-info btn-xs" type="button" onclick="history.back()" title="Go Back"><span class="visible-xs"><i class="fa fa-arrow-left"></i></span><span class="hidden-xs">Back</span></button>
        <button class="btn btn-info btn-xs" onclick="redirectTo(''<?= url('view-clear'); ?>'')" title="Refresh" type="button"><span class="visible-xs"><i class="fa fa-refresh"></i></span><span class="hidden-xs">Refresh</span></button>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-6 cxs_10 text-center">
        <h2 class="page-title">Create New Opening Balance</h2>
    </div>
    <div class="col-md-3 col-sm-3 col-xs-4 cxs_12 no_pad">
        <ul class="text-right no_mrgn">
            <li><a href="{{ url('/home') }}"><i class="fa fa-fw fa-dashboard"></i> Dashboard</a> <span class="divider">/</span></li><li><span><i class="fa fa-file"></i> Particular</span></li>
        </ul>
    </div>
</div>

<div class="well">
    <table width="100%">
        <tr>
            <td class="wmd_70">
                {!! Form::open(['method' => 'POST',  'class' => 'search-form', 'id' => 'frmSearch', 'name' => 'frmSearch']) !!}
                <div class="input-group">
                    <div class="input-group-btn clearfix">
                        <div style="width:13%;"  class="col-md-2 col-sm-3 xsw_50 no_pad">
                            <select id="InstituteList" class="form-control select2search" name="institute_id" required>
                                <option value="">Company</option>
                                @foreach ( $insList as $ins )
                                <option value="{{ $ins->id  }}">{{ $ins->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-2 col-sm-3 xsw_50 no_pad">
                            <select id="head" class="form-control select2search" name="subhead_id" required>
                                <option value="">Sub Head</option>
                            </select>
                        </div>
                        <div class="col-md-2 col-sm-3 xsw_100 no_pad">
                            <button type="button" id="search" class="btn btn-info xsw_50">Search</button>
                        </div>
                        <div class="col-md-2 col-sm-3 no_pad">
                            <input type="text" name="date" placeholder="(dd-mm-yyyy)" value="{{ date('d-m-Y') }}" class="form-control pickdate" size="30" readonly>
                        </div>
                    </div>
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    </table>
</div>
<div class="clearfix">
    <form action="" class="form-horizontal" method="POST">
        {{ csrf_field() }}
        <div id="ajax_content"></div>
    </form>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $(document).on("change", "#InstituteList", function () {
            var id = $(this).val();
            $.ajax({
                url: "{{ URL::to('institute/subhead') }}",
                type: "post",
                data: {'institute': id, '_token': '{{ csrf_token() }}'},
                success: function (data) {
                    //enable("#subhead");
                    $('#head').html(data);
                },
                error: function (xhr, status) {
                    alert('There is some error.Try after some time.');
                }
            });
        });
        $(document).on("click", "#search", function () {
            var _url = "{{ URL::to('particular/opening-balance/form') }}";
            var _form = $("#frmSearch");
            if ($("#head").val() == "") {
                $("#head").focus();
                return false;
            } else {
                $("#ajax_content").html("Fetching particular information...");
                $("#ajaxMessage").html("").hide();
                $.ajax({
                    url: _url,
                    type: "post",
                    data: _form.serialize(),
                    success: function (data) {
                        $("#ajax_content").html(data);
                    },
                    error: function (xhr, status) {
                        alert("There is some error.Try after some time.");
                    }
                });
            }
        });
        $(document).on("change", ".subhead", function () {
            var id = $(this).val();
            var target_id = $(this).attr('data-target');
            //alert(target_id);
            $.ajax({
                url: "{{ URL::to('subhead/particular') }}",
                type: "post",
                data: {'head': id, '_token': '{{ csrf_token() }}'},
                success: function (data) {
                    $("#particular_" + target_id).html(data);
                },
                error: function (xhr, status) {
                    alert('There is some error.Try after some time.');
                }
            });
        });
        $(document).on("click", "#btnSubmit", function () {
            // var _pid = $(this).attr('data-pid');
            // if ($("#qty_" + _pid).val() == '' || $("#qty_" + _pid).val() <= 0) {
            //     $('#ajaxMessage').showAjaxMessage({html: "Quantity required", type: 'error'});
            //     $("#qty_" + _pid).addClass('bdr_err').focus();
            //     return false;
            // }

            // if ($("#rate_" + _pid).val() == '' || $("#rate_" + _pid).val() <= 0) {
            //     $('#ajaxMessage').showAjaxMessage({html: "Rate required", type: 'error'});
            //     $("#rate_" + _pid).addClass('bdr_err').focus();
            //     return false;
            // }

            var _url = "{{ URL::to('particular/opening-balance/save_data') }}";
            var _formdata = $('form').serialize();
            // _formdata._token = '{{ csrf_token() }}';
            // _formdata.catid = $("#category").val();
            // _formdata.pid = _pid;
            // _formdata.qty = $("#qty_" + _pid).val();
            // _formdata.rate = $("#rate_" + _pid).val();
            // _formdata.amount = $("#subtotal_" + _pid).val();
            $.post(_url, _formdata, function (resp) {
                if (resp.success === true) {
                    $("#ajaxMessage").showAjaxMessage({html: resp.message, type: "success"});
                    $(".qty_rate").val('');
                    $(".subtotal").val('');
                } else {
                    // $("#row_" + _pid).addClass('bg-danger');
                    $("#ajaxMessage").showAjaxMessage({html: resp.message, type: "error"});
                }
            }, "json");
        });
    });
</script>
@endsection