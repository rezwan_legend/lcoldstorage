@extends('layouts.app')

@section('breadcrumbs')
    <div id="breadcrumbBar" class="breadcrumb site_nav_links no_bdr_rad clearfix">
        <div class="col-md-3 col-sm-3 col-xs-2 no_pad">
            <button class="btn btn-info btn-xs" type="button" onclick="history.back()" title="Go Back"><span
                    class="visible-xs"><i class="fa fa-arrow-left"></i></span><span
                    class="hidden-xs">Back</span></button>
            <button class="btn btn-info btn-xs" onclick="redirectTo('<?= url('view-clear') ?>')" title="Refresh"
                type="button"><span class="visible-xs"><i class="fa fa-refresh"></i></span><span
                    class="hidden-xs">Refresh</span></button>
        </div>
        <div class="col-md-6 col-sm-6 hidden-xs text-center">
            <h2 class="page-title">Particular List</h2>
        </div>
        <div class="col-md-3 col-sm-3 col-xs-10 no_pad">
            <ul class="text-right no_mrgn no_pad">
                <li><a href="{{ url('/home') }}">Home</a> <span class="fa fa-angle-right"></span></li>
                <li>Particular</li>
            </ul>
        </div>
    </div>
@endsection

@section('content')
    <div class="well">
        <div class="clearfix mb_10">
            <div class="text-center">
                    <a class="btn btn-success btn-xs " href="{{ url('/particular/create') }}"><i
                            class="fa fa-plus"></i> New</a>

                    <a href="{{ url('/particular/opening-balance') }}"><button class="btn btn-info btn-xs"><i
                                class="fa fa-plus"></i> Opening Balance</button></a>

                    <button type="button" class="btn btn-danger btn-xs " id="Del_btn" disabled><i
                            class="fa fa-trash-o"></i> Delete</button>
                    <button class="btn btn-primary btn-xs " onclick="printDiv('print_area')"><i
                            class="fa fa-print"></i> Print</button>
                
            </div>
        </div>
        <table width="100%">
            <tbody>
                <tr>
                    <td class="wmd_70">
                        {!! Form::open(['method' => 'POST', 'class' => 'search-form', 'id' => 'frmSearch', 'name' => 'frmSearch']) !!}
                        <div class="input-group">
                            <div class="input-group-btn clearfix">
                                <?php echo number_dropdown(50, 550, 50, null, 'xsw_30'); ?>
                                <div class="col-md-2 col-sm-3 xsw_70 no_pad">
                                    <select id="head" class="form-control select2search" name="subhead_id" required>
                                        <option value="">Subhead</option>
                                    </select>
                                </div>
                                <div class="col-md-2 col-sm-3 xsw_50 no_pad">
                                    <input style="width:auto" type="text" name="search" id="q" class="form-control"
                                        placeholder="search">
                                </div>
                                <div class="col-md-2 col-sm-3 xsw_50 no_pad">
                                    <input style="width:100%" type="text" name="amountSrc" id="q" class="form-control"
                                        placeholder="Amount">
                                </div>
                                <div style="width:auto;" class="col-md-2 col-sm-3 xsw_100 no_pad">
                                    <button type="button" id="search" class="btn btn-info xsw_50">Search</button>
                                    <button type="button" id="clear_from" class="btn btn-warning xsw_50">Clear</button>
                                </div>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </td>

                </tr>
            </tbody>
        </table>
    </div>

    {!! Form::open(['method' => 'POST', 'class' => 'search-form', 'id' => 'frmList', 'name' => 'frmList']) !!}
    <div id="print_area">
        <?php print_header('Particular Information'); ?>
        <div id="ajax_content">
            <div class="table-responsive">
                <table class="table table-bordered tbl_thin" id="check">
                    <tr class="bg-info" id="r_checkAll">
                        <th class="text-center" style="width:3%;">#</th>
                        <th class="">Company</th>
                        <th>Sub Head</th>
                        <th>Particular</th>
                        <th>Mobile</th>
                        <th>Address</th>
                        <th class="text-right">{{ trans('words.debit') }}</th>
                        <th class="text-right">{{ trans('words.credit') }}</th>
                        <th class="text-right">{{ trans('words.pay_item') }}</th>
                        <th class="text-right">{{ trans('words.receive_item') }}</th>
                        <th class="text-center hip" style="width:8%;">Actions</th>
                        <th class="text-center hip" style="width:3%;"><input type="checkbox" id="check_all" value="all">
                        </th>
                    </tr>
                    <?php
                    $counter = 0;
                    $total_debit = 0;
                    $total_credit = 0;
                    $total_payable = 0;
                    $total_receivable = 0;
                    
                    if (isset($_GET['page']) && $_GET['page'] > 1) {
                        $counter = ($_GET['page'] - 1) * $dataset->perPage();
                    }
                    ?>
                    @foreach ($dataset as $data)
                        <?php
                        $counter++;
                        $_debit = $tmodel->sumPartDebit($data->id);
                        $_credit = $tmodel->sumPartCredit($data->id);
                        $_balance = $tmodel->sumPartBalance($data->id);
                        
                        $payable = $_balance < 0 ? $_balance : 0;
                        $receivable = $_balance > 0 ? $_balance : 0;
                        
                        $total_debit += $_debit;
                        $total_credit += $_credit;
                        $total_payable += $payable;
                        $total_receivable += $receivable;
                        ?>
                        <tr onmouseover="change_color(this, true)" onmouseout="change_color(this, false)">
                            <td class="text-center">{{ $counter }}</td>
                            <td class="">{{ $data->institute->name }}</td>
                            <td>{{ $data->subhead->name }}</td>
                            <td>{{ $data->name }}</td>
                            <td>{{ $data->mobile }}</td>
                            <td>{{ $data->address }}</td>
                            <td class="text-right">{{ $_debit }}</td>
                            <td class="text-right">{{ $_credit }}</td>
                            <td class="text-right">{{ $payable }}</td>
                            <td class="text-right">{{ $receivable }}</td>
                            <td class="text-center hip">
                                @if (has_user_access('particular_edit'))
                                    <a class="btn btn-info btn-xs" href="particulars/{{ $data->_key }}/edit">Edit</a>
                                @endif
                                <a class="btn btn-primary btn-xs"
                                    href="{{ url('/particular/ledger/' . $data->id) }}">Ledger</a>
                            </td>
                            <td class="text-center hip">
                                @if (has_user_access('particular_delete'))
                                    <input type="checkbox" name="data[]" value="{{ $data->id }}">
                                @endif
                            </td>
                        </tr>
                    @endforeach
                    <tr class="bg_gray">
                        <th class="text-center" colspan="6">Total</th>
                        <th class="text-right">{{ $total_debit }}</th>
                        <th class="text-right">{{ $total_credit }}</th>
                        <th class="text-right">{{ $total_payable }}</th>
                        <th class="text-right">{{ $total_receivable }}</th>
                        <th colspan="2"></th>
                    </tr>
                </table>
                <div class="text-center hip">
                    {{ $dataset->render() }}
                </div>
            </div>
        </div>
    </div>
    {!! Form::close() !!}

    <script type="text/javascript">
        $(document).ready(function() {
            $(document).on("change", "#InstituteList", function() {
                var id = $(this).val();
                $.ajax({
                    url: "{{ URL::to('institute/subhead') }}",
                    type: "post",
                    data: {
                        'institute': id,
                        '_token': '{{ csrf_token() }}'
                    },
                    success: function(data) {
                        //enable("#subhead");
                        $('#head').html(data);
                    },
                    error: function(xhr, status) {
                        alert('There is some error.Try after some time.');
                    }
                });
            });

            $("#search").click(function() {
                var _url = "{{ URL::to('particulars/search') }}";
                var _form = $("#frmSearch");

                $.ajax({
                    url: _url,
                    type: "post",
                    data: _form.serialize(),
                    success: function(data) {
                        $('#ajax_content').html(data);
                    },
                    error: function() {
                        $('#ajaxMessage').showAjaxMessage({
                            html: 'There is some error.Try after some time.',
                            type: 'error'
                        });
                    }
                });
            });

            $("#Del_btn").click(function() {
                var _url = "{{ URL::to('particulars/delete') }}";
                var _form = $("#frmList");
                var _rc = confirm("Are you sure about this action? This cannot be undone!");

                if (_rc == true) {

                    $.post(_url, _form.serialize(), function(data) {
                        if (data.success === true) {
                            $('#ajaxMessage').showAjaxMessage({
                                html: data.message,
                                type: 'success'
                            });
                            $("#search").trigger("click");
                        } else {
                            $('#ajaxMessage').showAjaxMessage({
                                html: data.message,
                                type: 'error'
                            });
                        }
                    }, "json");

                }
            });
        });
    </script>
@endsection
