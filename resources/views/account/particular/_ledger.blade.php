<div class="row clearfix">
    <div class="col-md-8 mp_60">
        <div class="print_heading_left">
            <?php print_header("", true, true); ?>
        </div>            
    </div>
    <div class="col-md-4 pull-right mp_40">
        <div class="print_heading_left mpmt">
            <strong>{{ $particular->name }}</strong><br>
            <strong>Address:</strong> {{ $particular->address }}<br>
            <strong>Mobile:</strong> {{ $particular->mobile }}<br>
            @if(!empty($from_date) && !empty($end_date))
            <strong>Dates:</strong> {{ date_dmy($from_date) }} | {{ date_dmy($end_date) }}
            @endif
        </div>
    </div>
</div>

<?php
$prev_total_debit = 0;
$prev_total_credit = 0;
$_sum['dbt'] = [];
$_sum['cdt'] = [];
$_total['debit'] = [];
$_total['credit'] = [];
//array_push($_sum['dbt'], $particular_debit);
//array_push($_sum['cdt'], $particular_credit);
?>

<div class="table-responsive" style="margin: 0;">
    <table class="table table-bordered tbl_thin">
        @if($curPage < 2)
        <tr class="bg-warning">
            <th class="text-center" style="width: 3%;">#</th>
            <th>{{ date_dmy($prev_date) }}</th>
            <th class="text-center"></th>
            <th class="text-right" colspan="4">Previous Balance</th>
            <th class="text-right"></th>
            <th class="text-right"></th>
            <th class="text-right"></th>
            <th class="text-right"></th>
        </tr>
        @endif
        @if(!empty($prevDataset) && count($prevDataset) > 0)
        @foreach($prevDataset as $prevData)
        <?php
        $prev_total_debit += $prev_dbt = ($prevData->dr_particular_id == $id) ? $prevData->amount : 0;
        $prev_total_credit += $prev_cdt = ($prevData->cr_particular_id == $id) ? $prevData->amount : 0;
        $prev_balance = ($prev_total_debit - $prev_total_credit);
        $_lp_pay = ($prev_balance < 0) ? abs($prev_balance) : 0;
        $_lp_rec = ($prev_balance > 0) ? abs($prev_balance) : 0;
        $_sum['dbt'][] = $prev_dbt;
        $_sum['cdt'][] = $prev_cdt;
        ?>
        @endforeach
        <tr class="bg-warning">
            <th class="text-right" colspan="7">Last Page Balance</th>
            <th class="text-right">{{ $prev_total_debit }}</th>
            <th class="text-right">{{ $prev_total_credit }}</th>
            <th class="text-right">{{ $_lp_pay }}</th>
            <th class="text-right">{{ $_lp_rec }}</th>
            <?php
            array_push($_total['debit'], $prev_total_debit);
            array_push($_total['credit'], $prev_total_credit);
            ?>
        </tr>
        @endif
        <tr class="bg-info">
            <th class="text-center" style="width: 3%;">#</th>
            <th style="width: 90px;">Date</th>
            <th class="text-center">Type</th>
            <th style="width: 11%;">Voucher Type</th>
            <th class="text-center">Voucher No</th>
            <th style="width: 15%;">Account Name</th>
            <th>Description</th>
            <th class="text-right" style="width: 100px;">Debit</th>
            <th class="text-right" style="width: 100px;">Credit</th>
            <th class="text-right" style="width: 100px;">Payable (Cr)</th>
            <th class="text-right" style="width: 100px;">Receivable (Dr)</th>
        </tr>
        <?php
        $counter = 0;
        $total_debit = 0;
        $total_credit = 0;
        if (isset($_GET['page']) && $_GET['page'] > 1) {
            $counter = ($_GET['page'] - 1) * $dataset->perPage();
        }
        ?>
        @foreach ($dataset as $data)
        <?php
        $counter++;
        $_type = ($data->dr_particular_id == $id) ? 'Dr' : 'Cr';
        $total_debit += $_dbt = ($data->dr_particular_id == $id) ? $data->amount : 0;
        $total_credit += $_cdt = ($data->cr_particular_id == $id) ? $data->amount : 0;
        $_sum['dbt'][] = $_dbt;
        $_sum['cdt'][] = $_cdt;
        $_sumDbt = array_sum($_sum['dbt']);
        $_sumCdt = array_sum($_sum['cdt']);
        $_balance = ($_sumDbt - $_sumCdt);
        $_pay = ($_balance < 0) ? abs($_balance) : 0;
        $_rec = ($_balance > 0) ? abs($_balance) : 0;
        ?>
        <tr onmouseover="change_color(this, true)" onmouseout="change_color(this, false)">
            <td class="text-center">{{ $counter }}</td>
            <td>{{ date_dmy($data->date) }}</td>
            <td class="text-center">{{ $_type }}</td>
            <td>{{ $data->voucher_type }}</td>
            <td class="text-center">{{ $data->voucher_no }}</td>
            <td style="word-break: break-all">{{ !empty($data->dr_particular) ? $data->dr_particular->name : (!empty($data->dr_subhead) ? $data->dr_subhead->name : $data->dr_head->name) }}</td>
            <td style="word-break: break-all">{{ $data->description }}</td>
            <td class="text-right">{{ $_dbt }}</td>
            <td class="text-right">{{ $_cdt }}</td>
            <td class="text-right">{{ $_pay }}</td>
            <td class="text-right">{{ $_rec }}</td>
        </tr>
        @endforeach
        <tr class="bg-info">
            <th class="text-right" colspan="7">Total</th>
            <th class="text-right">{{ $total_debit }}</th>
            <th class="text-right">{{ $total_credit }}</th>
            <th class="text-right"></th>
            <th class="text-right"></th>
            <?php
            array_push($_total['debit'], $total_debit);
            array_push($_total['credit'], $total_credit);
            ?>
        </tr>
        <tr class="bg-warning">
            <th class="text-right" colspan="7">Total Balance</th>
            <th class="text-right">{{ array_sum($_total['debit']) }}</th>
            <th class="text-right">{{ array_sum($_total['credit']) }}</th>
            <th class="text-right"></th>
            <th class="text-right"></th>
        </tr>
    </table>
</div>
<div class="text-center hip" id="apaginate">
    {{ $dataset->render() }}
</div>