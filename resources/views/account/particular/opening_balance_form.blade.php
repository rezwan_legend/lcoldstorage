
<div class="table-responsive">
    <table class="table table-bordered tbl_thin" id="check">
        <tbody>
            <tr class="bg_gray" id="r_checkAll">
                <th class="text-center" style="width:5%;">SL#</th>
                <th>Particular Name</th>
                <th width="15%">Type</th>
                <th class="text-center">Opening Balance</th>
            </tr>
            <?php
            $counter = 0;
            if (isset($_GET['page']) && $_GET['page'] > 1) {
                $counter = ($_GET['page'] - 1) * $particulars->perPage();
            }
            ?>
            @foreach ($particulars as $particular)
            <?php
            $prev_op_balance_data = $particular->particular_balance;
            $prev_op_balance = !empty($prev_op_balance_data) ? $prev_op_balance_data->opening_balance : '';
            $from_to_subhead = !empty($prev_op_balance_data) ? $prev_op_balance_data->from_to_subhead : '';
            $from_to_particular = !empty($prev_op_balance_data) ? $prev_op_balance_data->from_to_particular : '';
            $type = !empty($prev_op_balance_data) ? $prev_op_balance_data->type : '';
            $counter++;
            ?>   
            <tr>
                <td class="text-center">{{ $counter }}</td>
                <td>{{ $particular->name }} ( {{ $particular->address }} )<input type="hidden" name="particular_id[{{ $particular->id }}]"></td>
                <td>
                    <select class="form-control" id="type_{{ $particular->id }}" name="type[{{ $particular->id }}]" style="height:24px; padding:0px 12px;" required>
                        <option value="">Select Type</option>
                        <option value="Debit" @if($type == 'Debit') {{ 'selected' }} @endif>Debit</option>
                        <option value="Credit" @if($type == 'Credit') {{ 'selected' }} @endif>Credit</option>
                    </select>
                </td>               
                <td class="text-center"><input type="number" class="text-center" name="opening_balance[{{ $particular->id }}]" step="any" min="0" value="{{ $prev_op_balance }}"></td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
<div class="text-center" style="margin:20px;"> 
    <input type="button" id="btnSubmit" class="btn btn-primary btn-lg" name="btnSave" value="Update Opening Balance">
</div>