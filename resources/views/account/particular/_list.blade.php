@if (!empty($dataset) && count($dataset) > 0)
<div class="table-responsive">
    <table class="table table-bordered tbl_thin" id="check">
        <tr class="bg-info" id="r_checkAll">
            <th class="text-center" style="width:3%;">#</th>
            <th class="">Company</th>
            <th>Sub Head</th>
            <th>Particular</th>
            <th>Mobile</th>
            <th>Address</th>
            <th class="text-right">{{ trans('words.debit') }}</th>
            <th class="text-right">{{ trans('words.credit') }}</th>
            <th class="text-right">{{ trans('words.pay_item') }}</th>
            <th class="text-right">{{ trans('words.receive_item') }}</th>
            <th class="text-center hip" style="width:8%;">Actions</th>
            <th class="text-center hip" style="width:3%;"><input type="checkbox" id="check_all" value="all"></th>
        </tr>
        <?php
        $counter = 0;
        $total_debit = 0;
        $total_credit = 0;
        $total_payable = 0;
        $total_receivable = 0;

        if (isset($_GET['page']) && $_GET['page'] > 1) {
            $counter = ($_GET['page'] - 1) * $dataset->perPage();
        }
        ?>
        @foreach ($dataset as $data)
        <?php
        $counter++;
        $_debit = $tmodel->sumPartDebit($data->id);
        $_credit = $tmodel->sumPartCredit($data->id);
        $_balance = $tmodel->sumPartBalance($data->id);

        $payable = ($_balance < 0) ? $_balance : 0;
        $receivable = ($_balance > 0) ? $_balance : 0;

        $total_debit += $_debit;
        $total_credit += $_credit;
        $total_payable += $payable;
        $total_receivable += $receivable;
        ?>
        <tr onmouseover="change_color(this, true)" onmouseout="change_color(this, false)">
            <td class="text-center">{{ $counter }}</td>
            <td class="">{{ $data->institute->name }}</td>
            <td>{{ $data->subhead->name }}</td>
            <td>{{ $data->name }}</td>
            <td>{{ $data->mobile }}</td>
            <td>{{ $data->address }}</td>
            <td class="text-right">{{ $_debit }}</td>
            <td class="text-right">{{ $_credit }}</td>
            <td class="text-right">{{ $payable }}</td>
            <td class="text-right">{{ $receivable }}</td>
            <td class="text-center hip">
                @if(has_user_access('particular_edit'))
                <a class="btn btn-info btn-xs" href="particulars/{{ $data->_key }}/edit">Edit</a>
                @endif
                <a class="btn btn-primary btn-xs" href="{{ url('/particular/ledger/'.$data->id) }}">Ledger</a>
            </td>
            <td class="text-center hip">
                @if(has_user_access('particular_delete'))
                <input type="checkbox" name="data[]" value="{{ $data->id }}">
                @endif
            </td>
        </tr>
        @endforeach
        <tr class="bg_gray">
            <th class="text-center" colspan="6">Total</th>
            <th class="text-right">{{ $total_debit }}</th>
            <th class="text-right">{{ $total_credit }}</th>
            <th class="text-right">{{ $total_payable }}</th>
            <th class="text-right">{{ $total_receivable }}</th>
            <th colspan="2"></th>
        </tr>
    </table>
    <div class="text-center hip" id="apaginate">
        {{ $dataset->render() }}
    </div>
</div>
@else
<div class="alert alert-info">No records found!</div>
@endif