<?php if (!empty($dataset) && count($dataset) > 0) : ?>
<div class="table-responsive">
    <table class="table table-bordered tbl_thin" id="check">
        <tbody>
            <tr class="bg_gray" id="r_checkAll">
                <th class="text-center" style="width:5%;">SL#</th>
                <th>Sub Head</th>
                <th>Name</th>
                <th>Address</th>
                <th>Mobile</th>
                <th class="text-center">Due Amount</th>
                <th class="text-center hip" style="width:12%;">Actions</th>
            </tr>
            <?php
            $counter = 0;
            $total_due = 0;
            if (isset($_GET['page']) && $_GET['page'] > 1) {
                $counter = ($_GET['page'] - 1) * $dataset->perPage();
            }
            ?>
            @foreach ($dataset as $data)
            <?php
            $_debit = $tmodel->sumPartDebit($data->id);
            $_credit = $tmodel->sumPartCredit($data->id);
            $_balance = $tmodel->sumPartBalance($data->id);
            if ($_balance > 0) :
                $counter++;
                $total_due += $_balance;
                ?>   
                <tr>
                    <td class="text-center">{{ $counter }}</td>
                    <td>{{ $data->subhead->name }}</td>
                    <td>{{ $data->name }}</td>
                    <td>{{ $data->address }}</td>
                    <td>{{ $data->mobile }}</td>
                    <td class="text-center">{{ $_balance }}</td>
                    <td class="text-center hip">
                        <a class="btn btn-primary btn-xs" href="{{ url('ledger/particular/'.$data->id) }}"><i class="fa fa-dashboard"></i> Details</a>
                    </td>
                </tr>
            <?php endif; ?>
            @endforeach
            <tr class="bg_gray">
                <th class="text-center" colspan="5">Total</th>
                <th class="text-center">{{ $total_due }}</th>
                <th class="hip"></th>
            </tr>
        </tbody>
    </table>
    <div class="text-center hip" id="apaginate">
        {{ $dataset->render() }}
    </div>
</div>
<?php else: ?>
    <div class="alert alert-info">No records found!</div>
    <?php endif; ?>