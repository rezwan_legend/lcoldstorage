@extends('layouts.app')

@section('breadcrumbs')
<div id="breadcrumbBar" class="breadcrumb site_nav_links no_bdr_rad clearfix">
    <div class="col-md-3 col-sm-3 col-xs-2 no_pad">
        <button class="btn btn-info btn-xs" type="button" onclick="history.back()" title="Go Back"><span class="visible-xs"><i class="fa fa-arrow-left"></i></span><span class="hidden-xs">Back</span></button>
        <button class="btn btn-info btn-xs" onclick="redirectTo('<?= url('/sr/create'); ?>')" title="Refresh" type="button"><span class="visible-xs"><i class="fa fa-refresh"></i></span><span class="hidden-xs">Refresh</span></button>
    </div>
    <div class="col-md-6 col-sm-6 hidden-xs text-center">
        <h2 class="page-title">{{$page_title}}</h2>
    </div>
    <div class="col-md-3 col-sm-3 col-xs-10 no_pad">
        <ul class="text-right no_mrgn no_pad">
            <li><a href="{{ url('/home') }}">Home</a> <i class="fa fa-angle-right"></i></li>
            <li><a href="{{ url('/sr') }}">Sr</a> <i class="fa fa-angle-right"></i></li>
            <li>Form</li>
        </ul>
    </div>
</div>
@endsection

@section('content')
<div class="col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title">Create New Sr</h3>
        </div>
        <div class="panel-body">
            {!! Form::open(['method' => 'POST', 'url' => '/sr', 'class' => 'form-horizontal']) !!}
            <div class="form-group">
                <label for="sr_no" class="col-md-4 control-label">Sr No</label>
                <div class="col-md-6">
                    <input type="number" class="form-control" name="sr_no" placeholder="sr_no" required>
                    <small class="text-danger">{{ $errors->first('sr_no') }}</small>
                </div>
            </div>
            <div class="form-group">
                <label for="lot_no" class="col-md-4 control-label">Lot No</label>
                <div class="col-md-6">
                    <input type="number" class="form-control" name="lot_no" placeholder="lot_no" required>
                    <small class="text-danger">{{ $errors->first('lot_no') }}</small>
                </div>
            </div>
            <div class="form-group">
                <label for="customer" class="col-md-4 control-label">Customer</label>
                <div class="col-md-6">
                    <input type="text" class="form-control" name="customer" placeholder="Customer Name" required>
                    <small class="text-danger">{{ $errors->first('customer') }}</small>
                </div>
            </div>
            <div class="form-group">
                <label for="fname" class="col-md-4 control-label">Father Name</label>
                <div class="col-md-6">
                    <input type="text" class="form-control" name="fname" placeholder="Father Name" required>
                    <small class="text-danger">{{ $errors->first('lot_no') }}</small>
                </div>
            </div>
            <div class="form-group">
                <label for="village" class="col-md-4 control-label">Village</label>
                <div class="col-md-6">
                    <textarea name="village" id="village" cols="30" class="form-control"></textarea>
                    <small class="text-danger">{{ $errors->first('village') }}</small>
                </div>
            </div>
            <div class="col-md-8 col-md-offset-5">
                <button type="button" class="btn btn-info" id="resetbtn">Reset</button>
                <button type="submit" class="btn btn-primary">Save</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
@endsection