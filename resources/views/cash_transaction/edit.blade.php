@extends('layouts.app')

@section('breadcrumbs')
    <div id="breadcrumbBar" class="breadcrumb site_nav_links no_bdr_rad clearfix">
        <div class="col-md-3 col-sm-3 col-xs-2 no_pad">
            <button class="btn btn-info btn-xs" type="button" onclick="history.back()" title="Go Back"><span
                    class="visible-xs"><i class="fa fa-arrow-left"></i></span><span
                    class="hidden-xs">Back</span></button>
            <button class="btn btn-info btn-xs" onclick="redirectTo('<?= url('view-clear') ?>')" title="Refresh"
                type="button"><span class="visible-xs"><i class="fa fa-refresh"></i></span><span
                    class="hidden-xs">Refresh</span></button>
        </div>
        <div class="col-md-6 col-sm-6 hidden-xs text-center">
            <h2 class="page-title">{{ $page_title }}</h2>
        </div>
        <div class="col-md-3 col-sm-3 col-xs-10 no_pad">
            <ul class="text-right no_mrgn no_pad">
                <li><a href="{{ url('/home') }}">Home</a> <i class="fa fa-angle-right"></i></li>
                <li><a href="{{ url('cash_transaction') }}">Cash Transaction</a> <i class="fa fa-angle-right"></i></li>
                <li>Form</li>
            </ul>
        </div>
    </div>
@endsection

@section('content')
    <div class="col-md-6 col-md-offset-3 col-sm-6 col-sm-offset-3">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">{{ $page_title }}</h3>
            </div>
            <div class="panel-body">
                {!! Form::open(['method' => 'POST', 'url' => '/cash_transaction', 'class' => 'form-horizontal']) !!}

                <div class="form-group">
                    <label for="date" class="col-md-4 control-label">Date</label>
                    <div class="col-md-6">
                        <input type="date" class="form-control" id="date" name="date" required>
                        <small class="text-danger">{{ $errors->first('date') }}</small>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-4 control-label" for="head">Head Name</label>
                    <div class="col-md-6">
                        <select class="form-control select2search" id="head" name="head" required>
                            <option value="">Select Branche</option>
                            @foreach ($head as $heads)
                                <option value="{{ $heads->id }}">{{ $heads->name }}</option>
                            @endforeach
                        </select>
                        <small class="text-danger">{{ $errors->first('branches') }}</small>
                    </div>
                </div>

                <div class="form-group">
                    <label for="bywhom" class="col-md-4 control-label">By Whom</label>
                    <div class="col-md-6">
                        <input type="text" class="form-control" id="bywhom" name="by_whom" required>
                        <small class="text-danger">{{ $errors->first('by_whom') }}</small>
                    </div>
                </div>

                <div class="form-group">
                    <label for="debit" class="col-md-4 control-label">Dabit</label>
                    <div class="col-md-6">
                        <input type="text" class="form-control" id="debit" name="debit" required>
                        <small class="text-danger">{{ $errors->first('debit') }}</small>
                    </div>
                </div>

                <div class="form-group">
                    <label for="description" class="col-md-4 control-label">Description</label>
                    <div class="col-md-6">
                        <textarea id="description" name="description" rows="4" cols="50" class="form-control"></textarea>
                        <small class="text-danger">{{ $errors->first('description') }}</small>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-4 control-label" for="ttype">Transaction Type</label>
                    <div class="col-md-6">
                        <select class="form-control select2search" id="ttype" name="transaction_type" required>
                            <option value="Cash">Cash</option>

                            <option value="Bank">Bank</option>

                        </select>
                        <small class="text-danger">{{ $errors->first('transaction_type') }}</small>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-4 control-label" for="bank">Bank Name</label>
                    <div class="col-md-6">
                        <select class="form-control select2search" id="bank" name="bank" disabled>
                            <option value="">Select Bank</option>
                            @foreach ($bank_setting as $bank)
                                <option value="{{ $bank->id }}">{{ $bank->name }}</option>
                            @endforeach
                        </select>
                        <small class="text-danger">{{ $errors->first('bank') }}</small>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-4 control-label" for="account">Account</label>
                    <div class="col-md-6">
                        <select class="form-control select2search" id="account" name="account" disabled>

                        </select>
                        <small class="text-danger">{{ $errors->first('bank') }}</small>
                    </div>
                </div>



                <div class="form-group">
                    <label for="cnumber" class="col-md-4 control-label">Check Number</label>
                    <div class="col-md-6">
                        <input type="text" class="form-control" id="cnumber" name="check_number" required>
                        <small class="text-danger">{{ $errors->first('account_number') }}</small>
                    </div>
                </div>

                <div class="col-md-8 col-md-offset-4">
                    <button type="button" class="btn btn-info" onclick="history.back()" title="Go Back">Cancel</button>
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    @endsection
