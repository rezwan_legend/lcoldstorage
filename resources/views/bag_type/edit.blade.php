@extends('layouts.app')

@section('breadcrumbs')
<div id="breadcrumbBar" class="breadcrumb site_nav_links no_bdr_rad clearfix">
    <div class="col-md-3 col-sm-3 col-xs-2 no_pad">
        <button class="btn btn-info btn-xs" type="button" onclick="history.back()" title="Go Back"><span class="visible-xs"><i class="fa fa-arrow-left"></i></span><span class="hidden-xs">Back</span></button>
        <button class="btn btn-info btn-xs" onclick="redirectTo('<?= url('view-clear'); ?>')" title="Refresh" type="button"><span class="visible-xs"><i class="fa fa-refresh"></i></span><span class="hidden-xs">Refresh</span></button>
    </div>
    <div class="col-md-6 col-sm-6 hidden-xs text-center">
        <h2 class="page-title">{{$page_title}}</h2>
    </div>
    <div class="col-md-3 col-sm-3 col-xs-10 no_pad">
        <ul class="text-right no_mrgn no_pad">
            <li><a href="{{ url('/home') }}">Home</a> <i class="fa fa-angle-right"></i></li>
            <li><a href="{{ url('/essential/bag_type') }}">Bag type</a> <i class="fa fa-angle-right"></i></li>
            <li>Form</li>
        </ul>
    </div>
</div>
@endsection

@section('content')
<div class="col-md-6 col-md-offset-3 col-sm-6 col-sm-offset-3">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title">{{$page_title}}</h3>
        </div>
        <div class="panel-body">
            {!! Form::open(['method' => 'PUT', 'url' => '/essential/bag_type/'.$data->id, 'class' => 'form-horizontal']) !!}
       
             <div class="form-group col-md-6 py-3">
               <label class="mb-2 fw-bold" for="session">Session <span class="text-danger">*</span></label>
                <select name="session" id="session" class="form-control">
                    <option value="">-- Select any Option --</option>
                    <option value="{{$data->session}}">{{$data->session}}</option>
                </select>
                 
            </div>

            <div class="form-group col-md-6 py-3">
                 <label class="mb-2 fw-bold" for="name">Name <span class="text-danger">*</span></label>
                    <input type="text" name="name" id="name" class="form-control" value="{{$data->name}}" >
                    <small class="text-danger">{{ $errors->first('bag_type') }}</small>
            </div>
            

             <div class="form-group col-md-6">
                 <label class="mb-2 fw-bold" for="per_bag_rent">Per Bag Rent</label>
                    <input value="{{$data->per_bag_rent}}" type="number" name="per_bag_rent" id="per_bag_rent" class="form-control">
                    <small class="text-danger">{{ $errors->first('bag_type') }}</small>
            </div>
            
            <div class="form-group col-md-6">
                 <label class="mb-2 fw-bold" for="per_kg_rent">Per Kg Rent</label>
                    <input  value="{{$data->per_kg_rent}}" type="number" name="per_kg_rent" id="per_kg_rent" class="form-control" >
                    <small class="text-danger">{{ $errors->first('bag_type') }}</small>
            </div>
            <div class="form-group col-md-6">
                <label class="mb-2 fw-bold" for="agent_bag_rent">Agent Bag Rent</label>
                    <input value="{{$data->agent_bag_rent}}" type="number" name="agent_bag_rent" id="agent_bag_rent" class="form-control" >
                    <small class="text-danger">{{ $errors->first('bag_type') }}</small>
            </div>

             <div class="form-group col-md-6">
                 <label class="mb-2 fw-bold" for="agent_kg_rent">Agent Kg Rent</label>
                 <input value="{{$data->agent_kg_rent}}" type="number" name="agent_kg_rent" id="agent_kg_rent" class="form-control" >
                    <small class="text-danger">{{ $errors->first('bag_type') }}</small>
            </div>
             <div class="form-group col-md-6">
                 <label class="mb-2 fw-bold" for="party_bag_rent">Party Bag Rent</label>
                    <input value="{{$data->party_bag_rent}}" type="number" name="party_bag_rent" id="party_bag_rent" class="form-control"  >
                    <small class="text-danger">{{ $errors->first('bag_type') }}</small>
                </div>


                <div class="form-group col-md-6">
                 <label class="mb-2 fw-bold" for="party_kg_rent">Party Kg Rent</label>
                    <input value="{{$data->party_kg_rent}}" type="number" name="party_kg_rent" id="party_kg_rent" class="form-control">
                    <small class="text-danger">{{ $errors->first('bag_type') }}</small>
                </div>
            <div class="form-group col-md-6">
                  <label class="mb-2 fw-bold" for="per_bag_loan">Per Bag Loan</label>
                    <input  value="{{$data->per_bag_loan}}" type="number" name="per_bag_loan" id="per_bag_loan" class="form-control"  >
                    <small class="text-danger">{{ $errors->first('bag_type') }}</small>
            </div>
            <div class="form-group col-md-6">
                 <label class="mb-2 fw-bold" for="empty_bag_rate">Empty Bag Rate</label>
                    <input value="{{$data->empty_bag_rent}}" type="number" name="empty_bag_rate" id="empty_bag_rate" class="form-control"  >
                    <small class="text-danger">{{ $errors->first('bag_type') }}</small>
            </div>
            <div class="form-group col-md-6">
                  <label class="mb-2 fw-bold" for="fan_charge">Fan Charge</label>
                    <input value="{{$data->fan_charge}}" type="number" name="fan_charge" id="fan_charge" class="form-control"  >
                    <small class="text-danger">{{ $errors->first('bag_type') }}</small>
            </div>
            <div class="form-group col-md-6">
                   <input type="checkbox" name="is_default" value="1" id="is_default" class="" >
                    <label class="mb-2 fw-bold" for="is_default">Is Default</label>
                    <small class="text-danger">{{ $errors->first('bag_type') }}</small>
            </div>
           
            <div class="col-md-8 col-md-offset-4">
                <button type="button" class="btn btn-info" onclick="history.back()" title="Go Back">Cancel</button>
                <button type="submit" class="btn btn-primary">Update</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
@endsection