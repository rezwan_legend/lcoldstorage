@extends('layouts.app')

@section('content')
<div id="breadcrumbBar" class="breadcrumb site_nav_links no_bdr_rad clearfix">
    <div class="col-md-3 col-sm-3 col-xs-2 cxs_2 no_pad">
        <button class="btn btn-info btn-xs" type="button" onclick="history.back()" title="Go Back"><span class="visible-xs"><i class="fa fa-arrow-left"></i></span><span class="hidden-xs">Back</span></button>
        <button class="btn btn-info btn-xs" onclick="redirectTo('<?= url('view-clear') ?>')" title="Refresh" type="button"><span class="visible-xs"><i class="fa fa-refresh"></i></span><span class="hidden-xs">Refresh</span></button>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-6 cxs_10 text-center">
        <h2 class="page-title">{{ $page_title }}</h2>
    </div>
    <div class="col-md-3 col-sm-3 col-xs-4 cxs_12 no_pad">
        <ul class="text-right no_mrgn">
            <li><a href="{{ url('/home') }}">Dashboard</a> <span class="fa fa-angle-right"></span></li>
            <li><a href="{{ url('/service') }}">Service</a> <span class="fa fa-angle-right"></span></li>
            <li>List</li>
        </ul>                            
    </div>
</div>

<div class="row clearfix">
    <div class="col-md-4 col-md-offset-4">
        <div class="panel panel-default">
            <div class="panel-heading">Update Service Information</div>
            <div class="panel-body">
                {!! Form::open(['method' => 'POST', 'url' => 'service/update/1', 'id' => 'frm_service', 'class' => 'form-horizontal']) !!} 
                <div class="form-group">
                    <label class="col-md-6 control-label no_pad_top">Demo Mode</label>
                    <div class="col-md-6">
                        <label class="no_mrgn_btm"><input type="radio" id="demo_mode_yes" name="demo_mode" value="{{ YES }}" <?= ($data->demo_mode == YES) ? ' checked' : ''; ?>> ON</label>
                        <label class="no_mrgn_btm" style="margin-left: 10px"><input type="radio" id="demo_mode_no" name="demo_mode" value="{{ NO }}" <?= ($data->demo_mode == NO) ? ' checked' : ''; ?>> OFF</label>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-6 control-label no_pad_top">Maintenance Mode</label>
                    <div class="col-md-6">
                        <label class="no_mrgn_btm"><input type="radio" id="maintenance_mode_yes" name="maintenance_mode" value="{{ YES }}" <?= ($data->maintenance_mode == YES) ? ' checked' : ''; ?>> ON</label>
                        <label class="no_mrgn_btm" style="margin-left: 10px"><input type="radio" id="maintenance_mode_no" name="maintenance_mode" value="{{ NO }}" <?= ($data->maintenance_mode == NO) ? ' checked' : ''; ?>> OFF</label>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-6 control-label no_pad_top">Ledger View</label>
                    <div class="col-md-6">
                        <select class="form-control" id="ledger_view" name="ledger_view">
                            <option value="{{ LEDGER_BANKING }}" <?= ($data->ledger_view == LEDGER_BANKING) ? ' selected' : ''; ?>>{{ LEDGER_BANKING }}</option>
                            <option value="{{ LEDGER_LEDGEND }}" <?= ($data->ledger_view == LEDGER_LEDGEND) ? ' selected' : ''; ?>>{{ LEDGER_LEDGEND }}</option>
                        </select>
                    </div>
                </div>
                <div class="text-center">
                    <input type="submit" class="btn btn-primary btn-block" name="btnSave" value="Update">
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
@endsection