@if (!empty($itemDataset) && count($itemDataset) > 0)
<div class="table-responsive">
    <table class="table table-bordered tbl_thin" id="check">
        <tbody>
            <tr class="bg_highlight" id="r_checkAll">
                <th class="text-center" style="width:3%;">#</th>
                <th>{{ trans('words.product_type') }}</th>
                <th>{{ trans('words.category') }}</th>
                <th>{{ trans('words.product') }}</th>
                <th>{{ trans('words.size') }}</th>
                <th>{{ trans('words.unit') }}</th>
                <th class="text-center">{{ trans('words.qty') }}</th>
                <th class="text-left">{{ trans('words.net_weight') }} </th>
                <th class="text-left">{{ trans('words.sale_price_unit') }} </th>
                <th class="text-center">{{ trans('words.sale_qty') }}  </th>
                <th class="text-center">{{ trans('words.rate') }}</th>
                <th class="text-right">{{ trans('words.total_price') }}</th>
                <th class="text-center hip">{{ trans('words.actions') }}</th>
            </tr>
            <?php
            $counter = 0;
            $total_quantity = 0;
            $total_price = 0;
            ?>
            @foreach ($itemDataset as $data)
            <?php
            $counter++;
            $total_quantity += $data->quantity;
            $total_price += $data->price;
            ?>   
            <tr onmouseover="change_color(this, true)" onmouseout="change_color(this, false)">
                <td class="text-center" style="width:5%;">{{ $counter }}</td>
                <td class="text-left">{{!empty( $data->product_type ) ? $data->product_type->name : '' }}</td>
                <td class="text-left">{{!empty( $data->category ) ? $data->category->name : '' }}</td>
                <td class="text-left">{{!empty( $data->product ) ? $data->product->name : '' }}</td>
                <td class="text-left">{{!empty( $data->productSize ) ? $data->productSize->name : '' }}</td>
                <td>{{!empty( $data->productUnit ) ? $data->productUnit->name : '' }}</td>
                <td  class="text-center">{{ $data->quantity }}</td>
                <td>{{  $data->net_weight }}</td>
                <td class="text-left">{{!empty( $data->unit_price ) ? $data->unit_price->unit_name : '' }}</td>
                <td  class="text-center">{{ $data->price_unit_qty }}</td>
                <td class="text-center">{{ $data->rate }}</td>
                <td class="text-right">{{ $data->price }}</td>
                <td class="text-center hip">
                    <a class="color_danger" title="Delete Item" href="{{ url('ricemill/dellivery/remove_item/'.$data->id) }}" onclick="return confirm('Are you sure about this action?')"><i class="fa fa-trash-o"></i></a>
                </td>
            </tr>
            @endforeach
            <tr class="bg_gray">
                <th colspan="6" class="text-center" style="background:#ddd; font-weight:600; width:5%;">Total</th>
                <th class="text-center">{{ $total_quantity }}</th>
                <th class="text-left" colspan="4"></th>
                <th class="text-right">{{ $total_price }}</th>
                <th class="hip" colspan="3"></th>
            </tr>
        </tbody>
    </table>
</div>
@else
<div class="alert alert-info">No records found!</div>
@endif