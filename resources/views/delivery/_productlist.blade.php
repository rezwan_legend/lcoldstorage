@if(!empty($dataset) && count($dataset) > 0)
<div class="table-responsive">
    <table class="table table-bordered tbl_thin" id="check">
        <tr class="bg-info" id="r_checkAll">
            <th class="text-center" style="width: 3%">#</th>
            <th>Company</th>
            <th>Type</th>
            <th>Category</th>
            <th>Product</th>
            <th class="text-center" style="width: 3%"><input type="checkbox" id="check_all" value="all"></th>
            <th class="text-center" style="width: 100px">Size</th>
            <th class="text-center" style="width: 100px">Cost Price</th>
            <th class="text-center" style="width: 100px">Sale Price</th>
            <th class="text-center" style="width: 100px">Date</th>
        </tr>
        <?php $sl = 0; ?>
        @foreach ( $dataset as $data )
        <?php $sl++; ?>
        <tr id="row_{{ $data->id }}">
            <td class="text-center">{{ $sl }}</td>
            <td>{{ !empty($data->company) ? $data->company->name : '' }}</td>
            <td>{{ !empty($data->product_type) ? $data->product_type->name : '' }}</td>
            <td>{{ !empty($data->category) ? $data->category->name : '' }}</td>
            <td>{{ !empty($data->name) ? $data->name : '' }}</td>
            <td class="text-center">
                <input type="checkbox" class="items" id="product_id_{{$data->id}}" name="product_id[]" value="{{$data->id}}">
                <input type="hidden" id="company_{{$data->id}}" name="company[{{$data->id}}]" value="{{ !empty($data->company) ? $data->company->id : '' }}">
                <input type="hidden" id="product_type_{{$data->id}}" name="product_type[{{$data->id}}]" value="{{ !empty($data->product_type) ? $data->product_type->id : '' }}">
                <input type="hidden" id="category_id_{{$data->id}}" name="category_id[{{$data->id}}]" value="{{ !empty($data->category) ? $data->category->id : '' }}">
            </td>
            <td>
                <select class="form-control" id="size_{{$data->id}}" name="size[{{$data->id}}]" style="height: 25px; padding-top: 0px;" disabled>
                    @foreach( $sizes as $size )
                    <option value="{{ $size->id }}">{{ $size->name }}</option>
                    @endforeach
                </select>
            </td>
            <td>
                <input type="text" class="wfull text-center price" id="cost_price_{{ $data->id }}" name="cost_price[{{$data->id}}]" value="" readonly>
            </td>
            <td>
                <input type="text" class="wfull text-center price" id="sale_price_{{ $data->id }}" name="sale_price[{{$data->id}}]" value="" readonly>
            </td>
            <td>
                <input type="text" class="wfull create_date" id="create_date_{{ $data->id }}" name="create_date[{{$data->id}}]" value="{{ date('d-m-Y') }}" required readonly>
            </td>
        </tr>
        @endforeach
    </table>
</div>
<div class="text-center">
    <button type="submit" class="btn btn-primary xsw_100">Save</button>
</div>
@else
<div class="alert alert-info">No records found.</div>
@endif