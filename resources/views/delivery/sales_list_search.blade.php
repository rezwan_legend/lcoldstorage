@if(!empty($searchValues) && count($searchValues) > 0)
<table class="table table-bordered tbl_thin" style="margin: 0">
    <tr class="bg-primary">
        @foreach ($searchValues as $key => $val)
        <td><strong>{{ $key }} : </strong> {{ $val }}</td>
        @endforeach
    </tr>
</table>
@endif
@if (!empty($dataset) && count($dataset) > 0) 
<div class="table-responsive">
    <table class="table table-bordered tbl_thin" id="check">
        <tbody>
            <tr class="bg_highlight" id="r_checkAll">
                <th class="text-center" style="width:3%;">#</th>
                <th>{{ trans('words.date') }}</th>
                <th>{{ trans('words.challan_no') }}</th>
                <th>{{ trans('words.party_name') }}</th>
                <th>{{ trans('words.products') }}</th>
                <th>{{ trans('words.size') }}</th>
                <th>{{ trans('words.quantity') }}</th>
                <th class="text-center">{{ trans('words.net_weight') }}</th>
                <th>{{ trans('words.sale_price_unit') }}</th>
                <th>{{ trans('words.sale_qty') }}</th>
                <th class="text-center">{{ trans('words.rate') }}</th>
                <th class="text-right">{{ trans('words.total') }} {{ trans('words.price') }}</th>
                <th class="text-right"> {{ trans('words.actions') }}</th>
            </tr>
            <?php
            $counter = 0;
            $total_quantity = 0;
            $total_price = 0;
            $total_netweight = 0;
            ?>
            @foreach ($dataset as $data)
            <?php
            $counter++;
            $total_quantity += $data->quantity;
            $total_price += $data->price;
            $total_netweight += $data->net_weight;
            ?>   
            <tr onmouseover="change_color(this, true)" onmouseout="change_color(this, false)">
                <td class="text-center" style="width:5%;">{{ $counter }}</td>
                <td class="text-left">{{date_dmy($data->date) }}</td>
                <td class="text-left">{{ $data->challan_no }}</td>
                <td class="text-left">{{!empty( $data->dellivery->particular) ? $data->dellivery->particular->name : $data->dellivery->from_subhead->name }}</td>
                <td class="text-left">{{!empty( $data->product) ? $data->product->name : '' }}</td>
                <td class="text-left">{{!empty( $data->productSize) ? $data->productSize->name : '' }}</td>
                <td class="text-center">{{ $data->quantity }}</td>
                <td class="text-center">{{ $data->net_weight }}</td>
                <td class="text-left">{{ !empty($data->unit_price) ? $data->unit_price->unit_name : '' }}</td>
                <td class="text-center">{{ $data->price_unit_qty }}</td>
                <td class="text-center">{{ $data->rate }}</td>
                <td class="text-right">{{ $data->price }}</td>
                <td class="text-center hip">
                    @if ($data->process_status == 'Pending')
                    <a class="btn btn-info btn-xs"  href="{{ url('ricemill/dellivery/item_edit/'.$data->id) }}" target="new"><i class="fa fa-edit"></i></a>
                    @endif
                </td>
            </tr>
            @endforeach
            <tr class="bg_gray">
                <th colspan="7" class="text-center" style="background:#ddd; font-weight:600; width:5%;">{{ trans('words.total') }}</th>
                <th class="text-center">{{ $total_quantity }}</th>
                <th class="text-center">{{ $total_netweight }}</th>
                <th colspan="2"></th>
                <th class="text-right">{{ $total_price }}</th>
                <th class="hip"></th>
            </tr>
        </tbody>
    </table>
</div>
@else
<div class="alert alert-info">No records found!</div>
@endif