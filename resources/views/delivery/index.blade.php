@extends('layouts.app')
@section('content')

<div id="breadcrumbBar" class="breadcrumb site_nav_links no_bdr_rad clearfix">
    <div class="col-md-3 col-sm-3 col-xs-2 cxs_2 no_pad">
        <button class="btn btn-info btn-xs" type="button" onclick="history.back()" title="Go Back"><span class="visible-xs"><i class="fa fa-arrow-left"></i></span><span class="hidden-xs">Back</span></button>
        <button class="btn btn-info btn-xs" onclick="redirectTo('<?= url('refresh_page') ?>')" title="Refresh" type="button"><span class="visible-xs"><i class="fa fa-refresh"></i></span><span class="hidden-xs">Refresh</span></button>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-6 cxs_10 text-center">
        <h2 class="page-title">{{ $page_title }}</h2>
    </div>
    <div class="col-md-3 col-sm-3 col-xs-4 cxs_12 no_pad">
        <ul class="text-right no_mrgn">
            <li><a href="{{ url('/home') }}"><i class="fa fa-fw fa-dashboard"></i> Dashboard</a> <span class="divider">/</span></li>
            <li><span>Delivery</span></li>
        </ul>
    </div>
</div>
<div class="well">
    <table width="100%">
        <tbody>
            <tr>
                <td class="wmd_70">
                    {!! Form::open(['method' => 'POST',  'class' => 'search-form', 'id' => 'frmSearch', 'name' => 'frmSearch']) !!}
                    <div class="input-group">
                        <div class="input-group-btn clearfix">
                            <?php echo number_dropdown(50, 550, 50) ?>
                            <div class="col-md-2 col-sm-3 no_pad" style="width: 10%;">
                                <input type="text" name="from_date" placeholder="(dd-mm-yyyy)" class="form-control pickdate" size="30" readonly>
                            </div>
                            <div style="width:5%" class="col-md-1 col-sm-1 no_pad">
                                <span style="font-size:14px; padding:14px; font-weight:600;">{{ trans('words.to') }}</span>
                            </div>
                            <div class="col-md-2 col-sm-3 no_pad" style="width: 10%;">
                                <input type="text" placeholder="(dd-mm-yyyy)" name="end_date" class="form-control pickdate" size="30" readonly>
                            </div>
                            <input type="text" style="width:15%;" name="search" id="q" class="form-control" placeholder="search challan" size="30"/>
                            <input type="text" style="width:15%;" name="_search_supplier" id="_search_supplier" class="form-control" placeholder="supplier/mobile" size="30"/>
                            <button type="button" id="search" class="btn btn-info">{{ trans('words.search') }}</button>
                            <button type="button" id="clear_from" class="btn btn-primary">{{ trans('words.clear') }}</button>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </td>
                <td class="text-right" style="width:20%">
                    @if (has_user_access('ricemill_delivery_create'))
                    <a href="{{ url ('dellivery/create') }}"><button class="btn btn-success btn-xs"><i class="fa fa-plus"></i>{{ trans('words.new') }}</button></a>
                    @endif
                    @if (has_user_access('ricemill_delivery_delete'))
                    <button type="button" class="btn btn-danger btn-xs" id="Del_btn" disabled><i class="fa fa-trash-o"></i>{{ trans('words.delete') }}</button>
                    @endif
                    <button class="btn btn-primary btn-xs" onclick="printDiv('print_area')"><i class="fa fa-print"></i>{{ trans('words.print') }}</button>
                </td>
            </tr>
        </tbody>
    </table>
</div>


<div class="order-list">
    {!! Form::open(['method' => 'POST', 'id'=>'frmList','name'=>'frmList']) !!}
    <div id="print_area">
        <?php print_header("Delivery List", true, false, []); ?>
        <div id="ajax_content">
            <div class="table-responsive">
                <table class="table table-bordered tbl_thin" id="check">
                    <tbody>
                        <tr class="bg_highlight" id="r_checkAll">
                            <th class="text-center" style="width:6%;">{{ trans('words.sl_no') }}</th>
                            <th>{{ trans('words.date') }}</th>
                            <th class="text-left">{{ trans('words.challan_no') }}</th>
                            <th class="text-left">{{ trans('words.customer_name') }}</th>
                            <th class="text-left">{{ trans('words.person_name') }}</th>
                            <th class="text-center">{{ trans('words.items') }}</th>
                            <th class="text-center">{{ trans('words.net_weight') }}</th>
                            <th class="text-right">{{ trans('words.price') }}</th>
                            <th class="text-center hip">{{ trans('words.actions') }}</th>
                            <th class="text-center hip">
                                <div class="checkbox">
                                    <label><input type="checkbox" id="check_all"value="all"></label>
                                </div>
                            </th>
                        </tr>
                        <?php
                        $counter = 0;
                        if (isset($_GET['page']) && $_GET['page'] > 1) {
                            $counter = ($_GET['page'] - 1) * $dataset->perPage();
                        }
                        $total_items = 0;
                        $total_price = 0;
                        $total_net_weight = 0;
                        $_itmes = 0;
                        $_price = 0;
                        ?>
                        @foreach ($dataset as $data)
                        <?php
                        $counter++;
                        $total_items += $_itmes = $data->count($data->id);
                        $total_price += $_price = $data->total_price($data->id);
                        $total_net_weight += $_net_weight = $data->total_net_weight($data->id);
                        ?>
                        <tr onmouseover="change_color(this, true)" onmouseout="change_color(this, false)">
                            <td class="text-center" style="width:5%;">{{ $counter }}</td>
                            <td>{{ date_dmy($data->date) }}</td>
                            <td class="text-left">{{$data->challan_no}}</td>
                            <td>{{ !empty($data->particular) ? $data->particular->name_address() : $data->from_subhead->name }}</td>
                            <td>{{ !empty($data->person_name) ? $data->person_name : '' }}</td>
                            <td class="text-center">{{ $_itmes }}</td>
                            <td class="text-center">{{ $_net_weight }}</td>
                            <td class="text-right">{{ $_price }}</td>
                            <td class="text-center hip">
                                @if (has_user_access('ricemill_delivery_details'))
                                <a class="btn btn-success btn-xs" href="{{ url('/ricemill/dellivery/itemlist/'.$data->_key) }}">Items</a>
                                @endif
                                @if($data->process_status == "Pending")
                                @if (has_user_access('ricemill_delivery_gatepass'))
                                <a class="btn btn-primary btn-xs" href="dellivery/gatepass/{{ $data->id }}"><i class="fa fa-outdent"></i> Gate Pass</a>
                                @endif
                                @if (has_user_access('ricemill_delivery_edit'))
                                <a class="btn btn-info btn-xs" href="dellivery/{{ $data->id }}/edit"><i class="fa fa-edit"></i> Edit</a>
                                @endif
                                @if (has_user_access('ricemill_delivery_process'))
                                <a class="btn btn-primary btn-xs" href="dellivery/{{ $data->id }}/confirm" onClick="return confirm('Are you sure, Sale Complete? This cannot be undone!');" ><i class="fa fa-gavel"></i> Process</a>
                                @endif
                                @endif
                                @if($data->process_status == "Complete")
                                @if (has_user_access('ricemill_delivery_details'))
                                <a class="btn btn-success btn-xs" href="dellivery/{{ $data->id }}"><i class="fa fa-plus"></i> View</a>
                                @endif
                                @if (has_user_access('ricemill_delivery_reset'))
                                <a class="btn btn-danger btn-xs" href="dellivery/{{ $data->id }}/reset" onClick="return confirm('Are you sure, Delivery Reset? This cannot be undone!');" ><i class="fa fa-refresh"></i> Reset</a>
                                @endif
                                @endif
                            </td>
                            <td class="text-center hip">
                                <div class="checkbox">
                                    @if (has_user_access('ricemill_delivery_delete'))
                                    <label><input type="checkbox" name="data[]" value="{{ $data->id }}"></label>
                                    @endif
                                </div>
                            </td>
                        </tr>
                        @endforeach
                        <tr class="bg_gray">
                            <th colspan="6" class="text-center" style="background:#ddd; font-weight:600; width:5%;">{{ trans('words.total') }}</th>
                            <th class="text-center">{{ $total_items }}</th>
                            <th class="text-center">{{ $total_net_weight }}</th>
                            <th class="text-right">{{ $total_price }}</th>
                            <th></th>
                            <th colspan="3"></th>
                        </tr>
                    </tbody>
                </table>
                <div class="text-center hip">
                    {{ $dataset->render() }}
                </div>
            </div>
        </div>
    </div>
</div>
{!! Form::close() !!}

<script type="text/javascript">
    $(document).ready(function () {
        $("#search").click(function () {
            if ($("#institute_id").val() == '') {
                $("#ajaxMessage").showAjaxMessage({html: 'Company Required.', type: 'error'});
                $("#institute_id").focus();
                return false;
            }
            hideAjaxMessage();

            var _url = "{{ URL::to('ricemill/dellivery/search') }}";
            var _form = $("#frmSearch");

            $.ajax({
                url: _url,
                type: "post",
                data: _form.serialize(),
                success: function (data) {
                    $('#ajax_content').html(data);
                },
                error: function (xhr, status) {
                    alert('There is some error.Try after some time.');
                }
            });

        })

        $("#Del_btn").click(function () {
            var _url = "{{ URL::to('ricemill/dellivery/delete') }}";
            var _form = $("#frmList");
            var _rc = confirm("Are you sure about this action? This cannot be undone!");

            if (_rc == true) {

                $.post(_url, _form.serialize(), function (data) {
                    if (data.success === true) {
                        $('#ajaxMessage').removeClass('alert-danger').addClass('alert-success').show().show();
                        $('#ajaxMessage').html(data.message);
                    } else {
                        $('#ajaxMessage').removeClass('alert-success').addClass('alert-danger').show();
                        $('#ajaxMessage').html(data.message);
                    }
                    setTimeout(function () {
                        window.location.reload();
                    }, 3000);
                }, "json");

            }

        })

    });
</script>
@endsection