@extends('layouts.app')
@section('content')
<div id="breadcrumbBar" class="breadcrumb site_nav_links no_bdr_rad clearfix">
    <div class="col-md-3 col-sm-3 col-xs-2 cxs_2 no_pad">
        <button class="btn btn-info btn-xs" type="button" onclick="history.back()" title="Go Back"><span class="visible-xs"><i class="fa fa-arrow-left"></i></span><span class="hidden-xs">Back</span></button>
        <button class="btn btn-info btn-xs" onclick="redirectTo('<?= url('refresh_page') ?>')" title="Refresh" type="button"><span class="visible-xs"><i class="fa fa-refresh"></i></span><span class="hidden-xs">Refresh</span></button>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-6 cxs_10 text-center">
        <h2 class="page-title">Sales Details</h2>
    </div>
    <div class="col-md-3 col-sm-3 col-xs-4 cxs_12 no_pad">
        <ul class="text-right no_mrgn">
            <li><a href="{{ url('/home') }}"><i class="fa fa-fw fa-dashboard"></i> Dashboard</a> <span class="divider">/</span></li>
            <li><a href="{{ url('/ricemill/dellivery') }}"><span> Details</span></a></li>
        </ul>
    </div>
</div>
<div class="invoice_area" id="print_area">
    <?php
    print_header("Rice Sales Challan", true, true, null, $data->institute_id);
    ?>

    <table class="table table-bordered tbl_thin" style="margin:0;">
        <tr>
            <td style="width:8%"><strong>Date:</strong> <?= ( Auth::user()->locale == 'bn' ) ? en2bnDate(date_dmy($data->date)) : date_dmy($data->date); ?> </td>
            <td style="width:8%"><strong>Challan No:</strong> {{ $data->challan_no}} </td>
            <td style="width:20%"><strong>Party Name :</strong> {{ !empty($data->particular) ? $data->particular->name_address() : $data->from_subhead->name }}</td>
            <td style="width:10%"><strong>Note :</strong> {{ $data->note }}</td>
        </tr>
    </table>
    <div class="product_info">
        <div class="table-responsive">
            <table class="table table-bordered tbl_thin" id="check" style="margin-bottom:15px;">
                <tbody>
                    <tr class="bg_gray" id="r_checkAll">
                        <th class="text-center" style="width:5%;">#</th>
                        <th class="text-left" style="width:20%;">Product</th>
                        <th class="text-left" style="width:8%;">Product Size</th>
                        <th class="text-left" style="width:5%;">Qty</th>
                        <th class="text-left" style="width:8%;">Net Weight</th>
                        <th class="text-left" style="width:8%;">Price Unit</th>
                        <th class="text-right" style="width:10%;">Price</th>
                        <th class="text-right" style="width:10%;">Total Price</th>
                    </tr>
                    <?php
                    $counter = 0;
                    $total_quantity = 0;
                    $total_price = 0;
                    $total_rate = 0;
                    ?>
                    @foreach ($itemDataset as $data)
                    <?php
                    $counter++;
                    $total_quantity += $data->quantity;
                    $total_rate += $data->rate;
                    $total_price += $data->price;
                    ?>
                    <tr>
                        <td class="text-center" style="width:5%;">{{ $counter }}</td>
                        <td class="text-left" style="width:20%;">{{!empty( $data->product) ? $data->product->name : '' }}</td>
                        <td class="text-left" style="width:8%;">{{!empty( $data->productSize) ? $data->productSize->name : '' }}</td>
                        <td class="text-center" style="width:5%;">{{ $data->quantity }}</td>
                        <td class="text-center" style="width:8%;">{{ $data->net_weight }}</td>
                        <td class="text-center" style="width:8%;">{{ !empty($data->unit_price) ? $data->unit_price->unit_name : '' }}</td>
                        <td class="text-right" style="width:10%;">{{ $data->rate }}</td>
                        <td class="text-right" style="width:10%;">{{ $data->price }}</td>
                    </tr>
                    @endforeach
                    <tr class="bg_gray">
                        <th colspan="3" class="text-center" style="background:#ddd; font-weight:600; width:5%;">{{ trans('words.total') }}</th>
                        <th class="text-center"> <input type="hidden" id="current_bag_qty" name="current_bag_qty" value="{{ $total_quantity }}"> {{ $total_quantity }}</th>
                        <th  colspan="2"></th>
                        <th class="text-right">{{ $total_rate }}</th>
                        <th class="text-right">{{ $total_price }}</th>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="row clearfix" style="padding-top:30px;">
            <div class="col-md-4 form-group mp_30">
                <div style="border-top:1px solid #000000;text-align:center;">Customer</div>
            </div>
            <div class="col-md-4 form-group mp_30">
                <div style="border-top:1px solid #000000;text-align:center;">Manager</div>
            </div>
            <div class="col-md-4 form-group mp_30">
                <div style="border-top:1px solid #000000;text-align:center;">Managing Director</div>
            </div>
        </div>
    </div>
</div>
<div class="text-center">
    <button class="btn btn-primary" onclick="printDiv('print_area')"><i class="fa fa-print"></i> Print</button>
</div>
@endsection