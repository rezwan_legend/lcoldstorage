@extends('layouts.app')
@section('content')
<div id="breadcrumbBar" class="breadcrumb site_nav_links no_bdr_rad clearfix">
    <div class="col-md-2 col-sm-3 col-xs-2 cxs_2 no_pad">
        <button class="btn btn-info btn-xs" type="button" onclick="history.back()" title="Go Back"><span class="visible-xs"><i class="fa fa-arrow-left"></i></span><span class="hidden-xs">Back</span></button>
        <button class="btn btn-info btn-xs" onclick="redirectTo('<?= url('refresh_page') ?>')" title="Refresh" type="button"><span class="visible-xs"><i class="fa fa-refresh"></i></span><span class="hidden-xs">Refresh</span></button>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-6 cxs_10 text-center">
        <h2 class="page-title">{{ trans('words.dellivery_details') }}</h2>
    </div>
    <div class="col-md-4 col-sm-3 col-xs-4 cxs_12 no_pad">
        <ul class="text-right no_mrgn">
            <li><a href="{{ url('/home') }}">{{ trans('words.dashboard') }}</a> <span class="divider">/</span></li>
            <li><a href="{{ url('/ricemill/dellivery') }}">{{ trans('words.dellivery_details') }}</a> <span class="divider">/</span></li>
            <li>{{ trans('words.items') }}</li>
        </ul>
    </div>
</div>

<div class="panel panel-primary">
    <div class="panel-heading">
        <h3 class="panel-title">Delivery Information</h3>
    </div>
    <div class="panel-body">
        <div class="row clearfix">
            <div class="col-md-2">
                <div class="mb_10 clearfix">
                    <label for="company_id">{{ trans('words.company') }}</label>
                    <input type="text" class="form-control" id="challan_no" name="challan_no" value="{{ !empty($data->company)? $data->company->name :"" }}" disabled>
                </div>
            </div>

            <div class="col-md-2">
                <div class="mb_10 clearfix">
                    <label for="order_date">{{ trans('words.date') }}</label>
                    <div class="input-group">
                        <input type="text" class="form-control pickdate" id="order_date" name="order_date" value="{{ date_dmy($data->date) }}" disabled>
                        <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                    </div>
                </div>
            </div>
            <div class="col-md-2">
                <div class="mb_10 clearfix">
                    <label for="supplier_fatherName">{{ trans('words.challan_no') }}</label>
                    <input type="number" class="form-control" id="challan_no" name="challan_no" value="{{ $data->challan_no }}" disabled>
                </div>
            </div>
            <div class="col-md-2">
                <div class="mb_10 clearfix">
                    <label for="from_subhead_id">{{ trans('words.party') }}</label>
                    <input type="text" class="form-control" id="challan_no" name="challan_no" value="{{ !empty($data->particular) ? $data->particular->name : $data->from_subhead->name }}" disabled>
                </div>
            </div>
            <div class="col-md-2">
                <div class="mb_10 clearfix">
                    <label for="from_subhead_id">{{ trans('words.order_no') }}</label>
                    <input type="text" class="form-control" id="challan_no" name="challan_no" value="{{ $data->order_no }}" disabled>
                </div>
            </div>
            <div class="col-md-2">
                <div class="mb_10 clearfix">
                    <label for="from_subhead_id">{{ trans('words.scale_weight') }}</label>
                    <input type="text" class="form-control" id="scal_weight" name="scal_weight" value="{{ $data->scal_weight }}" disabled>
                </div>
            </div>
            <div class="col-md-2">
                <div class="mb_10 clearfix">
                    <label for="from_subhead_id">{{ trans('words.bag') }}</label>
                    <input type="text" class="form-control" id="quantity" name="quantity" value="{{ $data->quantity }}" disabled>
                </div>
            </div>
            <div class="col-md-2">
                <div class="mb_10 clearfix">
                    <label for="from_subhead_id">{{ trans('words.challan_weight') }}</label>
                    <input type="text" class="form-control" id="challan_weight" name="challan_weight" value="{{ $data->challan_weight }}" disabled>
                </div>
            </div>
            <!--            <div class="col-md-2">
                            <div class="mb_10 clearfix">
                                <label for="from_subhead_id">{{ trans('words.less_weight') }}</label>
                                <input type="number" class="form-control" id="less_weight" name="less_weight" value="{{ $data->less_weight }}" disabled>
                            </div>
                        </div> -->
        </div>
    </div>
</div>

<div class="clearfix" style="border-bottom: 1px dashed #545454;margin: 15px 0;"></div>

@if($data->process_status == "Pending")
<div class="panel panel-primary">
    <div class="panel-heading">
        <h3 class="panel-title">Please Add Item Information</h3>
    </div>
    <div class="panel panel-primary" style="border: none; padding-bottom: 0px">
        <div class="panel-body" style="padding-bottom: 0px; ">
            {!! Form::open(['method' => 'POST', 'url' => 'ricemill/dellivery/add_item', 'id' => 'frmOrderItem']) !!}
            <input type="hidden" id="challan_id" name="challan_id" value="{{ $data->challan_no }}">
            <input type="hidden" id="dellivery_id" name="dellivery_id" value="{{ $data->id }}">
            <input type="hidden" id="purchases_date" name="purchases_date" value="{{ $data->date }}">
            <input type="hidden" id="avg_weight" name="avg_weight" value="{{ $data->avg_weight }}">
            <input type="hidden" id="bag_qty" name="bag_qty" value="{{ $data->quantity }}">
            <input type="hidden" id="company_id" name="company_id" value="{{ $data->company_id }}">
            <input type="hidden" id="avg_price" name="avg_price" value="">
            <input type="hidden" id="last_cost_price" name="last_cost_price" value="">
            <input type="hidden" id="last_sale_price" name="last_sale_price" value="">
            <input type="hidden" id="available_stock" name="available_stock" value="">
            <input type="hidden" id="unit_value" name="unit_value" value="">
            <input type="hidden" id="size_weight" name="size_weight" value="">
            <input type="hidden" id="sale_unit_weight" name="sale_unit_weight" value="">

            <div class="row clearfix" >
                <div class="col-md-2" style="margin-right: 0px; padding-right: 0px">
                    <div class="mb_10 clearfix" style="margin-left: 0px; padding-left: 0px ">
                        <label for="product_type">{{ trans('words.select_type') }}</label>
                        <select class="form-control select2search"  id="product_type" name="product_type" required>
                            <option value="" class="">Product Type</option>
                            @foreach( $product_type as $prod_type )
                            <option value="{{ $prod_type->id }}">{{ $prod_type->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-2" style="margin-right: 0px; padding-right: 0px">
                    <div class="mb_10 clearfix" style="margin-left: 0px; padding-left: 0px ">
                        <label for="category">{{ trans('words.select_category') }}</label>
                        <select class="form-control select2search" onchange="productByCategory(this.value);" id="category" name="category" required>
                            <option value="" class="">Category</option>
                        </select>
                    </div>
                </div>

                <div class="col-md-2" style="margin-right: 0px; padding-right: 0px">
                    <div class="mb_10 clearfix">
                        <label for="product">{{ trans('words.select_product') }}</label>
                        <select class="form-control select2search" id="product_id" name="product_id">
                            <option value=""> Product</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-2" style="margin-right: 0px; padding-right: 0px">
                    <div class="mb_10 clearfix">
                        <label for="size">{{ trans('words.select_size') }}</label>
                        <select  class="form-control select2search" id="size_id" name="size_id">
                            <option value="">Size</option>

                        </select>
                    </div>
                </div>
                <div class="col-md-2" style="margin-right: 0px; padding-right: 0px">
                    <div class="mb_10 clearfix">
                        <label for="unit">{{ trans('words.select_unit') }}</label>
                        <select  class="form-control select2search" id="unit_id" name="unit_id">
                            <option value=""> Unit </option>

                        </select>
                    </div>
                </div>
                <div class="col-md-2" style="margin-right: 0px; padding-right: 0px">
                    <div class="mb_10 clearfix">
                        <label for="qty">{{ trans('words.qty') }} </label>
                        <input type="number" class="form-control text-left qty_rate" id="qty" name="qty" value="" placeholder="Qty" min="0" step="any">
                    </div>
                </div>
                <div class="col-md-2" style="margin-right: 0px;padding-right: 0px">
                    <div class="mb_10 clearfix">
                        <label for="net_weight">{{ trans('words.net_weight') }} </label>
                        <input type="number" class="form-control text-left qty_rate" id="net_weight" name="net_weight" value="" placeholder="net weight" min="0" step="any">
                    </div>
                </div>
                <div class="col-md-2" style="margin-right: 0px;padding-right: 2px;">
                    <div class="mb_10 clearfix">
                        <label for="net_qty">নেট পরিমাণ</label>
                        <input type="number" class="form-control text-left qty_rate" id="net_qty" name="net_qty" value="" placeholder="net qty" min="0" step="any" readonly="">
                    </div>
                </div>
                <div class="col-md-2" style="margin-right: 0px; padding-right: 0px">
                    <div class="mb_10 clearfix">
                        <label for="unit_price">{{ trans('words.sale_price_unit') }}</label>
                        <select  class="form-control select2search" id="price_unit_id" name="price_unit_id">
                            <option value=""> Sale Price Unit</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-2" style="margin-right: 0px; padding-right: 0px">
                    <div class="mb_10 clearfix">
                        <label for="price_qty_unit">{{ trans('words.sale_qty') }}</label>
                        <input type="number" class="form-control text-left qty_rate" id="price_qty_unit" name="price_qty_unit" value="" placeholder="price qty unit" min="0" step="any">
                    </div>
                </div>
                <div class="col-md-1" style="margin-right: 0px; padding-right: 0px">
                    <div class="mb_10 clearfix">
                        <label for="rate"> {{ trans('words.rate') }}</label>
                        <input type="number" class="form-control text-left qty_rate" id="rate" name="rate" value="" placeholder="rate" min="0" step="any">
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="mb_10 clearfix">
                        <label for="price"> {{ trans('words.total_price') }}</label>
                        <input type="number" class="form-control text-left qty_rate" id="price" name="price" value="" placeholder="price" min="0" step="any" readonly>
                    </div>
                </div>
                <div class="col-md-2 d-inline-block" style="padding: 0px;margin-top: 27px;width: auto;">
                    <div class="mb_10 clearfix">
                        <button type="submit" class="btn btn-info btn-xs xsw_100" id="addItem" title="Add Item" style="height: 31px;width: 38px;"><i class="fa fa-share"></i></button>
                    </div>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
        <div>
            <label style="color: red">Stock :</label>
            <label style="color: red" id="c_stock" value=""></label>
        </div>
    </div>
</div>
@endif
<div class="clearfix" style="border-bottom: 1px dashed #545454;margin: 15px 0;"></div>

<div class="clearfix">
    <table class="table table-bordered tbl_thin" style="margin: 0">
        <tr class="bg-primary">
            <th class="text-center">{{ trans('words.dellivery_details') }}</th>
        </tr>
    </table>
    <div class="clearfix" id="order_itemlist_partial">
        @if(!empty($itemDataset) && count($itemDataset) > 0)
        <div class="table-responsive">
            <table class="table table-bordered tbl_thin" id="check">
                <tbody>
                    <tr class="bg_highlight" id="r_checkAll">
                        <th class="text-center" style="width:3%;">#</th>
                        <th>{{ trans('words.product_type') }}</th>
                        <th>{{ trans('words.category') }}</th>
                        <th>{{ trans('words.product') }}</th>
                        <th>{{ trans('words.size') }}</th>
                        <th>{{ trans('words.unit') }}</th>
                        <th class="text-center">{{ trans('words.qty') }}</th>
                        <th class="text-left">{{ trans('words.net_weight') }} </th>
                        <th class="text-left">{{ trans('words.sale_price_unit') }} </th>
                        <th class="text-center">{{ trans('words.sale_qty') }}  </th>
                        <th class="text-center">{{ trans('words.rate') }}</th>
                        <th class="text-right">{{ trans('words.total_price') }}</th>
                        @if($data->process_status == "Pending")
                        <th class="text-center hip">{{ trans('words.actions') }}</th>
                        @endif
                    </tr>
                    <?php
                    $counter = 0;
                    $total_quantity = 0;
                    $total_price = 0;
                    ?>
                    @foreach ($itemDataset as $data)
                    <?php
                    $counter++;
                    $total_quantity += $data->quantity;
                    $total_price += $data->price;
                    ?>
                    <tr onmouseover="change_color(this, true)" onmouseout="change_color(this, false)">
                        <td class="text-center" style="width:5%;">{{ $counter }}</td>
                        <td class="text-left">{{!empty( $data->product_type ) ? $data->product_type->name : '' }}</td>
                        <td class="text-left">{{!empty( $data->category ) ? $data->category->name : '' }}</td>
                        <td class="text-left">{{!empty( $data->product ) ? $data->product->name : '' }}</td>
                        <td class="text-left">{{!empty( $data->productSize ) ? $data->productSize->name : '' }}</td>
                        <td>{{!empty( $data->productUnit ) ? $data->productUnit->name : '' }}</td>
                        <td  class="text-center">{{ $data->quantity }}</td>
                        <td>{{  $data->net_weight }}</td>
                        <td class="text-left">{{!empty( $data->unit_price ) ? $data->unit_price->unit_name : '' }}</td>
                        <td  class="text-center">{{ $data->price_unit_qty }}</td>
                        <td class="text-center">{{ $data->rate }}</td>
                        <td class="text-right">{{ $data->price }}</td>
                        @if($data->process_status == "Pending")
                        <td class="text-center hip">
                            <a class="color_danger" title="Delete Item" href="{{ url('ricemill/dellivery/remove_item/'.$data->id) }}" onclick="return confirm('Are you sure about this action?')"><i class="fa fa-trash-o"></i></a>
                        </td>
                        @endif
                    </tr>
                    @endforeach
                    <tr class="bg_gray">
                        <th colspan="6" class="text-center" style="background:#ddd; font-weight:600; width:5%;">Total</th>
                        <th class="text-center">{{ $total_quantity }}</th>
                        <th class="text-left" colspan="4"></th>
                        <th class="text-right">{{ $total_price }}</th>
                        @if($data->process_status == "Pending")
                        <th class="hip" colspan="3"></th>
                        @endif
                    </tr>
                </tbody>
            </table>
        </div>
        @else
        <div class="alert alert-info">No items found</div>
        @endif
    </div>
</div>

<div class="text-center" style="margin-top:10px;">
    <a class="btn btn-primary" href="{{ url('/ricemill/dellivery/') }}">Save</a>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $(document).on("change", "#product_type ", function (e) {
            var _companyId = $("#company_id").val();
            var url = "{{ URL::to('category/categoryByType') }}";
            $.ajax({
                type: 'POST',
                url: url,
                data: {
                    companyId: _companyId,
                    productTypeId: this.value,
                    _token: _token
                },
                success: function (response) {
                    $("#category").html(response.category);
                    $("#size_id").html(response.sizes);
                    $("#price_unit_id").html(response.price_units);
                    $("#unit_id").html(response.units);
                }
            });

        });

        $(document).on("submit", "#frmOrderItem", function (event) {
            $('#ajaxMessage').hide();
            var dellivery_id = $("#dellivery_id").val();
            var _url = baseUrl + "/ricemill/dellivery/add_item";
            var _form = $("#frmOrderItem");
            $.post(_url, _form.serialize(), function (res) {
                if (res.success === true) {
                    render_dellivery_items(dellivery_id);
                } else {
                    $("#ajaxMessage").showAjaxMessage({html: res.message, type: "error"});
                }
            }, "json");
            event.preventDefault();
            return false;
        });

        $(document).on("input", "#qty", function (e) {
            net_weight();
            e.preventDefault();
        });

        $(document).on("input", "#price_qty_unit", function (e) {
            net_price();
            e.preventDefault();
        });

        $(document).on("input", "#net_weight", function (e) {
            _net_quantity();
            e.preventDefault();
        });

        $(document).on("input", "#rate", function (e) {
            net_price();
            e.preventDefault();
        });

        $(document).on("change", "#product_id", function (e) {
            var productId = this.value;
            stockInfo(productId);
            e.preventDefault();
        });

        $(document).on("change", "#size_id", function (e) {
            $("#size_weight").val(get_selected_option_info(this, 'data-weight'));
            var type = $("#product_type").val();
            var productId = $("#product_id").val();
            var sizeId = this.value;
            if (type == RICE_FINISH) {
                stockInfo(productId, sizeId);
            } else {
                stockInfo(productId);
            }
            stockInfo(productId, sizeId);
            e.preventDefault();
        });

        $(document).on("change", "#price_unit_id", function (e) {
            $("#sale_unit_weight").val(get_selected_option_info(this, 'data-weight'));
            _price_qty_unit();
            e.preventDefault();
        });
    });

    function net_weight() {
        hideAjaxMessage();
        var _qty = Number($("#qty").val());
        var _rate = Number($("#rate").val());
        console.log(_rate);
        var _avg_weight = $("#avg_weight").val();
        var _available_stock = $("#available_stock").val();
        var _product_type = $("#product_type").val();
        var _size_weight = $("#size_weight").val();
        if (_available_stock < _qty) {
            $("#ajaxMessage").showAjaxMessage({html: `<b>Total Bag Must not greater than </b>${_available_stock}</b>`, type: 'error'});
            disable(addItem);
            return false;
        }
        enable(addItem);
        var _net_weight = 0;
        if (isNaN(_qty)) {
            _qty = 0;
        }
        if (isNaN(_rate)) {
            _rate = 0;
        }
        if (isNaN(_avg_weight)) {
            _avg_weight = 0;
        }
//        if (_product_type == RICE_RAW) {
//            _net_weight = (parseFloat(_qty) * parseFloat(_avg_weight)).toFixed(3);
//            $("#net_weight").val(_net_weight);
//            net_price();
//        } else if (_product_type == RICE_FINISH) {
//            consol.log(_size_weight);
//            _net_weight = (parseFloat(_qty) * parseFloat(_size_weight)).toFixed(3);
//            $("#net_weight").val(_net_weight);
//            net_price();
//        } else {
//            var _price = _qty * _rate;
//            $("#price").val(_price);
//        }

        _net_weight = (parseFloat(_qty) * parseFloat(_size_weight)).toFixed(3);
        $("#net_weight").val(_net_weight);
        _net_quantity();
    }

    function render_dellivery_items(dellivery_id) {
        var _company_id = $("#company_id").val();
        var _url = baseUrl + "/ricemill/dellivery/search_itemlist";
        var _formData = {};
        _formData.company_id = _company_id;
        _formData.dellivery_id = dellivery_id;
        _formData._token = _token;
        $.ajax({
            url: _url,
            type: "POST",
            data: _formData,
            success: function (res) {
                $("#order_itemlist_partial").html(res);
                $("#qty").val(" ");
                $("#net_weight").val(" ");
                $("#net_qty").val(" ");
                $("#price_qty_unit").val(" ");
                $("#rate").val(" ");
                $("#price").val(" ");
            },
            error: function (xhr, status) {
                alert('There is some error.Try after some time.');
            }
        });
    }

    function productByCategory(category) {
        if (category) {
            var _company_id = $("#company_id").val();
            var _productType = $("#product_type").val();
            var url = "{{ URL::to('product/prod_by_category') }}";
            $.ajax({
                type: 'POST',
                url: url,
                data: {
                    companyId: _company_id,
                    productType: _productType,
                    categoryId: category,
                    _token: _token
                },
                success: function (response) {
                    $("#product_id").html(response);
                }
            });
        }
    }

    function _net_quantity() {
        var _size_weight = $("#size_weight").val();
        var _net_weight = Number($("#net_weight").val());
        var _net_qty = 0;
        if (isNaN(_size_weight)) {
            _size_weight = 0;
        }
        if (isNaN(_net_weight)) {
            _net_weight = 0;
        }
        _net_qty = (parseFloat(_net_weight) / parseFloat(_size_weight)).toFixed(2);
        $("#net_qty").val(_net_qty);
        var _ptype = $("#product_type").val();
        var _qty = Number($("#qty").val());
        if (_ptype == RICE_EBAG) {
            $("#net_qty").val(_qty);
        }
        _price_qty_unit();
    }

    function net_price() {
        var _rate = Number($("#rate").val());
        var _price_qty_unit = Number($("#price_qty_unit").val());
        var _price = 0;
        if (isNaN(_rate)) {
            _rate = 0;
        }
        if (isNaN(_price_qty_unit)) {
            _price_qty_unit = 0;
        }
        _price = (parseFloat(_rate) * parseFloat(_price_qty_unit)).toFixed(3);
        var roundPrice = Math.round(_price);
        $("#price").val(roundPrice);
        avg_price();
    }

    function avg_price() {
        var _qty = Number($("#qty").val());
        var _price = Number($("#price").val());
        var _avg_price = 0;
        if (isNaN(_qty)) {
            _qty = 0;
        }
        if (isNaN(_price)) {
            _price = 0;
        }
        _avg_price = (parseFloat(_price) / parseFloat(_qty)).toFixed(3);
        $("#avg_price").val(_avg_price);
    }

    function stockInfo(productId, sizeId = null) {
        hideAjaxMessage();
        var _companyId = $("#company_id").val();
        var _category_id = $("#category").val();
        var _prod_type = $("#prod_type").val();
        if (sizeId) {
            var _size_id = sizeId;
        }
        $.ajax({
            type: 'POST',
            url: "{{ URL::to('ricemill/purchase_stocks') }}",
            data: {companyId: _companyId, category_id: _category_id, product_type: _prod_type, product_id: productId, size_id: _size_id, _token: _token, },
            success: function (response) {
                if (response.success === true) {
                    hide_ajax_message();
                    $("#available_stock").val(response.available_stock);
                    $("#c_stock").html(response.available_stock);
                    $("#rate").val(response.sale_price);
                    $("#last_sale_price").val(response.sale_price);
                    $("#last_cost_price").val(response.cost_price);
                    net_weight();
                } else {
                    $("#available_stock").val("");
                    $("#c_stock").html("");
                    $("#rate").val("");
                    $("#sale_price").val("");
                    $("#cost_price").val("");
                    disable(addItem);
                    $("#ajaxMessage").showAjaxMessage({html: response.message, type: "error"});
                }

            }
        });
    }

    function _price_qty_unit() {
        var _net_weight = Number($("#net_weight").val());
        var _price_unit_id = Number($("#price_unit_id").val());
        var _qty = Number($("#qty").val());
        var _net_qty = Number($("#net_qty").val());
        var _sale_unit_weight = $("#sale_unit_weight").val();

        if (isNaN(_qty)) {
            _qty = 0;
        }
        if (_price_unit_id === RICE_BAG) {
            $("#price_qty_unit").val(_qty);
        } else if (price_unit_id === RICE_PICES) {
            console.log("pcs");
            $("#price_qty_unit").val(_net_qty);
        } else if (_price_unit_id === RICE_KG) {
            console.log("kg");
            $("#price_qty_unit").val(_net_weight);
        } else if (_price_unit_id === RICE_MON) {
            console.log("mon");
            var _mon = (_net_weight / _sale_unit_weight).toFixed(2);
            $("#price_qty_unit").val(_mon);
        } else {
            console.log("else");
            $("#price_qty_unit").val(_net_qty);
        }
        net_price();
    }

</script>
@endsection