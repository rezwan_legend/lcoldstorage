@if (!empty($dataset) && count($dataset) > 0) 
<div class="table-responsive">
    <table class="table table-bordered tbl_thin" id="check">
        <tbody>
            <tr class="bg_highlight" id="r_checkAll">
                <th class="text-center" style="width:6%;">{{ trans('words.sl_no') }}</th>
                <th class="text-left">{{ trans('words.company') }}</th>
                <th>{{ trans('words.date') }}</th>
                <th class="text-left">{{ trans('words.challan_no') }}</th>
                <th class="text-left">{{ trans('words.customer_name') }}</th>
                <th class="text-left">{{ trans('words.person_name') }}</th>
                <th class="text-center">{{ trans('words.items') }}</th>
                <th class="text-center">{{ trans('words.net_weight') }}</th>
                <th class="text-right">{{ trans('words.price') }}</th>
                <th class="text-center hip">{{ trans('words.actions') }}</th>
                <th class="text-center hip">
                    <div class="checkbox">
                        <label><input type="checkbox" id="check_all"value="all"></label>
                    </div>
                </th>
            </tr>
            <?php
            $counter = 0;
            if (isset($_GET['page']) && $_GET['page'] > 1) {
                $counter = ($_GET['page'] - 1) * $dataset->perPage();
            }
            $total_items = 0;
            $total_price = 0;
            $total_net_weight = 0;
            $_itmes = 0;
            $_price = 0;
            ?>
            @foreach ($dataset as $data)
            <?php
            $counter++;
            $total_items += $_itmes = $data->count($data->id);
            $total_price += $_price = $data->total_price($data->id);
            $total_net_weight += $_net_weight = $data->total_net_weight($data->id);
            ?>   
            <tr onmouseover="change_color(this, true)" onmouseout="change_color(this, false)">
                <td class="text-center" style="width:5%;">{{ $counter }}</td>
                <td class="text-left">{{ !empty($data->company) ? $data->company->name : '' }}</td>
                <td>{{ date_dmy($data->date) }}</td>
                <td class="text-left">{{$data->challan_no}}</td>
                <td>{{ !empty($data->particular) ? $data->particular->name_address() : $data->from_subhead->name }}</td>
                <td>{{ !empty($data->person_name) ? $data->person_name : '' }}</td>
                <td class="text-center">{{ $_itmes }}</td>
                <td class="text-center">{{ $_net_weight }}</td>
                <td class="text-right">{{ $_price }}</td>
                <td class="text-center hip">
                    @if (has_user_access('ricemill_delivery_details')) 
                    <a class="btn btn-success btn-xs" href="{{ url('/ricemill/dellivery/itemlist/'.$data->_key) }}">Items</a> 
                    @endif
                    @if($data->process_status == "Pending")
                    @if (has_user_access('ricemill_delivery_gatepass'))
                    <a class="btn btn-primary btn-xs" href="dellivery/gatepass/{{ $data->id }}"><i class="fa fa-outdent"></i> Gate Pass</a>
                    @endif
                    @if (has_user_access('ricemill_delivery_edit'))
                    <a class="btn btn-info btn-xs" href="dellivery/{{ $data->id }}/edit"><i class="fa fa-edit"></i> Edit</a>
                    @endif
                    @if (has_user_access('ricemill_delivery_process'))
                    <a class="btn btn-primary btn-xs" href="dellivery/{{ $data->id }}/confirm" onClick="return confirm('Are you sure, Sale Complete? This cannot be undone!');" ><i class="fa fa-gavel"></i> Process</a>
                    @endif
                    @endif
                    @if($data->process_status == "Complete")
                    @if (has_user_access('ricemill_delivery_details')) 
                    <a class="btn btn-success btn-xs" href="dellivery/{{ $data->id }}"><i class="fa fa-plus"></i> View</a> 
                    @endif
                    @if (has_user_access('ricemill_delivery_reset'))
                    <a class="btn btn-danger btn-xs" href="dellivery/{{ $data->id }}/reset" onClick="return confirm('Are you sure, Sales Reset? This cannot be undone!');" ><i class="fa fa-refresh"></i> Reset</a>
                    @endif
                    @endif
                </td>
                <td class="text-center hip">
                    <div class="checkbox">
                        @if (has_user_access('ricemill_delivery_delete'))
                        <label><input type="checkbox" name="data[]" value="{{ $data->id }}"></label>
                        @endif
                    </div>
                </td>
            </tr>
            @endforeach
            <tr class="bg_gray">
                <th colspan="6" class="text-center" style="background:#ddd; font-weight:600; width:5%;">{{ trans('words.total') }}</th>
                <th class="text-center">{{ $total_items }}</th>
                <th class="text-center">{{ $total_net_weight }}</th>
                <th class="text-right">{{ $total_price }}</th>
                <th></th>
                <th colspan="3"></th>
            </tr>
        </tbody>
    </table>
    <div class="text-center hip">
        {{ $dataset->render() }}
    </div>
</div>
@else
<div class="alert alert-info">No records found!</div>
@endif