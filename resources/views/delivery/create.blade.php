@extends('layouts.app')
@section('content')
<div id="breadcrumbBar" class="breadcrumb site_nav_links no_bdr_rad clearfix">
    <div class="col-md-2 col-sm-3 col-xs-2 cxs_2 no_pad">
        <button class="btn btn-info btn-xs" type="button" onclick="history.back()" title="Go Back"><span class="visible-xs"><i class="fa fa-arrow-left"></i></span><span class="hidden-xs">Back</span></button>
        <button class="btn btn-info btn-xs" onclick="redirectTo('<?= url('refresh_page') ?>')" title="Refresh" type="button"><span class="visible-xs"><i class="fa fa-refresh"></i></span><span class="hidden-xs">Refresh</span></button>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-6 cxs_10 text-center">
        <h2 class="page-title">{{ $page_title }} </h2>
    </div>
    <div class="col-md-4 col-sm-3 col-xs-4 cxs_12 no_pad">
        <ul class="text-right no_mrgn">
            <li><a href="{{ url('/home') }}"><i class="fa fa-fw fa-dashboard"></i>{{ trans('words.dashboard') }}  </a> <span class="divider">></span></li>
            <li><a href="{{ url('/dellivery') }}">{{ trans('words.delivery_order') }}</a></li>
        </ul>
    </div>
</div>
<div class="panel panel-primary">
    <div class="panel-heading">
        <h3 class="panel-title">{{ $page_title }}</h3>
    </div>
    <div class="panel-body">
        {!! Form::open(['method' => 'POST', 'url' => 'dellivery', 'id' => 'frm_purchase']) !!}
        <input type="hidden" id="sales_id" name="sales_id" value="">
        <div class="row clearfix">
            <div class="col-md-2" >
                <div class="mb_10 clearfix" >
                    <label for="order_date">{{ trans('words.date') }}</label>
                    <div class="input-group">
                        <input type="text" class="form-control pickdate" id="order_date" name="order_date" value="<?php echo date('d-m-Y'); ?>" required readonly>
                        <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                    </div>
                    <small class="text-danger">{{ $errors->first('order_date') }}</small>
                </div>
            </div>
            <div class="col-md-2">
                <div class="mb_10 clearfix">
                    <label for="delivery_no">{{ trans('words.delivery_no') }}</label>
                    <input type="text" class="form-control" id="delivery_no" name="delivery_no" value="{{ $deliveryNo }}" placeholder="Delivery no" readonly>
                </div>
            </div>
            <div class="col-md-2">
                <div class="mb_10 clearfix">
                    <label for="sales_order_no">{{ trans('words.sales_order_no') }}</label>
                    <select class="form-control select2search" id="order_no" name="sales_order_no" required>
                        <option value="">Select Sales Order</option>
                        @foreach($salesOrder as $order)
                        <option value="{{ $order->order_no }}">{{ $order->order_no }}</option>
                        @endforeach
                    </select>
                    <small class="text-danger">{{ $errors->first('from_subhead_id') }}</small>
                </div>
            </div>
            <div class="col-md-2">
                <div class="mb_10 clearfix">
                    <label for="challan_no">{{ trans('words.challan_no') }}</label>
                    <input type="text" class="form-control" id="challan_no" name="challan_no" value="" placeholder="Challan no" readonly>
                </div>
            </div>

            <div class="col-md-2">
                <div class="mb_10 clearfix">
                    <label for="party_name"> {{ trans('words.party_name') }}</label>
                    <select class="form-control select2search" id="party_name" name="party_name">
                        <option value="">Select Party</option>
                    </select>
                    <small class="text-danger">{{ $errors->first('party_name') }}</small>
                </div>
            </div>
            <div class="col-md-2">
                <div class="mb_10 clearfix">
                    <label for="sales_weight">{{ trans('words.sales_weight') }}</label>
                    <input type="number" class="form-control" id="sales_weight" name="sales_weight" value="" placeholder="Sales Weight" required readonly>
                </div>
            </div>
            <div class="col-md-2">
                <div class="mb_10 clearfix">
                    <label for="delivery_weight">{{ trans('words.delivery_weight') }}</label>
                    <input type="number" class="form-control " id="delivery_weight" name="delivery_weight" value="" placeholder="Delivery Weight" min="1" required>
                </div>
            </div>
            <div class="col-md-2">
                <div class="mb_10 clearfix">
                    <label for="remain_weight">{{ trans('words.remain_weight') }}</label>
                    <input type="number" class="form-control" id="remain_weight" name="remain_weight" value="" placeholder="Remain Weight" required>
                </div>
            </div>
            <div class="col-md-2">
                <div class="mb_10 clearfix">
                    <label for="vehicle_no">{{ trans('words.vehicle_no') }}</label>
                    <input type="text" class="form-control" id="vehicle_no" name="vehicle_no" value="" placeholder="Vehicle No">
                </div>
            </div>
            <div class="col-md-2">
                <div class="mb_10 clearfix">
                    <label for="person_name">{{ trans('words.person_name') }}</label>
                    <input type="text" class="form-control" id="person_name" name="person_name" value="" placeholder="Person Name">
                </div>
            </div>
            <div class="col-md-2">
                <div class="mb_10 clearfix">
                    <label for="note">{{ trans('words.note') }}</label>
                    <textarea class="form-control" id="note" name="note" style="min-height: 50px;" placeholder="Note"></textarea>
                </div>
            </div>
        </div>
        <div class="text-center">
            <input type="submit" class="btn btn-primary xsw_33" id="submit_order" name="submit_order" value="{{ trans('words.submit_order') }}">
        </div>
        {!! Form::close() !!}
    </div>
</div>
<div class="panel panel-primary">
    <div class="panel-heading">
        <h3 class="panel-title">Enter Sale Price Information</h3>
    </div>
    <div class="panel-body">
        {!! Form::open(['method' => 'POST', 'url' => '/ricemill/price_setting', 'id' => 'frmPriceSetting']) !!}
        <div class="clearfix" id="product_container"></div>
        {!! Form::close() !!}
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $(document).on("click", "#submit_order", function () {
            hideAjaxMessage();
            var _weight = Number($("#delivery_weight").val());
            if (isNaN(_weight)) {
                _weight = 0;
            }
            if (_weight <= 0) {
                $("#ajaxMessage").showAjaxMessage({html: `<b>Delivery Weight must be grater than Zero</b>`, type: 'error'});
                disable(submit_order);
                return false;
            }
        });

        $(document).on("change", "#order_no ", function (e) {
            var _companyId = $("#company_id").val();
            var url = "{{ URL::to('sales-info') }}";
            $.ajax({
                type: 'POST',
                url: url,
                data: {
                    institute: _companyId,
                    orderNo: this.value,
                    _token: _token
                },
                success: function (response) {
                    $("#party_name").html(response.particulars);
                    $("#challan_no").val(response.challan_no);
                    $("#sales_weight").val(response.sales_weight);
                    $("#sales_id").val(response.sales_id);
                    remainWeight();
                }
            });

        });

        $(document).on("input", "#delivery_weight ", function (e) {
            remainWeight();
        });

        $(document).on("change", "#check_all", function (event) {
            event.preventDefault();
            hideAjaxMessage();
            if (this.checked) {
                $(".items").trigger("change");
            } else {
                $(".items").trigger("change");
            }
        });

        $(document).on("change", ".items", function (event) {
            event.preventDefault();
            hideAjaxMessage();
            var _id = this.value;
            if (this.checked) {
                $("#cost_price_" + _id).removeAttr('readonly').attr('required', 'required');
                $("#sale_price_" + _id).removeAttr('readonly').attr('required', 'required');
                enable($("#size_" + _id)).attr('required', 'required');
            } else {
                $("#cost_price_" + _id).removeAttr('required').attr('readonly', 'readonly').val('');
                $("#sale_price_" + _id).removeAttr('required').attr('readonly', 'readonly').val('');
                disable($("#size_" + _id)).attr('required', 'required');
            }
        });

        $(document).on("submit", "#frmPriceSetting", function (event) {
            event.preventDefault();
            if ($(".items:checked").length < 1) {
                $("#ajaxMessage").showAjaxMessage({html: "Atleast 1 item need to be selected.", type: "error"});
                return false;
            }
            var _form = $(this);
            var _url = _form.attr('action');
            $.post(_url, _form.serialize(), function (res) {
                if (res.success === true) {
                    redirectTo(_url);
                } else {
                    $("#ajaxMessage").showAjaxMessage({html: res.message, type: "error"});
                }
            }, "json");
            return false;
        });

        $(document).on("change", "#order_no", function () {
            hide_ajax_message();
            var _url = "{{ URL::to('/ricemill/search_product') }}";
            var _formData = {};
            _formData._token = csrfToken;
            _formData.company_id = $("#company_id").val();
            _formData.orderNo = $("#order_no").val();
            $.ajax({
                url: _url,
                type: "POST",
                data: _formData,
                success: function (res) {
                    $("#product_container").html(res);
                },
                error: function (xhr, status) {
                    alert('There is some error. Try after some time.');
                }
            });
        });

        function remainWeight() {
            var _salesWeight = Number($("#sales_weight").val());
            var _deliveryWeight = Number($("#delivery_weight").val());
            if (isNaN(_salesWeight)) {
                _salesWeight = 0;
            }
            if (isNaN(_deliveryWeight)) {
                _deliveryWeight = 0;
            }
            var _remainWeight = (_salesWeight - _deliveryWeight).toFixed(2);
            $("#remain_weight").val(_remainWeight);
        }
    });
</script>
@endsection