@if(!empty($searchValues) && count($searchValues) > 0)
<table class="table table-bordered tbl_thin" style="margin: 0">
    <tr class="bg-primary">
        @foreach ($searchValues as $key => $val)
        <td><strong>{{ $key }} : </strong> {{ $val }}</td>
        @endforeach
    </tr>
</table>
@endif
@if (!empty($dataset) && count($dataset) > 0)
<div class="table-responsive">
    <table class="table table-bordered tbl_thin" id="check">
        <tbody>
            <tr class="bg_highlight" id="r_checkAll">
                <th class="text-center" style="width:3%;">#</th>
                <th>Date</th>
                <th>Challan No</th>
                <th>Party</th>
                <th>Products</th>
                <th>Size</th>
                <th class="text-center">Quantity</th>
                <th class="text-center">Net Weight</th>
                <th class="text-center">Purchase Price Unit</th>
                <th class="text-right">Rate</th>
                <th class="text-center">Rate(kg)</th>
                <th class="text-right">Total price</th>
                <th class="text-right hip"> Actions</th>
            </tr>
            <?php
            $counter = 0;
            $total_quantity = 0;
            $total_price = 0;
            ?>
            @foreach ($dataset as $data)
            <?php
            $counter++;
            $total_quantity += $data->quantity;
            $total_price += $data->price;
            ?>   
            <tr onmouseover="change_color(this, true)" onmouseout="change_color(this, false)">
                <td class="text-center" style="width:5%;">{{ $counter }}</td>
                <td class="text-left">{{date_dmy($data->date) }}</td>
                <td class="text-left">{{ $data->challan_no }}</td>
                <td class="text-left">{{!empty( $data->purchase->particular) ? $data->purchase->particular->name_address() : "" }}</td>
                <td class="text-left">{{!empty( $data->product) ? $data->product->name : '' }}</td>
                <td class="text-left">{{!empty( $data->productSize) ? $data->productSize->name : '' }}</td>
                <td class="text-center">{{ $data->quantity }}</td>
                <td class="text-center">{{ $data->net_weight }}</td>
                <td class="text-center">{{ !empty( $data->unit_price) ? $data->unit_price->unit_name : '' }}</td>
                <td class="text-right">{{ $data->rate }}</td>
                <td class="text-center">{{ $data->per_kg_rate }}</td>
                <td class="text-right">{{ $data->price }}</td>
                <td class="text-center hip">
                    @if ($data->process_status == 'Pending')
                    @if (has_user_access('ricemill_purchase_edit_items')) 
                    <a class="btn btn-info btn-xs"  href="{{ url('ricemill/purchase/item_edit/'.$data->id) }}" target="new"><i class="fa fa-edit"></i></a>
                    @endif
                    @endif
                </td>
            </tr>
            @endforeach
            <tr class="bg_gray">
                <th colspan="6" class="text-center" style="background:#ddd; font-weight:600; width:5%;">{{ trans('words.total') }}</th>
                <th class="text-center"> <input type="hidden" id="current_bag_qty" name="current_bag_qty" value="{{ $total_quantity }}"> {{ $total_quantity }}</th>
                <th colspan="4"></th>
                <th class="text-right">{{ $total_price }}</th>
                <th class="text-right hip"></th>
            </tr>
        </tbody>
    </table>
</div>
@else
<div class="alert alert-info">No records found!</div>
@endif