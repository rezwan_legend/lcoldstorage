@extends('layouts.app')
@section('content')
<div id="breadcrumbBar" class="breadcrumb site_nav_links no_bdr_rad clearfix">
    <div class="col-md-2 col-sm-3 col-xs-2 cxs_2 no_pad xsw_50">
        <button class="btn btn-info btn-xs" type="button" onclick="history.back()" title="Go Back"><span class="visible-xs"><i class="fa fa-arrow-left"></i></span><span class="hidden-xs">Back</span></button>
        <button class="btn btn-info btn-xs" onclick="redirectTo('<?= url('purchase') ?>')" title="Refresh" type="button"><span class="visible-xs"><i class="fa fa-refresh"></i></span><span class="hidden-xs">Refresh</span></button>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-6 cxs_10 text-center">
        <h2 class="page-title">{{ trans('words.purchase_list') }}</h2>
    </div>
    <div class="col-md-4 col-sm-3 col-xs-4 cxs_12 no_pad xsw_50">
        <ul class="text-right no_mrgn">
            <li><a href="{{ url('/') }}">Home</a> <span class="fa fa-angle-right"></span></li>
            <li>Purchase</li>
        </ul>
    </div>
</div>

<div class="well">
    <div class="clearfix mb_10">
        <div class="text-center">
            @if (has_user_access('purchase_create')) 
            <a href="{{ url ('purchase/create') }}"><button class="btn btn-success btn-xs"><i class="fa fa-plus"></i> {{ trans('words.new') }}</button></a>
            @endif
            @if (has_user_access('purchase_delete')) 
            <button type="button" class="btn btn-danger btn-xs" id="Del_btn" disabled><i class="fa fa-trash-o"></i> {{ trans('words.delete') }} </button>
            @endif
            <button class="btn btn-primary btn-xs" onclick="printDiv('print_area')"><i class="fa fa-print"></i> {{ trans('words.print') }}</button>
        </div>
    </div>
    <table width="100%">
        <tbody>
            <tr>
                <td class="wmd_70">
                    {!! Form::open(['method' => 'POST',  'class' => 'search-form', 'id' => 'frmSearch', 'name' => 'frmSearch']) !!} 
                    <div class="input-group">
                        <div class="input-group-btn clearfix">
                            <?php echo number_dropdown(50, 550, 50) ?>
                            <div class="col-md-2 col-sm-3 no_pad xsw_50" style="width:11%;">
                                <input type="text" name="from_date" placeholder="(dd-mm-yyyy)" class="form-control pickdate" size="30" readonly>
                            </div>
                            <div style="padding:7px;width:auto;" class="col-md-1 col-sm-1">
                                <span style="font-size:14px;font-weight:600;">To</span>
                            </div> 
                            <div class="col-md-2 col-sm-3 no_pad xsw_50" style="width:11%;">
                                <input type="text" placeholder="(dd-mm-yyyy)" name="end_date" class="form-control pickdate" size="30" readonly>
                            </div>
                            <div class="col-md-2 col-sm-3 no_pad xsw_50">
                                <input type="text" style="" name="search" id="q" class="form-control" placeholder="challan" size="30"/>
                            </div>
                            <div class="col-md-2 col-sm-3 no_pad xsw_50">
                                <input type="text" style="" name="supplier" id="p" class="form-control" placeholder="supplier" size="30"/>
                            </div>
                            <div class="col-md-2 col-sm-3 no_pad xsw_100">
                                <button type="button" id="search" class="btn btn-info xsw_50 xsw_50">{{ trans('words.search') }}</button>
                                <button type="button" id="clear_from" class="btn btn-primary xsw_50 xsw_50">{{ trans('words.clear') }}</button>
                            </div>
                        </div>
                        {!! Form::close() !!}
                </td>

            </tr>
        </tbody>
    </table>
</div>
<div class="clearfix"></div>
<div class="order-list"> 
    {!! Form::open(['method' => 'POST', 'id'=>'frmList','name'=>'frmList']) !!} 
    <div id="print_area">
    <?php print_header("Purchase List", true, false, []); ?>
        <div id="ajax_content">
            <table class="table table-bordered tbl_thin" id="check">
                <tr class="bg_highlight" id="r_checkAll">
                    <th class="text-center" style="width:3%;">#</th>
                    <th>Date</th>
                    <th class="text-left">Challan no</th>
                    <th class="text-left">Supplier</th>
                    <th class="text-center">Items</th>
                    <th class="text-center">Qty</th>
                    <th class="text-center">net_weight</th>
                    <th class="text-right">Price</th>
                    <th class="text-right">Vehicle rent</th>
                    <th class="text-right">Total amount</th>
                    <th class="text-center hip">Actions</th>
                    <th class="text-center hip" style="width:3%;"><input type="checkbox" id="check_all" value="all"></th>
                </tr>
                <?php
                $counter = 0;
                if (isset($_GET['page']) && $_GET['page'] > 1) {
                    $counter = ($_GET['page'] - 1) * $dataset->perPage();
                }
                $total_items = 0;
                $total_price = 0;
                $total_net_weight = 0;
                $total_Qty = 0;
                $_itmes = 0;
                $total_rent = 0;
                $_price = 0;
                $toalAmount = 0;
                ?>
                @foreach ($dataset as $data)
                <?php
                $counter++;
                $total_items += $_itmes = count($data->items);
                $total_price += $_price = $data->total_price($data->id);
                $total_Qty += $_quantity = $data->total_qty($data->id);
                $total_rent += $_rent = $data->vehicle_rent;
                $toalAmount = $_Amount = $_rent + $_price;
                $total_net_weight += $_net_weight = $data->total_net_weight($data->id);
                ?>   
                <tr onmouseover="change_color(this, true)" onmouseout="change_color(this, false)">
                    <td class="text-center">{{ $counter }}</td>
                    <td>{{ date_dmy($data->date) }}</td>
                    <td class="text-left">{{ !empty($data->challan_no) ? $data->challan_no : '' }}</td>
                    <td>{{ !empty($data->particular) ? $data->particular->name_address() : $data->from_subhead->name }}</td>
                    <td class="text-center">{{ $_itmes }}</td>
                    <td class="text-center">{{ $_quantity }}</td>
                    <td class="text-center">{{ $_net_weight }}</td>
                    <td class="text-right">{{ $_price }}</td>
                    <td class="text-right">{{ $_rent }}</td>
                    <td class="text-right">{{ $_Amount }}</td>
                    <td class="text-center hip">

                        @if (has_user_access('ricemill_purchase_items')) 
                        <a class="btn btn-success btn-xs" href="{{ url('/ricemill/purchase/itemlist/'.$data->_key) }}"> {{ trans('words.items') }}</a> 
                        @endif
                        @if($data->process_status == 'Pending')
                        @if (has_user_access('ricemill_purchase_gatepass')) 
                        <a class="btn btn-primary btn-xs" href="purchases/gatepass/{{ $data->id }}"><i class="fa fa-outdent"></i> {{ trans('words.gate_pass') }}</a> 
                        @endif
                        @if (has_user_access('purchase_edit')) 
                        <a class="btn btn-info btn-xs" href="purchase/{{ $data->id }}/edit"><i class="fa fa-edit"></i> {{ trans('words.edit') }}</a>
                        @endif
                        @if (has_user_access('ricemill_purchase_process')) 
                        <a class="btn btn-primary btn-xs" href="purchases/{{ $data->id }}/confirm" onClick="return confirm('Are you sure, Purchase Complete? This cannot be undone!');" ><i class="fa fa-gavel"></i> {{ trans('words.process') }}</a>
                        @endif
                        @endif
                        @if ($data->process_status == 'Complete')
                        @if (has_user_access('ricemill_purchase_details')) 
                        <a class="btn btn-success btn-xs" href="purchases/{{ $data->id }}"><i class="fa fa-plus"></i> {{ trans('words.view') }} </a> 
                        @endif
                        @if (has_user_access('ricemill_purchase_reset')) 
                        <a class="btn btn-danger btn-xs" href="purchases/{{ $data->id }}/reset" onClick="return confirm('Are you sure, Purchase Reset? This cannot be undone!');" ><i class="fa fa-refresh"></i> {{ trans('words.reset') }}</a>
                        @endif
                        @endif
                    </td>
                    <td class="text-center hip">
                        @if (has_user_access('ricemill_purchase_delete')) 
                        <input type="checkbox" name="data[]" value="{{ $data->id }}">
                        @endif
                    </td>
                </tr>
                @endforeach
                <tr class="bg_gray">
                    <th colspan="4" class="text-center" style="background:#ddd; font-weight:600;">{{ trans('words.total') }}</th>
                    <th class="text-center">{{ $total_items }}</th>
                    <th class="text-center">{{ $total_Qty }}</th>
                    <th class="text-center">{{ $total_net_weight }}</th>
                    <th class="text-right">{{ $total_price }}</th>
                    <th class="text-right">{{ $total_rent }}</th>
                    <th class="text-right">{{ $toalAmount }}</th>
                    <th class="hip" colspan="4"></th>
                </tr>
            </table>
            <div class="text-center hip">
                {{ $dataset->render() }}
            </div>
        </div>
    </div>
</div>
{!! Form::close() !!}

<script type="text/javascript">
    $(document).ready(function () {
        $("#search").click(function () {
            if ($("#institute_id").val() == '') {
                $("#ajaxMessage").showAjaxMessage({html: 'Company Required.', type: 'error'});
                $("#institute_id").focus();
                return false;
            }
            hideAjaxMessage();
            var _url = "{{ URL::to('/purchase/search') }}";
            var _form = $("#frmSearch");

            $.ajax({
                url: _url,
                type: "post",
                data: _form.serialize(),
                success: function (data) {
                    $('#ajax_content').html(data);
                    console.log(data);
                },
                error: function (xhr, status) {
                    alert('There is some error.Try after some time.');
                }
            });

        })

        $("#Del_btn").click(function () {
            var _url = "{{ URL::to('/purchase/delete') }}";
            var _form = $("#frmList");
            var _rc = confirm("Are you sure about this action? This cannot be undone!");
            if (_rc == true) {
                $.post(_url, _form.serialize(), function (data) {
                    if (data.success === true) {
                        $('#ajaxMessage').removeClass('alert-danger').addClass('alert-success').show().show();
                        $('#ajaxMessage').html(data.message);
                    } else {
                        $('#ajaxMessage').removeClass('alert-success').addClass('alert-danger').show();
                        $('#ajaxMessage').html(data.message);
                    }
                    $("#search").trigger("click");
                }, "json");

            }

        })

    });
</script>

@endsection