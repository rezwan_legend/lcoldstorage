@extends('layouts.app')
@section('content')

<div id="breadcrumbBar" class="breadcrumb site_nav_links no_bdr_rad clearfix">
    <div class="col-md-3 col-sm-3 col-xs-2 cxs_2 no_pad">
        <button class="btn btn-info btn-xs" type="button" onclick="history.back()" title="Go Back"><span class="visible-xs"><i class="fa fa-arrow-left"></i></span><span class="hidden-xs">Back</span></button>
        <button class="btn btn-info btn-xs" onclick="redirectTo('<?= url('purchase/'.$data->id.'/edit') ?>')" title="Refresh" type="button"><span class="visible-xs"><i class="fa fa-refresh"></i></span><span class="hidden-xs">Refresh</span></button>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-6 cxs_10 text-center">
        <h2 class="page-title">{{ trans('words.update_purchase_order') }}</h2>
    </div>
    <div class="col-md-3 col-sm-3 col-xs-4 cxs_12 no_pad">
        <ul class="text-right no_mrgn">
            <li><a href="{{ url('/purchases') }}">Purchase</a> <span class="fa fa-angle-right"></span></li>
            <li>Form</li>
        </ul>
    </div>
</div>

<div class="panel panel-primary">
    <div class="panel-heading">
        <h3 class="panel-title">{{ trans('words.update_purchase_order_form') }}</h3>
    </div>
    <div class="panel-body">
        {!! Form::open(['method' => 'PUT', 'url' => 'purchase/'.$data->id, 'id' => 'frm_purchase']) !!}
        <div class="row clearfix">
            <div class="col-md-2">
                <div class="mb_10 clearfix">
                    <label for="order_date">{{ trans('words.date') }}</label>
                    <div class="input-group">                
                        <input type="text" class="form-control pickdate" id="order_date" name="order_date" value="{{ date_dmy($data->date) }}" required readonly>
                        <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                    </div>
                    <small class="text-danger">{{ $errors->first('order_date') }}</small>
                </div>
            </div>
            <div class="col-md-2">
                <div class="mb_10 clearfix">
                    <label for="subhead_id">{{ trans('words.supplier') }}</label>
                    <select class="form-control select2search" id="subhead_id" name="from_subhead_id" required>
                        <option value="">Select Supplier</option>
                        @foreach($subheads as $subhead)
                        <option value="{{ $subhead->id }}" @if($subhead->id == $data->subhead_id) {{ 'selected' }} @endif> {{ $subhead->name }}</option>
                        @endforeach
                    </select>
                    <small class="text-danger">{{ $errors->first('from_subhead_id') }}</small>
                </div>
            </div>
            <div class="col-md-2">
                <div class="mb_10 clearfix">
                    <label for="supplier_particular"> {{ trans('words.party_name') }}</label>
                    <select class="form-control select2search" id="supplier_particular" name="supplier_particular">
                        <option value="">Select Supplier First</option>
                        @foreach($particulars as $particular)
                        <option value="{{ $particular->id }}" @if($particular->id == $data->particular_id) {{ 'selected' }} @endif>{{ $particular->name }}</option>
                        @endforeach
                    </select>
                    <small class="text-danger">{{ $errors->first('supplier_particular') }}</small>
                </div>
            </div>
            <div class="col-md-2">
                <div class="mb_10 clearfix">
                    <label for="challan_no">{{ trans('words.challan_no') }}</label>
                    <input type="number" class="form-control" id="challan_no" name="challan_no" value="{{$data->challan_no}}">
                </div>
            </div>
            <div class="col-md-2">
                <div class="mb_10 clearfix">
                    <label for="to_subhead_id">{{ trans('words.purchase_account') }}</label>
                    <select class="form-control select2search" id="to_subhead_id" name="to_subhead_id" required>
                        <option value="">Select Supplier</option>
                        @foreach($subheads as $subhead)
                        <option value="{{ $subhead->id }}" @if($subhead->id == $data->subhead_id) {{ 'selected' }} @endif> {{ $subhead->name }}</option>
                        @endforeach
                    </select>  
                    <small class="text-danger">{{ $errors->first('to_subhead_id') }}</small>
                </div>
            </div>

            <div class="col-md-2">
                <div class="mb_10 clearfix">
                    <label for="challan_weight">{{ trans('words.challan_weight') }}</label>
                    <input type="number" class="form-control" id="challan_weight" name="challan_weight" value="{{$data->challan_weight}}" placeholder="Challan Weight">
                </div>
            </div>
            <div class="col-md-2">
                <div class="mb_10 clearfix">
                    <label for="scale_weight">{{ trans('words.scale_weight') }}</label>
                    <input type="number" class="form-control _net_weight" id="scale_weight" name="scale_weight" value="{{$data->scal_weight}}" placeholder="Scale Weight">
                </div>
            </div>
            <div class="col-md-2">
                <div class="mb_10 clearfix">
                    <label for="bag_quantity">{{ trans('words.bag_quantity') }}</label>
                    <input type="number" class="form-control" id="bag_quantity" name="bag_quantity" placeholder="bag quantity" value="{{$data->quantity}}" placeholder="Bag Quantity">
                </div>
            </div>
            <div class="col-md-2">
                <div class="mb_10 clearfix">
                    <label for="bag_quantity">{{ trans('words.less_weight') }}</label>
                    <input type="number" class="form-control _net_weight" id="less_weight" name="less_weight" placeholder="less weight" value="{{$data->less_weight}}" placeholder="Bag weight">
                </div>
            </div>
            <div class="col-md-2">
                <div class="mb_10 clearfix">
                    <label for="bag_quantity">{{ trans('words.net_weight') }}</label>
                    <input type="number" class="form-control" id="net_weight" name="net_weight" placeholder="net weight" value="{{$data->net_weight}}" readonly placeholder="Net weight">
                </div>
            </div>
            <div class="col-md-2">
                <div class="mb_10 clearfix">
                    <label for="bag_quantity">{{ trans('words.avg_weight') }}</label>
                    <input type="number" class="form-control" id="avg_weight" name="avg_weight" placeholder="avg weight" value="{{$data->avg_weight}}" readonly>
                </div>
            </div>
            <div class="col-md-2">
                <div class="mb_10 clearfix">
                    <label for="vehicle_no">{{ trans('words.vehicle_no') }}</label>
                    <input type="text" class="form-control" id="vehicle_no" name="vehicle_no" placeholder="vehicle no" value="{{$data->vehicle_no}}">
                </div>
            </div>
            <div class="col-md-2">
                <div class="mb_10 clearfix">
                    <label for="vehicle_rent">{{ trans('words.Vehicle rent') }}</label>
                    <input type="number" class="form-control" id="vehicle_rent" name="vehicle_rent" placeholder="vehicle rent" value="{{$data->vehicle_rent}}">
                </div>
            </div>
            <div class="col-md-2">
                <div class="mb_10 clearfix">
                    <label for="person_name">{{ trans('words.reference_name') }}</label>
                    <input type="text" class="form-control" id="person_name" name="person_name" placeholder="reference name" value="{{$data->person_name}}">
                </div>
            </div>
            <div class="col-md-2">
                <div class="mb_10 clearfix">
                    <label for="note">{{ trans('words.note') }}</label>
                    <textarea class="form-control" id="note" placeholder="comments" name="note" style="min-height: 50px;">{{$data->note}}</textarea>
                </div>
            </div> 
        </div>
        <div class="text-center">
            <button type="submit" class="btn btn-primary" id="btnPurchase">{{ trans('words.update') }}</button>
        </div>
        {!! Form::close() !!}
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
    $(document).on("change", "#subhead_id", function () {
    var id = $(this).val();
    $.ajax({
    url: "{{ URL::to('subhead/particular') }}",
            type: "post",
            data: {'head': id, '_token': '{{ csrf_token() }}'},
            success: function (data) {
            enable("#supplier_particular");
            $('#supplier_particular').html(data);
            },
            error: function (xhr, status) {
            alert('There is some error.Try after some time.');
            }
    });
    });
    $(document).on("change", "#supplierParticular", function () {
    var id = $(this).val();
    $.ajax({
    url: "{{ URL::to('ricemill/purchases/challan_no') }}",
            type: "post",
            data: {particularId: id, _token: '{{ csrf_token() }}'},
            success: function (data) {
            $('#challan_no').val(data)
            },
            error: function (xhr, status) {
            alert('There is some error.Try after some time.');
            }
    });
    });
    $(document).on("input", "#scale_weight, #less_weight,#bag_quantity", function (e) {
    var _scale_weight = Number($("#scale_weight").val());
    var less_weight = Number($("#less_weight").val());
    var _bag_qty = Number($("#bag_quantity").val());
    var _net_weight = 0;
    var _avg_weight = 0;
    if (isNaN(_scale_weight)) {
    _scale_weight = 0;
    }
    if (isNaN(less_weight)) {
    bag_weight = 0;
    }
    if (isNaN(_bag_qty)) {
    _bag_qty = 0;
    }

    _net_weight = (parseFloat(_scale_weight) - parseFloat(less_weight)).toFixed(3);
    _avg_weight = (parseFloat(_net_weight) / parseFloat(_bag_qty)).toFixed(3);
    $("#net_weight").val(_net_weight);
    $("#avg_weight").val(_avg_weight);
    e.preventDefault();
    });
    });
</script>
@endsection