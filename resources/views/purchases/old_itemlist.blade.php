@extends('admin.layouts.column2')
@section('content')
<div id="breadcrumbBar" class="breadcrumb site_nav_links no_bdr_rad clearfix">
    <div class="col-md-2 col-sm-3 col-xs-2 cxs_2 no_pad">
        <button class="btn btn-info btn-xs" type="button" onclick="history.back()" title="Go Back"><span class="visible-xs"><i class="fa fa-arrow-left"></i></span><span class="hidden-xs">Back</span></button>
        <button class="btn btn-info btn-xs" onclick="redirectTo('<?= url('refresh_page') ?>')" title="Refresh" type="button"><span class="visible-xs"><i class="fa fa-refresh"></i></span><span class="hidden-xs">Refresh</span></button>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-6 cxs_10 text-center">
        <h2 class="page-title">{{ trans('words.purchase_details') }}</h2>
    </div>
    <div class="col-md-4 col-sm-3 col-xs-4 cxs_12 no_pad">
        <ul class="text-right no_mrgn">
            <li><a href="{{ url('/ricemill') }}">Ricemill</a> <span class="fa fa-angle-right"></span></li>
            <li><a href="{{ url('/ricemill/purchases') }}">Purchase</a> <span class="fa fa-angle-right"></span></li>
            <li>Items</li>
        </ul>
    </div>
</div>

<div class="panel panel-primary">
    <div class="panel-heading">
        <h3 class="panel-title">Purchase order information</h3>
    </div>
    <div class="panel-body">
        <div class="row clearfix">
            <div class="col-md-2 col-sm-3 no_pad xsw_50">
                <div class="mb_10 clearfix">
                    <label for="company_id">{{ trans('words.company') }}</label>
                    <input type="text" class="form-control" id="challan_no" name="challan_no" value="{{ !empty($data->company)? $data->company->name :"" }}" disabled>
                </div>
            </div>

            <div class="col-md-2 col-sm-3 no_pad xsw_50">
                <div class="mb_10 clearfix">
                    <label for="order_date">{{ trans('words.date') }}</label>
                    <div class="input-group">                
                        <input type="text" class="form-control pickdate" id="order_date" name="order_date" value="{{ date_dmy($data->date) }}" disabled>
                        <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                    </div>
                </div>
            </div>
            <div class="col-md-2 col-sm-3 no_pad xsw_50">
                <div class="mb_10 clearfix">
                    <label for="supplier_fatherName">{{ trans('words.challan_no') }}</label>
                    <input type="number" class="form-control" id="challan_no" name="challan_no" value="{{ $data->challan_no }}" disabled>
                </div>
            </div>
            <div class="col-md-2 col-sm-3 no_pad xsw_50">
                <div class="mb_10 clearfix">
                    <label for="order_no">{{ trans('words.order_no') }}</label>
                    <input type="text" class="form-control" id="order_no" name="order_no" value="{{ $data->order_no }}" disabled>
                </div>
            </div> 
            <div class="col-md-2 col-sm-3 no_pad xsw_50">
                <div class="mb_10 clearfix">
                    <label for="bag">{{ trans('words.bag') }}</label>
                    <input type="text" class="form-control" id="bag" name="bag" value="{{ $data->quantity }}" disabled>
                </div>
            </div> 
            <div class="col-md-2 col-sm-3 no_pad xsw_50">
                <div class="mb_10 clearfix">
                    <label for="party">{{ trans('words.party') }}</label>
                    <input type="text" class="form-control" id="party" name="party" value="{{ !empty($data->particular) ? $data->particular->name : $data->from_subhead->name }}" disabled>
                </div>
            </div> 
            <div class="col-md-2 col-sm-3 no_pad xsw_50">
                <div class="mb_10 clearfix">
                    <label for="challan_no">{{ trans('words.challan_weight') }}</label>
                    <input type="text" class="form-control" id="scal_weight" name="challan_no" value="{{ $data->scal_weight }}" disabled>
                </div>
            </div> 
            <div class="col-md-2 col-sm-3 no_pad xsw_50">
                <div class="mb_10 clearfix">
                    <label for="challan_no">{{ trans('words.scale_weight') }}</label>
                    <input type="text" class="form-control" id="challan_no" name="challan_no" value="{{ $data->scal_weight }}" disabled>
                </div>
            </div> 
            <div class="col-md-2 col-sm-3 no_pad xsw_50">
                <div class="mb_10 clearfix">
                    <label for="avg_weight">{{ trans('words.avg_weight') }}</label>
                    <input type="text" class="form-control" id="avg_weight" name="avg_weight" value="{{ $data->avg_weight }}" disabled>
                </div>
            </div> 
            <div class="col-md-2 col-sm-3 no_pad xsw_50">
                <div class="mb_10 clearfix">
                    <label for="less_weight">{{ trans('words.less_weight') }}</label>
                    <input type="text" class="form-control" id="less_weight" name="less_weight" value="{{ $data->less_weight }}" disabled>
                </div>
            </div> 
            <div class="col-md-2 col-sm-3 no_pad xsw_50">
                <div class="mb_10 clearfix">
                    <label for="order_net_weight">{{ trans('words.net_weight') }}</label>
                    <input type="text" class="form-control" id="order_net_weight" name="order_net_weight" value="{{ $data->net_weight }}" disabled>
                </div>
            </div> 
        </div>       
    </div>
</div>
<div class="clearfix"></div>
@if ($data->process_status == 'Pending')
<div class="panel panel-primary">
    <div class="panel-heading">
        <h3 class="panel-title">Please Add Item Information</h3>
    </div>
    <div class="clearfix">
        {!! Form::open(['method' => 'POST', 'url' => 'ricemill/purchases/add_item', 'id' => 'frmOrderItem']) !!}
        <input type="hidden" id="purchases_id" name="purchases_id" value="{{ $data->id }}">     
        <input type="hidden" id="company_id" name="company_id" value="{{ $data->company_id }}">     
        <input type="hidden" id="_avg_weight" name="_avg_weight" value="{{ $data->avg_weight }}">   
        <input type="hidden" id="_vehicle_rent" name="_vehicle_rent" value="{{ $data->vehicle_rent }}">   
        <input type="hidden" id="_net_weight" name="_net_weight" value="{{ $data->net_weight }}">   
        <input type="hidden" id="bag_qty" name="bag_qty" value="{{ $data->quantity }}">   
        <input type="hidden" id="avg_price" name="avg_price" value="">
        <input type="hidden" id="per_kg_rate" name="per_kg_rate" value="">
        <input type="hidden" id="sale_unit_weight" name="sale_unit_weight" value="">
        <div class="mb_10 clearfix">
            <div class="col-md-2" style="">
                <div class="mb_10 clearfix" style="">
                    <label for="product_type"> {{ trans('words.select_type') }}</label>
                    <select class="form-control select2search"  id="product_type" name="product_type" required>
                        <option value="" class="">Product Type</option>
                        @foreach( $product_type as $prod_type )
                        <option value="{{ $prod_type->id }}">{{ $prod_type->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-md-2" style="">
                <div class="mb_10 clearfix" >
                    <label for="category"> {{ trans('words.select_category') }}</label>
                    <select class="form-control select2search"  id="category" name="category" required>
                        <option value="" class="">Category</option>
                    </select>
                </div>
            </div>
            <div class="col-md-2" style="">
                <div class="mb_10 clearfix">
                    <label for="product"> {{ trans('words.select_product') }}</label>
                    <select class="form-control select2search"  id="product_id" name="product_id">
                        <option value=""> Product</option>
                    </select>
                </div>
            </div>
            <div class="col-md-2" style="">
                <div class="mb_10 clearfix">
                    <label for="size">{{ trans('words.select_size') }}</label>
                    <select  class="form-control select2search" id="size_id" name="size_id">
                        <option value="">Size</option>
                    </select> 
                </div>
            </div>
            <div class="col-md-2" style="">
                <div class="mb_10 clearfix">
                    <label for="unit">  {{ trans('words.select_unit') }}</label>
                    <select  class="form-control select2search" id="unit_id" name="unit_id">
                        <option value=""> Unit</option>
                        @foreach( $unitList as $unit )
                        <option value="{{ $unit->id }}">{{ $unit->name }}</option>
                        @endforeach
                    </select> 
                </div>
            </div>
            <div class="col-md-2" style="">
                <div class="mb_10 clearfix">
                    <label for="qty"> {{ trans('words.qty') }}</label>
                    <input type="number" class="form-control text-left qty_rate" id="qty" name="qty" value="" placeholder="Qty" min="0" step="any" required>
                </div>
            </div>
            <div class="col-md-2" style="">
                <div class="mb_10 clearfix">
                    <label for="net_weight"> {{ trans('words.net_weight') }}</label>
                    <input type="number" class="form-control text-left qty_rate" id="net_weight" name="net_weight" value="" placeholder="net weight" min="0" step="any">
                </div>
            </div>

            <div class="col-md-2" style="">
                <div class="mb_10 clearfix">
                    <label for="unit_price">{{ trans('words.Purchase_Price_Unit') }}</label>
                    <select  class="form-control select2search" id="price_unit_id" name="price_unit_id">
                        <option value="">Purchase Unit</option>
                    </select> 
                </div>
            </div>
            <div class="col-md-2" style="">
                <div class="mb_10 clearfix">
                    <label for="price_qty_unit">{{ trans('words.purchase_qty') }}</label>
                    <input type="number" class="form-control text-left qty_rate" id="price_qty_unit" name="price_qty_unit" value="" placeholder="price qty unit" min="0" step="any">
                </div>
            </div> 
            <div class="col-md-2" style="">
                <div class="mb_10 clearfix">
                    <label for="rate">{{ trans('words.unit') }} {{ trans('words.rate') }} </label>
                    <input type="number" class="form-control text-left qty_rate" id="rate" name="rate" value="" placeholder="rate" min="1" step="any">
                </div>
            </div>
            <div class="col-md-2" style="">
                <div class="mb_10 clearfix">
                    <label for="price"> {{ trans('words.total') }} {{ trans('words.price') }}</label>
                    <input type="number" class="form-control text-left qty_rate" id="price" name="price" value="" placeholder="price" min="0" step="any">
                </div>
            </div>
        </div>
        <div class="mb_10 clearfix" >
            <div class="text-center" style="position: relative;width: auto;">
                <button type="submit" class="btn btn-info btn-xs xsw_100" id="addItem" title="Add Item" style=""><i class="fa fa-share"></i> Submit</button>
            </div> 
        </div> 
        {!! Form::close() !!}
    </div>
</div>
@endif

<div class="clearfix">
    <div class="clearfix" id="order_itemlist_partial">
        @if(!empty($itemDataset) && count($itemDataset) > 0)
        <table class="table table-bordered tbl_thin" style="margin: 0">
            <tr class="bg-primary">
                <th class="text-center">{{ trans('words.purchase_details') }}</th>
            </tr>
        </table>
        <div class="table-responsive">
            <table class="table table-bordered tbl_thin" id="check">
                <tr class="bg-info" id="r_checkAll">
                    <th class="text-center" style="width:3%;">#</th>
                    <th>{{ trans('words.product_type') }}</th>
                    <th>{{ trans('words.category') }}</th>
                    <th>{{ trans('words.products') }}</th>
                    <th>{{ trans('words.size') }}</th>
                    <th>{{ trans('words.unit') }}</th>
                    <th>{{ trans('words.quantity') }}</th>
                    <th>{{ trans('words.net_weight') }}</th>
                    <th>{{ trans('words.Purchase_Price_Unit') }}</th>
                    <th>{{ trans('words.purchase_qty') }}</th>
                    <th class="text-center">{{ trans('words.rate') }}</th>
                    <th class="text-center">{{ trans('words.total') }} {{ trans('words.price') }}</th>
                    @if ($data->process_status == 'Pending')
                    <th class="text-center hip" style="width:3%;">Action</th>
                    @endif
                </tr>
                <?php
                $counter = 0;
                $total_quantity = 0;
                $total_price = 0;
                ?>
                @foreach ($itemDataset as $data)
                <?php
                $counter++;
                $total_quantity += $data->quantity;
                $total_price += $data->price;
                ?>   
                <tr onmouseover="change_color(this, true)" onmouseout="change_color(this, false)">
                    <td class="text-center">{{ $counter }}</td>
                    <td class="text-left">{{!empty( $data->product_type) ? $data->product_type->name : '' }}</td>
                    <td class="text-left">{{!empty( $data->category) ? $data->category->name : '' }}</td>
                    <td class="text-left">{{!empty( $data->product) ? $data->product->name : '' }}</td>
                    <td class="text-left">{{!empty( $data->productSize) ? $data->productSize->name : '' }}</td>
                    <td class="text-left">{{!empty( $data->productUnit) ? $data->productUnit->name : '' }}</td>
                    <td class="text-center">{{ $data->quantity }}</td>
                    <td class="text-center">{{ $data->net_weight }}</td>
                    <td class="text-center">{{ !empty($data->unit_price) ? $data->unit_price->unit_name : '' }}</td>
                    <td class="text-left">{{ $data->price_unit_qty  }}</td>
                    <td class="text-right">{{ $data->rate }}</td>
                    <td class="text-right">{{ $data->price }}</td>
                    @if ($data->process_status == 'Pending')
                    <td class="text-center hip">
                        <a class="color_danger" title="Delete Item" href="{{ url('ricemill/purchase/remove_item/'.$data->id) }}" onclick="return confirm('Are you sure about this action?')"><i class="fa fa-trash-o"></i></a>
                    </td>
                    @endif
                </tr>
                @endforeach
                <tr class="bg_gray">
                    <th colspan="6" class="text-center" style="background:#ddd;font-weight:600;">{{ trans('words.total') }}</th>
                    <th class="text-center"> <input type="hidden" id="current_bag_qty" name="current_bag_qty" value="{{ $total_quantity }}"> {{ $total_quantity }}</th>
                    <th class="text-right" colspan="4" ></th>
                    <th class="text-right">{{ $total_price }}</th>
                    @if ($data->process_status == 'Pending')
                    <th class="hip" colspan="2"></th>
                    @endif
                </tr>
            </table>
        </div>
        @else
        <div class="alert alert-info no_mrgn">{{ trans('words.no_items_found') }}</div>
        @endif
    </div>
</div>


<div class="mb_10 text-center" style="margin-top:10px;">   
    @if ($data->process_status == 'Pending')
    <button type="submit" id="saveButton" class="btn btn-success">Save Order</button>
    @else
    <a class="btn btn-primary" href="{{ url('/ricemill/purchases/') }}">Save</a>
    @endif
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $(document).on("change", "#product_type ", function (e) {
            var _productType = $(this.val);
            if (_productType != "") {
                enable(qty);
                enable(rate);
                enable(price);
            }
            var _companyId = $("#company_id").val();
            var url = "{{ URL::to('category/categoryByType') }}";
            $.ajax({
                type: 'POST',
                url: url,
                data: {
                    companyId: _companyId,
                    productTypeId: this.value,
                    _token: _token
                },
                success: function (response) {
                    $("#category").html(response.category);
                    $("#size_id").html(response.sizes);
                    $("#price_unit_id").html(response.price_units);
                }
            });

        });

        $(document).on("click", "#saveButton", function (e) {
            var _purchaseId = $("#purchases_id").val();
            var rent = $("#_vehicle_rent").val();
            var url = "{{ URL::to('ricemill/purchases/save_order') }}";
            var baseurl = "{{ URL::to('ricemill/purchases/') }}";
            $.ajax({
                type: 'POST',
                url: url,
                data: {
                    purchaseId: _purchaseId,
                    vehicleRent: rent,
                    _token: _token
                },
                success: function (response) {
                    if (response.success == true) {
                        top.location.href = baseurl;//redirection
                    }
                },
                error: function (xhr, status) {
                    alert('There is some error.Try after some time.');
                }
            });

        });

        $(document).on("change", "#category ", function (e) {
            var _companyId = $("#company_id").val();
            var _categoryId = $("#category").val();
            var _productType = $("#product_type").val();
            var url = "{{ URL::to('product/prod_by_category') }}";
            $.ajax({
                type: 'POST',
                url: url,
                data: {companyId: _companyId, categoryId: _categoryId, productType: _productType, _token: _token},
                success: function (response) {
                    $("#product_id").html(response);
                }
            });
        });

        $(document).on("change", "#price_unit_id", function (e) {
            $("#sale_unit_weight").val(get_selected_option_info(this, 'data-weight'));
            _price_quantity_unit();
            e.preventDefault();
        });

        $(document).on("submit", "#frmOrderItem", function (event) {
            $('#ajaxMessage').hide();
            var purchases_id = $("#purchases_id").val();
            var _url = baseUrl + "/ricemill/purchases/add_item";
            var _form = $("#frmOrderItem");
            $.post(_url, _form.serialize(), function (res) {
                if (res.success === true) {
                    render_purchase_items(purchases_id);
                } else {
                    $("#ajaxMessage").showAjaxMessage({html: res.message, type: "error"});
                }
            }, "json");
            event.preventDefault();
            return false;
        });

        $(document).on("input", "#rate", function (e) {
            net_price();
            e.preventDefault();
        });

        $(document).on("input", "#qty", function (e) {
//            var _enterBag = Number($("#current_bag_qty").val());
            var _productType = Number($("#product_type").val());
            var _qty = Number($("#qty").val());
            var _bag_qty = Number($("#bag_qty").val());
//            if (isNaN(_enterBag)) {
//                _enterBag = 0;
//            }
            if (isNaN(_qty)) {
                _qty = 0;
            }
            if (isNaN(_bag_qty)) {
                _bag_qty = 0;
            }
//            var _purchaseBag = _enterBag + _qty;
            if (_productType == RICE_RAW || _productType == RICE_FINISH) {
                if (_bag_qty < _qty) {
                    $("#ajaxMessage").showAjaxMessage({html: `Total Bag Must not greater than <b>${_bag_qty}</b>`, type: 'error'});
                    disable(addItem);
                    return false;
                }
                enable(addItem);
                net_weight();
            } else {
                ebag_net_price();
            }


            e.preventDefault();
        });

        $(document).on("input", "#net_weight", function (e) {
            _net_quantity();
            e.preventDefault();
        });

        $(document).on("input", "#price", function (e) {
            find_rate();
            e.preventDefault();
        });





        $(document).on("input", "#price_qty_unit", function (e) {
            net_price();
            e.preventDefault();
        });

    });

    function net_weight() {
        hideAjaxMessage();
        var _qty = Number($("#qty").val());
        var _avg_weight = $("#_avg_weight").val();
        var _net_weight = $("#net_weight").val();
        var _current_bag_qty = $("#current_bag_qty").val();
        var _product_type = $("#product_type").val();
        var _size_weight = $("#size_weight").val();
        var _rate = $("#rate").val();
        const PRODUCT_TYPE = 3;

        if (isNaN(_current_bag_qty)) {
            _current_bag_qty = 0;
        }
        if (isNaN(_rate)) {
            _rate = 0;
        }

        if (isNaN(_qty)) {
            _qty = 0;
        }
        if (isNaN(_avg_weight)) {
            _avg_weight = 0;
        }
        if (isNaN(_net_weight)) {
            _net_weight = 0;
        }

        if (_product_type == RICE_RAW) {
            _net_weight = (parseFloat(_qty) * parseFloat(_avg_weight)).toFixed(3);
            $("#net_weight").val(_net_weight);
            net_price();
        } else if (_product_type == RICE_FINISH) {
            _net_weight = (parseFloat(_qty) * parseFloat(_size_weight)).toFixed(3);
            $("#net_weight").val(_net_weight);
            net_price();
        } else {
            var _price = _qty * _rate;
            $("#price").val(_price);
        }
        _net_quantity();

    }

    function _net_quantity() {
        var _size_weight = $("#size_weight").val();
        var _net_weight = Number($("#net_weight").val());

        var _net_qty = 0;
        if (isNaN(_size_weight)) {
            _size_weight = 0;
        }
        if (isNaN(_net_weight)) {
            _net_weight = 0;
        }
        _net_qty = (parseFloat(_net_weight) / parseFloat(_size_weight)).toFixed(2);
        $("#net_qty").val(_net_qty);
        var _ptype = $("#product_type").val();
        var _qty = Number($("#qty").val());
        if (_ptype == RICE_EBAG) {
            $("#net_qty").val(_qty);
        }
        _price_quantity_unit();
    }

    function _price_quantity_unit() {
        var _unit_value = $("#unit_value").val();
        var _net_weight = $("#net_weight").val();
        var _sale_unit_weight = $("#sale_unit_weight").val();
        var _price_unit_id = $("#price_unit_id").val();
        var _qty = $("#qty").val();
        var _net_qty = $("#net_qty").val();
        if (isNaN(_qty)) {
            _qty = 0;
        }

        if (_price_unit_id == RICE_PICES) {
            $("#price_qty_unit").val(_net_qty);
        } else if (_price_unit_id == RICE_BAG) {
            $("#price_qty_unit").val(_qty);
        } else if (_price_unit_id == RICE_KG) {
            $("#price_qty_unit").val(_net_weight);
        } else if (_price_unit_id == RICE_MON) {
            var _mon = (_net_weight / _sale_unit_weight).toFixed(2);
            $("#price_qty_unit").val(_mon);
        } else if (_price_unit_id == RICE_BANGLA_MON) {
            var _bangla_mon = (_net_weight / _sale_unit_weight).toFixed(2);
            $("#price_qty_unit").val(_bangla_mon);
        } else {
            var _price_qty_unit = (parseFloat(_net_weight) / _unit_value).toFixed(2);
            $("#price_qty_unit").val(_price_qty_unit);
        }
        net_price();
    }

    function net_price() {
        var _rate = Number($("#rate").val());
        var _price_qty_unit = Number($("#price_qty_unit").val());
        var _vehicleRent = Number($("#_vehicle_rent").val());
        var _net_weight = Number($("#net_weight").val());
        var _price = 0;
        if (isNaN(_rate)) {
            _rate = 0;
        }
        if (isNaN(_price_qty_unit)) {
            _price_qty_unit = 0;
        }
        if (isNaN(_vehicleRent)) {
            _vehicleRent = 0;
        }
        if (isNaN(_net_weight)) {
            _net_weight = 0;
        }
        _price = Math.round((_price_qty_unit * _rate).toFixed(3));
        var _per_kg_rate = (_price / _net_weight).toFixed(2);

        $("#price").val(_price);
        $("#per_kg_rate").val(_per_kg_rate);
        avg_price();
    }

    function ebag_net_price() {
        var _rate = Number($("#rate").val());
        var _quantity = Number($("#qty").val());
        var _price = 0;
        if (isNaN(_rate)) {
            _rate = 0;
        }
        if (isNaN(_quantity)) {
            _quantity = 0;
        }
        _price = (_quantity * _rate).toFixed(3);
        console.log(_price);
        $("#price").val(_price);
    }

    function render_purchase_items(purchases_id) {
        var _company_id = $("#company_id").val();
        var _url = baseUrl + "/ricemill/purchases/search_itemlist";
        var _formData = {};
        _formData._token = _token;
        _formData.company_id = _company_id;
        _formData.purchases_id = purchases_id;
        $.ajax({
            url: _url,
            type: "POST",
            data: _formData,
            success: function (res) {
                $("#order_itemlist_partial").html(res);
                $("#qty").val(" ");
                $("#net_weight").val(" ");
                $("#net_qty").val(" ");
                $("#price_unit_id").val(" ");
                $("#price_qty_unit").val(" ");
                $("#rate").val(" ");
                $("#price").val(" ");
            },
            error: function (xhr, status) {
                alert('There is some error.Try after some time.');
            }
        });
    }

    function avg_price() {
        var _qty = Number($("#qty").val());
        var _price = Number($("#price").val());
        var _avg_price = 0;
        if (isNaN(_qty)) {
            _qty = 0;
        }
        if (isNaN(_price)) {
            _price = 0;
        }
        _avg_price = (parseFloat(_price) / parseFloat(_qty)).toFixed(3);
        $("#avg_price").val(_avg_price);
    }

    function find_rate() {
        var _purchaseQty = Number($("#price_qty_unit").val());
        var _price = Number($("#price").val());
        var _rate = 0;
        if (isNaN(_purchaseQty)) {
            _purchaseQty = 0;
        }
        if (isNaN(_price)) {
            _price = 0;
        }
        _rate = (parseFloat(_price) / parseFloat(_purchaseQty)).toFixed(3);
        $("#rate").val(_rate);
    }

</script>
@endsection