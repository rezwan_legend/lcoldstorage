@extends('admin.layouts.column2')
@section('content')
<div id="breadcrumbBar" class="breadcrumb site_nav_links no_bdr_rad clearfix">
    <div class="col-md-2 col-sm-3 col-xs-2 cxs_2 no_pad">
        <button class="btn btn-info btn-xs" type="button" onclick="history.back()" title="Go Back"><span class="visible-xs"><i class="fa fa-arrow-left"></i></span><span class="hidden-xs">Back</span></button>
        <button class="btn btn-info btn-xs" onclick="redirectTo('<?= url('refresh_page') ?>')" title="Refresh" type="button"><span class="visible-xs"><i class="fa fa-refresh"></i></span><span class="hidden-xs">Refresh</span></button>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-6 cxs_10 text-center">
        <h2 class="page-title">{{ trans('words.purchase_details') }}</h2>
    </div>
    <div class="col-md-4 col-sm-3 col-xs-4 cxs_12 no_pad">
        <ul class="text-right no_mrgn">
            <li><a href="{{ url('/home') }}">{{ trans('words.dashboard') }}</a> <span class="divider">/</span></li>
            <li><a href="{{ url('/ricemill/purchases') }}">{{ trans('words.purchase_details') }}</a> <span class="divider">/</span></li>
            <li>{{ trans('words.items') }}</li>
        </ul>
    </div>
</div>
<div class="panel panel-primary">
    <div class="panel-heading">
        <h3 class="panel-title">Purchase information</h3>
    </div>
    <div class="panel-body">
        {!! Form::open(['method' => 'POST', 'url' => 'ricemill/emptybag-purchase', 'id' => 'frm_purchase']) !!}
        <input type="hidden" class="form-control" id="order_no" name="order_no" value="" required readonly>
        <div class="row clearfix">
            <div class="col-md-2">
                <div class="mb_10 clearfix">
                    <label for="company_id">{{ trans('words.party') }}</label>
                       <input type="text" class="form-control" id="challan_no" name="challan_no" value="{{!empty( $data->purchase->from_particular) ? $data->purchase->from_particular->name : $data->purchase->from_subhead->name }}" disabled>
                </div>
            </div>

            <div class="col-md-2">
                <div class="mb_10 clearfix">
                    <label for="order_date">{{ trans('words.date') }}</label>
                    <div class="input-group">                
                        <input type="text" class="form-control pickdate" id="order_date" name="order_date" value="{{ date_dmy($data->date) }}" disabled>
                        <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                    </div>
                </div>
            </div>
            <div class="col-md-2">
                <div class="mb_10 clearfix">
                    <label for="supplier_fatherName">{{ trans('words.challan_no') }}</label>
                    <input type="number" class="form-control" id="challan_no" name="challan_no" value="{{ $data->challan_no }}" disabled>
                </div>
            </div>
        </div>       
        {!! Form::close() !!}
    </div>
</div>
<div class="clearfix" style="border-bottom: 1px dashed #545454;margin: 15px 0;"></div>
<div class="panel panel-primary">
    <div class="panel-heading">
        <h3 class="panel-title">Edit Purchase Details Information</h3>
    </div>
<div class="panel panel-primary" style="border: none; padding-bottom: 0px">
    <div class="panel-body" style="padding-bottom: 0px; ">
        {!! Form::open(['method' => 'POST', 'url' => 'ricemill/purchases/update_item', 'id' => 'frmOrderItem']) !!}
        <input type="hidden" id="item_id" name="item_id" value="{{ $data->id }}">
        <input type="hidden" id="challan_id" name="challan_id" value="{{ $data->challan_no }}">
        <input type="hidden" id="purchases_id" name="purchases_id" value="{{ $data->id }}">     
        <input type="hidden" id="_avg_weight" name="_avg_weight" value="{{ $data->avg_weight }}">   
        <input type="hidden" id="_net_weight" name="_net_weight" value="{{ $data->net_weight }}">   
        <input type="hidden" id="avg_price" name="avg_price" value="">
        <input type="hidden" id="size_weight" name="size_weight" value="">
        <input type="hidden" id="unit_value" name="unit_value" value="">
        <div class="row clearfix" >
            <div class="col-md-2 mb_10 clearfix">
                <label for="product_type"> {{ trans('words.select_type') }}</label>
                <input class="form-control text-left" disabled id="product_type" name="product_type" required disabled value="{{ $data->product_type->name }}">
            </div>
            <div class="col-md-2">
                <div class="mb_10 clearfix">
                    <label for="category"> {{ trans('words.select_category') }}</label>
                    <input class="form-control text-left" id="category" name="category"  required  value="{{ $data->category->name }}" disabled> 
                </div>
            </div>
            <div class="col-md-2">
                <div class="mb_10 clearfix">
                    <label for="product"> {{ trans('words.select_product') }}</label>
                    <input class="form-control text-left" id="product_id" name="product_id"   required  value="{{ $data->product->name }}" disabled>
                </div>
            </div>
            <div class="col-md-2">
                <div class="mb_10 clearfix">
                    <label for="size">{{ trans('words.select_size') }}</label>
                    <input  class="form-control text-left" id="size_id" name="size_id"  required  value="{{ $data->productSize->name }}" disabled>
                </div>
            </div>
            <div class="col-md-2">
                <div class="mb_10 clearfix">
                    <label for="unit">  {{ trans('words.select_unit') }}</label>
                    <input  class="form-control text-left" disabled id="unit_id" name="unit_id"  required  value="{{ $data->productUnit->name }}">
                </div>
            </div>

            <div class="col-md-2">
                <div class="mb_10 clearfix">
                    <label for="qty"> {{ trans('words.select_Qty') }}</label>
                    <input type="number" class="form-control text-left qty_rate" id="qty" name="qty" value="{{ $data->quantity }}" placeholder="Qty" min="0" step="any" disabled>
                </div>
            </div>
            <div class="col-md-2">
                <div class="mb_10 clearfix">
                    <label for="net_weight"> {{ trans('words.net_weight') }}</label>
                    <input type="number" class="form-control text-left qty_rate" id="net_weight" name="net_weight" value="{{ $data->net_weight }}" placeholder="net weight" min="0" step="any" readonly>
                </div>
            </div>
            <div class="col-md-2">
                <div class="mb_10 clearfix">
                    <label for="net_qty">{{ trans('words.net_qty') }}</label>
                    <input type="number" class="form-control text-left qty_rate" id="net_qty" name="net_qty" value="{{ $data->net_qty }}"  placeholder="net qty" min="0" step="any" readonly>
                </div>
            </div>
            <div class="col-md-2">
                <div class="mb_10 clearfix">
                    <label for="unit_price">{{ trans('words.Purchase_Price_Unit') }}</label>
                    <input class="form-control text-left" disabled id="price_unit_id" name="price_unit_id"   required  value="{{ $data->unit_price->unit_name }}">

                </div>
            </div>
            <div class="col-md-2">
                <div class="mb_10 clearfix">
                    <label for="price_qty_unit">{{ trans('words.purchase_qty') }}</label>
                    <input type="number" class="form-control text-left qty_rate" id="price_qty_unit" name="price_qty_unit" value="{{ $data->price_unit_qty }}" placeholder="price qty unit" min="0" readonly step="any">
                </div>
            </div>
            <div class="col-md-2">
                <div class="mb_10 clearfix">
                    <label for="rate">{{ trans('words.unit') }} {{ trans('words.rate') }} </label>
                    <input type="number" class="form-control text-left qty_rate" id="rate" name="rate" value="{{ $data->rate }}" placeholder="rate" min="1" step="any">
                </div>
            </div>
            <div class="col-md-2">
                <div class="mb_10 clearfix">
                    <label for="price"> {{ trans('words.total') }} {{ trans('words.price') }}</label>
                    <input type="number" class="form-control text-left qty_rate" id="price" name="price" value="{{ $data->price }}" placeholder="price" min="0" step="any" readonly>
                </div>
            </div>
        </div>
        <div class="text-center">
            <button type="submit" class="btn btn-primary" id="btnItemUpdate">{{ trans('words.update') }}</button>
        </div>
        {!! Form::close() !!}
    </div>
</div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $(document).on("submit", "#btnItemUpdate", function (event) {
            $('#ajaxMessage').hide();
            var _url = baseUrl + "/ricemill/purchases/update_item";
            var _form = $("#frmOrderItem");
            $.post(_url, _form.serialize(), function (res) {
                $("#ajaxMessage").showAjaxMessage({html: `Item update sucessfully`, type: "sucess"});
            }, "json");
            event.preventDefault();
            return false;
        });
        $(document).on("input", "#rate", function (e) {
            net_price();
            e.preventDefault();
        });
        $(document).on("input", "#qty, #rate", function (e) {
            hideAjaxMessage();
            var _qty = Number($("#qty").val());
            var _bag_qty = $("#bag_qty").val();
            var _avg_weight = $("#_avg_weight").val();
            var _net_weight = $("#net_weight").val();
            var _product_type = $("#product_type").val();
            var _rate = $("#rate").val();
            var _bag_qty = $("#bag_qty").val();
            if (isNaN(_rate)) {
                _rate = 0;
            }
            if (_bag_qty < _qty) {
                $("#ajaxMessage").showAjaxMessage({html: `<b>Total Bag Must not greater than</b>(${_qty})</b>`, type: 'error'});
                disable(addItem);
                return false;
            }
            enable(addItem);
            if (isNaN(_qty)) {
                _qty = 0;
            }
            if (isNaN(_avg_weight)) {
                _avg_weight = 0;
            }
            if (isNaN(_net_weight)) {
                _net_weight = 0;
            }
            if (_product_type == 1 || _product_type == 2) {
                _net_weight = (parseFloat(_qty) * parseFloat(_avg_weight)).toFixed(3);
                $("#net_weight").val(_net_weight);
                net_price();
            } else {
                var _price = _qty * _rate;
                $("#price").val(_price);
            }
            e.preventDefault();
        });

    });
    function _net_quantity() {
        var _size_weight = $("#size_weight").val();
        var _net_weight = Number($("#net_weight").val());
        var _net_qty = 0;
        if (isNaN(_size_weight)) {
            _size_weight = 0;
        }
        if (isNaN(_net_weight)) {
            _net_weight = 0;
        }
        _net_qty = (parseFloat(_net_weight) / parseFloat(_size_weight)).toFixed(2);
        $("#net_qty").val(_net_qty);
    }
    function net_price() {
        var _rate = Number($("#rate").val());
        var _price_qty_unit = $("#price_qty_unit").val();
        var _price = 0;
        if (isNaN(_rate)) {
            _rate = 0;
        }
        if (isNaN(_price_qty_unit)) {
            _price_qty_unit = 0;
        }
        _price = (parseFloat(_rate) * parseFloat(_price_qty_unit)).toFixed(3);
        $("#price").val(_price);
        avg_price();
    }
    function render_purchase_items(purchases_id) {
        var _url = baseUrl + "/ricemill/purchases/search_itemlist";
        var _formData = {};
        _formData._token = _token;
        _formData.purchases_id = purchases_id;
        $.ajax({
            url: _url,
            type: "POST",
            data: _formData,
            success: function (res) {
                $("#order_itemlist_partial").html(res);
                $("#qty").val(" ");
                $("#net_weight").val(" ");
                $("#net_qty").val(" ");
                $("#price_unit_id").val(" ");
                $("#price_qty_unit").val(" ");
                $("#rate").val(" ");
                $("#price").val(" ");
            },
            error: function (xhr, status) {
                alert('There is some error.Try after some time.');
            }
        });
    }
    function CategoryByType(prod_type) {
        if (prod_type) {
            var url = "{{ URL::to('product/categoryBy_type') }}";
            $.ajax({
                type: 'POST',
                url: url,
                data: {
                    type: prod_type,
                    _token: _token
                },
                success: function (response) {
                    $("#category").html(response.category);
                    $("#size_id").html(response.sizes);
                }
            });
        }
    }
    function productByCategory(category) {
        if (category) {
            var url = "{{ URL::to('main/rawsale/prod_by_category') }}";
            $.ajax({
                type: 'POST',
                url: url,
                data: {
                    category_id: category,
                    _token: _token
                },
                success: function (response) {
                    $("#product_id").html(response);
                }
            });
        }
    }
    function weightBysizeName(size_id) {
        if (size_id) {
            var url = "{{ URL::to('ricemill/purchases/weightBysize') }}";
            $.ajax({
                type: 'POST',
                url: url,
                data: {
                    id: size_id,
                    _token: _token
                },
                success: function (response) {
                    $("#size_weight").val(response.weight);
                    _net_quantity();
                }
            });
        }
    }
    function avg_price() {
        var _qty = Number($("#qty").val());
        var _price = Number($("#price").val());
        var _avg_price = 0;
        if (isNaN(_qty)) {
            _qty = 0;
        }
        if (isNaN(_price)) {
            _price = 0;
        }
        _avg_price = (parseFloat(_price) / parseFloat(_qty)).toFixed(3);
        $("#avg_price").val(_avg_price);
    }
    function _price_quantity_unit() {
        var _unit_value = $("#unit_value").val();
        var _net_weight = $("#net_weight").val();
        var _price_unit_id = $("#price_unit_id").val();
        var _qty = $("#net_qty").val();
        if (isNaN(_qty)) {
            _qty = 0;
        }

        if (_price_unit_id == 3 || _price_unit_id == 4) {
            $("#price_qty_unit").val(_qty);
        } else {
            var _price_qty_unit = (parseFloat(_net_weight) / _unit_value).toFixed(2);
            $("#price_qty_unit").val(_price_qty_unit);
        }
        net_price();
    }
    function unitValueByUnit(price_unit_id) {
        if (price_unit_id) {
            var url = "{{ URL::to('main/rawsale/unit_value') }}";
            $.ajax({
                type: 'POST',
                url: url,
                data: {
                    unit_id: price_unit_id,
                    _token: _token
                },
                success: function (response) {
                    $("#unit_value").val(response.unitValue);
                    _price_quantity_unit();
                }
            });
        }
    }

</script>
@endsection