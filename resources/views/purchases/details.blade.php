@extends('layouts.app')
@section('content')
<div id="breadcrumbBar" class="breadcrumb site_nav_links no_bdr_rad clearfix">
    <div class="col-md-3 col-sm-3 col-xs-2 cxs_2 no_pad">
        <button class="btn btn-info btn-xs" type="button" onclick="history.back()" title="Go Back"><span class="visible-xs"><i class="fa fa-arrow-left"></i></span><span class="hidden-xs">Back</span></button>
        <button class="btn btn-info btn-xs" onclick="redirectTo('<?= url('refresh_page') ?>')" title="Refresh" type="button"><span class="visible-xs"><i class="fa fa-refresh"></i></span><span class="hidden-xs">Refresh</span></button>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-6 cxs_10 text-center">
        <h2 class="page-title">Purchase Details</h2>
    </div>
    <div class="col-md-3 col-sm-3 col-xs-4 cxs_12 no_pad">
        <ul class="text-right no_mrgn">
            <li><a href="{{ url('/home') }}"><i class="fa fa-fw fa-dashboard"></i> Dashboard</a> <span class="divider">/</span></li>
            <li><a href="{{ url('/ricemill/purchases') }}"><span> Details</span></a></li>
        </ul>
    </div>
</div>

<div class="invoice_area" id="print_area">
    <?php print_header("Purchase Challan", true, true, [], $data->company_id); ?>

    <div class="panel panel-info">
        <div class="panel-heading">
            <h3 class="panel-title">Purchase Challan Information</h3>
        </div>
        <div class="panel-body no_pad">
            <table class="table table-bordered tbl_thin" style="margin:0;">
                <tr>
                    <th style="width:auto;">Invoice Date</th>
                    <td style="width:auto;">{{ date_dmy($data->date) }}</td>
                    <th style="width:auto;">Challan No</th>
                    <td style="width:auto;">{{ $data->challan_no }}</td>
                    <th style="width:auto;">Challan Date</th>
                    <td style="width:auto;">{{ date_dmy($data->date) }}</td>
                    <th style="width:auto;">Truck No</th>
                    <td style="width:auto;">{{ $data->vehicle_no }}</td>
                </tr>
                <tr>
                    <th style="width:auto;">Supplier Name</th>
                    <td style="width:auto;">{{ !empty($data->particular) ? $data->particular->name : $data->from_subhead->name }}</td>
                    <th style="width:auto;">Mobile</th>
                    <td style="width:auto;">{{ !empty($data->particular) ? $data->particular->mobile : '' }}</td>
                    <th colspan="2" style="width:auto;">Address</th>
                    <td colspan="2" style="width:auto;">{{ !empty($data->particular) ? $data->particular->address : '' }}</td>
                </tr>
            </table>
        </div>
    </div>

    <div class="panel panel-info">
        <div class="panel-heading">
            <h3 class="panel-title">Product&apos;s Information</h3>
        </div>
        <div class="panel-body no_pad">
            <div class="table-responsive">
                <table class="table table-bordered tbl_thin no_mrgn" id="check">
                    <tr class="bg-warning" id="r_checkAll">
                        <th class="text-center" style="width:3%;">#</th>
                        <th class="text-left" style="width:20%;">Product</th>
                        <th class="text-left" style="width:8%;">Size</th>
                        <th class="text-left" style="width:8%;">Unit</th>
                        <th class="text-left" style="width:5%;">Qty</th>
                        <th class="text-left" style="width:8%;">Net Weight</th>
                        <th class="text-left" style="width:8%;">Price Unit</th>
                        <th class="text-center" style="width:10%;">Price Unit Qty</th>
                        <th class="text-right" style="width:10%;">Price</th>
                        <th class="text-right" style="width:10%;">Total Price</th>
                    </tr>
                    <?php
                    $counter = 0;
                    $total_quantity = 0;
                    $total_price = 0;
                    $total_netWeight = 0;
                    $total_rate = 0;
                    ?>
                    @foreach ($itemDataset as $data)
                    <?php
                    $counter++;
                    $total_quantity += $data->quantity;
                    $total_netWeight += $data->net_weight;
                    $total_rate += $data->rate;
                    $total_price += $data->price;
                    ?>   
                    <tr onmouseover="change_color(this, true)" onmouseout="change_color(this, false)">
                        <td class="text-center">{{ $counter }}</td>
                        <td class="text-left" style="width:20%;">{{!empty( $data->product) ? $data->product->name : '' }}</td>
                        <td class="text-left" style="width:8%;">{{!empty( $data->productSize) ? $data->productSize->name : '' }}</td>
                        <td class="text-left" style="width:8%;">{{!empty( $data->productUnit) ? $data->productUnit->name : '' }}</td>
                        <td class="text-center" style="width:5%;">{{ $data->quantity }}</td>
                        <td class="text-center" style="width:8%;">{{ $data->net_weight }}</td>
                        <td class="text-center" style="width:8%;">{{ !empty($data->unit_price) ? $data->unit_price->unit_name : '' }}</td>
                        <td class="text-center" style="width:10%;">{{ $data->price_unit_qty  }}</td>
                        <td class="text-right" style="width:10%;">{{ $data->rate }}</td>
                        <td class="text-right" style="width:10%;">{{ $data->price }}</td>
                    </tr>
                    @endforeach
                    <tr class="bg-warning">
                        <th class="text-right" colspan="4">{{ trans('words.total') }}</th>
                        <th class="text-center">{{ $total_quantity }}</th>
                        <th class="text-center">{{ $total_netWeight }}</th>
                        <th colspan="3"></th>
                        <th class="text-right">{{ $total_price }}</th>
                    </tr>
                </table>
            </div>
        </div>
    </div>

    <div class="row clearfix" style="padding-top:30px;">
        <div class="col-md-4 form-group mp_30">
            <div style="border-top:1px solid #000000;text-align:center;">Customer</div>
        </div>
        <div class="col-md-4 form-group mp_30">
            <div style="border-top:1px solid #000000;text-align:center;">Manager</div>
        </div>
        <div class="col-md-4 form-group mp_30">
            <div style="border-top:1px solid #000000;text-align:center;">Managing Director</div>
        </div>
    </div>
</div>

<div class="mb_10 text-center">
    <button class="btn btn-primary" onclick="printDiv('print_area')"><i class="fa fa-print"></i> Print</button>
</div>
@endsection