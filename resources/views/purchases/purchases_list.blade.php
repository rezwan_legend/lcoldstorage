@extends('admin.layouts.column2')
@section('content')
<div id="breadcrumbBar" class="breadcrumb site_nav_links no_bdr_rad clearfix">
    <div class="col-md-3 col-sm-3 col-xs-2 cxs_2 no_pad">
        <button class="btn btn-info btn-xs" type="button" onclick="history.back()" title="Go Back"><span class="visible-xs"><i class="fa fa-arrow-left"></i></span><span class="hidden-xs">Back</span></button>
        <button class="btn btn-info btn-xs" onclick="redirectTo('{{url('view - clear')}}')" title="Refresh" type="button"><span class="visible-xs"><i class="fa fa-refresh"></i></span><span class="hidden-xs">Refresh</span></button>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-6 cxs_10 text-center">
        <h2 class="page-title">{{ trans('words.Purchases_Product_List') }}</h2>
    </div>
    <div class="col-md-3 col-sm-3 col-xs-4 cxs_12 no_pad">
        <ul class="text-right no_mrgn">
            <li><a href="{{ url('/home') }}"><i class="fa fa-fw fa-dashboard"></i> {{ trans('words.dashboard') }}</a> <span class="divider">/</span></li>
            <li><span> {{ trans('words.purchase') }}</span></li>
        </ul>                            
    </div>
</div>
<div class="well">
    <div class="mb_10 clearfix">
        <div class="text-center">
            @if (has_user_access('ricemill_purchase_create')) 
            <a href="{{ url ('ricemill/purchases/create') }}"><button class="btn btn-success btn-xs"><i class="fa fa-plus"></i> {{ trans('words.new') }}</button></a>
            @endif
            <button class="btn btn-primary btn-xs" onclick="printDiv('print_area')"><i class="fa fa-print"></i> {{ trans('words.print') }} </button>
        </div>
    </div>
    <table width="100%">
        <tbody>
            <tr>
                <td class="wmd_100">
                    {!! Form::open(['method' => 'POST',  'class' => 'search-form', 'id' => 'frmSearch', 'name' => 'frmSearch']) !!} 
                    <div class="input-group">
                        <div class="input-group-btn clearfix">
                            <select id="itemCount" class="form-control" name="item_count" style="width:45px;">
                                <?php
                                for ($i = 50; $i <= 100; $i += 10):
                                    echo "<option value='{$i}'>{$i}</option>";
                                endfor;
                                ?>
                            </select>
                            <div class="col-md-1 col-sm-3 no_pad xsw_50" style="width:10%">
                                <input type="text" name="from_date" placeholder="(dd-mm-yyyy)" class="form-control pickdate" size="30" readonly>
                            </div> 
                            <div style="width:auto;padding: 7px;" class="col-md-1 col-sm-1 hidden-xs">
                                <span style="font-size:14px;font-weight:600;">{{ trans('words.to') }}</span>
                            </div>
                            <div class="col-md-1 col-sm-3 no_pad xsw_50" style="width:10%">
                                <input type="text" placeholder="(dd-mm-yyyy)" name="end_date" class="form-control pickdate" size="30" readonly>
                            </div> 
                            <div class="col-md-2 col-sm-3 no_pad xsw_50" style="width:9%">
                                <select class="form-control select2search" id="business_id" name="business" required>
                                    @foreach( $businesslist as $business )
                                    <option value="{{ $business->id }}">{{ $business->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-md-2 col-sm-3 no_pad xsw_50" style="width:9%">
                                <select class="form-control select2search" id="product_type" name="product_type" required>
                                    <option value="" class="">Product Type</option>
                                    @foreach( $product_type as $prod_type )
                                    <option value="{{ $prod_type->id }}">{{ $prod_type->name }}</option>
                                    @endforeach
                                </select>  
                            </div>
                            <div class="col-md-1 col-sm-3 no_pad xsw_50" style="width:9%">
                                <select class="form-control select2search" id="category" name="category" required>
                                    <option value="" class="">Category</option>
                                </select> 
                            </div>
                            <div class="col-md-1 col-sm-3 no_pad xsw_50" style="width:9%">
                                <select class="form-control select2search"  id="product_id" name="product_id">
                                    <option value=""> Product</option>
                                </select>
                            </div>
                            <div class="col-md-1 col-sm-3 no_pad xsw_50" style="width:9%">
                                <select class="form-control select2search" id="size_id" name="size_id" required>
                                    <option value="" class="">Size</option>
                                </select> 
                            </div>

                            <div class="col-md-1 col-sm-3 no_pad xsw_50" style="width:10%">
                                <input type="text" name="srcParty" id="srcParty" class="form-control" size="30" placeholder="Party search">
                            </div>
                            <div class="col-md-2 col-sm-3 no_pad xsw_100">
                                <button type="button" id="search" class="btn btn-info xsw_50">{{ trans('words.search') }}</button>
                                <button type="button" id="clear_from" class="btn btn-primary xsw_50">{{ trans('words.clear') }}</button>
                            </div>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        </tbody>
    </table>
</div>
<div class="clearfix"></div>
<div class="order-list"> 
    {!! Form::open(['method' => 'POST', 'id'=>'frmList','name'=>'frmList']) !!} 
    <div id="print_area">
        <?php print_header("Pruchase List", true, false, [], institute_id()); ?>
        <div id="ajax_content">
            <div class="table-responsive">
                <table class="table table-bordered tbl_thin" id="check">
                    <tbody>
                        <tr class="bg_highlight" id="r_checkAll">
                            <th class="text-center" style="width:3%;">#</th>
                            <th>Date</th>
                            <th>Challan No</th>
                            <th>Party</th>
                            <th>Products</th>
                            <th>Size</th>
                            <th class="text-center">Quantity</th>
                            <th class="text-center">Net Weight</th>
                            <th class="text-center">Purchase Price Unit</th>
                            <th class="text-right">Rate</th>
                            <th class="text-center">Rate(kg)</th>
                            <th class="text-right">Total price</th>
                            <th class="text-right hip"> Actions</th>
                        </tr>
                        <?php
                        $counter = 0;
                        $total_quantity = 0;
                        $total_price = 0;
                        ?>
                        @foreach ($dataset as $data)
                        <?php
                        $counter++;
                        $total_quantity += $data->quantity;
                        $total_price += $data->price;
                        ?>   
                        <tr onmouseover="change_color(this, true)" onmouseout="change_color(this, false)">
                            <td class="text-center" style="width:5%;">{{ $counter }}</td>
                            <td class="text-left">{{date_dmy($data->date) }}</td>
                            <td class="text-left">{{ $data->challan_no }}</td>
                            <td class="text-left">{{!empty( $data->purchase->particular) ? $data->purchase->particular->name_address() : "" }}</td>
                            <td class="text-left">{{!empty( $data->product) ? $data->product->name : '' }}</td>
                            <td class="text-left">{{!empty( $data->productSize) ? $data->productSize->name : '' }}</td>
                            <td class="text-center">{{ $data->quantity }}</td>
                            <td class="text-center">{{ $data->net_weight }}</td>
                            <td class="text-center">{{ !empty( $data->unit_price) ? $data->unit_price->unit_name : '' }}</td>
                            <td class="text-right">{{ $data->rate }}</td>
                            <td class="text-center">{{ $data->per_kg_rate }}</td>
                            <td class="text-right">{{ $data->price }}</td>
                            <td class="text-center hip">
                                @if ($data->process_status == 'Pending')
                                @if (has_user_access('ricemill_purchase_edit_items')) 
                                <a class="btn btn-info btn-xs"  href="{{ url('ricemill/purchase/item_edit/'.$data->id) }}" target="new"><i class="fa fa-edit"></i></a>
                                @endif
                                @endif
                            </td>
                        </tr>
                        @endforeach
                        <tr class="bg_gray">
                            <th colspan="6" class="text-center" style="background:#ddd; font-weight:600; width:5%;">{{ trans('words.total') }}</th>
                            <th class="text-center"> <input type="hidden" id="current_bag_qty" name="current_bag_qty" value="{{ $total_quantity }}"> {{ $total_quantity }}</th>
                            <th colspan="4"></th>
                            <th class="text-right">{{ $total_price }}</th>
                            <th class="text-right hip"></th>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
{!! Form::close() !!}
<script type="text/javascript">
    $(document).ready(function () {
    $("#search").click(function () {
    var _url = "{{ URL::to('ricemill/purchase/item/search') }}";
    var _form = $("#frmSearch");
    $.ajax({
    url: _url,
            type: "post",
            data: _form.serialize(),
            success: function (data) {
            $('#ajax_content').html(data);
            },
            error: function () {
            $('#ajaxMessage').showAjaxMessage({html: 'There is some error.Try after some time.', type: 'error'});
            }
    });
    });
    $(document).on("change", "#business_id", function (e) {
    enable(product_type);
    });
    $(document).on("change", "#product_type ", function (e) {
    var _productType = $(this.val);
    var _companyId = $("#business_id").val();
    var url = "{{ URL::to('category/categoryByType') }}";
    $.ajax({
    type: 'POST',
            url: url,
            data: {
            companyId: _companyId,
                    productTypeId: this.value,
                    _token: _token
            },
            success: function (response) {
            $("#category").html(response.category);
            $("#size_id").html(response.sizes);
            $("#price_unit_id").html(response.price_units);
            }
    });
    });
    $(document).on("change", "#category ", function (e) {
    var _companyId = $("#business_id").val();
    var _categoryId = $("#category").val();
    var _productType = $("#product_type").val();
    var url = "{{ URL::to('product/prod_by_category') }}";
    $.ajax({
    type: 'POST',
            url: url,
            data: {companyId: _companyId, categoryId: _categoryId, productType: _productType, _token: _token},
            success: function (response) {
            $("#product_id").html(response);
            }
    });
    });
    });
</script>

@endsection