@extends('layouts.app')
@section('content')
<div id="breadcrumbBar" class="breadcrumb site_nav_links no_bdr_rad clearfix">
    <div class="col-md-2 col-sm-3 col-xs-2 cxs_2 no_pad">
        <button class="btn btn-info btn-xs" type="button" onclick="history.back()" title="Go Back"><span class="visible-xs"><i class="fa fa-arrow-left"></i></span><span class="hidden-xs">Back</span></button>
        <button class="btn btn-info btn-xs" onclick="redirectTo('<?= url('purchase/create') ?>')" title="Refresh" type="button"><span class="visible-xs"><i class="fa fa-refresh"></i></span><span class="hidden-xs">Refresh</span></button>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-6 cxs_10 text-center">
        <h2 class="page-title">{{ trans('words.purchase_order') }}</h2>
    </div>
    <div class="col-md-4 col-sm-3 col-xs-4 cxs_12 no_pad">
        <ul class="text-right no_mrgn">
            <li><a href="{{ url('/purchase') }}">Purchase</a> <span class="fa fa-angle-right"></span></li>
            <li>Form</li>
        </ul>
    </div>
</div>

<div class="panel panel-primary">
    <div class="panel-heading">
        <div class="pull-right">
            <a href="{{ url ('/particulars/create') }}" target="new"><button class="btn btn-success btn-xs"><i class="fa fa-plus"></i> new party</button></a>
        </div>
        <h3 class="panel-title">{{ trans('words.enter_purchase_order_information') }}</h3>
    </div>
    <div class="panel-body">
        {!! Form::open(['method' => 'POST', 'url' => 'purchase', 'id' => 'frm_purchase']) !!}
        <input type="hidden" class="form-control" id="order_no" name="order_no" value="" required readonly>
        <div class="row clearfix">
            <div class="col-md-2" >
                <div class="mb_10 clearfix" >
                    <label for="order_date">{{ trans('words.date') }}</label>
                    <div class="input-group">                
                        <input type="text" class="form-control pickdate" id="order_date" name="order_date" value="<?php echo date('d-m-Y'); ?>" required readonly>
                        <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                    </div>
                    <small class="text-danger">{{ $errors->first('order_date') }}</small>
                </div>
            </div>
            <div class="col-md-2">
                <div class="mb_10 clearfix">
                    <label for="order_no">{{ trans('words.order_no') }}</label>
                    <input type="text" class="form-control" id="order_no" name="order_no" value="{{ $order_no }}" placeholder="Order No" readonly>
                </div>
            </div>
            <div class="col-md-2">
                <div class="mb_10 clearfix">
                    <label for="subhead_id">{{ trans('words.supplier') }}</label>
                    <select class="form-control select2search" id="subhead_id" name="subhead_id" required>
                        <option value="">Select Account</option>
                        @foreach($subheads as $subhead)
                        <option value="{{ $subhead->id }}">{{ $subhead->name }}</option>
                        @endforeach
                    </select>  
                    <small class="text-danger">{{ $errors->first('subhead_id') }}</small>
                </div>
            </div>
            <div class="col-md-2">
                <div class="mb_10 clearfix">
                    <label for="supplierParticular"> {{ trans('words.party_name') }}</label>
                    <select class="form-control select2search" id="supplierParticular" name="supplier_particular" disabled>
                        <option value="">Select Party</option>
                    </select>
                    <small class="text-danger">{{ $errors->first('supplier_particular') }}</small>
                </div>
            </div>
            <div class="col-md-2">
                <div class="mb_10 clearfix">
                    <label for="challan_no">{{ trans('words.challan_no') }}</label>
                    <input type="text" class="form-control" id="challan_no" name="challan_no" value="" placeholder="Challan no" required>
                </div>
            </div>
            <div class="col-md-2">
                <div class="mb_10 clearfix">
                    <label for="to_subhead_id">{{ trans('words.purchase_account') }}</label>
                    <select class="form-control select2search" id="to_subhead_id" name="to_subhead_id" required>
                        <option value="">Select Account</option>
                        @foreach($subheads as $subhead)
                        <option value="{{ $subhead->id }}">{{ $subhead->name }}</option>
                        @endforeach
                    </select>  
                    <small class="text-danger">{{ $errors->first('supplier_subhead') }}</small>
                </div>
            </div>
            <div class="col-md-2">
                <div class="mb_10 clearfix">
                    <label for="challan_weight">{{ trans('words.challan_weight') }}</label>
                    <input type="number" class="form-control" id="challan_weight" name="challan_weight" value="" placeholder="Challan Weight" required>
                </div>
            </div>
            <div class="col-md-2">
                <div class="mb_10 clearfix">
                    <label for="scale_weight">{{ trans('words.scale_weight') }}</label>
                    <input type="number" class="form-control _net_weight" id="scale_weight" name="scale_weight" value="" placeholder="Scale Weight" required>
                </div>
            </div>
            <div class="col-md-2">
                <div class="mb_10 clearfix">
                    <label for="bag_quantity">{{ trans('words.bag_quantity') }}</label>
                    <input type="number" class="form-control" id="bag_quantity" name="bag_quantity" value="" placeholder="Bag Quantity" required>
                </div>
            </div>
            <div class="col-md-2">
                <div class="mb_10 clearfix">
                    <label for="bag_quantity">{{ trans('words.less_weight') }}</label>
                    <input type="number" class="form-control _net_weight" id="less_weight" name="less_weight" value="" placeholder="Bag weight">
                </div>
            </div>
            <div class="col-md-2">
                <div class="mb_10 clearfix">
                    <label for="bag_quantity">{{ trans('words.net_weight') }}</label>
                    <input type="number" class="form-control" id="net_weight" name="net_weight" value="" readonly placeholder="Net weight">
                </div>
            </div>
            <div class="col-md-2">
                <div class="mb_10 clearfix">
                    <label for="bag_quantity">{{ trans('words.avg_weight') }}</label>
                    <input type="number" class="form-control" id="avg_weight" name="avg_weight" value="" readonly placeholder="Avg weight">
                </div>
            </div>
            <div class="col-md-2">
                <div class="mb_10 clearfix">
                    <label for="vehicle_no">{{ trans('words.vehicle_no') }}</label>
                    <input type="text" class="form-control" id="vehicle_no" name="vehicle_no" value="" placeholder="Vehicle No">
                </div>
            </div>
            <div class="col-md-2">
                <div class="mb_10 clearfix">
                    <label for="vehicle_rent">{{ trans('words.Vehicle rent') }}</label>
                    <input type="number" class="form-control" id="vehicle_rent" name="vehicle_rent" value="" placeholder="Vehicle Rent">
                </div>
            </div>
            <div class="col-md-2">
                <div class="mb_10 clearfix">
                    <label for="person_name">{{ trans('words.reference_name') }}</label>
                    <input type="text" class="form-control" id="person_name" name="person_name" value="" placeholder="Person Name">
                </div>
            </div>
            <div class="col-md-2">
                <div class="mb_10 clearfix">
                    <label for="note">{{ trans('words.note') }}</label>
                    <textarea class="form-control" id="note" name="note" style="min-height: 50px;" placeholder="Note"></textarea>
                </div>
            </div> 
        </div>

        <div class="text-center">
            <input type="submit" class="btn btn-primary xsw_33" id="submit_order" name="submit_order" value="{{ trans('words.submit_order') }}">
        </div>
        {!! Form::close() !!}
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $(document).on("click", "#submit_order", function () {
            hideAjaxMessage();
            var _bag_qty = Number($("#bag_quantity").val());
            if (isNaN(_bag_qty)) {
                _bag_qty = 0;
            }
            if (_bag_qty <= 0) {
                $("#ajaxMessage").showAjaxMessage({html: `<b>Bag Quantity Must be greater than Zero</b>`, type: 'error'});
                disable(submit_order);
                return false;
            }
        });

        $(document).on("change", "#subhead_id", function () {
            var id = $(this).val();
            $.ajax({
                url: "{{ URL::to('subhead/particular') }}",
                type: "post",
                data: {'head': id, '_token': '{{ csrf_token() }}'},
                success: function (data) {
                    enable("#supplierParticular");
                    $('#supplierParticular').html(data);
                },
                error: function (xhr, status) {
                    alert('There is some error.Try after some time.');
                }
            });
        });

        $(document).on("change", "#to_subhead_id", function () {
            var id = $(this).val();
            $.ajax({
                url: "{{ URL::to('subhead/particular') }}",
                type: "post",
                data: {'head': id, '_token': '{{ csrf_token() }}'},
                success: function (data) {
                    enable("#particular")
                    $('#particular').html(data)
                },
                error: function (xhr, status) {
                    alert('There is some error.Try after some time.');
                }
            });
        });

        $(document).on("change", "#supplierParticular", function () {
            var id = $(this).val();
            $.ajax({
                url: "{{ URL::to('purchases/challan_no') }}",
                type: "post",
                data: {particularId: id, _token: '{{ csrf_token() }}'},
                success: function (data) {
                    $('#challan_no').val(data)
                },
                error: function (xhr, status) {
                    alert('There is some error.Try after some time.');
                }
            });
        });

        $(document).on("input", "#scale_weight, #less_weight, #bag_quantity", function (e) {
            hideAjaxMessage();
            enable(submit_order);
            var _scale_weight = Number($("#scale_weight").val());
            var less_weight = Number($("#less_weight").val());
            var _bag_qty = Number($("#bag_quantity").val());
            var _net_weight = 0;
            var _avg_weight = 0;
            if (isNaN(_scale_weight)) {
                _scale_weight = 0;
            }
            if (isNaN(less_weight)) {
                bag_weight = 0;
            }
            if (isNaN(_bag_qty)) {
                _bag_qty = 0;
            }

            _net_weight = (parseFloat(_scale_weight) - parseFloat(less_weight)).toFixed(3);
            _avg_weight = (parseFloat(_net_weight) / parseFloat(_bag_qty)).toFixed(3);
            $("#net_weight").val(_net_weight);
            $("#avg_weight").val(_avg_weight);
            e.preventDefault();
        });
    });
    function headsubheadByCompany(institute_id) {
        if (institute_id) {
            var url = "{{ URL::to('subhead/ledger/subhead') }}";
            $.ajax({
                type: 'POST',
                url: url,
                data: {
                    institute_id: institute_id,
                    _token: _token
                },
                success: function (response) {
                    $("#subhead_id").html(response.str);
                    $("#to_subhead_id").html(response.str);
                    $("#order_no").val(response.order_no);
                }
            });
        }
    }
</script>
@endsection