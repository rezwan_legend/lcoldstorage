@if (!empty($dataset) && count($dataset) > 0) 
<table class="table table-bordered tbl_thin" id="check">
    <tr class="bg_highlight" id="r_checkAll">
        <th class="text-center" style="width:3%;">#</th>
        <th>Date</th>
        <th class="text-left">Challan no</th>
        <th class="text-left">Supplier</th>
        <th class="text-center">Items</th>
        <th class="text-center">Qty</th>
        <th class="text-center">net_weight</th>
        <th class="text-right">Price</th>
        <th class="text-right">Vehicle rent</th>
        <th class="text-right">Total amount</th>
        <th class="text-center hip">Actions</th>
        <th class="text-center hip" style="width:3%;"><input type="checkbox" id="check_all" value="all"></th>
    </tr>
    <?php
    $counter = 0;
    if (isset($_GET['page']) && $_GET['page'] > 1) {
        $counter = ($_GET['page'] - 1) * $dataset->perPage();
    }
    $total_items = 0;
    $total_price = 0;
    $total_net_weight = 0;
    $total_Qty = 0;
    $_itmes = 0;
    $total_rent = 0;
    $_price = 0;
    $toalAmount = 0;
    ?>
    @foreach ($dataset as $data)
    <?php
    $counter++;
    $total_items += $_itmes = count($data->items);
    $total_price += $_price = $data->total_price($data->id);
    $total_Qty += $_quantity = $data->total_qty($data->id);
    $total_rent += $_rent = $data->vehicle_rent;
    $toalAmount = $_Amount = $_rent + $_price;
    $total_net_weight += $_net_weight = $data->total_net_weight($data->id);
    ?>   
    <tr onmouseover="change_color(this, true)" onmouseout="change_color(this, false)">
        <td class="text-center">{{ $counter }}</td>
        <td>{{ date_dmy($data->date) }}</td>
        <td class="text-left">{{ !empty($data->challan_no) ? $data->challan_no : '' }}</td>
        <td>{{ !empty($data->particular) ? $data->particular->name_address() : $data->from_subhead->name }}</td>
        <td class="text-center">{{ $_itmes }}</td>
        <td class="text-center">{{ $_quantity }}</td>
        <td class="text-center">{{ $_net_weight }}</td>
        <td class="text-right">{{ $_price }}</td>
        <td class="text-right">{{ $_rent }}</td>
        <td class="text-right">{{ $_Amount }}</td>
        <td class="text-center hip">

            @if (has_user_access('ricemill_purchase_items')) 
            <a class="btn btn-success btn-xs" href="{{ url('/ricemill/purchase/itemlist/'.$data->_key) }}"> {{ trans('words.items') }}</a> 
            @endif
            @if($data->process_status == 'Pending')
            @if (has_user_access('ricemill_purchase_gatepass')) 
            <a class="btn btn-primary btn-xs" href="purchases/gatepass/{{ $data->id }}"><i class="fa fa-outdent"></i> {{ trans('words.gate_pass') }}</a> 
            @endif
            @if (has_user_access('ricemill_purchase_edit')) 
            <a class="btn btn-info btn-xs" href="purchases/{{ $data->id }}/edit"><i class="fa fa-edit"></i> {{ trans('words.edit') }}</a>
            @endif
            @if (has_user_access('ricemill_purchase_process')) 
            <a class="btn btn-primary btn-xs" href="purchases/{{ $data->id }}/confirm" onClick="return confirm('Are you sure, Purchase Complete? This cannot be undone!');" ><i class="fa fa-gavel"></i> {{ trans('words.process') }}</a>
            @endif
            @endif
            @if ($data->process_status == 'Complete')
            @if (has_user_access('ricemill_purchase_details')) 
            <a class="btn btn-success btn-xs" href="purchases/{{ $data->id }}"><i class="fa fa-plus"></i> {{ trans('words.view') }} </a> 
            @endif
            @if (has_user_access('ricemill_purchase_reset')) 
            <a class="btn btn-danger btn-xs" href="purchases/{{ $data->id }}/reset" onClick="return confirm('Are you sure, Purchase Reset? This cannot be undone!');" ><i class="fa fa-refresh"></i> {{ trans('words.reset') }}</a>
            @endif
            @endif
        </td>
        <td class="text-center hip">
            @if (has_user_access('ricemill_purchase_delete')) 
            <input type="checkbox" name="data[]" value="{{ $data->id }}">
            @endif
        </td>
    </tr>
    @endforeach
    <tr class="bg_gray">
        <th colspan="4" class="text-center" style="background:#ddd; font-weight:600;">{{ trans('words.total') }}</th>
        <th class="text-center">{{ $total_items }}</th>
        <th class="text-center">{{ $total_Qty }}</th>
        <th class="text-center">{{ $total_net_weight }}</th>
        <th class="text-right">{{ $total_price }}</th>
        <th class="text-right">{{ $total_rent }}</th>
        <th class="text-right">{{ $toalAmount }}</th>
        <th class="hip" colspan="4"></th>
    </tr>
</table>
<div class="text-center hip" id="apaginate">
    {{ $dataset->render() }}
</div>
@else
<div class="alert alert-info">No records found!</div>
@endif