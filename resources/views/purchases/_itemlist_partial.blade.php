@if (!empty($itemDataset) && count($itemDataset) > 0)
<div class="table-responsive">
    <table class="table table-bordered tbl_thin">
        <tr class="bg-info" id="r_checkAll">
            <th>Product Type</th>
            <th>Category</th>
            <th>Products</th>
            <th>Size</th>
            <th>Unit</th>
            <th>Quantity</th>
            <th>Net Weight</th>
            <th>Purchase Price Unit</th>
            <th>Purchase Qty</th>
            <th class="text-center">Rate</th>
            <th class="text-center">Total price</th>
            <th class="text-center hip" style="width:3%;">Action</th>
        </tr>
        <?php
        $counter = 0;
        $totalQty = 0;
        $totalPrice = 0;
        $total_quantity = 0;
        ?>
        @foreach($itemDataset as $data)
        <?php
        $counter++;
        $totalQty += $_qty = $data->quantity;
        $_rate = $data->rate;
        $totalPrice += $_amount = $data->price;
        ?>
        <tr onmouseover="change_color(this, true)" onmouseout="change_color(this, false)">
            <td class="text-center">{{ $counter }}</td>
            <td class="text-left">{{!empty( $data->product_type) ? $data->product_type->name : '' }}</td>
            <td class="text-left">{{!empty( $data->category) ? $data->category->name : '' }}</td>
            <td class="text-left">{{!empty( $data->product) ? $data->product->name : '' }}</td>
            <td class="text-left">{{!empty( $data->productSize) ? $data->productSize->name : '' }}</td>
            <td class="text-left">{{!empty( $data->productUnit) ? $data->productUnit->name : '' }}</td>
            <td class="text-center">{{ $data->quantity }}</td>
            <td class="text-center">{{ $data->net_weight }}</td>
            <td class="text-center">{{ !empty($data->unit_price) ? $data->unit_price->unit_name : '' }}</td>
            <td class="text-left">{{ $data->price_unit_qty  }}</td>
            <td class="text-right">{{ $data->rate }}</td>
            <td class="text-right">{{ $data->price }}</td>
            <td class="text-center hip">
                <a class="color_danger" title="Delete Item" href="{{ url('ricemill/purchase/remove_item/'.$data->id) }}" onclick="return confirm('Are you sure about this action?')"><i class="fa fa-trash-o"></i></a>
            </td>
        </tr>
        @endforeach
        <tr class="bg-info">
            <th colspan="6" class="text-center" style="background:#ddd;font-weight:600;">{{ trans('words.total') }}</th>
            <th class="text-center">{{ $totalQty }}</th>
            <th class="text-right" colspan="4" ></th>
            <th class="text-right">{{ $totalPrice }}<input type="hidden" id="sub_total" value="{{$totalPrice}}"></th>
            <th class="text-center hip"></th>
        </tr>
    </table>
</div>
@else
<div class="alert alert-info">No records found!</div>
@endif

