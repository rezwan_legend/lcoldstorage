@extends('admin.layouts.column2')
@section('content')
<div id="breadcrumbBar" class="breadcrumb site_nav_links no_bdr_rad clearfix">
    <div class="col-md-3 col-sm-3 col-xs-2 cxs_2 no_pad">
        <button class="btn btn-info btn-xs" type="button" onclick="history.back()" title="Go Back"><span class="visible-xs"><i class="fa fa-arrow-left"></i></span><span class="hidden-xs">Back</span></button>
        <button class="btn btn-info btn-xs" onclick="redirectTo('<?= url('refresh_page') ?>')" title="Refresh" type="button"><span class="visible-xs"><i class="fa fa-refresh"></i></span><span class="hidden-xs">Refresh</span></button>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-6 cxs_10 text-center">
        <h2 class="page-title">Gate Pass</h2>
    </div>
    <div class="col-md-3 col-sm-3 col-xs-4 cxs_12 no_pad">
        <ul class="text-right no_mrgn">
            <li><a href="{{ url('/ricemill') }}">Ricemill</a> <span class="fa fa-angle-right"></span></li>
            <li><a href="{{ url('/ricemill/purchases') }}">Purchase</a> <span class="fa fa-angle-right"></span></li>
            <li>Gate Pass</li>
        </ul>
    </div>
</div>

<div class="invoice_area" id="print_area">
    <div class="clearfix">
        <?php print_header("Gate Pass (Office Copy)", true, true, [], $data->company_id); ?>
        <table class="table table-bordered tbl_thin" style="margin:0;">
            <tr>
                <td style="width:20%"><strong>Date:</strong> <?= ( Auth::user()->locale == 'bn' ) ? en2bnDate(date_dmy($data->date)) : date_dmy($data->date); ?> </td>
                <td style="width:20%"><strong>Challan No:</strong> {{ $data->challan_no}} </td>
                <td style="width:20%"><strong>Supplier :</strong> {{ !empty($data->particular) ? $data->particular->name : $data->from_head->name }}</td>
                <td style="width:20%"><strong>Vehicle No :</strong> {{ $data->vehicle_no }}</td>
                <td style="width:20%"><strong>Comments :</strong> {{ $data->note }}</td>
            </tr>
        </table>
    </div>
    <div class="product_info">
        <div class="table-responsive">
            <table class="table table-bordered tbl_thin" id="check" style="margin-bottom:15px;">
                <tr class="bg_gray" id="r_checkAll">
                    <th class="text-center" style="width:3%;">#</th>
                    <th class="text-left" style="width:20%;">Product</th>
                    <th class="text-left" style="width:8%;">Size</th>
                    <th class="text-left" style="width:8%;">Unit</th>
                    <th class="text-center" style="width:5%;">Qty</th>
                    <th class="text-center" style="width:8%;">Net Weight</th>
                    <th class="text-left" style="width:8%;">Price Unit</th>
                    <th class="text-center" style="width:10%;">Price Unit Qty</th>
                </tr>
                <?php
                $counter = 0;
                $total_quantity = 0;
                $total_price = 0;
                $total_net_weight = 0;
                ?>
                @foreach ($itemDataset as $_item)
                <?php
                $counter++;
                $total_quantity += $_item->quantity;
                $total_net_weight += $_item->net_weight;
                $total_price += $_item->price;
                ?>   
                <tr onmouseover="change_color(this, true)" onmouseout="change_color(this, false)">
                    <td class="text-center">{{ $counter }}</td>
                    <td class="text-left" style="width:20%;">{{!empty($_item->product) ? $_item->product->name : '' }}</td>
                    <td class="text-left" style="width:8%;">{{!empty($_item->productSize) ? $_item->productSize->name : '' }}</td>
                    <td class="text-left" style="width:8%;">{{!empty($_item->productUnit) ? $_item->productUnit->name : '' }}</td>
                    <td class="text-center" style="width:5%;">{{ $_item->quantity }}</td>
                    <td class="text-center" style="width:8%;">{{ $_item->net_weight }}</td>
                    <td class="text-left" style="width:8%;">{{ !empty($_item->unit_price) ? $_item->unit_price->unit_name : '' }}</td>
                    <td class="text-center" style="width:10%;">{{ $_item->price_unit_qty  }}</td>
                </tr>
                @endforeach
                <tr class="bg_gray">
                    <th colspan="4" class="text-center" style="background:#ddd; font-weight:600;">{{ trans('words.total') }}</th>
                    <th class="text-center"> {{ $total_quantity }}</th>
                    <th class="text-center"> {{ $total_net_weight }}</th>
                    <th  colspan="2"></th>
                </tr>
            </table>
        </div>
        <div class="row clearfix" style="padding-top:30px;">
            <div class="col-md-4 form-group mp_30">
                <div style="border-top:1px solid #000000;text-align:center;">Customer</div>
            </div>
            <div class="col-md-4 form-group mp_30">
                <div style="border-top:1px solid #000000;text-align:center;">Manager</div>
            </div>
            <div class="col-md-4 form-group mp_30">
                <div style="border-top:1px solid #000000;text-align:center;">Managing Director</div>
            </div>
        </div>
    </div>

    <div class="clearfix" style="padding-top: 30px;border-top: 1px solid #ddd;margin-top: 30px;">
        <?php print_header("Gate Pass (Client Copy)", true, true, [], $data->company_id); ?>
        <table class="table table-bordered tbl_thin" style="margin:0;">
            <tr>
                <td style="width:20%"><strong>Date:</strong> <?= ( Auth::user()->locale == 'bn' ) ? en2bnDate(date_dmy($data->date)) : date_dmy($data->date); ?> </td>
                <td style="width:20%"><strong>Challan No:</strong> {{ $data->challan_no}} </td>
                <td style="width:20%"><strong>Supplier :</strong> {{ !empty($data->particular) ? $data->particular->name : $data->from_head->name }}</td>
                <td style="width:20%"><strong>Vehicle No :</strong> {{ $data->vehicle_no }}</td>
                <td style="width:20%"><strong>Comments :</strong> {{ $data->note }}</td>
            </tr>
        </table>
        <div class="product_info">
            <div class="table-responsive">
                <table class="table table-bordered tbl_thin" id="check" style="margin-bottom:15px;">
                    <tr class="bg_gray" id="r_checkAll">
                        <th class="text-center" style="width:3%;">#</th>
                        <th class="text-left" style="width:20%;">Product</th>
                        <th class="text-left" style="width:8%;">Size</th>
                        <th class="text-left" style="width:8%;">Unit</th>
                        <th class="text-center" style="width:5%;">Qty</th>
                        <th class="text-center" style="width:8%;">Net Weight</th>
                        <th class="text-left" style="width:8%;">Price Unit</th>
                        <th class="text-center" style="width:10%;">Price Unit Qty</th>
                    </tr>
                    <?php
                    $counter2 = 0;
                    $total_quantity2 = 0;
                    $total_price2 = 0;
                    $total_netWeight2 = 0;
                    ?>
                    @foreach ($itemDataset as $_item)
                    <?php
                    $counter++;
                    $total_quantity2 += $_item->quantity;
                    $total_netWeight2 += $_item->net_weight;
                    $total_price2 += $_item->price;
                    ?> 
                    <tr onmouseover="change_color(this, true)" onmouseout="change_color(this, false)">
                        <td class="text-center">{{ $counter }}</td>
                        <td class="text-left" style="width:20%;">{{!empty($_item->product) ? $_item->product->name : '' }}</td>
                        <td class="text-left" style="width:8%;">{{!empty($_item->productSize) ? $_item->productSize->name : '' }}</td>
                        <td class="text-left" style="width:8%;">{{!empty($_item->productUnit) ? $_item->productUnit->name : '' }}</td>
                        <td class="text-center" style="width:5%;">{{ $_item->quantity }}</td>
                        <td class="text-center" style="width:8%;">{{ $_item->net_weight }}</td>
                        <td class="text-left" style="width:8%;">{{ !empty($_item->unit_price) ? $_item->unit_price->unit_name : '' }}</td>
                        <td class="text-center" style="width:10%;">{{ $_item->price_unit_qty  }}</td>
                    </tr>
                    @endforeach
                    <tr class="bg_gray">
                        <th colspan="4" class="text-center" style="background:#ddd; font-weight:600;">{{ trans('words.total') }}</th>
                        <th class="text-center"> {{ $total_quantity2 }}</th>
                        <th class="text-center"> {{ $total_netWeight2 }}</th>
                        <th  colspan="2"></th>
                    </tr>
                </table>
            </div>
            <div class="row clearfix" style="padding-top:30px;">
                <div class="col-md-4 form-group mp_30">
                    <div style="border-top:1px solid #000000;text-align:center;">Customer</div>
                </div>
                <div class="col-md-4 form-group mp_30">
                    <div style="border-top:1px solid #000000;text-align:center;">Manager</div>
                </div>
                <div class="col-md-4 form-group mp_30">
                    <div style="border-top:1px solid #000000;text-align:center;">Managing Director</div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="mb_10 text-center">
    <button class="btn btn-primary" onclick="printDiv('print_area')"><i class="fa fa-print"></i> Print</button>
</div>
@endsection