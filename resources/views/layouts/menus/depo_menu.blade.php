<ul class="nav navbar-nav side-nav">
    @if(isLegend())
    <li>
        <a href="javascript:;" data-toggle="collapse" data-target="#dropdown_legend">Legend <i class="fa fa-fw fa-caret-down"></i></a>
        <ul id="dropdown_legend" class="collapse {{ request()->is('service*') ? 'in' : '' }}">
            <li class="{{ request()->is('service') ? 'active' : '' }}">
                <a href="{{ url('/service') }}">Service</a>
            </li>
            <li class="{{ request()->is('service/maintenance') ? 'active' : '' }}">
                <a href="{{ url('/service/maintenance') }}">Maintenance</a>
            </li>
        </ul>
    </li>
    @endif

    <li class="{{ request()->is('home') ? ' active' : '' }}">
        <a href="{{ url('/home') }}">{{ trans('words.dashboard') }}</a>
    </li>

    @if (has_user_access('module_permission_account')) 
    <li>
        <a href="javascript:;" data-toggle="collapse" data-target="#dropdown_accounts">{{ trans('words.accounts') }} <i class="fa fa-fw fa-caret-down"></i></a>
        <ul id="dropdown_accounts" class="collapse">
            @if (has_user_access('particular_list')) 
            <li><a href="{{ url('/particular') }}">{{ trans('words.particular') }}</a></li>
            @endif
            @if (has_user_access('particular_ledger')) 
            <li><a href="{{ url('/transactions') }}">{{ trans('words.all_transactions') }}</a></li>
            @endif
        </ul>
    </li>
    @endif

    @if (has_user_access('module_permission_depo'))
    <li>
        <a href="javascript:;" data-toggle="collapse" data-target="#dropdown_depo"><i class="fa fa-archive"></i> {{ trans('words.depo') }} <i class="fa fa-fw fa-caret-down"></i></a>
        <ul id="dropdown_depo" class="collapse {{ (request()->is('depo/sale*') || request()->is('depo/delivery*')) ? 'in' : '' }}">
            @if(has_user_access('depo_stocks_view'))
            <li class="{{ request()->is('sale_person_stocks') ? 'active' : '' }}">
                <a href="{{ url('/sale_person_stocks') }}">{{ trans('words.stocks') }}</a>
            </li>
            <li class="{{ request()->is('sale_person_stocks/details') ? 'active' : '' }}">
                <a href="{{ url('/sale_person_stocks/details') }}">{{ trans('words.stock_register') }}</a>
            </li>
            @endif
            @if(has_user_access('depo_sales_order_list'))
            <li class="{{ request()->is('depo/sale_orders') ? 'active' : '' }}">
                <a href="{{ url('/depo/sale_orders') }}">{{ trans('words.sale_order_list') }}</a>
            </li>
            @endif
            @if(has_user_access('depo_delivery_list'))
            <li class="{{ request()->is('depo/delivery') ? 'active' : '' }}">
                <a href="{{ url('/depo/delivery') }}">{{ trans('words.delivery_list') }}</a>
            </li>
            @endif
            @if(has_user_access('depo_delivery_items'))
            <li class="{{ request()->is('depo/delivery/items') ? 'active' : '' }}">
                <a href="{{ url('/depo/delivery/items') }}">{{ trans('words.delivery_item_list') }}</a>
            </li>
            @endif
            @if(has_user_access('depo_delivery_ledger'))
            <li class="{{ request()->is('depo/delivery/ledger') ? 'active' : '' }}">
                <a href="{{ url('/depo/delivery/ledger') }}">{{ trans('words.delivery_ledger') }}</a>
            </li>
            @endif
        </ul>
    </li>
    @endif

    @if (has_user_access('module_permission_distribution'))
    <li>
        <a href="javascript:;" data-toggle="collapse" data-target="#dropdown_report">{{ trans('words.reporting') }} <i class="fa fa-fw fa-caret-down"></i></a>
        <ul id="dropdown_report" class="collapse {{ (request()->is('report/monthly*') || request()->is('ledger/product')) ? 'in' : '' }}">
            @if(has_user_access('monthly_sales_report'))
            <li class="{{ request()->is('report/monthly/sales') ? 'active' : '' }}">
                <a href="{{ url('/report/monthly/sales') }}"> {{ trans('words.monthly_sale_report') }}</a>
            </li>
            @endif
            @if(has_user_access('monthly_commission_report'))
            <li class="{{ request()->is('report/monthly/commission') ? 'active' : '' }}">
                <a href="{{ url('/report/monthly/commission') }}"> {{ trans('words.monthly_commission_report') }}</a>
            </li>
            @endif
            @if(has_user_access('product_sales_report'))
            <li class="{{ request()->is('report/product') ? 'active' : '' }}">
                <a href="{{ url('/report/product/') }}"> {{ trans('words.product_report') }}</a>
            </li>
            @endif
        </ul>
    </li>
    @endif
</ul>
