<ul class="nav navbar-nav side-nav">
    @if(isLegend())
    <li>
        <a href="javascript:;" data-toggle="collapse" data-target="#dropdown_setting">Setting <i class="fa fa-fw fa-caret-down"></i></a>
        <ul id="dropdown_setting" class="collapse {{ request()->is('service*') ? 'in' : '' }}">
            <li class="{{ request()->is('service') ? 'active' : '' }}">
                <a href="javascript:;" data-toggle="collapse" data-target="#dropdown_module">Product Module<i class="fa fa-fw fa-caret-down"></i></a>
                <ul id="dropdown_module" class="changestyle collapse {{ request()->is('service*') ? 'in' : '' }}">
                    @if (has_user_access('product_type'))
                    <li class="{{ request()->is('product_type*') ? 'active' : '' }}">
                        <a href="{{ url('/product_type') }}">{{ trans('words.product_type') }}</a>
                    </li>
                    @endif
                    @if (has_user_access('category'))
                    <li class="{{ request()->is('category*') ? 'active' : '' }}">
                        <a href="{{ url('/category') }}">Category</a>
                    </li>
                    @endif
                    @if (has_user_access('products'))
                    <li class="{{ request()->is('products*') ? 'active' : '' }}">
                        <a href="{{ url('/products') }}">Product</a>
                    </li>
                    @endif
                    @if (has_user_access('unit'))
                    <li class="{{ request()->is('unit*') ? 'active' : '' }}">
                        <a href="{{ url('/unit') }}">Unit</a>
                    </li>
                    @endif
                    <li class="{{ request()->is('unit_size*') ? 'active' : '' }}">
                        <a href="{{ url('/unit_size') }}">Unit Size</a>
                    </li>
                </ul>
            </li>
            <li class="{{ request()->is('service/maintenance') ? 'active' : '' }}">
                <a href="javascript:;" data-toggle="collapse" data-target="#dropdown_rent" href="">Rent Setting<i class="fa fa-fw fa-caret-down"></i></a>
                <ul id="dropdown_rent" class="changestyle collapse {{ request()->is('service*') ? 'in' : '' }}">
                    @if (has_user_access('rent_type'))
                    <li class=""><a href="{{ url('/rent_type') }}">{{ trans('words.rent_type') }}</a></li>
                    @endif
                    @if (has_user_access('rent'))
                    <li class=""><a href="{{ url('/rent') }}">{{ trans('words.rent') }}</a></li>
                    @endif
                </ul>
            </li>
            <li class="{{ request()->is('service/maintenance') ? 'active' : '' }}">
                <a href="javascript:;" data-toggle="collapse" data-target="#dropdown_party" href="">Party<i class="fa fa-fw fa-caret-down"></i></a>
                <ul id="dropdown_party" class="changestyle collapse {{ request()->is('service*') ? 'in' : '' }}">
                    <li class=""><a href="{{ url('/party_type') }}">Party Type Setting</a></li>
                    <li class=""><a href="{{ url('/party') }}">Party Info</a></li>
                </ul>
            </li>
            <li class="{{ request()->is('service/maintenance') ? 'active' : '' }}">
                <a href="javascript:;" data-toggle="collapse" data-target="#dropdown_party_commision" href="">Party Commision<i class="fa fa-fw fa-caret-down"></i></a>
                <ul id="dropdown_party_commision" class="changestyle collapse {{ request()->is('service*') ? 'in' : '' }}">
                    <li class=""><a href="{{ url('/party_commission') }}">Party Commission</a></li>
                    <li class=""><a href="{{ url('/party_commission_multi') }}">Party Commission Multi</a></li>
                    <li class=""><a href="{{ url('/party_rent') }}">Party Rent</a></li>
                    <li class=""><a href="{{ url('/party_loan') }}">Party Loan</a></li>
                </ul>
            </li>
            <li class="{{ request()->is('service/maintenance') ? 'active' : '' }}">
                <a href="javascript:;" data-toggle="collapse" data-target="#dropdown_location_setting" href="">Location Setting<i class="fa fa-fw fa-caret-down"></i></a>
                <ul id="dropdown_location_setting" class="changestyle collapse {{ request()->is('service*') ? 'in' : '' }}">
                    <li class=""><a href="{{ url('/chamber_setting') }}">Chamber Setting</a></li>
                    <li class=""><a href="{{ url('/floor_setting') }}">Floor Setting</a></li>
                    <li class=""><a href="{{ url('/pocket_setting') }}">Pocket Setting</a></li>
                </ul>
            </li>
            <li class="{{ request()->is('service/maintenance') ? 'active' : '' }}">
                <a href="javascript:;" data-toggle="collapse" data-target="#dropdown_store_setting" href="">Store Setting<i class="fa fa-fw fa-caret-down"></i></a>
                <ul id="dropdown_store_setting" class="changestyle collapse {{ request()->is('service*') ? 'in' : '' }}">
                    <li class=""><a href="{{ url('/store_condition') }}">Store Condition</a></li>
                    <li class=""><a href="{{ url('/loan_condition') }}">Loan Condition</a></li>
                </ul>
            </li>
            <li class="{{ request()->is('service/maintenance') ? 'active' : '' }}">
                <a href="javascript:;" data-toggle="collapse" data-target="#dropdown_delivery_setting" href="">Delivery Setting<i class="fa fa-fw fa-caret-down"></i></a>
                <ul id="dropdown_delivery_setting" class="changestyle collapse {{ request()->is('service*') ? 'in' : '' }}">
                    <li class=""><a href="{{ url('/delivery_condition') }}">Delivery Condition</a></li>
                </ul>
            </li>
            <li class="{{ request()->is('service/maintenance') ? 'active' : '' }}">
                <a href="javascript:;" data-toggle="collapse" data-target="#dropdown_account_setting" href="">Account Setting<i class="fa fa-fw fa-caret-down"></i></a>
                <ul id="dropdown_account_setting" class="changestyle collapse {{ request()->is('service*') ? 'in' : '' }}">
                    <li class=""><a href="{{ url('/head') }}">Head</a></li>
                    <li class=""><a href="{{ url('/subhead') }}">Sub Head</a></li>
                    <!--<li class=""><a href="{{ url('/particular') }}">Particular</a></li>-->
                </ul>
            </li>
        </ul>
    </li>

    <li>
        <a href="javascript:;" data-toggle="collapse" data-target="#dropdown_inventory">Inventory<i class="fa fa-fw fa-caret-down"></i></a>
        <ul id="dropdown_inventory" class="collapse {{ request()->is('service*') ? 'in' : '' }}">
            <li class="{{ request()->is('service') ? 'active' : '' }}">
                <a href="{{ url('/purchase') }}">Purchase</a>
                <a href="{{ url('#') }}">Purchase Order</a>
                <a href="{{ url('#') }}">Sales Order</a>
                <a href="{{ url('#') }}">Sales Order info</a>
                <a href="{{ url('#') }}">Delivery</a>
                <a href="{{ url('#') }}">Delivery Item Info</a>
                <a href="{{ url('/stock') }}">Stock</a>
                <a href="{{ url('#') }}">Stock Register</a>
                <a href="{{ url('#') }}">Product Register</a>
            </li>
        </ul>
    </li>

    <li>
        <a href="javascript:;" data-toggle="collapse" data-target="#dropdown_material">Raw Materials<i class="fa fa-fw fa-caret-down"></i></a>
        <ul id="dropdown_material" class="collapse {{ request()->is('service*') ? 'in' : '' }}">
            <li class="{{ request()->is('service') ? 'active' : '' }}">
                <a href="{{ url('#') }}">Raw Jute</a>
                <a href="{{ url('#') }}">Chemical</a>
                <a href="{{ url('#') }}">Spare Parts</a>
            </li>
        </ul>
    </li>

    <li>
        <a href="javascript:;" data-toggle="collapse" data-target="#dropdown_production">Production<i class="fa fa-fw fa-caret-down"></i></a>
        <ul id="dropdown_production" class="collapse {{ request()->is('service*') ? 'in' : '' }}">
            <li class="{{ request()->is('service') ? 'active' : '' }}">
                <a href="{{ url('#') }}">Batch Create</a>
                <a href="{{ url('#') }}">Batch Detail</a>
                <a href="{{ url('#') }}">Raw Jute</a>
                <a href="{{ url('#') }}">Chemical</a>
                <a href="{{ url('#') }}">Spare Parts</a>
            </li>
        </ul>
    </li>

    <li>
        <a href="javascript:;" data-toggle="collapse" data-target="#dropdown_production_deport">Production Deport<i class="fa fa-fw fa-caret-down"></i></a>
        <ul id="dropdown_production_deport" class="collapse {{ request()->is('service*') ? 'in' : '' }}">
            <li class="{{ request()->is('service') ? 'active' : '' }}">
                <a href="{{ url('#') }}">Yarn Section</a>
                <a href="{{ url('#') }}">Cutting Section</a>
                <a href="{{ url('#') }}">Swing Section</a>
                <a href="{{ url('#') }}">Jute Bag</a>
            </li>
        </ul>
    </li>

    <li>
        <a href="javascript:;" data-toggle="collapse" data-target="#dropdown_Essential">Essential Setting <i class="fa fa-fw fa-caret-down"></i></a>
        <ul id="dropdown_Essential" class="collapse {{ request()->is('service*') ? 'in' : '' }}">

            <li class="{{ request()->is('service/maintenance') ? 'active' : '' }}">
                <a  href="{{ url('/essential/bag_type') }}">Bag Type</a>

            </li>
        
            <li class="{{ request()->is('service/maintenance') ? 'active' : '' }}">
                <a  href="{{ url('/essential/pc_settings') }}">Pc settings</a>

            </li>
            <li class="{{ request()->is('service/maintenance') ? 'active' : '' }}">
                <a  href="{{ url('/essential/shade_settings') }}">Shade Setting</a>

            </li>
            <li class="{{ request()->is('service/maintenance') ? 'active' : '' }}">
                <a  href="{{ url('/essential/agent_list') }}">Agent/party list</a>

            </li>
            <li class="{{ request()->is('service/maintenance') ? 'active' : '' }}">
                <a  href="{{ url('/essential/conditions') }}">Condition</i></a>

            </li>
            <li class="{{ request()->is('service/maintenance') ? 'active' : '' }}">
                <a  href="{{ url('/essential/branches') }}">Branch setting</i></a>
            </li>
            <li class="{{ request()->is('service/maintenance') ? 'active' : '' }}">
                <a  href="{{ url('/essential/bank_settings') }}">Bank Setting</i></a>
            </li>

            <li class="{{ request()->is('service/maintenance') ? 'active' : '' }}">
                <a  href="{{ url('/essential/basic_settings') }}">Basic Setting</i></a>
            </li>

        </ul>
    </li>
    <li>
        <a href="javascript:;" data-toggle="collapse" data-target="#dropdown_daily_work">Daily Work<i class="fa fa-fw fa-caret-down"></i></a>
        <ul id="dropdown_daily_work" class="collapse {{ request()->is('service*') ? 'in' : '' }}">
            <li class="{{ request()->is('service') ? 'active' : '' }}">
                <a href="{{ url('product_in_create') }}">Product In Create</a>
                <a href="{{ url('#') }}">LoadPallot Create</a>
                <a href="{{ url('#') }}">Advance Loan Payment</a>
                <a href="{{ url('#') }}">Advance Loan Receive</a>
                <a href="{{ url('#') }}">Loan Create</a>
                <a href="{{ url('#') }}">Loan Receive</a>
                <a href="{{ url('#') }}">Delivery</a>
                <a href="{{ url('#') }}">Transfer</a>
            </li>
        </ul>
    </li>


    <li>
        <a href="javascript:;" data-toggle="collapse" data-target="#dropdown_Accounts">Accounts<i class="fa fa-fw fa-caret-down"></i></a>
        <ul id="dropdown_Accounts" class="collapse {{ request()->is('service*') ? 'in' : '' }}">
            <li class="{{ request()->is('service') ? 'active' : '' }}">
                <a href="{{ url('bank_transaction') }}">Bank Transaction</a>
                <a href="{{ url('cash_transaction') }}">Cash Transaction</a>
                <a href="{{ url('account_head') }}">Account Head</a>
                <a href="{{ url('requisition') }}">Requisition</a>
            </li>
        </ul>
    </li>
    <li>
            <a href="{{ url('/sr') }}">SR</a>
    </li>
    <li>
            <a href="{{ url('/user_list') }}">User List</a>
    </li>
     <li>
            <a href="{{ url('#') }}">Report</a>
    </li>
</ul>
@endif
