<ul class="nav navbar-nav side-nav">
    @if(isLegend())
    <li>
        <a href="javascript:;" data-toggle="collapse" data-target="#dropdown_legend">Legend <i class="fa fa-fw fa-caret-down"></i></a>
        <ul id="dropdown_legend" class="collapse {{ request()->is('service*') ? 'in' : '' }}">
            <li class="{{ request()->is('service') ? 'active' : '' }}">
                <a href="{{ url('/service') }}">Service</a>
            </li>
            <li class="{{ request()->is('service/maintenance') ? 'active' : '' }}">
                <a href="{{ url('/service/maintenance') }}">Maintenance</a>
            </li>
        </ul>
    </li>
    @endif

    <li class="{{ request()->is('home') ? ' active' : '' }}">
        <a href="{{ url('/home') }}">{{ trans('words.dashboard') }}</a>
    </li>
</ul>
