<ul class="nav navbar-nav side-nav">
    @if(isLegend())
    <li>
        <a href="javascript:;" data-toggle="collapse" data-target="#dropdown_legend">Legend <i class="fa fa-fw fa-caret-down"></i></a>
        <ul id="dropdown_legend" class="collapse {{ request()->is('service*') ? 'in' : '' }}">
            <li class="{{ request()->is('service') ? 'active' : '' }}">
                <a href="{{ url('/service') }}">Service</a>
            </li>
            <li class="{{ request()->is('service/maintenance') ? 'active' : '' }}">
                <a href="{{ url('/service/maintenance') }}">Maintenance</a>
            </li>
        </ul>
    </li>
    @endif

    <li class="{{ request()->is('home') ? ' active' : '' }}">
        <a href="{{ url('/home') }}">{{ trans('words.dashboard') }}</a>
    </li>

    @if (has_user_access('module_permission_sr'))
    <li>
        <a href="javascript:;" data-toggle="collapse" data-target="#dropdown_sr"><i class="fa fa-user"></i> {{ trans('words.sr') }} <i class="fa fa-fw fa-caret-down"></i></a>
        <ul id="dropdown_sr" class="collapse {{ request()->is('sr/sale*') ? 'in' : '' }}">            
            @if(has_user_access('sr_sales_list'))
            <li class="{{ request()->is('sr/sale') ? 'active' : '' }}">
                <a href="{{ url('/sr/sale') }}">{{ trans('words.sale_order') }}</a>
            </li>
            @endif
            @if(has_user_access('sr_sales_item_list'))
            <li class="{{ request()->is('sr/sale/items') ? 'active' : '' }}">
                <a href="{{ url('/sr/sale/items') }}">{{ trans('words.sale_order_items') }}</a>
            </li>
            @endif
            @if(has_user_access('sr_sales_ledger'))
            <li class="{{ request()->is('sr/sale/ledger') ? 'active' : '' }}">
                <a href="{{ url('/sr/sale/ledger') }}">{{ trans('words.sale_order_ledger') }}</a>
            </li>
            @endif
        </ul>
    </li>
    @endif
</ul>
