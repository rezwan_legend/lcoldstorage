<?php

use App\Models\Setting;

$setting = Setting::get_setting();
?>
<!DOCTYPE html>
<html lang="{{ App::getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta name="base-url" content="{{ url('/') }}">
        <meta name="current-uid" content="{{ Auth::id() }}">

        <title>{{ $setting->title }}</title>

        <link href="{{ asset('public/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ asset('public/css/select2.min.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ asset('public/css/bootstrap.min.css') }}" rel="stylesheet">
        <link href="{{ asset('public/css/style.css') }}" rel="stylesheet" media="all">
        <link href="{{ asset('public/css/responsive.css') }}" rel="stylesheet" media="screen">
        <link href="{{ asset('public/css/print.css') }}" rel="stylesheet" media="print">
        <link href="{{ asset('public/css/bootstrap-datepicker.min.css') }}" rel="stylesheet">
        <style type="text/css">
            @yield('page_style')
        </style>
        <script src="{{ asset('public/js/jquery-1.12.2.min.js') }}"></script>
        <script>window.Laravel = {!! json_encode(['csrfToken' => csrf_token()]) !!};</script>
    </head>
    <body>
        <div id="wrapper">
            <!-- Navigation -->
            <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <!-- Branding Image -->
                    <a class="navbar-brand" href="{{ url('/home') }}">{{ $setting->title }}</a>
                </div>
                <!-- Top Menu Items | Authentication Links -->
                <ul class="nav navbar-right top-nav">
                    @if (Auth::guest())
                    <li><a href="{{ route('login') }}">Login</a></li>
                    <li><a href="{{ route('register') }}">Register</a></li>
                    @else
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                            <i class="fa fa-user"></i> Hi, {{ Auth::user()->name }} <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu" role="menu">
                            <li>
                                <a href="{{ url('/home') }}">Dashboard</a>
                            </li>
                            <li>
                                <a href="{{ url('/user/'.Auth::id()) }}">Profile</a>
                            </li>
                            <li>
                                <a href="{{ url('/user/'.Auth::id().'/password') }}">Change Password</a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">Logout</a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </li>
                        </ul>
                    </li>
                    @endif
                </ul>

                <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
                <div class="collapse navbar-collapse navbar-ex1-collapse">
                    <ul class="nav navbar-nav side-nav">
                        <li>
                            <a href="{{ url('/dsr/home') }}"><i class="fa fa-fw fa-dashboard"></i> {{ trans('words.dashboard') }}</a>
                        </li>                        

                        <li>
                            <a href="javascript:;" data-toggle="collapse" data-target="#salesPerson"><i class="fa fa-users"></i> {{ trans('words.sales_person') }} <i class="fa fa-fw fa-caret-down"></i></a>
                            <ul id="salesPerson" class="collapse {{ (request()->is('retailer-*') || request()->is('brand-*') || request()->is('commission-*') || request()->is('sales-*') || request()->is('sale_*')) ? 'in' : '' }}">
                                <li class="{{ request()->is('retailer-*') ? 'active' : '' }}">
                                    <a href="{{ url('/retailer-setting') }}">{{ trans('words.retailer_setting') }}</a>
                                </li>
                                <li class="{{ request()->is('brand-*') ? 'active' : '' }}">
                                    <a href="{{ url('/brand-setting') }}">{{ trans('words.brand_setting') }}</a>
                                </li>
                                <li class="{{ request()->is('commission-*') ? 'active' : '' }}">
                                    <a href="{{ url('/commission-setting') }}">{{ trans('words.commission_setting') }}</a>
                                </li>
                                <li class="{{ request()->is('sales-*') ? 'active' : '' }}">
                                    <a href="{{ url('/sales-target') }}">{{ trans('words.target_setting') }}</a>
                                </li>
                                <li class="{{ request()->is('sale_person_stocks') ? 'active' : '' }}">
                                    <a href="{{ url('/sale_person_stocks') }}">{{ trans('words.stocks') }}</a>
                                </li>
                                <li class="{{ request()->is('sale_person_stocks/details') ? 'active' : '' }}">
                                    <a href="{{ url('/sale_person_stocks/details') }}">{{ trans('words.stock_register') }}</a>
                                </li>
                                <li class="{{ request()->is('sale_person_sale') ? 'active' : '' }}">
                                    <a href="{{ url('/sale_person_sale') }}">{{ trans('words.sale_list') }}</a>
                                </li>
                                <li class="{{ request()->is('sale_person_sale/items') ? 'active' : '' }}">
                                    <a href="{{ url('/sale_person_sale/items') }}">{{ trans('words.sale_item_list') }}</a>
                                </li>
                                <li class="">
                                    <a href="#">{{ trans('words.sale_invoice') }}</a>
                                </li>
                                <li class="{{ request()->is('report/monthly') ? 'active' : '' }}">
                                    <a href="{{ url('/report/monthly') }}"> {{ trans('words.monthly_sale_report') }}</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div><!-- /.navbar-collapse -->
            </nav>

            <div id="page-wrapper">
                <!-- Main Content -->
                <div class="container-fluid">
                    @if(has_flash_message())
                    <?php echo view_flash_message(); ?>
                    @endif
                    <div class="alert"  id="ajaxMessage" style="display:none;"></div>
                    <div class="text-center btn-warning" id="loading_image">Loading... <img src="{{ asset('img/ajax-loader.gif') }}" alt="Loading...."> </div>
                    @yield('content')
                </div><!-- End Main Content -->
            </div><!-- /#page-wrapper -->

            <footer id="footerPanel">
                <div id="footerBottom">
                    <div class="container-fluid">
                        <div class="text-center">
                            <div class="text-center">
                                <p>Developed and Maintenance By <a href="https://legendsoftbd.com" target="_blank" style="color:#FFEE58">Legend Soft</a> {{ date('Y') }}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
        </div><!-- /#wrapper -->
        <div id="mask"></div>
        <!-- jQuery -->
        <script src="{{ asset('public/js/select2.min.js') }}"></script>
        <script src="{{ asset('public/js/bootstrap.min.js') }}"></script>
        <script src="{{ asset('public/js/bootstrap-datepicker.min.js') }}"></script>
        <script src="{{ asset('public/js/custom.js') }}"></script>
        @yield('page_script')
    </body>
</html>