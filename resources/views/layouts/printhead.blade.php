<?php 
use App\Setting;
$settings = Setting::get_setting();
?>
<div class="invoice_heading mpmt show_in_print">
    <div class="row">
        <div class="col-md-12 text-center">
            <h3 style="margin:0px;">{{ $settings->title }}</h3>
            <span>{{ $settings->address }}</span><br/>
            <span>Contact:{{ $settings->mobile }}</span><br/>
            <span style="display:block;margin-bottom:5px; text-decoration:underline;"><strong>Purchase Voucher</strong></span>
        </div>
    </div>
</div>