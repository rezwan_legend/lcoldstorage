
@extends('layouts.app')

@section('breadcrumbs')
<div id="breadcrumbBar" class="breadcrumb site_nav_links no_bdr_rad clearfix">
    <div class="col-md-2 col-sm-3 col-xs-2 no_pad">
        <button class="btn btn-info btn-xs" type="button" onclick="history.back()" title="Go Back"><span class="visible-xs"><i class="fa fa-arrow-left"></i></span><span class="hidden-xs">Back</span></button>
        <button class="btn btn-info btn-xs" onclick="redirectTo('<?= url('view-clear'); ?>')" title="Refresh" type="button"><span class="visible-xs"><i class="fa fa-refresh"></i></span><span class="hidden-xs">Refresh</span></button>
    </div>
    <div class="col-md-6 col-sm-6 hidden-xs text-center">
        <h2 class="page-title">{{$page_title}}</h2>
    </div>
    <div class="col-md-4 col-sm-3 col-xs-10 no_pad">
        <ul class="text-right no_mrgn no_pad">
            <li><a href="{{ url('/home') }}">Home</a> <i class="fa fa-angle-right"></i></li>
            <li>balance</li>
        </ul>
    </div>
</div>
@endsection

@section('content')


<div id="bodyPanel" style="min-height: 507px;">
            <div class="container-fluid">
                <div class="row clearfix">
                                        <div class="col-md-12" id="admin_view">
                        <div id="breadcrumbBar" class="breadcrumb site_nav_links clearfix">
                            <div class="col-md-3 col-sm-3 col-xs-3 no_pad">
                                <button type="button" class="btn btn-info btn-xs" onclick="redirectTo('/en/site/clear_cache')" title="Refresh"><i class="fa fa-refresh"></i> <span class="hidden-xs">Refresh</span></button>
                                <button type="button" class="btn btn-primary btn-xs" onclick="printDiv('printDiv')" title="Print"><i class="fa fa-print"></i> <span class="hidden-xs">Print</span></button>
                            </div>
                            <div class="col-md-6 col-sm-6 hidden-xs text-center">
                                <h2 class="page-title">Account Balance</h2>
                            </div>
                            <div class="col-md-3 col-sm-3 col-xs-9 no_pad">
                                <ul class="text-right no_mrgn">
<li><a href="/">Dashboard</a> <span class="fa fa-angle-right"></span></li><li><a href="/en/account">Bank Account</a> <span class="fa fa-angle-right"></span></li><li><span>Balance</span></li></ul>                            </div>
                        </div>
                    

                        
                        <table class="table table-bordered tbl_invoice_view" style="margin:0">
    <tbody><tr class="bg-warning">
        <td><strong>Bank Name :</strong> FCAD </td>
        <td><strong>Branch Name :</strong> Florence Howard</td>
        <td><strong>Account Name :</strong> Forrest Conway</td>
        <td><strong>Account Number :</strong> 5027852</td>
    </tr>
</tbody></table>
<div class="well">
    <table width="100%">
        <tbody><tr>
            <td>
                <form class="search-form" method="post" name="frmSearch" id="frmSearch">
                    <input type="hidden" name="accountID" value="1">
                    <div class="input-group">
                        <div class="input-group-btn clearfix">
                            <select class="form-control " id="itemCount" name="itemCount" style="width:55px"><option value="25">25</option><option value="50" selected="">50</option><option value="100">100</option><option value="200">200</option><option value="300">300</option><option value="400">400</option><option value="500">500</option></select>                            <div class="col-md-2 col-sm-3 no_pad xsw_30" style="width:7%"><select class="form-control" id="session" name="session"><option value="2022">2022</option></select></div>                            <select id="type" name="type" class="form-control" style="width:auto;">
                                <option value="All">All</option>
                                <option value="Debit">Debit</option>
                                <option value="Credit">Credit</option>
                            </select>
                            <div class="col-md-2 col-sm-3 no_pad">
                                <div class="input-group xsw_100">
                                    <input type="text" id="from_date" class="form-control" name="from_date" placeholder="(dd-mm-yyyy)" readonly="">
                                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                </div>
                            </div>
                            <div class="col-md-1 col-sm-1 text-center" style="font-size:14px;width:5%;">
                                <b style="color: rgb(0, 0, 0); vertical-align: middle; display: block; padding: 6px 0px;">TO</b>
                            </div>
                            <div class="col-md-2 col-sm-3 no_pad">
                                <div class="input-group xsw_100">
                                    <input type="text" id="to_date" class="form-control" name="to_date" placeholder="(dd-mm-yyyy)" readonly="">
                                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                </div>
                            </div>
                            <button type="button" id="search" class="btn btn-info">Search</button>
                            <button type="button" id="clear_from" class="btn btn-primary" data-info="/account/balance">Clear</button>
                        </div>
                    </div>
                </form>
            </td>
        </tr>
    </tbody></table>
</div>

<form id="deleteForm" action="" method="post">
    <table class="table table-bordered tbl_invoice_view" style="margin:0">
        <tbody><tr>
            <td><strong>Total Debit : </strong> 0&nbsp;Tk</td>
            <td><strong>Total Credit : </strong> 0&nbsp;Tk</td>
            <td><strong>Total Balance : </strong> 0&nbsp;Tk</td>
        </tr>
    </tbody></table>
    <div id="ajaxContent">
                    <div class="alert alert-info">No records found!</div>
            </div>
</form>
<div class="modal fade" id="containerForDetailInfo" tabindex="-1" role="dialog" aria-labelledby="containerForDetailInfoLabel"></div>
<script type="text/javascript">
    $(document).ready(function () {
        $("#from_date, #to_date").datepicker({format: 'dd-mm-yyyy', autoclose: true, orientation: "auto"});

        $(document).on("click", "#search", function (e) {
            showLoader("Processing...", true);
            var _form = $("#frmSearch");

            $.ajax({
                type: "POST",
                url: baseUrl + "/account/search_balance",
                data: _form.serialize(),
                success: function (res) {
                    showLoader("", false);
                    $("#ajaxContent").html('');
                    $("#ajaxContent").html(res);
                }
            });
            e.preventDefault();
        });

        $(document).on("click", "#admin_del_btn", function (e) {
            var _rc = confirm('Are you sure about this action? This cannot be undone!');

            if (_rc === true) {
                showLoader("Processing...", true);
                var _form = $("#deleteForm");
                var _url = ajaxUrl + "/account/deleteall_balance";

                $.post(_url, _form.serialize(), function (res) {
                    if (res.success === true) {
                        $("#ajaxMessage").showAjaxMessage({html: res.message, type: 'success'});
                        $("tr.bg-danger").remove();
                        $("#search").trigger('click');
                    } else {
                        $("#ajaxMessage").showAjaxMessage({html: res.message, type: 'error'});
                    }
                    reset_index();
                    showLoader("", false);
                }, "json");
            } else {
                return false;
            }
            e.preventDefault();
        });
    });
</script>                    </div>
                </div>
            </div>
        </div>
@endsection