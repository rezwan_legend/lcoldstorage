@if(!empty($dataset) && count($dataset) > 0)
<div class="table-responsive">
    <table class="table table-bordered tbl_thin" id="check">
        <tr class="bg-info" id="r_checkAll">
            <th class="text-center" style="width:3%;">#</th>
              <th>Branch</th>
                    <th>Bank Name</th>
                    <th>Branch Name</th>
                    <th>Account Type</th>
                    <th>Account Name</th>
                    <th>Account Number</th>
                    <th>Address</th>
            <th class="text-center hip" style="width:10%;">Actions</th>
            <th class="text-center hip" style="width:3%;"><input type="checkbox" id="check_all" value="all"></th>
        </tr>
        <?php
        $counter = 0;
        if (isset($_GET['page']) && $_GET['page'] > 1) {
            $counter = ($_GET['page'] - 1) * $dataset->perPage();
        }
        ?>
        @foreach ($dataset as $data)
        <?php $counter++; ?>
        <tr onmouseover="change_color(this, true)" onmouseout="change_color(this, false)">
            <td class="text-center">{{ $counter }}</td>
             <th>{{  !empty($data->branchs) ? $data->branchs->name : ""}}</th>
                    <th>{{  !empty($data->banks) ? $data->banks->name : ""}}</th>
                    <th>{{ $data->branch_name }}</th>
                    <th>{{ $data->account_type }}</th>
                    <th>{{ $data->account_name }}</th>
                    <th>{{ $data->account_number }}</th>
                    <th>{{ $data->address }}</th>
            <td class="text-center hip" style="width:10%;">
                @if(has_user_access('unit_edit'))
                <a class="btn btn-info btn-xs" href="{{ url('bank_transaction/'.$data->_key.'/edit') }}"><i class="fa fa-edit"></i> Edit</a>
                @endif
            </td>
            <td class="text-center hip">
                @if(has_user_access('unit_delete'))
                <input type="checkbox" name="data[]" value="{{ $data->id }}">
                @endif
            </td>
        </tr>
        @endforeach
    </table>
</div>
<div class="mb_10 text-center hip" id="apaginate">
    {{ $dataset->render() }}
</div>
@else
<div class="alert alert-info">No records found.</div>
@endif