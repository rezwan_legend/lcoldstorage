@extends('layouts.app')

@section('breadcrumbs')
<div id="breadcrumbBar" class="breadcrumb site_nav_links no_bdr_rad clearfix">
    <div class="col-md-3 col-sm-3 col-xs-2 no_pad">
        <button class="btn btn-info btn-xs" type="button" onclick="history.back()" title="Go Back"><span class="visible-xs"><i class="fa fa-arrow-left"></i></span><span class="hidden-xs">Back</span></button>
        <button class="btn btn-info btn-xs" onclick="redirectTo('<?= url('view-clear'); ?>')" title="Refresh" type="button"><span class="visible-xs"><i class="fa fa-refresh"></i></span><span class="hidden-xs">Refresh</span></button>
    </div>
    <div class="col-md-6 col-sm-6 hidden-xs text-center">
        <h2 class="page-title">{{$page_title}}</h2>
    </div>
    <div class="col-md-3 col-sm-3 col-xs-10 no_pad">
        <ul class="text-right no_mrgn no_pad">
            <li><a href="{{ url('/home') }}">Home</a> <i class="fa fa-angle-right"></i></li>
            <li><a href="{{ url('/bank_transaction') }}">Bank Transaction</a> <i class="fa fa-angle-right"></i></li>
            <li>Form</li>
        </ul>
    </div>
</div>
@endsection

@section('content')
<div class="col-md-6 col-md-offset-3 col-sm-6 col-sm-offset-3">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title">{{$page_title}}</h3>
        </div>
        <div class="panel-body">
            {!! Form::open(['method' => 'POST', 'url' => '/bank_transaction', 'class' => 'form-horizontal']) !!}
            
        <div class="form-group">
            <label class="col-md-4 control-label" for="branches">Branch</label>
            <div class="col-md-6">
                <select class="form-control select2search" id="branches" name="branches" required>
                    <option value="">Select Branch</option>
                    @foreach($branches as $branche)
                    <option value="{{$branche->id}}">{{$branche->name}}</option>
                    @endforeach
                </select>
                <small class="text-danger">{{ $errors->first('branches') }}</small>
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-4 control-label" for="banks">Banks</label>
            <div class="col-md-6">
                <select class="form-control select2search" id="banks" name="banks" required>
                    <option value="">Select Bank</option>
                    @foreach($banks as $bank)
                    <option value="{{$bank->id}}">{{$bank->name}}</option>
                    @endforeach
                </select>
                <small class="text-danger">{{ $errors->first('banks') }}</small>
            </div>
        </div>


            <div class="form-group">
                <label for="bname" class="col-md-4 control-label">Branch Name</label>
                <div class="col-md-6">
                    <input type="text" class="form-control" id="bname" name="branche_name" required>
                    <small class="text-danger">{{ $errors->first('bank_transaction') }}</small>
                </div>
            </div>


            <div class="form-group">
                <label class="col-md-4 control-label" for="atype">Account Type</label>
                <div class="col-md-6">
                    <select class="form-control select2search" id="atype" name="account_type" required>
                        <option value="">Select Account</option>
                        <option value="CC Account">CC Account</option>
                        <option value="Current Account">Current Account</option>
                        <option value="LC Account">LC Account</option>
                        <option value="Savings Account">Savings Account</option>
                    </select>
                    <small class="text-danger">{{ $errors->first('account_type') }}</small>
                </div>
            </div>

             <div class="form-group">
                <label for="aname" class="col-md-4 control-label">Account Name</label>
                <div class="col-md-6">
                    <input type="text" class="form-control" id="aname" name="account_name" required>
                    <small class="text-danger">{{ $errors->first('account_name') }}</small>
                </div>
            </div>

             <div class="form-group">
                <label for="anumber" class="col-md-4 control-label">Account Number</label>
                <div class="col-md-6">
                    <input type="text" class="form-control" id="anumber" name="account_number" required>
                    <small class="text-danger">{{ $errors->first('account_number') }}</small>
                </div>
            </div>    
             <div class="form-group">
                <label for="address" class="col-md-4 control-label">Adderss</label>
                <div class="col-md-6">
                    <textarea id="address" name="address" rows="4" cols="50" class="form-control"></textarea>
                    <small class="text-danger">{{ $errors->first('address') }}</small>
                </div>
            </div>           
            <div class="col-md-8 col-md-offset-4">
                <button type="button" class="btn btn-info" id="resetbtn">Reset</button>
                <button type="submit" class="btn btn-primary">Save</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>

@endsection