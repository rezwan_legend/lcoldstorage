@extends('layouts.app')

@section('content')
    <div id="breadcrumbBar" class="breadcrumb site_nav_links no_bdr_rad clearfix">
        <div class="col-md-3 col-sm-3 col-xs-2 cxs_2 no_pad">
            <button class="btn btn-info btn-xs" type="button" onclick="history.back()" title="Go Back"><span
                    class="visible-xs"><i class="fa fa-arrow-left"></i></span><span
                    class="hidden-xs">Back</span></button>
            <button class="btn btn-info btn-xs" onclick="redirectTo('<?= url('party_commission_multi') ?>')" title="Refresh"
                type="button"><span class="visible-xs"><i class="fa fa-refresh"></i></span><span
                    class="hidden-xs">Refresh</span></button>
        </div>
        <div class="col-md-6 col-sm-6 col-xs-6 cxs_10 text-center">
            <h2 class="page-title">{{ $page_title }}</h2>
        </div>
        <div class="col-md-3 col-sm-3 col-xs-4 cxs_12 no_pad">
            <ul class="text-right no_mrgn">
                <li><a href="{{ url('/home') }}">Dashboard</a> <span class="fa fa-angle-right"></span></li>
                <li>Party commission</li>
            </ul>
        </div>
    </div>

    <div class="well">

        <div class="clearfix mb_10">
            <div class="text-center">

               
                    @if (has_user_access('party_commission_multi_create'))
                        <a class="btn btn-success btn-xs " href="{{ url('/party_commission_multi/create') }}"><i
                                class="fa fa-plus"></i> New</a>
                    @endif
                    @if (has_user_access('party_commission_multi_delete'))
                        <button type="button" class="btn btn-danger btn-xs " id="Del_btn" disabled><i
                                class="fa fa-trash-o"></i> Delete</button>
                    @endif
                    <button class="btn btn-primary btn-xs " onclick="printDiv('print_area')"><i
                            class="fa fa-print"></i> Print</button>
               

            </div>
        </div>
        <table width="100%">
            <tr>
                <td class="wmd_70">
                    {!! Form::open(['method' => 'POST', 'id' => 'frmSearch', 'name' => 'frmSearch']) !!}
                    <div class="input-group">
                        <div class="input-group-btn clearfix">
                            <?php echo number_dropdown(50, 550, 50, null, 'xsw_30'); ?>
                        
                            <div class="col-md-2 col-sm-3 no_pad xsw_70">
                                <select class="form-control select2search" id="party_type_id" name="party_type_id">
                                    <option value="">Party type</option>
                                    @foreach ($party_type as $type)
                                        <option value="{{ $type->id }}">{{ $type->party_type }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-md-2 col-sm-3 no_pad xsw_50">
                                <select class="form-control select2search" id="party_id" name="party_id">
                                    <option value="">Select Party</option>
                                </select>
                            </div>

                            <div class="col-md-2 col-sm-3 no_pad xsw_50">
                                <select class="form-control select2search" id="category_id" name="category_id">
                                    <option value="">Category</option>
                                    @foreach ($category as $category_name)
                                        <option value="{{ $category_name->id }}">{{ $category_name->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-md-2 col-sm-3 no_pad xsw_50">
                                <select class="form-control select2search" id="Product_id" name="Product_id">
                                    <option value="">Select Product</option>
                                </select>
                            </div>

                            <div class="col-md-2 col-sm-3 no_pad xsw_50">
                                <select class="form-control select2search" id="unit_id" name="unit_id">
                                    <option value="">Unit</option>
                                    @foreach ($units as $unit_name)
                                        <option value="{{ $unit_name->id }}">{{ $unit_name->unit }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-md-2 col-sm-3 no_pad xsw_50">
                                <select class="form-control select2search" id="unit_size_id" name="unit_size_id">
                                    <option value="">Unit Size</option>
                                </select>
                            </div>
                            <div class="col-md-2 col-sm-3 no_pad xsw_50">
                                <input type="text" class="form-control" placeholder="commission" name="commission">
                            </div>
                            <div class="col-md-2 col-sm-3 no_pad xsw_50">

                            </div>
                            <div class="col-md-2 no_pad xsw_100" style="width: auto">
                                <button type="button" id="search" class="btn btn-info xsw_50">Search</button>
                                <button type="button" id="clear_from" class="btn btn-warning xsw_50">Clear</button>
                            </div>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </td>

            </tr>
        </table>
    </div>

    {!! Form::open(['method' => 'POST', 'id' => 'frmList', 'name' => 'frmList']) !!}
    <div id="print_area">
        <div class="table-responsive" id="ajax_content">
            <table class="table table-bordered tbl_thin" id="check">
                <tr class="bg-info" id="r_checkAll">
                    <th class="text-center" style="width:3%;">#</th>
                    <th>Party Type</th>
                    <th>Party Name</th>
                    <th>Category</th>
                    <th>Product</th>
                    <th>Unit</th>
                    <th>Unit Size</th>
                    <th>Commission</th>
                    <th class="text-center" style="width: 8%;">Actions</th>
                    <th class="text-center" style="width: 3%;"><input type="checkbox" id="check_all" value="all"></th>
                </tr>
                <?php
                $counter = 0;
                if (isset($_GET['page']) && $_GET['page'] > 1) {
                    $counter = ($_GET['page'] - 1) * $dataset->perPage();
                }
                ?>
                @forelse ($dataset as $data)
                    <?php $counter++; ?>
                    <tr onmouseover="change_color(this, true)" onmouseout="change_color(this, false)">
                        <td class="text-center">{{ $counter }}</td>
                        <td>{{ !empty($data->party_type) ? $data->party_type->party_type : '' }}</td>
                        <td>{{ !empty($data->party) ? $data->party->name : '' }}</td>
                        <td>{{ !empty($data->categories) ? $data->categories->name : '' }}</td>
                        <td>{{ !empty($data->products) ? $data->products->name : '' }}</td>
                        <td>{{ !empty($data->units) ? $data->units->unit : '' }}</td>
                        <td>{{ !empty($data->UnitSize) ? $data->UnitSize->name : '' }}</td>
                        <td>{{ $data->commission }}</td>
                        <td class="text-center">
                            @if (has_user_access('party_commission_multi_edit'))
                                <a class="btn btn-info btn-xs"
                                    href="party_commission_multi/{{ $data->id }}/edit">Edit</a>
                            @endif
                        </td>
                        <td class="text-center">
                            @if (has_user_access('party_commission_multi_delete'))
                                <input type="checkbox" name="data[]" value="{{ $data->id }}">
                            @endif
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td class="text-center" colspan="7"> No records found </td>
                    </tr>
                @endforelse
            </table>
        </div>
        <div class="text-center hip">
            {{ $dataset->render() }}
        </div>
    </div>
    {!! Form::close() !!}
@endsection

@section('page_script')
    <script type="text/javascript">
        $(document).on("change", "#party_type_id", function() {
            var _partyType = $("#party_type_id").val();
            $.ajax({
                url: "{{ URL::to('party_commission/partyListByType') }}",
                type: "post",
                data: {
                    _token: csrfToken,
                    partyTypeId: _partyType
                },
                success: function(res) {
                    $('#party_id').html(res);
                },
                error: function(xhr, status) {
                    alert('There is some error.Try after some time.');
                }
            });

        });
        $(document).on("change", "#category_id", function() {
            var _productType = $("#category_id").val();
            $.ajax({
                url: "{{ URL::to('/party_commission_multi/ProductByCategory') }}",
                type: "post",
                data: {
                    _token: csrfToken,
                    productTypeId: _productType
                },
                success: function(res) {
                    $('#Product_id').html(res);
                },
                error: function(xhr, status) {
                    alert('There is some error.Try after some time.');
                }
            });

        });
        $(document).on("change", "#unit_id", function() {
            var _Unit_id = $("#unit_id").val();
            $.ajax({
                url: "{{ URL::to('/party_commission_multi/UnitSizeByUnit') }}",
                type: "post",
                data: {
                    _token: csrfToken,
                    UnitSizeId: _Unit_id
                },
                success: function(res) {
                    $('#unit_size_id').html(res);
                },
                error: function(xhr, status) {
                    alert('There is some error.Try after some time.');
                }
            });

        });
    </script>
    <script type="text/javascript">
        $(document).ready(function() {
            $(document).on("click", "#search", function() {
                var _url = "{{ URL::to('party_commission_multi/search') }}";
                var _form = $("#frmSearch");
                $.ajax({
                    url: _url,
                    type: "post",
                    data: _form.serialize(),
                    success: function(res) {
                        $('#ajax_content').html(res);
                    },
                    error: function(xhr, status) {
                        alert('There is some error.Try after some time.');
                    }
                });
            });

            $(document).on("click", "#Del_btn", function() {
                var _url = "{{ URL::to('party_commission_multi/delete') }}";
                var _form = $("#frmList");
                var _rc = confirm("Are you sure about this action? This cannot be undone!");
                if (_rc == true) {
                    $.post(_url, _form.serialize(), function(resp) {
                        if (resp.success === true) {
                            $('#ajaxMessage').showAjaxMessage({
                                html: resp.message,
                                type: 'success'
                            });
                            $("#search").trigger("click");
                        } else {
                            $('#ajaxMessage').showAjaxMessage({
                                html: resp.message,
                                type: 'error'
                            });
                        }
                    }, "json");
                }
            });

            $(document).on("click", "#search", function() {
                var _url = "{{ URL::to('party_commission_multi/search') }}";
                var _form = $("#frmSearch");
                $.ajax({
                    url: _url,
                    type: "post",
                    data: _form.serialize(),
                    success: function(res) {
                        $('#ajax_content').html(res);
                    },
                    error: function(xhr, status) {
                        alert('There is some error.Try after some time.');
                    }
                });
            });
        });
    </script>
@endsection
