@if (!empty($dataset) && count($dataset) > 0)
<div class="table-responsive">
     <table class="table table-bordered tbl_thin" id="check">
                <tr class="bg-info" id="r_checkAll">
                    <th class="text-center" style="width:3%;">#</th>
                    <th>Party Type</th>
                    <th>Party Name</th>
                    <th>Category</th>
                    <th>Product</th>
                    <th>Unit</th>
                    <th>Unit Size</th>
                    <th>Commission</th>
                    <th class="text-center" style="width: 8%;">Actions</th>
                    <th class="text-center" style="width: 3%;"><input type="checkbox" id="check_all" value="all"></th>
                </tr>
                <?php
                $counter = 0;
                if (isset($_GET['page']) && $_GET['page'] > 1) {
                    $counter = ($_GET['page'] - 1) * $dataset->perPage();
                }
                ?>
                @forelse ($dataset as $data)
                    <?php $counter++; ?>
                    <tr onmouseover="change_color(this, true)" onmouseout="change_color(this, false)">
                    <td class="text-center">{{ $counter }}</td>
                    <td>{{ !empty($data->party_type) ? $data->party_type->party_type : "" }}</td>
                    <td>{{ !empty($data->party) ? $data->party->name : "" }}</td>
                    <td>{{ !empty($data->categories) ? $data->categories->name : "" }}</td>
                    <td>{{ !empty($data->products) ? $data->products->name : "" }}</td>
                    <td>{{ !empty($data->units) ? $data->units->unit : ""}}</td>
                    <td>{{ !empty($data->UnitSize) ? $data->UnitSize->name : "" }}</td>
                    <td>{{ $data->commission }}</td>
                        <td class="text-center">
                            @if (has_user_access('party_commission_multi_edit'))
                                <a class="btn btn-info btn-xs" href="party_commission_multi/{{ $data->id }}/edit">Edit</a>
                            @endif
                        </td>
                        <td class="text-center">
                            @if (has_user_access('party_commission_multi_delete'))
                                <input type="checkbox" name="data[]" value="{{ $data->id }}">
                            @endif
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td class="text-center" colspan="7"> No records found </td>
                    </tr>
                @endforelse
            </table>
</div>
<div class="text-center hip">
    {{ $dataset->render() }}
</div>
@else
<div class="alert alert-info">No records found!</div>
@endif