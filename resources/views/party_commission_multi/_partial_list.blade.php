@if(!empty($itemDataset) && count($itemDataset) > 0)
        <table class="table table-bordered tbl_thin" style="margin: 0">
            <tr class="bg-primary">
                <th class="text-center">commission type details</th>
            </tr>
        </table>
        <div class="table-responsive">
            <table class="table table-bordered tbl_thin" id="check">
                <tbody>
                <tr class="bg-info" id="r_checkAll">
                    <th class="text-center" style="width:3%;">#</th>
                    <th>Party Type</th>
                    <th>Party Name</th>
                    <th>Category</th>
                    <th>Product</th>
                    <th>Unit</th>
                    <th>Unit Size</th>
                    <th>commission</th>
                    <th class="text-center" style="width: 8%;">Actions</th>
                </tr>
                <?php
                $counter = 0;
                if (isset($_GET['page']) && $_GET['page'] > 1) {
                    $counter = ($_GET['page'] - 1) * $dataset->perPage();
                }
                ?>
                @forelse ($itemDataset as $data)
                    <?php $counter++; ?>
                    <tr onmouseover="change_color(this, true)" onmouseout="change_color(this, false)">
                    <td class="text-center">{{ $counter }}</td>
                    <td>{{ !empty($data->party_type) ? $data->party_type->party_type : "" }}</td>
                    <td>{{ !empty($data->party) ? $data->party->name : "" }}</td>
                    <td>{{ !empty($data->categories) ? $data->categories->name : "" }}</td>
                    <td>{{ !empty($data->products) ? $data->products->name : "" }}</td>
                    <td>{{ !empty($data->units) ? $data->units->unit : ""}}</td>
                    <td>{{ !empty($data->UnitSize) ? $data->UnitSize->name : ""}}</td>
                    <td>{{ $data->commission }}</td>
                        <td class="text-center">
                          
                             @if (has_user_access('party_commission_multi_delete'))
                              <a class="color_danger" title="Delete Item" href="{{ url('/party_commission_multi/remove_item/'.$data->id) }}" onclick="return confirm('Are you sure about this action?')"><i class="fa fa-trash-o"></i></a>
                             @endif
                        </td>
                      
                    </tr>
                @empty
                    <tr>
                        <td class="text-center" colspan="7"> No records found </td>
                    </tr>
                @endforelse
                </tbody>
            </table>
        </div>
      
        @endif

        <script>
             $(document).on("click", "#Del_btn", function() {
            var _url = "{{ URL::to('party_commission_multi/delete') }}";
            var _form = $("#frmList");
            var _rc = confirm("Are you sure about this action? This cannot be undone!");
            if (_rc == true) {
                $.post(_url, _form.serialize(), function(resp) {
                    if (resp.success === true) {
                        $('#ajaxMessage').showAjaxMessage({
                            html: resp.message,
                            type: 'success'
                        });
                        $("#search").trigger("click");
                    } else {
                        $('#ajaxMessage').showAjaxMessage({
                            html: resp.message,
                            type: 'error'
                        });
                    }
                }, "json");
            }
        });
        </script>