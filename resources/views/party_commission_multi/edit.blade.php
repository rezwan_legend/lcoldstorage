@extends('layouts.app')

@section('breadcrumbs')
<div id="breadcrumbBar" class="breadcrumb site_nav_links no_bdr_rad clearfix">
    <div class="col-md-3 col-sm-3 col-xs-2 no_pad">
        <button class="btn btn-info btn-xs" type="button" onclick="history.back()" title="Go Back"><span class="visible-xs"><i class="fa fa-arrow-left"></i></span><span class="hidden-xs">Back</span></button>
        <button class="btn btn-info btn-xs" onclick="redirectTo('<?= url('party_commission_multi/' . $data->id . '/edit'); ?>')" title="Refresh" type="button"><span class="visible-xs"><i class="fa fa-refresh"></i></span><span class="hidden-xs">Refresh</span></button>
    </div>
    <div class="col-md-6 col-sm-6 hidden-xs text-center">
        <h2 class="page-title">{{$page_title}}</h2>
    </div>
    <div class="col-md-3 col-sm-3 col-xs-10 no_pad">
        <ul class="text-right no_mrgn no_pad">
            <li><a href="{{ url('/home') }}">Home</a> <i class="fa fa-angle-right"></i></li>
            <li><a href="{{ url('/party_commission_multi') }}">commission</a> <i class="fa fa-angle-right"></i></li>
            <li>Form</li>
        </ul>
    </div>
</div>
@endsection

@section('content')
<div class="col-md-6 col-md-offset-3 col-sm-6 col-sm-offset-3">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title">{{$page_title}}</h3>
        </div>
        {!! Form::open(['method' => 'PUT', 'url' => 'party_commission_multi/'.$data->id, 'class' => 'form-horizontal']) !!}
        <div class="panel-body">
            {!! Form::open(['method' => 'POST', 'class' => 'search-form', 'id' => 'frmSearch', 'name' => 'frmSearch']) !!}
            <div class="form-group">
                <label class="col-md-4 control-label" for="unit">Product Type</label>
                <div class="col-md-6">
                    <select class="form-control select2search" id="party_type_id" name="party_type_id" required>
                        <option value="">Select Type</option>
                        @foreach($party_type as $type)
                        <option value="{{$type->id}}" <?php if ($type->id == $data->party_type_id) echo ' selected'; ?> >{{$type->party_type}}</option>
                        @endforeach
                    </select>
                    <small class="text-danger">{{ $errors->first('product_type') }}</small>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-4 control-label" for="unit">Party</label>
                <div class="col-md-6">
                    <select class="form-control select2search" id="party_id" name="party_id" required>
                        <option value="">Select Type</option>
                        @foreach($party as $partyItem)
                        <option value="{{$partyItem->id}}" <?php if ($partyItem->id == $data->party_id) echo ' selected'; ?> >{{$partyItem->name}}</option>
                        @endforeach
                    </select>
                    <small class="text-danger">{{ $errors->first('product_type') }}</small>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-4 control-label" for="unit">Category</label>
                <div class="col-md-6">
                    <select class="form-control select2search" id="category_id" name="category_id">
                    @foreach($categories as $category)
                        <option value="{{$category->id}}" <?php if ($category->id == $data->category_id) echo ' selected'; ?> >{{$category->name}}</option>
                        @endforeach
                    </select>
                    <small class="text-danger">{{ $errors->first('category') }}</small>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-4 control-label" for="unit">Product</label>
                <div class="col-md-6">
                    <select class="form-control select2search" id="product_id" name="product_id">
                    @foreach($products as $product)
                        <option value="{{$product->id}}" <?php if ($product->id == $data->product_id) echo ' selected'; ?> >{{$product->name}}</option>
                        @endforeach
                    </select>
                    <small class="text-danger">{{ $errors->first('product') }}</small>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-4 control-label" for="unit">Unit</label>
                <div class="col-md-6">
                    <select class="form-control select2search" id="unit_id" name="unit_id" required>
                        <option value="">Select Unit</option>
                        @foreach($units as $unitItem)
                        <option value="{{$unitItem->id}}" <?php if ($unitItem->id == $data->unit_id) echo ' selected'; ?> >{{$unitItem->unit}}</option>
                        @endforeach
                    </select>
                    <small class="text-danger">{{ $errors->first('unit') }}</small>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-4 control-label" for="unit_size_id">Unit Size</label>
                <div class="col-md-6">
                <select class="form-control select2search" id="unit_size_id" name="unit_size_id" required>
                       <option value="">Select Unit size</option>
                        @foreach($unitSize as $unit_size)
                        <option value="{{$unit_size->id}}" <?php if ($unit_size->id == $data->unit_size_id) echo ' selected'; ?> >{{$unit_size->name}}</option>
                        @endforeach
                    </select>
                    <small class="text-danger">{{ $errors->first('name') }}</small>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-4 control-label" for="commission">commission</label>
                <div class="col-md-6">
                    <input type="text" class="form-control" id="commission" name="commission" value="{{ $data->commission }}" required>
                    <small class="text-danger">{{ $errors->first('commission') }}</small>
                </div>
            </div>
            <div class="col-md-8 col-md-offset-4">
                <button type="button" class="btn btn-info" onclick="history.back()" title="Go Back">Reset</button>
                <button type="submit" class="btn btn-primary">Save</button>
            </div>
            {!! Form::close() !!}
        </div>
        {!! Form::close() !!}
    </div>
</div>
@endsection