@extends('layouts.app')

@section('breadcrumbs')
<div id="breadcrumbBar" class="breadcrumb site_nav_links no_bdr_rad clearfix">
    <div class="col-md-3 col-sm-3 col-xs-2 no_pad">
        <button class="btn btn-info btn-xs" type="button" onclick="history.back()" title="Go Back"><span class="visible-xs"><i class="fa fa-arrow-left"></i></span><span class="hidden-xs">Back</span></button>
        <button class="btn btn-info btn-xs" onclick="redirectTo('<?= url('party_loan/create'); ?>')" title="Refresh" type="button"><span class="visible-xs"><i class="fa fa-refresh"></i></span><span class="hidden-xs">Refresh</span></button>
    </div>
    <div class="col-md-6 col-sm-6 hidden-xs text-center">
        <h2 class="page-title">{{$breadcrumb_title}}</h2>
    </div>
    <div class="col-md-3 col-sm-3 col-xs-10 no_pad">
        <ul class="text-right no_mrgn no_pad">
            <li><a href="{{ url('/home') }}">Home</a> <i class="fa fa-angle-right"></i></li>
            <li><a href="{{ url('/party_loan') }}">Party Loan</a> <i class="fa fa-angle-right"></i></li>
            <li>Form</li>
        </ul>
    </div>
</div>
@endsection

@section('content')
<div class="panel panel-primary">
    <div class="panel-heading">
        <h3 class="panel-title">Enter Party Loan</h3>
    </div>
    {!! Form::open(['method' => 'POST','id'=>'DataSave', 'url'=>'/party_loan/DataSave', 'class' => 'form-horizontal']) !!}
    <div class="panel-body">

        <div class="form-group col-md-2">
            <select class="form-control select2search" id="party_type_id" name="party_type_id" required>
                <option value="">Select party Type</option>
                @foreach($party_type as $type)
                <option value="{{$type->id}}">{{$type->party_type}}</option>
                @endforeach
            </select>
            <small class="text-danger">{{ $errors->first('party_type') }}</small>
        </div>
        <div class="form-group col-md-2">
            <select class="form-control select2search" id="party_id" name="party_id">
                <option value="">Select Party</option>
            </select>
            <small class="text-danger">{{ $errors->first('party_rent') }}</small>
        </div>
        <div class="form-group col-md-2">
            <select class="form-control select2search" id="category_id" name="category_id">
                <option value="">Select Category</option>
                @foreach($categories as $category)
                <option value="{{ $category->id }}">{{ $category->name }}</option>
                @endforeach
            </select>
            <small class="text-danger">{{ $errors->first('category') }}</small>
        </div>
        <div class="form-group col-md-2">
            <select class="form-control select2search" id="product_id" name="product_id">
                <option value="">Select Product</option>

            </select>
            <small class="text-danger">{{ $errors->first('product') }}</small>
        </div>
        <div class="form-group col-md-2">
            <select class="form-control select2search" id="unit_id" name="unit_id" required>
                <option value="">Select Unit</option>
                @foreach($units as $unit)
                <option value="{{$unit->id}}">{{$unit->unit}}</option>
                @endforeach
            </select>
            <small class="text-danger">{{ $errors->first('unit') }}</small>
        </div>
        <div class="form-group col-md-2">
            <select class="form-control select2search" id="unit_size_id" name="unit_size_id" required>
                <option value="">Select Unit Size</option>

            </select>
            <small class="text-danger">{{ $errors->first('unit_size') }}</small>
        </div>
        <div class="form-group col-md-1">
            <input type="text" class="form-control" id="loan" name="loan" placeholder="Loan" required>
            <small class="text-danger">{{ $errors->first('loan') }}</small>
        </div>
        <div class="form-group col-md-1">
            <button style="padding: 5px;" class="text-center"><span id="datasave" style="border-radius:50%;color:white; cursor: pointer; padding:5px;background:#383838"><i class="fa fa-plus"></i></span></button>
        </div>
    </div>
    {!! Form::close() !!}
</div>
<div class="clearfix">
    <div class="clearfix" id="item_list">
        <table class="table table-bordered tbl_thin" style="margin: 0">
            <tr class="bg-primary">
                <th class="text-center"><span>Party Loan details</span>
                @if (has_user_access('party_loan_delete'))
                <button type="button" class="btn btn-danger btn-xs xsw_33 pull-right" id="Del_btn" disabled><i class="fa fa-trash-o"></i> Delete</button>
                @endif
            </th>
            </tr>
        </table>
        {!! Form::open(['method' => 'POST', 'id' => 'frmList', 'name' => 'frmList']) !!}
        <!-- {!! Form::open(['method' => 'POST', 'id' => 'currentItemUpdate', 'name' => 'currentUpdate']) !!} -->
        
        <div class="table-responsive">
            <table class="table table-bordered tbl_thin" id="check">
                <thead>
                    <tr class="bg-info" id="r_checkAll">
                        <th class="text-center" style="width:3%;">#</th>
                        <th>Party Type</th>
                        <th>Party Name</th>
                        <th>Category</th>
                        <th>Product</th>
                        <th>Unit</th>
                        <th>Unit Size</th>
                        <th>Loan</th>
                        <th class="text-center" style="width: 3%;"><input type="checkbox" id="check_all" value="all"></th>
                    </tr>
                </thead>
                <tbody id="loanitems">
                    <?php
                    $counter = 0;
                    if (isset($_GET['page']) && $_GET['page'] > 1) {
                        $counter = ($_GET['page'] - 1) * $dataset->perPage();
                    }
                    ?>
                    @forelse ($dataset as $data)
                    <?php $counter++; ?>
                    <tr onmouseover="change_color(this, true)" onmouseout="change_color(this, false)">
                        <td class="text-center">{{ $counter }}</td>
                        <td>{{ !empty($data->party_type) ? $data->party_type->party_type : "" }}</td>
                        <td>{{ !empty($data->party) ? $data->party->name : "" }}</td>
                        <td>{{ !empty($data->categories) ? $data->categories->name : "" }}</td>
                        <td>{{ !empty($data->products) ? $data->products->name : "" }}</td>
                        <td>{{ !empty($data->units) ? $data->units->unit : ""}}</td>
                        <td>{{ !empty($data->unitsize) ? $data->unitsize->weight : ""}}</td>
                        <td>{{ $data->loan }}</td>
                        <td class="text-center">
                            @if (has_user_access('party_loan_delete'))
                            <input type="checkbox" name="data[]" value="{{ $data->id }}">
                            @endif
                        </td>
                    </tr>
                    @empty
                    <tr>
                        <td class="text-center" colspan="8"> No records found </td>
                    </tr>
                    @endforelse
                </tbody>
            </table>
        </div>
        {!! Form::close() !!}
    </div>
</div>

<div class="text-center" style="margin-top:10px;">
    <a id="currentItemUpdateBtn" class="btn btn-primary xsw_100">Save</a>
</div>
<script type="text/javascript">
    //on change on party type  to party 
    $(document).on("change", "#party_type_id", function() {
        var _partyType = $("#party_type_id").val();
        $.ajax({
            url: "{{ URL::to('party_laon/partyListByType') }}",
            type: "post",
            data: {
                _token: csrfToken,
                partyTypeId: _partyType
            },
            success: function(res) {
                $('#party_id').html(res);
            },
            error: function(xhr, status) {
                alert('There is some error.Try after some time.');
            }
        });

    });
    //on change on category to product
    $(document).on("change", "#category_id", function() {
        var _productType = $("#category_id").val();
        $.ajax({
            url: "{{ URL::to('/party_loan/ProductByCategory') }}",
            type: "post",
            data: {
                _token: csrfToken,
                productTypeId: _productType
            },
            success: function(res) {
                $('#product_id').html(res);
            },
            error: function(xhr, status) {
                alert('There is some error.Try after some time.');
            }
        });

    });

    //on change on unit to unit size
    $(document).on("change", "#unit_id", function() {
        var _Unit_id = $("#unit_id").val();
        $.ajax({
            url: "{{ URL::to('/party_loan/UnitSizeByUnit') }}",
            type: "post",
            data: {
                _token: csrfToken,
                UnitSizeId: _Unit_id
            },
            success: function(res) {
                $('#unit_size_id').html(res);
            },
            error: function(xhr, status) {
                alert('There is some error.Try after some time.');
            }
        });

    });

    //form data save 

    $(document).on("submit", "#DataSave", function(event) {
        $('#ajaxMessage').hide();
        var party_type_id = $("#party_type_id").val();
        var _url = baseUrl + "/party_loan/DataSave";
        var _form = $("#DataSave");
        $.post(_url, _form.serialize(), function(res) {
            if (res.success === true) {
                getPartyLoanItem(1);
            } else {
                $("#ajaxMessage").showAjaxMessage({
                    html: res.message,
                    type: "error"
                });
            }
        }, "json");
        event.preventDefault();
        return false;
    });

    function getPartyLoanItem(_id) {
        axios.post("{{ url('/party_loan/itemList/createdLoanList') }}", {
                id: _id
            })
            .then((res) => {
                let count = 1;
                const data = res.data;
                console.log(data);
                const loanitems = document.querySelector("#loanitems");
                loanitems.innerHTML = data.map(item => `<tr>
                <td>${count++}</td>
                <td>${item.party_type.party_type}</td>
                <td>${item.party.name}</td>
                <td>${item.categories.name}</td>
                <td>${item.products.name}</td>
                <td>${item.units.unit}</td>
                <td>${item.unitsize.name}</td>
                <td>${item.loan}</td>
                <td class="text-center">
                    @if (has_user_access('party_loan_delete'))
                        <input type="checkbox" name="data[]" value="${item.id}">
                    @endif
                </td>
            </tr>`).join('');
            })
            .catch((error) => {
                console.log(error)
            })
    }

    $(document).on("click", "#Del_btn", function() {
        var _url = "{{ URL::to('party_loan/delete') }}";
        var _form = $("#frmList");
        var _rc = confirm("Are you sure about this action? This cannot be undone!");
        if (_rc == true) {
            $.post(_url, _form.serialize(), function(resp) {
                if (resp.success === true) {
                    $('#ajaxMessage').showAjaxMessage({
                        html: resp.message,
                        type: 'success'
                    });
                    $("input:checked").closest("tr").remove();
                    $("#search").trigger("click");
                } else {
                    $('#ajaxMessage').showAjaxMessage({
                        html: resp.message,
                        type: 'error'
                    });
                }
            }, "json");
        }
    });
    $(document).on("click", "#currentItemUpdateBtn", function() {
        var _url = "{{ URL::to('party_loan/currentItem/update') }}";
        var _form = $("#frmList");
        var _rc = confirm("Are want to save this data! Please select the Item.");
        if (_rc == true) {
            $.post(_url, _form.serialize(), function(resp) {
                if (resp.success === true) {
                    $('#ajaxMessage').showAjaxMessage({
                        html: resp.message,
                        type: 'success'
                    });
                    window.location.href = "{{ url('/party_loan') }}";
                    $("#search").trigger("click");
                } else {
                    $('#ajaxMessage').showAjaxMessage({
                        html: resp.message,
                        type: 'error'
                    });
                }
            }, "json");
        }
    });
</script>
@endsection