@extends('layouts.app')

@section('breadcrumbs')
<div id="breadcrumbBar" class="breadcrumb site_nav_links no_bdr_rad clearfix">
    <div class="col-md-3 col-sm-3 col-xs-2 no_pad">
        <button class="btn btn-info btn-xs" type="button" onclick="history.back()" title="Go Back"><span class="visible-xs"><i class="fa fa-arrow-left"></i></span><span class="hidden-xs">Back</span></button>
        <button class="btn btn-info btn-xs" onclick="redirectTo('<?= url('view-clear'); ?>')" title="Refresh" type="button"><span class="visible-xs"><i class="fa fa-refresh"></i></span><span class="hidden-xs">Refresh</span></button>
    </div>
    <div class="col-md-6 col-sm-6 hidden-xs text-center">
        <h2 class="page-title">{{$page_title}}</h2>
    </div>
    <div class="col-md-3 col-sm-3 col-xs-10 no_pad">
        <ul class="text-right no_mrgn no_pad">
            <li><a href="{{ url('/home') }}">Home</a> <i class="fa fa-angle-right"></i></li>
            <li><a href="{{ url('/chamber_setting') }}">Chamber Setting</a> <i class="fa fa-angle-right"></i></li>
            <li>Form</li>
        </ul>
    </div>
</div>
@endsection

@section('content')
<div class="col-md-6 col-md-offset-3 col-sm-6 col-sm-offset-3">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title">Create New Chamber Setting</h3>
        </div>
        <div class="panel-body">
            {!! Form::open(['method' => 'POST', 'url' => 'chamber_setting', 'class' => 'form-horizontal']) !!}
            <div class="form-group">
                <label for="chamber" class="col-md-4 control-label">Chamber</label>
                <div class="col-md-6">
                    <input type="text" class="form-control" name="chamber" placeholder="Chamber" required>
                    <small class="text-danger">{{ $errors->first('chamber') }}</small>
                </div>
            </div>
            <div class="form-group">
                <label for="capacity" class="col-md-4 control-label">Capacity</label>
                <div class="col-md-6">
                    <input type="text" class="form-control" name="capacity" placeholder="Capacity" required>
                    <small class="text-danger">{{ $errors->first('capacity') }}</small>
                </div>
            </div>
            <div class="col-md-8 col-md-offset-4">
                <button type="button" class="btn btn-info">Reset</button>
                <button type="submit" class="btn btn-primary">Save</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
@endsection