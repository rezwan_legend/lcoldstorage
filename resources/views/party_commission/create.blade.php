@extends('layouts.app')

@section('breadcrumbs')
<div id="breadcrumbBar" class="breadcrumb site_nav_links no_bdr_rad clearfix">
    <div class="col-md-3 col-sm-3 col-xs-2 no_pad">
        <button class="btn btn-info btn-xs" type="button" onclick="history.back()" title="Go Back"><span class="visible-xs"><i class="fa fa-arrow-left"></i></span><span class="hidden-xs">Back</span></button>
        <button class="btn btn-info btn-xs" onclick="redirectTo('<?= url('party_commission/create'); ?>')" title="Refresh" type="button"><span class="visible-xs"><i class="fa fa-refresh"></i></span><span class="hidden-xs">Refresh</span></button>
    </div>
    <div class="col-md-6 col-sm-6 hidden-xs text-center">
        <h2 class="page-title">{{$breadcrumb_title}}</h2>
    </div>
    <div class="col-md-3 col-sm-3 col-xs-10 no_pad">
        <ul class="text-right no_mrgn no_pad">
            <li><a href="{{ url('/home') }}">Home</a> <i class="fa fa-angle-right"></i></li>
            <li><a href="{{ url('/party_commission') }}">Party Commission</a> <i class="fa fa-angle-right"></i></li>
            <li>Form</li>
        </ul>
    </div>
</div>
@endsection

@section('content')
<div class="panel panel-primary">
    <div class="panel-heading">
        <h3 class="panel-title">Enter Party Commission</h3>
    </div>
    {!! Form::open(['method' => 'POST', 'url' => 'party_commission', 'class' => 'form-horizontal']) !!}
    <div class="panel-body">
        {!! Form::open(['method' => 'POST', 'class' => 'search-form', 'id' => 'frmSearch', 'name' => 'frmSearch']) !!}
        <div class="form-group">
            <label class="col-md-4 control-label" for="unit">Party Type</label>
            <div class="col-md-6">
                <select class="form-control select2search" id="party_type_id" name="party_type_id" required>
                    <option value="">Select Type</option>
                    @foreach($party_type as $type)
                    <option value="{{$type->id}}">{{$type->party_type}}</option>
                    @endforeach
                </select>
                <small class="text-danger">{{ $errors->first('party_type') }}</small>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-4 control-label" for="unit">Party</label>
            <div class="col-md-6">
                <select class="form-control select2search" id="party_id" name="party_id">
                    <option value="">Select Party</option>
                </select>
                <small class="text-danger">{{ $errors->first('party') }}</small>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-4 control-label" for="category_id">Category</label>
            <div class="col-md-6">
                <select class="form-control select2search" id="category_id" name="category_id">
                    <option value="">Select Category</option>
                    @foreach($categories as $category)
                        <option value="{{ $category->id }}">{{ $category->name }}</option>
                    @endforeach
                </select>
                <small class="text-danger">{{ $errors->first('category') }}</small>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-4 control-label" for="product_id">Product</label>
            <div class="col-md-6">
                <select class="form-control select2search" id="product_id" name="product_id">
                    <option value="">Select Product</option>
                </select>
                <small class="text-danger">{{ $errors->first('product') }}</small>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-4 control-label" for="unit">Unit</label>
            <div class="col-md-6">
                <select class="form-control select2search" id="unit_id" name="unit_id" required>
                    <option value="">Select Unit</option>
                    @foreach($units as $unit)
                    <option value="{{$unit->id}}">{{$unit->unit}}</option>
                    @endforeach
                </select>
                <small class="text-danger">{{ $errors->first('unit_id') }}</small>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-4 control-label" for="unit">Unit Size</label>
            <div class="col-md-6">
                <select class="form-control select2search" id="unit_size" name="unit_size" required>
                    <option value="">Select Unit Size</option>
                    
                </select>
                <small class="text-danger">{{ $errors->first('size') }}</small>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-4 control-label" for="commission">Commission</label>
            <div class="col-md-6">
                <input type="text" class="form-control" id="commission" name="commission" required>
                <small class="text-danger">{{ $errors->first('rent') }}</small>
            </div>
        </div>
        <div class="col-md-8 col-md-offset-4">
            <button type="button" class="btn btn-info" id="resetbtn">Reset</button>
            <button type="submit" class="btn btn-primary">Save</button>
        </div>
        {!! Form::close() !!}
    </div>
    {!! Form::close() !!}
</div>
<script type="text/javascript">
    $(document).on("change", "#party_type_id", function() {
        var _partyType = $("#party_type_id").val();
        $.ajax({
            url: "{{ URL::to('party_commission/partyListByType') }}",
            type: "post",
            data: {
                _token: csrfToken,
                partyTypeId: _partyType
            },
            success: function(res) {
                $('#party_id').html(res);
            },
            error: function(xhr, status) {
                alert('There is some error.Try after some time.');
            }
        });

    });
    $(document).on("change", "#category_id", function() {
        var _product_id = $("#category_id").val();
        $.ajax({
            url: "{{ URL::to('party_commission/productByCategory') }}",
            type: "post",
            data: {
                _token: csrfToken,
                product_id: _product_id
            },
            success: function(res) {
                $('#product_id').html(res);
            },
            error: function(xhr, status) {
                alert('There is some error.Try after some time.');
            }
        });

    });
    $(document).on("change", "#unit_id", function() {
        var _unit_id = $("#unit_id").val();
        $.ajax({
            url: "{{ URL::to('party_commission/unitSizeByUnit') }}",
            type: "post",
            data: {
                _token: csrfToken,
                unit_id: _unit_id
            },
            success: function(res) {
                $('#unit_size').html(res);
            },
            error: function(xhr, status) {
                alert('There is some error.Try after some time.');
            }
        });

    });
</script>
@endsection