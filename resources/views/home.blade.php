@extends('layouts.app')

@section('content')
<div id="breadcrumbBar" class="breadcrumb site_nav_links no_bdr_rad clearfix">
    <div class="col-md-3 col-sm-3 col-xs-2 cxs_2 no_pad">
        <button class="btn btn-info btn-xs" type="button" onclick="history.back()" title="Go Back"><span class="visible-xs"><i class="fa fa-arrow-left"></i></span><span class="hidden-xs">Back</span></button>
        <button class="btn btn-info btn-xs" onclick="redirectTo('<?= url('view-clear') ?>')" title="Refresh" type="button"><span class="visible-xs"><i class="fa fa-refresh"></i></span><span class="hidden-xs">Refresh</span></button>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-6 cxs_10 text-center">
        <h2 class="page-title">Dashboard</h2>
    </div>
    <div class="col-md-3 col-sm-3 col-xs-4 cxs_12 no_pad">
        <ul class="text-right no_mrgn">
            <li>Dashboard</li>
        </ul>
    </div>
</div>

<div class="row clearfix">
    <div class="col-lg-3 col-md-6">
        <div class="form-group clearfix">
            <button class="grow_box"><i class="fa fa-money fa-3x"></i><a href="#">Setting</a></button>
        </div>
    </div>
    <div class="col-lg-3 col-md-6">
        <div class="form-group clearfix">
            <button class="grow_box"><i class="fa fa-credit-card fa-3x"></i><a href="#">Product store</a></button>
        </div>
    </div>
    <div class="col-lg-3 col-md-6">
        <div class="form-group clearfix">
            <button class="grow_box"><i class="fa fa-handshake-o fa-3x"></i><a href="#">Delivery</a></button>
        </div>
    </div>
    <div class="col-lg-3 col-md-6">
        <div class="form-group clearfix">
            <button class="grow_box"><i class="fa fa-sort-amount-asc fa-3x"></i><a href="#">Loan</a></button>
        </div>
    </div>
</div>
<div class="row clearfix">


    <div class="col-lg-3 col-md-6">
        <div class="form-group clearfix">
            <button class="grow_box"><i class="fa fa-line-chart fa-3x"></i><a href="#">Accounts</a></button>
        </div>
    </div>
    <div class="col-lg-3 col-md-6">
        <div class="form-group clearfix">
            <button class="grow_box"><i class="fa fa-list-alt fa-3x"></i><a href="#">HR</a></button>
        </div>
    </div>
    <div class="col-lg-3 col-md-6">
        <div class="form-group clearfix">
            <button class="grow_box"><i class="fa fa-list-alt fa-3x"></i><a href="#">Inventory</a></button>
        </div>
    </div>
    <div class="col-lg-3 col-md-6">
        <div class="form-group clearfix">
            <button class="grow_box"><i class="fa fa-list-alt fa-3x"></i><a href="#">Others</a></button>
        </div>
    </div>
    <div class="col-lg-3 col-md-6">
        <div class="form-group clearfix">
            <button class="grow_box"><i class="fa fa-list-alt fa-3x"></i><a href="#">Store Location</a></button>
        </div>
    </div>
</div>
@endsection