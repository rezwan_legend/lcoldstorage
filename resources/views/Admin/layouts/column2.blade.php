<?php
$_ck = isset($_REQUEST['k']) ? $_REQUEST['k'] : null;
$_route_file = request()->segment(1);
//$_bd_class = body_class();
//pr($_ck, false);
?>
@include('admin.layouts.header')

<div id="body_panel" style="">
    <div class="clearfix">
        <div class="clearfix" id="sidebar_area">
            <?php $_sidebar_file = "widgets.sidebar_{$_route_file}"; ?>
            @include($_sidebar_file, ['ck' => $_ck])
        </div>
        <div class="" id="content_area">
            @if ($_bd_class == "column2")
            <div class="row clearfix">
                @include('admin.layouts.notification')
            </div>
            @endif

            @yield('content')
        </div>
    </div>
</div>

@include('admin.layouts.footer')