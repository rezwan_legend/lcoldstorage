<?php

use App\Models\GeneralSetting;
use App\Models\Institute;
use App\Models\User;

$_user_companies = User::get_user_company_permisstion(Auth::id());
$comID = [];
if (!isLegend()) {
    if (!empty($_user_companies) && count($_user_companies) > 0) {
        foreach ($_user_companies as $_key => $_val) {
            $comID[] = $_val;
        }
    }
}
$_companies = Institute::model()->getList($comID);

$setting = GeneralSetting::get_setting();
$title = !empty($setting) ? $setting->title : 'Cold Storage';
$_authKey = Auth::user()->_key;
//$jsData['cid'] = Auth::user()->institute->id;
//$jsData['name'] = Auth::user()->institute->name;
//$jsData['rname'] = strtolower(Auth::user()->institute->route_name);
?>
<!DOCTYPE html>
<html lang="{{ App::getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta name="base-url" content="{{ url('/') }}">
        <meta name="refresh-url" content="{{ url('/refresh_page') }}">

        <title>{{ $title }}</title>

        <link href="{{ asset('public/img/favicon.ico') }}" rel="icon" type="image/x-icon">
        <link href="{{ asset('public/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ asset('public/css/select2.min.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ asset('public/css/bootstrap.min.css') }}" rel="stylesheet">
        <link href="{{ asset('public/css/bootstrap-datepicker.min.css') }}" rel="stylesheet">
        <link href="{{ asset('public/css/style.css') }}" rel="stylesheet" type="text/css" media="all">
        <link href="{{ asset('public/css/responsive.css') }}" rel="stylesheet" type="text/css" media="screen">
        <link href="{{ asset('public/css/print.css') }}" rel="stylesheet" type="text/css" media="print">
        @yield('extra_css_file')
        <style type="text/css">
            @yield('page_style')
        </style>

        <script src="{{ asset('public/js/jquery-1.12.2.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('public/js/select2.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('public/js/bootstrap.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('public/js/bootstrap-datepicker.min.js') }}"></script>
        <script src="{{ asset('public/js/custom.js') }}" type="text/javascript"></script>
        @yield('extra_js_file')
        <script>window.Laravel = {!! json_encode(['csrfToken' => csrf_token()]) !!}</script>
    </head>
    <body onload="setToCurrentState()">
        <div class="navbar-inverse" id="topBar" style="background-color:#6bb100;">
            <div class="container-fluid">
                <a class="pull-left" href="{{ url('/refresh_page') }}" title="Refresh Page" style="color: #FFFFFF;display: inline-block;"><i class="fa fa-refresh"></i></a>
                <a class="pull-left" href="javascript:void(0)" title="Print" onclick="printDiv('print_area')" style="color: #FFFFFF;display: inline-block;margin-left: 10px;"><i class="fa fa-print"></i></a>
                <a id="top_link" href="{{ url('/home') }}" onclick="updateLocalStorage(this)" style="color:#FFFFFF;text-decoration:none;"><?= $title; ?></a>
                <span id="checkbox_counter"></span>
            </div>
        </div>

        <nav class="navbar navbar-inverse navbar-fixed-top" id="topnav_bar" role="navigation" style="">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle pull-left" id="toggle_leftnav">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#app_nav_collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <h2 class="page-title text-center visible-xs" style="color:#FFFFFF;margin:8px 0;"><?php echo!empty($breadcrumb_title) ? $breadcrumb_title : ''; ?></h2>
                </div>

                <div class="collapse navbar-collapse" id="app_nav_collapse">
                    <ul class="nav navbar-nav navbar-right topnav">
                        @if (Auth::guest())
                        <li><a href="{{ route('login') }}">Login</a></li>
                        @else
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button">
                                <i class="fa fa-user"></i> Hi, {{ limit_text_length(Auth::user()->name, 10) }} <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li class="{{ request()->is('home') ? 'active' : '' }}">
                                    <a href="{{ url('/home') }}"><i class="fa fa-fw fa-dashboard"></i> Dashboard</a>
                                </li>
                                <li class="<?php if (request()->is("user/{$_authKey}")) echo 'active'; ?>">
                                    <a href="{{ url('user') }}/{{ $_authKey }}"><i class="fa fa-fw fa-user"></i> Profile</a>
                                </li>
                                <li class="{{ request()->is('password') ? 'active' : '' }}">
                                    <a href="{{ url('password') }}"><i class="fa fa-fw fa-lock"></i> Change Password</a>
                                </li>
                                <li class="divider hidden-xs"></li>
                                <li>
                                    <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><i class="fa fa-fw fa-power-off"></i> Logout</a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </li>
                            </ul>
                        </li>
                        @endif
                    </ul>
                </div>
            </div>
        </nav>

        @yield('breadcrumbs')
