{{ print_header("Daily Report (" . date_dmy($date) . ")") }}

<table class="table table-bordered tbl_thin" style="margin: 0;">
    <tr class="bg-warning">
        <td><strong>Opening Balance :</strong> {{ $opening_balance }}&nbsp;TK</td>
    </tr>
</table>

<div class="clearfix">               
    <div class="col-md-6 mp_50" style="padding:0;">
        <div class="table-responsive">
            <table class="table table-bordered tbl_thin" style="margin: 0;">
                <tr class="bg-info">
                    <th class="text-center" colspan="5">Receives</th>
                </tr>
                <tr class="bg_gray">
                    <th class="text-center" style="width:4%;">#</th>
                    <th>Customer</th>
                    <th class="" style="width:30%;">Description</th>
                    <th class="text-right" style="width:10%;">Amount</th>
                </tr>                  
                <?php
                $counter = 0;
                $total_debit = 0;
                ?>
                @foreach ($receives as $receive)
                <?php
                $counter++;
                $total_debit += $receive->debit;
                ?>
                <tr>
                    <td class="text-center">{{ $counter }}</td>
                    <td>{{ !empty($receive->from_particular) ? $receive->from_particular->name : $receive->from_subhead->name }} </td>
                    <td class="">{{ $receive->description }}</td>
                    <td class="text-right">{{ $receive->credit }}</td>
                </tr>    
                @endforeach
                <tr class="bg-info">
                    <th class="text-right" colspan="3">Total</th>
                    <th class="text-right">{{ $total_debit }}</th>
                </tr>
            </table>
        </div>
    </div>
    <div class="col-md-6 pull-right mp_50" style="padding:0;">
        <div class="table-responsive">
            <table class="table table-bordered tbl_thin" style="margin: 0;">
                <tr class="bg-info">
                    <th class="text-center" colspan="5">Payments</th>
                </tr>
                <tr class="bg_gray">
                    <th class="text-center" style="width:4%;">#</th>
                    <th>Account</th>
                    <th class="" style="width:30%;">Description</th>
                    <th class="text-right" style="width:10%;">Amount</th>
                </tr>  
                <?php
                $counter_pay = 0;
                $total_credit = 0;
                ?>
                @foreach ($payments as $payment)
                <?php
                $counter_pay++;
                $total_credit += $payment->credit;
                ?>
                <tr>
                    <td class="text-center">{{ $counter_pay }}</td>
                    <td>{{ !empty($payment->to_particular) ? $payment->to_particular->name : $payment->to_subhead->name }} </td>
                    <td class="">{{ $payment->description }}</td>
                    <td class="text-right">{{ $payment->credit }}</td>
                </tr>   
                @endforeach
                <tr class="bg-info">
                    <th class="text-right" colspan="3">Total</th>
                    <th class="text-right">{{ $total_credit }}</th>
                </tr>
            </table>
        </div>
    </div>
</div>
<?php $closing_balance = ($opening_balance + $total_debit) - $total_credit; ?>
<div class="clearfix" style="margin-top: 20px;">
    <table class="table table-bordered tbl_thin" style="margin: 0;">
        <tr class="bg-warning">
            <th class="text-left">Total Receives : {{ $total_debit }} TK</th>
            <th class="text-center">Total Payments : {{ $total_credit }} TK</th>
            <th class="text-right">Closing Balance : {{ $closing_balance }} TK</th>
        </tr>
    </table>
</div>
