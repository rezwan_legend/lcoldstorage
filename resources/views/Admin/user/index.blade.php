@extends('layouts.app')

@section('content')
<div id="breadcrumbBar" class="breadcrumb site_nav_links no_bdr_rad clearfix">
    <div class="col-md-3 col-sm-3 col-xs-2 cxs_2 no_pad">
        <button class="btn btn-info btn-xs" type="button" onclick="history.back()" title="Go Back"><span class="visible-xs"><i class="fa fa-arrow-left"></i></span><span class="hidden-xs">Back</span></button>
        <button class="btn btn-info btn-xs" onclick="redirectTo('<?= url('view-clear') ?>')" title="Refresh" type="button"><span class="visible-xs"><i class="fa fa-refresh"></i></span><span class="hidden-xs">Refresh</span></button>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-6 cxs_10 text-center">
        <h2 class="page-title">{{ $page_title }}</h2>
    </div>
    <div class="col-md-3 col-sm-3 col-xs-4 cxs_12 no_pad">
        <ul class="text-right no_mrgn">
            <li><a href="{{ url('/home') }}">Dashboard</a> <span class="fa fa-angle-right"></span></li>
            <li>User</li>
        </ul>                         
    </div>
</div>

<div class="well">
    <table width="100%">
        <tr>
            <td class="wmd_70">
                {!! Form::open(['method' => 'POST', 'id' => 'frmSearch', 'name' => 'frmSearch']) !!} 
                <div class="input-group">
                    <div class="input-group-btn clearfix">
                        <?php echo number_dropdown(50, 550, 50, null, 'xsw_30') ?>
                        <div class="col-md-2 col-sm-3 no_pad xsw_70">
                            <input type="text" name="search" id="srch" class="form-control" placeholder="search name or email" size="30"/>
                        </div>
                        <div class="col-md-2 col-sm-3 no_pad xsw_50">
                            <select id="sortBy" class="form-control" name="sort_by">
                                <option value="name" selected style="text-transform:capitalize">User Name</option>
                                <option value="email" style="text-transform:capitalize">Email</option>                        
                            </select>
                        </div>
                        <div class="col-md-2 col-sm-3 no_pad xsw_50">
                            <select id="sortType" class="form-control" name="sort_type">
                                <option value="ASC" selected>Ascending</option>
                                <option value="DESC">Descending</option>
                            </select>
                        </div>
                        <div class="col-md-2 col-sm-3 no_pad xsw_100" style="width: auto">
                            <button type="button" id="search" class="btn btn-info xsw_50">Search</button>
                            <button type="button" id="clear_from" class="btn btn-warning xsw_50">Clear</button>
                        </div>
                    </div>
                </div>
                {!! Form::close() !!}
            </td>
            <td class="text-right wmd_30 xsw_100" style="">
                @if (has_user_access('user_create'))
                <a class="btn btn-success btn-xs xsw_33" href="{{ url ('/register') }}"><i class="fa fa-plus"></i> New</a>
                @endif
                @if (has_user_access('user_delete'))
                <button type="button" class="btn btn-danger btn-xs xsw_33" id="Del_btn" disabled><i class="fa fa-trash-o"></i> Delete</button>
                @endif
            </td>
        </tr>
    </table>
</div>

<div class="clearfix">
    {!! Form::open(['method' => 'POST', 'id' => 'frmList', 'name' => 'frmList']) !!} 
    <div id="ajax_content">
        @if (!empty($dataset) && count($dataset) > 0)
        <div class="table-responsive">
            <table class="table table-bordered tbl_thin" id="check">
                <tr class="bg-info">
                    <th class="text-center" style="width: 3%;">#</th>
                    <th>Name</th>
                    <th class="text-center">Type</th>
                    <th>Email</th>
                    <th class="text-center">Status</th>
                    <th class="text-center">Loggedin</th>
                    <th class="text-center" style="width: 25%;">Actions</th>
                    <th class="text-center" style="width: 3%;"><input type="checkbox" id="check_all" value="all"></th>
                </tr>
                <?php $sl = 0; ?>
                @foreach ($dataset as $data)
                <?php $sl++; ?>
                <tr onmouseover="change_color(this, true)" onmouseout="change_color(this, false)">
                    <td class="text-center">{{ $sl }}</td>
                    <td>{{ $data->name }}</td>
                    <td class="text-center">{{ $data->user_type }}</td>
                    <td>{{ $data->email }}</td>
                    <td class="text-center">
                        @if ($data->status == 0)
                        <span class="label label-danger">Inactive</span>
                        @else
                        <span class="label label-success">Active</span>
                        @endif
                    </td>
                    <td class="text-center">{{ ($data->is_loggedin == 1) ? "Yes" : "No" }}</td>
                    <td class="text-center">
                        @if (has_user_access('user_edit'))
                        <a class="btn btn-info btn-xs" href="{{ url('/user/'.$data->_key.'/admin_edit') }}">Edit</a>
                        @endif
                        @if (has_user_access('user_access_control'))
                        <a class="btn btn-warning btn-xs" href="{{ url('/user/'.$data->_key.'/access') }}">Access Control</a>
                        @endif
                        @if ($data->status == 0)
                        <span title="Click To Activate User" onclick="make_user_active(<?= $data->id ?>)" class="btn btn-success btn-xs">Active</span>
                        @else
                        <span title="Click To Deactivate User" onclick="make_user_inactive(<?= $data->id ?>)" class="btn btn-danger btn-xs">Inactive</span>
                        @endif
                        <a class="btn btn-primary btn-xs" href="{{ url('/user/'.$data->_key) }}">View</a>
                    </td>
                    <td class="text-center">
                        @if (has_user_access('user_delete'))
                        <input type="checkbox" name="data[]" value="{{ $data->id }}">
                        @endif
                    </td>
                </tr>          
                @endforeach
            </table>
        </div>
        <div class="text-center hip">
            {{ $dataset->render() }}
        </div>
        @else
        <div class="alert alert-info">No records found!</div>
        @endif
    </div>
    {!! Form::close() !!}
</div>
@endsection

@section('page_script')
<script type="text/javascript">
    $(document).ready(function () {
        $(document).on("click", "#search", function () {
            var _url = "{{ URL::to('user/search') }}";
            var _form = $("#frmSearch");
            $.ajax({
                url: _url,
                type: "POST",
                data: _form.serialize(),
                success: function (data) {
                    $("#ajax_content").html(data);
                },
                error: function (xhr, status) {
                    alert('There is some error.Try after some time.');
                }
            });
        });

        $(document).on("click", "#Del_btn", function () {
            var _url = "{{ URL::to('user/delete') }}";
            var _form = $("#frmList");
            var _rc = confirm("Are you sure about this action? This cannot be undone!");
            if (_rc === true) {
                $.post(_url, _form.serialize(), function (data) {
                    if (data.success === true) {
                        $("#search").trigger("click");
                    } else {
                        $("#ajaxMessage").showAjaxMessage({html: data.message, type: "error"});
                    }
                }, "json");
            }
        });
    });

    function make_user_active(id) {
        var _url = "{{ URL::to('user') }}/" + id + "/active";
        var _rc = confirm("Are you sure about this action? This cannot be undone!");
        if (_rc === true) {
            $.get(_url, function (res) {
                if (res.success === true) {
                    window.location.reload();
                } else {
                    $("#ajaxMessage").showAjaxMessage({html: res.message, type: "error"});
                }
            }, "json");
        }
    }

    function make_user_inactive(id) {
        var _url = "{{ URL::to('user') }}/" + id + "/inactive";
        var _rc = confirm("Are you sure about this action? This cannot be undone!");
        if (_rc === true) {
            $.get(_url, function (res) {
                if (res.success === true) {
                    window.location.reload();
                } else {
                    $("#ajaxMessage").showAjaxMessage({html: res.message, type: "error"});
                }
            }, "json");
        }
    }
</script>
@endsection