@if (!empty($dataset) && count($dataset) > 0)
<div class="table-responsive">
    <table class="table table-bordered tbl_thin" id="check">
        <tr class="bg-info">
            <th class="text-center" style="width: 3%;">#</th>
            <th>Name</th>
            <th class="text-center">Type</th>
            <th>Email</th>
            <th class="text-center">Status</th>
            <th class="text-center">Loggedin</th>
            <th class="text-center" style="width: 25%;">Actions</th>
            <th class="text-center" style="width: 3%;"><input type="checkbox" id="check_all" value="all"></th>
        </tr>
        <?php $sl = 0; ?>
        @foreach ($dataset as $data)
        <?php $sl++; ?>
        <tr onmouseover="change_color(this, true)" onmouseout="change_color(this, false)">
            <td class="text-center">{{ $sl }}</td>
            <td>{{ $data->name }}</td>
            <td class="text-center">{{ $data->user_type }}</td>
            <td>{{ $data->email }}</td>
            <td class="text-center">
                @if ($data->status == 0)
                <span class="label label-danger">Inactive</span>
                @else
                <span class="label label-success">Active</span>
                @endif
            </td>
            <td class="text-center">{{ ($data->is_loggedin == 1) ? "Yes" : "No" }}</td>
            <td class="text-center">
                @if (has_user_access('user_edit'))
                <a class="btn btn-info btn-xs" href="{{ url('/user/'.$data->_key.'/admin_edit') }}">Edit</a>
                @endif
                @if (has_user_access('user_access_control'))
                <a class="btn btn-warning btn-xs" href="{{ url('/user/'.$data->_key.'/access') }}">Access Control</a>
                @endif
                @if ($data->status == 0)
                <span title="Click To Activate User" onclick="make_user_active(<?= $data->id ?>)" class="btn btn-success btn-xs">Active</span>
                @else
                <span title="Click To Deactivate User" onclick="make_user_inactive(<?= $data->id ?>)" class="btn btn-danger btn-xs">Inactive</span>
                @endif
                <a class="btn btn-primary btn-xs" href="{{ url('/user/'.$data->_key) }}">View</a>
            </td>
            <td class="text-center">
                @if (has_user_access('user_delete'))
                <input type="checkbox" name="data[]" value="{{ $data->id }}">
                @endif
            </td>
        </tr>          
        @endforeach
    </table>
</div>
<div class="text-center hip" id="apaginate">
    {{ $dataset->render() }}
</div>
@else
<div class="alert alert-info">No records found!</div>
@endif