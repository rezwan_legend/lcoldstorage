@extends('layouts.app')

@section('content')
<div id="breadcrumbBar" class="breadcrumb site_nav_links no_bdr_rad clearfix">
    <div class="col-md-3 col-sm-3 col-xs-2 cxs_2 no_pad">
        <button class="btn btn-info btn-xs" type="button" onclick="history.back()" title="Go Back"><span class="visible-xs"><i class="fa fa-arrow-left"></i></span><span class="hidden-xs">Back</span></button>
        <button class="btn btn-info btn-xs" onclick="redirectTo('<?= url('view-clear') ?>')" title="Refresh" type="button"><span class="visible-xs"><i class="fa fa-refresh"></i></span><span class="hidden-xs">Refresh</span></button>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-6 cxs_10 text-center">
        <h2 class="page-title">{{ $page_title }}</h2>
    </div>
    <div class="col-md-3 col-sm-3 col-xs-4 cxs_12 no_pad">
        <ul class="text-right no_mrgn">
            <li><a href="{{ url('/home') }}">Dashboard</a> <span class="fa fa-angle-right"></span></li>
            <li><a href="{{ url('/user') }}">User</a> <span class="fa fa-angle-right"></span></li>
            <li>Form</li>
        </ul>                             
    </div>
</div>

<div class="panel panel-default">
    <div class="panel-heading">Update {{ $page_title }}</div>
    <div class="panel-body">
        {!! Form::open(['method' => 'PUT', 'url' => 'user/'.$user->id, 'enctype' => 'multipart/form-data']) !!} 
        <div class="row clearfix">
            <div class="col-md-2 col-sm-3">
                <div class="thumbnail">
                    <img alt="{{ $user->name }}" class="img-responsive" id="user_avatar" src="{{ user_avatar($user) }}" style="max-height: 170px">
                </div>
                <div class="clearfix">
                    <label for="file">Picture</label>
                    <input type="file" class="form-control" id="file" name="file" onchange="readURL(this, 'user_avatar')">
                </div>
            </div>
            <div class="col-md-3 col-sm-4">
                @if(isAdminUser())
                <div class="form-group clearfix">
                    <label for="user_type">User Type</label>
                    <select class="form-control" id="user_type" name="user_type" required>
                        <option value="">Select</option>
                        <option value="{{ USER_TYPE_ADMIN }}" @if($user->user_type == USER_TYPE_ADMIN) {{ ' selected' }} @endif>{{ USER_TYPE_ADMIN }}</option>
                        <option value="{{ USER_TYPE_DEPO }}" @if($user->user_type == USER_TYPE_DEPO) {{ ' selected' }} @endif>{{ USER_TYPE_DEPO }}</option>
                        <option value="{{ USER_TYPE_DSM }}" @if($user->user_type == USER_TYPE_DSM) {{ ' selected' }} @endif>{{ USER_TYPE_DSM }}</option>
                        <option value="{{ USER_TYPE_ZM }}" @if($user->user_type == USER_TYPE_ZM) {{ ' selected' }} @endif>{{ USER_TYPE_ZM }}</option>
                        <option value="{{ USER_TYPE_SR }}" @if($user->user_type == USER_TYPE_SR) {{ ' selected' }} @endif>{{ USER_TYPE_SR }}</option>
                        <option value="{{ USER_TYPE_DEALER }}" @if($user->user_type == USER_TYPE_DEALER) {{ ' selected' }} @endif>{{ USER_TYPE_DEALER }}</option>
                    </select>
                </div>
                @endif
                <div class="form-group clearfix">
                    <label for="name">User Name</label>
                    <input type="text" class="form-control" id="name" name="name" value="{{ $user->name }}" required>
                    <small class="text-danger">{{ $errors->first('name') }}</small>
                </div>
                <div class="form-group clearfix">
                    <label for="email">Email Address</label>
                    <input type="email" class="form-control" id="email" name="email" value="{{ $user->email }}" required <?php if (!isAdminUser()) echo ' readonly'; ?>>
                    <small class="text-danger">{{ $errors->first('email') }}</small>
                </div>
                <div class="form-group clearfix">
                    <label for="language">Language</label>
                    <select class="form-control" id="language" name="locale">
                        <option value="">Select</option>
                        <option value="bn" @if($user->locale == 'bn') {{ ' selected' }} @endif>Bangla</option>
                        <option value="en" @if($user->locale == 'en') {{ ' selected' }} @endif>English</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="clearfix">
            <div class="text-center">
                <button type="submit" class="btn btn-primary">Update</button>
            </div>
        </div>
        {!! Form::close() !!} 
    </div>
</div>
@endsection