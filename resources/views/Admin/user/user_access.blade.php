@extends('layouts.app')

@section('page_style')
#frmUserAccessControl .panel-default{margin-bottom: 0;}
#frmUserAccessControl .panel-heading{padding: 6px 10px;}
@endsection

<?php
//$access_items = user_access_items();
$active_access_items = $modelUser->get_user_permisstion_by_id($user->id);
$active_access_item_list = !empty($active_access_items) ? $active_access_items : [];
$_allModules = permission_modules();
?>
@section('content')
<div id="breadcrumbBar" class="breadcrumb site_nav_links no_bdr_rad clearfix">
    <div class="col-md-3 col-sm-3 col-xs-2 cxs_2 no_pad">
        <button class="btn btn-info btn-xs" type="button" onclick="history.back()" title="Go Back"><span class="visible-xs"><i class="fa fa-arrow-left"></i></span><span class="hidden-xs">Back</span></button>
        <button class="btn btn-info btn-xs" onclick="redirectTo('<?= url('view-clear') ?>')" title="Refresh" type="button"><span class="visible-xs"><i class="fa fa-refresh"></i></span><span class="hidden-xs">Refresh</span></button>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-6 cxs_10 text-center">
        <h2 class="page-title">{{ $page_title }}</h2>
    </div>
    <div class="col-md-3 col-sm-3 col-xs-4 cxs_12 no_pad">
        <ul class="text-right no_mrgn">
            <li><a href="{{ url('/home') }}">Dashboard</a> <span class="fa fa-angle-right"></span></li>
            <li><a href="{{ url('/user') }}">User</a> <span class="fa fa-angle-right"></span></li>
            <li>Access Items</li>
        </ul>                            
    </div>
</div>

<div class="clearfix">
    <div class="panel panel-info no_bdr_rad" style="margin: 0">
        <div class="panel-heading no_bdr_rad">

            <h3 class="panel-title text-center">
                Access Items Of <strong>{{ $user->name }}</strong> <?= ($user->user_type != USER_TYPE_ADMIN) ? "[{$user->user_type}]" : "[Admin]"; ?>
                <label class="pull-right no_mrgn va_mid" style="cursor: pointer">
                    <input type="checkbox" class="checkbox-inline no_mrgn" id="check_all" value="all"> Check All
                </label>
            </h3>
        </div>
    </div>
</div>

<div class="clearfix">
    {!! Form::open(['method' => 'POST', 'url' => 'user/acess/update', 'id' => 'frmUserAccessControl']) !!}
    <input type="hidden" name="user_id" value="{{ $user->id }}">
    @foreach ($_allModules as $_mkey => $_moduleName)
    <div class="panel panel-default no_bdr_rad">
        <div class="panel-heading no_bdr_rad">
            <h3 class="panel-title">
                <label style="cursor: pointer;margin: 0;">
                    <input type="checkbox" class="module_heading no_mrgn" id="heading_{{$_mkey}}" name="access[{{$_mkey}}]" value="{{ $_moduleName }}" data-rel="{{$_mkey}}" @if(array_key_exists($_mkey, $active_access_item_list)) {{ ' checked' }} @endif></span>
                    {{ $_moduleName }}
                </label>
            </h3>
        </div>
        <div class="panel-body no_pad {{$_mkey}}">
            @foreach ($_mkey() as $_key => $_item)
            <li class="list-group-item access_item" style="float: left;width: 25%;">
                <label>
                    <input type="checkbox" class="module_item" name="access[{{ $_key }}]" value="{{ $_item }}" data-rel="{{$_mkey}}" @if(array_key_exists($_key, $active_access_item_list)) {{ ' checked' }} @endif> {{ $_item }}              
                </label>
            </li>
            @endforeach
        </div>
    </div>
    @endforeach

    <div class="text-center" style="margin-top: 15px;">
        <div class="form-group">
            <button type="submit" class="btn btn-primary">Save</button>
        </div>
    </div>
    {!! Form::close() !!}
</div>
@endsection

@section('page_script')
<script type="text/javascript">
    $(document).ready(function () {
        if ($("#access_list input[type='checkbox']:checked").length > 0) {
            $("#check_all").prop('checked', true);
        }

        $(document).on("change", "#check_all", function () {
            if (this.checked) {
                $("input[type='checkbox']").prop('checked', true);
            } else {
                $("input[type='checkbox']").prop('checked', false);
            }
        });

        $(document).on("change", ".module_heading", function () {
            var _div = $(this).attr('data-rel');
            if (this.checked) {
                $("." + _div + " .module_item").prop('checked', true);
            } else {
                $("." + _div + " .module_item").prop('checked', false);
            }
        });

        $(document).on("change", ".module_item", function () {
            var _div = $(this).attr('data-rel');
            if ($("." + _div + " .module_item:checked").length > 0) {
                $("#heading_" + _div).prop('checked', true);
            } else {
                $("#heading_" + _div).prop('checked', false);
            }
        });
    });
</script>
@endsection