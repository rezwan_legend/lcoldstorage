@extends('layouts.app')
@section('content')

<div id="breadcrumbBar" class="breadcrumb site_nav_links no_bdr_rad clearfix">
    <div class="col-md-3 col-sm-3 col-xs-2 cxs_2 no_pad">
        <button class="btn btn-info btn-xs" type="button" onclick="history.back()" title="Go Back"><span class="visible-xs"><i class="fa fa-arrow-left"></i></span><span class="hidden-xs">Back</span></button>
        <button class="btn btn-info btn-xs" onclick="redirectTo('<?= url('view-clear') ?>')" title="Refresh" type="button"><span class="visible-xs"><i class="fa fa-refresh"></i></span><span class="hidden-xs">Refresh</span></button>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-6 cxs_10 text-center">
        <h2 class="page-title">Update Purpose </h2>
    </div>
    <div class="col-md-3 col-sm-3 col-xs-4 cxs_12 no_pad">
        <ul class="text-right no_mrgn">
            <li><a href="{{ url('/home') }}">Dashboard</a> <span class="fa fa-angle-right"></span></li>
            <li><a href="{{ url('/purpose') }}">Purpose</a> <span class="fa fa-angle-right"></span></li>
            <li>Form</li>
        </ul>
    </div>
</div>

<div class="panel panel-default">
    <div class="panel-heading">Purpose Information</div>
    <div class="panel-body">
        {!! Form::open(['method' => 'PUT', 'url' => 'purpose/'.$data->id, 'id' => 'frmPurpose'  , 'class' => 'form-horizontal']) !!} 
        <div class="form-group">
            <label for="name" class="col-md-4 control-label">Name</label>
            <div class="col-md-6">
                <input id="name" type="text" class="form-control" name="name" value="{{ $data->name }}" required>
                <small class="text-danger">{{ $errors->first('name') }}</small>
            </div>
        </div> 
        <div class="form-group">
            <label for="code" class="col-md-4 control-label">Code</label>
            <div class="col-md-6">
                <input type="text" id="code" class="form-control" name="code" value="{{ $data->code }}">
                <small class="text-danger">{{ $errors->first('code') }}</small>
            </div>
        </div> 
        <div class="form-group">
            <label for="description" class="col-md-4 control-label">Description</label>
            <div class="col-md-6">                    
                <textarea id="description" class="form-control" name="description">{{ $data->description }}</textarea>
                <small class="text-danger">{{ $errors->first('description') }}</small>
            </div>
        </div> 
        <div class="clearfix">
            <div class="col-md-8 col-md-offset-4">
                <button type="button" class="btn btn-info xsw_50" id="reset_from">Reset</button>
                <input type="submit" class="btn btn-primary xsw_50" name="btnSave" value="Update">
            </div>
        </div>
        {!! Form::close() !!} 
    </div>
</div>
@endsection