@if (!empty($dataset) && count($dataset) > 0)
<div class="table-responsive">
    <table class="table table-bordered tbl_thin" id="check">
        <tbody>
            <tr class="bg-info" id="r_checkAll">
                <th class="text-center" style="width: 3%;">#</th>
                <th>Name</th>
                <th>Code</th>
                <th>Description</th>
                <th class="text-center hip" style="width: 10%;">Actions</th>
                <th class="text-center hip" style="width: 3%;"><input type="checkbox" id="check_all"value="all"></th>
            </tr>
            <?php
            $counter = 0;
            if (isset($_GET['page']) && $_GET['page'] > 1) {
                $counter = ($_GET['page'] - 1) * $dataset->perPage();
            }
            ?>
            @foreach ($dataset as $data)
            <?php $counter++; ?>   
            <tr onmouseover="change_color(this, true)" onmouseout="change_color(this, false)">
                <td class="text-center">{{ $counter }}</td>
                <td>{{ $data->name }}</td>
                <td>{{ $data->code }}</td>
                <td>{{ $data->description }}</td>
                <td class="text-center hip">
                    <a class="btn btn-info btn-xs" href="{{ url('/purpose/'.$data->id.'/edit') }}"><i class="fa fa-edit"></i> Edit</a>
                </td>
                <td class="text-center hip">
                    <input type="checkbox" name="data[]" value="{{ $data->id }}">
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    <div class="text-center hip">
        {{ $dataset->render() }}
    </div>
</div>
@else
<div class = "alert alert-info">No records found!</div>
@endif