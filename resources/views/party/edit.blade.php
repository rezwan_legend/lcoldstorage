@extends('layouts.app')

@section('breadcrumbs')
<div id="breadcrumbBar" class="breadcrumb site_nav_links no_bdr_rad clearfix">
    <div class="col-md-3 col-sm-3 col-xs-2 no_pad">
        <button class="btn btn-info btn-xs" type="button" onclick="history.back()" title="Go Back"><span class="visible-xs"><i class="fa fa-arrow-left"></i></span><span class="hidden-xs">Back</span></button>
        <button class="btn btn-info btn-xs" onclick="redirectTo('<?= url('party/' . $data->id . '/edit'); ?>')" title="Refresh" type="button"><span class="visible-xs"><i class="fa fa-refresh"></i></span><span class="hidden-xs">Refresh</span></button>
    </div>
    <div class="col-md-6 col-sm-6 hidden-xs text-center">
        <h2 class="page-title">{{$page_title}}</h2>
    </div>
    <div class="col-md-3 col-sm-3 col-xs-10 no_pad">
        <ul class="text-right no_mrgn no_pad">
            <li><a href="{{ url('/home') }}">Home</a> <i class="fa fa-angle-right"></i></li>
            <li><a href="{{ url('/party') }}">Party</a> <i class="fa fa-angle-right"></i></li>
            <li>Form</li>
        </ul>
    </div>
</div>
@endsection

@section('content')
<div class="col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title">{{$page_title}}</h3>
        </div>
        <div class="panel-body">
            {!! Form::open(['method' => 'PUT', 'url' => 'party/'.$data->id, 'class' => 'form-horizontal']) !!}
            <div class="form-group">
                <label class="col-md-3 control-label" for="party_type_id">Party Type</label>
                <div class="col-md-8">
                    <select class="form-control select2search" id="party_type_id" name="party_type_id" required>
                        @foreach ($party_type as $type)
                        <option value="{{ $type->id }}" <?php if ($type->id == $data->party_type_id) echo ' selected'; ?>>{{ $type->party_type }}</option>
                        @endforeach
                    </select>
                    <small class="text-danger">{{ $errors->first('party_type_id') }}</small>
                </div>
            </div>
            <div class="form-group">
                <label for="name" class="col-md-3 control-label">Name</label>
                <div class="col-md-8">
                    <input name="name" id="name" class="form-control" value="{{ $data->name }}" required>
                    <small class="text-danger">{{ $errors->first('name') }}</small>
                </div>
            </div>
            <div class="form-group">
                <label for="mobile" class="col-md-3 control-label">Mobile</label>
                <div class="col-md-8">
                    <input type="number" name="mobile" id="mobile" class="form-control" value="{{ $data->mobile }}">
                    <small class="text-danger">{{ $errors->first('mobile') }}</small>
                </div>
            </div>
            <div class="form-group">
                <label for="code" class="col-md-3 control-label">Code</label>
                <div class="col-md-8">
                    <input name="code" id="code" class="form-control" value="{{ $data->code }}">
                    <small class="text-danger">{{ $errors->first('code') }}</small>
                </div>
            </div>
            <div class="form-group">
                <label for="name" class="col-md-3 control-label">Address</label>
                <div class="col-md-8">
                    <textarea name="address" id="address" class="form-control" rows="5" cols="5" value="{{ $data->address }}">{{ $data->address }}</textarea>
                    <small class="text-danger">{{ $errors->first('address') }}</small>
                </div>
            </div>
            <div class="col-md-8 col-md-offset-4">
                <button type="button" class="btn btn-info" onclick="history.back()" title="Go Back">Cancel</button>
                <button type="submit" class="btn btn-primary">Update</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
@endsection