@extends('layouts.app')

@section('breadcrumbs')
<div id="breadcrumbBar" class="breadcrumb site_nav_links no_bdr_rad clearfix">
    <div class="col-md-3 col-sm-3 col-xs-2 no_pad">
        <button class="btn btn-info btn-xs" type="button" onclick="history.back()" title="Go Back"><span class="visible-xs"><i class="fa fa-arrow-left"></i></span><span class="hidden-xs">Back</span></button>
        <button class="btn btn-info btn-xs" onclick="redirectTo('<?= url('view-clear'); ?>')" title="Refresh" type="button"><span class="visible-xs"><i class="fa fa-refresh"></i></span><span class="hidden-xs">Refresh</span></button>
    </div>
    <div class="col-md-6 col-sm-6 hidden-xs text-center">
        <h2 class="page-title">{{$breadcrumb_title}}</h2>
    </div>
    <div class="col-md-3 col-sm-3 col-xs-10 no_pad">
        <ul class="text-right no_mrgn no_pad">
            <li><a href="{{ url('/home') }}">Home</a> <i class="fa fa-angle-right"></i></li>
            <li><a href="{{ url('/products') }}">Product</a> <i class="fa fa-angle-right"></i></li>
            <li>Form</li>
        </ul>
    </div>
</div>
@endsection

@section('content')
<div class="panel panel-primary">
    <div class="panel-heading">
        <h3 class="panel-title">Update Product Information</h3>
    </div>
    <div class="panel-body">
        {!! Form::open(['method' => 'PUT', 'url' => 'products/'.$data->id, 'class' => 'form-horizontal']) !!}
        <input type="hidden" name="product_same" id="product_same" value="">
        <input type="hidden" name="branch_id" id="branch_id" value="{{ $data->branch_id }} ">

        <div class="form-group">
            <label class="col-md-4 control-label" for="product_type_id">Product Type</label>
            <div class="col-md-6">
                <select class="form-control select2search" id="product_type_id" name="product_type" required>
                    <option value="">Select Type</option>
                    @foreach ($types as $type)
                    <option value="{{ $type->id }}" <?php if ($type->id == $data->product_type_id) echo ' selected'; ?>>{{ $type->name }}</option>
                    @endforeach
                </select>
                <small class="text-danger">{{ $errors->first('product_type') }}</small>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-4 control-label" for="category_id">Category</label>
            <div class="col-md-6">
                <select class="form-control select2search" id="category_id" name="category_id" required>
                    <option value="">Select</option>
                    @foreach ($categories as $category)
                    <option value="{{ $category->id }}" <?php if ($category->id == $data->category_id) echo ' selected'; ?>>{{ $category->name }}</option>
                    @endforeach
                </select>
                <small class="text-danger">{{ $errors->first('category_id') }}</small>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-4 control-label" for="name">Product Name</label>
            <div class="col-md-6">
                <input type="text" class="form-control" id="name" name="name" value="{{ $data->name }}" required>
                <small class="text-danger">{{ $errors->first('name') }}</small>
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-4 control-label" for="description">Description</label>
            <div class="col-md-6">
                <textarea class="form-control" id="description" name="description" rows="3">{{ $data->description }}</textarea>
                <small class="text-danger">{{ $errors->first('description') }}</small>
            </div>
        </div>

        <div class="col-md-8 col-md-offset-4">
           <button type="button" class="btn btn-info" onclick="history.back()" title="Go Back">Cancel</button>
            <button type="submit" class="btn btn-primary">Update</button>
        </div>
        {!! Form::close() !!}
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        var _branch_id = $("#branch_id").val();
        if (_branch_id === " ") {
            $("#conditional_branch").hide();
        }

        $(document).on("change", "#company_id", function () {
            $.ajax({
                url: "{{ URL::to('institute/components') }}",
                type: "post",
                data: {institute: this.value, _token: _token},
                success: function (res) {
                    $('#product_type_id').html(res.product_types);
                },
                error: function (xhr, status) {
                    alert('There is some error.Try after some time.');
                }
            });
        });

        $(document).on("change", "#product_type_id", function () {
            var _company_id = $("#company_id").val();
            $.ajax({
                url: "{{ URL::to('category/categoryByType') }}",
                type: "post",
                data: {companyId: _company_id, productTypeId: this.value, _token: _token},
                success: function (res) {
                    $('#category_id').html(res.category);
                },
                error: function (xhr, status) {
                    alert('There is some error.Try after some time.');
                }
            });
        });
    });
</script>
@endsection