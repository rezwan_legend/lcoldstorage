@extends('layouts.app')

@section('breadcrumbs')
<div id="breadcrumbBar" class="breadcrumb site_nav_links no_bdr_rad clearfix">
    <div class="col-md-3 col-sm-3 col-xs-2 no_pad">
        <button class="btn btn-info btn-xs" type="button" onclick="history.back()" title="Go Back"><span class="visible-xs"><i class="fa fa-arrow-left"></i></span><span class="hidden-xs">Back</span></button>
        <button class="btn btn-info btn-xs" onclick="redirectTo('<?= url('view-clear'); ?>')" title="Refresh" type="button"><span class="visible-xs"><i class="fa fa-refresh"></i></span><span class="hidden-xs">Refresh</span></button>
    </div>
    <div class="col-md-6 col-sm-6 hidden-xs text-center">
        <h2 class="page-title">{{$breadcrumb_title}}</h2>
    </div>
    <div class="col-md-3 col-sm-3 col-xs-10 no_pad">
        <ul class="text-right no_mrgn no_pad">
            <li><a href="{{ url('/home') }}">Home</a> <i class="fa fa-angle-right"></i></li>
            <li><a href="{{ url('/products') }}">Product</a> <i class="fa fa-angle-right"></i></li>
            <li>Form</li>
        </ul>
    </div>
</div>
@endsection

@section('content')
<div class="panel panel-primary">
    <div class="panel-heading">
        <h3 class="panel-title">Enter Product Information</h3>
    </div>
    <div class="panel-body">
        {!! Form::open(['method' => 'POST', 'url' => 'products', 'class' => 'form-horizontal']) !!}
        <div class="form-group">
            <label class="col-md-4 control-label" for="category_id">Category</label>
            <div class="col-md-6">
                <select class="form-control select2search" id="category_id" name="category_id" required>
                    <option value="">Select Category</option>
                    @foreach($categories as $category)
                    <option value="{{ $category->id }}"> {{$category->name}} </option>
                    @endforeach
                </select>
                <small class="text-danger">{{ $errors->first('category_id') }}</small>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-4 control-label" for="name">Product Name</label>
            <div class="col-md-6">
                <input type="text" class="form-control" id="name" name="name" required>
                <small class="text-danger">{{ $errors->first('name') }}</small>
            </div>
        </div>
        <div class="form-group ">
            <label class="col-md-4 control-label" for="description">Description</label>
            <div class="col-md-6">
                <textarea class="form-control" id="description" name="description" rows="3"></textarea>
                <small class="text-danger">{{ $errors->first('description') }}</small>
            </div>
        </div>
        <div class="col-md-8 col-md-offset-4">
            <button type="button" class="btn btn-info" id="resetbtn">Reset</button>
            <button type="submit" class="btn btn-primary">Save</button>
        </div>
        {!! Form::close() !!}
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $("#conditional_branch").hide();
        $(document).on("change", "#company_id", function () {
            $.ajax({
                url: "{{ URL::to('institute/components') }}",
                type: "post",
                data: {institute: this.value, _token: _token},
                success: function (res) {
                    $('#product_type_id').html(res.product_types);
                },
                error: function (xhr, status) {
                    alert('There is some error.Try after some time.');
                }
            });
        });

        $(document).on("change", "#product_type_id", function () {
            var _company_id = $("#company_id").val();
            $.ajax({
                url: "{{ URL::to('category/categoryByType') }}",
                type: "post",
                data: {companyId: _company_id, productTypeId: this.value, _token: _token},
                success: function (res) {
                    $('#category_id').html(res.category);
                },
                error: function (xhr, status) {
                    alert('There is some error.Try after some time.');
                }
            });
        });
    });
</script>
@endsection