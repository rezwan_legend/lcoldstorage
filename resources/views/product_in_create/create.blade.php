@extends('layouts.app')

@section('breadcrumbs')
    <div id="breadcrumbBar" class="breadcrumb site_nav_links no_bdr_rad clearfix">
        <div class="col-md-3 col-sm-3 col-xs-2 no_pad">
            <button class="btn btn-info btn-xs" type="button" onclick="history.back()" title="Go Back"><span
                    class="visible-xs"><i class="fa fa-arrow-left"></i></span><span
                    class="hidden-xs">Back</span></button>
            <button class="btn btn-info btn-xs" onclick="redirectTo('<?= url('view-clear') ?>')" title="Refresh"
                type="button"><span class="visible-xs"><i class="fa fa-refresh"></i></span><span
                    class="hidden-xs">Refresh</span></button>
        </div>
        <div class="col-md-6 col-sm-6 hidden-xs text-center">
            <h2 class="page-title">{{ $page_title }}</h2>
        </div>
        <div class="col-md-3 col-sm-3 col-xs-10 no_pad">
            <ul class="text-right no_mrgn no_pad">
                <li><a href="{{ url('/home') }}">Home</a> <i class="fa fa-angle-right"></i></li>
                <li><a href="{{ url('/product_in_create') }}">Product In Create</a> <i class="fa fa-angle-right"></i></li>
                <li>Form</li>
            </ul>
        </div>
    </div>
@endsection

@section('content')
    <div class="col-md-6 col-md-offset-3 col-sm-6 col-sm-offset-3">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">{{ $page_title }}</h3>
            </div>
            <div class="panel-body">
                {!! Form::open(['method' => 'POST', 'url' => 'product_in_create/', 'class' => 'form-horizontal']) !!}



                <div class="col-md-6">
                    <div class="form-group ">
                        <label class="mb-2 fw-bold" for="date">Date <span class="text-danger">*</span></label>
                        <input type="date" name="date" id="date" class="form-control " placeholder="Date">
                        <small class="text-danger">{{ $errors->first('date') }}</small>
                    </div>

                    <div class="form-group">
                        <label class="mb-2 fw-bold" for="sr_no">SR No<span class="text-danger">*</span></label>
                        <input type="number" name="sr_no" id="sr_no" class="form-control" placeholder="SR No">
                        <small class="text-danger">{{ $errors->first('date') }}</small>
                    </div>

                    <div class="form-group">
                        <label class="mb-2 fw-bold" for="booking_no">Booking No<span
                                class="text-danger">*</span></label>
                        <input type="number" name="booking_no" id="booking_no" class="form-control"
                            placeholder="Booking No">
                        <small class="text-danger">{{ $errors->first('date') }}</small>
                    </div>


                    <div class="form-group">
                        <label class="mb-2 fw-bold" for="agent">Agent <span class="text-danger">*</span></label>
                        <select name="agent" id="agent" class="form-control">
                            <option value="">-- Select Agent --</option>
                            @foreach ($agent as $agents)
                                <option value="{{ $agents->id }}">{{ $agents->name }}</option>
                            @endforeach

                        </select>
                    </div>

                    <div class="form-group">
                        <label class="mb-2 fw-bold" for="name">Customer Name <span class="text-danger">*</span></label>
                        <input type="text" name="name" id="name" class="form-control" placeholder="Name">
                        <small class="text-danger">{{ $errors->first('name') }}</small>
                    </div>


                    <div class="form-group">
                        <label class="mb-2 fw-bold" for="fname">Father Name</label>
                        <input type="text" name="fname" id="fname" class="form-control" placeholder="Father Name">
                        <small class="text-danger">{{ $errors->first('fname') }}</small>
                    </div>

                    <div class="form-group">
                        <label class="mb-2 fw-bold" for="address">Address</label>
                        <input type="text" name="address" id="address" class="form-control" placeholder="Address">
                        <small class="text-danger">{{ $errors->first('address') }}</small>
                    </div>
                    <div class="form-group">
                        <label class="mb-2 fw-bold" for="bag_quantity">Submitted Bag Quantity</label>
                        <input type="number" name="bag_quantity" id="bag_quantity" class="form-control"
                            placeholder="Submited bag quantity">
                        <small class="text-danger">{{ $errors->first('bag_quantity') }}</small>
                    </div>

                    <div class="form-group">
                        <label class="mb-2 fw-bold" for="empty_bag">খালি বস্তা</label>
                        <input type="number" name="empty_bag" id="empty_bag" class="form-control"
                            placeholder="খালি বস্তা">
                        <small class="text-danger">{{ $errors->first('empty_bag') }}</small>
                    </div>
                    <div class="form-group ">
                        <label class="mb-2 fw-bold" for="carrying">পরিবহন</label>
                        <input type="number" name="carrying" id="carrying" class="form-control" placeholder="পরিবহন">
                        <small class="text-danger">{{ $errors->first('carrying') }}</small>
                    </div>

                    <div class="form-group ">
                        <label class="mb-2 fw-bold" for="net_weight">Net Weight</label>
                        <input type="number" name="net_weight" id="net_weight" class="form-control"
                            placeholder="Net Weight">
                        <small class="text-danger">{{ $errors->first('net_weight') }}</small>
                    </div>
                    <div class="form-group ">
                        <label class="mb-2 fw-bold" for="mobile">Mobile</label>
                        <input type="number" name="mobile" id="mobile" class="form-control" placeholder="Mobile ">
                        <small class="text-danger">{{ $errors->first('mobile') }}</small>
                    </div>
                    <div class="form-group ">
                        <label class="mb-2 fw-bold" for="remarks">Remarks</label>

                        <textarea id="remarks" name="remarks" rows="1" cols="50" class="form-control"></textarea>
                        <small class="text-danger">{{ $errors->first('remarks') }}</small>

                    </div>
                </div>
                <div class="col-md-6">



                    <div class="form-group">
                        <label class="mb-2 fw-bold" for="agent_type">Agent Type<span
                                class="text-danger">*</span></label>
                        <input type="text" readonly name="agent_type" id="agent_type" class="form-control">

                    </div>

                    <div class="form-group">
                        <label class="mb-2 fw-bold" for="agent_name">Agent Name <span
                                class="text-danger">*</span></label>
                        <input type="text" name="agent_name" id="agent_name" class="form-control"
                            placeholder="Agent Name" readonly>
                        <small class="text-danger">{{ $errors->first('agent_name') }}</small>
                    </div>


                    <div class="form-group">
                        <label class="mb-2 fw-bold" for="father_name">Father Name</label>
                        <input type="text" readonly name="father_name" id="father_name" class="form-control"
                            placeholder="Father Name">
                        <small class="text-danger">{{ $errors->first('father_name') }}</small>
                    </div>

                    <div class="form-group">
                        <label class="mb-2 fw-bold" for="address2">Address</label>
                        <input type="text" readonly name="address2" id="address2" class="form-control" placeholder="Address">
                        <small class="text-danger">{{ $errors->first('address2') }}</small>
                    </div>
                    <div class="form-group ">
                        <label class="mb-2 fw-bold" for="lot_number">Lot Number</label>
                        <input type="text" readonly name="lot_number" id="lot_number" class="form-control"
                            placeholder="lot_number">
                        <small class="text-danger">{{ $errors->first('lot_number') }}</small>
                    </div>
                     <div class="form-group">
                        <label class="mb-2 fw-bold" for="category">Category <span class="text-danger">*</span></label>
                        <select name="category" id="category" class="form-control">
                            <option value="">-- Select category --</option>
                            @foreach ($category as $categorys)
                                <option value="{{ $categorys->id }}">{{ $categorys->name }}</option>
                            @endforeach
                        <small class="text-danger">{{ $errors->first('category') }}</small>
                        </select>
                    </div>
                     <div class="form-group">
                        <label class="mb-2 fw-bold" for="bag_type">Bag type <span class="text-danger">*</span></label>
                        <select name="bag_type" id="bag_type" class="form-control">
                            <option value="">-- Select Agent --</option>
                            @foreach ($bag_type as $bag_types)
                                <option value="{{ $bag_types->id }}">{{ $bag_types->name }}</option>
                            @endforeach
                            <small class="text-danger">{{ $errors->first('bag_type') }}</small>
                        </select>
                    </div>
                    <div class="form-group ">
                        <label class="mb-2 fw-bold" for="bag_rent">Bag Rent</label>
                        <input type="number" name="bag_rent" id="bag_rent" class="form-control"
                            placeholder="Bag rent">
                        <small class="text-danger">{{ $errors->first('bag_rent') }}</small>
                    </div>
                    <div class="form-group ">
                        <label class="mb-2 fw-bold" for="total_rent">Total Rent</label>
                        <input type="number" readonly name="total_rent" id="total_rent" class="form-control"
                            placeholder="Total rent">
                        <small class="text-danger">{{ $errors->first('total_rent') }}</small>
                    </div>
                    <div class="form-group ">
                        <label class="mb-2 fw-bold" for="advance_rent_receive">Advance Rent Receive</label>
                        <input type="number" name="advance_rent_receive" id="advance_rent_receive" class="form-control"
                            placeholder="Advance rent receive">
                        <small class="text-danger">{{ $errors->first('advance_rent_receive') }}</small>
                    </div>
                    <div class="form-group ">
                        <label class="mb-2 fw-bold" for="advance_rent_adjust">Advance Rent Adjust</label>
                        <input type="number" name="advance_rent_adjust" id="advance_rent_adjust" class="form-control"
                            placeholder="advance rent adjust">
                        <small class="text-danger">{{ $errors->first('advance_rent_adjust') }}</small>
                    </div>
                    <div class="form-group ">
                        <label class="mb-2 fw-bold" for="remain_rent">Remain Rent</label>
                        <input type="number" readonly name="remain_rent" id="remain_rent" class="form-control"
                            placeholder="Remain">
                        <small class="text-danger">{{ $errors->first('remain_rent') }}</small>
                    </div>

                </div>


                <div class="col-md-8 col-md-offset-4">
                    <button type="button" class="btn btn-info" id="resetbtn">Reset</button>
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>

    <script>
        //on change on category to product
        $(document).ready(function() {
            $(document).on("change", "#agent", function() {
                var _agent_id = $("#agent").val();
                $.ajax({
                    url: "{{ URL::to('/product_in_create/agent') }}",
                    type: "post",
                    data: {
                        _token: csrfToken,
                        AgentId: _agent_id
                    },
                    success: function(res) {
                        $("#agent_name").val(res.name);
                        $("#name").val(res.name);
                        $("#fname").val(res.father_name);
                        $("#father_name").val(res.father_name);
                        $("#address").val(res.address);
                        $("#address2").val(res.address);
                        $("#mobile").val(res.mobile);
                        $("#agent_type").val(res.agent_type);
                    },
                    error: function(xhr, status) {
                        alert('There is some error.Try after some time.');
                    }
                });

            });


         

            $(document).on("input", "#sr_no, #bag_quantity", function() {
                var sr_no = $("#sr_no").val();
                var sub = $("#bag_quantity").val();
                $("#lot_number").val(sr_no+'/'+sub);

            });

  
        })
    </script>
@endsection
