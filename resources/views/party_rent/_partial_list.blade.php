@if(!empty($itemDataset) && count($itemDataset) > 0)


<table class="table table-bordered tbl_thin" style="margin: 0">
    <tr class="bg-primary">
        <th class="text-center">Party Rent details</th>
    </tr>
</table>

<div class="table-responsive">
    <table class="table table-bordered tbl_thin" id="check">
        <tbody>
            <tr class="bg-info" id="r_checkAll">
                <th class="text-center" style="width:3%;">#</th>
                <th>Party Type</th>
                <th>Party Name</th>
                <th>Category</th>
                <th>Product</th>
                <th>Unit</th>
                <th>Unit Size</th>
                <th>Rent</th>
                <th class="text-center" style="width: 8%;">Actions</th>

            </tr>

            <?php
            $counter = 0;
            if (isset($_GET['page']) && $_GET['page'] > 1) {
                $counter = ($_GET['page'] - 1) * $dataset->perPage();
            }
            ?>
            @forelse ($itemDataset as $data)
            <?php $counter++; ?>
            <tr onmouseover="change_color(this, true)" onmouseout="change_color(this, false)">

                <td class="text-center">{{ $counter }}</td>
                <td>{{ !empty($data->party_type) ? $data->party_type->party_type : "" }}</td>
                <td>{{ !empty($data->party) ? $data->party->name : "" }}</td>
                <td>{{ !empty($data->categories) ? $data->categories->name : "" }}</td>
                <td>{{ !empty($data->products) ? $data->products->name : "" }}</td>
                <td>{{ !empty($data->units) ? $data->units->unit : ""}}</td>
                <td>{{ !empty($data->UnitSize) ? $data->UnitSize->name : ""}}</td>
                <td>{{ $data->rent }}</td>
                <td class="text-center">

                    <input type="hidden" value="{{$data->id}}" id="id">

                    @if (has_user_access('party_rent_delete'))
                    <a class="color_danger" id="Del_btn" title="Delete Item" ><i class="fa fa-trash-o"></i></a>
                    @endif
                </td>

            </tr>
            @empty
            <tr>
                <td class="text-center" colspan="7"> No records found </td>
            </tr>
            @endforelse
        </tbody>
    </table>
</div>
<div class="text-center" style="margin-top:10px;">

    <a class="btn btn-primary xsw_100" href="{{ url('/party_rent') }}">Save</a>


</div>
@else
<div class="alert alert-info">
    <table class="table table-bordered tbl_thin" style="margin: 0">
        <tr class="bg-primary">
            <th class="text-center">Rent type details</th>
        </tr>
    </table>
</div>
@endif
<script>
          $(document).on("click", "#Del_btn", function() {
            var _url = "{{ URL::to('/party_rent/remove/item') }}";
            var _id=$("#id").val();
                axios.post(_url, {id:_id})
                .then((res) => {
                    $("#Del_btn").closest("tr").remove();
                    toastr.success("Successfully deleted !");
                })
                .catch((error) => {
                   
                })
            
        });
</script>