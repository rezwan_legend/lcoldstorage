@extends('layouts.app')

@section('breadcrumbs')
<div id="breadcrumbBar" class="breadcrumb site_nav_links no_bdr_rad clearfix">
    <div class="col-md-3 col-sm-3 col-xs-2 no_pad">
        <button class="btn btn-info btn-xs" type="button" onclick="history.back()" title="Go Back"><span class="visible-xs"><i class="fa fa-arrow-left"></i></span><span class="hidden-xs">Back</span></button>
        <button class="btn btn-info btn-xs" onclick="redirectTo('<?= url('party_rent/create'); ?>')" title="Refresh" type="button"><span class="visible-xs"><i class="fa fa-refresh"></i></span><span class="hidden-xs">Refresh</span></button>
    </div>
    <div class="col-md-6 col-sm-6 hidden-xs text-center">
        <h2 class="page-title">{{$breadcrumb_title}}</h2>
    </div>
    <div class="col-md-3 col-sm-3 col-xs-10 no_pad">
        <ul class="text-right no_mrgn no_pad">
            <li><a href="{{ url('/home') }}">Home</a> <i class="fa fa-angle-right"></i></li>
            <li><a href="{{ url('/party_rent') }}">Party Rent</a> <i class="fa fa-angle-right"></i></li>
            <li>Form</li>
        </ul>
    </div>
</div>
@endsection

@section('content')
<div class="panel panel-primary">
    <div class="panel-heading">
        <h3 class="panel-title">Enter Party Rent</h3>
    </div>
    <div class="panel-body">
        <div class="form-group col-md-2">
            <select class="form-control select2search" id="party_type_id" name="party_type_id" required>
                <option value="">Select party Type</option>
                @foreach($party_type as $type)
                <option value="{{$type->id}}">{{$type->party_type}}</option>
                @endforeach
            </select>
            <small class="text-danger">{{ $errors->first('party_type') }}</small>
        </div>
        <div class="form-group col-md-2">
            <select class="form-control select2search" id="party_id" name="party_id">
                <option value="">Select Party</option>
            </select>
            <small class="text-danger">{{ $errors->first('party_rent') }}</small>
        </div>
        <div class="form-group col-md-2">
            <select class="form-control select2search" id="category_id" name="category_id">
                <option value="">Select Category</option>
                @foreach($categories as $category)
                <option value="{{ $category->id }}">{{ $category->name }}</option>
                @endforeach
            </select>
            <small class="text-danger">{{ $errors->first('category') }}</small>
        </div>
        <div class="form-group col-md-2">
            <select class="form-control select2search" id="product_id" name="product_id">
                <option value="">Select Product</option>

            </select>
            <small class="text-danger">{{ $errors->first('product') }}</small>
        </div>
        <div class="form-group col-md-2">
            <select class="form-control select2search" id="unit_id" name="unit_id" required>
                <option value="">Select Unit</option>
                @foreach($units as $unit)
                <option value="{{$unit->id}}">{{$unit->unit}}</option>
                @endforeach
            </select>
            <small class="text-danger">{{ $errors->first('unit') }}</small>
        </div>
        <div class="form-group col-md-2">
            <select class="form-control select2search" id="unit_size_id" name="unit_size_id" required>
                <option value="">Select Unit Size</option>

            </select>
            <small class="text-danger">{{ $errors->first('unit_size') }}</small>
        </div>
        <div class="form-group col-md-1">
            <input type="text" class="form-control" id="rent" name="rent" placeholder="rent" required>
            <small class="text-danger">{{ $errors->first('rent') }}</small>
        </div>
        <div class="form-group col-md-1">
            <button style="padding: 5px;" class="text-center"><span id="addRowItem" style="border-radius:50%;color:white; cursor: pointer; padding:5px;background:#383838"><i class="fa fa-plus"></i></span></button>
        </div>
    </div>
</div>
<div class="clearfix">
    <div class="clearfix" id="item_list">
        <table class="table table-bordered tbl_thin" style="margin: 0">
            <tr class="bg-primary">
                <th class="text-center"><span>Party rent details</span></th>
            </tr>
        </table>
        <form id="frmList" action="{{  route('party_rent.store') }}">
            <div class="table-responsive">
                <table class="table table-bordered tbl_thin" id="check">
                    <thead>
                        <tr class="bg-info" id="r_checkAll">
                            <th>Party Type</th>
                            <th>Party Name</th>
                            <th>Category</th>
                            <th>Product</th>
                            <th>Unit</th>
                            <th>Unit Size</th>
                            <th>Rent</th>
                            <th class="text-center" style="width: 3%;"></th>
                        </tr>
                    </thead>
                    <tbody id="rentitems">

                    </tbody>
                </table>
            </div>
        </form>
    </div>
</div>

<div class="text-center" style="margin-top:10px;">
    <button id="itemInsertBtn" class="btn btn-primary xsw_100" disabled>Save</button>
</div>
<script type="text/javascript">    
    // Dropdown Validation
    const isDropdownSelected = () => {
        let status = true;
        if ($('#party_type_id').val() === '') {
            toastr.error('Please select Party Type dropdown');
            status = false;
        }
        if ($('#party_id').val() === '') {
            toastr.error('Please select Party dropdown');
            status = false;
        }
        if ($('#category_id').val() === '') {
            toastr.error('Please select Category dropdown');
            status = false;
        }
        if ($('#product_id').val() === '') {
            toastr.error('Please select Product dropdown');
            status = false;
        }
        if ($('#unit_id').val() === '') {
            toastr.error('Please select Unit dropdown');
            status = false;
        }
        if ($('#unit_size_id').val() === '') {
            toastr.error('Please select Unit Size dropdown');
            status = false;
        }
        if ($('#rent').val() === '') {
            toastr.error('rent must be required!!ty');
            status = false;
        }
        return status;
    }

    var addedProductId = [];
    const addItemId = () => {
        let object        = {};
        let partyId       = $("#party_id").val();
        let productId     = $("#product_id").val();
        let unitId          = $("#unit_id").val()
        object["partyId"]   = partyId;
        object["productId"]   = productId;
        object["unitId"]   = unitId;
    
        addedProductId.push(object);
        enable(itemInsertBtn);
    }

    // Table Data generate Function
    const getDomeUi = () => {
        addItemId();
        return `<tr>
        <td>${$('#party_type_id option:selected').text()}
            <input class="party_type_id" data-id="${$('#party_type_id').val()}"  type="hidden" name="party_type_id[]" value="${$('#party_type_id').val()}">
        </td>
        <td>${$('#party_id option:selected').text()}
            <input class="party_id" data-id="${$('#party_id').val()}"  type="hidden" name="party_id[]" value="${$('#party_id').val()}">
        </td>
        <td>${$('#category_id option:selected').text()}
            <input class="category_id" data-id="${$('#category_id').val()}"  type="hidden" name="category_id[]" value="${$('#category_id').val()}">
        </td>
        <td>${$('#product_id option:selected').text()}
            <input class="product_id" data-id="${$('#product_id').val()}"  type="hidden" name="product_id[]" value="${$('#product_id').val()}">
        </td>
        <td>${$('#unit_id option:selected').text()}
            <input class="unit_id" data-id="${$('#unit_id').val()}"  type="hidden" name="unit_id[]" value="${$('#unit_id').val()}">
        </td>
        <td>${$('#unit_size_id option:selected').text()}
            <input class="unit_size_id" data-id="${$('#unit_size_id').val()}"  type="hidden" name="unit_size_id[]" value="${$('#unit_size_id').val()}">
        </td>
        <td>${$('#rent').val()}<input style="display:none" type="number" name="rent[]" value="${ $('#rent').val() }" class="form-control rent" placeholder="Amount" readonly ></td>
        <td><i id='deleteItem' class='fa fa-trash' role='button'></i></td>
        </tr>`;
    }

    // Table Row Create function
    const addRow = () => {
        if (isDropdownSelected() === false) {
            return false;
        }
        for (let item of addedProductId) {
            if (item["partyId"] == $('#party_id').val() && item["productId"] == $('#product_id').val() && item["unitId"] == $("#unit_id").val()) {
                toastr.warning('Item already added');
                return false;
            }
        }
        document
            .querySelector("#rentitems")
            .insertAdjacentHTML("beforeend", getDomeUi());
    }

    // Table Row Remove
    const removeRow = () => {
        $(document).on("click", "#deleteItem", function() {
            let partyId = $(this).parents('tr').find('.party_id').data('id');
            let productId = $(this).parents('tr').find('.product_id').data('id');
            let unitId = $(this).parents('tr').find('.unit_id').data('id');
            for (let index in addedProductId) {
                if (addedProductId[index]["partyId"] == partyId) {
                    addedProductId.splice(index, 1)
                }
               else if (addedProductId[index]["productId"] == productId) {
                    addedProductId.splice(index, 1)
                }
               else if (addedProductId[index]["unitId"] == unitId) {
                    addedProductId.splice(index, 1)
                }
            }
            $(this).parents("tr").remove();
            toastr.success("Record Deleted Successfully!");
            disable(itemInsertBtn);
        })
    }

    // Table Row Insert Function
    const processOnSubmit = () => {
        let url = "{{ route('party_rent.store') }}";
        let itemForm = $("#frmList").serialize();
       
        axios.post(url,itemForm)
        .then((response) => {
            toastr.success(response.data);
            window.onbeforeunload = true;
            
                window.location = "{{ route('party_rent.index') }}";
            
        })
        .catch((error) => {
            toastr.error("Something went Wrong!");
        })
    }

        $(document).on("click", "#itemInsertBtn", function(e) {
        disable(itemInsertBtn);
        return true;
        e.preventDefault();
    });

    //     $(document).on("change", "#party_type_id", function() {
    //     enable(itemInsertBtn);
    //     return true;
    // });

    // Table Row Create and Save Btn click function
    $(document).ready(function() {
        $('#addRowItem').click(addRow)
        removeRow();
        $('#itemInsertBtn').click(function() {
            processOnSubmit();
        })
    })

    //on change on party type  to party 
    $(document).on("change", "#party_type_id", function() {
        var _partyType = $("#party_type_id").val();
        $.ajax({
            url: "{{ URL::to('party_laon/partyListByType') }}",
            type: "post",
            data: {
                _token: csrfToken,
                partyTypeId: _partyType
            },
            success: function(res) {
                $('#party_id').html(res);
            },
            error: function(xhr, status) {
                alert('There is some error.Try after some time.');
            }
        });

    });

    //on change on category to product
    $(document).on("change", "#category_id", function() {
        var _productType = $("#category_id").val();
        $.ajax({
            url: "{{ URL::to('/party_rent/ProductByCategory') }}",
            type: "post",
            data: {
                _token: csrfToken,
                productTypeId: _productType
            },
            success: function(res) {
                $('#product_id').html(res);
            },
            error: function(xhr, status) {
                alert('There is some error.Try after some time.');
            }
        });

    });

    //on change on unit to unit size
    $(document).on("change", "#unit_id", function() {
        var _Unit_id = $("#unit_id").val();
        $.ajax({
            url: "{{ URL::to('/party_rent/UnitSizeByUnit') }}",
            type: "post",
            data: {
                _token: csrfToken,
                UnitSizeId: _Unit_id
            },
            success: function(res) {
                $('#unit_size_id').html(res);
            },
            error: function(xhr, status) {
                alert('There is some error.Try after some time.');
            }
        });

    });

</script>
@endsection