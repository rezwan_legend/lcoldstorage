@extends('layouts.app')

@section('breadcrumbs')
    <div id="breadcrumbBar" class="breadcrumb site_nav_links no_bdr_rad clearfix">
        <div class="col-md-3 col-sm-3 col-xs-2 no_pad">
            <button class="btn btn-info btn-xs" type="button" onclick="history.back()" title="Go Back"><span
                    class="visible-xs"><i class="fa fa-arrow-left"></i></span><span
                    class="hidden-xs">Back</span></button>
            <button class="btn btn-info btn-xs" onclick="redirectTo('<?= url('/account_head') ?>')" title="Refresh"
                type="button"><span class="visible-xs"><i class="fa fa-refresh"></i></span><span
                    class="hidden-xs">Refresh</span></button>
        </div>
        <div class="col-md-6 col-sm-6 hidden-xs text-center">
            <h2 class="page-title">{{ $page_title }}</h2>
        </div>
        <div class="col-md-3 col-sm-3 col-xs-10 no_pad">
            <ul class="text-right no_mrgn no_pad">
                <li><a href="{{ url('/home') }}">Home</a> <span class="fa fa-angle-right"></span></li>
                <li>Account Head</li>
            </ul>
        </div>
    </div>
@endsection

@section('content')
    <div class="well">

        <div class="clearfix mb_10">
            <div class="text-center">
                @if (has_user_access('account_head_create'))
                    <a class="btn btn-success btn-xs" href="{{ url('/account_head/create') }}"><i
                            class="fa fa-plus"></i> New</a>
                @endif
                @if (has_user_access('account_head_delete'))
                    <button type="button" class="btn btn-danger btn-xs" id="Del_btn" disabled><i
                            class="fa fa-trash-o"></i> Delete</button>
                @endif
                <button class="btn btn-primary btn-xs " onclick="printDiv('print_area')"><i
                        class="fa fa-print"></i> Print</button>

            </div>
        </div>
        <table width="100%">
            <tr>
                <td class="wmd_70">
                    {!! Form::open(['method' => 'POST', 'class' => 'search-form', 'id' => 'frmSearch', 'name' => 'frmSearch']) !!}
                    <div class="input-group">
                        <div class="input-group-btn clearfix">
                            <?php echo number_dropdown(50, 550, 50, null, 'xsw_30'); ?>
                            <div class="col-md-2 col-sm-3 xsw_70 no_pad">
                                <input type="text" name="name" id="name" class="form-control" placeholder="Name"
                                    >
                            </div>
                            <div class="col-md-2 col-sm-3  no_pad">
                                <input type="text" name="search" id="q" class="form-control" placeholder="search"
                                    style="width:100%">
                            </div>
                            <div class="col-md-2 col-sm-3 xsw_50 no_pad" style="width: 10%;">
                                <select id="sortType" class="form-control" name="sort_by">
                                    <option value="type">Type</option>
                                    <option value="head_type" selected>Head Type</option>
                                </select>
                            </div>
                            <div class="col-md-2 col-sm-3 xsw_50 no_pad" style="width: 10%;">
                                <select id="sortType" class="form-control" name="sort_type">
                                    <option value="ASC" selected>Ascending</option>
                                    <option value="DESC">Descending</option>
                                </select>
                            </div>
                            <div class="col-md-2 col-sm-3 xsw_100 no_pad" style="width: auto;">
                                <button type="button" id="search" class="btn btn-info xsw_50">Search</button>
                                <button type="button" id="clear_from" class="btn btn-warning xsw_50">Clear</button>
                            </div>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </td>

            </tr>
        </table>
    </div>

    {!! Form::open(['method' => 'POST', 'class' => 'search-form', 'id' => 'frmList', 'name' => 'frmList']) !!}
    <div id="print_area">
        <?php print_header('Head Information'); ?>
        <div id="ajax_content">
            <div class="table-responsive">
                <table class="table table-bordered tbl_thin" id="check">
                    <tr class="bg-info" id="r_checkAll">
                        <th class="text-center" style="width:3%;">#</th>
                        <th>Name</th>
                        <th class="text-center">Type</th>
                        <th class="text-center">Head Type</th>
                        <th class="text-center">Common</th>
                        <th class="text-right">Debit</th>
                        <th class="text-right">Credit</th>
                        <th class="text-right">Balance</th>
                        <th class="text-center hip">Actions</th>
                        <th class="text-center hip"><input type="checkbox" id="check_all" value="all"></th>
                    </tr>
                    <?php
                    $counter = 0;
                    if (isset($_GET['page']) && $_GET['page'] > 1) {
                        $counter = ($_GET['page'] - 1) * $dataset->perPage();
                    }
                    ?>
                    @foreach ($dataset as $data)
                        <tr onmouseover="change_color(this, true)" onmouseout="change_color(this, false)">
                            <td class="text-center">{{ $loop->iteration }}</td>
                            <td>{{ $data->name }}</td>
                            <td class="text-center">{{ $data->type }}</td>
                            <td class="text-center">{{ $data->head_type }}</td>
                            <td class="text-center">{{ $data->common }}</td>
                            <td class="text-center">{{ $data->debit }}</td>
                            <td class="text-center">{{ $data->credit }}</td>
                            <td class="text-center">{{ $data->balence }}</td>
                            <td class="text-center hip">
                                @if (has_user_access('account_head_edit'))
                                    <a class="btn btn-info btn-xs"
                                        href="{{ url('account_head/' . $data->_key . '/edit') }}">Edit</a>
                                @endif
                                <a class="btn btn-primary btn-xs" href="{{ url('account_head/ledger') }}">Ledger</a>
                            </td>
                            <td class="text-center hip">
                                @if (has_user_access('account_head_delete'))
                                    <input type="checkbox" name="data[]" value="{{ $data->id }}">
                                @endif
                            </td>
                        </tr>
                    @endforeach
                </table>
                <div class="text-center hip">
                    {{ $dataset->render() }}
                </div>
            </div>
        </div>
    </div>
    {!! Form::close() !!}
@endsection

@section('page_script')
    <script type="text/javascript">
        $(document).ready(function() {
            $("#search").click(function() {
                var _url = "{{ URL::to('account_head/search') }}";
                var _form = $("#frmSearch");

                $.ajax({
                    url: _url,
                    type: "post",
                    data: _form.serialize(),
                    success: function(data) {
                        $('#ajax_content').html(data);
                    },
                    error: function() {
                        $('#ajaxMessage').showAjaxMessage({
                            html: 'There is some error.Try after some time.',
                            type: 'error'
                        });
                    }
                });
            });

            $("#Del_btn").click(function() {
                var _url = "{{ URL::to('account_head/delete') }}";
                var _form = $("#frmList");
                var _rc = confirm("Are you sure about this action? This cannot be undone!");
                if (_rc == true) {
                    $.post(_url, _form.serialize(), function(data) {
                        if (data.success === true) {
                            toastr.success(data.message);
                            $("#search").trigger("click");
                        } else {
                            toastr.error(data.message);
                        }
                    }, "json");
                }

            });
        });
    </script>
@endsection
