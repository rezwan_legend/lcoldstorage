@extends('layouts.app')

@section('breadcrumbs')
<div id="breadcrumbBar" class="breadcrumb site_nav_links no_bdr_rad clearfix">
    <div class="col-md-3 col-sm-3 col-xs-2 no_pad">
        <button class="btn btn-info btn-xs" type="button" onclick="history.back()" title="Go Back"><span class="visible-xs"><i class="fa fa-arrow-left"></i></span><span class="hidden-xs">Back</span></button>
        <button class="btn btn-info btn-xs" onclick="redirectTo('<?= url('/account_head/'.$data->_key.'/edit'); ?>')" title="Refresh" type="button"><span class="visible-xs"><i class="fa fa-refresh"></i></span><span class="hidden-xs">Refresh</span></button>
    </div>
    <div class="col-md-6 col-sm-6 hidden-xs text-center">
        <h2 class="page-title">{{ $page_title }}</h2>
    </div>
    <div class="col-md-3 col-sm-3 col-xs-10 no_pad">
        <ul class="text-right no_mrgn no_pad">
            <li><a href="{{ url('/home') }}">Home</a> <span class="fa fa-angle-right"></span></li>
            <li><a href="{{ url('/account_head') }}">Account Head <span class="fa fa-angle-right"></span></a></li>
            <li>Form</li>
        </ul>
    </div>
</div>
@endsection

@section('content')
<div class="col-md-8 col-md-offset-2">
<div class="panel panel-primary">
    <div class="panel-heading">
        <h3 class="panel-title">Edit {{ $page_title }}</h3>
    </div>
    <div class="panel-body">
        {!! Form::open(['method' => 'PUT', 'url' => 'account_head/'.$data->_key, 'class' => 'form-horizontal']) !!}
        <div class="form-group">
                <label for="type" class="col-md-4 control-label">Type</label>
                <div class="col-md-6">
                    <select class="form-control select2search" id="type" name="type">
                        <option value="">Select</option>
                        <option value="Debit" <?php if ($data->type == "Debit") echo ' selected'; ?> >Debit</option>
                        <option value="Credit" <?php if ($data->type == "Credit") echo ' selected'; ?> >Credit</option>
                    </select>
                    <small class="text-danger">{{ $errors->first('type') }}</small>
                </div>
            </div>
            <div class="form-group">
                <label for="htype" class="col-md-4 control-label">Head Type</label>
                <div class="col-md-6">
                    <select class="form-control select2search" id="htype" name="htype">
                        <option value="">Select</option>
                        <option value="Expense" <?php if ($data->head_type == "Expense") echo ' selected'; ?> >Expense</option>
                        <option value="Income" <?php if ($data->head_type == "Income") echo ' selected'; ?> >Income</option>
                    </select>
                    <small class="text-danger">{{ $errors->first('head_type') }}</small>
                </div>
            </div>
            <div class="form-group">
                <label for="name" class="col-md-4 control-label">Name</label>
                <div class="col-md-6">
                    <input type="text" class="form-control" name="name" value="{{ $data->name }}" required>
                    <small class="text-danger">{{ $errors->first('name') }}</small>
                </div>
            </div>
            <div class="form-group">
                <label for="common" class="col-md-4 control-label">Common</label>
                <div class="col-md-6">
                    <select class="form-control select2search" id="common" name="common">
                        <option value="">Select</option>
                        <option value="Yes" <?php if ($data->common == "Yes") echo ' selected'; ?> >Yes</option>
                        <option value="No" <?php if ($data->common == "No") echo ' selected'; ?> >No</option>
                    </select>
                    <small class="text-danger">{{ $errors->first('common') }}</small>
                </div>
            </div>
        <div class="form-group">
            <div class="col-md-8 col-md-offset-4">
                <input type="submit" class="btn btn-primary xsw_50" name="btnSave" value="Update">
            </div>
        </div>
        {!! Form::close() !!}
    </div>
</div>
</div>
@endsection