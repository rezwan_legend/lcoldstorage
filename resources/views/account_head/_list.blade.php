@if (!empty($dataset) && count($dataset) > 0)
<div class="table-responsive">
    <table class="table table-bordered tbl_thin" id="check">
        <tr class="bg-info" id="r_checkAll">
            <th class="text-center" style="width:3%;">#</th>
            <th>Name</th>
            <th class="text-center">Type</th>
            <th class="text-center">Head Type</th>
            <th class="text-center">Common</th>
            <th class="text-right">Debit</th>
            <th class="text-right">Credit</th>
            <th class="text-right">Balance</th>
            <th class="text-center hip">Actions</th>
            <th class="text-center hip" style="width:3%;"><input type="checkbox" id="check_all" value="all"></th>
        </tr>
        <?php
        $counter = 0;
        if (isset($_GET['page']) && $_GET['page'] > 1) {
            $counter = ($_GET['page'] - 1) * $dataset->perPage();
        }
        ?>
        @foreach ($dataset as $data)
        <tr onmouseover="change_color(this, true)" onmouseout="change_color(this, false)">
            <td class="text-center">{{ $loop->iteration  }}</td>
            <td>{{ $data->name }}</td>
            <td class="text-center">{{ $data->type }}</td>
            <td class="text-center">{{ $data->head_type }}</td>
            <td class="text-center">{{ $data->common }}</td>
            <td class="text-center">{{ $data->debit }}</td>
            <td class="text-center">{{ $data->credit }}</td>
            <td class="text-center">{{ $data->balence }}</td>
            <td class="text-center hip">
                @if(has_user_access('account_head_edit'))
                <a class="btn btn-info btn-xs" href="{{ url('/account_head/'.$data->_key.'/edit') }}">Edit</a>
                @endif
                <a class="btn btn-primary btn-xs" href="{{ url('/ledger/head/'.$data->id) }}">Ledger</a>
            </td>
            <td class="text-center hip">
                @if(has_user_access('account_head_delete'))
                <input type="checkbox" name="data[]" value="{{ $data->id }}">
                @endif
            </td>
        </tr>
        @endforeach
    </table>
    <div class="text-center hip" id="apaginate">
        {{ $dataset->render() }}
    </div>
</div>
@else
<div class="alert alert-info">No records found!</div>
@endif