@extends('layouts.app')

@section('breadcrumbs')
<div id="breadcrumbBar" class="breadcrumb site_nav_links no_bdr_rad clearfix">
    <div class="col-md-3 col-sm-3 col-xs-2 no_pad">
        <button class="btn btn-info btn-xs" type="button" onclick="history.back()" title="Go Back"><span class="visible-xs"><i class="fa fa-arrow-left"></i></span><span class="hidden-xs">Back</span></button>
        <button class="btn btn-info btn-xs" onclick="redirectTo('<?= url('pocket_setting'); ?>')" title="Refresh" type="button"><span class="visible-xs"><i class="fa fa-refresh"></i></span><span class="hidden-xs">Refresh</span></button>
    </div>
    <div class="col-md-6 col-sm-6 hidden-xs text-center">
        <h2 class="page-title">Pocket List</h2>
    </div>
    <div class="col-md-3 col-sm-3 col-xs-10 no_pad">
        <ul class="text-right no_mrgn no_pad">
            <li><a href="{{ url('/home') }}">Home</a> <span class="fa fa-angle-right"></span></li>
            <li>Pocket</li>
        </ul>
    </div>
</div>
@endsection

@section('content')
<div class="well">
    <table width="100%">
        <tr>
            <td class="wmd_70">
                {!! Form::open(['method' => 'POST', 'class' => 'search-form', 'id' => 'frmSearch', 'name' => 'frmSearch']) !!}
                <div class="input-group">
                    <div class="input-group-btn clearfix">
                        <?php echo number_dropdown(50, 550, 50, null, 'xsw_30') ?>
                        <div class="col-md-2 col-sm-3 xsw_50 no_pad">
                            <select id="chamber_id" class="form-control select2search" name="chamber_id" required>
                                <option value="">Select Chamber</option>
                                @foreach($chambers as $chamber)
                                <option value="{{ $chamber->id }}">{{ $chamber->chamber }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-2 col-sm-3 xsw_50 no_pad">
                            <select id="floor_id" class="form-control select2search" name="floor_id" required>
                                <option value="">Select Floor</option>
                            </select>
                        </div>
                        <div class="col-md-2 col-sm-3 xsw_70 no_pad">
                            <input type="text" name="search" id="q" class="form-control" placeholder="search" size="30">
                        </div>
                        <div class="col-md-2 col-sm-3 xsw_50 no_pad" style="width: 10%;">
                            <select id="sortType" class="form-control" name="sort_by">
                                <option value="name">Name</option>
                                <option value="capacity" selected>Capacity</option>
                            </select>
                        </div>
                        <div class="col-md-2 col-sm-3 xsw_50 no_pad" style="width: 10%;">
                            <select id="sortType" class="form-control" name="sort_type">
                                <option value="ASC" selected>Ascending</option>
                                <option value="DESC">Descending</option>
                            </select>
                        </div>
                        <div class="col-md-2 col-sm-3 xsw_100 no_pad" style="width: auto;">
                            <button type="button" id="search" class="btn btn-info xsw_50">Search</button>
                            <button type="button" id="clear_from" class="btn btn-warning xsw_50">Clear</button>
                        </div>
                    </div>
                </div>
                {!! Form::close() !!}
            </td>
            <td class="text-right wmd_30" style="width:20%">
                @if(has_user_access('pocket_create'))
                <a class="btn btn-success btn-xs xsw_33" href="{{ url ('/pocket_setting/create') }}"><i class="fa fa-plus"></i> New</a>
                @endif
                @if(has_user_access('pocket_delete'))
                <button type="button" class="btn btn-danger btn-xs xsw_33" id="Del_btn" disabled><i class="fa fa-trash-o"></i> Delete</button>
                @endif
                <button class="btn btn-primary btn-xs xsw_33" onclick="printDiv('print_area')"><i class="fa fa-print"></i> Print</button>
            </td>
        </tr>
    </table>
</div>

{!! Form::open(['method' => 'POST', 'class' => 'search-form', 'id'=>'frmList','name'=>'frmList']) !!}
<div id="print_area">
    <?php print_header("Floor Information"); ?>
    <div id="ajax_content">
    @if(!empty($dataset) && count($dataset) > 0)
        <div class="table-responsive">
            <table class="table table-bordered tbl_thin" id="check">
                <tr class="bg-info" id="r_checkAll">
                    <th class="text-center" style="width:3%;">#</th>
                    <th>Chamber</th>
                    <th>Floor</th>
                    <th>Pocket</th>
                    <th>Capacity</th>
                    <th class="text-center hip" style="width:10%;">Actions</th>
                    <th class="text-center hip" style="width:3%;"><input type="checkbox" id="check_all" value="all"></th>
                </tr>
                <?php
                $counter = 0;
                if (isset($_GET['page']) && $_GET['page'] > 1) {
                    $counter = ($_GET['page'] - 1) * $dataset->perPage();
                }
                ?>
                @foreach ($dataset as $data)
                <?php
                $counter++;
                ?>
                <tr onmouseover="change_color(this, true)" onmouseout="change_color(this, false)">
                    <td class="text-center">{{ $counter }}</td>
                    <td>{{ ($data->chambers) ? $data->chambers->chamber : "" }}</td>
                    <td>{{ ($data->floors) ? $data->floors->name : "" }}</td>
                    <td>{{ $data->name }}</td>
                    <td class="">{{ $data->capacity }}</td>
                    <td class="text-center hip">
                        @if(has_user_access('pocket_edit'))
                        <a class="btn btn-info btn-xs" href="{{ url('pocket_setting/'.$data->_key.'/edit') }}">Edit</a>
                        @endif
                    </td>
                    <td class="text-center hip">
                        @if(has_user_access('floor_delete'))
                        <input type="checkbox" name="data[]" value="{{ $data->id }}">
                        @endif
                    </td>
                </tr>
                @endforeach
            </table>
            <div class="text-center hip">
                {{ $dataset->render() }}
            </div>
            @else
            <div class="alert alert-info">No records found.</div>
            @endif
        </div>
    </div>
</div>
{!! Form::close() !!}
@endsection

@section('page_script')
<script type="text/javascript">
    $(document).on("change", "#chamber_id", function() {
        var _chamber_id = $("#chamber_id").val();
        $.ajax({
            url: "{{ URL::to('pocket_setting/floorListByChamber') }}",
            type: "post",
            data: {
                _token: csrfToken,
                chamber_id: _chamber_id
            },
            success: function(res) {
                $('#floor_id').html(res);
            },
            error: function(xhr, status) {
                alert('There is some error.Try after some time.');
            }
        });

    });
    $(document).ready(function() {
        $("#search").click(function() {
            var _url = "{{ URL::to('pocket_setting/search') }}";
            var _form = $("#frmSearch");

            $.ajax({
                url: _url,
                type: "post",
                data: _form.serialize(),
                success: function(data) {
                    $('#ajax_content').html(data);
                },
                error: function() {
                    $('#ajaxMessage').showAjaxMessage({
                        html: 'There is some error.Try after some time.',
                        type: 'error'
                    });
                }
            });
        });

        $("#Del_btn").click(function() {
            var _url = "{{ URL::to('pocket_setting/delete') }}";
            var _form = $("#frmList");
            var _rc = confirm("Are you sure about this action? This cannot be undone!");

            if (_rc == true) {

                $.post(_url, _form.serialize(), function(data) {
                    if (data.success === true) {
                        $('#ajaxMessage').showAjaxMessage({
                            html: data.message,
                            type: 'success'
                        });
                        $("#search").trigger("click");
                    } else {
                        $('#ajaxMessage').showAjaxMessage({
                            html: data.message,
                            type: 'error'
                        });
                    }
                }, "json");
            }

        });
    });
</script>
@endsection