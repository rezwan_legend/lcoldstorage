@if (!empty($dataset) && count($dataset) > 0)
  
        <div class="table-responsive">
            <table class="table table-bordered tbl_invoice_view" id="check">
                <tbody>
                    <tr class="bg_gray" id="r_checkAll">
                        <th class="text-center" style="width:3%;">#</th>
                        <th>Palot Location</th>

                    </tr>
                    @foreach ($chambers as $chamber)
                        <tr>
                            <td class="text-center">1</td>
                            <td><span class="dis_blok" style="font-weight: 600;">{{ $chamber->chamber }}</span>

                                <ul class="floor_list">
                                    @foreach ($chamber->floors as $floor)
                                        <li id="floor_no_39" style="">
                                            <div class=""
                                                style="background-color:#ebebeb;border-bottom:1px solid #cdcdcd;padding:5px 10px;">
                                                <span style="display:inline-block;width:150px">{{ $floor->name }}</span>
                                                <span style="display:inline-block;">Total Stored Bag On This Floor =
                                                    {{ $floor->capacity }}</span>
                                                </span>
                                            </div>
                                            <ul class="pocket_list clearfix">
                                                @foreach ($floor->pockets as $pocket)
                                                    <li id="" style="margin: 20px">
                                                        <div id="">
                                                            <span style="display:inline-block;margin-right:10px;"
                                                                id="">{{ $pocket->name }}
                                                                1</span> <span>[ (capacity:{{ $pocket->capacity }})
                                                                (stock:0)
                                                                ]</span>
                                                            <span>
                                                                <input type="button" class="btn btn-danger btn-xs Del_btn"
                                                                    data-id="{{ $pocket->id }}" value="delete"></span>
                                                            <span class="btn_edel btn_edit btn_pocket_edit">
                                                                @if (has_user_access('pocket_setting_edit'))
                                                                    <a class="btn btn-info btn-xs"
                                                                        href="{{ url('pocket_setting/' . $pocket->_key . '/edit') }}"><i
                                                                            class="fa fa-edit"></i> Edit</a>
                                                                @endif
                                                            </span>
                                                        </div>
                                                    </li>
                                                @endforeach



                                            </ul>
                                        </li>
                                    @endforeach
                                </ul>

                            </td>


                        </tr>
                    @endforeach

                </tbody>
            </table>
    

        <div class="paging text-center dis_print">
        </div>
    </div>
@else
<div class="alert alert-info">No records found!</div>
@endif