@extends('layouts.app')

@section('breadcrumbs')
    <div id="breadcrumbBar" class="breadcrumb site_nav_links no_bdr_rad clearfix">
        <div class="col-md-3 col-sm-3 col-xs-2 no_pad">
            <button class="btn btn-info btn-xs" type="button" onclick="history.back()" title="Go Back"><span
                    class="visible-xs"><i class="fa fa-arrow-left"></i></span><span
                    class="hidden-xs">Back</span></button>
            <button class="btn btn-info btn-xs" onclick="redirectTo('<?= url('pocket_setting') ?>')" title="Refresh"
                type="button"><span class="visible-xs"><i class="fa fa-refresh"></i></span><span
                    class="hidden-xs">Refresh</span></button>
        </div>
        <div class="col-md-6 col-sm-6 hidden-xs text-center">
            <h2 class="page-title">Pocket List</h2>
        </div>
        <div class="col-md-3 col-sm-3 col-xs-10 no_pad">
            <ul class="text-right no_mrgn no_pad">
                <li><a href="{{ url('/home') }}">Home</a> <span class="fa fa-angle-right"></span></li>
                <li>Pocket</li>
            </ul>
        </div>
    </div>
@endsection

@section('content')
    <div class="well">
     <div class="clearfix mb_10">
            <div class="text-center">
          
            <a class="btn btn-success btn-xs " href="{{ url('/chamber_setting/create') }}"><i
                    class="fa fa-plus"></i>&nbsp;Room</a>
            <a class="btn btn-success btn-xs " href="{{ url('/floor_setting/create') }}"><i
                    class="fa fa-plus"></i>&nbsp;Floor</a>
            <a class="btn btn-success btn-xs " href="{{ url('/pocket_setting/create') }}"><i
                    class="fa fa-plus"></i>&nbsp;Pocket</a>
            </div>
        </div>

       


        <table width="100%">
            <tbody>
                <tr>
                    <td class="wmd_70">
                        {!! Form::open(['method' => 'POST', 'class' => 'search-form', 'id' => 'frmSearch', 'name' => 'frmSearch']) !!}
                        <div class="input-group">
                            <div class="input-group-btn clearfix">
                                <?php echo number_dropdown(50, 550, 50, null, 'xsw_30'); ?>
                                <div class="col-md-2 col-sm-3 xsw_70 no_pad">
                                    <select id="chamber_id" class="form-control select2search" name="chamber_id" required>
                                        <option value="">Select Chamber</option>
                                        @foreach ($chambers as $chamber)
                                            <option value="{{ $chamber->id }}">{{ $chamber->chamber }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-2 col-sm-3 xsw_50 no_pad">
                                    <select id="floor_id" class="form-control select2search" name="floor_id" required>
                                        <option value="">Select Floor</option>
                                    </select>
                                </div>
                                <div class="col-md-2 col-sm-3 xsw_50  no_pad" >
                                    <input type="text" name="search" id="q" class="form-control" placeholder="search" style="width: 100%">
                                    
                                </div>
                                <div class="col-md-2 col-sm-3 xsw_50 no_pad" style="width: 10%;">
                                    <select id="sortType" class="form-control" name="sort_by">
                                        <option value="name">Name</option>
                                        <option value="capacity" selected>Capacity</option>
                                    </select>
                                </div>
                                <div class="col-md-2 col-sm-3 xsw_50 no_pad" style="width: 10%;">
                                    <select id="sortType" class="form-control" name="sort_type">
                                        <option value="ASC" selected>Ascending</option>
                                        <option value="DESC">Descending</option>
                                    </select>
                                </div>
                                <div class="col-md-2 col-sm-3 xsw_100 no_pad" style="width: auto;">
                                    <button type="button" id="search" class="btn btn-info xsw_50">Search</button>
                                    <button type="button" id="clear_from" class="btn btn-warning xsw_50">Clear</button>
                                </div>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </td>

                </tr>
            </tbody>
        </table>
    </div>
    <div id="ajaxContent">
        <div class="table-responsive">
            <table class="table table-bordered tbl_invoice_view" id="check">
                <tbody>
                    <tr class="bg_gray" id="r_checkAll">
                        <th class="text-center" style="width:3%;">#</th>
                        <th>Palot Location</th>

                    </tr>
                    @foreach ($chambers as $chamber)
                        <tr>
                            <td class="text-center">1</td>
                            <td><span class="dis_blok" style="font-weight: 600;">{{ $chamber->chamber }}</span>

                                <ul class="floor_list">
                                    @foreach ($chamber->floors as $floor)
                                        <li id="floor_no_39" style="">
                                            <div class=""
                                                style="background-color:#ebebeb;border-bottom:1px solid #cdcdcd;padding:5px 10px;">
                                                <span style="display:inline-block;width:150px">{{ $floor->name }}</span>
                                                <span style="display:inline-block;">Total Stored Bag On This Floor =
                                                    {{ $floor->capacity }}</span>
                                                </span>
                                            </div>
                                            <ul class="pocket_list clearfix">
                                                @foreach ($floor->pockets as $pocket)
                                                    <li id="" style="margin: 20px; list-style:none;">
                                                        <div id="">
                                                            <span style="display:inline-block;margin-right:10px;"
                                                                class="span">{{ $pocket->name }}
                                                                1</span> <span>[ (capacity:{{ $pocket->capacity }})
                                                                (stock:0)
                                                                ]</span>

                                                            <input type="button" class="btn btn-danger btn-xs Del_btn"
                                                                data-id="{{ $pocket->id }}" value="delete">
                                                            <span class="btn_edel btn_edit btn_pocket_edit">
                                                                @if (has_user_access('pocket_setting_edit'))
                                                                    <a class="btn btn-info btn-xs"
                                                                        href="{{ url('pocket_setting/' . $pocket->_key . '/edit') }}"><i
                                                                            class="fa fa-edit"></i> Edit</a>
                                                                @endif
                                                            </span>
                                                        </div>
                                                    </li>
                                                @endforeach



                                            </ul>
                                        </li>
                                    @endforeach
                                </ul>

                            </td>


                        </tr>
                    @endforeach

                </tbody>
            </table>
        </div>

        <div class="paging text-center dis_print">
        </div>
    </div>
@endsection

@section('page_script')
    <script type="text/javascript">
        $(document).on("change", "#chamber_id", function() {
            var _chamber_id = $("#chamber_id").val();
            $.ajax({
                url: "{{ URL::to('pocket_setting/floorListByChamber') }}",
                type: "post",
                data: {
                    _token: csrfToken,
                    chamber_id: _chamber_id
                },
                success: function(res) {
                    $('#floor_id').html(res);
                },
                error: function(xhr, status) {
                    alert('There is some error.Try after some time.');
                }
            });

        });
        $(document).ready(function() {
            $("#search").click(function() {
                var _url = "{{ URL::to('pocket_setting/search') }}";
                var _form = $("#frmSearch");

                $.ajax({
                    url: _url,
                    type: "post",
                    data: _form.serialize(),
                    success: function(data) {
                        $('#ajaxContent').html(data);
                    },
                    error: function() {
                        $('#ajaxMessage').showAjaxMessage({
                            html: 'There is some error.Try after some time.',
                            type: 'error'
                        });
                    }
                });
            });





            $(".Del_btn").click(function() {
                var _id = $(this).data('id');

                let _url = "{{ url('pocket_setting/delete') }}";

                var _rc = confirm("Are you sure about this action? This cannot be undone!");
                if (_rc == true) {
                    axios.post(_url, {
                        id: _id
                    }).then((response) => {
                        toastr.success("successfully deleted");

                        $(this).parent().remove();



                    }).catch((error) => {
                        toastr.warning('somthing went wrong')
                    })

                }
            });

        });
    </script>
@endsection
