@extends('layouts.app')

<style>
    .input_fields_wrap .input_fields:first-child a.remove_field{display: none;}
</style>

@section('breadcrumbs')
<div id="breadcrumbBar" class="breadcrumb site_nav_links no_bdr_rad clearfix">
    <div class="col-md-3 col-sm-3 col-xs-2 no_pad">
        <button class="btn btn-info btn-xs" type="button" onclick="history.back()" title="Go Back"><span class="visible-xs"><i class="fa fa-arrow-left"></i></span><span class="hidden-xs">Back</span></button>
        <button class="btn btn-info btn-xs" onclick="redirectTo('<?= url('/pocket_setting/create'); ?>')" title="Refresh" type="button"><span class="visible-xs"><i class="fa fa-refresh"></i></span><span class="hidden-xs">Refresh</span></button>
    </div>
    <div class="col-md-6 col-sm-6 hidden-xs text-center">
        <h2 class="page-title">Create New Floor</h2>
    </div>
    <div class="col-md-3 col-sm-3 col-xs-10 no_pad">
        <ul class="text-right no_mrgn no_pad">
            <li><a href="{{ url('/home') }}">Home</a> <span class="fa fa-angle-right"></span></li>
            <li><a href="{{ url('/pocket_setting') }}">Pocket <span class="fa fa-angle-right"></span></a></li>
            <li>Form</li>
        </ul>
    </div>
</div>
@endsection

@section('content')
<div class="panel panel-primary">
    <div class="panel-heading">
        <h3 class="panel-title">Enter Pocket Information</h3>
    </div>
    <div class="panel-body">
        {!! Form::open(['method' => 'POST', 'url' => 'pocket_setting', 'id' => 'frm_head'  , 'class' => 'form-horizontal']) !!}
        <div class="form-group">
            <!-- <label for="name" class="col-md-4 control-label">Name</label> -->
            <div class="col-md-6 col-md-offset-3">
                <div class="form-group">
                <select name="chamber_id" id="chamber_id" class="form-control select2search">
                    <option value="">Select Chamber</option>
                    @foreach($chambers as $chamber)
                    <option value="{{ $chamber->id }}">{{ $chamber->chamber }}</option>
                    @endforeach
                </select>
            </div>
            </div>
            <div class="col-md-6 col-md-offset-3">
                <div class="form-group">
                <select name="floor_id" id="floor_id" class="form-control select2search">
                    <option value="">Select Floor</option>
                </select>
                </div>
            </div>
            <div class="col-md-8 col-md-offset-1">
                <div class="input_fields_wrap">
                    <div class="input_fields">
                        <div class="row clearfix">
                            <div class="col-md-6">
                                <input type="text" class="form-control" name="name[]" id="name[]" placeholder="Pocket name" required>
                            </div>
                            <div class="col-md-4">
                                <input type="text" class="form-control" name="capacity[]" id="capacity[]" placeholder="Capacity">
                            </div>
                            <div class="col-md-2">
                                <a href="#" class="remove_field btn btn-danger btn-block btn-sm" data-rel="">Remove</a>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
            <div class="col-md-2">
            <button class="add_field_button btn btn-default btn-block"><i class="fa fa-plus" aria-hidden="true"></i></button>
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-8 col-md-offset-4">
                <button type="button" id="reset_from" class="btn btn-info xsw_50">Reset</button>
                <input type="submit" class="btn btn-primary xsw_50" name="btnSave" value="Save">
            </div>
        </div>
        {!! Form::close() !!}
    </div>

</div>

<script type="text/javascript">
    $(document).on("change", "#chamber_id", function() {
        var _chamber_id = $("#chamber_id").val();
        $.ajax({
            url: "{{ URL::to('pocket_setting/floorListByChamber') }}",
            type: "post",
            data: {
                _token: csrfToken,
                chamber_id: _chamber_id
            },
            success: function(res) {
                $('#floor_id').html(res);
            },
            error: function(xhr, status) {
                alert('There is some error.Try after some time.');
            }
        });

    });
    $(document).ready(function () {
        var max_fields = 10; //maximum input boxes allowed
        var wrapper = $(".input_fields"); //Fields wrapper
        var add_button = $(".add_field_button"); //Add button ID

        var x = 1; //initlal text box count
        $(add_button).click(function (e) { //on add input button click
            var $this = $(this);
            var copyField = wrapper.clone();

            if (x < max_fields) { //max input box allowed
                x++; //text box increment
                //$(wrapper).append('<div class="text-center"><input type="text" class="duplicate_field" name="name[]" required/>&nbsp;<a href="#" class="remove_field btn btn-danger btn-sm">Remove </a></div>'); //add input box
                copyField.insertBefore(wrapper);
            }
            e.preventDefault();
        });
        var list = document.querySelector(".input_fields_wrap");
        $(".remove_field").on("click", function (e) { //user click on remove text
            // $(this).closest(".input_fields").remove()
            list.removeChild(list.childNodes[0]);
            // $(this).parents().closest('div.input_fields').remove();
            // $(this).parent().parent().parent('div.input_fields').remove();
            x--;
            e.preventDefault();
        });
    });

    $("#reset_from").click(function () {
        var _form = $("#frm_head");
        _form[0].reset();
    });
</script>
@endsection