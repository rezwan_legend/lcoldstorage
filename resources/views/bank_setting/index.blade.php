@extends('layouts.app')

@section('breadcrumbs')
<div id="breadcrumbBar" class="breadcrumb site_nav_links no_bdr_rad clearfix">
    <div class="col-md-2 col-sm-3 col-xs-2 no_pad">
        <button class="btn btn-info btn-xs" type="button" onclick="history.back()" title="Go Back"><span class="visible-xs"><i class="fa fa-arrow-left"></i></span><span class="hidden-xs">Back</span></button>
        <button class="btn btn-info btn-xs" onclick="redirectTo('<?= url('view-clear'); ?>')" title="Refresh" type="button"><span class="visible-xs"><i class="fa fa-refresh"></i></span><span class="hidden-xs">Refresh</span></button>
    </div>
    <div class="col-md-6 col-sm-6 hidden-xs text-center">
        <h2 class="page-title">{{$page_title}}</h2>
    </div>
    <div class="col-md-4 col-sm-3 col-xs-10 no_pad">
        <ul class="text-right no_mrgn no_pad">
            <li><a href="{{ url('/home') }}">Home</a> <i class="fa fa-angle-right"></i></li>
            <li >Bank setting</li>
        </ul>
    </div>
</div>
@endsection

@section('content')
<div class="well">

<div class="clearfix mb_10">
        <div class="text-center">
                @if(has_user_access('bank_setting_create'))
                <a class="btn btn-success btn-xs " href="{{ url ('/essential/bank_settings/create') }}"><i class="fa fa-plus"></i> New</a>
                @endif
                @if(has_user_access('bank_setting_delete'))
                <button type="button" class="btn btn-danger btn-xs " id="Del_btn" disabled><i class="fa fa-trash-o"></i> Delete</button>
                @endif
                <button class="btn btn-primary btn-xs " onclick="printDiv('print_area')"><i class="fa fa-print"></i> Print</button>           
        </div>
</div>
    <table width="100%">
        <tr>
            <td class="wmd_70">
                {!! Form::open(['method' => 'POST', 'id' => 'frmSearch', 'name' => 'frmSearch']) !!}

                <div class="input-group">
                    <div class="input-group-btn clearfix">
                        <?php echo number_dropdown(50, 550, 50, null, 'xsw_30') ?>

                        <div class="col-md-2 no_pad xsw_70">
                            <input type="text" class="form-control" id="srch" name="srch" placeholder="search" size="30"/>
                        </div>
                        <div class="col-md-2 no_pad xsw_100" style="width: auto">
                            <button type="button" id="search" class="btn btn-info xsw_50">Search</button>
                            <button type="button" id="clear_from" class="btn btn-warning xsw_50">Clear</button>
                        </div>
                    </div>
                </div>
                {!! Form::close() !!}
            </td>
           
        </tr>
    </table>
</div>

{!! Form::open(['method' => 'POST', 'id'=>'frmList','name'=>'frmList']) !!}
<div id="print_area">
    <?php print_header("Unit List", true, false); ?>
    <div id="ajax_content">
        @if(!empty($dataset))
        <div class="table-responsive">
            <table class="table table-bordered tbl_thin" id="check">
                <tr class="bg-info" id="r_checkAll">
                    <th class="text-center" style="width:3%;">#</th>
                    <th>Name</th>
                    <th>Last Modified</th>
                    <th>Created at</th>
                    <th class="text-center hip" style="width:10%;">Actions</th>
                    <th class="text-center hip" style="width:3%;"><input type="checkbox" id="check_all" value="all"></th>
                </tr>
                <?php
                $counter = 0;
                if (isset($_GET['page']) && $_GET['page'] > 1) {
                    $counter = ($_GET['page'] - 1) * $dataset->perPage();
                }
                ?>
                @foreach ($dataset as $data)
                <?php $counter++; ?>
                <tr onmouseover="change_color(this, true)" onmouseout="change_color(this, false)">
                    <td class="text-center">{{ $counter }}</td>
                    <td>{{ $data->name }}</td>
                    <td>{{ $data->modified_at ?? "" }}</td>
                    <td>{{ $data->created_at }}</td>
                    <td class="text-center hip" style="width:10%;">
                        @if(has_user_access('bank_settings_edit'))
                        <a class="btn btn-info btn-xs" href="{{ url('/essential/bank_settings/'.$data->_key.'/edit') }}"><i class="fa fa-edit"></i> Edit</a>
                        @endif
                    </td>
                    <td class="text-center hip">
                        @if(has_user_access('bank_settings_delete'))
                        <input type="checkbox" name="data[]" value="{{ $data->id }}">
                        @endif
                    </td>
                </tr>
                @endforeach
            </table>
        </div>
        @else
        <div class="alert alert-info">No records found.</div>
        @endif
    </div>
</div>
{!! Form::close() !!}
<script type="text/javascript">
    $(document).ready(function () {
        $(document).on("click", "#search", function () {
            var _url = "{{ URL::to('/essential/bank_settings/search') }}";
            var _form = $("#frmSearch");
            $.ajax({
                url: _url,
                type: "post",
                data: _form.serialize(),
                success: function (data) {
                    $('#ajax_content').html(data);
                },
                error: function (xhr, status) {
                    alert('There is some error.Try after some time.');
                }
            });
        });

        $(document).on("click", "#Del_btn", function () {
            var _url = "{{ URL::to ('/essential/bank_settings/delete') }}";
            var _form = $("#frmList");
            var _rc = confirm("Are you sure about this action? This cannot be undone!");
            if (_rc === true) {
                $.post(_url, _form.serialize(), function (resp) {
                    if (resp.success === true) {
                        $('#ajaxMessage').showAjaxMessage({html: resp.message, type: 'success'});
                        $("#search").trigger("click");
                    } else {
                        $('#ajaxMessage').showAjaxMessage({html: resp.message, type: 'error'});
                    }
                }, "json");
            }
        });
    });
</script>
@endsection