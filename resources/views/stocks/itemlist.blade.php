@extends('admin.layouts.column2')
@section('content')
<div id="breadcrumbBar" class="breadcrumb site_nav_links no_bdr_rad clearfix">
    <div class="col-md-3 col-sm-3 col-xs-2 cxs_2 no_pad">
        <button class="btn btn-info btn-xs" type="button" onclick="history.back()" title="Go Back"><span class="visible-xs"><i class="fa fa-arrow-left"></i></span><span class="hidden-xs">Back</span></button>
        <button class="btn btn-info btn-xs" onclick="redirectTo('<?= url('refresh_page') ?>')" title="Refresh" type="button"><span class="visible-xs"><i class="fa fa-refresh"></i></span><span class="hidden-xs">Refresh</span></button>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-6 cxs_10 text-center">
        <h2 class="page-title">Opening Stock</h2>
    </div>
    <div class="col-md-3 col-sm-3 col-xs-4 cxs_12 no_pad">
        <ul class="text-right no_mrgn">
            <li><a href="{{ url('/home') }}">Home</a> <span class="divider">/</span></li>
            <li><a href="{{ url('ricemill/stocks') }}">Opening Stock</a></li>
        </ul>
    </div>
</div>
<div class="panel panel-primary">
    <div class="panel-heading">
        <h3 class="panel-title">Opening Stock Information</h3>
    </div>
    <div class="panel panel-primary" style="border: none; padding-bottom: 0px">
        <div class="panel-body" style="padding-bottom: 0px; ">
            {!! Form::open(['method' => 'POST', 'url' => 'ricemill/stocks/add_item', 'id' => 'frmOrderItem']) !!}
            <input type="hidden" id="size_weight" name="size_weight" value="">
            <input type="hidden" id="sale_unit_weight" name="sale_unit_weight" value="">
            <div class="row clearfix" >
                <div class="col-md-2">
                    <div class="mb_10 clearfix">
                        <label for="net_qty">{{ trans('words.date') }}</label>
                        <input type="text" class="form-control pickdate" name="opening_date" id="opening_date" value="<?php echo date('d-m-Y'); ?>"  size="30" readonly>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="mb_10 clearfix">
                        <label for="institute">{{ trans('words.select_institute') }}</label>
                        <select class="form-control select2search"  id="institute" name="institute" required>
                            @foreach( $institutes as $institute )
                            <option value="{{ $institute->id }}">{{ $institute->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="mb_10 clearfix">
                        <label for="type">{{ trans('words.select_type') }}</label>
                        <select class="form-control select2search"  id="prod_type" name="prod_type">
                            <option value="">Type</option>
                            @foreach( $product_type as $prod_type )
                            <option value="{{ $prod_type->id }}">{{ $prod_type->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="mb_10 clearfix">
                        <label for="category"> {{ trans('words.select_category') }}</label>
                        <select class="form-control select2search"  id="category" name="category" required>
                            <option value="" class="">Category</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="mb_10 clearfix">
                        <label for="product"> {{ trans('words.select_product') }}</label>
                        <select class="form-control select2search"  id="product_id" name="product_id">
                            <option value=""> Product</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="mb_10 clearfix">
                        <label for="size">{{ trans('words.select_size') }}</label>
                        <select  class="form-control select2search" id="size_id" name="size_id">
                            <option value=""> Size</option>
                        </select> 
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="mb_10 clearfix">
                        <label for="unit">  {{ trans('words.select_unit') }}</label>
                        <select  class="form-control select2search" id="unit_id" name="unit_id">
                            <option value=""> Unit</option>
                        </select> 
                    </div>
                </div>

                <div class="col-md-2">
                    <div class="mb_10 clearfix">
                        <label for="qty"> {{ trans('words.select_Qty') }}</label>
                        <input type="number" class="form-control text-left" id="qty" name="qty" value="" placeholder="Qty" min="0" step="any">
                    </div>
                </div>
                <div class="col-md-2 rawopening">
                    <div class="mb_10 clearfix">
                        <label for="net_weight"> {{ trans('words.net_weight') }}</label>
                        <input type="number" class="form-control " id="net_weight" name="net_weight" value="" placeholder="Net Weight" min="0" step="any">
                    </div>
                </div>
                <div class="col-md-2 rawopening">
                    <div class="mb_10 clearfix">
                        <label for="unit_price">Select Price Unit</label>
                        <select  class="form-control select2search" id="price_unit_id" name="price_unit_id">
                            <option value="">{{ trans('words.select_price_unit') }}</option>
                        </select> 
                    </div>
                </div>
                <div class="col-md-2 rawopening">
                    <div class="mb_10 clearfix">
                        <label for="price_qty_unit">{{ trans('words.opening_qty') }}</label>
                        <input type="number" class="form-control text-left" id="price_qty_unit" name="price_qty_unit" value="" placeholder="Price Qty Unit" min="0" step="any">
                    </div>
                </div> 
                <div class="col-md-2">
                    <div class="mb_10 clearfix">
                        <label for="rate">{{ trans('words.rate') }} </label>
                        <input type="number" class="form-control text-left" id="rate" name="rate" value="" placeholder="rate" min="1" step="any">
                    </div>
                </div> 
                <div class="col-md-2">
                    <div class="mb_10 clearfix">
                        <label for="price"> {{ trans('words.total') }} {{ trans('words.price') }}</label>
                        <input type="number" class="form-control text-left" id="price" name="price" value="" placeholder="price" min="0" step="any">
                    </div>
                </div> 
                <div class="col-md-2 d-inline-block" style="margin-right: 0px; padding-right: 0px; margin-left: 0px; padding-left: 0px; margin-top: 27px; width: 6px;">
                    <div class="mb_10 clearfix">
                        <button type="button" class="btn btn-info btn-xs xsw_100" id="addItem" title="Add Item" style="height: 31px;width: 38px;"><i class="fa fa-share"></i></button>
                    </div>
                </div> 
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
<div class="clearfix">
    <div class="clearfix" id="order_itemlist_partial">
        @if (!empty($itemDataset) && count($itemDataset) > 0)
        <div class="table-responsive">
            <table class="table table-bordered tbl_thin" id="check">
                <tbody>
                    <tr class="bg_gray" id="r_checkAll">
                        <th class="text-center" style="width:5%;">#</th>
                        <th>Date</th>
                        <th>Category</th>
                        <th>Product</th>
                        <th>Size</th>
                        <th>Unit</th>
                        <th class="text-center">Qty</th>
                        <th class="text-center">Net Weight</th>
                        <th class="text-center">Avg Weight</th>
                        <th class="text-right">Rate</th>
                        <th class="text-right">Net price</th>
                        <th class="text-center">Action</th>
                    </tr>
                    <?php
                    $counter = 0;
                    $total_quantity = 0;
                    $total_net_qty = 0;
                    $total_net_weight = 0;
                    $total_price = 0;
                    ?>
                    @foreach ($itemDataset as $data)
                    <?php
                    $counter++;
                    $total_quantity += $data->quantity;
                    $total_net_qty += $data->net_qty;
                    $total_net_weight += $data->net_weight;
                    $total_price += $data->price;
                    ?>   
                    <tr>
                        <td class="text-center" style="width:5%;">{{ $counter }}</td>
                        <td class="text-left">{{ date_dmy($data->date) }}</td>
                        <td class="text-left">{{!empty( $data->category) ? $data->category->name : '' }}</td>
                        <td class="text-left">{{!empty( $data->product) ? $data->product->name : '' }}</td>
                        <td class="text-left">{{!empty( $data->productSize) ? $data->productSize->name : '' }}</td>
                        <td class="text-left">{{!empty( $data->productUnit) ? $data->productUnit->name : '' }}</td>
                        <td class="text-center">{{ $data->quantity }}</td>
                        <td class="text-center">{{ $data->net_weight }}</td>
                        <td class="text-center">{{ $data->avg_weight }}</td>
                        <td class="text-right">{{ $data->rate }}</td>
                        <td class="text-right">{{ $data->price }}</td>
                        <td class="text-center hip">
                            <a class="color_danger" title="Delete Item" href="{{ url('ricemill/stocks/remove_item/'.$data->id) }}" onclick="return confirm('Are you sure about this action?')"><i class="fa fa-trash-o"></i></a>
                            <a class="btn btn-info btn-xs" href="{{ url('ricemill/stocks/edit/'.$data->id) }}"><i class="fa fa-edit"></i> Edit</a>
                        </td>
                    </tr>
                    @endforeach
                    <tr class="bg_gray">
                        <th colspan="6" class="text-center" style="background:#ddd; font-weight:600; width:5%;">{{ trans('words.total') }}</th>
                        <th class="text-center"> {{ $total_quantity }}</th>
                        <th class="text-center">{{ $total_net_weight }}</th>
                        <th class="text-right" colspan="2"></th>
                        <th class="text-right">{{ $total_price }}</th>
                        <th class="text-right"></th>
                    </tr>
                </tbody>
            </table>
        </div>
        @else
        <div class="alert alert-info">No records found!</div>
        @endif
    </div>
</div>
<div class="text-center" style="margin-top:10px;">    
    <a class="btn btn-primary" href="{{ url('/ricemill/stocks/') }}">{{ trans('words.save') }}</a>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $(document).on("click", "button", "#frmOrderItem", function (event) {
            $('#ajaxMessage').hide();
            var institute_id = $("#institute").val();
            var _url = baseUrl + "/ricemill/stocks/addItem";
            var _form = $("#frmOrderItem");
            $.post(_url, _form.serialize(), function (res) {
                if (res.success === true) {
                    render_opening_items(institute_id);
                } else {
                    $("#ajaxMessage").showAjaxMessage({html: res.message, type: "error"});
                }
            }, "json");
            event.preventDefault();
            return false;
        });

        $(document).on("input", "#qty ,#rate", function (e) {
            var prodtypeId = $("#prod_type").val();
            if (prodtypeId == RICE_EBAG || prodtypeId == RICE_SPARE || prodtypeId == RICE_OTHERS) {
                ebag_net_price();
            } else {
                _net_weight();

            }
            e.preventDefault();
        });

        $(document).on("change", "#size_id", function (e) {
            $("#size_weight").val(get_selected_option_info(this, 'data-weight'));
            _net_weight();
            e.preventDefault();
        });

        $(document).on("change", "#price_unit_id", function (e) {
            $("#sale_unit_weight").val(get_selected_option_info(this, 'data-weight'));
            _price_qty_unit();
            e.preventDefault();
        });

        $(document).on("change", "#prod_type", function (e) {
            var Id = this.value
            if ((Id == RICE_EBAG) || (Id == RICE_SPARE) || (Id == RICE_OTHERS)) {
                $(".rawopening").hide();
            } else {
                $(".rawopening").show();
            }
            var _companyId = $("#institute").val();
            var url = "{{ URL::to('category/categoryByType') }}";
            $.ajax({
                type: 'POST',
                url: url,
                data: {
                    companyId: _companyId,
                    productTypeId: this.value,
                    _token: _token
                },
                success: function (response) {
                    $("#category").html(response.category);
                    $("#size_id").html(response.sizes);
                    $("#unit_id").html(response.units);
                    $("#price_unit_id").html(response.price_units);
                }
            });
        });

        $(document).on("change", "#category ", function (e) {
            var _companyId = $("#institute").val();
            var _categoryId = $("#category").val();
            var _productType = $("#prod_type").val();
            var url = "{{ URL::to('product/prod_by_category') }}";
            $.ajax({
                type: 'POST',
                url: url,
                data: {companyId: _companyId, categoryId: _categoryId, productType: _productType, _token: _token},
                success: function (response) {
                    $("#product_id").html(response);
                }
            });
        });

        $(document).on("input", "#net_weight", function (e) {
            _price_qty_unit();
            e.preventDefault();
        });

        $(document).on("input", "#price_qty_unit", function (e) {
            net_price();
            e.preventDefault();
        });

        $(document).on("input", "#price", function (e) {
            find_rate();
            e.preventDefault();
        });
    });

    function _net_weight() {
        var _size_weight = $("#size_weight").val();
        var _qty = Number($("#qty").val());
        var _net_weight = 0;
        if (isNaN(_size_weight)) {
            _size_weight = 0;
        }
        if (isNaN(_qty)) {
            _qty = 0;
        }
        _net_weight = (parseFloat(_qty) * parseFloat(_size_weight)).toFixed(2);
        $("#net_weight").val(_net_weight);
        _price_qty_unit();
    }

    function net_price() {
        var _rate = Number($("#rate").val());
        var _price_qty_unit = $("#price_qty_unit").val();
        var _price = 0;
        if (isNaN(_rate)) {
            _rate = 0;
        }
        if (isNaN(_price_qty_unit)) {
            _price_qty_unit = 0;
        }
        _price = (parseFloat(_rate) * parseFloat(_price_qty_unit)).toFixed(3);
        $("#price").val(_price);
    }

    function ebag_net_price() {
        var _rate = Number($("#rate").val());
        var _qty = Number($("#qty").val());
        var _price = 0;
        if (isNaN(_rate)) {
            _rate = 0;
        }
        if (isNaN(_qty)) {
            _qty = 0;
        }
        _price = (_rate * _qty).toFixed(3);
        $("#price").val(_price);
    }

    function render_opening_items(institute_id) {
        var _url = baseUrl + "/ricemill/stocks/search_itemlist";
        var _date = $("#opening_date").val();
        var _formData = {};
        _formData.date = _date;
        _formData.institute_id = institute_id;
        _formData._token = _token;
        $.ajax({
            url: _url,
            type: "POST",
            data: _formData,
            success: function (res) {
                $("#order_itemlist_partial").html(res);
                $("#qty").val("");
                $("#net_weight").val("");
                $("#price_qty_unit").val("");
                $("#rate").val("");
                $("#price").val("");
            },
            error: function (xhr, status) {
                alert('There is some error.Try after some time.');
            }
        });
    }

    function _price_qty_unit() {
        var _unit_value = $("#unit_value").val();
        var _net_weight = Number($("#net_weight").val());
        var _price_unit_id = $("#price_unit_id").val();
        var _sale_unit_weight = Number($("#sale_unit_weight").val());
        var _qty = Number($("#qty").val());
        if (isNaN(_qty)) {
            _qty = 0;
        }
        if (_price_unit_id == RICE_PICES) {
            var _pricePcs = (_net_weight / _qty).toFixed(2);
            $("#price_qty_unit").val(_pricePcs);
        } else if (_price_unit_id == RICE_BAG) {
            $("#price_qty_unit").val(_qty);
        } else if (_price_unit_id == RICE_KG) {
            $("#price_qty_unit").val(_net_weight);
        } else if (_price_unit_id == RICE_MON) {
            var _mon = (_net_weight / _sale_unit_weight).toFixed(2);
            $("#price_qty_unit").val(_mon);
        } else if (_price_unit_id == RICE_BANGLA_MON) {
            var _bangla_mon = (_net_weight / _sale_unit_weight).toFixed(2);
            $("#price_qty_unit").val(_bangla_mon);
        } else {
            var _price_qty_unit = (parseFloat(_net_weight) / _unit_value).toFixed(2);
            $("#price_qty_unit").val(_price_qty_unit);
        }

        net_price();
    }

    function find_rate() {
        var _purchaseQty = Number($("#price_qty_unit").val());
        var _price = Number($("#price").val());
        var _rate = 0;
        if (isNaN(_purchaseQty)) {
            _purchaseQty = 0;
        }
        if (isNaN(_price)) {
            _price = 0;
        }
        _rate = (parseFloat(_price) / parseFloat(_purchaseQty)).toFixed(3);
        $("#rate").val(_rate);
    }
</script>
@endsection