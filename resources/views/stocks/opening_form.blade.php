@extends('admin.layouts.column2')
@section('content')
<div id="breadcrumbBar" class="breadcrumb site_nav_links no_bdr_rad clearfix">
    <div class="col-md-3 col-sm-3 col-xs-2 cxs_2 no_pad">
        <button class="btn btn-info btn-xs" type="button" onclick="history.back()" title="Go Back"><span class="visible-xs"><i class="fa fa-arrow-left"></i></span><span class="hidden-xs">Back</span></button>
        <button class="btn btn-info btn-xs" onclick="redirectTo('<?= url('view - clear') ?>')" title="Refresh" type="button"><span class="visible-xs"><i class="fa fa-refresh"></i></span><span class="hidden-xs">Refresh</span></button>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-6 cxs_10 text-center">
        <h2 class="page-title">Raw Opening Stocks</h2>
    </div>
    <div class="col-md-3 col-sm-3 col-xs-4 cxs_12 no_pad">
        <ul class="text-right no_mrgn">
            <li><a href="{{ url('/home') }}"><i class="fa fa-fw fa-dashboard"></i> Dashboard</a> <span class="divider">/</span></li>
            <li><span>Stock</span></li>
        </ul>                            
    </div>
</div>
{!! Form::open(['method' => 'POST', 'url' => '',   'class' => 'search-form', 'id'=>'frmStock','name'=>'frmStock']) !!} 
<div class="row" style="margin-bottom: 20px;">
    <div class="col-md-6">
        <div class="form-group">
            <label for="datepicker" class="col-md-3">Date</label>
            <div class="col-md-8">
                <div class="input-group">
                    <input type="text" id="datepicker" class="form-control pickdate" name="date" placeholder="(dd-mm-yyyy)" value="<?= date('d-m-Y'); ?>" readonly>
                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                    <small class="text-danger">{{ $errors->first('date') }}</small>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="table-responsive">
    <table class="table table-bordered tbl_thin" id="check">
        <thead>
            <tr class="bg_gray">
                <th>#</th>
                <th>Category</th>
                <th>Product</th>
                <th class="text-center">Size</th>
                <th class="text-center">Quantity</th>
                <th class="text-center">Net Weight (Kg)</th>
                <th class="text-center">Per Kg Price</th>
                <th class="text-center">Total Price</th>
            </tr>
        </thead>
        <?php $sl = 1; ?>
        <tbody>
            @foreach ($dataset as $data)
            <?php
            $os_qty = $stock->sumOsQuantity($data->id);
            $os_weight = $stock->sumOsWeight($data->id);
            $os_per_kg_price = $stock->sumOsPerKgPrice($data->id);
            $os_total_price = $stock->sumOsTotalPrice($data->id);
            ?>
            <tr>
                <td class="text-center">{{ $sl }}
                <td>{{ $data->category->name }}<input type="hidden" value="{{ $data->category_id }}" name="category_id[]"> <input type="hidden" class="form-control" id="bag_weight_{{ $data->id }}"></td>
                <td>{{ $data->name }} <input type="hidden" value="{{ $data->id }}" name="product_id[]" ></td>
                <td class="text-center">{{ $data->weight }} </td>
                <td class="text-center"><input type="number" class="form-control text-center qty" data-info="{{ $data->id }}" id="quantity_{{ $data->id }}" name="quantity[]" value="{{ $os_qty }}"></td>
                <td class="text-center"><input type="number" class="form-control text-center qty" data-info="{{ $data->id }}" id="net_weight_{{ $data->id }}" name="net_weight[]" value="{{ $os_weight }}" step="any"></td> 
                <td class="text-center"><input type="number" class="form-control text-center qty" data-info="{{ $data->id }}" id="per_kg_price_{{ $data->id }}" name="per_kg_price[]" value="{{ $os_per_kg_price }}" step="any"></td> 
                <td class="text-center"><input type="number" class="form-control text-center" id="total_price_{{ $data->id }}" name="total_price[]" value="{{ $os_total_price }}" readonly></td> 
            </tr>
            <?php $sl++; ?>
            @endforeach
        </tbody>
    </table>
</div>
<div class="col-md-12">
    <div class="form-group">
        <div class="text-center">
            <button type="reset" class="btn btn-info">Reset</button>
            <button type="submit" class="btn btn-primary" id="btnPurchase">Save</button>
        </div>
    </div>
</div>
{!! Form::close() !!}
<script type="text/javascript">
    $(document).ready(function () {
        $(document).on("input", ".qty", function (e) {
            var id = $(this).attr('data-info');
            var quantity = parseFloat(document.getElementById("quantity_" + id).value);
            var per_kg_price = parseFloat(document.getElementById("per_kg_price_" + id).value);
            var net_weight = parseFloat(document.getElementById("net_weight_" + id).value);
            var total_price = 0;

            if (isNaN(quantity) || quantity == '') {
                quantity = 0;
            }
            if (isNaN(per_kg_price) || per_kg_price == '') {
                per_kg_price = 0;
            }

            total_price = net_weight * per_kg_price;

            document.getElementById("total_price_" + id).value = total_price;
            e.preventDefault();
        });

        $(document).on("submit", "#frmStock", function (e) {
            var _form = $(this);
            var _url = "{{ URL::to ('main/raw/openingstocks') }}";
            $.post(_url, _form.serialize(), function (res) {
                if (res.success === true) {
                    _form[0].reset();
                    redirectTo(res.url);
                    $("#ajaxMessage").showAjaxMessage({html: res.message, type: 'success'});
                } else {
                    $("#ajaxMessage").showAjaxMessage({html: res.message, type: 'error'});
                }
            }, "json");
            e.preventDefault();
            return false;
        });
    });
</script>
@endsection