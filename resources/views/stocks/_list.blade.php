<?php
$_total = [];
?>
<div id="print_area">
    {{ print_header("Stocks Information", true, false, [], $institute_id) }}
    @if (!empty($dataset) && count($dataset) > 0)
    <div class="table-responsive">
        <table class="table table-bordered table-striped tbl_thin">
            <tr class="bg_highlight" id="r_checkAll">
                <th class="text-center" style="width:3%;">#</th>
                <th class="text-center">Institute</th>
                <th class="text-center">Product</th>
                <th class="text-center">Size</th>
                <th class="text-center no_pad">
                    <table class="table table-bordered tbl_thin no_mrgn">
                        <tr class="bg_highlight">
                            <th class="text-center" colspan="3">Quantity</th>
                        </tr>
                        <tr class="bg_highlight">
                            <th class="text-center" style="width: 33.33%">In</th>
                            <th class="text-center" style="width: 33.33%">Out</th>
                            <th class="text-center" style="width: 33.33%">Remain</th>
                        </tr>
                    </table>
                </th>
                <th class="text-center no_pad">
                    <table class="table table-bordered tbl_thin no_mrgn">
                        <tr class="bg_highlight">
                            <th class="text-center" colspan="3">Net Weight</th>
                        </tr>
                        <tr class="bg_highlight">
                            <th class="text-center" style="width: 33.33%">In</th>
                            <th class="text-center" style="width: 33.33%">Out</th>
                            <th class="text-center" style="width: 33.33%">Remain</th>
                        </tr>
                    </table>
                </th>
                <th class="text-right" style="width: 115px;">Rate</th>
                <th class="text-right" style="width: 115px;">Stock Price</th>
            </tr>
            <?php
            $_cond = [];
            $counter = 0;
            if (isset($_GET['page']) && $_GET['page'] > 1) {
                $counter = ($_GET['page'] - 1) * $dataset->perPage();
            }
            $total_quantity_in = 0;
            $total_quantity_out = 0;
            $total_quantity_remain = 0;

            $total_net_quantity_in = 0;
            $total_net_quantity_out = 0;
            $total_net_quantity_remain = 0;

            $total_net_weight_in = 0;
            $total_net_weight_out = 0;
            $total_net_weight_remain = 0;
            $total_price_in = 0;
            $total_price_out = 0;
            $per_kg_rate = 0;
            $total_price_remain = 0;
            ?>
            @foreach ($dataset as $data)
            <?php
            if (!empty($productTypeId)) {
                $_cond['product_type_id'] = $productTypeId;
            }
            if (!empty($categoryId)) {
                $_cond['category_id'] = $categoryId;
            }
            if (!empty($productId)) {
                $_cond['product_id'] = $productId;
            }
            if (!empty($sizeId)) {
                $_cond['size_id'] = $sizeId;
            }
            $counter++;
            $total_quantity_in += $_qty_in = $data->sum_in($data->company_id, $data->product_id, $data->size_id, 'quantity', $_cond);
            $total_quantity_out += $_qty_out = $data->sum_out($data->company_id, $data->product_id, $data->size_id, 'quantity', $_cond);
            $total_quantity_remain += $_qty_remain = ($_qty_in - $_qty_out);

            $total_net_weight_in += $_net_weight_in = $qty_in = $data->sum_in($data->company_id, $data->product_id, $data->size_id, 'net_weight', $_cond);
            $total_net_weight_out += $_net_weight_out = $qty_in = $data->sum_out($data->company_id, $data->product_id, $data->size_id, 'net_weight', $_cond);
            $total_net_weight_remain += $_net_weight_remain = ($_net_weight_in - $_net_weight_out);

            $price_in = $qty_in = $data->sum_in($data->company_id, $data->product_id, $data->size_id, 'price', $_cond);
            $price_out = $qty_in = $data->sum_out($data->company_id, $data->product_id, $data->size_id, 'price', $_cond);
            $total_price_remain += $stock_price = ($price_in - $price_out);
            if ($_net_weight_remain > 0) {
                $per_kg_rate = round($stock_price / $_net_weight_remain, 2);
            }
            ?>
            <tr>
                <td class="text-center">{{ $counter }}</td>
                <td> {{ !empty($data->institute) ? $data->institute->name : '' }} </td>
                <td> {{ !empty($data->product) ? $data->product->name : '' }} </td>
                <td>{{ !empty($data->productSize) ? $data->productSize->name : '' }}</td>
                <td class="text-center no_pad">
                    <table class="table table-bordered tbl_thin no_mrgn">
                        <tr>
                            <td class="text-center" style="width: 33.33%">{{ $_qty_in }}</td>
                            <td class="text-center" style="width: 33.33%">{{ $_qty_out }}</td>
                            <td class="text-center" style="width: 33.33%">{{ $_qty_remain }}</td>
                        </tr>
                    </table>
                </td>
                <!--Net Quantity section-->

                <td class="text-center no_pad">
                    <table class="table table-bordered tbl_thin no_mrgn">
                        <tr>
                            <td class="text-center" style="width: 33.33%">{{ $_net_weight_in }}</td>
                            <td class="text-center" style="width: 33.33%">{{ $_net_weight_out }}</td>
                            <td class="text-center" style="width: 33.33%">{{ $_net_weight_remain }}</td>
                        </tr>
                    </table>
                </td>
                <td class="text-right">{{ $per_kg_rate }}</td>
                <td class="text-right">{{ $stock_price }}</td>
            </tr>
            @endforeach
            <tr class="bg_gray">
                <th class="text-right" colspan="4" style="background:#ddd; font-weight:600;">Total</th>
                <th class="text-center no_pad">
                    <table class="table table-bordered tbl_thin no_mrgn">
                        <tr class="bg_gray">
                            <th class="text-center" style="width: 33.33%">{{ $total_quantity_in }}</th>
                            <th class="text-center" style="width: 33.33%">{{ $total_quantity_out }}</th>
                            <th class="text-center" style="width: 33.33%">{{ $total_quantity_remain }}</th>
                        </tr>
                    </table>
                </th>

                <th class="text-center no_pad">
                    <table class="table table-bordered tbl_thin no_mrgn">
                        <tr class="bg_gray">
                            <th class="text-center" style="width: 33.33%">{{ $total_net_weight_in }}</th>
                            <th class="text-center" style="width: 33.33%">{{ $total_net_weight_out }}</th>
                            <th class="text-center" style="width: 33.33%">{{ $total_net_weight_remain }}</th>
                        </tr>
                    </table>
                </th>
                <th class="text-right"></th>
                <th class="text-right">{{ $total_price_remain }}</th>
            </tr>
        </table>
    </div>

    <div class="row clearfix" style="padding-top:60px;">
        <div class="col-md-4 form-group mp_25 pull-left">
            <div style="border-top:1px solid #000000;text-align:center;">Accounts Manager</div>
        </div>
        <div class="col-md-4 col-md-offset-4 pull-right  form-group mp_25">
            <div style="border-top:1px solid #000000;text-align:center;">IT Officer</div>
        </div>
    </div>
    @else
    <div class="alert alert-info">No records found!</div>
    @endif
</div>