@extends('layouts.app')
@section('content')
<div id="breadcrumbBar" class="breadcrumb site_nav_links no_bdr_rad clearfix">
    <div class="col-md-3 col-sm-3 col-xs-2 cxs_2 no_pad">
        <button class="btn btn-info btn-xs" type="button" onclick="history.back()" title="Go Back"><span class="visible-xs"><i class="fa fa-arrow-left"></i></span><span class="hidden-xs">Back</span></button>
        <button class="btn btn-info btn-xs" onclick="redirectTo('<?= url('view-clear'); ?>')" title="Refresh" type="button"><span class="visible-xs"><i class="fa fa-refresh"></i></span><span class="hidden-xs">Refresh</span></button>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-6 cxs_10 text-center">
        <h2 class="page-title">Stocks List</h2>
    </div>
    <div class="col-md-3 col-sm-3 col-xs-4 cxs_12 no_pad">
        <ul class="text-right no_mrgn">
            <li><a href="{{ url('/home') }}">Home</a> <span class="divider">/</span></li>
            <li>Stock</li>
        </ul>                            
    </div>
</div>

<div class="well">
    <table width="100%">
        <tr>
            <td class="wmd_70">
                {!! Form::open(['method' => 'POST',  'class' => 'search-form', 'id' => 'frmSearch', 'name' => 'frmSearch']) !!} 
                <div class="input-group">
                    <div class="input-group-btn clearfix">
                        <?php echo number_dropdown(50, 500, 50, null, 'xsw_50') ?>
                        <div style="width:12%;" class="col-md-2 col-sm-3 no_pad xsw_50">
                            <select class="form-control select2search" id="institute_id" name="institute_id" required>
                                @foreach( $institutes as $institute )
                                <option value="{{ $institute->id }}">{{ $institute->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-2 col-sm-3 no_pad xsw_50" style="width:12%;">
                            <select  class="form-control select2search"  id="prod_type" name="prod_type">
                                <option value="">Product Type</option>
                                @foreach( $product_type as $product )
                                <option value="{{ $product->id }}">{{ $product->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-2 col-sm-3 no_pad xsw_50" style="width:10%;">
                            <select class="form-control select2search"  id="category" name="category" >
                                <option value="">Category</option>
                            </select>
                        </div>  
                        <div class="col-md-2 col-sm-3 no_pad xsw_50" style="width:10%;">
                            <select  class="form-control select2search" id="product_id" name="product_id">
                                <option value="">Product</option>
                            </select>
                        </div>
                        <div class="col-md-2 col-sm-3 no_pad xsw_50" style="width:10%;">
                            <select class="form-control select2search" id="size_id" name="size_id">
                                <option value="">Sizes</option>
                            </select>
                        </div>
                        <div class="col-md-2 col-sm-4 no_pad xsw_50" style="width:auto;">
                            <button type="button" class="btn btn-info" id="search">Search</button>
                            <button type="button" class="btn btn-warning" id="clear_from">Clear</button>
                            <a class="btn btn-success" href="{{ url ('stocks/opening_stock') }}"><i class="fa fa-plus"></i> Opening Stocks</a>
                            <button class="btn btn-primary" onclick="printDiv('print_area')"><i class="fa fa-print"></i> Print</button> 
                        </div>
                    </div>
                </div>
                {!! Form::close() !!}  
            </td>
        </tr>
    </table>
</div>
<div class="clearfix"></div>
<div class="order-list">
    <div style="display:none;" id="ajaxmsg">
    
    </div>
    <div id="ajax_content"></div>
</div>


<script type="text/javascript">
    $(document).ready(function () {
        $("#search").click(function () {
            var _url = "{{ URL::to('ricemill/stocks/search') }}";
            var _form = $("#frmSearch");
            $.ajax({
                url: _url,
                type: "post",
                data: _form.serialize(),
                success: function (data) {
                    $("#ajax_content").html(data);
                },
                error: function (xhr, status) {
                    alert('There is some error.Try after some time.');
                }
            });
        });
    });
</script>
@endsection