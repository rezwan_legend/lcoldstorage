@if (!empty($itemDataset) && count($itemDataset) > 0)
<div class="table-responsive">
    <table class="table table-bordered tbl_thin" id="check">
        <tbody>
            <tr class="bg_gray" id="r_checkAll">
                <th class="text-center" style="width:5%;">#</th>
                <th>Date</th>
                <th>Category</th>
                <th>Product</th>
                <th>Size</th>
                <th>Unit</th>
                <th class="text-center">Qty</th>
                <th class="text-center">Net Weight</th>
                <th class="text-center">Avg Weight</th>
                <th class="text-right">Rate</th>
                <th class="text-right">Net price</th>
                <th class="text-center">Action</th>
            </tr>
            <?php
            $counter = 0;
            $total_quantity = 0;
            $total_net_qty = 0;
            $total_net_weight = 0;
            $total_price = 0;
            ?>
            @foreach ($itemDataset as $data)
            <?php
            $counter++;
            $total_quantity += $data->quantity;
            $total_net_qty += $data->net_qty;
            $total_net_weight += $data->net_weight;
            $total_price += $data->price;
            ?>   
            <tr>
                <td class="text-center" style="width:5%;">{{ $counter }}</td>
                <td class="text-left">{{ date_dmy($data->date) }}</td>
                <td class="text-left">{{!empty( $data->category) ? $data->category->name : '' }}</td>
                <td class="text-left">{{!empty( $data->product) ? $data->product->name : '' }}</td>
                <td class="text-left">{{!empty( $data->productSize) ? $data->productSize->name : '' }}</td>
                <td class="text-left">{{!empty( $data->productUnit) ? $data->productUnit->name : '' }}</td>
                <td class="text-center">{{ $data->quantity }}</td>
                <td class="text-center">{{ $data->net_weight }}</td>
                <td class="text-center">{{ $data->avg_weight }}</td>
                <td class="text-right">{{ $data->rate }}</td>
                <td class="text-right">{{ $data->price }}</td>
                <td class="text-center hip">
                    <a class="color_danger" title="Delete Item" href="{{ url('ricemill/stocks/remove_item/'.$data->id) }}" onclick="return confirm('Are you sure about this action?')"><i class="fa fa-trash-o"></i></a>
                    <a class="btn btn-info btn-xs" href="{{ url('ricemill/stocks/edit/'.$data->id) }}"><i class="fa fa-edit"></i> Edit</a>
                </td>
            </tr>
            @endforeach
            <tr class="bg_gray">
                <th colspan="6" class="text-center" style="background:#ddd; font-weight:600; width:5%;">{{ trans('words.total') }}</th>
                <th class="text-center"> {{ $total_quantity }}</th>
                <th class="text-center">{{ $total_net_weight }}</th>
                <th class="text-right" colspan="2"></th>
                <th class="text-right">{{ $total_price }}</th>
                <th class="text-right"></th>
            </tr>
        </tbody>
    </table>
</div>
@else
<div class="alert alert-info">No records found!</div>
@endif