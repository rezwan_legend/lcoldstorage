@extends('admin.layouts.column2')
@section('content')
<div id="breadcrumbBar" class="breadcrumb site_nav_links no_bdr_rad clearfix">
    <div class="col-md-3 col-sm-3 col-xs-2 cxs_2 no_pad">
        <button class="btn btn-info btn-xs" type="button" onclick="history.back()" title="Go Back"><span class="visible-xs"><i class="fa fa-arrow-left"></i></span><span class="hidden-xs">Back</span></button>
        <button class="btn btn-info btn-xs" onclick="redirectTo('<?= url('refresh_page') ?>')" title="Refresh" type="button"><span class="visible-xs"><i class="fa fa-refresh"></i></span><span class="hidden-xs">Refresh</span></button>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-6 cxs_10 text-center">
        <h2 class="page-title">Opening Stock</h2>
    </div>
    <div class="col-md-3 col-sm-3 col-xs-4 cxs_12 no_pad">
        <ul class="text-right no_mrgn">
            <li><a href="{{ url('/home') }}">Home</a> <span class="divider">/</span></li>
            <li><a href="{{ url('ricemill/stocks') }}">Opening Stock</a></li>
        </ul>
    </div>
</div>
<div class="panel panel-primary">
    <div class="panel-heading">
        <h3 class="panel-title">Opening Stock Information</h3>
    </div>
    <div class="panel panel-primary" style="border: none; padding-bottom: 0px">
        <div class="panel-body" style="padding-bottom: 0px; ">
            {!! Form::open(['method' => 'POST', 'url' => 'ricemill/stocks/update_item', 'id' => 'frm_purchase']) !!}
            <input type="hidden" id="size_weight" name="size_weight" value="{{ $sizeWeight }}">
            <input type="hidden" id="sale_unit_weight" name="sale_unit_weight" value="">
            <input type="hidden" id="stock_id" name="stock_id" value="{{ $data->id }}">
            <div class="row clearfix">
                <div class="col-md-2">
                    <div class="mb_10 clearfix">
                        <label for="net_qty">{{ trans('words.date') }}</label>
                        <input type="text" class="form-control pickdate" name="opening_date" id="opening_date" value="{{ date_dmy($data->date) }}"  size="30" readonly>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="mb_10 clearfix">
                        <label for="institute">{{ trans('words.select_institute') }}</label>
                        <select class="form-control select2search"  id="institute" name="institute" required>
                            @foreach( $institutes as $institute )
                            <option value="{{ $institute->id }}" @if($institute->id == $data->company_id) {{ 'selected' }} @endif>{{ $institute->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="mb_10 clearfix">
                        <label for="type">{{ trans('words.select_type') }}</label>
                        <select class="form-control select2search"  id="prod_type" name="prod_type">
                            <option value="">Type</option>
                            @foreach( $product_type as $prod_type )
                            <option value="{{ $prod_type->id }}"  @if($prod_type->id == $data->product_type_id) {{ 'selected' }} @endif>{{ $prod_type->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="mb_10 clearfix">
                        <label for="category"> {{ trans('words.select_category') }}</label>
                        <select class="form-control select2search"  id="category" name="category" required>
                            <option value="" class="">Category</option>
                            @foreach( $categories as $category )
                            <option value="{{ $category->id }}"  @if($category->id == $data->category_id) {{ 'selected' }} @endif>{{ $category->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="mb_10 clearfix">
                        <label for="product"> {{ trans('words.select_product') }}</label>
                        <select class="form-control select2search"  id="product_id" name="product_id">
                            <option value=""> Product</option>
                            @foreach( $products as $product )
                            <option value="{{ $product->id }}"  @if($product->id == $data->product_id) {{ 'selected' }} @endif>{{ $product->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="mb_10 clearfix">
                        <label for="size">{{ trans('words.select_size') }}</label>
                        <select  class="form-control select2search" id="size_id" name="size_id">
                            <option value=""> Size</option>
                            @foreach( $sizes as $size )
                            <option value="{{ $size->id }}" data-weight="{{ $size->weight }}"  @if($size->id == $data->size_id) {{ 'selected' }} @endif>{{ $size->name }}</option>
                            @endforeach
                        </select> 
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="mb_10 clearfix">
                        <label for="unit">  {{ trans('words.select_unit') }}</label>
                        <select  class="form-control select2search" id="unit_id" name="unit_id">
                            <option value=""> Unit</option>
                            @foreach( $unites as $unit )
                            <option value="{{ $unit->id }}"  @if($unit->id == $data->unit_id) {{ 'selected' }} @endif>{{ $unit->name }}</option>
                            @endforeach
                        </select> 
                    </div>
                </div>

                <div class="col-md-2">
                    <div class="mb_10 clearfix">
                        <label for="qty"> {{ trans('words.select_Qty') }}</label>
                        <input type="number" class="form-control text-left" id="qty" name="qty" value="{{ $data->quantity }}" placeholder="Qty" min="0" step="any">
                    </div>
                </div>
                <div class="col-md-2 rawopening">
                    <div class="mb_10 clearfix">
                        <label for="net_weight"> {{ trans('words.net_weight') }}</label>
                        <input type="number" class="form-control " id="net_weight" name="net_weight" value="{{ $data->net_weight }}" placeholder="Net Weight" min="0" step="any">
                    </div>
                </div>
                <div class="col-md-2 rawopening">
                    <div class="mb_10 clearfix">
                        <label for="unit_price">Select Price Unit</label>
                        <select  class="form-control select2search" id="price_unit_id" name="price_unit_id">
                            <option value="">{{ trans('words.select_price_unit') }}</option>
                            @foreach( $priceUnites as $price )
                            <option value="{{ $price->id }}"  @if($price->id == $data->price_unit_id) {{ 'selected' }} @endif>{{ $price->unit_name }}</option>
                            @endforeach
                        </select> 
                    </div>
                </div>
                <div class="col-md-2 rawopening">
                    <div class="mb_10 clearfix">
                        <label for="price_qty_unit">{{ trans('words.opening_qty') }}</label>
                        <input type="number" class="form-control text-left" id="price_qty_unit" name="price_qty_unit" value="{{ $data->price_unit_qty }}" placeholder="Price Qty Unit" min="0" step="any">
                    </div>
                </div> 
                <div class="col-md-2">
                    <div class="mb_10 clearfix">
                        <label for="rate">{{ trans('words.rate') }} </label>
                        <input type="number" class="form-control text-left" id="rate" name="rate" value="{{ $data->rate }}" placeholder="rate" min="1" step="any">
                    </div>
                </div> 
                <div class="col-md-2">
                    <div class="mb_10 clearfix">
                        <label for="price"> {{ trans('words.total') }} {{ trans('words.price') }}</label>
                        <input type="number" class="form-control text-left" id="price" name="price" value="{{ $data->price }}" placeholder="price" min="0" step="any">
                    </div>
                </div> 

                <div class="text-center">
                    <button type="submit" class="btn btn-primary" id="btnItemUpdate">{{ trans('words.update') }}</button>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $(document).on("submit", "#btnItemUpdate", function (event) {
            $('#ajaxMessage').hide();
            var _url = baseUrl + "/ricemill/stocks/update_item";
            var _form = $("#frmOrderItem");
            $.post(_url, _form.serialize(), function (res) {
                $("#ajaxMessage").showAjaxMessage({html: `Item update sucessfully`, type: "sucess"});
            }, "json");
            event.preventDefault();
            return false;
        });

        $(document).on("input", "#qty", function (e) {
            var prodtypeId = $("#prod_type").val();
            if (prodtypeId == RICE_EBAG || prodtypeId == RICE_SPARE || prodtypeId == RICE_OTHERS) {
                ebag_net_price();
            } else {
                _net_weight();

            }
            e.preventDefault();
        });

        $(document).on("change", "#size_id", function (e) {
            $("#size_weight").val(get_selected_option_info(this, 'data-weight'));
            _net_weight();
            e.preventDefault();
        });

        $(document).on("change", "#price_unit_id", function (e) {
            $("#sale_unit_weight").val(get_selected_option_info(this, 'data-weight'));
            _price_qty_unit();
            e.preventDefault();
        });

        $(document).on("change", "#prod_type", function (e) {
            var Id = this.value
            if ((Id == RICE_EBAG) || (Id == RICE_SPARE) || (Id == RICE_OTHERS)) {
                $(".rawopening").hide();
            } else {
                $(".rawopening").show();
            }
            var _companyId = $("#institute").val();
            var url = "{{ URL::to('category/categoryByType') }}";
            $.ajax({
                type: 'POST',
                url: url,
                data: {
                    companyId: _companyId,
                    productTypeId: this.value,
                    _token: _token
                },
                success: function (response) {
                    $("#category").html(response.category);
                    $("#size_id").html(response.sizes);
                    $("#unit_id").html(response.units);
                    $("#price_unit_id").html(response.price_units);
                }
            });
        });

        $(document).on("change", "#category ", function (e) {
            var _companyId = $("#institute").val();
            var _categoryId = $("#category").val();
            var _productType = $("#prod_type").val();
            var url = "{{ URL::to('product/prod_by_category') }}";
            $.ajax({
                type: 'POST',
                url: url,
                data: {companyId: _companyId, categoryId: _categoryId, productType: _productType, _token: _token},
                success: function (response) {
                    $("#product_id").html(response);
                }
            });
        });

        $(document).on("input", "#net_weight", function (e) {
            _price_qty_unit();
            e.preventDefault();
        });

        $(document).on("input", "#price_qty_unit", function (e) {
            net_price();
            e.preventDefault();
        });

        $(document).on("input", "#price", function (e) {
            find_rate();
            e.preventDefault();
        });
    });

    function _net_weight() {
        var _size_weight = $("#size_weight").val();
        var _qty = Number($("#qty").val());
        var _net_weight = 0;
        if (isNaN(_size_weight)) {
            _size_weight = 0;
        }
        if (isNaN(_qty)) {
            _qty = 0;
        }
        _net_weight = (parseFloat(_qty) * parseFloat(_size_weight)).toFixed(2);
        $("#net_weight").val(_net_weight);
        _price_qty_unit();
    }

    function net_price() {
        var _rate = Number($("#rate").val());
        var _price_qty_unit = $("#price_qty_unit").val();
        var _price = 0;
        if (isNaN(_rate)) {
            _rate = 0;
        }
        if (isNaN(_price_qty_unit)) {
            _price_qty_unit = 0;
        }
        _price = (parseFloat(_rate) * parseFloat(_price_qty_unit)).toFixed(3);
        $("#price").val(_price);
    }

    function ebag_net_price() {
        var _rate = Number($("#rate").val());
        var _qty = Number($("#qty").val());
        var _price = 0;
        if (isNaN(_rate)) {
            _rate = 0;
        }
        if (isNaN(_qty)) {
            _qty = 0;
        }
        _price = (_rate * _qty).toFixed(3);
        $("#price").val(_price);
    }

    function render_opening_items(institute_id) {
        var _url = baseUrl + "/ricemill/stocks/search_itemlist";
        var _date = $("#opening_date").val();
        var _formData = {};
        _formData.date = _date;
        _formData.institute_id = institute_id;
        _formData._token = _token;
        $.ajax({
            url: _url,
            type: "POST",
            data: _formData,
            success: function (res) {
                $("#order_itemlist_partial").html(res);
                $("#qty").val("");
                $("#net_weight").val("");
                $("#price_qty_unit").val("");
                $("#rate").val("");
                $("#price").val("");
            },
            error: function (xhr, status) {
                alert('There is some error.Try after some time.');
            }
        });
    }

    function _price_qty_unit() {
        var _unit_value = $("#unit_value").val();
        var _net_weight = Number($("#net_weight").val());
        var _price_unit_id = $("#price_unit_id").val();
        var _sale_unit_weight = Number($("#sale_unit_weight").val());
        var _qty = Number($("#qty").val());
        if (isNaN(_qty)) {
            _qty = 0;
        }
        if (_price_unit_id == RICE_PICES) {
            var _pricePcs = (_net_weight / _qty).toFixed(2);
            $("#price_qty_unit").val(_pricePcs);
        } else if (_price_unit_id == RICE_BAG) {
            $("#price_qty_unit").val(_qty);
        } else if (_price_unit_id == RICE_KG) {
            $("#price_qty_unit").val(_net_weight);
        } else if (_price_unit_id == RICE_MON) {
            var _mon = (_net_weight / _sale_unit_weight).toFixed(2);
            $("#price_qty_unit").val(_mon);
        } else if (_price_unit_id == RICE_BANGLA_MON) {
            var _bangla_mon = (_net_weight / _sale_unit_weight).toFixed(2);
            $("#price_qty_unit").val(_bangla_mon);
        } else {
            var _price_qty_unit = (parseFloat(_net_weight) / _unit_value).toFixed(2);
            $("#price_qty_unit").val(_price_qty_unit);
        }

        net_price();
    }

    function find_rate() {
        var _purchaseQty = Number($("#price_qty_unit").val());
        var _price = Number($("#price").val());
        var _rate = 0;
        if (isNaN(_purchaseQty)) {
            _purchaseQty = 0;
        }
        if (isNaN(_price)) {
            _price = 0;
        }
        _rate = (parseFloat(_price) / parseFloat(_purchaseQty)).toFixed(3);
        $("#rate").val(_rate);
    }
</script>
@endsection