@extends('layouts.app')

@section('breadcrumbs')
<div id="breadcrumbBar" class="breadcrumb site_nav_links no_bdr_rad clearfix">
    <div class="col-md-3 col-sm-3 col-xs-2 no_pad">
        <button class="btn btn-info btn-xs" type="button" onclick="history.back()" title="Go Back"><span class="visible-xs"><i class="fa fa-arrow-left"></i></span><span class="hidden-xs">Back</span></button>
        <button class="btn btn-info btn-xs" onclick="redirectTo('<?= url('/unit_size/create'); ?>')" title="Refresh" type="button"><span class="visible-xs"><i class="fa fa-refresh"></i></span><span class="hidden-xs">Refresh</span></button>
    </div>
    <div class="col-md-6 col-sm-6 hidden-xs text-center">
        <h2 class="page-title">{{$page_title}}</h2>
    </div>
    <div class="col-md-3 col-sm-3 col-xs-10 no_pad">
        <ul class="text-right no_mrgn no_pad">
            <li><a href="{{ url('/home') }}">Home</a> <i class="fa fa-angle-right"></i></li>
            <li><a href="{{ url('/unit_size') }}">Unit Size</a> <i class="fa fa-angle-right"></i></li>
            <li>Form</li>
        </ul>
    </div>
</div>
@endsection

@section('content')
<div class="col-md-6 col-md-offset-3 col-sm-6 col-sm-offset-3">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title">{{$page_title}}</h3>
        </div>
        <div class="panel-body">
            {!! Form::open(['method' => 'POST', 'url' => 'unit_size', 'class' => 'form-horizontal']) !!}
            <div class="form-group">
                <label class="col-md-4 control-label" for="unit_id">Unit</label>
                <div class="col-md-6">
                    <select name="unit_id" id="unit_id" class="form-control select2search">
                        <option value="">Select Unit</option>
                        @foreach($units as $unit)
                        <option value="{{ $unit->id }}">{{ $unit->unit }}</option>
                        @endforeach
                    </select>
                    <small class="text-danger">{{ $errors->first('unit') }}</small>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-4 control-label" for="name">Unit Size</label>
                <div class="col-md-6">
                    <input type="text" class="form-control" id="name" name="name" required>
                    <small class="text-danger">{{ $errors->first('name') }}</small>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-4 control-label" for="weight">Value</label>
                <div class="col-md-6">
                    <input type="number" class="form-control" id="weight" name="weight" value="" step="any" required>
                    <small class="text-danger">{{ $errors->first('weight') }}</small>
                </div>
            </div>

            <div class="col-md-8 col-md-offset-4">
                <button type="button" class="btn btn-info" id="resetbtn">Reset</button>
                <button type="submit" class="btn btn-primary">Save</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $(document).on("change", "#company_id", function () {
            $.ajax({
                url: "{{ URL::to('institute/components') }}",
                type: "post",
                data: {institute: this.value, _token: _token},
                success: function (res) {
                    $('#product_type_id').html(res.product_types);
                },
                error: function (xhr, status) {
                    alert('There is some error.Try after some time.');
                }
            });
        });
    });
</script>
@endsection