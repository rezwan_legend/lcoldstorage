@extends('layouts.app')

@section('breadcrumbs')
<div id="breadcrumbBar" class="breadcrumb site_nav_links no_bdr_rad clearfix">
    <div class="col-md-3 col-sm-3 col-xs-2 no_pad">
        <button class="btn btn-info btn-xs" type="button" onclick="history.back()" title="Go Back"><span class="visible-xs"><i class="fa fa-arrow-left"></i></span><span class="hidden-xs">Back</span></button>
        <button class="btn btn-info btn-xs" onclick="redirectTo('<?= url('/floor_setting/'.$data->_key.'/edit'); ?>')" title="Refresh" type="button"><span class="visible-xs"><i class="fa fa-refresh"></i></span><span class="hidden-xs">Refresh</span></button>
    </div>
    <div class="col-md-6 col-sm-6 hidden-xs text-center">
        <h2 class="page-title">Update Floor </h2>
    </div>
    <div class="col-md-3 col-sm-3 col-xs-10 no_pad">
        <ul class="text-right no_mrgn no_pad">
            <li><a href="{{ url('/home') }}">Home</a> <span class="fa fa-angle-right"></span></li>
            <li><a href="{{ url('/floor_setting') }}">Floor <span class="fa fa-angle-right"></span></a></li>
            <li>Form</li>
        </ul>
    </div>
</div>
@endsection

@section('content')
<div class="panel panel-primary">
    <div class="panel-heading">
        <h3 class="panel-title">Update Floor Information</h3>
    </div>
    <div class="panel-body">
        {!! Form::open(['method' => 'PUT', 'url' => 'floor_setting/'.$data->id, 'class' => 'form-horizontal']) !!}
        <div class="form-group">
            <label for="chamber_id" class="col-md-4 control-label">Chamber</label>
            <div class="col-md-6">
                <select name="chamber_id" id="chamber_id" class="form-control select2search">
                    <option value="">Select Chamber</option>
                    @foreach($chambers as $chamber)
                    <option value="{{ $chamber->id }}" <?php if ($data->chamber_id == $chamber->id) echo ' selected'; ?> >{{ $chamber->chamber }}</option>
                    @endforeach
                </select>
                <small class="text-danger">{{ $errors->first('chamber_id') }}</small>
            </div>
        </div>
        <div class="form-group">
            <label for="name" class="col-md-4 control-label">Name</label>
            <div class="col-md-6">
                <input type="text" class="form-control" name="name" id="name" value="{{$data->name}}">
                <small class="text-danger">{{ $errors->first('name') }}</small>
            </div>
        </div>
        <div class="form-group">
            <label for="capacity" class="col-md-4 control-label">Capacity</label>
            <div class="col-md-6">
                <input type="text" class="form-control" name="capacity" value="{{$data->capacity}}">
                <small class="text-danger">{{ $errors->first('capacity') }}</small>
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-8 col-md-offset-4">
            <button type="button" class="btn btn-info" onclick="history.back()" title="Go Back">Cancel</button>
            <button type="submit" class="btn btn-primary">Update</button>
            </div>
        </div>
        {!! Form::close() !!}
    </div>
</div>
@endsection