----Mamun --------------- date: 13/02/2022----------------
CREATE TABLE `rent` (
  `id` int(11) NOT NULL,
  `product_type_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `unit_id` int(11) NOT NULL,
  `unit_size` varchar(100) NOT NULL,
  `rent_type_id` int(11) NOT NULL,
  `rent` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `modified_at` datetime DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `is_deleted` tinyint(1) NOT NULL DEFAULT 0,
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL,
  `_key` int(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

ALTER TABLE `rent`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `rent`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

------ date: 14/02/2022-------------------------------------------------

CREATE TABLE `party_commission` (
  `id` int(11) NOT NULL,
  `party_type_id` int(11) NOT NULL,
  `party_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `unit_id` int(11) NOT NULL,
  `unit_size` varchar(100) NOT NULL,
  `commission` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `modified_at` datetime DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `is_deleted` tinyint(1) NOT NULL DEFAULT 0,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL,
  `_key` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

ALTER TABLE `party_commission`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `party_commission`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

---------------------------------------------------------------------

CREATE TABLE `delivery_condition` (
  `id` int(11) NOT NULL,
  `description` text NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `modified_at` datetime DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `is_deleted` tinyint(1) NOT NULL DEFAULT 0,
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL,
  `_key` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

ALTER TABLE `delivery_condition`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `delivery_condition`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

-----------date: 15/02/2022--------------------------------------------

CREATE TABLE `floors` (
  `id` int(11) NOT NULL,
  `chamber_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `capacity` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `modified_at` datetime DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `is_deleted` tinyint(1) NOT NULL DEFAULT 0,
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL,
  `_key` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

ALTER TABLE `floors`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `floors`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

---------------------------------------------------------------------

CREATE TABLE `pockets` (
  `id` int(11) NOT NULL,
  `chamber_id` int(11) NOT NULL,
  `floor_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `capacity` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `modified_at` datetime DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `is_deleted` tinyint(1) NOT NULL DEFAULT 0,
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL,
  `_key` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

ALTER TABLE `pockets`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `pockets`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--------------------------------------------------------------------

CREATE TABLE `party_loan` (
  `id` int(11) NOT NULL,
  `party_type_id` int(11) NOT NULL,
  `party_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `unit_id` int(11) NOT NULL,
  `unit_size_id` int(11) NOT NULL,
  `loan` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `modified_at` datetime DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT 0,
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL,
  `_key` varchar(32) NOT NULL,
  `current_loan` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

ALTER TABLE `party_loan`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `party_loan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

---------------------------------------------------------------------------------------

CREATE TABLE `sr` (
  `id` int(11) NOT NULL,
  `sr_no` int(11) NOT NULL,
  `lot_no` int(11) NOT NULL,
  `customer_name` varchar(100) NOT NULL,
  `father_name` varchar(100) NOT NULL,
  `village` varchar(100) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `modified_at` datetime DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `is_deleted` tinyint(1) NOT NULL DEFAULT 0,
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL,
  `_key` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

ALTER TABLE `sr`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `sr`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

-------------------------------------------------------------------------------------------

  CREATE TABLE `account_heads` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `type` varchar(50) NOT NULL,
  `head_type` varchar(50) NOT NULL,
  `common` varchar(50) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `modified_at` datetime DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `is_deleted` tinyint(1) NOT NULL DEFAULT 0,
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL,
  `_key` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

ALTER TABLE `account_heads`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `account_heads`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;