--pc setting
CREATE TABLE `pc_settings` (
  `id` int(11) NOT NULL,
  `name` varchar(25) NOT NULL,
  `created_at` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `modified_at` datetime NOT NULL,
  `modified_by` int(11) NOT NULL,
  `is_deleted` tinyint(4) NOT NULL DEFAULT 0,
  `deleted_at` datetime NOT NULL,
  `deleted_by` int(11) NOT NULL,
  `_key` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


ALTER TABLE `pc_settings`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `pc_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;



--shade setting
CREATE TABLE `shade_settings` (
  `id` int(11) NOT NULL,
  `name` varchar(25) NOT NULL,
  `created_at` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `modified_at` datetime NOT NULL,
  `modified_by` int(11) NOT NULL,
  `is_deleted` tinyint(4) NOT NULL DEFAULT 0,
  `deleted_at` datetime NOT NULL,
  `deleted_by` int(11) NOT NULL,
  `_key` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

ALTER TABLE `shade_settings`
  ADD PRIMARY KEY (`id`);


ALTER TABLE `shade_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;





  --agent/party
  CREATE TABLE `agent_party` (
  `id` int(11) NOT NULL,
  `agent_type` varchar(6) NOT NULL,
  `name` varchar(50) NOT NULL,
  `father_name` varchar(50) NOT NULL,
  `code` int(10) NOT NULL,
  `mobile` varchar(14) NOT NULL,
  `address` text NOT NULL,
  `per_bag_commission` int(4) DEFAULT NULL,
  `interest_rate` int(4) DEFAULT NULL,
  `is_deleted` tinyint(2) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `modified_at` datetime DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `deleted_by` int(11) NOT NULL,
  `_key` varchar(32) NOT NULL,
  `deleted_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


ALTER TABLE `agent_party`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `agent_party`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;


  --Bank setting
CREATE TABLE `bank_settings` (
  `id` int(11) NOT NULL,
  `name` varchar(25) NOT NULL,
  `created_at` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `modified_at` datetime NOT NULL,
  `modified_by` int(11) NOT NULL,
  `is_deleted` tinyint(4) NOT NULL DEFAULT 0,
  `deleted_at` datetime NOT NULL,
  `deleted_by` int(11) NOT NULL,
  `_key` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


ALTER TABLE `bank_settings`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `bank_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;




 --condition table

CREATE TABLE `condition` (
  `id` int(11) NOT NULL,
  `name` varchar(25) DEFAULT NULL,
  `type` varchar(25) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `modified_at` datetime DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `is_deleted` tinyint(4) NOT NULL DEFAULT 0,
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL,
  `_key` varchar(32) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

ALTER TABLE `condition`
  ADD PRIMARY KEY (`id`);


ALTER TABLE `condition`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;




  --party rent


CREATE TABLE `party_rent` (
  `id` int(11) NOT NULL,
  `party_type_id` int(11) DEFAULT NULL,
  `party_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `unit_id` int(11) DEFAULT NULL,
  `unit_size_id` int(11) DEFAULT NULL,
  `rent_id` int(11) DEFAULT NULL,
  `_key` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `modified_at` datetime DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `is_deleted` tinyint(4) NOT NULL DEFAULT 0,
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


ALTER TABLE `party_rent`
  ADD PRIMARY KEY (`id`);


ALTER TABLE `party_rent`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--------------------------------------------------------------------------------
ALTER TABLE `party_rent` CHANGE `rent_id` `rent` INT(11) NULL DEFAULT NULL;



--bag type


CREATE TABLE `bag_type` (
  `id` int(11) NOT NULL,
  `session` int(11) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `per_bag_rent` int(11) DEFAULT NULL,
  `per_kg_rent` int(11) DEFAULT NULL,
  `agent_bag_rent` int(11) DEFAULT NULL,
  `agent_kg_rent` int(11) DEFAULT NULL,
  `party_bag_rent` int(11) DEFAULT NULL,
  `party_kg_rent` int(11) DEFAULT NULL,
  `per_bag_loan` int(11) DEFAULT NULL,
  `empty_bag_rent` int(11) DEFAULT NULL,
  `fan_charge` int(11) DEFAULT NULL,
  `default` tinyint(2) NOT NULL,
  `created_at` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `modified_at` int(11) DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `is_deleted` int(11) NOT NULL DEFAULT 0,
  `deleted_at` int(11) DEFAULT NULL,
  `deleted_by` int(11) NOT NULL,
  `_key` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


ALTER TABLE `bag_type`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `bag_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;