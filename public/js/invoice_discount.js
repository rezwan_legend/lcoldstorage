const loadMaxId = false;
const codePrefix = "SZ";
const codePattern = 0;
const minimumField = 2;
const incrementBy = 1;
const data = {
  from_amount: [],
  to_amount: [],
  percentage: [],
};

const IsDropdownSelected = () => {
  if ($("#subhead_id").val() === "") {
    $("#ajaxMessage").showAjaxMessage({
      html: "please Select Subhead first",
      type: "warning",
    });
    return false;
  }
  return true;
};

const getDomUI = (code) => {
  code = code.toString();
  if (code.length == 2) {
    code = "0" + code;
  } else if (code.length < 2) {
    code = "00" + code;
  }
  let moduleCode = codePrefix + code;
  return `<tr>
 <td class="subhead" data-id="${$("#subhead_id").val()}">${$(
    "#subhead_id option:selected"
  ).text()}</td>
 <td><input type="number" class="form-control" name="from_amount"></td>
 <td><input type="number" class="form-control" name="to_amount"></td>
 <td><input type="number" class="form-control" name="percentage"></td>
 <td class="text-center trash"><i class="btn fa fa-trash"></i></td>
</tr>`;
};

const getSubheadId = () => {
  const zoneId = [];
  $.each($("table tbody .subhead"), function (index, value) {
    zoneId.push($(value).data("id"));
  });
  return zoneId;
};

const push_if_has_more_data = (formData) => {
  formData["subhead_id"] = getSubheadId();
  return formData;
};
