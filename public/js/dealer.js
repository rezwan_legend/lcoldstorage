const codePrefix = "KSB";
const codePattern = 10100;
const minimumField = 7;
const incrementBy = 1;

const data = {
  code:[],
  address:[]
}

const dropdownObject = [
  'region_id',
  'zone_id',
  'base_id',
  'subhead_id',
];



const loadBase = (id) => {
  axios.post(baseURL, {
    zone_id: id
  })
    .then((response) => {

      $('#base_id').html(response.data);
    })
    .catch((error) => {
      $('#ajaxMessage').showAjaxMessage({
        html: 'something went wrong',
        type: 'error'
      });
    })
}


const loadZone = (id) => {
  axios
    .post(zoneURL, {
      region_id: id,
    })
    .then((response) => {
      $("#zone_id").html(response.data);
    })
    .catch((error) => {
      $("#ajaxMessage").showAjaxMessage({
        html: "something went wrong",
        type: "error",
      });
    });
};


const loadSubhead = (id) => {
  axios.post(subheadURL, {
    head_id: id
  })
    .then((response) => {
      $('#subhead_id').html(response.data);
    })
    .catch((error) => {
      $('#ajaxMessage').showAjaxMessage({
        html: 'something went wrong',
        type: 'error'
      });
    })
}



const loadParticular = (id) => {
  axios.post(particularURL, {
    subhead_id: id
  })
    .then((response) => {
      $('#ajax_content').html(response.data);
    })
    .catch((error) => {
      $('#ajaxMessage').showAjaxMessage({
        html: 'something went wrong',
        type: 'error'
      });
    })
}


const IsDropdownSelected = () => {
  for(let item in dropdownObject){
    if($('#'+dropdownObject[item]).val() === ""){
      $("#ajaxMessage").showAjaxMessage({
        html: "Please make sure the dropdown boxs are selected",
        type: "error",
      });
      return false;
    }
  }
  return true;
}


if ($("#region_id").val() !== "") {
  loadZone($("#region_id").val());
}

$("#region_id").change(function (event) {
  loadZone($(this).val());
});

$('#zone_id').change(function (event) {
  loadBase($(this).val());
})


$('#subhead_id').change(function (event) {
  loadParticular($(this).val());
})

if($('#subhead_id').val() !== ""){
  loadParticular($('#subhead_id').val());
}


const getRegionId = () => {
  const baseId = [];
  $.each($("table .region"), function (index, value) {
    baseId.push($(value).data("id"));
  });
  return baseId;
}
const getZoneId = () => {
  const baseId = [];
  $.each($("table .zone"), function (index, value) {
    baseId.push($(value).data("id"));
  });
  return baseId;
}
const getBaseId = () => {
  const baseId = [];
  $.each($("table .base"), function (index, value) {
    baseId.push($(value).data("id"));
  });
  return baseId;
}

const getHeadId = () => {
  const getParticularId = [];
  $.each($("table .head"), function (index, value) {
    getParticularId.push($(value).data("id"));
  });
  return getParticularId;
}

const getSubheadId = () => {
  const getParticularId = [];
  $.each($("table .subhead"), function (index, value) {
    getParticularId.push($(value).data("id"));
  });
  return getParticularId;
}



const getParticularId = () => {
  const getParticularId = [];
  $.each($("table .base"), function (index, value) {
    getParticularId.push($(value).data("id"));
  });
  return getParticularId;
}




$(document).ready(function(){

  $('#search').click(function(){
    let data = {search:$('#particularSearchForm').val()}

    axios.post(searchURL, data)
      .then((response) => {
        $('#ajax_content').html(response.data);
      })
      .catch((error) => {
        $('#ajaxMessage').showAjaxMessage({
          html: 'something went wrong',
          type: 'error'
        });
      })
  })

  $('#multiform').submit(function(event){
    event.preventDefault();
    if(IsDropdownSelected() === false){
      return false;
    }
    let _form = $(this).serialize();
    axios.post(this.action, _form)
    .then((response)=>{
      $("#ajaxMessage").showAjaxMessage({
        html: response.data,
        type: response.status === 200 ? "success" : "danger",
      });
    })
  })
})
