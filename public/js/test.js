$(document).ready(function () {
    $(document).on("change", ".customer_type", function () {
        if ($(this).val() == 'customer') {
            $(".new_customer_panel").show('50');
            $('.new_agent_panel').find('input, textarea, button, select').attr('disabled', 'disabled');
            $('.new_company_panel').find('input, textarea, button, select').attr('disabled', 'disabled');
            $('.new_customer_panel').find('input, textarea, button, select').removeAttr('disabled');
        } else {
            $(".new_customer_panel").hide();
        }

        if ($(this).val() == 'agent') {
            $(".new_agent_panel").show('50');
            $('.new_customer_panel').find('input, textarea, button, select').attr('disabled', 'disabled');
            $('.new_company_panel').find('input, textarea, button, select').attr('disabled', 'disabled');
            $('.new_agent_panel').find('input, textarea, button, select').removeAttr('disabled');
        } else {
            $(".new_agent_panel").hide();
        }

        if ($(this).val() == 'company') {
            $(".new_company_panel").show('50');
            $('.new_agent_panel').find('input, textarea, button, select').attr('disabled', 'disabled');
            $('.new_customer_panel').find('input, textarea, button, select').attr('disabled', 'disabled');
            $('.new_company_panel').find('input, textarea, button, select').removeAttr('disabled');
        } else {
            $(".new_company_panel").hide();
        }
    });

    $(document).on("change", ".exist_customer", function () {
        if ($(this).val() == 'exist') {
            $(".exist_item").show('');
        } else {
            $(".exist_item").hide();
        }

        if ($(this).val() == 'new') {
            $(".create_new").show('');
        } else {
            $(".create_new").hide();
        }

    });

    $("#reset_from").click(function () {
        var _form = $("#frm_purchase");
        _form[0].reset();
    });

    $(document).on("click", "#btnPurchase", function (e) {
        var _form = $("#frmpurchase");
        var _url = "{{ URL::to ('create-new-order') }}";
        $.post(_url, _form.serialize(), function (res) {
            if (res.success === true) {
                $("#ajaxMessage").showAjaxMessage({html: res.message, type: 'success'});
                redirectTo(_purchaseUrl);
            } else {
                $("#ajaxMessage").showAjaxMessage({html: res.message, type: 'error'});
            }
        }, "json");
        e.preventDefault();
        return false;
    });
});

function weight_measure() {
    var net_weight = null;
    var wpq = null;
    var quantity = document.getElementById("quantity").value;
    var first_weight = document.getElementById("first_weight").value;
    var second_weight = document.getElementById("second_weight").value;
    net_weight = first_weight - second_weight;
    document.getElementById('net_weight').value = net_weight;
    wpq = net_weight / quantity;
    document.getElementById('wpq').value = wpq.toFixed(2);
}

function CheckInvoice(invoice) {
    //var preinv= document.getElementById('pre_inv').value;
    var _url = "{{ URL::to('check-invoice') }}/" + invoice;
    // alert (_url);
    $.ajax({
        url: _url,
        type: "get",
        // data: {'dist':dist_id,'_token': '{{ csrf_token() }}'},
        success: function (data) {
            $('#check_inv').html(data);
        },
        error: function (xhr, status) {
            alert('There is some error.Try after some time.');
        }
    });
}

function measure_wpq(id) {
    var total_quantity = parseInt(document.getElementById("quantity").value, 10);
    var wpq = parseFloat(document.getElementById("wpq").value, 10);
    var qun = parseInt(document.getElementById("qty_" + id).value, 10);
    var sumqty = get_sum("tqty_check");
    //console.log(sumqty);

    if (isNaN(total_quantity)) {
        total_quantity = 0;
    }

    if (sumqty > total_quantity) {
        document.getElementById("alt_" + id).style.display = "block";
    } else {
        document.getElementById("alt_" + id).style.display = "none";
        var mainwpq = qun * wpq;
        document.getElementById("wpq_" + id).value = mainwpq.toFixed(2);
    }
}

function measure_bag_price(id) {
    var qun = parseInt(document.getElementById("qty_" + id).value, 10);
    var per_price = parseInt(document.getElementById("per_price_" + id).value, 10);
    var mulprice = qun * per_price;

    if (isNaN(per_price)) {
        document.getElementById("palt_" + id).style.display = "block";
    } else {
        document.getElementById("palt_" + id).style.display = "none";
        var mulprice = qun * per_price;
        document.getElementById("net_price_" + id).value = mulprice.toFixed(2);
    }
}

//Function for Ajax req to get Thana by Dist Id
function get_product_by_id(id) {
    var _url = "{{ URL::to('get-product') }}/" + id;
    // alert (_url);
    $.ajax({
        url: _url,
        type: "get",
        // data: {'dist':dist_id,'_token': '{{ csrf_token() }}'},
        success: function (data) {
            $('#show_product_list').html(data);
        },
        error: function (xhr, status) {
            alert('There is some error.Try after some time.');
        }
    });
}

//Function for Ajax req to get Thana by Dist Id
function get_customer_info(id) {
    var _url = "{{ URL::to('get-customer-info') }}/" + id;
    // alert (_url);
    $.ajax({
        url: _url,
        type: "get",
        // data: {'dist':dist_id,'_token': '{{ csrf_token() }}'},
        success: function (data) {
            $('#exist_customer_info').html(data);
        },
        error: function (xhr, status) {
            alert('There is some error.Try after some time.');
        }
    });
}

//Function for Ajax req to get Thana by Dist Id
function get_company_info(id) {
    var _url = "{{ URL::to('get-company-info') }}/" + id;
    // alert (_url);
    $.ajax({
        url: _url,
        type: "get",
        // data: {'dist':dist_id,'_token': '{{ csrf_token() }}'},
        success: function (data) {
            $('#exist_company_info').html(data);
        },
        error: function (xhr, status) {
            alert('There is some error.Try after some time.');
        }
    });
}

//Function for Ajax req to get Thana by Dist Id
function get_agent_info(id) {
    var _url = "{{ URL::to('get-agent-info') }}/" + id;
    //alert (_url);
    $.ajax({
        url: _url,
        type: "get",
        // data: {'dist':dist_id,'_token': '{{ csrf_token() }}'},
        success: function (data) {
            $('#exist_agent_info').html(data);
        },
        error: function (xhr, status) {
            alert('There is some error.Try after some time.');
        }
    });
}

//Function for Ajax req to get Product by Category Id
function get_product_number_id(id) {
    var _url = "{{ URL::to('get-product-number') }}/" + id;
    // alert (_url);
    $.ajax({
        url: _url,
        type: "get",
        // data: {'dist':dist_id,'_token': '{{ csrf_token() }}'},
        success: function (data) {
            $('#show_product_number').html(data);
        },
        error: function (xhr, status) {
            alert('There is some error.Try after some time.');
        }
    });
}

//Function for Ajax req to get Thana by Dist Id
function get_thana(dist_id) {
    $.ajax({
        url: "{{ URL::to('get-thana') }}", //Full path of your action
        // alert (url);
        type: "post",
        data: {'dist': dist_id, '_token': '{{ csrf_token() }}'},
        success: function (data) {
            $('.show_thana_list').html(data);
        },
        error: function (xhr, status) {
            alert('There is some error.Try after some time.');
        }
    });
}