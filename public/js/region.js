
const codePrefix = "RE";
const codePattern = 0;
const minimumField = 1;
const incrementBy = 1;
const data = {
    'code': [],
    'name': []
}

const getDomUI = (code) => {
    code = code.toString();
    if (code.length == 2) {
        code = "0" + code;
    } else if (code.length < 2) {
        code = "00" + code;
    }
    let moduleCode = codePrefix + code;
    return `<tr>
        <td><input class="form-control code" type="text" name="code" value="${moduleCode}"></td>
        <td><input class="form-control" type="text" name="name" ></td>
        <td class="text-center trash"><i class="btn fa fa-trash"></i></td>
    </tr>`;
}


const push_if_has_more_data = (formData) => {
    return formData;
}




