const codePrefix = "SZ";
const codePattern = 0;
const minimumField = 2;
const incrementBy = 1;
const data = {
  'code':[],
  'name':[]
}

const IsDropdownSelected = () => {
  if ($("#region_id").val() === "") {
    $("#ajaxMessage").showAjaxMessage({
      html: "please Select region first",
      type: "warning",
    });
    return false;
  }
  return true;
};

const getDomUI = (code) => {
  code = code.toString();
  if (code.length == 2) {
    code = "0" + code;
  } else if (code.length < 2) {
    code = "00" + code;
  }
  let moduleCode = codePrefix + code;
  return `<tr>
 <td class="region" data-id="${$("#region_id").val()}">${$(
    "#region_id option:selected"
  ).text()}</td>
 <td><input type="text" class="form-control code" name="code" value="${moduleCode}" ></td>
 <td><input type="text" class="form-control" name="name"></td>
 <td class="text-center trash"><i class="btn fa fa-trash"></i></td>
</tr>`;
};

const getRegionId = () => {
  const zoneId = [];
  $.each($("table tbody .region"), function (index, value) {
    zoneId.push($(value).data("id"));
  });
  return zoneId;
};

const push_if_has_more_data = (formData) => {
  formData["region_id"] = getRegionId();
  return formData;
};
