const codePrefix = "KSB";
const codePattern = 10100;
const minimumField = 3;
const incrementBy = 1;

const data = {
  'code':[],
  'name':[]
}


const loadZone = (id) => {
  axios
    .post(zoneURL, {
      region_id: id,
    })
    .then((response) => {
      $("#zone_id").html(response.data);
    })
    .catch((error) => {
      $("#ajaxMessage").showAjaxMessage({
        html: "something went wrong",
        type: "error",
      });
    });
};

const IsDropdownSelected = () => {
  if ($("#zone_id").val() === "") {
    if ($("#region").val() === "") {
      $("#ajaxMessage").showAjaxMessage({
        html: "please Select region",
        type: "warning",
      });
      return false;
    }
    $("#ajaxMessage").showAjaxMessage({
      html: "please Select zone",
      type: "warning",
    });
    return false;
  }
  return true;
};

const getDomUI = (code) => {
  code = code.toString();
  if (code.length < 6) {
    code = "0" + code;
  }
  let moduleCode = codePrefix + code;
  return `<tr>
            <td>${$("#region option:selected").text()}</td>
            <td class="zone" data-id="${$("#zone_id").val()}">${$(
    "#zone_id option:selected"
  ).text()}</td>
            <td><input type="text" class="form-control code" name="code" value="${moduleCode}"></td>
            <td><input type="text" class="form-control" name="name" autofocus></td>
            <td class="text-center trash"><i class="btn fa fa-trash"></i></td>
        </tr>`;
};

if ($("#region").val() !== "") {
  loadZone($("#region").val());
}

$("#region").change(function (event) {
  loadZone($(this).val());
});

const getZoneId = () => {
  const zoneId = [];
  $.each($("table .zone"), function (index, value) {
    zoneId.push($(value).data("id"));
  });
  return zoneId;
};

const push_if_has_more_data = (formData) => {
  formData["zone_id"] = getZoneId();
  return formData;
};
