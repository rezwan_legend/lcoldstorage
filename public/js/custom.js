const _lang = document.documentElement.lang;
const csrfToken = document.head.querySelector('meta[name="csrf-token"]').content;
const baseUrl = document.head.querySelector('meta[name="base-url"]').content;
const UIN = document.head.querySelector('meta[name="current-uid"]').content;
const winHeight = $(window).height();
const winWidth = $(window).width();
const CONST_NO = "No";
const CONST_YES = "Yes";
const monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
var requiredFields = [];
var Products = [];

function load_category_dropdown_list(elm, target) {
    stopAjaxLoading();
    var _val = $(elm).val();
    var _target = $("#" + target);

    $(_target).parent().css('position', 'relative');
    $(_target).parent().append(`<img class='this_loader' src="${baseUrl}/public/img/ajax-loader.gif" style="position: absolute;top: 10px;right: 5px;z-index: 9999;">`);

    if (_val !== "") {
        $.ajax({
            url: baseUrl + "/product_type/category_dropdown_list",
            type: "POST",
            data: {
                _token: csrfToken,
                type: _val
            },
            success: function (res) {
                $(_target).html(res.html);
                $(".this_loader").remove();
            },
            error: function (xhr, status) {
                $(".this_loader").remove();
                alert('There is some error. Try after some time.');
            }
        });
    } else {
        $(".this_loader").remove();
        $(_target).html('<option value="">No Records Found</option>');
    }
}



function load_zone_dropdown_list(elm, target) {
    stopAjaxLoading();
    var _val = $(elm).val();
    var _target = $("#" + target);

    $(_target).parent().css('position', 'relative');
    $(_target).parent().append(`<img class='this_loader' src="${baseUrl}/public/img/ajax-loader.gif" style="position: absolute;top: 10px;right: 5px;z-index: 9999;">`);

    if (_val !== "") {
        $.ajax({
            url: baseUrl + "/product_type/category_dropdown_list",
            type: "POST",
            data: {
                _token: csrfToken,
                type: _val
            },
            success: function (res) {
                $(_target).html(res.html);
                $(".this_loader").remove();
            },
            error: function (xhr, status) {
                $(".this_loader").remove();
                alert('There is some error. Try after some time.');
            }
        });
    } else {
        $(".this_loader").remove();
        $(_target).html('<option value="">No Records Found</option>');
    }
}



function load_product_dropdown_list(elm, target) {
    stopAjaxLoading();
    var _val = $(elm).val();
    var _target = $("#" + target);

    $(_target).parent().css('position', 'relative');
    $(_target).parent().append(`<img class='this_loader' src="${baseUrl}/public/img/ajax-loader.gif" style="position: absolute;top: 10px;right: 5px;z-index: 9999;">`);

    if (_val !== "") {
        $.ajax({
            url: baseUrl + "/category/product_dropdown_list",
            type: "POST",
            data: {
                _token: csrfToken,
                category: _val
            },
            success: function (res) {
                $(_target).html(res.html);
                $(".this_loader").remove();
            },
            error: function (xhr, status) {
                $(".this_loader").remove();
                alert('There is some error. Try after some time.');
            }
        });
    } else {
        $(".this_loader").remove();
        $(_target).html('<option value="">No Records Found</option>');
    }
}

function load_description_dropdown_list(elm, target) {
    stopAjaxLoading();
    var _val = $(elm).val();
    var _target = $("#" + target);

    $(_target).parent().css('position', 'relative');
    $(_target).parent().append(`<img class='this_loader' src="${baseUrl}/public/img/ajax-loader.gif" style="position: absolute;top: 10px;right: 5px;z-index: 9999;">`);

    if (_val !== "") {
        $.ajax({
            url: baseUrl + "/product_description/get_colorcode_list",
            type: "POST",
            data: {
                _token: csrfToken,
                product: _val
            },
            success: function (res) {
                $(_target).html(res.html);
                $(".this_loader").remove();
            },
            error: function (xhr, status) {
                $(".this_loader").remove();
                alert('There is some error. Try after some time.');
            }
        });
    } else {
        $(".this_loader").remove();
        $(_target).html('<option value="">No Records Found</option>');
    }
}

function load_description_depo_stock_dropdown_list(elm, target) {
    stopAjaxLoading();
    var _val = $(elm).val();
    var _target = $("#" + target);

    $(_target).parent().css('position', 'relative');
    $(_target).parent().append(`<img class='this_loader' src="${baseUrl}/public/img/ajax-loader.gif" style="position: absolute;top: 10px;right: 5px;z-index: 9999;">`);

    if (_val !== "") {
        $.ajax({
            url: baseUrl + "/product_description/get_depo_stock_list",
            type: "POST",
            data: {
                _token: csrfToken,
                product: _val
            },
            success: function (res) {
                $(_target).html(res.html);
                $(".this_loader").remove();
            },
            error: function (xhr, status) {
                $(".this_loader").remove();
                alert('There is some error. Try after some time.');
            }
        });
    } else {
        $(".this_loader").remove();
        $(_target).html('<option value="">No Records Found</option>');
    }
}

function load_subhead_dropdown_list(elm, target) {
    stopAjaxLoading();
    var _val = $(elm).val();
    var _target = $("#" + target);

    $(_target).parent().css('position', 'relative');
    $(_target).parent().append(`<img class='this_loader' src="${baseUrl}/public/img/ajax-loader.gif" style="position: absolute;top: 10px;right: 5px;z-index: 9999;">`);

    if (_val !== "") {
        $.ajax({
            url: baseUrl + "/head/subhead_dropdown_list",
            type: "POST",
            data: {
                _token: csrfToken,
                head: _val
            },
            success: function (res) {
                $(_target).html(res.html);
                $(".this_loader").remove();
            },
            error: function (xhr, status) {
                $(".this_loader").remove();
                alert('There is some error. Try after some time.');
            }
        });
    } else {
        $(".this_loader").remove();
        $(_target).html('<option value="">No Category Found</option>');
    }
}

$(document).ready(function () {
    //setTimeout(hideAjaxMessage, 5000);
    $(document).on("click", "#apaginate ul li a", function (e) {
        showLoader("Processing...", true);
        var _form = $("#frmSearch");
        var _srcUrl = $(this).attr('href');

        $.ajax({
            type: "POST",
            url: _srcUrl,
            data: _form.serialize(),
            success: function (res) {
                showLoader("", false);
                $("#ajax_content").html('');
                $("#ajax_content").html(res);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                var _html = "<b>Request failed :</b> " + errorThrown + " [" + jqXHR.status + "]";
                showLoader("", false);
                $("#ajax_content").showAjaxMessage({
                    html: _html,
                    type: 'error'
                });
            }
        });
        e.preventDefault();
    });

    $(document).on("change", "#check_all", function () {
        if ($(this).is(":checked")) {
            $("#check input[type='checkbox']").prop("checked", true);
            $("#check input[type='checkbox']").closest("tr").not("#r_checkAll").addClass("bg-danger");
            $("#Del_btn").removeAttr("disabled");
            $("#btnReset").removeAttr("disabled");
        } else {
            $("#check input[type='checkbox']").prop("checked", false);
            $("#check tr").removeClass("bg-danger");
            $("#Del_btn").attr("disabled", "disabled");
            $("#btnReset").attr("disabled", "disabled");
        }
        checkbox_count();
    });

    $(document).on("change", "#check input[type='checkbox']", function () {
        if (this.checked) {
            $(this).closest("tr").not("#r_checkAll").addClass("bg-danger");
        } else {
            $(this).closest("tr").removeClass("bg-danger");
        }

        if ($("#check input[type='checkbox']:checked").not("#check_all").length > 0) {
            $("#check_all").prop("checked", true);
            $("#Del_btn").prop("disabled", false);
            $("#btnReset").prop("disabled", false);
        } else {
            $("#check_all").prop("checked", false);
            $("#Del_btn").prop("disabled", true);
            $("#btnReset").prop("disabled", true);
        }
        checkbox_count();
    });




    //Reset Search Form
    $(document).on("click", "#clear_from", function () {
        var _form = $("#frmSearch");
        _form[0].reset();
        $("#search").trigger('click');
    });

    $(document).ajaxStart(function () {
        $(document.body).css({
            'cursor': 'wait'
        });
        $("#loading_image").show();
        $("#mask").show();
    }).ajaxStop(function () {
        $(document.body).css({
            'cursor': 'default'
        });
        $("#loading_image").hide();
        $("#mask").hide();
    });

    //Date Picker
    $('.pickdate').datepicker({
        format: 'dd-mm-yyyy',
        autoclose: true,
        orientation: 'bottom'
    });

    //reset form
    $(document).on("click", "#resetbtn", function () {
        $('.form-horizontal').trigger("reset");

    });

    // Select 2 Javascript
    $('.select2search').select2({
        theme: "classic"
    });

    $(document).on("click", "#ajaxMessage a.close", function (e) {
        e.preventDefault();
        hideAjaxMessage();
    });

    $('.modal').on('hidden.bs.modal', function () {
        hideAjaxMessage();
    });
});

function readURL(input, to) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $("#" + to).attr("src", e.target.result);
        };
        reader.readAsDataURL(input.files[0]);
    }
}

function validate_required_field(arr, zero = false) {
    for (var field in arr) {
        var obj = document.getElementById(field);
        var msg = arr[field];
        //console.log(msg);

        if (obj.value == '') {
            $("#ajaxMessage").showAjaxMessage({
                html: msg,
                type: "error"
            });
            $(obj).focus();
            return false;
        }
        if (zero === true) {
            if (obj.value == 0) {
                $("#ajaxMessage").showAjaxMessage({
                    html: `${msg} and value should not be 0.`,
                    type: "error"
                });
                $(obj).focus();
                return false;
            }
        }
    }
    return true;
}

function getProducts(sort) {
    stopAjaxLoading();
    var _url = baseUrl + "/products/to_json_data";
    var _formData = {};
    _formData._token = csrfToken;
    _formData.key = sort;

    $.post(_url, _formData, function (res) {
        if (res.success === true) {
            Products.length = 0;
            Products.push(res.items);
            //console.log(Products);
        } else {
            console.log(res.message);
        }
    }, "json");
}
//getProducts();

function findById(arr, id) {
    return arr[id];
}

function findByCode(arr, barcode) {
    return arr[barcode];
}

function check_imei_sale(val, _id) {
    var _url = baseUrl + "/sale/check_imei_sold";
    $.post(_url, {
        _token: csrfToken,
        imei: val
    }, function (res) {
        if (res.success === true) {
            $("#ajaxMessage").showAjaxMessage({
                html: `IMEI <b>${val}</b> has already been sold.`,
                type: "error"
            });
            $("#btnId_" + _id).addClass('disabled');
        } else {
            $("#btnId_" + _id).removeClass('disabled');
        }
    }, "json");
}

function check_imei_stock(val, _id) {
    var _url = baseUrl + "/purchase/check_imei_stock";
    $.post(_url, {
        _token: csrfToken,
        imei: val
    }, function (res) {
        if (res.success === false) {
            $("#ajaxMessage").showAjaxMessage({
                html: `IMEI <b>${val}</b> is not in your stock list.`,
                type: "error"
            });
            $("#btnId_" + _id).addClass('disabled');
        } else {
            $("#btnId_" + _id).removeClass('disabled');
        }
    }, "json");
}

function checkbox_count() {
    var str = '';
    var ln = $("input[type='checkbox']:checked").not("#check_all").not("#select_all").not("#all_structure").not("#all_data").not("#all_tbl").length;

    if (ln > 0) {
        if (ln > 1) {
            str += `${ln} items checked`;
        } else {
            str += `${ln} item checked`;
        }
    } else {
        str = '';
    }

    $("#checkbox_counter").html(str);
}

function stopAjaxLoading() {
    $(document).ajaxStart(function () {
        $(document.body).css({
            'cursor': 'default'
        });
        $("#loading_image").hide();
        $("#mask").hide();
    }).ajaxStop(function () {
        $(document.body).css({
            'cursor': 'default'
        });
        $("#loading_image").hide();
        $("#mask").hide();
    });
}

function hide_ajax_message() {
    return $(".alert").slideUp(200);
}

function hideAjaxMessage() {
    $("#ajaxMessage").animate({
        right: '-15px'
    }, 200).hide();
}

(function ($) {
    $.fn.showAjaxMessage = function (options) {
        var _handler = $(this);
        var settings = {
            html: 'Undefined',
            type: 'alert'
        };
        settings = $.extend(settings, options);
        if (settings.type == 'success') {
            _handler
                .removeClass('alert-info')
                .removeClass('alert-warning')
                .removeClass('alert-danger')
                .addClass('alert-success')
                .html('<a href="#" class="close" style="margin-left: 10px;">×</a>' + settings.html)
                .animate({
                    right: '15px'
                }, 200).show();
        } else if (settings.type == 'error') {
            _handler
                .removeClass('alert-info')
                .removeClass('alert-warning')
                .removeClass('alert-success')
                .addClass('alert-danger')
                .html('<a href="#" class="close" style="margin-left: 10px;">×</a>' + settings.html)
                .animate({
                    right: '15px'
                }, 200).show();
        } else if (settings.type == 'warning') {
            _handler
                .removeClass('alert-info')
                .removeClass('alert-danger')
                .removeClass('alert-success')
                .addClass('alert-warning')
                .html('<a href="#" class="close" style="margin-left: 10px;">×</a>' + settings.html)
                .animate({
                    right: '15px'
                }, 200).show();
        } else {
            _handler
                .removeClass('alert-success')
                .removeClass('alert-warning')
                .removeClass('alert-danger')
                .addClass('alert-info')
                .html('<a href="#" class="close" style="margin-left: 10px;">×</a>' + settings.html)
                .animate({
                    right: '15px'
                }, 200).show();
        }
    };

    $.fn.callAjax = function (options) {
        var _handler = $(this);

        var settings = {
            url: '',
            data: {},
            dataType: 'json',
            crossDomain: false,
            global: true,
            processData: true,
            type: 'POST',
            respTo: $('<div>'),
            beforeSend: function () {
                settings.respTo.hide();
                _handler.show();
            },
            done: function (data, textStatus, jqXHR) {
                _handler.hide();
                settings.respTo.html(JSON.stringify(data)).show();
            },
            fail: function (jqXHR, textStatus, errorThrown) {
                _handler.hide();
                settings.respTo.html('Error: while processing the request <br/>' + errorThrown).show();
            },
            always: function (data, textStatus, jqXHR) {}
        };

        settings = $.extend(settings, options);

        $.ajaxSetup({
            url: settings.url,
            dataType: settings.dataType,
            crossDomain: settings.crossDomain,
            global: settings.global,
            processData: settings.processData,
            type: settings.type,
            beforeSend: settings.beforeSend
        });

        var jqxhr = $.ajax({
                data: settings.data
            })
            .done(settings.done)
            .fail(settings.fail)
            .always(settings.always);

        return jqxhr;
    };

    $.fn.priceField = function () {
        $(this).keydown(function (e) {
            var val = $(this).val();
            var code = (e.keyCode ? e.keyCode : e.which);
            var nums = ((code >= 96) && (code <= 105)) || ((code >= 48) && (code <= 57)); //keypad || regular
            var backspace = (code == 8);
            var specialkey = (e.metaKey || e.altKey || e.shiftKey);
            var arrowkey = ((code >= 37) && (code <= 40));
            var Fkey = ((code >= 112) && (code <= 123));
            var decimal = ((code == 110 || code == 190) && val.indexOf('.') == -1);

            // UGLY!!
            var misckey = (code == 9) || (code == 144) || (code == 145) || (code == 45) || (code == 46) || (code == 33) || (code == 34) || (code == 35) || (code == 36) || (code == 19) || (code == 20) || (code == 92) || (code == 93) || (code == 27);

            var properKey = (nums || decimal || backspace || specialkey || arrowkey || Fkey || misckey);
            var properFormatting = backspace || specialkey || arrowkey || Fkey || misckey || ((val.indexOf('.') == -1) || (val.length - val.indexOf('.') < 3) || ($(this).getCaret() < val.length - 2));

            if (!(properKey && properFormatting)) {
                return false;
            }
        });

        $(this).blur(function () {
            var val = $(this).val();
            if (val === '') {
                $(this).val('0.00');
            } else if (val.indexOf('.') == -1) {
                $(this).val(val + '.00');
            } else if (val.length - val.indexOf('.') == 1) {
                $(this).val(val + '00');
            } else if (val.length - val.indexOf('.') == 2) {
                $(this).val(val + '0');
            }
        });

        return $(this);
    };
})(jQuery);

function load_particular(elm) {
    var _val = $(elm).val();
    var _target = $(elm).attr('data-target');
    if (_val !== "") {
        $.ajax({
            url: baseUrl + "/subhead/particular",
            type: "POST",
            data: {
                head: _val,
                _token: csrfToken
            },
            success: function (res) {
                enable(_target).html(res);
            },
            error: function (xhr, status) {
                alert('There is some error. Try after some time.');
            }
        });
    } else {
        disable(_target).html('<option value="">Select Account First</option>');
    }
}

function get_particulars(elm, target) {
    var _val = $(elm).val();
    var _target = $("#" + target);
    if (_val !== "") {
        $.ajax({
            url: baseUrl + "/subhead/particular",
            type: "POST",
            data: {
                head: _val,
                _token: csrfToken
            },
            success: function (res) {
                enable(_target).html(res);
            },
            error: function (xhr, status) {
                alert('There is some error. Try after some time.');
            }
        });
    } else {
        disable(_target).html('<option value="">Select Account First</option>');
    }
}

function element_heights() {
    var _topbar = $("#topBar").outerHeight();
    var _header = $("#topnav_bar").outerHeight();
    var _breadcrumb = $("#breadcrumbBar").outerHeight();
    var _leftnav_width = $("#sidebar_area").outerWidth();
    var _footer = $("#footerPanel").outerHeight();

    var cssArr = [];
    cssArr.topbar = _topbar;
    cssArr.header = _header;
    cssArr.breadcrumb = _breadcrumb;
    cssArr.leftnav_width = _leftnav_width;
    cssArr.footer = _footer;
    return cssArr;
}

function breadcrumb_style() {
    var _leftnav_width = element_heights().leftnav_width;
    var _left, _wth;

    if ($(window).width() <= 767) {
        _left = 0;
        _wth = $(window).width();
    } else {
        _left = (_leftnav_width);
        _wth = ($(window).width() - _leftnav_width);
    }

    return $("#breadcrumbBar").css({
        'top': (element_heights().topbar + element_heights().header) - 1
    });
}

function get_sum(elm) {
    var tval = document.getElementsByClassName(elm),
        sum = 0,
        i;
    for (i = tval.length; i--;)
        if (tval[i].value)
            sum += parseFloat(tval[i].value, 2);
    return sum;
}

function sumvalue_int(elm) {
    var tval = document.getElementsByClassName(elm),
        sum = 0,
        i;
    for (i = tval.length; i--;)
        if (tval[i].value)
            sum += parseInt(tval[i].value, 10);
    return sum;
}



function sumvalue_double(elm) {
    var tval = document.getElementsByClassName(elm),
        sum = 0,
        i;
    for (i = tval.length; i--;)
        if (tval[i].value)
            sum += parseInt(tval[i].value, 10);
    return sum;
}

function sumvalue_float(elm) {
    var tval = document.getElementsByClassName(elm),
        sum = 0,
        i;
    for (i = tval.length; i--;)
        if (tval[i].value)
            sum += Number(tval[i].value);
    return sum.toFixed(2);
}

function sum_float_val(elm) {
    var tval = document.getElementsByClassName(elm),
        sum = 0,
        i;
    for (i = tval.length; i--;)
        if (tval[i].value)
            sum += Number(tval[i].value);
    return sum.toFixed(2);
}

function change_color(tableRow, highLight) {
    if (highLight) {
        tableRow.style.backgroundColor = "#e2eaef";
        tableRow.style.cursor = "pointer";
    } else {
        tableRow.style.backgroundColor = "";
    }
}

// Function to Calculate Kg to Mon
function kgToMon(kg, mon = 40) {
    var result = kg / mon;
    return parseFloat(result).toFixed(2);
}

function printDiv(divName) {
    var printContents = document.getElementById(divName).innerHTML;
    var originalContents = document.body.innerHTML;

    document.body.innerHTML = printContents;

    window.print();

    document.body.innerHTML = originalContents;
}

function redirectTo(url) {
    window.location = url;
}

function show_percentage_ratio(id) {
    var total_weight = parseFloat(document.getElementById("total_weight").value);
    var net_weight = parseFloat(document.getElementById("net_weight_" + id).value);
    var sum_weight = get_sum("total_weight_check");
    //alert (total_weight);
    //alert (sum_weight);

    if (isNaN(net_weight)) {
        net_weight = 0;
    }
    if (isNaN(total_weight)) {
        total_weight = 0;
    }

    if (sum_weight > total_weight) {
        document.getElementById("alt_" + id).style.display = "block";
    } else {
        document.getElementById("alt_" + id).style.display = "none";
        var percentage = (net_weight * 100) / total_weight;
        if (isNaN(percentage)) {
            percentage = 0;
        }
        document.getElementById("ratio_" + id).value = percentage.toFixed(2);
    }

}

function disable(element) {
    return $(element).attr('disabled', 'disabled');
}

function enable(element) {
    return $(element).removeAttr('disabled');
}

function readable(element) {
    return $(element).attr('readonly', 'readonly');
}

function editable(element) {
    return $(element).removeAttr('readonly');
}

function showLoader(text, show) {
    if (show == true) {
        $("#cover").show();
        $('#superLoader').show();
        $('#superLoaderText').html(text);
    } else {
        $("#cover").hide();
        $('#superLoader').hide();
        $('#superLoaderText').html(text);
    }
}

function day_diff(start, end) {
    const date1 = new Date(start);
    const date2 = new Date(end);
    const diffTime = Math.abs(date2 - date1);
    const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24));
    return diffDays;
}

function days_in_month(month, year) {
    return new Date(year, month, 0).getDate();
}

function date_parts(str, part = 0) {
    var _arr = str.split("-");
    var _retVal = Math.abs(_arr[part]);
    return _retVal;
}

function get_selected_option_text(select) {
    var elm = document.getElementById(select);
    return elm.options[elm.selectedIndex].text;
}

function get_selected_option_info(select, name) {
    var selectedOption = select.options[select.selectedIndex];
    var value = selectedOption.getAttribute(name);
    return value;
}

function get_selected_option_datainfo(element, name) {
    var select = document.getElementById(element);
    var selectedOption = select.options[select.selectedIndex];
    var value = selectedOption.getAttribute(name);
    return value;
}

// Define a global variable array to store the last selected checkbox Id
var g_selectedCheckboxId = [];

// setter method to update checkbox id array
function setLastSelected(checkboxObj) {
    if (checkboxObj.checked) {
        g_selectedCheckboxId.push(checkboxObj.id);
    } else {
        g_selectedCheckboxId.splice(g_selectedCheckboxId.indexOf(checkboxObj.id), 1);
    }
}

// getter method to get ID of the last checked checkbox
function getLastSelected() {
    if (g_selectedCheckboxId.length > 0)
        return g_selectedCheckboxId[g_selectedCheckboxId.length - 1];
    return null;
}

// local storage functions
function setLocalStorage(fieldName, value) {
    window.localStorage.setItem(fieldName, JSON.stringify(value));
}

function getLocalStorage(fieldName) {
    if (window.localStorage.getItem(fieldName) == null) {
        return null;
    }
    return JSON.parse(window.localStorage.getItem(fieldName));
}

function removeLocalStorage(fieldName) {
    window.localStorage.removeItem(fieldName);
}

function clearLocalStorage() {
    window.localStorage.clear();
}

// session storage functions
function setSessionStorage(fieldName, value) {
    window.sessionStorage.setItem(fieldName, JSON.stringify(value));
}

function getSessionStorage(fieldName) {
    return JSON.parse(window.sessionStorage.getItem(fieldName));
}

function removeSessionStorage(fieldName) {
    window.sessionStorage.removeItem(fieldName);
}

function clearSessionStorage() {
    window.sessionStorage.clear();
}

function redirectTo(url) {
    window.location = url;
}