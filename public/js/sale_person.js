const codePrefix = "KSB";
const codePattern = 10100;
const minimumField = 7;
const incrementBy = 1;

let base = '';

const data = {
  'assigned_at': [],
}

const dropdownObject = [
  'region_id',
  'zone_id',
  'base_id',
  'subhead_id',
  'particular_id',
  'status',
];




const loadBase = (id) => {
  axios.post(baseURL, {
    zone_id: id
  })
    .then((response) => {

      $('#base_id').html(response.data);
    })
    .catch((error) => {
      $('#ajaxMessage').showAjaxMessage({
        html: 'something went wrong',
        type: 'error'
      });
    })
}


const loadZone = (id) => {
  axios
    .post(zoneURL, {
      region_id: id,
    })
    .then((response) => {
      $("#zone_id").html(response.data);
    })
    .catch((error) => {
      $("#ajaxMessage").showAjaxMessage({
        html: "something went wrong",
        type: "error",
      });
    });
};

const loadParticular = (id) => {
  axios.post(particularURL, {
    subhead_id: id
  })
    .then((response) => {
      $('#particular_id').html(response.data);
    })
    .catch((error) => {
      $('#ajaxMessage').showAjaxMessage({
        html: 'something went wrong',
        type: 'error'
      });
    })
}


const IsDropdownSelected = () => {
  for(let item in dropdownObject){
    if($('#'+dropdownObject[item]).val() === ""){
      $("#ajaxMessage").showAjaxMessage({
        html: "Please make sure the dropdown boxs are selected",
        type: "error",
      });
      return false;
    }
  }
  return true;
}



const getDomUI = (code) => {

  if(base !== "" && base === $("#base_id").val() ){
    alert('Please Select Another Base');
    return "";
  }

  code = code.toString();
  if (code.length < 6) {
    code = "0" + code;
  }

  base = $("#base_id").val();
  let moduleCode = codePrefix + code;
  return `<tr>
            <td class="region" data-id="${$("#region_id").val()}" >${$("#region_id option:selected").text()}</td>
            <td class="zone" data-id="${$("#zone_id").val()}" >${$("#zone_id option:selected").text()}</td>
            <td class="base" data-id="${$("#base_id").val()}" >${$("#base_id option:selected").text()}</td>
            <td class="subhead" data-id="${$("#subhead_id").val()}" >${$("#subhead_id option:selected").text()}</td>
            <td class="particular" data-id="${$('#particular_id').val()}" >${$("#particular_id option:selected").text()}</td>
            <td class="status" data-id="${$('#status').val()}">${$("#status option:selected").text()}</td>
            <td><input type="date" value="${new Date()}" placeholder="(dd-mm-yyyy)" name="assigned_at" class="form-control pickdate" size="30"></td>
            <td class="text-center trash"><i class="btn fa fa-trash"></i></td>
        </tr>`;
};

if ($("#region_id").val() !== "") {
  loadZone($("#region_id").val());
}

$("#region_id").change(function (event) {
  loadZone($(this).val());
});

$('#zone_id').change(function (event) {
  loadBase($(this).val());
})

$('#subhead_id').change(function (event) {
  loadParticular($(this).val());
})


const getRegionId = () => {
  const baseId = [];
  $.each($("table .region"), function (index, value) {
    baseId.push($(value).data("id"));
  });
  return baseId;
}
const getZoneId = () => {
  const baseId = [];
  $.each($("table .zone"), function (index, value) {
    baseId.push($(value).data("id"));
  });
  return baseId;
}
const getBaseId = () => {
  const baseId = [];
  $.each($("table .base"), function (index, value) {
    baseId.push($(value).data("id"));
  });
  return baseId;
}

const getSubheadId = () => {
  const getParticularId = [];
  $.each($("table .subhead"), function (index, value) {
    getParticularId.push($(value).data("id"));
  });
  return getParticularId;
}

const getParticularId = () => {
  const getParticularId = [];
  $.each($("table .particular"), function (index, value) {
    getParticularId.push($(value).data("id"));
  });
  return getParticularId;
}

const getStatus = () => {
  const status = [];
  $.each($("table .status"), function (index, value) {
    status.push($(value).data("id"));
  });
  return status;
}

const push_if_has_more_data = (formData) => {
  formData["region_id"] = getRegionId();
  formData["zone_id"] = getZoneId();
  formData["base_id"] = getBaseId();
  formData["subhead_id"] = getSubheadId();
  formData["particular_id"] = getParticularId();
  formData["status"] = getStatus();
  return formData;
};
