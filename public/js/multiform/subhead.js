const loadMaxId = false;
const codePrefix = "";
var codePattern = 0;
const minimumField = 2;
const incrementBy = 1;
const data = {
  code:[],
  name:[]
}


const loadHead = (head_id)=>{
  for(let head of heads){
    if(head.id == head_id){
      codePattern = parseInt(head.code);
      break;
    }
  }
}

if ($("#head_id").val() !== "") {
  loadHead($("#head_id").val());
}

$('#head_id').change(function(){
  let head_id = this.value;
  loadHead(head_id);
  moduleCode = undefined;
});


const IsDropdownSelected = () => {
  if ($("#head_id").val() === "") {
    $("#ajaxMessage").showAjaxMessage({
      html: "please Select region first",
      type: "warning",
    });
    return false;
  }
  return true;
};

const getDomUI = (code) => {
  code = code.toString();
  if (code.length == 2) {
    code = "0" + code;
  } else if (code.length < 2) {
    code = "00" + code;
  }
  let moduleCode = codePrefix + code;
  return `<tr>
 <td class="head" data-id="${$("#head_id").val()}">${$(
    "#head_id option:selected"
  ).text()}</td>
 <td><input type="text" class="form-control code" name="code" value="${Math.floor(Math.random() * 9999999)}" ></td>
 <td><input type="text" class="form-control" name="name"></td>
 <td class="text-center trash"><i class="btn fa fa-trash"></i></td>
</tr>`;
};

const getHeadId = () => {
  const zoneId = [];
  $.each($("table tbody .head"), function (index, value) {
    zoneId.push($(value).data("id"));
  });
  return zoneId;
};

const push_if_has_more_data = (formData) => {
  formData["head_id"] = getHeadId();
  return formData;
};
