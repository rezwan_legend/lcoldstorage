var moduleCode;
var maxId;


const loadMax = async function () {
  const { data } = await axios.post(url);
  maxId = data;
};

if (typeof loadMaxId == 'undefined') {
  loadMax();
}

/**
 *
 * @param {update after removing field} codeUI
 */

const updateCode = (codeUI) => {
  codePrefix;
  moduleCode = codePattern;
  if (maxId) {
    moduleCode = codePattern + maxId;
  }
  Array.prototype.forEach.call(codeUI, (item) => {
    moduleCode += incrementBy;
    let strCode = moduleCode.toString();
    strCode = strCode.length == 2 ? "0" + strCode : strCode;
    strCode = strCode.length == 1 ? "00" + strCode : strCode;
    item.value = codePrefix + strCode;
  });
};

/**
 * create new row
 */

const createNewRow = () => {
  if (typeof moduleCode === "undefined") {
    if (maxId) {
      moduleCode = codePattern + maxId;
    } else {
      moduleCode = codePattern;
    }
  }
  moduleCode = moduleCode + incrementBy;

  document
    .querySelector("tbody")
    .insertAdjacentHTML("beforeend", getDomUI(moduleCode));
};

// remove row

//remove table row
$(document).on("click", ".trash", function () {
  $(this).parent().remove();
  if ((document.querySelector("#multiform").elements.length = minimumField)) {
    moduleCode = undefined;
  }
  let code = document.querySelectorAll(".code");
  updateCode(code);
});

const isFormValidated = (formElements) => {
  var status = true;
  $.each(formElements, function (index, item) {
    if (item.hasAttribute("name")) {
      const attribute = item.getAttribute("name").trim();
      if (attribute in data) {
        if (item.value === "") {
          item.classList.add("is-invalid");
          status = false;
        }
      }
    }
  });
  return status;
};

const getFormValue = (formElements) => {
  $.each(formElements, function (index, item) {
    if (item.hasAttribute("name")) {
      const attribute = item.getAttribute("name").trim();
      if (attribute in data) {
        data[attribute].push(item.value);
      }
    }
  });
  return data;
};

//finally save data using axios

const saveData = (actionUrl, formData) => {
  $("#saveButton").attr("disabled", true);
  axios
    .post(actionUrl, formData)
    .then((response) => {
      $("tbody").html("");

      console.log(response.data);
      $("#ajaxMessage").showAjaxMessage({
        html: response.data,
        type: response.status === 200 ? "success" : "danger",
      });
      $("#saveButton").removeAttr("disabled");
    })
    .catch((error) => {
      $("#ajaxMessage").showAjaxMessage({
        html: error.response.data,
        type: "error",
      });
      $("#saveButton").removeAttr("disabled");
    });
};

$(document).ready(function () {
  $("#addRow").click(function () {
    if (typeof IsDropdownSelected === "function") {
      if (IsDropdownSelected()) {
        createNewRow();
      }
      return false;
    } else {
      createNewRow();
    }
  });

  //submit region form

  $("#multiform").submit(function (event) {

    event.preventDefault();
    let formElements = this.elements;
    if (formElements.length == minimumField) {
      alert("Please add your data field first");
      return false;
    }

    if (isFormValidated(formElements)) {
     
      for(const key of Object.keys(data)){
        data[key] = [];
      }

      let formdata = getFormValue(formElements);
      formdata = push_if_has_more_data(formdata);
      console.log(formdata);
      saveData(this.action, formdata);
    }
  });
});
