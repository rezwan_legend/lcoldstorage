<?php

Auth::routes();
// Resource Route for HeadController
// Route::resource('head', 'Account\HeadController');
// Route::post('head/search', 'Account\HeadController@search');
// Route::post('head/delete', 'Account\HeadController@delete');
// Route::post('head/subhead_dropdown_list', 'Account\HeadController@subhead_dropdown_list');
// Route::post('head/max_id/get/', 'Account\HeadController@getMaxId')->name('head.getMaxId');

// Resource Route for SubHeadController
// Route::resource('subhead', 'Account\SubHeadController');
// Route::post('subhead/search', 'Account\SubHeadController@search');
// Route::post('subhead/delete', 'Account\SubHeadController@delete');
// Route::post('subhead/subhead', 'Account\SubHeadController@get_sub_head');
// Route::post('subhead/particular', 'Account\SubHeadController@get_particular');
// Route::post('subhead/max_id/get/', 'Account\SubHeadController@getMaxId')->name('subhead.getMaxId');
// Route::post('subhead/throught/head/', 'Account\SubHeadController@getSubheadThroughHead')->name('getSubheadThroughHead');


// Resource Route for Particulars
Route::get('particular/ledger/{id}', 'Account\ParticularController@ledger');
Route::post('particular/search_ledger', 'Account\ParticularController@search_ledger');
Route::post('particular/get_customers', 'Account\ParticularController@get_dsr_customers');
Route::resource('particular', 'Account\ParticularController');
Route::get('particular/create/{id}', 'Account\ParticularController@create_particular');
Route::post('particular/search', 'Account\ParticularController@search');
Route::post('particular/delete', 'Account\ParticularController@delete');

// Resource Route for Purpose
Route::resource('purpose', 'Account\PurposeController');
Route::post('purpose/search', 'Account\PurposeController@search');
Route::post('purpose/delete', 'Account\PurposeController@delete');

Route::get('business-ledger/{id}', 'BusinessLedgerController@view');

// Resource Route for Transactions
Route::get('transactions/update_dsrinfo', 'Account\TransactionController@update_dsrinfo');
Route::get('transactions/update_dsr_data', 'Account\TransactionController@update_dsr_data');
Route::resource('transactions', 'Account\TransactionController');
Route::get('transactions/create/{tp}', 'Account\TransactionController@create');
Route::post('transactions/search', 'Account\TransactionController@search');
Route::post('transactions/delete', 'Account\TransactionController@delete');
Route::post('transactions/subhead', 'Account\TransactionController@get_sub_head');
Route::get('transaction/expense', 'Account\TransactionController@expenses');
Route::post('transactions/expense-search', 'Account\TransactionController@expense_search');

// Resource Route for LedgerController
Route::resource('ledger', 'Account\LedgerController');
Route::get('ledger-view', 'Account\LedgerController@view_ledger');
Route::post('ledger/search', 'Account\LedgerController@ledger_search');
Route::get('ledger/head/{id}', 'Account\LedgerController@head_transaction');
Route::post('ledger/head/search', 'Account\LedgerController@head_transaction_search');
Route::get('ledger/subhead/{id}', 'Account\LedgerController@subhead_transaction');
Route::post('ledger/subhead/search', 'Account\LedgerController@subhead_transaction_search');
Route::get('ledger/particular/{id}', 'Account\LedgerController@particular_transaction');
Route::post('ledger/particular/search', 'Account\LedgerController@particular_transaction_search');

//Account Opening Related Route
Route::get('subhead_opening', 'Account\OpeningController@subhead_opening');
Route::post('subhead_opening/search', 'Account\OpeningController@subhead_opening_search');
Route::post('subhead_opening_save', 'Account\OpeningController@subhead_opening_save');
Route::get('particular_opening', 'Account\OpeningController@particular_opening');
Route::post('particular_opening/search', 'Account\OpeningController@particular_opening_search');
Route::post('particular_opening_save', 'Account\OpeningController@particular_opening_save');
Route::get('purpose_opening', 'Account\OpeningController@purpose_opening');
Route::post('purpose_opening/search', 'Account\OpeningController@purpose_opening_search');
Route::post('purpose_opening_save', 'Account\OpeningController@purpose_opening_save');

//Balancesheet Setting
Route::get('balancesheet_setting/delete_item/{id}', 'Account\BalancesheetSettingController@delete_item');
Route::get('balancesheet_setting/restore_item/{id}', 'Account\BalancesheetSettingController@restore_item');
Route::resource('balancesheet_setting', 'Account\BalancesheetSettingController');
Route::post('balancesheet_setting/search', 'Account\BalancesheetSettingController@search');
Route::post('balancesheet_setting/delete', 'Account\BalancesheetSettingController@delete');

//Balancesheet
Route::resource('balancesheet', 'Account\BalancesheetController');
Route::post('balancesheet/search', 'Account\BalancesheetController@search');
Route::post('balancesheet/delete', 'Account\BalancesheetController@delete');

// Daily Report
Route::get('dailyreport', 'Account\ReportController@daily');
Route::post('dailyreport/search', 'Account\ReportController@search_daily');

// Financial Statement Report
Route::get('financial-statement', 'Account\FinancialController@financial_statement');
Route::post('financial-statement/search', 'Account\FinancialController@search_financial_statement');

// Profit-Loss Statement Report
Route::get('statement/profit_loss', 'Account\StatementController@index');
Route::post('statement/search_profit_loss', 'Account\StatementController@search_profit_loss');

//Assets Routes
Route::get('assets_statement', 'AssetsController@assets_statement');
Route::get('assets/setting', 'AssetsController@setting');
Route::resource('assets', 'AssetsController');
Route::post('assets/search', 'AssetsController@search');
Route::post('assets/delete', 'AssetsController@delete');
Route::post('assets/list_purpose_details', 'AssetsController@list_purpose_details');
Route::post('assets/file_closing', 'AssetsController@file_closing');
Route::post('assets/search_setting', 'AssetsController@search_setting');
Route::post('assets_statement/search', 'AssetsController@assets_statement_search');
