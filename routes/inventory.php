<?php

// Product Type Routes
// Route::resource('product_type', 'ProductTypeController');

// Route::post('product_type/search', 'ProductTypeController@search');
// Route::post('product_type/delete', 'ProductTypeController@delete');
// Route::post('product_type/category_dropdown_list', 'ProductTypeController@category_dropdown_list');

// Route::post('/product_type/import/file','ProductTypeController@import')->name('product_type.import');



// Category Routes
// Route::resource('category', 'CategoryController');
// Route::post('category/search', 'CategoryController@search');
// Route::post('category/delete', 'CategoryController@delete');
// Route::post('category/list_by_product_type', 'CategoryController@list_by_product_type');
// Route::post('category/product_dropdown_list', 'CategoryController@product_dropdown_list');

// Product Routes
// Route::post('products/get_products', 'ProductController@get_products');
// Route::post('products/to_json_data', 'ProductController@get_json_data');
// Route::resource('products', 'ProductController');
// Route::post('products/search', 'ProductController@search');
// Route::post('products/delete', 'ProductController@delete');

// Product Description Routes
Route::resource('product_description', 'ProductDescriptionController');
Route::post('product_description/search', 'ProductDescriptionController@search');
Route::post('product_description/delete', 'ProductDescriptionController@delete');
Route::post('product_description/remove', 'ProductDescriptionController@remove');
Route::post('product_description/render_newlist', 'ProductDescriptionController@render_newlist');
Route::post('product_description/get_colorcode_list', 'ProductDescriptionController@get_colorcode_list');

// PurchasePriceSetting routes
Route::post('price_setting_purchase/search_product', 'PurchasePriceSettingController@search_product');
Route::resource('price_setting_purchase', 'PurchasePriceSettingController');
Route::post('price_setting_purchase/search', 'PurchasePriceSettingController@search');
Route::post('price_setting_purchase/delete', 'PurchasePriceSettingController@delete');

// SalePriceSetting routes
Route::post('price_setting_sale/search_product', 'SalePriceSettingController@search_product');
Route::resource('price_setting_sale', 'SalePriceSettingController');
Route::post('price_setting_sale/search', 'SalePriceSettingController@search');
Route::post('price_setting_sale/delete', 'SalePriceSettingController@delete');

// Purchase routes
Route::post('purchase/save_order', 'PurchaseController@save_order');
Route::get('purchase/payment/{id}', 'PurchaseController@payment');
Route::get('purchase/payment_form/{id}', 'PurchaseController@payment_form');
Route::post('purchase/save_payment', 'PurchaseController@save_payment');
Route::get('purchase/items', 'PurchaseController@items');
Route::post('purchase/search_items', 'PurchaseController@search_items');
Route::get('purchase/itemlist/{id}', 'PurchaseController@itemlist');
Route::post('purchase/search_itemlist', 'PurchaseController@search_itemlist');
Route::post('purchase/add_item', 'PurchaseController@add_item');
Route::get('purchase/item_detail/{id}/{productId}', 'PurchaseController@item_detail');
Route::get('purchase/remove_item/{id}', 'PurchaseController@remove_item');
Route::post('purchase/check_imei_stock', 'PurchaseController@check_imei_stock');
Route::post('purchase/search_product', 'PurchaseController@search_product');
Route::get('purchase/ledger', 'PurchaseController@ledger');
Route::post('purchase/ledger_search', 'PurchaseController@ledger_search');
Route::get('purchase/{id}/confirm', 'PurchaseController@confirm');
Route::resource('purchase', 'PurchaseController');
Route::post('purchase/search', 'PurchaseController@search');
Route::post('purchase/delete', 'PurchaseController@delete');
Route::post('purchase/delete_item', 'PurchaseController@delete_item');
Route::post('purchase/reset', 'PurchaseController@reset');

// Sale Related routes
Route::post('sale/save_order', 'SaleController@save_order');
Route::get('sale/payment/{id}', 'SaleController@payment');
Route::get('sale/payment_form/{id}', 'SaleController@payment_form');
Route::post('sale/save_payment', 'SaleController@save_payment');
Route::get('sale/items', 'SaleController@items');
Route::post('sale/search_items', 'SaleController@search_items');
Route::get('sale/itemlist/{id}', 'SaleController@itemlist');
Route::post('sale/search_itemlist', 'SaleController@search_itemlist');
Route::post('sale/add_item', 'SaleController@add_item');
Route::get('sale/item_detail/{id}/{productId}', 'SaleController@item_detail');
Route::get('sale/remove_item/{id}', 'SaleController@remove_item');
Route::post('sale/check_imei_sold', 'SaleController@check_imei_sold');
Route::post('sale/search_product', 'SaleController@search_product');
Route::get('sale/ledger', 'SaleController@ledger');
Route::post('sale/ledger_search', 'SaleController@ledger_search');
Route::get('sale/{id}/confirm', 'SaleController@confirm');
Route::resource('sale', 'SaleController');
Route::post('sale/search', 'SaleController@search');
Route::post('sale/delete', 'SaleController@delete');
Route::post('sale/delete_item', 'SaleController@delete_item');
Route::post('sale/reset', 'SaleController@reset');

// sale return routes
Route::post('sale_return/save_order', 'SaleReturnController@save_order');
Route::get('sale_return/items', 'SaleReturnController@items');
Route::post('sale_return/search_items', 'SaleReturnController@search_items');
Route::get('sale_return/itemlist/{id}', 'SaleReturnController@itemlist');
Route::post('sale_return/search_itemlist', 'SaleReturnController@search_itemlist');
Route::post('sale_return/add_item', 'SaleReturnController@add_item');
Route::get('sale_return/item_detail/{id}/{productId}', 'SaleReturnController@item_detail');
Route::get('sale_return/remove_item/{id}', 'SaleReturnController@remove_item');
Route::post('sale_return/check_imei_sold', 'SaleReturnController@check_imei_sold');
Route::post('sale_return/search_product', 'SaleReturnController@search_product');
Route::get('sale_return/ledger', 'SaleReturnController@ledger');
Route::post('sale_return/ledger_search', 'SaleReturnController@ledger_search');
Route::get('sale_return/{id}/confirm', 'SaleReturnController@confirm');
Route::resource('sale_return', 'SaleReturnController');
Route::post('sale_return/search', 'SaleReturnController@search');
Route::post('sale_return/delete', 'SaleReturnController@delete');
Route::post('sale_return/delete_item', 'SaleReturnController@delete_item');
Route::post('sale_return/reset', 'SaleReturnController@reset');

// Stock Routes
Route::get('stocks/opening', 'StockController@opening');
Route::post('stocks/save_opening', 'StockController@save_opening');
Route::post('stocks/search_opening_items', 'StockController@search_opening_items');
Route::get('stocks/item_detail/{productId}', 'StockController@item_detail');
Route::get('stocks/remove_item/{id}', 'StockController@remove_item');
Route::resource('stocks', 'StockController');
Route::post('stocks/search', 'StockController@search');

// Stock Register Routes
Route::get('stock-register', 'StockRegisterController@index');
Route::post('stock-register/search', 'StockRegisterController@search');

// update service routes
Route::get('service', 'ServiceController@index');
Route::get('service/maintenance', 'ServiceController@maintenance');
Route::post('service/update/{id}', 'ServiceController@update');
// sales, dsr-sales update routes
Route::get('service/update_sales', 'ServiceController@update_sales');
Route::get('service/update_dsr_sales', 'ServiceController@update_dsr_sales');
// stock update routes
Route::get('service/update_opening_stock', 'ServiceController@update_opening_stock');
