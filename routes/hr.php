<?php

Route::get('hr', 'Hr\DashboardController@index');

// Resource Route for Designation
Route::resource('hr/designation', 'Hr\DesignationController');
Route::post('hr/designation/search', 'Hr\DesignationController@search');
Route::post('hr/designation/delete', 'Hr\DesignationController@delete');

// Resource Route for Employee
Route::get('hr/employee/ledger/{id}', 'Hr\EmployeeController@ledger');
Route::resource('hr/employee', 'Hr\EmployeeController');
Route::post('hr/employee/search', 'Hr\EmployeeController@search');
Route::post('hr/employee/search_ledger', 'Hr\EmployeeController@search_ledger');
Route::post('hr/employee/delete', 'Hr\EmployeeController@delete');
Route::post('hr/employee/dropdown_by_designation', 'Hr\EmployeeController@dropdown_by_designation');
Route::post('hr/employee/dropdown_list', 'Hr\EmployeeController@dropdown_list');

// Resource Route for Employee Salary
Route::get('hr/salary/items', 'Hr\SalaryController@items');
Route::post('hr/salary/delete_item', 'Hr\SalaryController@delete_item');
Route::post('hr/salary/view_form', 'Hr\SalaryController@view_form');
Route::post('hr/salary/save_form', 'Hr\SalaryController@save_form');
Route::resource('hr/salary', 'Hr\SalaryController');
Route::post('hr/salary/search', 'Hr\SalaryController@search');
Route::post('hr/salary/search_items', 'Hr\SalaryController@search_items');
Route::post('hr/salary/delete', 'Hr\SalaryController@delete');

// Resource Route for Advance Transactions
Route::resource('hr/advance', 'Hr\AdvanceController');
Route::get('hr/advance/create/{tp}', 'Hr\AdvanceController@create');
Route::post('hr/advance/search', 'Hr\AdvanceController@search');
Route::post('hr/advance/delete', 'Hr\AdvanceController@delete');
