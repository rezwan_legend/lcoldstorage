<?php

use Illuminate\Support\Facades\Auth;

//pc settings related routes
Route::resource('/essential/pc_settings', 'EssentialSetting\PcSettingsController');
Route::post('/essential/pc_settings/delete', 'EssentialSetting\PcSettingsController@delete');
Route::post('/essential/pc_settings/search', 'EssentialSetting\PcSettingsController@search');

//shade settings related routes
Route::resource('/essential/shade_settings', 'EssentialSetting\ShadeSettingsController');
Route::post('/essential/shade_settings/delete', 'EssentialSetting\ShadeSettingsController@delete');
Route::post('/essential/shade_settings/search', 'EssentialSetting\ShadeSettingsController@search');

//shade settings related routes
Route::resource('/essential/agent_list', 'EssentialSetting\AgentPartyController');
Route::post('/essential/agent_list/delete', 'EssentialSetting\AgentPartyController@delete');
Route::post('/essential/agent_list/search', 'EssentialSetting\AgentPartyController@search');

//Bank settings related routes
Route::resource('/essential/bank_settings', 'EssentialSetting\BankSettingsController');
Route::post('/essential/bank_settings/delete', 'EssentialSetting\BankSettingsController@delete');
Route::post('/essential/bank_settings/search', 'EssentialSetting\BankSettingsController@search');

//condition related routes
Route::resource('/essential/conditions', 'EssentialSetting\ConditionController');
Route::post('/essential/conditions/delete', 'EssentialSetting\ConditionController@delete');
Route::post('/essential/conditions/search', 'EssentialSetting\ConditionController@search');

//condition related routes
Route::resource('/essential/bag_type', 'EssentialSetting\BagTypeController');
Route::post('/essential/bag_type/delete', 'EssentialSetting\BagTypeController@delete');
Route::post('/essential/bag_type/search', 'EssentialSetting\BagTypeController@search');

//basic settings

Route::resource('/essential/basic_settings', 'EssentialSetting\BasicSettingsController');
Route::post('/essential/basic_setting/change', 'EssentialSetting\BasicSettingsController@change');
Route::post('/essential/basic_settings/recent/update', 'EssentialSetting\BasicSettingsController@update')->name('settings.update');



//Branches related routes
Route::resource('/essential/branches', 'EssentialSetting\BranchesController');
Route::post('/essential/branches/delete', 'EssentialSetting\BranchesController@delete');
Route::post('/essential/branches/search', 'EssentialSetting\BranchesController@search');


