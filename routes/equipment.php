<?php

/* Equipment Related Route */
Route::get('equipment', 'Equipment\DashboardController@index');

/* Inventory Purchase Related Route */
Route::get('equipment/purchase/cart', 'Equipment\PurchaseController@cart');
Route::post('equipment/purchase/save_cart', 'Equipment\PurchaseController@cart');
Route::get('equipment/purchase/clear_cart', 'Equipment\PurchaseController@clear_cart');
Route::get('equipment/purchase/detail/{id}', 'Equipment\PurchaseController@detail');
Route::get('equipment/purchase/payment/{id}', 'Equipment\PurchaseController@payment');
Route::post('equipment/purchase/payment/{id}', 'Equipment\PurchaseController@payment');
Route::get('equipment/purchase/create_stock', 'Equipment\PurchaseController@create_stock');
Route::get('equipment/purchase/update_stock', 'Equipment\PurchaseController@update_stock');
Route::get('equipment/purchase/update_items', 'Equipment\PurchaseController@update_items');
Route::get('equipment/purchase/update_invoice_no', 'Equipment\PurchaseController@update_invoice_no');
Route::get('equipment/purchase/update_institute', 'Equipment\PurchaseController@update_institute');
Route::get('equipment/purchase/items', 'Equipment\PurchaseController@items');
Route::resource('equipment/purchase', 'Equipment\PurchaseController');
Route::post('equipment/purchase/search', 'Equipment\PurchaseController@search');
Route::post('equipment/purchase/search_items', 'Equipment\PurchaseController@search_items');
Route::post('equipment/purchase/delete', 'Equipment\PurchaseController@delete');
Route::post('equipment/purchase/search_product', 'Equipment\PurchaseController@search_product');
Route::post('equipment/purchase/add_cart_item', 'Equipment\PurchaseController@add_to_cart');
Route::post('equipment/purchase/delete_cart_item', 'Equipment\PurchaseController@delete_cart_item');
Route::post('equipment/purchase/delete_item', 'Equipment\PurchaseController@delete_item');
Route::post('equipment/purchase/process', 'Equipment\PurchaseController@process');
Route::post('equipment/purchase/reset', 'Equipment\PurchaseController@reset');

/* Inventory Purchase Return Related Route */
Route::get('equipment/purchase-return/cart', 'Equipment\PurchaseReturnController@cart');
Route::post('equipment/purchase-return/save_cart', 'Equipment\PurchaseReturnController@cart');
Route::get('equipment/purchase-return/clear_cart', 'Equipment\PurchaseReturnController@clear_cart');
Route::get('equipment/purchase-return/detail/{id}', 'Equipment\PurchaseReturnController@detail');
Route::get('equipment/purchase-return/payment/{id}', 'Equipment\PurchaseReturnController@payment');
Route::post('equipment/purchase-return/payment/{id}', 'Equipment\PurchaseReturnController@payment');
Route::get('equipment/purchase-return/create_stock', 'Equipment\PurchaseReturnController@create_stock');
Route::get('equipment/purchase-return/update_stock', 'Equipment\PurchaseReturnController@update_stock');
Route::get('equipment/purchase-return/update_items', 'Equipment\PurchaseReturnController@update_items');
Route::get('equipment/purchase-return/update_invoice_no', 'Equipment\PurchaseReturnController@update_invoice_no');
Route::get('equipment/purchase-return/items', 'Equipment\PurchaseReturnController@items');
Route::resource('equipment/purchase-return', 'Equipment\PurchaseReturnController');
Route::post('equipment/purchase-return/search', 'Equipment\PurchaseReturnController@search');
Route::post('equipment/purchase-return/delete', 'Equipment\PurchaseReturnController@delete');
Route::post('equipment/purchase-return/search_product', 'Equipment\PurchaseReturnController@search_product');
Route::post('equipment/purchase-return/add_cart_item', 'Equipment\PurchaseReturnController@add_to_cart');
Route::post('equipment/purchase-return/delete_cart_item', 'Equipment\PurchaseReturnController@delete_cart_item');
Route::post('equipment/purchase-return/delete_item', 'Equipment\PurchaseReturnController@delete_item');
Route::post('equipment/purchase-return/process', 'Equipment\PurchaseReturnController@process');
Route::post('equipment/purchase-return/reset', 'Equipment\PurchaseReturnController@reset');

/* Inventory Sale Related Route */
Route::get('equipment/delivery/cart', 'Equipment\SaleController@cart');
Route::post('equipment/delivery/save_cart', 'Equipment\SaleController@cart');
Route::get('equipment/delivery/clear_cart', 'Equipment\SaleController@clear_cart');
Route::get('equipment/delivery/detail/{id}', 'Equipment\SaleController@detail');
Route::get('equipment/delivery/payment/{id}', 'Equipment\SaleController@payment');
Route::post('equipment/delivery/payment/{id}', 'Equipment\SaleController@payment');
Route::get('equipment/delivery/create_stock', 'Equipment\SaleController@create_stock');
Route::get('equipment/delivery/update_stock', 'Equipment\SaleController@update_stock');
Route::get('equipment/delivery/update_items', 'Equipment\SaleController@update_items');
Route::get('equipment/delivery/update_invoice_no', 'Equipment\SaleController@update_invoice_no');
Route::get('equipment/delivery/update_institute', 'Equipment\SaleController@update_institute');
Route::get('equipment/delivery/items', 'Equipment\SaleController@items');
Route::resource('equipment/delivery', 'Equipment\SaleController');
Route::post('equipment/delivery/search', 'Equipment\SaleController@search');
Route::post('equipment/delivery/search_items', 'Equipment\SaleController@search_items');
Route::post('equipment/delivery/delete', 'Equipment\SaleController@delete');
Route::post('equipment/delivery/search_product', 'Equipment\SaleController@search_product');
Route::post('equipment/delivery/add_cart_item', 'Equipment\SaleController@add_to_cart');
Route::post('equipment/delivery/delete_cart_item', 'Equipment\SaleController@delete_cart_item');
Route::post('equipment/delivery/delete_item', 'Equipment\SaleController@delete_item');
Route::post('equipment/delivery/process', 'Equipment\SaleController@process');
Route::post('equipment/delivery/reset', 'Equipment\SaleController@reset');

/* Inventory Sale Return Related Route */
Route::get('equipment/sale-return/cart', 'Equipment\SaleReturnController@cart');
Route::post('equipment/sale-return/save_cart', 'Equipment\SaleReturnController@cart');
Route::get('equipment/sale-return/clear_cart', 'Equipment\SaleReturnController@clear_cart');
Route::get('equipment/sale-return/detail/{id}', 'Equipment\SaleReturnController@detail');
Route::get('equipment/sale-return/payment/{id}', 'Equipment\SaleReturnController@payment');
Route::post('equipment/sale-return/payment/{id}', 'Equipment\SaleReturnController@payment');
Route::get('equipment/sale-return/create_stock', 'Equipment\SaleReturnController@create_stock');
Route::get('equipment/sale-return/update_stock', 'Equipment\SaleReturnController@update_stock');
Route::get('equipment/sale-return/update_items', 'Equipment\SaleReturnController@update_items');
Route::get('equipment/sale-return/update_invoice_no', 'Equipment\SaleReturnController@update_invoice_no');
Route::get('equipment/sale-return/items', 'Equipment\SaleReturnController@items');
Route::resource('equipment/sale-return', 'Equipment\SaleReturnController');
Route::post('equipment/sale-return/search', 'Equipment\SaleReturnController@search');
Route::post('equipment/sale-return/delete', 'Equipment\SaleReturnController@delete');
Route::post('equipment/sale-return/search_product', 'Equipment\SaleReturnController@search_product');
Route::post('equipment/sale-return/add_cart_item', 'Equipment\SaleReturnController@add_to_cart');
Route::post('equipment/sale-return/delete_cart_item', 'Equipment\SaleReturnController@delete_cart_item');
Route::post('equipment/sale-return/delete_item', 'Equipment\SaleReturnController@delete_item');
Route::post('equipment/sale-return/process', 'Equipment\SaleReturnController@process');
Route::post('equipment/sale-return/reset', 'Equipment\SaleReturnController@reset');
