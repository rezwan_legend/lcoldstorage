<?php

// Distribution Type
Route::resource('distribution/type', 'Distribution\TypeController');
Route::post('distribution/type/search', 'Distribution\TypeController@search');
Route::post('distribution/type/delete', 'Distribution\TypeController@delete');

// Distribution Setting
Route::resource('distribution/setting', 'Distribution\SettingController');
Route::post('distribution/setting/search', 'Distribution\SettingController@search');
Route::post('distribution/setting/delete', 'Distribution\SettingController@delete');
Route::post('distribution/setting/customer-list', 'Distribution\SettingController@getCustomerList');

// distribution Target Setting
Route::get('distribution/sales_target/items', 'Distribution\SalesTargetController@items');
Route::post('distribution/sales_target/search_items', 'Distribution\SalesTargetController@search_items');
Route::resource('distribution/sales_target', 'Distribution\SalesTargetController');
Route::post('distribution/sales_target/search', 'Distribution\SalesTargetController@search');
Route::post('distribution/sales_target/delete', 'Distribution\SalesTargetController@delete');
Route::post('distribution/sales_target/customer-list', 'Distribution\SalesTargetController@getCustomerList');

// distribution Commission Setting
Route::get('distribution/commission_setting/items', 'Distribution\CommissionSettingController@items');
Route::post('distribution/commission_setting/search_items', 'Distribution\CommissionSettingController@search_items');
Route::get('distribution/commission_setting/{subhead}/{month}', 'Distribution\CommissionSettingController@show');
Route::resource('distribution/commission_setting', 'Distribution\CommissionSettingController');
Route::post('distribution/commission_setting/search', 'Distribution\CommissionSettingController@search');
Route::post('distribution/commission_setting/delete', 'Distribution\CommissionSettingController@delete');
Route::post('distribution/commission_setting/partial_form', 'Distribution\CommissionSettingController@getPartialForm');

// reports routes
Route::get('distribution/report/monthly_commission', 'Distribution\ReportController@monthly_commission');
Route::post('distribution/report/search_monthly_commission', 'Distribution\ReportController@search_monthly_commission');

// depo stocks
Route::get('depo/stocks/details', 'Depo\StockController@detail');
Route::post('depo/stocks/search_details', 'Depo\StockController@search_detail');
Route::resource('depo/stocks', 'Depo\StockController');
Route::post('depo/stocks/search', 'Depo\StockController@search');

// depo Sale Orders
Route::get('depo/sale_orders', 'Depo\SaleOrderController@index');
Route::post('depo/sale_orders/search', 'Depo\SaleOrderController@search');

// depo Delivery
Route::get('depo/delivery/create/{id}', 'Depo\DeliveryController@create');
Route::get('depo/delivery/{id}/confirm', 'Depo\DeliveryController@confirm');
Route::get('depo/delivery/items', 'Depo\DeliveryController@items');
Route::post('depo/delivery/items_search', 'Depo\DeliveryController@search_items');
Route::get('depo/delivery/ledger', 'Depo\DeliveryController@ledger');
Route::post('depo/delivery/ledger_search', 'Depo\DeliveryController@ledger_search');
Route::resource('depo/delivery', 'Depo\DeliveryController');
Route::post('depo/delivery/search', 'Depo\DeliveryController@search');
Route::post('depo/delivery/delete', 'Depo\DeliveryController@delete');
Route::post('depo/delivery/delete_item', 'Depo\DeliveryController@delete_item');

Route::get('depo/report/monthly_sales', 'Depo\ReportController@monthly_sales');
Route::post('depo/report/search_monthly_sales', 'Depo\ReportController@search_monthly_sales');
Route::get('depo/report/product_sale', 'Depo\ReportController@product_sale');
Route::post('depo/report/search_product_sale', 'Depo\ReportController@search_product_sale');

// SR sales
Route::post('sr/sale/get_barcode_list', 'Sr\SaleController@get_barcode_list');
Route::post('sr/sale/save_order', 'Sr\SaleController@save_order');
Route::get('sr/sale/items', 'Sr\SaleController@items');
Route::post('sr/sale/search_items', 'Sr\SaleController@search_items');
Route::get('sr/sale/itemlist/{id}', 'Sr\SaleController@itemlist');
Route::post('sr/sale/search_itemlist', 'Sr\SaleController@search_itemlist');
Route::post('sr/sale/add_item', 'Sr\SaleController@add_item');
Route::get('sr/sale/item_detail/{id}/{productId}', 'Sr\SaleController@item_detail');
Route::get('sr/sale/remove_item/{id}', 'Sr\SaleController@remove_item');
Route::post('sr/sale/check_imei_sold', 'Sr\SaleController@check_imei_sold');
Route::post('sr/sale/search_product', 'Sr\SaleController@search_product');
Route::get('sr/sale/ledger', 'Sr\SaleController@ledger');
Route::post('sr/sale/ledger_search', 'Sr\SaleController@ledger_search');
Route::get('sr/sale/{id}/confirm', 'Sr\SaleController@confirm');
Route::resource('sr/sale', 'Sr\SaleController');
Route::post('sr/sale/search', 'Sr\SaleController@search');
Route::post('sr/sale/delete', 'Sr\SaleController@delete');
Route::post('sr/sale/delete_item', 'Sr\SaleController@delete_item');
Route::post('sr/sale/reset', 'Sr\SaleController@reset');





