<?php

Route::get('production', 'Production\DashboardController@index');

// Production Type Routes
Route::resource('production/type', 'Production\TypeController');
Route::post('production/type/search', 'Production\TypeController@search');
Route::post('production/type/delete', 'Production\TypeController@delete');

// Production Order Route
Route::get('production/order/itemlist/{id}', 'Production\OrderController@itemlist');
Route::post('production/order/search_itemlist', 'Production\OrderController@search_itemlist');
Route::get('production/order/item_detail/{id}/{productId}', 'Production\OrderController@item_detail');
Route::get('production/order/remove_item/{id}', 'Production\OrderController@remove_item');
Route::post('production/order/add_item', 'Production\OrderController@add_item');
Route::get('production/order/items', 'Production\OrderController@items');
Route::post('production/order/search_items', 'Production\OrderController@search_items');
Route::get('production/order/{id}/confirm', 'Production\OrderController@confirm');
Route::resource('production/order', 'Production\OrderController');
Route::post('production/order/search', 'Production\OrderController@search');
Route::post('production/order/reset', 'Production\OrderController@reset');
Route::post('production/order/delete', 'Production\OrderController@delete');

// Production Depot Route
Route::get('production/depot/itemlist/{id}', 'Production\DepotController@itemlist');
Route::post('production/depot/search_itemlist', 'Production\DepotController@search_itemlist');
Route::get('production/depot/item_detail/{id}/{productId}', 'Production\DepotController@item_detail');
Route::get('production/depot/remove_item/{id}', 'Production\DepotController@remove_item');
Route::post('production/depot/add_item', 'Production\DepotController@add_item');
Route::get('production/depot/items', 'Production\DepotController@items');
Route::post('production/depot/search_items', 'Production\DepotController@search_items');
Route::get('production/depot/{id}/confirm', 'Production\DepotController@confirm');
Route::resource('production/depot', 'Production\DepotController');
Route::post('production/depot/search', 'Production\DepotController@search');
Route::post('production/depot/reset', 'Production\DepotController@reset');
Route::post('production/depot/delete', 'Production\DepotController@delete');

// Raw Setting
Route::get('production/raw_setting/itemlist/{id}', 'Production\RawSettingController@itemlist');
Route::post('production/raw_setting/search_itemlist', 'Production\RawSettingController@search_itemlist');
Route::get('production/raw_setting/item_detail/{id}/{productId}', 'Production\RawSettingController@item_detail');
Route::get('production/raw_setting/remove_item/{id}', 'Production\RawSettingController@remove_item');
Route::post('production/raw_setting/add_item', 'Production\RawSettingController@add_item');
Route::resource('production/raw_setting', 'Production\RawSettingController');
Route::post('production/raw_setting/search', 'Production\RawSettingController@search');
Route::post('production/raw_setting/reset', 'Production\RawSettingController@reset');
Route::post('production/raw_setting/delete', 'Production\RawSettingController@delete');

// Cost Setting
Route::get('production/cost_setting/itemlist/{id}', 'Production\CostSettingController@itemlist');
Route::post('production/cost_setting/search_itemlist', 'Production\CostSettingController@search_itemlist');
Route::get('production/cost_setting/item_detail/{id}/{productId}', 'Production\CostSettingController@item_detail');
Route::get('production/cost_setting/remove_item/{id}', 'Production\CostSettingController@remove_item');
Route::post('production/cost_setting/add_item', 'Production\CostSettingController@add_item');
Route::resource('production/cost_setting', 'Production\CostSettingController');
Route::post('production/cost_setting/search', 'Production\CostSettingController@search');
Route::post('production/cost_setting/reset', 'Production\CostSettingController@reset');
Route::post('production/cost_setting/delete', 'Production\CostSettingController@delete');
