<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    // return bcrypt('admin');
    return view('auth.login');
})->middleware('guest');
Auth::routes();

//Clear View cache:
Route::get('/view-clear', function () {
    Artisan::call('view:clear');
    Artisan::call('cache:clear');
    return redirect()->back()->with('success', 'Page Refresh Successfull');
});
Route::get('/clear_view_cache', function () {
    Artisan::call('view:clear');
    Artisan::call('cache:clear');
    return redirect()->back()->with('success', 'Page Refresh Successfull');
});

Route::get('/home', function () {
    return view('home');
});
Route::get('/signout', 'LogoutController@signout');

// Resource Routes for UserController
Route::get('user/update_key', 'UserController@update_key');
Route::get('user/{id}/admin_edit', 'UserController@admin_edit');
Route::get('user/{id}/access', 'UserController@user_access_by_id');
Route::post('user/acess/update', 'UserController@update_user_access_by_id');
Route::get('user/{id}/password', 'UserController@password');
Route::post('password/update', 'UserController@update_user_password');
Route::get('user/{id}/active', 'UserController@user_active');
Route::get('user/{id}/inactive', 'UserController@user_inactive');
Route::resource('user', 'UserController');
Route::post('user/search', 'UserController@search');
Route::post('user/delete', 'UserController@delete');

// /* Product Type Related Route */


    Route::resource('product_type', 'Setting\ProductModule\ProductTypeController');
    Route::post('product_type/search', 'Setting\ProductModule\ProductTypeController@search');
    Route::post('product_type/delete', 'Setting\ProductModule\ProductTypeController@delete');



// Category Routes
Route::resource('category', 'Setting\ProductModule\CategoryController');
Route::post('category/search', 'Setting\ProductModule\CategoryController@search');
Route::post('category/delete', 'Settin\ProductModule\CategoryController@delete');
Route::post('category/categoryByType', 'Setting\ProductModule\CategoryController@categoryListByType');

// Product Routes
Route::post('products/get_products', 'Setting\ProductModule\ProductController@get_products');
Route::post('products/to_json_data', 'Setting\ProductModule\ProductController@get_json_data');
Route::resource('products', 'Setting\ProductModule\ProductController');
Route::post('products/search', 'Setting\ProductModule\ProductController@search');
Route::post('products/delete', 'Setting\ProductModule\ProductController@delete');

// Unit Routes
Route::resource('/unit', 'Setting\ProductModule\UnitController');
Route::post('unit/search', 'Setting\ProductModule\UnitController@search');
Route::post('unit/delete', 'Setting\ProductModule\UnitController@delete');

// unit size
Route::resource('unit_size', 'Setting\ProductModule\UnitSizeController');
Route::post('unit_size/search', 'Setting\ProductModule\UnitSizeController@search');
Route::post('unit_size/delete', 'Setting\ProductModule\UnitSizeController@delete');
Route::post('unit_size/prod_type', 'Setting\ProductModule\UnitSizeController@prod_typeByInstitute');
Route::post('unit_size/size_list', 'Setting\ProductModule\UnitSizeController@sizeList');

// Rent Type Releted Routes
Route::resource('/rent_type', 'Setting\RentSetting\RentTypeController');
Route::post('rent_type/search', 'Setting\RentSetting\RentTypeController@search');
Route::post('rent_type/delete', 'Setting\RentSetting\RentTypeController@delete');

//chamber setting
Route::resource('/chamber_setting', 'Setting\LocationSetting\ChamberSettingController');
Route::post('/chamber_setting/search', 'Setting\LocationSetting\ChamberSettingController@search');
Route::post('/chamber_setting/delete', 'Setting\LocationSetting\ChamberSettingController@delete');

// Floor 
Route::resource('/floor_setting', 'Setting\LocationSetting\FloorController');
Route::post('floor_setting/search', 'Setting\LocationSetting\FloorController@search');
Route::post('floor_setting/delete', 'Setting\LocationSetting\FloorController@delete');

// Pocket 
Route::resource('/pocket_setting', 'Setting\LocationSetting\PocketController');
Route::post('pocket_setting/search', 'Setting\LocationSetting\PocketController@search');
Route::post('pocket_setting/delete/', 'Setting\LocationSetting\PocketController@delete');
Route::post('pocket_setting/floorListByChamber', 'Setting\LocationSetting\PocketController@floorListByChamber');

//store condition
Route::resource('/store_condition', 'Setting\StoreSetting\StoreConditionController');
Route::post('/store_condition/search', 'Setting\StoreSetting\StoreConditionController@search');
Route::post('/store_condition/delete', 'Setting\StoreSetting\StoreConditionController@delete');

//loan condition
Route::resource('/loan_condition', 'Setting\StoreSetting\LoanConditionController');
Route::post('/loan_condition/search', 'Setting\StoreSetting\LoanConditionController@search');
Route::post('/loan_condition/delete', 'Setting\StoreSetting\LoanConditionController@delete');

//Delivery condition
Route::resource('/delivery_condition', 'Setting\DeleverySetting\DeliveryConditionController');
Route::post('/delivery_condition/search', 'Setting\DeleverySetting\DeliveryConditionController@search');
Route::post('/delivery_condition/delete', 'Setting\DeleverySetting\DeliveryConditionController@delete');

//party type
Route::resource('/party_type', 'Setting\Party\PartyTypeController');
Route::post('/party_type/search', 'Setting\Party\PartyTypeController@search');
Route::post('/party_type/delete', 'Setting\Party\PartyTypeController@delete');

// Resource Route for HeadController
Route::resource('head', 'Setting\AccountSetting\HeadController');
Route::post('head/search', 'Setting\AccountSetting\HeadController@search');
Route::post('head/delete', 'Setting\AccountSetting\HeadController@delete');
Route::post('head/subhead_dropdown_list', 'Setting\AccountSetting\HeadController@subhead_dropdown_list');
Route::post('head/max_id/get/', 'Setting\AccountSetting\HeadController@getMaxId')->name('head.getMaxId');

// Resource Route for SubHeadController
Route::resource('subhead', 'Setting\AccountSetting\SubHeadController');
Route::post('subhead/search', 'Setting\AccountSetting\SubHeadController@search');
Route::post('subhead/delete', 'Setting\AccountSetting\SubHeadController@delete');
Route::post('subhead/subhead', 'Setting\AccountSetting\SubHeadController@get_sub_head');
Route::post('subhead/particular', 'Setting\AccountSetting\SubHeadController@get_particular');
Route::post('subhead/max_id/get/', 'Setting\AccountSetting\SubHeadController@getMaxId')->name('subhead.getMaxId');
Route::post('subhead/throught/head/', 'Setting\AccountSetting\SubHeadController@getSubheadThroughHead')->name('getSubheadThroughHead');

// Resource Route for Particulars
Route::get('particular/ledger/{id}', 'ParticularController@ledger');
Route::post('particular/search_ledger', 'ParticularController@search_ledger');
Route::post('particular/get_customers', 'ParticularController@get_dsr_customers');
Route::resource('particular', 'ParticularController');
Route::get('particular/create/{id}', 'ParticularController@create_particular');
Route::post('particular/search', 'ParticularController@search');
Route::post('particular/delete', 'ParticularController@delete');


// Resource Route for SubHeadController
Route::resource('subhead', 'Setting\AccountSetting\SubHeadController');
Route::post('subhead/search', 'Setting\AccountSetting\SubHeadController@search');
Route::post('subhead/delete', 'Setting\AccountSetting\SubHeadController@delete');
Route::post('subhead/subhead', 'Setting\AccountSetting\SubHeadController@get_sub_head');
Route::post('subhead/particular', 'Setting\AccountSetting\SubHeadController@get_particular');

// Resource Route for Particular
Route::get('particular/opening-balance', 'ParticularController@openingBalance');
Route::post('particular/opening-balance/form', 'ParticularController@getOpeningBalanceForm');
Route::post('particular/opening-balance/save_data', 'ParticularController@storeOpeningBalance');

Route::get('particular/ledger/{id}', 'ParticularController@ledger');
Route::post('particular/ledger_search', 'ParticularController@search_ledger');
Route::resource('particulars', 'ParticularController');
Route::get('particular/create/{id}', 'ParticularController@create_particular');
Route::post('particulars/search', 'ParticularController@search');
Route::post('particulars/delete', 'ParticularController@delete');
Route::post('particulars/particular', 'ParticularController@get_particular');

/* Product Delivery Related Route */
Route::get('dellivery/items', 'DeliveryController@sales_itmes');
Route::resource('dellivery', 'DeliveryController');
Route::post('dellivery/itmes/search', 'DeliveryController@sales_itmes_search');
Route::post('dellivery/search', 'DeliveryController@search');
Route::post('dellivery/delete', 'DeliveryController@delete');
Route::get('dellivery/{id}/confirm', 'DeliveryController@confirm');
Route::get('dellivery/{id}/reset', 'DeliveryController@reset');
Route::get('dellivery/gatepass/{id}', 'DeliveryController@gatepass');
Route::post('dellivery/add_item', 'DeliveryController@add_item');
Route::get('dellivery/itemlist/{id}', 'DeliveryController@itemlist');
Route::get('dellivery/remove_item/{id}', 'DeliveryController@remove_item');
Route::post('dellivery/search_itemlist', 'DeliveryController@search_itemlist');
Route::get('dellivery/item_edit/{id}', 'DeliveryController@edit_item');
Route::post('dellivery/update_item/', 'DeliveryController@update_item');
Route::post('search_product/', 'DeliveryController@search_product');

// Party
Route::resource('/party', 'Setting\Party\PartyController');
Route::post('/party/search', 'Setting\Party\PartyController@search');
Route::post('/party/delete', 'Setting\Party\PartyController@delete');

// unit size
Route::resource('unit_size', 'Setting\ProductModule\UnitSizeController');
Route::post('unit_size/search', 'Setting\ProductModule\UnitSizeController@search');
Route::post('unit_size/delete', 'Setting\ProductModule\UnitSizeController@delete');
Route::post('unit_size/prod_type', 'Setting\ProductModule\UnitSizeController@prod_typeByInstitute');
Route::post('unit_size/size_list', 'Setting\ProductModule\UnitSizeController@sizeList');

// Rent Releted Routes
Route::resource('/rent', 'Setting\RentSetting\RentController');
Route::post('rent/search', 'Setting\RentSetting\RentController@search');
Route::post('rent/delete', 'Setting\RentSetting\RentController@delete');
Route::post('rent/categoryByType', 'Setting\RentSetting\RentController@categoryListByType');
Route::post('rent/productByCategory', 'Setting\RentSetting\RentController@productListByCategory');
Route::post('rent/product_sizeByUnit', 'Setting\RentSetting\RentController@product_sizeByUnit');

//stock
Route::resource('stock', 'StockController');

//Party Commission
Route::resource('/party_commission', 'Setting\PartyCommission\PartyCommissionController');
Route::post('/party_commission/search', 'Setting\PartyCommission\PartyCommissionController@search');
Route::post('/party_commission/delete', 'Setting\PartyCommission\PartyCommissionController@delete');
Route::post('/party_commission/partyListByType', 'Setting\PartyCommission\PartyCommissionController@partyListByType');
Route::post('/party_commission/categoryListByType', 'Setting\PartyCommission\PartyCommissionController@categoryListByType');
Route::post('/party_commission/productByCategory', 'Setting\PartyCommission\PartyCommissionController@productListByCategory');
Route::post('/party_commission/unitSizeByUnit', 'Setting\\artyCommission\PartyCommissionController@unitSizeByUnit');


//party rent
Route::resource('/party_rent', 'Setting\PartyCommission\PartyRentController');
Route::post('/party_rent/search', 'Setting\PartyCommission\PartyRentController@search');
Route::post('/party_rent/delete', 'Setting\PartyCommission\PartyRentController@delete');
Route::post('/party_rent/PartyNameByType', 'Setting\PartyCommission\PartyRentController@PartyNameByType');
Route::post('/party_rent/ProductByCategory', 'Setting\PartyCommission\PartyRentController@ProductByCategory');
Route::post('/party_rent/UnitSizeByUnit', 'Setting\PartyCommission\PartyRentController@UnitSizeByUnit');
Route::post('/party_rent/DataSave', 'Setting\PartyCommission\PartyRentController@DataSave');
Route::post('/party_rent/search_itemlist/list', 'Setting\PartyCommission\PartyRentController@search_itemlist');
Route::post('/party_rent/remove/item', 'Setting\PartyCommission\PartyRentController@remove_item');


//party commision multi
Route::resource('/party_commission_multi', 'Setting\PartyCommission\PartyCommissionMultiController');
Route::post('/party_commission_multi/search', 'Setting\PartyCommission\PartyCommissionMultiController@search');
Route::post('/party_commission_multi/delete', 'Setting\PartyCommission\PartyCommissionMultiController@delete');
Route::post('/party_commission_multi/PartyNameByType', 'Setting\PartyCommission\PartyCommissionMultiController@PartyNameByType');
Route::post('/party_commission_multi/ProductByCategory', 'Setting\PartyCommission\PartyCommissionMultiController@ProductByCategory');
Route::post('/party_commission_multi/UnitSizeByUnit', 'Setting\PartyCommission\PartyCommissionMultiController@UnitSizeByUnit');
Route::post('/party_commission_multi/DataSave', 'Setting\PartyCommission\PartyCommissionMultiController@DataSave');
Route::post('/party_commission_multi/itemList/createdLoanList', 'Setting\PartyCommissio\/PartyCommissionMultiController@createdLoanList');
Route::post('/party_commission_multi/currentItem/update', 'Setting\PartyCommission\PartyCommissionMultiController@currentItemUpdate');




//Delivery condition
Route::resource('/delivery_condition', 'Setting\DeleverySetting\DeliveryConditionController');
Route::post('/delivery_condition/search', 'Setting\DeleverySetting\DeliveryConditionController@search');
Route::post('/delivery_condition/delete', 'Setting\DeleverySetting\DeliveryConditionController@delete');

// DSR routes
Route::get('/dsr', 'HomeController@dsr_index');
Route::get('/dsr/home', 'HomeController@dsr_index');


//party Loan
Route::resource('/party_loan', 'Setting\PartyCommission\PartyLoanController');
Route::post('/party_loan/search', 'Setting\PartyCommission\PartyLoanController@search');
Route::post('/party_loan/delete', 'Setting\PartyCommission\PartyLoanController@delete');
Route::post('/party_laon/partyListByType', 'Setting\PartyCommission\PartyLoanController@partyListByType');
Route::post('/party_loan/ProductByCategory', 'Setting\PartyCommission\PartyLoanController@ProductByCategory');
Route::post('/party_loan/UnitSizeByUnit', 'Setting\PartyCommission\PartyLoanController@UnitSizeByUnit');
Route::post('/party_loan/DataSave', 'Setting\PartyCommission\PartyLoanController@DataSave');
Route::post('/party_loan/itemList/createdLoanList', 'Setting\PartyCommission\PartyLoanController@createdLoanList');
Route::post('/party_loan/currentItem/update', 'Setting\PartyCommission\PartyLoanController@currentItemUpdate');

// Advance Loan Payment
Route::get('/loan/advance/payment_create', 'AdvanceLoanPaymentController@index');
Route::post('/advance/loan/payment/agent_list', 'AdvanceLoanPaymentController@agentInformation');
Route::post('/advance/loan/payment/bag_type_ist', 'AdvanceLoanPaymentController@getBagTypeInformation');

// Sr Setting
Route::resource('/sr/setting', 'SrSettingController');

// Sr 
Route::resource('/sr', 'SrController');
Route::post('/sr/search', 'SrController@search');
Route::post('/sr/delete', 'SrController@delete');

//Bank Transaction related routes
Route::resource('/bank_transaction', 'Accounts\BankTransactionController');
Route::post('/bank_transaction/delete', 'Accounts\BankTransactionController@delete');
Route::post('/bank_transaction/search', 'Accounts\BankTransactionController@search');
Route::post('bank_transaction/ledger', 'Accounts\BankTransactionController@show');


//Cash_transaction related routes
Route::resource('/cash_transaction', 'Accounts\CashTransactionController');
Route::post('/cash_transaction/delete', 'Accounts\CashTransactionController@delete');
Route::post('/cash_transaction/search', 'Accounts\CashTransactionController@search');
Route::post('/cash_transaction/account', 'Accounts\CashTransactionController@AccountByBank');

// Resource Route for AccountHead 
Route::resource('/account_head', 'Accounts\AccountHeadController');
Route::post('account_head/search', 'Accounts\AccountHeadController@search');
Route::post('account_head/delete', 'Accounts\AccountHeadController@delete');
Route::post('account_head/ledger', 'Accounts\AccountHeadController@show');
Route::post('account_head/subhead_dropdown_list', 'Accounts\AccountHeadController@subhead_dropdown_list');
Route::post('account_head/max_id/get/', 'Accounts\AccountHeadController@getMaxId')->name('head.getMaxId');


Route::resource('requisition', 'Accounts\RequisitionController');
Route::post('requisition/delete', 'Accounts\RequisitionController@delete');
Route::post('requisition/search', 'Accounts\RequisitionController@search');


Route::resource('product_in_create', 'DailyWork\ProductInCreateController');
Route::post('product_in_create/delete', 'DailyWork\ProductInCreateController@delete');
Route::post('product_in_create/search', 'DailyWork\ProductInCreateController@search');
Route::post('product_in_create/agent', 'DailyWork\ProductInCreateController@AgentsInfo');
